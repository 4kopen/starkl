#!/bin/bash
# This script is auto-generated, and must be run from a checkouted working tree.

set -euo pipefail

dir=$(cd "$(dirname "$0")" && pwd)

case $1 in

kconfig-frontends|kconfig-frontends-v4.11.0.1)
# ST Git URL: git://ymorin.is-a-geek.org/kconfig-frontends
# ST Git ref: v4.11.0.1
echo Preparing kconfig-frontends-custom...
mkdir kconfig-frontends-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory kconfig-frontends-custom --extract
echo kconfig-frontends-custom is ready!
;;

stmfbdev|stmfbdev-1.9.3)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/VIBE_STMfbdev_Ext
# ST Git ref: STMFBDEV_20.4P2_1.9.3
echo Preparing stmfbdev-custom...
mkdir stmfbdev-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stmfbdev-custom --extract
echo stmfbdev-custom is ready!
;;

multicom|multicom-4.6.18)
# ST Git URL: ssh://gitolite@codex.cro.st.com/multicom/multicom
# ST Git ref: Multicom_4KOpen_1.0_4.6.18
echo Preparing multicom-custom...
mkdir multicom-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/multicom.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory multicom-custom --extract
echo multicom-custom is ready!
;;

mali400|mali400-1.0.9.4KOPEN.0.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/cpt-middleware/MALI400_drvsrc_ext
# ST Git ref: MALI_ST_R4P1-1_0_9_4KOPEN_0.1
echo Preparing mali400-custom...
mkdir mali400-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory mali400-custom --extract
echo mali400-custom is ready!
;;

stpoke|stpoke-1.02)
# ST Git URL: ssh://gitolite@codex.cro.st.com/sharkvalidation/validation-valid-stpoke
# ST Git ref: STPOKE_1.02
echo Preparing stpoke-custom...
mkdir stpoke-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stpoke-custom --extract
echo stpoke-custom is ready!
;;

analog-firmware|analog-firmware-1.9.0)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/DISPLAY_analogsync_firmwares_ext
# ST Git ref: AnalogSync_ML_1.9.0
echo Preparing analog-firmware-custom...
mkdir analog-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/analog-firmware.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory analog-firmware-custom --extract
echo analog-firmware-custom is ready!
;;

uboot|uboot-v2015.01.02.25)
# Official Git URL: https://github.com/u-boot/u-boot
# Official Git tag: v2015.01
# ST Git URL: ssh://gitolite@codex.cro.st.com/u-boot/u-boot
# ST Git ref: UBOOT_4KOpen_1.1_v2015.01.02.25
echo Preparing uboot-custom...
mkdir uboot-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory uboot-custom --extract
echo uboot-custom is ready!
;;

audio-firmware|audio-firmware-38.3.0.24-4)
# ST Git URL: ssh://gitolite@codex.cro.st.com/pltf/PLTF_build_ext
# ST Git ref: PLTF_build_4KOpen_1.4_1.438.51
echo Preparing audio-firmware-custom...
mkdir audio-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{3\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/audio-firmware.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory audio-firmware-custom --extract
echo audio-firmware-custom is ready!
;;

streaming-engine|streaming-engine-1.129.104.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_StreamingEngine_ext
# ST Git ref: SE_4KOpen_1.1_1.129.104
echo Preparing streaming-engine-custom...
mkdir streaming-engine-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/streaming-engine.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory streaming-engine-custom --extract
echo streaming-engine-custom is ready!
;;

display|display-5.85.58.2)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/Display_Ext
# ST Git ref: DISPLAY_4KOpen_1.2_5.85.58
echo Preparing display-custom...
mkdir display-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory display-custom --extract
echo display-custom is ready!
;;

linux|linux-57.40)
# Official Git URL: git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
# Official Git tag: v3.10.92
# ST Git URL: ssh://gitolite@codex.cro.st.com/stlinux/STLinux-sti-3.10
# ST Git ref: KERNEL_4KOpen_1.0_3.10.92_57.40
echo Preparing linux-custom...
mkdir linux-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory linux-custom --extract
echo linux-custom is ready!
;;

mali400-bin|mali400-bin-0.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/cpt-middleware/MALI400_bin_int
# ST Git ref: MALI400_R4P1_01.9.0_4KOPEN_0.1
echo Preparing mali400-bin-custom...
mkdir mali400-bin-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory mali400-bin-custom --extract
echo mali400-bin-custom is ready!
;;

targetpack|targetpack-20160812-3)
# ST Git URL: ssh://gitolite@codex.cro.st.com/targetpacks/targetpacks-ext
# ST Git ref: TARGETPACKS_4KOpen_1.0_20160812-3
echo Preparing targetpack-custom...
mkdir targetpack-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/targetpack.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory targetpack-custom --extract
echo targetpack-custom is ready!
;;

sdk2-dt|sdk2-dt-1.438.51)
# ST Git URL: ssh://gitolite@codex.cro.st.com/pltf/PLTF_build_ext
# ST Git ref: PLTF_build_4KOpen_1.4_1.438.51
echo Preparing sdk2-dt-custom...
mkdir sdk2-dt-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{3\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/sdk2-dt.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory sdk2-dt-custom --extract
echo sdk2-dt-custom is ready!
;;

strelay|strelay-1.129.104.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_StreamingEngine_ext
# ST Git ref: SE_4KOpen_1.1_1.129.104
echo Preparing strelay-custom...
mkdir strelay-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{3\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/strelay.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory strelay-custom --extract
echo strelay-custom is ready!
;;

display-splash|display-splash-2.38.0)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/Display_SplashScreen_Ext
# ST Git ref: DISPLAY_SPLASHSCREEN_ML_2.38.0
echo Preparing display-splash-custom...
mkdir display-splash-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/display-splash.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory display-splash-custom --extract
echo display-splash-custom is ready!
;;

omx|omx-0.18.04)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_OMX
# ST Git ref: STREAM_OMX_20.4P1_0.18.04
echo Preparing omx-custom...
mkdir omx-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/omx.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory omx-custom --extract
echo omx-custom is ready!
;;

video-apiheaders|video-apiheaders-hades4.16)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_video_apiheaders_ext
# ST Git ref: 4KOpen_1.1_21.3-2.0.1_hades4.16
echo Preparing video-apiheaders-custom...
mkdir video-apiheaders-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory video-apiheaders-custom --extract
echo video-apiheaders-custom is ready!
;;

debug-scripts|debug-scripts-1.6.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/pltf/PLTF_debug_scripts
# ST Git ref: PLTF_debug_scripts_ML_1.6.1
echo Preparing debug-scripts-custom...
mkdir debug-scripts-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory debug-scripts-custom --extract
echo debug-scripts-custom is ready!
;;

stlinuxtv|stlinuxtv-1.187.33.2)
# ST Git URL: ssh://gitolite@codex.cro.st.com/apps/APPS_STLinuxTV_ext
# ST Git ref: STLINUXTV_4KOpen_1.2_1.187.33
echo Preparing stlinuxtv-custom...
mkdir stlinuxtv-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/stlinuxtv.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stlinuxtv-custom --extract
echo stlinuxtv-custom is ready!
;;

omxse|omxse-0.14.03)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_OMXSE
# ST Git ref: STREAM_OMXSE_4KOpen_1.0_0.14.03
echo Preparing omxse-custom...
mkdir omxse-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory omxse-custom --extract
echo omxse-custom is ready!
;;

pti-firmware|pti-firmware-2.99.14)
# ST Git URL: ssh://gitolite@codex.cro.st.com/dmxsec/DMXSEC_transport_engine-firmware_ext
# ST Git ref: TE_FW_20.4_2.99.14
echo Preparing pti-firmware-custom...
mkdir pti-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory pti-firmware-custom --extract
echo pti-firmware-custom is ready!
;;

transport-engine|transport-engine-115.32.2)
# ST Git URL: ssh://gitolite@codex.cro.st.com/dmxsec/DMXSEC_TransportEngine_ext
# ST Git ref: TE_4KOpen_1.2_115.32
echo Preparing transport-engine-custom...
mkdir transport-engine-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/transport-engine.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory transport-engine-custom --extract
echo transport-engine-custom is ready!
;;

video-firmware|video-firmware-hades4.16)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_video_firmwares_ext
# ST Git ref: 21.3-2.0.1_hades4.16
echo Preparing video-firmware-custom...
mkdir video-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/video-firmware.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory video-firmware-custom --extract
echo video-firmware-custom is ready!
;;

hades-firmware|hades-firmware-hades4.16)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_video_firmwares_ext
# ST Git ref: 21.3-2.0.1_hades4.16
echo Preparing hades-firmware-custom...
mkdir hades-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/projects/4kopen/export/hades-firmware.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory hades-firmware-custom --extract
echo hades-firmware-custom is ready!
;;

stgfx2|stgfx2-5.85.34)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/Grab_Render_Ext
# ST Git ref: GRAB_REND_4KOpen_1.2_5.85.34
echo Preparing stgfx2-custom...
mkdir stgfx2-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{1\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/stgfx2.exclude --files-from "$dir"/export/projects/4kopen/export/stgfx2.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stgfx2-custom --extract
echo stgfx2-custom is ready!
;;

fdma-firmware|fdma-firmware-20140320-14.3)
# ST Git URL: ssh://gitolite@codex.cro.st.com/infra/INFRA_fdma_firmwares_ext
# ST Git ref: fdma_firmware_20140320-14.3
echo Preparing fdma-firmware-custom...
mkdir fdma-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory fdma-firmware-custom --extract
echo fdma-firmware-custom is ready!
;;

stmedia-ctl|stmedia-ctl-INT12)
# ST Git URL: ssh://gitolite@codex.cro.st.com/apps/APPS_MediaCtl
# ST Git ref: MEDIA-CTL_INT12
echo Preparing stmedia-ctl-custom...
mkdir stmedia-ctl-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stmedia-ctl-custom --extract
echo stmedia-ctl-custom is ready!
;;

stmf|stmf-2.40.12.6)
# ST Git URL: ssh://gitolite@codex.cro.st.com/apps/APPS_STMF_1.2_ext
# ST Git ref: STMF_4KOpen_1.0_2.40.12.6
echo Preparing stmf-custom...
mkdir stmf-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stmf-custom --extract
echo stmf-custom is ready!
;;

infrastructure|infrastructure-1.103.12.1.2)
# ST Git URL: ssh://gitolite@codex.cro.st.com/infra/INFRA_infrastructure_ext
# ST Git ref: INFRASTRUCTURE_4KOpen_1.2_1.103.12.1
echo Preparing infrastructure-custom...
mkdir infrastructure-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/infrastructure.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory infrastructure-custom --extract
echo infrastructure-custom is ready!
;;

openmax-headers|openmax-headers-0.9.01)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_OMXHEADERS
# ST Git ref: STREAM_OMXHEADERS_20.4P1_0.9.01
echo Preparing openmax-headers-custom...
mkdir openmax-headers-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory openmax-headers-custom --extract
echo openmax-headers-custom is ready!
;;

obsp|obsp-2.18)
# ST Git URL: ssh://gitolite@codex.cro.st.com/bssp/obsp_release
# ST Git ref: OBSP_REL2.18
echo Preparing obsp-custom...
mkdir obsp-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/obsp.exclude --exclude-from "$dir"/export/shared/default/export/obsp.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory obsp-custom --extract
echo obsp-custom is ready!
;;

lpm-firmware|lpm-firmware-2.0.18)
# ST Git URL: ssh://gitolite@codex.cro.st.com/infra/INFRA_lpm-firmware_pkg_ext
# ST Git ref: FW_LPM_2.0.18
echo Preparing lpm-firmware-custom...
mkdir lpm-firmware-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/lpm-firmware.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory lpm-firmware-custom --extract
echo lpm-firmware-custom is ready!
;;

test-utils|test-utils-1.70.22)
# ST Git URL: ssh://gitolite@codex.cro.st.com/pltf/PLTF_tests_utils_ext
# ST Git ref: TEST_UTILS_EXT_4KOpen_1.0_1.70.22
echo Preparing test-utils-custom...
mkdir test-utils-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/test-utils.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory test-utils-custom --extract
echo test-utils-custom is ready!
;;

stmfapp|stmfapp-2.27.8)
# ST Git URL: ssh://gitolite@codex.cro.st.com/apps/APPS_STMFApp_1.2_ext
# ST Git ref: STMFApp_1.2_20.4P2_2.27.8
echo Preparing stmfapp-custom...
mkdir stmfapp-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory stmfapp-custom --extract
echo stmfapp-custom is ready!
;;

devlog|devlog-2.6.1)
# ST Git URL: ssh://gitolite@codex.cro.st.com/pltf/PLTF_devlog
# ST Git ref: PLTF_devlog_4KOpen_1.1_2.6.1
echo Preparing devlog-custom...
mkdir devlog-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory devlog-custom --extract
echo devlog-custom is ready!
;;

selector|selector-1.3.4)
# ST Git URL: ssh://gitolite@codex.cro.st.com/dmxsec/DMXSEC_Selector_ext
# ST Git ref: SELECTOR_EXT_4KOpen_1.0_1.3.4
echo Preparing selector-custom...
mkdir selector-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/selector.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory selector-custom --extract
echo selector-custom is ready!
;;

blitter|blitter-5.85.34)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/Grab_Render_Ext
# ST Git ref: GRAB_REND_4KOpen_1.2_5.85.34
echo Preparing blitter-custom...
mkdir blitter-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{1\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/blitter.exclude --files-from "$dir"/export/projects/4kopen/export/blitter.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory blitter-custom --extract
echo blitter-custom is ready!
;;

grab-rend|grab-rend-5.85.34)
# ST Git URL: ssh://gitolite@codex.cro.st.com/vibe/Grab_Render_Ext
# ST Git ref: GRAB_REND_4KOpen_1.2_5.85.34
echo Preparing grab-rend-custom...
mkdir grab-rend-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/projects/4kopen/export/grab-rend.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory grab-rend-custom --extract
echo grab-rend-custom is ready!
;;

audio-apiheaders|audio-apiheaders-38.3.0.20-2)
# ST Git URL: ssh://gitolite@codex.cro.st.com/stream/STREAM_audio_apiheaders_ext
# ST Git ref: 4KOpen_1.0_38.3.0.20-2
echo Preparing audio-apiheaders-custom...
mkdir audio-apiheaders-custom
tar --create --directory . --verbose --transform 's,^\([^/]*/\?\)\{0\},./,S' --exclude-from "$dir"/export/shared/default/export/default.exclude --files-from "$dir"/export/shared/default/export/default.include --show-transformed-names --owner=0 --group=0 --numeric-owner | tar --directory audio-apiheaders-custom --extract
echo audio-apiheaders-custom is ready!
;;


*)
echo "Unspecified component name.  Maybe you are not supposed to use this script."
;;

esac
