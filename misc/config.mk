# Included by top-level GNUmakefile.  It must provides:
# - printf-how-to-launch/flash functions
# - distros
# - required-$(distro)-packages for each "distro" in $(distros)
# - splitted-headers

######################################################################

printf-how-to-launch = @printf '\nYou can now launch these images:\n\n    launchers/stmc2/launch-starkl $1 $$stmc2_ip\n\n'
printf-how-to-flash  = @printf 'You can also flash these images:\n\n    launchers/stmc2/launch-starkl -f $1 $$stmc2_ip\n\n'


######################################################################

distros = ubuntu debian centos redhat fedora

common-required-packages = bzip2 flex bison m4 gperf telnet file rsync bc unzip

required-debian-packages = $(common-required-packages) build-essential g++
optional-debian-packages = ncurses-dev device-tree-compiler u-boot-tools

required-fedora-packages = $(common-required-packages) '"Development Tools"' \
                           gcc-c++ perl-ExtUtils-MakeMaker perl-Thread-Queue
optional-fedora-packages = ncurses-devel dtc uboot-tools

required-centos-packages = $(required-fedora-packages)
optional-centos-packages = $(optional-fedora-packages)

required-redhat-packages = $(required-fedora-packages)
optional-redhat-packages = $(optional-fedora-packages)

required-ubuntu-packages = $(required-debian-packages)
optional-ubuntu-packages = $(optional-debian-packages)


######################################################################

splitted-headers =
splitted-headers += streaming-engine
splitted-headers += display
splitted-headers += grab-rend

images-hook = if [ -e $@/linux-src/arch/arm/boot/Image ]; then cp $@/linux-src/arch/arm/boot/Image $@; fi
