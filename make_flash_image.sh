#!/bin/bash -eu

if [ ${#} -lt 1 ]; then
	{
		echo "usage:"
		echo "    ${0} <output_dir> [target_image] [kernel_args]"
		echo ""
		echo "    The target image defaults to starkl-b2264-minimal, and must have been"
		echo "    built before running this script"
		echo ""
		echo "    Extra kernel arguments are optional"
		echo ""
		echo "    The Ethernet MAC address may optionally be passed in the ETHADDR"
		echo "    environemnt variable"
	} >&2
	exit 1
fi

readonly OUTDIR="${1}"
mkdir -p "${OUTDIR}"


readonly TARGET="${2:-starkl-b2264-minimal}"
readonly TARGET_DIR="$(readlink -f "./outputs/${TARGET}/images/")"

readonly KERNEL_ARGS_EXTRA="${3:-}"; shift

echo "Building flash image for ${TARGET}"

if [ ! -d "${TARGET_DIR}" ]; then
	{
		echo "ERROR: missing target directory..."
		echo "       please build starkl before running this script."
		echo ""
		echo "       ${TARGET_DIR}"
	} >&2
	exit 1
fi

# inspiration for the following procedure is taken from `./launchers/stmc2/.handle-images.bash`
# unfortunately the launcher doesn't provide a friendly way to produce an SPI flash image, so
# we do it here instead...

# prepare the PBL & uboot
readonly IMAGE_BOOT_SRC="${TARGET_DIR}/pbl_uboot.img"
readonly IMAGE_BOOT="${OUTDIR}/pbl_uboot.img"
cp -v "${IMAGE_BOOT_SRC}" "${IMAGE_BOOT}"

# prepare the ethernet address
readonly IMAGE_HWADDR="${OUTDIR}/hwaddr.txt"
echo -e "ethaddr=${ETH_ADDR:-00:00:00:00:00:00}" > "${IMAGE_HWADDR}"

# prepare the kernel
readonly IMAGE_KERNEL_SRC="${TARGET_DIR}/Image"
readonly IMAGE_KERNEL="${OUTDIR}/kernel.gz"
gzip < "${IMAGE_KERNEL_SRC}" > "${IMAGE_KERNEL}"

# prepare the device tree
readonly IMAGE_DTB_SRC="${TARGET_DIR}/sdk2-dt.dtb"
readonly IMAGE_DTB="${OUTDIR}/sdk2-dt.dtb"
cp "${IMAGE_DTB_SRC}" "${IMAGE_DTB}"
if [ ! -z "${KERNEL_ARGS_EXTRA:+z}" ]; then
	read KERNEL_ARGS < <( fdtget "${IMAGE_DTB}" /chosen bootargs )
	fdtput --type s "${IMAGE_DTB}" /chosen bootargs "${KERNEL_ARGS} ${KERNEL_ARGS_EXTRA}"
fi

# prepare the FIT image
readonly IMAGE_FIT_SRC="${OUTDIR}/image.fit.dts"
readonly IMAGE_FIT="image.fit"
cat << EOF > "${IMAGE_FIT_SRC}"
/*
 * Template to produce a simple U-Boot FIT uImage.
 * The image includes:
 *    - a single kernel image (gzip compressed)
 *    - the relevant DTB
 *    - a "config" node which tells to u-boot which images to
 *      to load and run (a FIT may contain copies of same object)
 */

/dts-v1/;

/ {
	description = "Flash FIT image";
	#address-cells = <1>;

	images {
		kernel@1 {
			description = "Starkl Linux kernel";
			data = /incbin/("${IMAGE_KERNEL##*/}");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "gzip";
			load = <0x82008000>;
			entry = <0x82008000>;
			hash@1 {
				algo = "crc32";
			};
		};
		fdt@1 {
			description = "FDT blob";
			data = /incbin/("${IMAGE_DTB##*/}");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			hash@1 {
				algo = "crc32";
			};
		};
	};

	/*
	 * Configuration is to tell to u-boot what to load and run
	 * by default (i.e. the behaviour of bootm command)
	 */
	configurations {
		default = "conf@1";
		conf@1 {
			description = "starkl boot";
			kernel = "kernel@1";
			fdt = "fdt@1";
		};
	};
};
EOF
mkimage -f "${IMAGE_FIT_SRC}" "${IMAGE_FIT}"

# preare the flash image
# the image is laid out as follows:
#
#     +----------+  0x00000000
#     |          |
#     |   pbl    |		OBSP + U-Boot
#     |          |
#     +----------+  0x000c0000
#     |   env    |		Reserved: U-Boot environemnt
#     +----------+  0x000d0000
#     |  hwaddr  |		Mac address of the ethernet interface
#     +----------+  0x00100000
#     |          |
#     |   fit    |		Kernel + DTB (see FIT format in U-Boot documentation)
#     :          :
#     |          |
#     +----------+  end of Flash
readonly IMAGE="${OUTDIR}/flash.bin"
truncate -s $(( 32 * 1024 * 1024 )) "${IMAGE}"
readonly DD_ARGS=( of="${IMAGE}" bs=4M conv=notrunc oflag=seek_bytes )
dd "${DD_ARGS[@]}" if="${IMAGE_BOOT}"
#dd "${DD_ARGS[@]}" if="${IMAGE_ENV}"    seek=$((768 * 1024))
dd "${DD_ARGS[@]}" if="${IMAGE_HWADDR}" seek=$((832 * 1024))
dd "${DD_ARGS[@]}" if="${IMAGE_FIT}"    seek=$((1 * 1024 * 1024))

if [ $(stat -c'%s' "${IMAGE}") -gt $(( 32 * 1024 * 1024 )) ]; then
	{
		echo "WARNING: the generated flash image is larger than 32 MiB..."
		echo "         it may not fit on the SPI flash"
	} >&2
fi

# finally, compress the flash image
gzip < "${IMAGE}" > "${IMAGE}.gz"

echo "Done!"
