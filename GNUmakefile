# Starkl: Starter Kit Linux, embedded Linux made easy on ST boards.
# Copyright (C) STMicroelectronics 2016-2017
# Author: Cédric Vincent <cedric.vincent@st.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##################################################

.DELETE_ON_ERROR:
.SECONDEXPANSION:

	# checks for licensed tarballs being present
LICENSED_TARBALLS=\
    ./tarballs/analog-firmware-*.tar.gz \
    ./tarballs/audio-apiheaders-*.tar.gz \
    ./tarballs/audio-firmware-*.tar.gz \
    ./tarballs/fdma-firmware-*.tar.gz \
    ./tarballs/hades-firmware-hades*.tar.gz \
    ./tarballs/lpm-firmware-*.tar.gz \
    ./tarballs/mali400-bin-*.tar.gz \
    ./tarballs/obsp-*.tar.gz \
    ./tarballs/pti-firmware-*.tar.gz \
    ./tarballs/stmfapp-*.tar.gz \
    ./tarballs/targetpack-*.tar.gz \
    ./tarballs/video-apiheaders-hades*.tar.gz \
    ./tarballs/video-firmware-hades*.tar.gz

ifneq ($(words $(LICENSED_TARBALLS)),$(words $(wildcard $(LICENSED_TARBALLS))))
$(error licensed tarballs are missing, please download them from the website.)
endif

images      = $(sort $(patsubst configs/%.config,outputs/%/images,$(wildcard configs/*.config)))
image-names = $(sort $(patsubst outputs/%/images,%,$(images)))

define check-config-name-template
    $(foreach name,$(image-names),   \
      $(if $(findstring $1,$(name)), \
        $(error character "$1" is not supported in config filename, please rename configs/$(name).config)))
endef

$(eval $(call check-config-name-template,:))
$(eval $(call check-config-name-template,+))


##################################################

possible-goals := $(images) $(patsubst outputs/%/images,outputs/%,$(images))
possible-goals += $(patsubst %,%/,$(possible-goals))

br2-cmd-separators = + /+

# # "+" is enough, no need to handle "/+" yet.
strip-br2-cmd = $(word 1,$(subst +,$(space),$1))
get-br2-cmd   = $(word 2,$(subst +,$(space),$1))

requested-goals = $(filter $(possible-goals),$(foreach goal,$(MAKECMDGOALS),$(call strip-br2-cmd,$(goal))))


##################################################

empty =
space = $(empty) $(empty)

define clean-from-cwd
    cleaned-$1 = $(subst $(space),:,$(filter-out .,$(subst :,$(space),$($1))))

    ifneq ($$(cleaned-$1),$($1))
        $$(info The current working directory has been removed from $1)
        $$(info Old $1 = $$($1))
        $$(info New $1 = $$(cleaned-$1))
        export $1 := $$(cleaned-$1)
    endif
endef

$(eval $(call clean-from-cwd,PATH))
$(eval $(call clean-from-cwd,LD_LIBRARY_PATH))


##################################################

.PHONY: help
help:
	@echo ''
	@echo 'You may want to run:'
	@echo ''
	@echo '    make menuconfig'
	@echo '    make outputs/starkl-custom/images'
	@echo ''
	@echo 'or any of:'
	@echo ''
	@for image in $(images); do echo "    make $$image"; done
	@echo ''
	@echo 'See section "Development" in manual.pdf for more details.'
	@echo ''

config-custom-destination = configs/starkl-custom.config
config-custom-source      = menuconfig/.config

CONFIG = config mconfig nconfig qconfig gconfig menuconfig xconfig
.PHONY: $(CONFIG)
$(CONFIG):
	$(MAKE) --directory menuconfig run-$@

	@if    [ -e $(config-custom-source) ]                                          \
           && (   [ ! -e $(config-custom-destination) ]                                \
               || [ $(config-custom-destination) -ot $(config-custom-source) ]); then  \
	    cp --verbose $(config-custom-source) $(config-custom-destination);         \
	    echo '';                                                                   \
	    echo -n 'Now, ';                                                           \
	else                                                                           \
	    echo '';                                                                   \
	    echo 'You saved your custom configuration to a location which is not the'; \
	    echo 'default one. Thus, do not forget to:';                               \
	    echo '';                                                                   \
	    echo '    cp <path-to-your-custom-config> $(config-custom-destination)';   \
	    echo '';                                                                   \
	    echo -n 'Then, ';                                                          \
	fi

	@echo 'you can:'
	@echo ''
	@echo '    1. optionally fine-tune the generated Buildroot configuration:'
	@echo ''
	@echo '           make outputs/starkl-custom+menuconfig'
	@echo '           make outputs/starkl-custom+linux-menuconfig'
	@echo ''
	@echo '    2. build it:'
	@echo ''
	@echo '           make outputs/starkl-custom/images'
	@echo ''

.PHONY: clean
clean:
	rm --force --recursive outputs/starkl-*


##################################################

misc/known-packages.mk:
	echo -n 'known-packages = ' > $@.tmp
	find buildroot br2-external $< -regex '.*/\(.*\)/\1.mk' -printf '%f ' | sed 's/\.mk / /g' >> $@.tmp
	mv $@.tmp $@


##################################################

-include misc/config.mk
-include misc/known-packages.mk

list-required-foo-packages = $(patsubst %,list-required-%-packages,$(distros))
.PHONY: $(list-required-foo-packages)
$(list-required-foo-packages):
	@echo $($(patsubst list-%,%,$@))

.PHONY: list-supported-images
list-supported-images:
	@for image in $(images); do echo $$image; done

.PHONY: configure-all-images
configure-all-images: $$(patsubst %/images,%/buildroot/.config,$(images))

.PHONY: build-all-images
build-all-images: $(images)


##################################################

ifneq (,$(findstring custom-many,$(.VARIABLES)))
$(foreach entry,$(wildcard $(custom-many)/*),$(eval custom-$(notdir $(entry))=$(entry)))
endif

define reset-custom-packages
custom-packages := $$(patsubst custom-%,%, \
	$$(filter-out custom-packages,     \
	$$(filter-out custom-many,         \
	$$(filter custom-%,$$(.VARIABLES)))))
endef

$(eval $(call reset-custom-packages))
$(foreach package,$(filter $(splitted-headers),$(custom-packages)),$(eval custom-$(package)-headers=$(custom-$(package))))
$(eval $(call reset-custom-packages))

# List extracted from Buildroot 2018.2.1+
targets-without-packages := menuconfig nconfig gconfig xconfig config	\
    oldconfig randconfig defconfig %_defconfig allyesconfig		\
    allnoconfig alldefconfig silentoldconfig release			\
    randpackageconfig allyespackageconfig allnopackageconfig		\
    print-version olddefconfig distclean manual manual-%

command-dont-build = $(filter $(targets-without-packages) help, \
                       $(foreach target,$(MAKECMDGOALS),$(call get-br2-cmd,$(target))))

# All the targets used by "$(rebuild-override-srcdir-template)" are
# not available when the top-level Buildroot target belongs to the set
# "$(targets-without-packages)".
ifeq ($(command-dont-build),)
    uppercase = $(shell echo $1 | tr a-z- A-Z_)

    define rebuild-template
        ifneq ($(subst all,$1,$(dont-dirclean-rdepends)),$1)
            dirclean-rdepends += $1-dirclean-rdepends
        endif
        clean-for-rebuild += $1-clean-for-rebuild
        rebuild           += $1-rebuild
    endef

    define rebuild-override-srcdir-template
        $(if $(filter $1,$(known-packages) $(addprefix host-,$(known-packages))),, \
              $(info It seems you made a typo on "$1"...)
              $(error package "$1" not known))
        $(call rebuild-template,$1)
        override-srcdir += $(call uppercase,$1)_OVERRIDE_SRCDIR=$(abspath $(custom-$1))
        ifdef patch-$1
            override-patch += $(call uppercase,$1)_OVERRIDE_SRCDIR_AND_PATCH=Y
        endif
    endef
endif

get-buildroot-dirs-from-goal = $(patsubst %,%/buildroot, \
                               $(patsubst %/images,%,    \
                               $(patsubst %/,%,$1)))

removed-custom-packages = $(sort                                                         \
                            $(filter-out $(custom-packages),                             \
                              $(foreach buildroot-dir,                                   \
                                $(call get-buildroot-dirs-from-goal,$(requested-goals)), \
                                $(shell cat $(buildroot-dir)/custom-packages 2>/dev/null))))

$(foreach custom-package,$(custom-packages),$(eval $(call rebuild-override-srcdir-template,$(custom-package))))
$(foreach removed-custom-package,$(removed-custom-packages),$(eval $(call rebuild-template,$(removed-custom-package))))

$(foreach package,$(custom-packages),$(info Using custom sources for "$(package)": $(custom-$(package))))
$(foreach package,$(removed-custom-packages),$(info Not using custom sources for "$(package)" anymore))

newest-vmlinux = stat --format='%Z %n' $1/../buildroot/build/linux-*/vmlinux \
                 | sort --numeric-sort                                       \
                 | tail -1                                                   \
                 | cut --fields=2 --delimiter=' '                            \
                 | xargs --no-run-if-empty readlink --canonicalize-existing


##################################################

define check-no-parallel-jobs
$(if $(findstring -j,$(MFLAGS)),$(error top-level parallel make is currently not supported by Buildroot, use the BR2_JLEVEL= variable instead.  Remember the build of each individual package is already done in parallel by default))
endef

define build-from-buildroot
	$(call check-no-parallel-jobs)
	$(if $(command-dont-build),,echo '$(custom-packages)' > $1/custom-packages)
	$(MAKE) --directory $1 $(override-srcdir) $(override-patch) $(dirclean-rdepends) $(clean-for-rebuild) $2
endef


##################################################

no-image+br2-cmd = $(foreach name,$(image-names),                       \
                     $(foreach br2-cmd-separator,$(br2-cmd-separators), \
                       outputs/$(name)$(br2-cmd-separator)%))

image+br2-cmd = $(foreach image,$(images),                           \
                  $(foreach br2-cmd-separator,$(br2-cmd-separators), \
                    $(image)$(br2-cmd-separator)%))

# Sadly .PHONY doesn't work on % targets :-/

$(no-image+br2-cmd): $$(call strip-br2-cmd,$$@)/buildroot/.config | outputs
	$(call build-from-buildroot,$(call strip-br2-cmd,$@)/buildroot,$(subst +,$(space),$*))

$(image+br2-cmd): $$(subst /images,,$$@)
	$(MAKE) $(call strip-br2-cmd,$@)

outputs/%+:
	@echo 'Buildroot command is missing, maybe you meant:'
	@echo ''
	@echo '   $(MAKE) $@menuconfig'
	@echo ''
	@echo 'To get the list of available Buildroot commands:'
	@echo ''
	@echo '   $(MAKE) $@help'
	@echo ''
	@false


##################################################

all-images := $(images)

ifneq ($(MAKE_VERSION),3.81)
all-images += $(patsubst %,%/,$(images))
endif

.PHONY: $(all-images)
$(all-images): $$(patsubst %/images,%/buildroot/.config,$$@) | outputs
	mkdir --parents $@
	$(call build-from-buildroot,$@/../buildroot,$(rebuild) all)
	$(call newest-vmlinux,$@) \
	  | xargs --no-run-if-empty --replace=foo ln --verbose --symbolic --force --no-dereference foo $@/
	$(call newest-vmlinux,$@)           \
	  | xargs --no-run-if-empty dirname \
	  | xargs --no-run-if-empty --replace=foo ln --verbose --symbolic --force --no-dereference foo $@/linux-src
	find $@/../buildroot/images -mindepth 1 -maxdepth 1 \
	  | xargs --no-run-if-empty --replace=foo cp --archive --verbose foo $@
	cp $< $@/buildroot.config
	$(images-hook)
	$(call printf-how-to-launch,$@)
	$(call printf-how-to-flash,$@)
	@echo
	@echo ==================================================================================================================================================================================
	@echo The software which has been built is bound by a number of licenses.
	@echo Please consult the LICENSE and/or COPYING file of each package for details. I acknowledge that by using any of the software I am agreeing to the terms of the applicable licenses.
	@echo ==================================================================================================================================================================================
	@echo
outputs:
	mkdir $@


##################################################

buildroot-defconfigs = $(patsubst %,outputs/%/buildroot/.defconfig,$(image-names))
linux-defconfigs     = $(patsubst %,outputs/%/buildroot/linux-defconfig,$(image-names))
image-configs        = $(patsubst %,outputs/%/config,$(image-names))

get-starkl-config  = $(abspath $(patsubst outputs/%/buildroot/,configs/%.config,$(dir $1)))
get-starkl-config2 = $(abspath $(patsubst outputs/%/,configs/%.config,$(dir $1)))

$(buildroot-defconfigs): $$(call get-starkl-config,$$@) menuconfig/br2-defconfig.m4
	$(MAKE) --directory menuconfig $(abspath $@) BR2_EXTERNAL_STARKL_PATH=$(abspath br2-external) CONFIG=$<

$(linux-defconfigs): $$(call get-starkl-config,$$@) menuconfig/linux-defconfig.m4
	$(MAKE) --directory menuconfig $(abspath $@) BR2_EXTERNAL_STARKL_PATH=$(abspath br2-external) CONFIG=$<

$(image-configs): $$(call get-starkl-config2,$$@)
	$(MAKE) --directory menuconfig $(abspath $@) BR2_EXTERNAL_STARKL_PATH=$(abspath br2-external) CONFIG=$<

.PHONY: update-configs
update-configs: $(image-configs)
	$(foreach image,$(image-names), cp outputs/$(image)/config configs/$(image).config;)

outputs/%/buildroot/.config: outputs/$$*/buildroot/.defconfig outputs/$$*/buildroot/linux-defconfig
	$(call check-no-parallel-jobs)
	$(MAKE) --directory buildroot BR2_EXTERNAL=$(abspath br2-external) BR2_DEFCONFIG=$(abspath $<) O=$(abspath outputs/$*/buildroot) defconfig
