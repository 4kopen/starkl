#
# Handle Starkl options passed through /proc/cmdline.
#

cmdline=''
ip='ip'
udhcpc='udhcpc'

case "$1" in
  start)
	cmdline=$(cat /proc/cmdline)
	;;

  stop)
	for interface in $interfaces; do
		echo "Stopping DHCP client on $interface"
		[ -e "/tmp/udhcpc.$interface" ] && kill "$(cat "/tmp/udhcpc.$interface")"
	done
	exit 0
	;;

  restart|reload)
	"$0" stop
	"$0" start
	;;

  test)
	cmdline=$*
	ip='echo ip'
	udhcpc='true'
	;;

  *)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac

# My sed-fu isn't as powerful as my shell-fu :-(
options=$(echo "$cmdline" | xargs -n 1 echo | grep starkl | cut -f 2- -d = | xargs echo | sed 's/,/ /g')

for interface in $interfaces; do
	$ip link set "$interface" down
done

for option in $options; do
	case $option in
	  gw:*)
		echo "ERROR, Starkl boot option '$option' is deprecated.  Instead, use:"
		echo '	ip:<interface>:<ip-address>/<prefix-length>:<gateway-address>'
		;;

	  mac:*)
		echo "ERROR, Starkl boot option '$option' is deprecated.  Instead, use any of:"
		echo '	ip:<interface>:<ip-address>/<prefix-length>:<gateway-address>:<mac-address>'
		echo '	ip:<interface>:dhcp:<mac-address>'
		;;

	  ip:*:ignore)
		interface=$(   echo "$option" | cut -d : -f 2  )
		interfaces=$(echo $interfaces | sed s/$interface//)
		;;

	  ip:*:dhcp)
		interface=$(   echo "$option" | cut -d : -f 2  )
		echo "$interfaces" | grep -vqw "$interface" && interfaces="$interfaces $interface"
		;;

	  ip:*:dhcp:*) # Format: ip:<interface>:dhcp:<mac-address>
		interface=$(   echo "$option" | cut -d : -f 2  )
		mac_address=$( echo "$option" | cut -d : -f 4- )

		[ "$interface" = '' ]    && echo 'ERROR, no interface specified.'   && continue
		[ "$mac_address" = '' ]  && echo 'ERROR, no MAC address specified.' && continue

		$ip link set "$interface" addr "$mac_address"
		echo "$interfaces" | grep -vqw "$interface" && interfaces="$interfaces $interface"
		;;

	  ip:*) # Format: ip:<interface>:<ip-address>/<prefix-length>[:<gateway-address>[:<mac-address>]]
		interface=$(     echo "$option" | cut -d : -f 2  )
		ip_address=$(    echo "$option" | cut -d : -f 3 | cut -d / -f 1 )
		prefix_length=$( echo "$option" | cut -d : -f 3 | cut -d / -f 2 )
		gateway=$(       echo "$option" | cut -d : -f 4  )
		mac_address=$(   echo "$option" | cut -d : -f 5- )

		[ "$interface"     = '' ] && echo 'ERROR, no interface specified.'             && continue
		[ "$ip_address"    = '' ] && echo 'ERROR, no IP address specified.'            && continue
		[ "$prefix_length" = '' ] && echo 'ERROR, no network prefix length specified.' && continue

		$ip link set dev "$interface" down
		$ip addr add "$ip_address"/"$prefix_length" dev "$interface"
		[ "$mac_address" != '' ] && $ip link set "$interface" addr "$mac_address"
		$ip link set dev "$interface" up
		[ "$gateway" != '' ] && $ip route add default via "$gateway"
		;;

	  *)
		echo "WARNING, unknown Starkl network boot option: $option"
		;;

	esac
done

for interface in $interfaces; do
	if ! $ip link show "$interface" | grep -wq UP; then
		echo "Starting DHCP client on $interface"
		$udhcpc --background --release --pidfile "/tmp/udhcpc.$interface" --interface "$interface" 1>/dev/null 2>&1 &
	fi
done

exit 0
