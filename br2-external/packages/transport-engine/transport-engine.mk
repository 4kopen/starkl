################################################################################
#
# transport-engine
#
################################################################################

TRANSPORT_ENGINE_VERSION = 115.32.2
TRANSPORT_ENGINE_SOURCE  = transport-engine-$(TRANSPORT_ENGINE_VERSION).tar.gz
TRANSPORT_ENGINE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
TRANSPORT_ENGINE_LICENSE = GPL-2.0

TRANSPORT_ENGINE_DEPENDENCIES += infrastructure
TRANSPORT_ENGINE_DEPENDENCIES += multicom
TRANSPORT_ENGINE_DEPENDENCIES += strelay
TRANSPORT_ENGINE_DEPENDENCIES += streaming-engine-headers
TRANSPORT_ENGINE_DEPENDENCIES += display-headers


TRANSPORT_ENGINE_MODULE_SUBDIRS = linux/

TRANSPORT_ENGINE_MODULE_MAKE_OPTS = \
      CONFIG_INFRASTRUCTURE=$(INFRASTRUCTURE_BUILDDIR) \
      CONFIG_MULTICOM_PATH=$(MULTICOM_BUILDDIR) \
      CONFIG_PLAYER2_PATH=$(STREAMING_ENGINE_HEADERS_BUILDDIR) \
      CONFIG_STRELAY_PATH=$(STRELAY_BUILDDIR) \
      CONFIG_DISPLAY_PATH=$(DISPLAY_HEADERS_BUILDDIR)


$(eval $(kernel-module))
$(eval $(generic-package))
