################################################################################
#
# FDMA firmware
#
################################################################################

FDMA_FIRMWARE_VERSION = $(call qstrip,$(BR2_PACKAGE_FDMA_FIRMWARE_VERSION))
FDMA_FIRMWARE_SOURCE  = fdma-firmware-$(FDMA_FIRMWARE_VERSION).tar.gz
FDMA_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
FDMA_FIRMWARE_LICENSE = ST Proprietary

define FDMA_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware/
	cd $(@D); cp $(call qstrip,$(BR2_PACKAGE_FDMA_FIRMWARE_FILES)) $(TARGET_DIR)/lib/firmware/
endef

$(eval $(generic-package))
