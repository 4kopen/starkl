################################################################################
#
# Open Boot Support Package
#
################################################################################

OBSP_VERSION = $(call qstrip,$(BR2_PACKAGE_OBSP_VERSION))
OBSP_SOURCE  = obsp-$(OBSP_VERSION).tar.gz
OBSP_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
OBSP_LICENSE = GPL-2.0, ST Proprietary

HOST_OBSP_DEPENDENCIES += host-libxml2 host-zlib host-openssl
OBSP_DEPENDENCIES      += host-obsp targetpack host-romgen zlib uboot

ifeq ($(BR2_PACKAGE_OBSP_SERIAL),y)
OBSP_MAKE_OPTS += PBL_WITH_SERIAL=1
endif

OBSP_PBL_BOARD_NAME      = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_BOARD_NAME))
OBSP_PBL_SOC_NAME        = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_SOC_NAME))
OBSP_PBL_PROFILE_NAME    = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_PROFILE_NAME))
OBSP_PBL_ROMGEN_SOC      = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_ROMGEN_SOC))
OBSP_PBL_BINARY_FILENAME = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_BINARY_FILENAME))
OBSP_HOM_TAG_PHY_ADD     = $(call qstrip,$(BR2_PACKAGE_OBSP_HOM_TAG_PHYSICAL_ADDRESS))
OBSP_PBL_DEFS            = $(call qstrip,$(BR2_PACKAGE_OBSP_PBL_DEFS))

OBSP_TARGET_STRING_PARAMS= active_cores=$(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_CPU)),$(subst $(space),$(comma),$(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_OPTIONS)))

ifeq ($(BR2_PACKAGE_OBSP_PBL_HOM_RESUME_WITHOUT_CPS_CHECK),y)
OBPS_PBL_HOM_RESUME_WITHOUT_CPS_CHECK=1
else
OBPS_PBL_HOM_RESUME_WITHOUT_CPS_CHECK=0
endif

ifeq ($(BR2_PACKAGE_OBSP_DEBUG),y)
OBSP_MAKE_OPTS += DEBUG=1
OBSP_PBL_BINARY_FILENAME := $(OBSP_PBL_BINARY_FILENAME)_debug
endif

ifeq ($(BR2_PACKAGE_OBSP_BCH_DRIVER),y)
OBSP_NAND_DRIVER := PBL_NAND_DRIVER=bch
endif

ifeq ($(BR2_PACKAGE_OBSP_AFM_DRIVER),y)
OBSP_NAND_DRIVER := PBL_NAND_DRIVER=afm
endif

ifeq ($(BR2_PACKAGE_OBSP_BOOT_NAND),y)
OBSP_PBL_FLASH_OPTION := PBL_BOOT_FROM_NAND=1 PBL_NAND_SUPPORT=1 $(OBSP_NAND_DRIVER)
PBLMKIMAGE_MEDIA := 1
endif
ifeq ($(BR2_PACKAGE_OBSP_BOOT_EMMC),y)
OBSP_PBL_FLASH_OPTION := PBL_BOOT_FROM_MMC=1 PBL_MMC_SUPPORT=1
PBLMKIMAGE_MEDIA := 4
PBLMKIMAGE_BOOTBOUNDARY := --bootboundary before --bootboundary-align=4096
endif
ifeq ($(BR2_PACKAGE_OBSP_BOOT_SPI),y)
PBLMKIMAGE_MEDIA := 2
endif

define HOST_OBSP_BUILD_CMDS
	$(MAKE1) -C $(@D) host_tools V=1               \
	    HOST_OS=linux                              \
	    DESTDIR=$(HOST_DIR)/usr                    \
	    DESTDIR_ROOT=$(HOST_DIR)/usr               \
	    XML2LIBDIR=$(HOST_DIR)/usr/lib             \
	    XML2INCDIR=$(HOST_DIR)/usr/include/libxml2 \
	    OPENSSLDIR=$(HOST_DIR)/usr                 \
	    ZLIBDIR=$(HOST_DIR)/usr
endef

define OBSP_BUILD_CMDS
	$(MAKE1) -C $(@D) pbl V=1                         \
	    $(OBSP_MAKE_OPTS)                             \
	    AR=$(TARGET_AR)                               \
	    ARCH=armv7                                    \
	    BFR_DATA=$(BINARIES_DIR)/btfmw_params.bin     \
	    BFR_FW=$(BINARIES_DIR)/libbtfmw.a             \
	    BOARD=$(OBSP_PBL_BOARD_NAME)                  \
	    BUILDDIR=$(@D)/build                          \
	    CC=$(TARGET_CC)                               \
	    CONFIG_HOM_TAG_PHYSICAL_ADDRESS=$(OBSP_HOM_TAG_PHY_ADD) \
	    CPP=$(TARGET_CPP)                             \
	    DESTDIR=$(@D)/build                           \
	    OBJCOPY=$(TARGET_OBJCOPY)                     \
	    PBL_DEFS=$(BR2_PACKAGE_OBSP_PBL_DEFS)         \
	    PBL_HOLDING_PEN_IN_SBC_DMEM=1                 \
	    PBL_HOM_RESUME_WITHOUT_CPS_CHECK=$(OBPS_PBL_HOM_RESUME_WITHOUT_CPS_CHECK)\
	    PBL_LINUX_HOM_RESUME=2                        \
	    PBL_NBS=1                                     \
	    PBL_RUN_FROM_RAM=1                            \
	    PBL_WITH_ZLIB=1                               \
	    PROFILE=$(OBSP_PBL_PROFILE_NAME)              \
	    ROMGEN="cd $(BINARIES_DIR); env PROOT_OPTIONS='-b $(@D)' $(HOST_DIR)/usr/bin/romgen" \
	    ROMGEN_SOC=$(OBSP_PBL_ROMGEN_SOC)             \
	    SOC=$(OBSP_PBL_SOC_NAME)                      \
	    STTP_XML_PATH=$(BINARIES_DIR)                 \
	    TARGET_OS=bare                                \
	    TARGET_STRING_PARAMS=$(OBSP_TARGET_STRING_PARAMS) \
	    $(OBSP_PBL_FLASH_OPTION)

	$(HOST_DIR)/usr/bin/pblmkimage -v --media $(PBLMKIMAGE_MEDIA) --type pbl \
	    $(@D)/pbl_uboot.img                                \
	    $(@D)/build/target_bare/armv7/boot/$(OBSP_PBL_BINARY_FILENAME)

	$(TARGET_STRIP) -s -R .notes -R .comment -o $(@D)/u-boot.elf $(UBOOT_BUILDDIR)/u-boot

	$(HOST_DIR)/usr/bin/pblelftool -v --compress $(@D)/u-boot.elf $(@D)/u-boot.z.elf

	$(HOST_DIR)/usr/bin/pblmkimage -v --media $(PBLMKIMAGE_MEDIA) --type armlinux           \
	    --boot --align 0x1000 $(PBLMKIMAGE_BOOTBOUNDARY) --size $$(stat -L -c %s $(@D)/u-boot.z.elf) \
	    $(@D)/pbl_uboot.img                                               \
	    $(@D)/u-boot.z.elf

	$(HOST_DIR)/usr/bin/pblshowindex $(@D)/pbl_uboot.img
endef

define OBSP_INSTALL_TARGET_CMDS
	cp $(@D)/pbl_uboot.img $(BINARIES_DIR)
endef

$(eval $(generic-package))
$(eval $(host-generic-package))
