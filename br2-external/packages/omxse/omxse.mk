################################################################################
#
# omxse
#
################################################################################

OMXSE_VERSION = 0.14.03
OMXSE_SOURCE  = omxse-$(OMXSE_VERSION).tar.gz
OMXSE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
OMXSE_LICENSE = GPL-2.0

OMXSE_DEPENDENCIES += stlinuxtv
OMXSE_DEPENDENCIES += infrastructure
OMXSE_DEPENDENCIES += streaming-engine
OMXSE_DEPENDENCIES += display
OMXSE_DEPENDENCIES += blitter

OMXSE_MODULE_MAKE_OPTS += TREE_ROOT=$(@D)
OMXSE_MODULE_MAKE_OPTS += CONFIG_INFRA_PATH=$(INFRASTRUCTURE_BUILDDIR)
OMXSE_MODULE_MAKE_OPTS += CONFIG_PLAYER2_PATH=$(STREAMING_ENGINE_BUILDDIR)
OMXSE_MODULE_MAKE_OPTS += CONFIG_STGFB_PATH=$(DISPLAY_BUILDDIR)
OMXSE_MODULE_MAKE_OPTS += CONFIG_BLITTER_PATH=$(BLITTER_BUILDDIR)

define OMXSE_INSTALL_TARGET_CMDS
	$(INSTALL) -d   $(TARGET_DIR)/etc/modules.d/
	echo 'stmomx' > $(TARGET_DIR)/etc/modules.d/040-stmomx
endef

$(eval $(kernel-module))
$(eval $(generic-package))
