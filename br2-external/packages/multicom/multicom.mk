################################################################################
#
# multicom
#
################################################################################

MULTICOM_VERSION = 4.6.18
MULTICOM_SOURCE  = multicom-$(MULTICOM_VERSION).tar.gz
MULTICOM_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
MULTICOM_LICENSE = GPL-2.0 or ST Proprietary

MULTICOM_MODULE_SUBDIRS = source/
MULTICOM_MODULE_MAKE_OPTS += CONFIG_MACH_STM_STIH418=y
MULTICOM_MODULE_MAKE_OPTS += MULTICOM_EXTRA_CFLAGS=-D__LITTLE_ENDIAN__
# copied  MULTICOM_EXTRA_CFLAGS=-D__LITTLE_ENDIAN__ from SDK2 makefile
# line preceded by comment: "Temporary workaround until next release of Multicom"

ifeq ($(BR2_PACKAGE_MULTICOM_ICS_DEBUG),y)
MULTICOM_MODULE_MAKE_OPTS += DEBUG_CFLAGS="-DICS_DEBUG -DICS_DEBUG_FLAGS=$(call qstrip,$(BR2_PACKAGE_MULTICOM_ICS_DEBUG_FLAGS))"
endif
ifeq ($(BR2_PACKAGE_MULTICOM_MME_DEBUG),y)
MULTICOM_MODULE_MAKE_OPTS += DEBUG_CFLAGS="-DMME_DEBUG -DMME_DEBUG_FLAGS=$(call qstrip,$(BR2_PACKAGE_MULTICOM_MME_DEBUG_FLAGS))"
endif

ifeq ($(BR2_PACKAGE_MULTICOM_USERLIB),y)
define MULTICOM_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/source \
		OBJDIR=$(@D)/obj LIBDIR=$(@D)/lib \
		CROSS_COMPILE=$(TARGET_CROSS) \
		-f Makefile.linux
endef

define MULTICOM_USERLIB_INSTALL_TARGET_CMDS
	$(INSTALL) -d $(TARGET_DIR)/usr/lib/
	$(INSTALL) -t $(TARGET_DIR)/usr/lib/ -m 0644 $(@D)/lib/lib*.a
	$(INSTALL) -t $(TARGET_DIR)/usr/lib/ -m 0755 $(@D)/lib/lib*.so

endef
endif

define MULTICOM_INSTALL_TARGET_CMDS
	$(MULTICOM_USERLIB_INSTALL_TARGET_CMDS)
	$(INSTALL) -d $(TARGET_DIR)/etc/modprobe.d/
	$(INSTALL) -t $(TARGET_DIR)/etc/modprobe.d/ -m 0644 $(BR2_EXTERNAL_STARKL_PATH)/packages/multicom/multicom4.conf
endef

$(eval $(kernel-module))
$(eval $(generic-package))
