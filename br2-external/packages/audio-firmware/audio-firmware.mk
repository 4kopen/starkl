################################################################################
#
# audio-firmware
#
################################################################################

AUDIO_FIRMWARE_VERSION = 38.3.0.24-4
AUDIO_FIRMWARE_SOURCE  = audio-firmware-$(AUDIO_FIRMWARE_VERSION).tar.gz
AUDIO_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
AUDIO_FIRMWARE_LICENSE = ST Proprietary

AUDIO_FIRMWARE_TARGETFILENAME = audio_firmware-stih418

define AUDIO_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware
	cp $(@D)/$(BR2_PACKAGE_AUDIO_FIRMWARE_FILENAME0) $(TARGET_DIR)/lib/firmware/$(AUDIO_FIRMWARE_TARGETFILENAME).0.elf
	cp $(@D)/$(BR2_PACKAGE_AUDIO_FIRMWARE_FILENAME1) $(TARGET_DIR)/lib/firmware/$(AUDIO_FIRMWARE_TARGETFILENAME).1.elf
endef

$(eval $(generic-package))
