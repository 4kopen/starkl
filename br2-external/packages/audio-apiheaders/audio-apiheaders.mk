################################################################################
#
# audio-apiheaders
#
################################################################################

AUDIO_APIHEADERS_VERSION = 38.3.0.20-2
AUDIO_APIHEADERS_SOURCE  = audio-apiheaders-$(AUDIO_APIHEADERS_VERSION).tar.gz
AUDIO_APIHEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
AUDIO_APIHEADERS_LICENSE = ST Proprietary

define AUDIO_APIHEADERS_INSTALL_TARGET_CMDS
      true
endef

$(eval $(generic-package))
