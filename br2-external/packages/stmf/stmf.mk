################################################################################
#
# stmf
#
################################################################################

STMF_VERSION = 2.40.12.6
STMF_SOURCE  = stmf-$(STMF_VERSION).tar.gz
STMF_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STMF_LICENSE = LGPL-2.1

STMF_AUTORECONF = YES
STMF_INSTALL_STAGING = YES

STMF_DEPENDENCIES += zvbi
STMF_DEPENDENCIES += libxml2
STMF_DEPENDENCIES += alsa-lib
STMF_DEPENDENCIES += stmedia-ctl
STMF_DEPENDENCIES += gstreamer1

STMF_MAKE_ENV += DESTDIR=$(TARGET_DIR)

$(eval $(autotools-package))
