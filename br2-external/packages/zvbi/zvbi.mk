################################################################################
#
# zvbi
#
################################################################################

ZVBI_VERSION = 0.2.35
ZVBI_SOURCE  = zvbi-$(ZVBI_VERSION).tar.bz2
ZVBI_SITE    = http://mbtwww.gnb.st.com:8080/~toromano/starkl/tarballs
ZVBI_LICENSE = GPL-2.0+

ZVBI_DEPENDENCIES += libpng
ZVBI_AUTORECONF = YES

ZVBI_CONF_OPTS = --without-x

ZVBI_INSTALL_STAGING = YES

$(eval $(autotools-package))
