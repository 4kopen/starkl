################################################################################
#
# stgfx2
#
################################################################################

STGFX2_VERSION = 5.85.34
STGFX2_SOURCE  = stgfx2-$(STGFX2_VERSION).tar.gz
STGFX2_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STGFX2_LICENSE = GPL-2.0

STGFX2_INSTALL_STAGING = YES
STGFX2_AUTORECONF = YES
STGFX2_AUTORECONF_OPTS = --verbose --install --force

STGFX2_DEPENDENCIES += display
STGFX2_DEPENDENCIES += directfb
STGFX2_DEPENDENCIES += blitter

STGFX2_MAKE_ENV += SYSROOT_DIR=$(STAGING_DIR)

$(eval $(autotools-package))
