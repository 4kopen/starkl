################################################################################
#
# analog-firmware
#
################################################################################

ANALOG_FIRMWARE_VERSION = 1.9.0
ANALOG_FIRMWARE_SOURCE  = analog-firmware-$(ANALOG_FIRMWARE_VERSION).tar.gz
ANALOG_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
ANALOG_FIRMWARE_LICENSE = ST Proprietary

ANALOG_FIRMWARE_FILES-y = denc_H407_b2120.fw hdf_H407_b2120.fw

define ANALOG_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware/
	cp $(addprefix $(@D)/,$(ANALOG_FIRMWARE_FILES-y)) $(TARGET_DIR)/lib/firmware/
endef

$(eval $(generic-package))
