################################################################################
#
# openmax-headers
#
################################################################################

OPENMAX_HEADERS_VERSION = 0.9.01
OPENMAX_HEADERS_SOURCE  = openmax-headers-$(OPENMAX_HEADERS_VERSION).tar.gz
OPENMAX_HEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
OPENMAX_HEADERS_LICENSE = Apache-2.0


define OPENMAX_HEADERS_INSTALL_TARGET_CMDS
	true
endef

$(eval $(generic-package))
