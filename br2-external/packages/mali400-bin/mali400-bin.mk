################################################################################
#
# mali400-bin
#
################################################################################

MALI400_BIN_VERSION = 0.3
MALI400_BIN_SOURCE  = mali400-bin-$(MALI400_BIN_VERSION).tar.gz
MALI400_BIN_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
MALI400_BIN_LICENSE = Proprietary
MALI400_BIN_INSTALL_STAGING = YES

MALI400_BIN_DEPENDENCIES += mesa3d-headers

MALI400_BIN_PROVIDES = libegl libgles


MALI400_BIN_DEST_PATH=$(TARGET_DIR)/usr/lib/mali
MALI400_BIN_STAGING_PATH=$(STAGING_DIR)/usr/lib/mali

ifneq ($(BR2_PACKAGE_MALI400_BIN_DIRECTFB),)
define MALI400_BIN_INSTALL_DIRECTFB
	@cp -Rap $(@D)/directfb $(MALI400_BIN_DEST_PATH)
endef
define MALI400_BIN_STAGING_DIRECTFB
	@cp -Rap $(@D)/directfb $(MALI400_BIN_STAGING_PATH)
endef
endif

ifneq ($(BR2_PACKAGE_MALI400_BIN_STMFB),)
define MALI400_BIN_INSTALL_STMFB
	@cp -Rap $(@D)/fbdev $(MALI400_BIN_DEST_PATH)
	@cp -Rap $(@D)/stmfb $(MALI400_BIN_DEST_PATH)
endef
define MALI400_BIN_STAGING_STMFB
	@cp -Rap $(@D)/fbdev $(MALI400_BIN_STAGING_PATH)
	@cp -Rap $(@D)/stmfb $(MALI400_BIN_STAGING_PATH)
endef
endif

ifeq ($(BR2_PACKAGE_MALI400_BIN_DIRECTFB_DEFAULT),y)
define MALI400_BIN_INSTALL_DIRECTFB_DEFAULT
	for f in $(@D)/fbdev/release/* ; do ln -fs mali/directfb/release/`basename $$f` $(TARGET_DIR)/usr/lib/ ; done
endef
define MALI400_BIN_STAGING_DIRECTFB_DEFAULT
	for f in $(@D)/fbdev/release/* ; do ln -fs mali/directfb/release/`basename $$f` $(STAGING_DIR)/usr/lib/ ; done
endef
endif

ifeq ($(BR2_PACKAGE_MALI400_BIN_STMFB_DEFAULT),y)
define MALI400_BIN_INSTALL_STMFB_DEFAULT
	for f in $(@D)/fbdev/release/* ; do ln -fs mali/fbdev/release/`basename $$f` $(TARGET_DIR)/usr/lib/ ; done
endef
define MALI400_BIN_STAGING_STMFB_DEFAULT
	for f in $(@D)/fbdev/release/* ; do ln -fs mali/fbdev/release/`basename $$f` $(STAGING_DIR)/usr/lib/ ; done
endef
endif

define MALI400_BIN_INSTALL_STAGING_CMDS
	@if test ! -d $(MALI400_BIN_STAGING_PATH); then mkdir -p $(MALI400_BIN_STAGING_PATH); fi
	$(MALI400_BIN_STAGING_DIRECTFB)
	$(MALI400_BIN_STAGING_DIRECTFB_DEFAULT)
	$(MALI400_BIN_STAGING_STMFB)
	$(MALI400_BIN_STAGING_STMFB_DEFAULT)
	$(INSTALL) -D -m 0644 $(MALI400_BIN_PKGDIR)/egl.pc \
		$(STAGING_DIR)/usr/lib/pkgconfig/egl.pc
	$(INSTALL) -D -m 0644 $(MALI400_BIN_PKGDIR)/glesv2.pc \
		$(STAGING_DIR)/usr/lib/pkgconfig/glesv2.pc
	$(INSTALL) -D -m 0644 $(MALI400_BIN_PKGDIR)/gles.pc \
		$(STAGING_DIR)/usr/lib/pkgconfig/gles.pc
	@echo "Mali libraries installed: $(MALI400_BIN_STAGING_PATH)"
endef

define MALI400_BIN_INSTALL_TARGET_CMDS
	@if test ! -d $(MALI400_BIN_DEST_PATH); then mkdir -p $(MALI400_BIN_DEST_PATH); fi
	$(MALI400_BIN_INSTALL_DIRECTFB)
	$(MALI400_BIN_INSTALL_DIRECTFB_DEFAULT)
	$(MALI400_BIN_INSTALL_STMFB)
	$(MALI400_BIN_INSTALL_STMFB_DEFAULT)
	@echo "Mali libraries installed: $(MALI400_BIN_DEST_PATH)"
endef

$(eval $(generic-package))
