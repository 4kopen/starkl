################################################################################
#
# PTI firmware
#
################################################################################

PTI_FIRMWARE_VERSION = $(call qstrip,$(BR2_PACKAGE_PTI_FIRMWARE_VERSION))
PTI_FIRMWARE_SOURCE  = pti-firmware-$(PTI_FIRMWARE_VERSION).tar.gz
PTI_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
PTI_FIRMWARE_LICENSE = ST Proprietary

define PTI_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware/
	cd $(@D); cp $(call qstrip,$(BR2_PACKAGE_PTI_FIRMWARE_FILES)) $(TARGET_DIR)/lib/firmware/
endef

$(eval $(generic-package))
