################################################################################
#
# stmfbdev
#
################################################################################

STMFBDEV_VERSION = 1.9.3
STMFBDEV_SOURCE  = stmfbdev-$(STMFBDEV_VERSION).tar.gz
STMFBDEV_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STMFBDEV_LICENSE = LGPL-2.1

STMFBDEV_INSTALL_STAGING = YES
STMFBDEV_AUTORECONF = YES

STMFBDEV_DEPENDENCIES += display
STMFBDEV_DEPENDENCIES += directfb

$(eval $(autotools-package))
