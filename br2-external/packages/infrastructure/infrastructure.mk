################################################################################
#
# infrastructure
#
################################################################################

INFRASTRUCTURE_VERSION = 1.103.12.1.2
INFRASTRUCTURE_SOURCE  = infrastructure-$(INFRASTRUCTURE_VERSION).tar.gz
INFRASTRUCTURE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
INFRASTRUCTURE_LICENSE = GPLv2, ST Proprietary

INFRASTRUCTURE_MODULE_SUBDIRS = linux/

INFRASTRUCTURE_MODULE_MAKE_OPTS += SDK2_ENABLE_ATTRIBUTES=y

$(eval $(kernel-module))
$(eval $(generic-package))
