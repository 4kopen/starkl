################################################################################
#
# display headers
#
################################################################################

DISPLAY_HEADERS_VERSION = 5.85.58.2
# TODO  version number should the same as the no -headers one
DISPLAY_HEADERS_SOURCE  = display-$(DISPLAY_HEADERS_VERSION).tar.gz
DISPLAY_HEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
DISPLAY_HEADERS_LICENSE = GPL-2.0

$(eval $(generic-package))
