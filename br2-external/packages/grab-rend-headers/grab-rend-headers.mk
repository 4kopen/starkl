################################################################################
#
# grab_rend
#
################################################################################

GRAB_REND_HEADERS_VERSION = 5.85.34
GRAB_REND_HEADERS_SOURCE  = grab-rend-$(GRAB_REND_VERSION).tar.gz
GRAB_REND_HEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
GRAB_REND_HEADERS_LICENSE = GPL-2.0

define GRAB_REND_HEADERS_INSTALL_TARGET_CMDS
      true
endef

$(eval $(generic-package))
