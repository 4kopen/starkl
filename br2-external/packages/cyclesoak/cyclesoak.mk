################################################################################
#
# cyclesoak
#
################################################################################

CYCLESOAK_SOURCE = zc.tar.gz
CYCLESOAK_SITE   = https://web.archive.org/web/20080722053136/http://www.zipworld.com.au/~akpm/linux

CYCLESOAK_DEPENDENCIES += gdbm

define CYCLESOAK_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) CC=$(TARGET_CC) cyclesoak
endef

define CYCLESOAK_INSTALL_TARGET_CMDS
	$(INSTALL) -d $(TARGET_DIR)/usr/bin
	$(INSTALL) -t $(TARGET_DIR)/usr/bin $(@D)/cyclesoak
endef

$(eval $(generic-package))
