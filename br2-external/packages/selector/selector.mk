################################################################################
#
# selector
#
################################################################################

SELECTOR_VERSION = 1.3.4
SELECTOR_SOURCE  = selector-$(SELECTOR_VERSION).tar.gz
SELECTOR_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
SELECTOR_LICENSE = GPL-2.0

SELECTOR_DEPENDENCIES += streaming-engine-headers
SELECTOR_DEPENDENCIES += display-headers


SELECTOR_MODULE_SUBDIRS = /linux

SELECTOR_MODULE_MAKE_OPTS += PLAYER2=$(STREAMING_ENGINE_HEADERS_BUILDDIR)
SELECTOR_MODULE_MAKE_OPTS += CONFIG_INFRA_PATH=$(INFRASTRUCTURE_BUILDDIR)
SELECTOR_MODULE_MAKE_OPTS += CONFIG_DISPLAY_PATH=$(DISPLAY_HEADERS_BUILDDIR)

$(eval $(kernel-module))
$(eval $(generic-package))
