################################################################################
#
# debug-scripts
#
################################################################################

DEBUG_SCRIPTS_VERSION = 1.6.1
DEBUG_SCRIPTS_SOURCE  = debug-scripts-$(DEBUG_SCRIPTS_VERSION).tar.gz
DEBUG_SCRIPTS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
DEBUG_SCRIPTS_LICENSE = ST Proprietary

define DEBUG_SCRIPTS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/etc/debug_scripts
	cp $(@D)/sdk2_debug_trace $(TARGET_DIR)/usr/bin/
	cp $(@D)/sdk2_debug_readme.txt $(TARGET_DIR)/etc/debug_scripts/
	mkdir -p $(TARGET_DIR)/etc/debug_scripts/dvbgraph
	cp $(@D)/dvbgraph/dvbgraph.sh $(TARGET_DIR)/etc/debug_scripts/dvbgraph/
endef

$(eval $(generic-package))
