################################################################################
#
# display
#
################################################################################

DISPLAY_VERSION = 5.85.58.2
DISPLAY_SOURCE  = display-$(DISPLAY_VERSION).tar.gz
DISPLAY_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
DISPLAY_LICENSE = GPL-2.0

DISPLAY_MODULE_SUBDIRS = linux/kernel

DISPLAY_DEPENDENCIES += infrastructure
DISPLAY_DEPENDENCIES += blitter
DISPLAY_DEPENDENCIES += grab-rend

#include path
DISPLAY_MODULE_MAKE_OPTS += STG_TOPDIR=$(@D)
DISPLAY_MODULE_MAKE_OPTS += CONFIG_INFRASTRUCTURE_PATH=$(INFRASTRUCTURE_BUILDDIR)
DISPLAY_MODULE_MAKE_OPTS += CONFIG_BLITTER_PATH=$(BLITTER_BUILDDIR)
DISPLAY_MODULE_MAKE_OPTS += CONFIG_GRAB_REND_PATH=$(GRAB_REND_BUILDDIR)

# configuration option
DISPLAY_MODULE_MAKE_OPTS += CONFIG_DISPLAY_CEC=$(BR2_PACKAGE_STLINUXTV_CEC)
DISPLAY_MODULE_MAKE_OPTS += CONFIG_DISPLAY_HDMIRX=$(BR2_PACKAGE_HDMIRX)
DISPLAY_MODULE_MAKE_OPTS += CONFIG_DISPLAY_PIXEL_CAPTURE=$(or $(BR2_PACKAGE_STLINUXTV_V4L2_VIDEO_CAPTURE),$(BR2_PACKAGE_STLINUXTV_DVP))

ifeq ($(BR2_PACKAGE_DISPLAY_ENABLE_TESTS),y)
DISPLAY_DEPENDENCIES += strelay
DISPLAY_MODULE_MAKE_OPTS += CONFIG_STRELAY_PATH=$(STRELAY_BUILDDIR)
DISPLAY_TESTS_INSTALL_TARGET_CMDS = echo 'coredisplaytest' > $(TARGET_DIR)/etc/modules.d/070-coredisplaytest
else
DISPLAY_MODULE_MAKE_OPTS += DISABLE_DISPLAY_TESTS=y
endif

DISPLAY_MODULE_MAKE_OPTS += SDK2_ENABLE_ATTRIBUTES=y
DISPLAY_MODULE_MAKE_OPTS += SDK2_ENABLE_HDMI_TX_ATTRIBUTES=y

DISPLAY_MODULE_MAKE_OPTS += CONFIG_MACH_STM_STIH418=y

ifeq ($(BR2_PACKAGE_DISPLAY_USER_TESTS),y)
DISPLAY_DEPENDENCIES += directfb
define DISPLAY_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/linux CROSS_COMPILE=$(TARGET_CROSS) STG_TOPDIR=$(@D) PKG_CONFIG_LIBDIR=$(STAGING_DIR)/usr/lib/pkgconfig  PKG_CONFIG_SYSROOT_DIR=$(STAGING_DIR) PKG_CONFIG=$(HOST_DIR)/usr/bin/pkg-config tests
endef

DISPLAY_USER_TESTS_INSTALL_TARGET_CMDS = $(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/linux CROSS_COMPILE=$(TARGET_CROSS) STG_TOPDIR=$(@D) tests_install
endif

define DISPLAY_INSTALL_TARGET_CMDS
	$(DISPLAY_USER_TESTS_INSTALL_TARGET_CMDS)
	$(INSTALL) -d $(TARGET_DIR)/etc/modprobe.d/
	$(INSTALL) -t $(TARGET_DIR)/etc/modprobe.d/ -m 0644 $(BR2_EXTERNAL_STARKL_PATH)/packages/display/coredisplay.conf
	$(INSTALL) -t $(TARGET_DIR)/etc/modprobe.d/ -m 0644 $(BR2_EXTERNAL_STARKL_PATH)/packages/display/stmfb.conf
	$(INSTALL) -d        $(TARGET_DIR)/etc/modules.d/
	echo 'coredisplay' > $(TARGET_DIR)/etc/modules.d/010-coredisplay
	echo 'stmfb'       > $(TARGET_DIR)/etc/modules.d/050-coredisplay
	$(DISPLAY_TESTS_INSTALL_TARGET_CMDS)
endef

$(eval $(kernel-module))
define DISPLAY_INSTALL_KERNEL_HEADERS
	install -m 444 -D $(@D)/linux/include/hdcp.h $(STAGING_DIR)/usr/include/linux/stm/hdcp.h
	install -m 444 -D $(@D)/linux/include/hdmi.h $(STAGING_DIR)/usr/include/linux/stm/hdmi.h
	install -m 444 -D $(@D)/linux/include/stmcp.h $(STAGING_DIR)/usr/include/linux/stm/stmcp.h
	install -m 444 -D $(@D)/linux/kernel/drivers/video/stmfb.h $(STAGING_DIR)/usr/include/linux/stmfb.h
endef
DISPLAY_POST_BUILD_HOOKS += DISPLAY_INSTALL_KERNEL_HEADERS
$(eval $(generic-package))
