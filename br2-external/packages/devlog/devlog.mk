################################################################################
#
# devlog
#
################################################################################

DEVLOG_VERSION = 2.6.1
DEVLOG_SOURCE  = devlog-$(DEVLOG_VERSION).tar.gz
DEVLOG_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
DEVLOG_LICENSE = GPL-2.0+

DEVLOG_INSTALL_STAGING = YES
DEVLOG_AUTORECONF = YES
DEVLOG_AUTORECONF_OPTS = --verbose --install --force

DEVLOG_DEPENDENCIES += linux
DEVLOG_DEPENDENCIES += stlinuxtv
DEVLOG_DEPENDENCIES += display

DEVLOG_MAKE_ENV += SDK2_SOURCE_STLINUXTV=$(STLINUXTV_BUILDDIR)
DEVLOG_MAKE_ENV += KERNEL3_10_STI=$(LINUX_BUILDDIR)
DEVLOG_MAKE_ENV += DISPLAY_ENGINE=$(DISPLAY_BUILDDIR)
DEVLOG_MAKE_ENV += CC=$(TARGET_CC)

$(eval $(autotools-package))

