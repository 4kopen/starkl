################################################################################
#
# stream-tests
#
################################################################################

STREAM_TESTS_VERSION = 1.129.98
STREAM_TESTS_SOURCE  = stream-tests-$(STREAM_TESTS_VERSION).tar.gz
STREAM_TESTS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STREAM_TESTS_LICENSE = ST Proprietary

define STREAM_TESTS_INSTALL_TARGET_CMDS
	mkdir $(TARGET_DIR)/etc/scripts/
	cp $(@D)/target/streaming_engine.sh $(TARGET_DIR)/etc/scripts/
endef

$(eval $(generic-package))
