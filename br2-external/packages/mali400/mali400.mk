################################################################################
#
# mali400
#
################################################################################

MALI400_VERSION = 1.0.9.4KOPEN.0.1
MALI400_SOURCE  = mali400-$(MALI400_VERSION).tar.gz
MALI400_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
MALI400_LICENSE = GPL-2.0

#cannot use the $(eval $(kernel-module)) buildroot kernel rules,
# mali use some own pre-build/post-build rules
MALI400_DEPENDENCIES = linux

MALI400_MODULE_MAKE_CMD = $(TARGET_MAKE_ENV)  \
		KDIR=$(LINUX_DIR)             \
		ARCH=arm                      \
		CROSS_COMPILE=$(TARGET_CROSS) \
		$(MAKE)


define MALI400_BUILD_CMDS
	$(MALI400_MODULE_MAKE_CMD) -C $(@D)/mali
endef

define MALI400_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) ARCH=arm -C $(LINUX_DIR) M=$(@D)/mali INSTALL_MOD_DIR=mali INSTALL_MOD_PATH=$(TARGET_DIR) modules_install
	$(INSTALL) -d $(TARGET_DIR)/etc/modules.d/
	echo 'mali' > $(TARGET_DIR)/etc/modules.d/060-mali
endef


$(eval $(generic-package))
