################################################################################
#
# LPM firmware
#
################################################################################

LPM_FIRMWARE_VERSION = $(call qstrip,$(BR2_PACKAGE_LPM_FIRMWARE_VERSION))
LPM_FIRMWARE_SOURCE  = lpm-firmware-$(LPM_FIRMWARE_VERSION).tar.gz
LPM_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
LPM_FIRMWARE_LICENSE = ST Proprietary

define LPM_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware/
	cd $(@D); cp $(call qstrip,$(BR2_PACKAGE_LPM_FIRMWARE_FILES)) $(TARGET_DIR)/lib/firmware/
endef

$(eval $(generic-package))
