################################################################################
#
# test-utils
#
################################################################################

TEST_UTILS_VERSION = 1.70.22
TEST_UTILS_SOURCE  = test-utils-$(TEST_UTILS_VERSION).tar.gz
TEST_UTILS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
TEST_UTILS_LICENSE = GPL-2.0

TEST_UTILS_DEPENDENCIES += stlinuxtv
TEST_UTILS_DEPENDENCIES += streaming-engine
TEST_UTILS_DEPENDENCIES += infrastructure
TEST_UTILS_DEPENDENCIES += display
TEST_UTILS_DEPENDENCIES += directfb
TEST_UTILS_DEPENDENCIES += alsa-lib
TEST_UTILS_DEPENDENCIES += libxml2
TEST_UTILS_DEPENDENCIES += stmedia-ctl

TEST_UTILS_MAKE_CMD = $(TARGET_MAKE_ENV) RPMIT=n \
		    $(MAKE) \
		    ARCH=armv7 \
		    PKG_CONFIG=$(HOST_DIR)/usr/bin/pkg-config \
		    PLAYER2=$(STREAMING_ENGINE_BUILDDIR) \
		    CONFIG_INFRASTRUCTURE_PATH=$(INFRASTRUCTURE_BUILDDIR) \
		    CONFIG_DISPLAY_PATH=$(DISPLAY_BUILDDIR) \
		    CC=$(TARGET_CC) AR=$(TARGET_AR)

define TEST_UTILS_BUILD_CMDS
	 $(TEST_UTILS_MAKE_CMD) -C $(@D) \
		 STM_CROSS_TARGET_DIR=$(STAGING_DIR) \
		 all
endef

define TEST_UTILS_INSTALL_TARGET_CMDS
	$(TEST_UTILS_MAKE_CMD) -C $(@D) \
		STM_CROSS_TARGET_DIR=$(TARGET_DIR) \
		TARGET_BIN_DIR=$(TARGET_DIR)/usr/bin \
		TARGET_LIB_DIR=$(TARGET_DIR)/usr/lib \
		prefix=$(TARGET_DIR)/usr install
endef

$(eval $(generic-package))
