################################################################################
#
# stmedia-ctl
#
################################################################################

STMEDIA_CTL_VERSION = INT12
STMEDIA_CTL_SOURCE  = stmedia-ctl-$(STMEDIA_CTL_VERSION).tar.gz
STMEDIA_CTL_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STMEDIA_CTL_LICENSE = LGPL-2.1

STMEDIA_CTL_INSTALL_STAGING = YES

STMEDIA_CTL_AUTORECONF = YES
STMEDIA_CTL_AUTORECONF_OPTS = --verbose --install --force -Wno-error

$(eval $(autotools-package))
