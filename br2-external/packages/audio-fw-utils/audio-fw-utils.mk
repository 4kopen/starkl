################################################################################
#
# audio-fw-utils
#
################################################################################

AUDIO_FW_UTILS_VERSION = 1.2.0
AUDIO_FW_UTILS_SOURCE  = audio-fw-utils-$(AUDIO_FW_UTILS_VERSION).tar.gz
AUDIO_FW_UTILS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
AUDIO_FW_UTILS_LICENSE = ST Proprietary

AUDIO_FW_UTILS_DEPENDENCIES += multicom

AUDIO_FW_UTILS_MAKE_OPTS += MULTICOM4=$(MULTICOM_BUILDDIR)
AUDIO_FW_UTILS_MAKE_OPTS += SDK2_SOURCE_audio_apiheaders_ext=$(AUDIO_APIHEADERS_BUILDDIR)
AUDIO_FW_UTILS_MAKE_OPTS += ARCH=armv7

define AUDIO_FW_UTILS_BUILD_CMDS
	$(TARGET_MAKE_ENV) RPMIT=n $(MAKE) $(AUDIO_FW_UTILS_MAKE_OPTS) CC=$(TARGET_CC) -C $(@D)
endef
define AUDIO_FW_UTILS_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) ARCH=armv7 TARGET_BIN_DIR=$(TARGET_DIR)/usr/bin install
endef

$(eval $(generic-package))
