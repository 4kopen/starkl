################################################################################
#
# stmfapp
#
################################################################################

STMFAPP_VERSION = 2.27.8
STMFAPP_SOURCE  = stmfapp-$(STMFAPP_VERSION).tar.gz
STMFAPP_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STMFAPP_LICENSE = ST Proprietary

STMFAPP_AUTORECONF = YES
STMFAPP_DEPENDENCIES += stmf

ifeq ($(BR2_PACKAGE_GST1_PLUGINS_BAD_PLUGIN_DVB),y)
define STMFAPP_INSTALL_CONFIG_FILES_DVB_CHANNELS
	mkdir -p $(TARGET_DIR)/root/.gstreamer-1.5/
	cp $(@D)/src/.gstreamer/*.conf $(TARGET_DIR)/root/.gstreamer-1.5/
endef
endif

ifeq ($(BR2_PACKAGE_LIRC_TOOLS),y)
STMFAPP_DEPENDENCIES += lirc-tools
define STMFAPP_INSTALL_CONFIG_FILES_LIRC
	mkdir -p $(TARGET_DIR)/usr/share/applications/gst-apps/
	cp $(@D)/src/gst-apps.lircrc $(TARGET_DIR)/usr/share/applications/gst-apps/
endef
endif

STMFAPP_POST_INSTALL_TARGET_HOOKS+=$(STMFAPP_INSTALL_CONFIG_FILES_LIRC)
STMFAPP_POST_INSTALL_TARGET_HOOKS+=$(STMFAPP_INSTALL_CONFIG_FILES_DVB_CHANNELS)


$(eval $(autotools-package))
