################################################################################
#
# stpoke
#
################################################################################

STPOKE_VERSION = $(call qstrip,$(BR2_PACKAGE_STPOKE_VERSION))
STPOKE_SOURCE  = stpoke-$(STPOKE_VERSION).tar.gz
STPOKE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STPOKE_LICENSE = ST Proprietary

define STPOKE_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) CROSS_COMPILE="$(TARGET_CROSS)" all
endef

define STPOKE_INSTALL_TARGET_CMDS
	$(SED) s,usr/local,usr,g $(@D)/makefile
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) install
endef

$(eval $(generic-package))
