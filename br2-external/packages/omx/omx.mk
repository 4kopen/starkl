################################################################################
#
# omx
#
################################################################################

OMX_VERSION = 0.18.04
OMX_SOURCE  = omx-$(OMX_VERSION).tar.gz
OMX_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
OMX_LICENSE = Apache-2.0

OMX_DEPENDENCIES += openmax-headers
OMX_DEPENDENCIES += omxse
OMX_DEPENDENCIES += alsa-lib
OMX_DEPENDENCIES += stlinuxtv

OMX_MAKE_ENV += SDK2_SOURCE_OMXHEADERS=$(OPENMAX_HEADERS_BUILDDIR)
OMX_MAKE_ENV += SDK2_SOURCE_STLINUXTV=$(STLINUXTV_BUILDDIR)
OMX_MAKE_ENV += SDK2_SOURCE_OMXSE=$(OMXSE_BUILDDIR)
OMX_MAKE_ENV += CC=$(TARGET_CC) \
                CXX=$(TARGET_CXX)


define OMX_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) $(OMX_MAKE_ENV) all
endef

define OMX_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(generic-package))
