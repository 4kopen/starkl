################################################################################
#
# apps-tests
#
################################################################################

APPS_TESTS_VERSION = 1.194.10
APPS_TESTS_SOURCE  = apps-tests-$(APPS_TESTS_VERSION).tar.gz
APPS_TESTS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
APPS_TESTS_LICENSE = ST Proprietary

define APPS_TESTS_INSTALL_TARGET_CMDS
	cd $(@D) ; ./install.sh $(TARGET_DIR)
endef

$(eval $(generic-package))
