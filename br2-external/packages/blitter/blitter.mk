################################################################################
#
# blitter
#
################################################################################

BLITTER_VERSION = 5.85.34
BLITTER_SOURCE  = blitter-$(BLITTER_VERSION).tar.gz
BLITTER_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
BLITTER_LICENSE = GPL-2.0, LGPL-2.1

BLITTER_MODULE_SUBDIRS = linux/kernel

#include directory
BLITTER_MODULE_MAKE_OPTS += STM_BLITTER_TOPDIR=$(@D)
#options
BLITTER_MODULE_MAKE_OPTS += CONFIG_STM_DRIVERS=y

ifeq ($(BR2_PACKAGE_BLITTER_UTILS),y)

define BLITTER_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)/linux/utils CROSS_COMPILE=$(TARGET_CROSS) all
endef

define BLITTER_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) TARGET_DIR=$(TARGET_DIR) $(MAKE) -C $(@D)/linux/utils  CROSS_COMPILE=$(TARGET_CROSS) install
endef

endif

$(eval $(kernel-module))
$(eval $(generic-package))
