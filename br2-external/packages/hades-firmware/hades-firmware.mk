################################################################################
#
# hades-firmware
#
################################################################################

HADES_FIRMWARE_VERSION = hades4.16
HADES_FIRMWARE_SOURCE  = hades-firmware-$(HADES_FIRMWARE_VERSION).tar.gz
HADES_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
HADES_FIRMWARE_LICENSE = ST Proprietary

HADES_FIRMWARE_TARGETFILENAME = hades_fw_cannes25.bin

define HADES_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware
	cp $(@D)/$(BR2_PACKAGE_HADES_FIRMWARE_FILENAME) $(TARGET_DIR)/lib/firmware/$(HADES_FIRMWARE_TARGETFILENAME)
endef

$(eval $(generic-package))
