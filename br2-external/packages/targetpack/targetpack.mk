################################################################################
#
# targetpack
#
################################################################################

TARGETPACK_VERSION = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_VERSION))
TARGETPACK_SOURCE  = targetpack-$(TARGETPACK_VERSION).tar.gz
TARGETPACK_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
TARGETPACK_LICENSE = ST Proprietary

TARGETPACK_INSTALL_IMAGES = YES

TARGETPACK_PLATFORM = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_PLATFORM))
TARGETPACK_PARAMS   = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_OPTIONS))
TARGETPACK_BOARD    = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_BOARD))
TARGETPACK_CPU      = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_CPU))

define TARGETPACK_ECHO_STTP_XML
	echo '<sttp:targetpack                                     \
	 xmlns:sttp="http://www.st.com/XMLSchema/TargetPack">      \
	  <sttp:directories>                                       \
	    <sttp:directory sttp:path="./$(TARGETPACK_PLATFORM)"/> \
	  </sttp:directories>                                      \
	</sttp:targetpack>' > $(@D)/sttp.xml
endef

ifeq ($(BR2_PACKAGE_TARGETPACK_PARAMS_LEGACY),y)
  ifeq ($(TARGETPACK_PARAMS),)
    define TARGETPACK_PARAMS_BUILD_CMDS
	$(TARGETPACK_ECHO_STTP_XML)
    endef
  else
    TARGETPACK_DEPENDENCIES += host-romgen
    define TARGETPACK_PARAMS_BUILD_CMDS
	$(TARGETPACK_ECHO_STTP_XML)
	cd $(@D); $(HOST_DIR)/usr/bin/romgen "no_stmc:$(TARGETPACK_BOARD):$(TARGETPACK_CPU):active_cores=$(TARGETPACK_CPU),$(subst $(space),$(comma),$(TARGETPACK_PARAMS))"
    endef
  endif
else
  TARGETPACK_DEPENDENCIES += host-python
  define TARGETPACK_PARAMS_BUILD_CMDS
    $(TARGETPACK_ECHO_STTP_XML)
    cd $(@D); python2 "$(TARGETPACK_PLATFORM)/btfw_params/gen_params.py" "fw_params_file=$(TARGETPACK_PLATFORM)/" $(TARGETPACK_PARAMS)
  endef
endif

TARGETPACK_REBUILD_CURRENT_DIR = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_REBUILD_CURRENT_DIR))
TARGETPACK_REBUILD_ENV         = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_REBUILD_ENV))

TARGETPACK_PARAMS_FILENAME = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_PARAMS_FILENAME))
TARGETPACK_LIB_FILENAME    = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_LIB_FILENAME))
TARGETPACK_EXE_FILENAME    = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_EXE_FILENAME))

ifeq ($(BR2_PACKAGE_TARGETPACK_REBUILD),y)
  TARGETPACK_MAKE_CMDS = bash -c 'cd $(@D)/$(TARGETPACK_REBUILD_CURRENT_DIR) \
                         && $(TARGET_MAKE_ENV) source ./setup_$(TARGETPACK_REBUILD_ENV).sh none \
                         && $(TARGET_MAKE_ENV) $(MAKE1) $1'

  define TARGETPACK_BUILD_CMDS
	$(call TARGETPACK_MAKE_CMDS,clean)
	$(call TARGETPACK_MAKE_CMDS,all)
	$(TARGETPACK_PARAMS_BUILD_CMDS)
  endef
else
  TARGETPACK_BUILD_CMDS = $(TARGETPACK_PARAMS_BUILD_CMDS)
endif

define TARGETPACK_INSTALL_LINK
	if [ "$1" ]; then cd "$(BINARIES_DIR)" && ln -fs "$(TARGETPACK_PLATFORM)/$1" $2; fi
endef

define TARGETPACK_INSTALL_IMAGES_PATH
	if [ -e "$(@D)/$1" ]; then cd "$(@D)" && cp --archive --parents "$1" "$(BINARIES_DIR)"; fi

endef

TARGETPACK_INSTALL_IMAGES_PATHS = $(call qstrip,$(BR2_PACKAGE_TARGETPACK_INSTALL_IMAGES_PATHS))

define TARGETPACK_INSTALL_IMAGES_CMDS
	cp "$(@D)/sttp.xml" "$(BINARIES_DIR)"
	cd "$(@D)" && cp --archive --parents "$(TARGETPACK_PLATFORM)" "$(BINARIES_DIR)"
	$(call TARGETPACK_INSTALL_LINK,$(TARGETPACK_PARAMS_FILENAME),btfmw_params.bin)
	$(call TARGETPACK_INSTALL_LINK,$(TARGETPACK_LIB_FILENAME),libbtfmw.a)
	$(call TARGETPACK_INSTALL_LINK,$(TARGETPACK_EXE_FILENAME),btfmw.out)
	$(foreach path,$(TARGETPACK_INSTALL_IMAGES_PATHS),$(call TARGETPACK_INSTALL_IMAGES_PATH,$(path)))
endef

$(eval $(generic-package))
