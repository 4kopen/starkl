################################################################################
#
# strelay
#
################################################################################

STRELAY_VERSION = 1.129.104.1
STRELAY_SOURCE  = strelay-$(STRELAY_VERSION).tar.gz
STRELAY_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STRELAY_LICENSE = GPL-2.0


STRELAY_DEPENDENCIES += streaming-engine-headers

STRELAY_MODULE_MAKE_OPTS += TREE_ROOT=$(STREAMING_ENGINE_HEADERS_BUILDDIR)
STRELAY_MODULE_MAKE_OPTS += CONFIG_STM_STREAMINGENGINE=m


$(eval $(kernel-module))
$(eval $(generic-package))
