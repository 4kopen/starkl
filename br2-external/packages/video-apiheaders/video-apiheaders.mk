################################################################################
#
# video-apiheaders
#
################################################################################

VIDEO_APIHEADERS_VERSION = hades4.16
VIDEO_APIHEADERS_SOURCE  = video-apiheaders-$(VIDEO_APIHEADERS_VERSION).tar.gz
VIDEO_APIHEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
VIDEO_APIHEADERS_LICENSE = ST Proprietary

define VIDEO_APIHEADERS_INSTALL_TARGET_CMDS
      true
endef

$(eval $(generic-package))
