################################################################################
#
# romgen
#
################################################################################

# We don't have the sources of romgen :-/

ROMGEN_ASSETS = $(BR2_EXTERNAL_STARKL_PATH)/../launchers/stmc2/assets
ROMGEN_SCRIPT = $(HOST_DIR)/usr/bin/romgen
ROMGEN_LICENSE = ST Proprietary

define HOST_ROMGEN_INSTALL_CMDS
	mkdir -p $(dir $(ROMGEN_SCRIPT))
	echo '#!/bin/sh' > $(ROMGEN_SCRIPT)
	echo 'env HOME=/ PROOT_NO_SECCOMP=1 $(ROMGEN_ASSETS)/proot-x86 -r $(ROMGEN_ASSETS) -b /dev -b /proc -b . $$PROOT_OPTIONS /opt/STM/STLinux-2.4/host/stmc/bin/romgen $$*' >> $(ROMGEN_SCRIPT)
	chmod +x $(ROMGEN_SCRIPT)
endef

$(eval $(host-generic-package))
