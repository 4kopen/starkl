################################################################################
#
# grab_rend
#
################################################################################

GRAB_REND_VERSION = 5.85.34
GRAB_REND_SOURCE  = grab-rend-$(GRAB_REND_VERSION).tar.gz
GRAB_REND_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
GRAB_REND_LICENSE = GPL-2.0

GRAB_REND_MODULE_SUBDIRS = linux/kernel

GRAB_REND_DEPENDENCIES += infrastructure
GRAB_REND_DEPENDENCIES += blitter
GRAB_REND_DEPENDENCIES += display-headers

#include path
GRAB_REND_MODULE_MAKE_OPTS += STG_TOPDIR=$(@D)
GRAB_REND_MODULE_MAKE_OPTS += CONFIG_INFRASTRUCTURE_PATH=$(INFRASTRUCTURE_BUILDDIR)
GRAB_REND_MODULE_MAKE_OPTS += CONFIG_BLITTER_PATH=$(BLITTER_BUILDDIR)
GRAB_REND_MODULE_MAKE_OPTS += DISPLAY_ENGINE=$(DISPLAY_HEADERS_BUILDDIR)
#tests include path
GRAB_REND_MODULE_MAKE_OPTS += INFRA_PATH=$(INFRASTRUCTURE_BUILDDIR)
GRAB_REND_MODULE_MAKE_OPTS += STMFB_PATH=$(DISPLAY_HEADERS_BUILDDIR)
GRAB_REND_MODULE_MAKE_OPTS += KERNEL_ROOT=$(LINUX_DIR)

# configuration option
GRAB_REND_MODULE_MAKE_OPTS += CONFIG_DISPLAY_PIXEL_CAPTURE=$(or $(BR2_PACKAGE_STLINUXTV_V4L2_VIDEO_CAPTURE),$(BR2_PACKAGE_STLINUXTV_DVP))
GRAB_REND_MODULE_MAKE_OPTS += CONFIG_DISPLAY_HDMIRX=$(BR2_PACKAGE_HDMIRX)
GRAB_REND_MODULE_MAKE_OPTS += CONFIG_MACH_STM_STIH418=y

GRAB_REND_MODULE_MAKE_OPTS += DISABLE_DISPLAY_TESTS=y

GRAB_REND_MODULE_MAKE_OPTS += SDK2_ENABLE_ATTRIBUTES=y


$(eval $(kernel-module))
$(eval $(generic-package))
