################################################################################
#
# streaming-engine-headers
#
################################################################################

STREAMING_ENGINE_HEADERS_VERSION = 1.129.104.1
# TODO  version number shoud the same as the no -headers one
STREAMING_ENGINE_HEADERS_SOURCE  = streaming-engine-$(STREAMING_ENGINE_HEADERS_VERSION).tar.gz
STREAMING_ENGINE_HEADERS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
STREAMING_ENGINE_HEADERS_LICENSE = GPL-2.0

$(eval $(generic-package))
