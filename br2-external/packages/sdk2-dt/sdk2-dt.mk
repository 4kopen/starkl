################################################################################
#
# SDK2 Devicetree
#
################################################################################

SDK2_DT_VERSION = $(call qstrip,$(BR2_PACKAGE_SDK2_DT_VERSION))
SDK2_DT_SOURCE  = sdk2-dt-$(SDK2_DT_VERSION).tar.gz
SDK2_DT_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
SDK2_DT_LICENSE = GPLv-2.0

SDK2_DT_ADDITIONAL_DEFINES = $(call qstrip,$(BR2_PACKAGE_SDK2_DT_ADDITIONAL_DEFINES))

SDK2_DT_DTS_NAME = $(filter %.dts,$(notdir $(call qstrip,$(BR2_LINUX_KERNEL_CUSTOM_DTS_PATH))))


define SDK2_DT_DEFINE
	echo \#define $(shell echo $1 | tr = ' ') >> $(@D)/$2

endef

define SDK2_DT_GENERATE_DTS
	-cat $(@D)/template-$1 > $(@D)/$1
	$(foreach variable,$(SDK2_DT_ADDITIONAL_DEFINES),$(call SDK2_DT_DEFINE,$(variable),$1))
	echo '#include $(BR2_PACKAGE_SDK2_DT_INCLUDED_DTSP)' >> $(@D)/$1

endef

define SDK2_DT_BUILD_CMDS
	$(foreach dts,$(SDK2_DT_DTS_NAME),$(call SDK2_DT_GENERATE_DTS,$(dts)))
endef

$(eval $(generic-package))
