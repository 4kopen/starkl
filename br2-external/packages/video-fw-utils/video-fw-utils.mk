################################################################################
#
# video-fw-utils
#
################################################################################

VIDEO_FW_UTILS_VERSION = 17.2-0
VIDEO_FW_UTILS_SOURCE  = video-fw-utils-$(VIDEO_FW_UTILS_VERSION).tar.gz
VIDEO_FW_UTILS_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
VIDEO_FW_UTILS_LICENSE = GPL-2.0

VIDEO_FW_UTILS_MAKE_OPTS += MULTICOM4=$(MULTICOM_BUILDDIR)
VIDEO_FW_UTILS_MAKE_OPTS += SDK2_SOURCE_video_apiheaders_ext=$(VIDEO_APIHEADERS_BUILDDIR)
VIDEO_FW_UTILS_MAKE_OPTS += ARCH=armv7


define VIDEO_FW_UTILS_BUILD_CMDS
	$(TARGET_MAKE_ENV) RPMIT=n $(MAKE) $(VIDEO_FW_UTILS_MAKE_OPTS) CC=$(TARGET_CC) -C $(@D)
endef
define VIDEO_FW_UTILS_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) ARCH=armv7 TARGET_BIN_DIR=$(TARGET_DIR)/usr/bin install
endef


$(eval $(generic-package))
