################################################################################
#
# video-firmware
#
################################################################################

VIDEO_FIRMWARE_VERSION = hades4.16
VIDEO_FIRMWARE_SOURCE  = video-firmware-$(VIDEO_FIRMWARE_VERSION).tar.gz
VIDEO_FIRMWARE_SITE    = file://$(BR2_EXTERNAL_STARKL_PATH)/../tarballs
VIDEO_FIRMWARE_LICENSE = ST Proprietary

VIDEO_FIRMWARE_TARGETFILENAME = vid_firmware_stih418-delta.elf


define VIDEO_FIRMWARE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/lib/firmware
	cp $(@D)/$(BR2_PACKAGE_VIDEO_FIRMWARE_FILENAME) $(TARGET_DIR)/lib/firmware/$(VIDEO_FIRMWARE_TARGETFILENAME)
endef

$(eval $(generic-package))
