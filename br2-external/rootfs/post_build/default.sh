#!/bin/sh

set -eux

if grep -q ^BR2_ENABLE_DEBUG=y$ "${BR2_CONFIG}"; then
	echo 'debugfs /sys/kernel/debug debugfs defaults 0 0' >> "$1/etc/fstab"
fi
