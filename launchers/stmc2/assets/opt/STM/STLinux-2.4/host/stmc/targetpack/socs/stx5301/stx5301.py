"""
STi5301 SoC specific functions
"""

import sttp
import sttp.stmc
import sttp.targetpack

import tap
import tap.jtag

import tapmux

def connect(parameters):
    """
    Performs all STi5301 SoC connect actions
    Must be used prior to performing board-level intiialization
    """
    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    tapmux.connect(False, parameters)

    tap.jtag.check_soc_device_id("stx5301")

def complete_connect(parameters):
    tapmux.complete_connect(parameters)

