import sttp
import sttp.jtag
import sttp.logging

import tap
import tap.jtag

import convertor

jtag_debug   = 0
reset_debug  = 0

def init(p):
    global parameters
    global jtag_debug, reset_debug
    parameters = p

    if parameters.has_key("targetpack_debug") and parameters["targetpack_debug"].find("jtag")!=-1:
       jtag_debug = 1

    if parameters.has_key("debug") and sttp.stmc.get_operating_mode() != "ROMGEN":
        debugList = parameters["debug"].split(":")
        if "reset" in debugList:
            reset_debug = 1

def debug_print(debug, s):

    if debug:
        sttp.logging.print_out(s)


def autodetect_chain(verbose=False):

    vals = {
              'tms' : "000110_000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000_110",
      'td_to_target': "000000_111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111_100",
    }

    inputs = sttp.jtag.sequence( vals )

    if verbose:
        sttp.logging.print_out("autodetect_chain() td_from: %s" % (inputs["td_from_target"],))

    return inputs["td_from_target"]


# setup_chain()
#    chain           - the desired, complete chain including all DBUs if appropriate
#    dbu_unavailable - the DBUs which are not available because the correspoding TMC
#                      has not been put in DBU mode so must be left in BYPASS

def setup_chain(chain, dbu_unavailable=[]):

    allCores = sttp.stmc.get_all_core_info()

    debug_print(jtag_debug,"g4tap.setup_chain(): chain %s\n" % (chain,))

    # hack
    chain_list = []
    for coreName in chain:
        info = allCores[coreName]

        debug_print(jtag_debug,"  setup_chain: core %s, info %s\n" % (coreName, info,))

        if info.has_key("parameters") and info["parameters"].has_key("chain_position"):
            chain_list.append(info)

    debug_print(jtag_debug,"g4tap.setup_chain(): chain_list %s\n" % (chain_list,))

    def ChainListCmp(a, b):
        c_a = int(a["parameters"]["chain_position"] )
        c_b = int(b["parameters"]["chain_position"] )
 
        if c_a == c_b:
            return 0
        elif c_a < c_b:
            return -1
        else:
            return 1

    def DumpChain():
        for c in chain_list:
            debug_print(jtag_debug,"    chain item %s, soc pos %s, ir length %s, ir before %s, ir after %s, taps before %s, taps after %s" % \
                  ( c["parameters"]["core_name"], 
                    c["parameters"]["chain_position"], 
                    c["parameters"]["ir_length"],
                    c["parameters"]["ir_bits_before"],
                    c["parameters"]["ir_bits_after"],
                    c["parameters"]["taps_before"],
                    c["parameters"]["taps_after"],
                  ) )

    chain_list.sort(ChainListCmp)

    last_core = None
    ir_bits_before  = 0
    taps_before     = 0
    ir_bits_after   = 0
    taps_after      = 0

    total_taps      = 0
    total_ir_length = 0
    core_set = ""

    TMC_IR_LENGTH = 5

    for c in chain_list:
        core_name  = c["parameters"]["core_name"]

        # An implicit assumption that if core_name is a DBU in dbu_unavailable list
        # then we have the TMC still in this position in the chain
        if core_name in dbu_unavailable:
            ir_length = TMC_IR_LENGTH
            c["parameters"]["ir_length"] = ir_length
            c["parameters"]["inactive_dbu"] = True
            a_tmc = True
        else:
            ir_length = int(c["parameters"]["ir_length"])
            a_tmc = False

        # TODO - "parallel" cores - a9_0/a9_1
        core_set += core_name + ":" + str(ir_length) + ","

        if last_core and last_core["parameters"]["chain_position"] == c["parameters"]["chain_position"]:
            if (False == a_tmc) and (int(last_core["parameters"]["ir_length"]) != ir_length):
                raise sttp.STTPException("Mismatching ir_length for cores in chain position %d" % (int(last_core["parameters"]["chain_position"])))

            c["parameters"]["ir_bits_before"] = str( ir_bits_before - ir_length)
            c["parameters"]["taps_before"]    = str( taps_before - 1)
        else:
            total_ir_length += ir_length
            total_taps      += 1

            c["parameters"]["ir_bits_before"] = str( ir_bits_before )
            c["parameters"]["taps_before"]    = str( taps_before )

            ir_bits_before += ir_length
            taps_before    += 1

        last_core = c

    for c in chain_list:
        core_name = c["parameters"]["core_name"]
        ir_length = int(c["parameters"]["ir_length"])

        ir_bits_before = int(c["parameters"]["ir_bits_before"])
        taps_before    = int(c["parameters"]["taps_before"])

        ir_bits_after  = total_ir_length - (ir_bits_before + ir_length)
        taps_after     = total_taps      - (taps_before + 1)

        c["parameters"]["ir_bits_after"] = str( ir_bits_after )
        c["parameters"]["taps_after"]    = str( taps_after )

        cp = sttp.stmc.get_core_params(core_name)
        cp["ir_length"]      = str( ir_length )
        cp["ir_bits_before"] = str( ir_bits_before )
        cp["taps_before"]    = str( taps_before )
        cp["ir_bits_after"]  = str( ir_bits_after )
        cp["taps_after"]     = str( taps_after )

        if not cp.has_key("core_name"):
            cp["core_name"] = str(core_name)

        sttp.stmc.set_core_params(core_name, cp)
        
        if not c["parameters"].has_key("core_name"):
            c["parameters"]["core_name"]      = str(core_name)

    debug_print(jtag_debug,"Chain configured:" )
    DumpChain()

    tp = sttp.stmc.get_target_params()
    tp["core_set"] = core_set
    sttp.stmc.set_target_params(tp)


def setup_jtag_and_reset(leave_reset_asserted):
    global reset_asserted

    global post_reset_assert_period
    global reset_assert_period
    global tap_post_reset_assert_period
    global tap_reset_assert_period

    if parameters.has_key("reset_assert_period"):
        reset_assert_period = int(parameters["reset_assert_period"])
    else:
        reset_assert_period = 100000

    if parameters.has_key("post_reset_assert_period"):
        post_reset_assert_period = int(parameters["post_reset_assert_period"])
    else:
        post_reset_assert_period = 100000

    if parameters.has_key("tap_reset_assert_period"):
        tap_reset_assert_period = int(parameters["tap_reset_assert_period"])
    else:
        tap_reset_assert_period = 100000

    if parameters.has_key("tap_post_reset_assert_period"):
        tap_post_reset_assert_period = int(parameters["tap_post_reset_assert_period"])
    else:
        tap_post_reset_assert_period = 100000

    tap.jtag.init(parameters)

    convertor.check_convertor(parameters)

    sttp.targetinfo.setup_cores()

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    # assume all initialized to 0 in driver
    p = "use_rtck"
    # default to 0
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 0
    sttp.stmc.ioctl("STMC_BASE_FPGA_SET_USE_RTCK", [ v ])
        
    p = "rtck_timeout"
    # default to 1
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 1 
    sttp.stmc.ioctl("STMC_BASE_FPGA_SET_RTCK_TIMEOUT", [ v ])

    p = "tdo_sample_delay"
    # default to 0
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 0
    sttp.stmc.ioctl("STMC_BASE_FPGA_SET_TDO_SAMPLE_DELAY", [ v ])
  
    cb = sttp.targetpack.get_callback("init_jtag")
    if cb:
        cb()
    else:
        inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1"})

    # Now enable outputs
    sttp.stmc.output_enable(True)

    cb = sttp.targetpack.get_callback("post_init_jtag")
    if cb:
        cb()
    
    p = "vtref_check"
    # default to 0
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 0

    if v:
        # Check vtref - note VTREF can be read when LVDS outputs are disabled
        if 0 == int(inputs["vtref"]):
            raise sttp.STTPException("vtref signal on target JTAG connector is low - target may be disconnected or not powered")

    tap_reset = True
    sys_reset = True

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1: 
        sys_reset = False

    if parameters.has_key("no_tap_reset") and int(parameters["no_tap_reset"])==1:
        tap_reset = False

    if parameters.has_key("no_sys_reset") and int(parameters["no_sys_reset"])==1:
        sys_reset = False

    # sttp.logging.print_out("parameters %s, sys_reset %s, tap_reset %s\n" % (parameters, sys_reset, tap_reset) )
 
    # Note sys reset is asserted while tap reset is performed
    if sys_reset:
        cb = sttp.targetpack.get_callback("pre_reset")
        if cb: cb()

        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        debug_print(reset_debug," >-> nrst set to 1 <-<")
        sttp.jtag.sequence({'nrst' : "0"})
        debug_print(reset_debug," >-> nrst set to 0 <-<")

        # What should the min delay be?
        debug_print(reset_debug," -> reset_assert_period <-")
        sttp.stmc.delay(reset_assert_period)

        sttp.targetinfo.target_in_reset_state()

        debug_print(jtag_debug,"Performed system reset")
    else:
        # No reset case
        sttp.targetinfo.target_in_no_reset_state()  

        debug_print(jtag_debug,"*NOT* performed system reset")

    if tap_reset:
        sttp.jtag.sequence({'ntrst' : "1"})
        debug_print(reset_debug," >-> ntrst set to 1 <-<")
        sttp.jtag.sequence({'ntrst' : "0"})
        debug_print(reset_debug," >-> ntrst set to 0 <-<")

        # What should the min delay be?
        debug_print(reset_debug," -> tap_reset_assert_period <-")
        sttp.stmc.delay(tap_reset_assert_period)

        sttp.jtag.sequence({'ntrst' : "1"})    
        debug_print(reset_debug," >-> ntrst set to 1 <-<")

        debug_print(reset_debug," -> tap_post_reset_assert_period <-")
        sttp.stmc.delay(tap_post_reset_assert_period)

        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        # Move to RTI
        sttp.jtag.sequence({"tms": "111110",  "td_to_target": "000000"})

        debug_print(jtag_debug,"Performed TAP reset and moved to RTI")
    else:
        debug_print(jtag_debug,"*NOT* performed TAP reset")

    if False == leave_reset_asserted:
        # Now deassert sys-reset
        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
        sttp.jtag.sequence({'nrst' : "1"})    
        debug_print(reset_debug," >-> nrst set to 1 <-<")
        debug_print(reset_debug," -> post_reset_assert_period <-")
        sttp.stmc.delay(post_reset_assert_period)
        reset_asserted = False
        debug_print(jtag_debug,"reset_asserted = False")
    else:
        reset_asserted = True
        debug_print(jtag_debug,"reset_asserted = True")

    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    if not parameters.has_key("alt_jtag_port"):

        tmcs, num_tmc, master = tap.jtag.get_tmcs_by_index()
        #sttp.logging.print_out("tmcs    %s" % tmcs )
        #sttp.logging.print_out("num_tmc %d" % (num_tmc) )
        #sttp.logging.print_out("master  %d" % (master) )

        if "ROMGEN" != sttp.stmc.get_operating_mode() and \
          not parameters.has_key("no_tmc_validate"):
            silent = parameters.has_key("no_tmc_verbose") and (1==int(parameters["no_tmc_verbose"]))
            tdf = autodetect_chain(not silent)
            tmc_ir_len = int(tmcs[0]['ir_length'],0)  # assume that all TMCs on the scan chain are of the same ir_length
            tmc_ir_patten = "10000"
            if (tmc_ir_len == 7):
                tmc_ir_patten = "1000000"
            if tdf[7:7+(tmc_ir_len*num_tmc)] != tmc_ir_patten * num_tmc:
                raise sttp.STTPException("autodetect chain: failed to get chained TMC pattern " + tmc_ir_patten * num_tmc)

        tap.jtag.tmc_shift_ir(2)
        devid = tap.jtag.tmc_shift_dr(0, 32)
        sttp.logging.print_out("Main TMC devid 0x%08x" % (devid,) )

        # concatentate the devids in each die
        full_devid = 0
        for i in range(num_tmc):
            tap.jtag.tmc_shift_ir(2, i)
            v = tap.jtag.tmc_shift_dr(0, 32, i)
            full_devid <<= 32
            full_devid |= v
        sttp.logging.print_out("Complete TMC chain devid 0x%x" % (full_devid,) )
        sttp.targetinfo.set_target_info("device_id", full_devid)

    # Wait until we have read out the devid before calling the post_reset
    # callback because this may well call get_target_info("device_id")
    if not reset_asserted:
        # *** --- JPU Unlock callback --- for no_reset=1 ***
        cb = sttp.targetpack.get_callback("post_reset")
        if cb: cb()

    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)


def complete_reset():
    global reset_asserted

    if reset_asserted:

        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
        sttp.jtag.sequence({'nrst' : "1"})    
        debug_print(reset_debug," >-> nrst set to 1 <-<")
        debug_print(reset_debug," -> post_reset_assert_period <-")
        sttp.stmc.delay(post_reset_assert_period)
        reset_asserted = False

        # *** --- JPU Unlock callback --- for normal connection ***
        cb = sttp.targetpack.get_callback("post_reset")
        if cb: cb()

        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)


def get_all_cores():
    soc = sttp.targetpack.get_soc()
    a = []
    for c1 in soc.children:
        for c2 in c1.children:
           if isinstance(c2, sttp.targetpack.Cpu):
               if "dbu" != c2.parameters["coreType"]:
                   a.append(c1.name)
    return a


