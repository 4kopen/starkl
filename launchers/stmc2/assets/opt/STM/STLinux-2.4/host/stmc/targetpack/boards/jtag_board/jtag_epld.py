# Script for board with typical single core jtag device

import utilities

def jtag_connect(parameters):

    if parameters.has_key("get_convertor_type"):
        c = sttp.stmc.get_convertor_code()
        sttp.stmc.close()
        raise sttp.STTPException("convertor code=%d"%(c,))


    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    sttp.stmc.check_convertor_code(parameters["convertor_type"])

    try:
        frequency = int(parameters["tck_frequency"])
    except LookupError, e:
        raise sttp.STTPException("Parameter tck_frequency must be supplied")

    if frequency < 1:
        raise sttp.STTPException("tck_frequency must be greater than 0")

    real_tck = sttp.stmc.set_tck_frequency_no_override(frequency)

    sttp.logging.print_out( "jtag_connect: tck frequency requested %d Hz, set to %d Hz" % (frequency, real_tck) )

    for k, v in parameters["core_parameters"].iteritems():
        sttp.stmc.set_core_params(k, v)
        
    sttp.stmc.set_target_params(parameters["target_parameters"])
    
    # Allow a target timeout setup (in millisecs)
    if parameters.has_key("target_timeout"):
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_IO_TIMEOUT", [ int(parameters["target_timeout"]) ])

    # Switch driver interrupt usage
    if parameters.has_key("use_driver_interrupt"):
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_INTERRUPT_CONTROL", [ not int(parameters["use_driver_interrupt"]) ])

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
    
    if parameters["soc_info"]["debug"] == "tapmux":
        # Manual control of all JTAG lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        # Need this for TAPmux FPGA
        sttp.stmc.core_map({"tmc":"0"})
  
    inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1"})

    cb = sttp.targetpack.get_callback("post_init_jtag")
    if cb:
        cb()

    # Now enable outputs
    sttp.stmc.output_enable(True)
    
    if parameters["soc_info"]["debug"] == "jtagstreamer" and parameters["convertor_type"] == "STMC_Type_C":
        # Check vtref - note VTREF can be read when LVDS outputs are disabled
        if 0 == int(inputs["vtref"]):
            raise sttp.STTPException("vtref signal on target JTAG connector is low - target may be diconnected or not powered")

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1: 
        pass
    else:
        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        sttp.jtag.sequence({'nrst' : "0"})
        # What should the min delay be? Currently 100mS
        time.sleep(0.1)
        sttp.jtag.sequence({'nrst' : "1"})    
        sttp.jtag.sequence({'ntrst' : "1"})
        sttp.jtag.sequence({'ntrst' : "0"})
        # What should the min delay be? Currently 100mS
        time.sleep(0.1)
        sttp.jtag.sequence({'ntrst' : "1"})    
        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        # Move to RTI
        init_endstate = "RTI"
        if parameters.has_key("fsm_state_sequence"): 
            try:
                v = parameters["fsm_state_sequence"]
                sequences = v.split(";")
                for seq in sequences:
                    if seq.find("init:") == 0 and seq[-4:] == "SDRS" :
                        init_endstate = "SDRS"
                    elif seq.find("init:") == 0 and seq[-3:] == "RTI" :
                        init_endstate = "RTI"
            except Exception, e:
                raise sttp.STTPException("Invalid format for fsm_state_sequence")
            
        if  init_endstate == "RTI" :
            sttp.jtag.sequence({"tms": "111110",  "td_to_target": "000000"})
        elif init_endstate == "SDRS" :
            sttp.jtag.sequence({"tms": "1111101", "td_to_target": "0000000"})
        else : 
            raise sttp.STTPException("Invalid format for fsm_state_sequence - invalid endstate for init:")

def jtag_disconnect():
    pass

def _jtag_get_topology(core_string):
    
    def add_core(hash, name, position, ir_length, bits_before):
        hash[name] = {"core_name"     : name, 
                      "chain_position": str(pos), 
                      "ir_length"     : str(length), 
                      "ir_bits_before": str(totalIrBits), 
                      "ir_bits_after" : str(-1),
                      "taps_before"   : str(pos),
                      "taps_after"    : str(-1) }
                      
    errString = "Invalid format for core_set - expecting core_name:ir_length{,core_name2:ir_length2} or [core_name1,core_name2:ir_length]..."
         
    coreDetails = {}
    pos = 0
    totalIrBits = 0
    
    while len(core_string)>0:
        if core_string[0] == "[":
            # A set of cores in a STAR
            f = core_string.find("]")
            if -1 == f:
                raise sttp.STTPException(errString)
        
            star_set = core_string[1:f]
            # there may be a trailing comman after the closing ]
            f += 1
            try:
                starCores, length = star_set.split(":")
                cores = starCores.split(",")
            except ValueError, e:
                raise sttp.STTPException(errString)
        
            irLength = int(length)
            
            for name in cores:
                add_core(coreDetails, name, pos, length, totalIrBits);
        else:
            f = core_string.find(",") 
            if (f == -1):
                # Assume only a single core specified on remainder of string
                f = len(core_string)
            x = core_string[0:f]

            try:
                name, length = x.split(":")
            except ValueError, e:
                raise sttp.STTPException(errString)
                
            irLength = int(length)
            add_core(coreDetails, name, pos, length, totalIrBits);

        pos += 1
        totalIrBits += irLength
        core_string = core_string[f+1:]
            
    # Fixup "items" after    
    for k, v in coreDetails.iteritems():
        c = v
        c["ir_bits_after"] = str(totalIrBits - int(c["ir_bits_before"]) - int(c["ir_length"]) )
        c["taps_after"]    = str(pos - int(c["chain_position"]) - 1)

    """    
    for k, v in coreDetails.iteritems():
        print "Core: ", k
        for k2, v2 in v.iteritems():
            print "    %s = > %s" % (k2, v2)
    """
    return coreDetails


def jtag_configuration(parameters):
    coreParams = {}
    targetParams = {}

    coreList = {}
    soc = {}

    # Use TAPmux for I/O Convertor Type A
    # soc["debug"]     = "tapmux"
    soc["debug"]     = "jtagstreamer"
    
    firstCore = None

    # Handle the parameter "core_set"
    if parameters.has_key("core_set"):
        core_string = parameters["core_set"]
        
        coreParams = _jtag_get_topology(core_string)
        for k, v in coreParams.iteritems():
            pos      = int(coreParams[k]["chain_position"])  
            irLength = int(coreParams[k]["ir_length"])
            coreList[k] = {"core_index"  : pos,
                           "type"        : "jtag",
                           "debug"       : "jtag", 
                           "resetSelect" : "true", 
                           "parameters"  :  {"ir_length" : irLength } }

        targetParams["core_set"] = parameters["core_set"]
    else:
        # This allows another connection to a target that has already had a TargetConnect
        # with full configuration details
        pass

    # Handle the parameter "ir_debug_value"
    if parameters.has_key("ir_debug_value"):
        c, v = parameters["ir_debug_value"].split(":")
        if not c or not v:
            raise sttp.STTPException("Invalid format for ir_debug_value - expected core_name:value")
        try:
            coreParams[c]["ir_debug_value"] = v
        except LookupError, e:
            raise sttp.STTPException("Invalid core specified in ir_debug_value: " + c)
        
    if parameters.has_key("gpio_permit"):
        gpio = parameters["gpio_permit"]
        coreSignals = gpio.split(",")
        for c in coreSignals:
            coreName, sigName = c.split(":")
            if not coreList.has_key(coreName):
                raise sttp.STTPException("Invalid core %s specified in gpio_permit." % (coreName) )
        
        targetParams["gpio_permit"] = parameters["gpio_permit"]

    if parameters.has_key("fsm_state_sequence"):
        targetParams["fsm_state_sequence"] = parameters["fsm_state_sequence"]
        
    if parameters.has_key("fsm_state_validate"):
        targetParams["fsm_state_validate"] = parameters["fsm_state_validate"]

    if parameters.has_key("epld_on_second_jtag") and 1 == int(parameters["epld_on_second_jtag"]):
        # For now we need to pick up a different FPGA when driving the EPLD
        soc["debug_driverdata"] = "jtagstreamer_v2_epld.bin"

    if parameters.has_key("use_fpga"):
        soc["debug_driverdata"] = parameters["use_fpga"].strip()
        
    # These are "system" parameters required by the host dll to do setup]
    parameters["core_list"] = coreList
    parameters["soc_info"] = soc
    
    # Core and target parameters to be set by "connect" function
    if not parameters.has_key("convertor_type"):
        parameters["convertor_type"] = "STMC_Type_A"
    parameters["core_parameters"] = coreParams
    parameters["target_parameters"] = targetParams

   
