import time
import sttp
import sttp.stmc
import tap.jtag
import coresight
import core_utils

do_debug_print = False

ReadMemAPLocation = coresight.ReadMemAPLocation
WriteMemAPLocation = coresight.WriteMemAPLocation

def init_arm_debug(parameters, core):

    cp = sttp.stmc.get_core_params(core)

    coresight.power_up_debug(cp)
    coresight.dump_arm_dp_info(cp)
    memap = int(cp["coresight_memap"], 0)
    coresight.dump_arm_mem_ap_info(cp, memap)

    # Initialize the debug registers base address
    SCS_base_addr = cm4_rom_table_discovery(parameters, core)

    # Program the core to enter in debug mode
    boot_cm4(cp, SCS_base_addr)


def cm4_rom_table_discovery(parameters, core):

    cp = sttp.stmc.get_core_params(core)

    coresight_memap = int(cp["coresight_memap"], 0)
    ROM_BASE = int(cp["coresight_rom"], 0)

    debug_print(" -- coresight_memap = 0x%08x" % coresight_memap)
    debug_print(" -- ROM_BASE = 0x%08x" % ROM_BASE)

    SCS_entry = ReadMemAPLocation(cp, coresight_memap, ROM_BASE)
    SCS_base_addr = ( ROM_BASE + (SCS_entry & ~0x03)) & 0xFFFFFFFF

    debug_print( " -- CM4 SCS offset value  : 0x%08x" % (SCS_entry))
    debug_print( " -- CM4 SCS base address  : 0x%08x\n" % (SCS_base_addr))

    r = 0xffffffff
    index = 0
    # find out all entry tables
    while (r !=0):
       r = ReadMemAPLocation(cp, coresight_memap, ROM_BASE +(index*0x04))
       debug_print(" -- ROM Table entry %.2d: 0x%08x" % (index, r))
       index +=1

    return SCS_base_addr

def boot_cm4(cp, SCS_base_addr):

    coresight_memap = int(cp["coresight_memap"], 0)

    CPUIDR    = SCS_base_addr + 0xD00
    DFSR      = SCS_base_addr + 0xD30
    DHCSR     = SCS_base_addr + 0xDF0
    AIRCR     = SCS_base_addr + 0xD0C
    HFSR      = SCS_base_addr + 0xD2C
    ICSR      = SCS_base_addr + 0xD04
    C_DEBUGEN = (0xA05F <<16) | 1<<0

    # Read CPU ID as a control checkpoint
    cpuid = ReadMemAPLocation(cp, coresight_memap, CPUIDR)
    if (cpuid != 0x410FC241):
       raise sttp.STTPException("ERROR: CPUID read 0x%x at address 0x%x instead of 0x410FC241" % (cpuid, CPUIDR))
    else:
       sttp.logging.print_out("\n Found CM4 CPUID : 0x%.8x" % cpuid)

    # enable halting mode debug Bit(0) in DHCSR
    WriteMemAPLocation(cp, coresight_memap, DHCSR, C_DEBUGEN)

    sttp.stmc.delay(2000)

    # Check the DHCSR debug enable bit
    dhcsr = ReadMemAPLocation(cp, coresight_memap, DHCSR)
    debug_print( " -- CM4 DHCSR value : 0x%08x\n" % (dhcsr,))

    # set Halt Request Bit(1) in DHCSR
    dhcsr = C_DEBUGEN | 1<<1
    WriteMemAPLocation(cp, coresight_memap, DHCSR, dhcsr)

    sttp.stmc.delay(1000)

    # Check the DFSR halted bit
    dfsr = ReadMemAPLocation(cp, coresight_memap, DFSR)
    debug_print( " -- CM4 DFSR (Debug Fault Status register ): 0x%08x\n" % (dfsr))

    # Check the DHCSR debug mode bit
    dhcsr = ReadMemAPLocation(cp, coresight_memap, DHCSR)
    debug_print( " -- CM4 halted : %.1d \n" % ((dhcsr>>17)&0x1))

    if ((dhcsr>>17)&0x1) ==0:
       raise sttp.STTPException(" !! Failed to put CM4 in debug mode - Target Pack exited !! ")
    else:
       sttp.logging.print_out(" CM4 in debug mode. DHCSR value = 0x%x" % dhcsr)
       
    # Need to clean up the processor state that may have executed erroneous code until it stopped
    # Use the VECTRESET bit in the Application Interrupt and Reset Control Register
    WriteMemAPLocation(cp, coresight_memap, AIRCR, ((0x05FA <<16) | 0x01))
    sttp.stmc.delay(10000)

    # Check the HFSR value
    hfsr = ReadMemAPLocation(cp, coresight_memap, HFSR)
    debug_print( " Checking HFSR (0x%x) value  : 0x%x" % (HFSR, hfsr))
       
    # Check the ICSR value
    icsr = ReadMemAPLocation(cp, coresight_memap, ICSR)
    debug_print( " Checking ICSR (0x%x) value  : 0x%x\n" % (ICSR, icsr))

core_info = {}

def init(arm_core_info, parameters):
    # all info comes from XML file
    global core_info

    sttp_root = sttp.targetpack.get_tree()

    arm_core_name = arm_core_info["arm_core_name"]

#    def find_cpu(root, name):
#        for c in root.get_children():
#            if type(c) == sttp.targetpack.Cpu and str(c.parent.name) == str(name):
#                return c
#            if type(c) == sttp.targetpack.Component:
#                r = find_cpu(c, name)
#                if r != None:
#                    return r
#        return None

    arm_core = core_utils.find_cpu(sttp_root, arm_core_name)

    if arm_core:
        container = arm_core.parent.parent
    else:
        raise sttp.STTPException("Failed to find cpu name %s" % (arm_core_name,))

    coresight_amba_apb   = None
    coresight_debug_base = None
    coresight_rom_base   = None

    # Iterate of all components in the "container" which may be the "soc" to determine core position
    for d in container.__dict__:

        if d == arm_core_name:
            e = eval("container." + d)
            core_index = int(str(e.parameters["core_id"]))

    # Iterate of all components in the "container" which may be the "soc" to determine parameters
    for d in container.__dict__:

        if (None == coresight_amba_apb) and (d == arm_core_name):
            e = eval("container." + d)
            coresight_amba_apb = str(e.parameters["coresight_mem_ap"])

        if (None == coresight_debug_base and (0 == d.find("coresight_debug_c" + str(core_index)))):
            e = eval("container." + d)
            coresight_debug_base = str(e.parameters["APB_baseAddress"])
            coresight_rom_base   = str(e.parameters["ROM_baseAddress"])
            core_info[arm_core_name] = {
                    "coresight_debug_base"      : long(coresight_debug_base, 0),
                    "coresight_rom_base"        : long(coresight_rom_base, 0),
                    "coresight_debug_stbus_base": e.coresight_debug.baseAddress,
                }

    if None == coresight_amba_apb:
        raise sttp.STTPException("TargetPack error - component coresight_amba_apb not defined")
    if None == coresight_rom_base:
        raise sttp.STTPException("TargetPack error - component coresight_rom_base not defined")
    if None == coresight_debug_base:
        raise sttp.STTPException("TargetPack error - component coresight_debug not defined")

    names = list()
    names.append(arm_core_name)

    tp = sttp.stmc.get_target_params()
    for c in names:
        cp = sttp.stmc.get_core_params(c)

        jtag_timeout = None
        if parameters.has_key("jtag_timeout"):
            #  Pick up jtag_timeout passed as TargetString parameter
            jtag_timeout = str(parameters["jtag_timeout"])
        elif tp.has_key("jtag_timeout"):
            # Pick up jtag_timeout if specified in TargetPack top-level XML
            jtag_timeout = tp["jtag_timeout"]

        if jtag_timeout:
            # If the value has not been passed as stmc_core_param_target_timeout set the core param
            if not cp.has_key("target_timeout"):
                cp["target_timeout"] = jtag_timeout
            if not cp.has_key("jtag_timeout"):
                cp["jtag_timeout"]   = jtag_timeout
            cp["linktimeout"] = jtag_timeout

        cp["coresight_memap"] = str(coresight_amba_apb)
        cp["coresight_debug"] = str(coresight_debug_base)
        cp["coresight_rom"]   = str(coresight_rom_base)
        # Hack the core id to cope with GDB ARM v7 agent that assumes that the maximum number of cores are 4 !!
        cp["core_id"] = str(0)

        sttp.stmc.set_core_params(c, cp)

def debug_print(p):
    if do_debug_print:
       sttp.logging.print_out(p)
