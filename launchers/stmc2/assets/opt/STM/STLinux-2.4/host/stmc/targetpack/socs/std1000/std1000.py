# Functions specific to the std1000

# Import ST supplied libraries

import sttp
import sttp.pokepeek
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import st40_nomux

def connect(parameters):
    """
    Performs all std1000 SoC connect actions
    Must be used prior to performing board-level intiialization
    """

    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    st40_nomux.connect(parameters)


def std1000_set_clockgen(ckg, genconf):
    ## Setup PLL2/LMI clock path
    ckg.CTL_PLL2_FREQ.and_const(0xfffffffe)
    ckg.STATUS_PLL.while_and_ne(0x00000002, 0x00000002)

    ## Enable clk_lmi_2x
    ckg.CTL_EN.or_const(1)

    ## Wait for LMI DLL1 to lock
    genconf.LMI_STATUS.while_and_ne(0x00000001, 0x00000001)

    ## Wait for LMI DLL2 to lock
    genconf.LMI_STATUS.while_and_ne(0x00000002, 0x00000002)


def std1000_lmi_scr_nop(lmi, lminop):
    ## LMI.SCR_L
    ##   001=sms (Mode select)=nop
    ##   0=reserved
    ##   xxx=brfsh (Burst refresh)
    ##   000000000=reserved
    ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
    ##   0000=reserved
    ##
    ## LMI.SCR_H
    ##   00000000000000000000000000000000=reserved
    for i in xrange(lminop):
        lmi.LMI_SCR_0.poke(0x00010011)
