import sys
import time

import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import tap
import tap.jtag

import convertor

current_bypass_core_name = None
inMuxMode = False

def connect(mux, parameters):
    global current_bypass_core_name
    global inMuxMode

    register_tmc()

    # Is "pokepeek" core different to user's specified TargetString core
    if not parameters.has_key("tapmux_bypass_init"):
        connect_core = sttp.targetinfo.get_targetstring_core()    
        allCores = sttp.stmc.get_all_core_info()
        for core_name, info in allCores.iteritems():
            if info.has_key("poke_peek_default"):
                connect_core = core_name
                break

        parameters["tapmux_bypass_init"] = connect_core

    if parameters.has_key("tapmux_bypass_init") and parameters.has_key("tapmux_mux") and (int(parameters["tapmux_mux"]) == 1):
        # Allow core selection in bypass for initial setup prior to going to mux mode  
        inMuxMode = True
    elif parameters.has_key("tapmux_bypass"):
        inMuxMode = False        
    elif parameters.has_key("tapmux_mux") and (int(parameters["tapmux_mux"]) == 1):
        inMuxMode = True
    else:
        inMuxMode = mux

    convertor.check_convertor(parameters)

    # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
    sttp.stmc.core_map({"tmc":0})

    cb = sttp.targetpack.get_callback("init_jtag")
    if cb: 
        cb()
    else:
        # Ensure system is not in mux mode
        # TODO sttp.stmc.mux_mode(False)

        # Manual control of all JTAG lines
        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

        # Initially all high
        sttp.jtag.sequence({"nrst" : "1", "ntrst" : "1", "tms" : "1", "tck" : "1", "td_to_target" : "1", "nasebrk" : "1"})
 
        # Now enable outputs
        sttp.stmc.output_enable(True)

    cb = sttp.targetpack.get_callback("post_init_jtag")
    if cb: 
        cb()
        
    # Note on the mb421 the switches SW10:4 and SW11:1 impact the core seen
    # when reset and thus the device id read out
    # To read out the TMC device id, both switches must be set to ON 
    # i.e. they pull EMI ADDR[7:8] low. See ADCS 7716339 (mb421 schematics) and ADCS 7720683 (STx8010 datasheet)
 
    # Manual control of all JTAG lines
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    if not (parameters.has_key("no_devid_check") and (int(parameters["no_devid_check"]) == 1)):
        # Verify device id
        tapmux_reset_and_bypass(0)

        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

        id = tap.jtag.read_device_id()
        sttp.targetinfo.set_target_info("device_id", id)
        sttp.logging.print_out("Device id ", hex(id))

        # Manual control of all JTAG lines
        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    if not (parameters.has_key("no_reset") and (int(parameters["no_reset"]) == 1)):
        cb = sttp.targetpack.get_callback("pre_reset")
        if cb: cb()
            

        # does this SoC require trig_to_target high
        soc = sttp.targetpack.get_soc()

        if parameters.has_key("reset_assert_period"):
            reset_assert_period = int(parameters["reset_assert_period"])
        elif parameters.has_key("reset_low_period"):
            reset_assert_period = int(parameters["reset_low_period"])
        else:
            reset_assert_period = 0

        if parameters.has_key("post_reset_assert_period"):
            post_reset_assert_period = int(parameters["post_reset_assert_period"])
        else:
            post_reset_assert_period = 0

        if soc.parameters.has_key("trig_to_target_high_on_reset") and \
           soc.parameters["trig_to_target_high_on_reset"] == "true" :            
            # The STx8010/STx5301 needs to manipulate trig_to_target as part of the reset sequence            
            
            # TAPmux reset - all channels - back to channel 0
            sttp.jtag.sequence({"ntrst": "0", "tms": "0", "td_to_target": "0", "tck": "0"})

            # Reset device with trigout high to send CPU into debug ROM
            sttp.jtag.sequence({"nrst" : "1", "ntrst" : "1", "tms" : "1", "tck" : "1", "td_to_target" : "1", "trig_to_target" : "1"})
            sttp.jtag.sequence({"nrst" : "0"})

            if reset_assert_period:
                sttp.stmc.delay(reset_assert_period)

            sttp.jtag.sequence({"nrst" : "1"})

            if parameters.has_key("post_reset_delay"):
                sttp.stmc.delay(int(parameters["post_reset_delay"]))

            # do not drop trigout even after the reset sequence else we cannot boot
            # always boot program - depends on ROM contents

        else:
            # Assume a TAPmux SoC with nASEBRK
            no_asebrk_assert = \
               (parameters.has_key("no_asebrk_low") and (int(parameters["no_asebrk_low"]) == 1)) or \
               (parameters.has_key("no_asebrk_assert") and (int(parameters["no_asebrk_assert"]) == 1))

            no_asebrk_deassert = \
               (parameters.has_key("no_asebrk_deassert") and (int(parameters["no_asebrk_deassert"]) == 1))

            # TAPmux reset - all channels - back to channel 0
            # Then assert ntrst, followed by nrst
            tapmux_reset_and_bypass( 0 )

            sttp.jtag.sequence({"ntrst":        "10", 
                                "nrst":         "110"                                
                               })

            if reset_assert_period:
                sttp.stmc.delay(reset_assert_period)

            if not no_asebrk_assert:
                # assert asebrk
                sttp.jtag.sequence({"nasebrk" : "0"})

                if parameters.has_key("asebrk_assert_period"):
                    sttp.stmc.delay(int(parameters["asebrk_assert_period"]))
            else:
                sttp.logging.print_out("Not asserting nasebrk during reset sequence")

            # deassert reset
            sttp.jtag.sequence({"nrst" : "1"})

            if post_reset_assert_period:
                sttp.stmc.delay(post_reset_assert_period)

            # note there must be a hold period of "some millisecs" before deasserting nasebrk
            # this period is normally met by the latency between the above call to 
            # sttp.jtag.sequence() and the subsequent call below
           
            # deasert nasebrk, ntrst
            if no_asebrk_deassert:
                nasebrk_val = "0"
            else:
                nasebrk_val = "1"

            if parameters.has_key("post_asebrk_delay"):    
                sttp.jtag.sequence({"nasebrk" : nasebrk_val })
                sttp.stmc.delay(int(parameters["post_asebrk_delay"]))
                sttp.jtag.sequence({"ntrst"   : "1" })
            else:
                sttp.jtag.sequence({"nasebrk" : nasebrk_val,
                                    "ntrst"   : "01" })
   
        # back to TMC for callbacks   
        tapmux_reset_and_bypass(0)

        cb = sttp.targetpack.get_callback("post_reset")
        if cb: cb()

        sttp.targetinfo.target_in_reset_state()
    else:
        # back to TMC for callbacks   
        tapmux_reset_and_bypass(0)

        cb = sttp.targetpack.get_callback("no_reset")
        if cb: cb()
        # No reset case
        sttp.targetinfo.target_in_no_reset_state()


    # Single core mode
    sttp.logging.print_out("tapmux connect(): boot mode single core setup")

    # Now setup TAPmux
    # 
    # Manual control of all JTAG lines
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    # bypass to the core in the TargetString if an override is not supplied
    if parameters.has_key("tapmux_bypass_init"):
        bypass_core_name = parameters["tapmux_bypass_init"]
    elif parameters.has_key("tapmux_bypass"):
        bypass_core_name = parameters["tapmux_bypass"]
    else:
        bypass_core_name = sttp.targetinfo.get_targetstring_core()
        
    # Get the tapmux_channel on which this core attached    
    tapmux_bypass_channel = sttp.stmc.get_tapmux_channel(bypass_core_name);
    tapmux_reset_and_bypass(tapmux_bypass_channel)
          
    # TAPmux - Normal TAP operation for selected channel asset ntrst 
    # Autoclock of tck
    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        
    # Setup the STMC multiplexor side so that the core is plumbed to the tapmux channel 0
    sttp.stmc.core_map({bypass_core_name:0})
    current_bypass_core_name = bypass_core_name

    sttp.logging.print_out("tapmux setup to bypass to core %s, channel %s" % (bypass_core_name, tapmux_bypass_channel))

def complete_connect(parameters):
    global current_bypass_core_name
    global inMuxMode    

    cb = sttp.targetpack.get_callback("pre_complete_connect")
    if cb: cb(current_bypass_core_name)

    if inMuxMode==False:
        sttp.logging.print_out("tapmux complete_connect(): single core setup")

        if parameters.has_key("tapmux_bypass_init"):
            current_bypass_core_name = parameters["tapmux_bypass_init"]
            # Do we need to bypass to a different core 
            if parameters.has_key("tapmux_bypass"):
                bypass_core_name = parameters["tapmux_bypass"]
            else:
                bypass_core_name = sttp.targetinfo.get_targetstring_core()

            if cb or (current_bypass_core_name != bypass_core_name):
                sttp.logging.print_out("Initialized on core %s, bypassing to core %s" % (current_bypass_core_name, bypass_core_name))
                # Get the tapmux_channel on which this core attached    
                tapmux_bypass_channel = sttp.stmc.get_tapmux_channel(bypass_core_name);

                # Setup the STMC multiplexor side so that the tmc is plumbed to the tapmux channel 0
                sttp.stmc.core_map({"tmc":0})

                sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

                tapmux_reset_and_bypass(tapmux_bypass_channel)

                # Setup the STMC multiplexor side so that the core is plumbed to the tapmux channel 0
                sttp.stmc.core_map({bypass_core_name:0})
        
                sttp.logging.print_out("tapmux complete_connect(): bypass to core %s, tapmux channel %s" % (bypass_core_name, tapmux_bypass_channel))
    else:
        # MUX mode
        sttp.logging.print_out("tapmux complete_connect(): 3 core multiplexed mode")
        # SoC TAPmux setup by the FPGA when going into mux_mode
        # 
        # Setup the STMC multiplexor side so that cores to be debugged are plumbed to debug channels, 

        map = {"tmc":0}
        allCores = sttp.stmc.get_all_core_info()
        
        for core_name, info in allCores.iteritems():
            if info.has_key("active") and info["active"]:
                tapmux_channel = sttp.stmc.get_tapmux_channel(core_name)
                if tapmux_channel:
                    map[core_name] = tapmux_channel
        sttp.stmc.core_map(map)
        sttp.stmc.mux_mode(True)

def tapmux_reset_and_bypass(channel):
    
    class IllegalTapmuxChannel(sttp.STTPException):
        def __init__(self, p):
            sttp.STTPException.__init__(self,p)

    if channel == 0:
        tmsBP = "0"
        tdoBP = "0"
    elif channel == 1:
        tmsBP = "0"
        tdoBP = "1"
    elif channel == 2:
        tmsBP = "1"
        tdoBP = "0"
    elif channel == 3:
        tmsBP = "1"
        tdoBP = "1"
    else:
        raise IllegalTapmuxChannel(channel)

    # Manual control of tck
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
 
    sttp.jtag.sequence({"ntrst":         "000011111", \
                        "tms":           "00000" + tmsBP*3 + "0", \
                        "td_to_target" : "01111" + tdoBP*3 + "0", \
                        "tck":           "001000100" \
                       })

def register_tmc():
    # add a default TMC into the data structure in tap.jtag
    if {} == tap.jtag._the_tmcs:
        tap.jtag._the_tmcs["tmc"] = { "chainpos" : "0", "ir_length" : "5", "master" : "1" }

