# TargetScript for connecting and disconnecting to a mb422 (std2000)

import sttp
import sttp.logging
import sttp.targetpack
import sttp.stmc

# TargetPack imports
import std2000
import utilities

global sttp_root
sttp_root = sttp.targetpack.get_tree()


def mb422_configure(parameters):
    # Apply core address offset.
    connect_core = sttp.targetinfo.get_targetstring_core()
    # Set a core specific view of the registers for features such as Memory Mapped Register Set
    if connect_core == "st40":
        sttp.targetpack.apply_address_offset("st40_offset")


def mb422_connect(parameters):
    sttp.logging.print_out("mb422 connect start - parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(True, parameters)

    # Connect to std2000 SoC
    std2000.connect(parameters)

    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        # Set ST40 view of register addresses during connect.
        sttp.targetpack.apply_address_offset("st40_offset")
        sttp.pokepeek.enable()

        sttp.logging.print_out("mb422 initialization start ...")
        mb422_setup(parameters)

    utilities.set_tck_frequency(False, parameters)

    sttp.logging.print_out("mb422 initialization complete")


def mb422_disconnect():
    pass


# Configure the mb422
def mb422_setup(parameters):
    sttp.pokepeek.rom_delay_scale(27)
    if not (parameters.has_key("no_clockgen_config") and (int(parameters["no_clockgen_config"]) == 1)):
        mb422_clockgen_configure()
        sttp.pokepeek.rom_delay_scale(266)
    mb422_emi_configure()
    mb422_lmi1_configure()
    mb422_lmi1_configure()

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        ccn = sttp_root.std2000.st40.CCN.st40_ccn_regs
        ccn.CCN_CCR.poke(0x8000090d)


# mb422 CLOCKGEN Configuration
def mb422_clockgen_configure():
    global sttp_root
    ckgc = sttp_root.std2000.CKGC.std2000_ckgc_regs
    gen_conf = sttp_root.std2000.GEN_CONF.std2000_gen_conf_regs
    std2000.std2000_set_clockgen(ckgc, gen_conf, 0x06, 0x25, 0)


#  mb422 EMI Configuration
def mb422_emi_configure():
    emi = sttp_root.std2000.EMI.st40_emi_regs
    emi.EMI_BANK0_EMICONFIGDATA0.poke(0x04001691)
    emi.EMI_BANK0_EMICONFIGDATA1.poke(0x0a000000)
    emi.EMI_BANK0_EMICONFIGDATA2.poke(0x0e004400)
    emi.EMI_BANK0_EMICONFIGDATA3.poke(0x00000000)


# mb422 LMI Configuration
def mb422_mixer_common_configure(mixerNum):
    if mixerNum == 1:
        mixer = sttp_root.std2000.MIXER1.std2000_mixer_regs
    elif mixerNum == 2:
        mixer = sttp_root.std2000.MIXER2.std2000_mixer_regs

    mixer.MODC.poke(0x00000000)
    mixer.FILC.poke(0x0000003f)
    mixer.BTAC.poke(0x0000070e)
    mixer.HPC.poke(0x00000a01)
    mixer.RDBSC.poke(0x0004060a)
    mixer.WDBSC.poke(0x000f0f0f)
    mixer.OBCLC.poke(0x00080010)
    mixer.BKCLC.poke(0x00200018)
    mixer.LPBWC.poke(0x0a1e04b0)
    mixer.GENPC0.poke(0x00000000)
    mixer.GENPC1.poke(0x00000000)


def mb422_lmi1_configure():
    # Configuring LMI-1 for DDR SDRAM
    mb422_mixer_common_configure(1)

    mixer = sttp_root.std2000.MIXER1.std2000_mixer_regs
    mixer.ATC.poke(0x00050706)
    mixer.LMIAC.poke(0x00080009)
    mixer.PMC.poke(0x1fffff00)

    lmi = sttp_root.std2000.LMI1.st40_lmi3_regs
    # SDRAM Mode Register
    lmi.LMI_MIM_0.poke(0x0514005f)

    # SDRAM Timing Register
    lmi.LMI_STR_0.poke(0x35ad4345)

    # SDRAM Row Attribute 0
    lmi.LMI_SDRA0_0.poke(0xafe01a03)

    # SDRAM Row Attribute 1
    lmi.LMI_SDRA1_0.poke(0xafe01a03)

    # SDRAM Control Register
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000003)
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SDMR0.poke(0x00000400)
    lmi.LMI_SDMR0.poke(0x00000163)
    sttp.stmc.delay(200)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SCR_0.poke(0x00000004)
    lmi.LMI_SDMR0.poke(0x00000063)
    lmi.LMI_MIM_0.or_const(0x00000200)
    lmi.LMI_SCR_0.poke(0x00000000)

    # LMI Delay Registers
    gen_conf = sttp_root.std2000.GEN_CONF.std2000_gen_conf_regs
    gen_conf.LMI1_PDL_0_3_MSB_CTL.poke(0xd0d0d0d0)
    gen_conf.LMI1_PDL_0_3_LSB_CTL.poke(0x0000000f)


def mb422_lmi2_configure():
    # Configuring LMI-2 for DDR SDRAM
    mb422_mixer_common_configure(2)

    mixer = sttp_root.std2000.MIXER2.std2000_mixer_regs
    mixer.ATC.poke(0x00050706)
    mixer.LMIAC.poke(0x00170010)
    mixer.PMC.poke(0x1fffff00)

    lmi = sttp_root.std2000.LMI2.st40_lmi3_regs
    # SDRAM Mode Register
    lmi.LMI_MIM_0.poke(0x0514005f)

    # SDRAM Timing Register
    lmi.LMI_STR_0.poke(0x35ad4345)

    # SDRAM Row Attribute 0
    lmi.LMI_SDRA0_0.poke(0xb6e01a03)

    # SDRAM Row Attribute 1
    lmi.LMI_SDRA1_0.poke(0xb6e01a03)

    # SDRAM Control Register
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000003)
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SDMR0.poke(0x00000400)
    lmi.LMI_SDMR0.poke(0x00000163)
    sttp.stmc.delay(200)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SCR_0.poke(0x00000004)
    lmi.LMI_SDMR0.poke(0x00000063)
    lmi.LMI_MIM_0.or_const(0x00000200)
    lmi.LMI_SCR_0.poke(0x00000000)

    # LMI Delay Registers
    gen_conf = sttp_root.std2000.GEN_CONF.std2000_gen_conf_regs
    gen_conf.LMI2_PDL_0_3_MSB_CTL.poke(0xd0d0d0d0)
    gen_conf.LMI2_PDL_0_3_LSB_CTL.poke(0x0000000f)
