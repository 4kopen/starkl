#!/bin/sh

# Upgrade script for Release 0.4.1.

# This upgrade flashes new u-boot env.
# We must remember state of u-boot env variable 'current'.
# Variable 'current' will be initialised by u-boot env upgrade.
# This script keeps a copy of u-boot var 'current', which will
# be written back after u-boot env upgrade.

echo Saving state of U-Boot Env variable, current...
current=`fw_printenv current | sed 's/current=//'`
script=/tmp/upgrade/remember_curent.sh
echo '#!/bin/sh' > $script
echo fw_setenv current $current >> $script
echo >> $script
chmod a+x $script
cat $script
