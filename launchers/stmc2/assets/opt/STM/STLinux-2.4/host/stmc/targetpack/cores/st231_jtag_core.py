"""
Utilities for SoCs with st200 cores
"""
import os

import sttp
import sttp.stmc
import sttp.targetinfo

import core_utils

"""
Note: the code in this file sets up core parameters that are read by the ST200
toolset and are specific in name and value to this toolset

The parameter "core_state" has the following possible values:

  not set            - a "wire" reset sequence has been performed
                     * IMOPORTANT - it must be not set at all - if set to a spurious
                       value or "reset" the connection will fail in certain circumstances

  pokepeek_completed - the core is spinning in the debug rom after the poke peek agent has run

  no_reset           - the STMC/TargetPack has not manipulated reset

"""

debugPrefix = None

def setup_reset_state(coreName):
    global debugPrefix
    debugPrefix = core_utils.setup_debug("st231 (%s)" % (coreName,) )
 
    core_utils.debug(debugPrefix, ">>>> setup_reset_state(%s)" % (coreName,))

    try:
        c = sttp.stmc.get_core_params(coreName)
    except:
        c = dict()

    setup_debugram(coreName, c)
    core_utils.setup_default_timeout(debugPrefix, c)
    setup_pre_pokepeek_state(coreName)
    setup_post_pokepeek_state(coreName)
    
    sttp.stmc.set_core_params(coreName, c)

    core_utils.debug(debugPrefix, "    sttp.stmc.set_core_params(%s, %s)" % (coreName, c))
    core_utils.debug(debugPrefix, "<<<< setup_reset_state()")

def setup_no_reset_state(coreName):
    global debugPrefix
    debugPrefix = core_utils.setup_debug("st231 (%s)" % (coreName,) )

    core_utils.debug(debugPrefix, ">>>> setup_no_reset_state(%s)" % (coreName,))

    try:
        c = sttp.stmc.get_core_params(coreName)
    except:
        c = dict()

    c["core_state"] = "no_reset"
    
    setup_debugram(coreName, c)
    core_utils.setup_default_timeout(debugPrefix, c)
    setup_pre_pokepeek_state(coreName)
    setup_post_pokepeek_state(coreName)

    sttp.stmc.set_core_params(coreName, c)

    core_utils.debug(debugPrefix, "<<<< setup_no_reset_state(%s)" % (coreName,))


def setup_pre_pokepeek_state(coreName):
    global debugPrefix
    core_utils.debug(debugPrefix, ">>>> setup_pre_pokepeek_state(%s)" % (coreName,))

    def PrePokePeekFn():
        core_utils.debug(debugPrefix, ">>>> PrePokePeekFn(%s)" % (coreName,))

        # If this core has not been used for poke/peek do not update state
        if not core_utils.coreIsPokePeek(debugPrefix, coreName):
            core_utils.debug(debugPrefix, "<<<< PrePokePeekFn()")
            return

        try:
            c = sttp.stmc.get_core_params(coreName)
        except:
            c = dict()
 
        # setup the timeout
        core_utils.set_pokepeek_coreagent_timeout(debugPrefix, coreName, c)

        sttp.stmc.set_core_params(coreName, c)
        core_utils.debug(debugPrefix, "     sttp.stmc.set_core_params(%s, %s)" % (coreName, c))
        core_utils.debug(debugPrefix, "<<<< PrePokePeekFn()")

    core_utils.debug(debugPrefix, ">>>> setup_pre_pokepeek_state()")
    sttp.stmc.register_pokepeek_enable_callback(PrePokePeekFn)
    core_utils.debug(debugPrefix, "<<<< setup_pre_pokepeek_state()")


def setup_post_pokepeek_state(coreName):
    global debugPrefix
    core_utils.debug(debugPrefix, ">>>> setup_post_pokepeek_state(%s)" % (coreName,))

    def PostPokePeekFn():
        core_utils.debug(debugPrefix, ">>>> st231.py: PostPokePeekFn(core %s)" % (coreName,))

        # If this core has not been used for poke/peek do not update state
        allCoreInfo = sttp.stmc.get_all_core_info()
        pp = "poke_peek"

        if not allCoreInfo[coreName].has_key(pp):
            core_utils.debug(debugPrefix, "<<<< PostPokePeekFn()")
            return

        try:
            c = sttp.stmc.get_core_params(coreName)
        except:
            c = dict()

        c["core_state"] = "pokepeek_completed"

        # Timeout set for poke peek operation only.
        if c.has_key("stmc_pokepeek_timeout"):
            del c["stmc_pokepeek_timeout"]
        if c.has_key("linktimeout"):
            del c["linktimeout"]
        if c.has_key("linktimeout_original"):
            c["linktimeout"] = c["linktimeout_original"]
            
        sttp.stmc.set_core_params(coreName, c)

        core_utils.debug(debugPrefix, "     sttp.stmc.set_core_params(coreName %s, params %s)" % (coreName, c))
        core_utils.debug(debugPrefix, "<<<< PostPokePeekFn()")

    sttp.stmc.register_pokepeek_disable_callback(PostPokePeekFn)
    core_utils.debug(debugPrefix, "<<<< setup_post_pokepeek_state(%s)" % (coreName,))


debugram_base = None
debugram_size = None
debugram_remaining = None

def setup_debugram(coreName, coreParams):

    def MakeInt(value):
        import struct
        s = struct.pack("L", value)
        [v] = struct.unpack("i", s) 
        return v

    global debugram_base
    global debugram_size
    global debugram_remaining

    core_utils.debug(debugPrefix, ">>> setup_debugram(coreName %s, coreParams %s)" % (coreName, coreParams))

    # If debugram_base has not been set up, do so now
    if None == debugram_base:
        t = sttp.stmc.get_target_params()
        parameters = sttp.targetinfo.get_targetstring_parameters()

        # This is a nasty hack !!!
        if parameters.has_key("debugram_base") and parameters.has_key("debugram_size"):
            debugram_key      = "None"
            debugram_size_key = "None"
            debugram_base = int(parameters["debugram_base"], 0)
            debugram_remaining = debugram_size = int(parameters["debugram_size"], 0)
        elif (t.has_key("debugram_base_se") and \
             ((parameters.has_key("se") and (int(parameters["se"]) == 1)) or \
              (parameters.has_key("seuc") and (int(parameters["seuc"]) == 1)) or \
              (parameters.has_key("se29") and (int(parameters["se29"]) == 1)))):
            debugram_key = "debugram_base_se"
            debugram_size_key = "debugram_size_se"
        else:        
            debugram_key = "debugram_base"
            debugram_size_key = "debugram_size"
 
        if t.has_key(debugram_key):
            debugram_base = int(t[debugram_key], 0)

        if t.has_key(debugram_size_key):
            debugram_remaining = debugram_size = int(t[debugram_size_key], 0)

        if debugram_base:
            core_utils.debug(debugPrefix, "debugram_key %s, debugram_base 0x%08x" % (debugram_key, debugram_base))
        else:
            core_utils.debug(debugPrefix, "target param debugram_base or debugram_base_se not defined")

    # if this core is active, set "debugram" core parameter with address
    allCoreInfo = sttp.stmc.get_all_core_info()

    bytesPerCore = (4 * 1024)
    if debugram_base and \
       debugram_remaining >= bytesPerCore:
        import string
        coreParams["debugram"] = string.strip(hex(debugram_base), "L")
        # add 4k for next core
        debugram_base += bytesPerCore
        debugram_remaining -= bytesPerCore
 
    core_utils.debug(debugPrefix, "<<<< setup_debugram() - coreParams" % coreParams)

