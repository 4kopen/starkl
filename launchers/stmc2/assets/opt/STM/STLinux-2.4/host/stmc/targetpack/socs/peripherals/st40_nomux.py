import sys
import time

import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import convertor

def connect(parameters):

    root = sttp.targetpack.get_tree()

    convertor.check_convertor(parameters)

    sttp.stmc.core_map({"tmc":0})

    cb = sttp.targetpack.get_callback("init_jtag")
    if cb: 
        cb()
    else:
        # Manual control of all JTAG lines
        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

        # Initially all high
        sttp.jtag.sequence({"nrst" : "1", "ntrst" : "1", "tms" : "1", "tck" : "1", "td_to_target" : "1", "nasebrk" : "1"})
 
        # Now enable outputs
        sttp.stmc.output_enable(True)

    cb = sttp.targetpack.get_callback("post_init_jtag")
    if cb: 
        cb()

    if parameters.has_key("reset_assert_period"):
        reset_assert_period = int(parameters["reset_assert_period"])
    elif parameters.has_key("reset_low_period"):
        reset_assert_period = int(parameters["reset_low_period"])
    else:
        reset_assert_period = 0

    # Do a reset
    if not (parameters.has_key("no_reset") and (int(parameters["no_reset"]) == 1)):
        no_asebrk_low = parameters.has_key("no_asebrk_low") and (int(parameters["no_asebrk_low"]) == 1)

        cb = sttp.targetpack.get_callback("pre_reset")
        if cb: cb()

        # assert ntrst
        sttp.jtag.sequence({"ntrst" : "0"})

        # assert nrst
        sttp.jtag.sequence({"nrst" : "0"})
        
        if reset_assert_period:
            sttp.stmc.delay(reset_assert_period)

        if not no_asebrk_low:
            # assert asebrk
            sttp.jtag.sequence({"nasebrk" : "0"})

            if parameters.has_key("asebrk_assert_period"):
                sttp.stmc.delay(int(parameters["asebrk_assert_period"]))
        else:
            sttp.logging.print_out("Not asserting nasebrk during reset sequence")
        
        # deassert reset
        sttp.jtag.sequence({"nrst" : "1"})

        if parameters.has_key("post_reset_delay"):
            sttp.stmc.delay(int(parameters["post_reset_delay"]))

        # deassert asebrk
        sttp.jtag.sequence({"nasebrk" : "1"})

        if parameters.has_key("post_asebrk_delay"):    
            sttp.stmc.delay(int(parameters["post_asebrk_delay"]))

        # deassert ntrst
        sttp.jtag.sequence({"ntrst" : "1"})

        sttp.targetinfo.target_in_reset_state()
    else:
        cb = sttp.targetpack.get_callback("no_reset")
        if cb: cb()
 
        sttp.targetinfo.target_in_no_reset_state()

        sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
        sttp.jtag.sequence({"nrst" : "1", "ntrst" : "1", "tms" : "1", "tck" : "1", "td_to_target" : "1", "nasebrk" : "1"})

    cb = sttp.targetpack.get_callback("post_reset")
    if cb: cb()
    
    # Setup the STMC multiplexor side so that the core (core 0) is plumbed to debug channel 0
    bypass_core_name = sttp.targetinfo.get_targetstring_core()
    sttp.stmc.core_map({bypass_core_name:0})
