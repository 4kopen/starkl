import sttp
import sttp.jtag
import sttp.stmc
import sttp.targetpack

import arm_a9
import arm_a5x
import arm_cm4
import dbu
import g4tap

import st40_pmb_regs
import tap
import tap.jtag
import core_utils

no_sys_reset = 0
jtag_debug   = 0
reset_debug   = 0
clocks_debug = 0
cores_debug  = 0

die_info     = None

TMC_IR7_PATTERN = "1000000"
TMC_IR_PATTERN  = "10000"
DBU_IR_PATTERN  = "100"

def connect(targetstring_parameters, connect_info):

    global parameters
    global no_sys_reset, jtag_debug, reset_debug, clocks_debug, cores_debug

    global post_reset_assert_period
    global reset_assert_period
    global tap_post_reset_assert_period
    global tap_reset_assert_period

    global die_info, poke_peek_dbus

    debug_print(jtag_debug,">>>> g4soc.connect()")

    active_dbus           = ( connect_info.has_key("active_dbus") and connect_info["active_dbus"] ) or None
    cti_callback          = ( connect_info.has_key("cti_callback") and connect_info["cti_callback"] ) or None
    dbu_pattern           = connect_info["dbu_pattern"]
    die_info              = connect_info["die_info"]
    masked_reset_callback = ( connect_info.has_key("masked_reset_callback") and connect_info["masked_reset_callback"] ) \
                            or None
    poke_peek_dbus        = connect_info["poke_peek_dbus"]
    reset_tmc_pattern     = connect_info["reset_tmc_pattern"]

    parameters = targetstring_parameters

    if ( parameters.has_key("no_reset")     and 1==int(parameters["no_reset"]) ) or \
       ( parameters.has_key("no_sys_reset") and 1==int(parameters["no_sys_reset"]) ):
        no_sys_reset = True

    if parameters.has_key("reset_assert_period"):
        reset_assert_period = int(parameters["reset_assert_period"])
    elif parameters.has_key("on_emu"):
        reset_assert_period = 1000000
    else:
        reset_assert_period = 100000

    if parameters.has_key("post_reset_assert_period"):
        post_reset_assert_period = int(parameters["post_reset_assert_period"])
    elif parameters.has_key("on_emu"):
        post_reset_assert_period = 1000000
    else:
        post_reset_assert_period = 100000

    if parameters.has_key("tap_reset_assert_period"):
        tap_reset_assert_period = int(parameters["tap_reset_assert_period"])
    elif parameters.has_key("on_emu"):
        tap_reset_assert_period = 1000000
    else:
        tap_reset_assert_period = 100000

    if parameters.has_key("tap_post_reset_assert_period"):
        tap_post_reset_assert_period = int(parameters["tap_post_reset_assert_period"])
    elif parameters.has_key("on_emu"):
        tap_post_reset_assert_period = 1000000
    else:
        tap_post_reset_assert_period = 100000

    if parameters.has_key("debug") and sttp.stmc.get_operating_mode() != "ROMGEN":
        debugList = parameters["debug"].split(":")
        if "jtag" in debugList:
            jtag_debug = 1
        if "reset" in debugList:
            reset_debug = 1
        if "clocks" in debugList:
            clocks_debug = 1
        if "cores" in debugList:
            cores_debug = 1

    if parameters.has_key("sbag_enable") and (int(parameters["sbag_enable"]) == 1):
        try:
            import sys
            import sbag
            debug_print(jtag_debug, " Python Version is %d.%d.%d" %(sys.version_info.major, sys.version_info.minor, sys.version_info.micro))
            sttp.targetpack.register_callback("pre_connect",  sbag.pre_connect_callback_fn)
        except:
            import os
            debug_print(jtag_debug,  " STMC2 release in use is %s" %sttp.get_release_info())
            sttp.logging.print_out(" Error when attempting to access the secure GIT server - Please check your STMC2 Toolset version !!")
            os._exit(1)

    debug_print(jtag_debug,">>>> g4tap.init()")
    # Initialize the g4tap module
    g4tap.init(parameters)
    debug_print(jtag_debug,"<<<< g4tap.init()")

    dbu.init(die_info, parameters)

    core_utils.arm_core_module_init(parameters)

    if cti_callback:
        cti_callback()

    if parameters.has_key("reset_only") :
        # Reset the device only
        # nRESET & nTRST, print device_id's 
        g4tap.setup_jtag_and_reset(False)

        debug_print(jtag_debug, "Chain...")   
        g4tap.autodetect_chain(True)
        return

    if no_sys_reset:
        # nRESET & nTRST, print device_id's
        # *** --- JPU Unlock (Master) called in Here --- for no_reset=1 ***
        debug_print(jtag_debug,">>>> g4tap.setup_jtag_and_reset()")
        g4tap.setup_jtag_and_reset(False)

        tdf = g4tap.autodetect_chain(True)
        ## TODO - for Si SASC1 + MPE  
        if tdf[7:7+len(reset_tmc_pattern)] != reset_tmc_pattern:
            raise sttp.STTPException("autodetect chain: failed to get chained TMC pattern %s at bit position 4" % reset_tmc_pattern)
        dbu.tmc2dbu(active_dbus)
        return

    # Callbacks may be made from within the call to g4tap.setup_jtag_and_reset()
    # nRESET & nTRST, print device_id's
    g4tap.setup_jtag_and_reset(True)

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        tdf = g4tap.autodetect_chain()
        if tdf[7:7+len(reset_tmc_pattern)] != reset_tmc_pattern:
            raise sttp.STTPException("autodetect chain: failed to get chained TMC pattern %s at bit position 4" % reset_tmc_pattern)

        full_devid = sttp.targetinfo.get_target_info("device_id")

        for i in range(len(die_info)):
            
            # ignore the cut - it is too much hassle
            devid = full_devid & 0x0fffffff 
            
            expected_devid = int( tap.jtag.get_tmc_by_name(die_info[i]["tmc"])["tmc_id"], 0 ) & 0x0fffffff

            full_devid >>= 32

            if (not parameters.has_key("no_devid_abort")) and (devid != expected_devid):
                raise sttp.STTPException("Error: die %d, devid 0x%08x, does not match expected 0x%08x" % (i, devid, expected_devid))

    # *** --- JPU Unlock (Master) called in Here --- for normal connection ***
    debug_print(jtag_debug,">>>> g4tap.complete_reset()")
    g4tap.complete_reset()
    debug_print(jtag_debug,"<<<< g4tap.complete_reset()")

    # Switch from TMCs to DBUs
    dbu.tmc2dbu(active_dbus)

    # Prevent the next reset from resetting the DBUs
    dbu.mask_reset()

    if masked_reset_callback:
        masked_reset_callback()

    # Handle any TargetString modepin overrides
    dbu.set_modepin_overrides(parameters)

    # We are now unlocked (if previously locked) : second system nReset leaves DBUs in chain because we have masked reset above
    # We need to stop the cores which have been running since reset was released just before unlocking

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    debug_print(reset_debug," >>>> masked reset")
    sttp.jtag.sequence({'nrst' : "0"})
    debug_print(reset_debug," >-> nrst set to 0 <-<")
    debug_print(reset_debug," -> reset_assert_period <-")
    sttp.stmc.delay(reset_assert_period)
    sttp.jtag.sequence({'nrst' : "1"})
    debug_print(reset_debug," >-> nrst set to 1 <-<")
    debug_print(reset_debug," -> post_reset_assert_period <-")
    sttp.stmc.delay(post_reset_assert_period)

    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    if parameters.has_key("on_emu"):
        sttp.stmc.delay(2000000)

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        tdf = g4tap.autodetect_chain(True)

        loop_count = 0
        while not (tdf[7:7+len(dbu_pattern)] == dbu_pattern):
            # sleep a while
            sttp.stmc.delay(post_reset_assert_period)
            loop_count = loop_count + 1
            if loop_count == 100:
                raise sttp.STTPException("autodetect chain: failed to get chained DBU pattern %s" % (dbu_pattern,) )
            tdf = g4tap.autodetect_chain()

    ## dbu.idcode()

    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    ## dbu.setup_chain(['dbu_sas'])

    # Show physical mode pin inputs and show/modify mode pin outputs
    dbu.display_modepins()

    debug_print(jtag_debug,"Chain for pokepeek operations")
    g4tap.autodetect_chain(True)

    dbu.idcode()
    dbu.setup_dbu_pokepeek(poke_peek_dbus)

    # JPU callback
    cb = sttp.targetpack.get_callback("post_connect")
    if cb:
        cb(connect_info)

    debug_print(jtag_debug,"<<<< g4soc.connect()")


def connect_secondary(parameters, dies_to_connect=None, activate_dbus=None):
    # Assume each die has an associated TMC
    # By this stage an initial poke-peek phase has released reset etc to the other dies to be connected

    # disable any poke peek in operation before we mod the chain
    e = sttp.pokepeek.isEnabled()
    if e:
        sttp.pokepeek.disable()

    die_indexes = dies_to_connect
    if None == die_indexes:
        die_indexes = []
        for i in range(len(dbu.tmc_bypass)):
            if dbu.tmc_bypass[i]:
                die_indexes.append(i)

    # Call connect_secondary callback to allow any unlock to be done
    cb = sttp.targetpack.get_callback("connect_secondary")
    if cb:
        cb(die_indexes)    

    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    dbu.tmc2dbu_secondary(die_indexes, activate_dbus)

    # Show physical mode pin inputs and show/modify mode pin outputs
    dbu.display_modepins()

    dbu.setup_dbu_pokepeek(poke_peek_dbus)

    debug_print(jtag_debug,"Chain for pokepeek operations")
    g4tap.autodetect_chain(True)

    if e:
        sttp.pokepeek.enable()

def complete_connect(parameters):

    global no_sys_reset

    if parameters.has_key("reset_only"):
        return

    debug_print(jtag_debug,">>> soc complete_connect")

    # diable any poke peek operation in before we mod the chain
    if sttp.pokepeek.isEnabled():
        sttp.pokepeek.disable()

    # Set up the chain control register

    coreName = sttp.targetinfo.get_targetstring_core()

    if parameters.has_key("active_cores"):
        chainList = parameters["active_cores"].split(":")
        if coreName not in chainList:
            chainList.append(coreName)
    else:
        chainList = [coreName,]


    errorChainList = list(chainList)

    dbu_set = {}
    dbu_list = []

    # Iter over dies (each with one or more dbus)
    for die in die_info:
        if die.has_key("dbu_info"):
            dbu_list += die["dbu_info"]
        else:
            dbu_list.append( die, )

    # Iter over dbus on the die
    for thedbu in dbu_list:

        # initialize an empty list value, keyed on the DBU name
        dbu_set[thedbu["name"]] = []

        for i in range(len(chainList)):
            c = chainList[i]
            if thedbu["chain_ctrl"].has_key(c):
                dbu_set[thedbu["name"]].append(c)
                errorChainList[i] = None

        # if we did not add any items remove it now
        if [] == dbu_set[thedbu["name"]]:
            del dbu_set[thedbu["name"]]

     # Setting the "mpe_die" bit in the SASGC1 - is it required?
    def enable_chained_dies(chainList):
         for die in die_info:
             if not die.has_key("chain_activate"):
                 continue
             for k, v in die["chain_activate"].iteritems():
                 for c in chainList:
                     if c in v["chain_ctrl"].keys():
                         # find the dbu with the die "avtivate" flag
                         for thedbu in dbu_list:
                             if thedbu["chain_ctrl"].has_key(k):
                                 if not dbu_set.has_key(thedbu["name"]):
                                     dbu_set[thedbu["name"]] = [k,]
                                 elif k not in dbu_set[thedbu["name"]]:
                                     dbu_set[thedbu["name"]].append(k)

    enable_chained_dies(chainList)
  
    for c in errorChainList:
        if c:
            raise sttp.STTPException("Invalid core %s in active_cores" % (c,))        

    dbu.setup_chain_items(0, dbu_set)

    dbu.setup_chain(chainList)
    debug_print(jtag_debug,"Chain for debug connect")

    g4tap.autodetect_chain(True)
    debug_print(jtag_debug,"<<< soc complete_connect")


def boot_a9(core, soft_reset_bitfield):
    # The "official" boot sequence from Martin Hummel, via Olivier Roulenq:
    # 1.     hard_reset_n = 0
    # 2.     pll_disable = 1 (mpe sysconf "a9 pll config" 0xFDDE00D8 set bit0 to '1' )
    # 3.     presetdbg_n = 0
    # 4.     presetdbg_n = 1  
    # 5.     halt core through DBGDSCR
    # 6.     pll_disable = 0 (0xFDDE00D8 set bit0 to '0' )
    # 7.     hard_reset_n = 1
    
    # Step 2 : pll_disable = 1
    #systemcfg.A9_PLL.DISABLE.write(1)
        
    # Step 3 & 4 : reset pulse on A9 coresight logic (presetdbg_n)
    debug_print(cores_debug,"  Pulse a9 presetdbg_n")

    dbu.set_cpu_debug_enable(True, (core + "_0",))
    sttp.stmc.delay(1000)
    dbu.set_cpu_debug_enable(False, (core + "_0",))

    arm_a9.boot_a9(core + "_0")
    arm_a9.boot_a9(core + "_1")
    sttp.stmc.delay(1000)   
        
    # Step 6 : pll_disable = 0
    #systemcfg.A9_PLL.DISABLE.write(0)
        
    debug_print(cores_debug,"  Releasing CA9 soft reset " )
    soft_reset_bitfield.write(1)
        
    arm_a9.check_a9(core + "_0")
    arm_a9.check_a9(core + "_1")


def boot_st40(core, soft_reset_bitfield):

    debug_print(cores_debug,"  Releasing %s from reset" % (core,))
    sttp.stmc.delay(100000)
    debug_print(cores_debug,"  # 2. assert asebrk ")
    dbu.set_cpu_debug_enable(True, (core,))
        
    sttp.stmc.delay(reset_assert_period)

    debug_print(cores_debug,"  # 3. assert trstn")
    dbu.set_trst(True, (core,))
        
    sttp.stmc.delay(tap_reset_assert_period)
        
    debug_print(cores_debug,"  # 4. deassert poweron reset ")
    soft_reset_bitfield.write(1)
    
    sttp.stmc.delay(reset_assert_period)
    debug_print(cores_debug,"  # 5. wait for sh4 reset completion ")
        
    sttp.stmc.delay(post_reset_assert_period)

    debug_print(cores_debug,"  # 6. deassert asebrk")
    dbu.set_cpu_debug_enable(False, (core,))
        
    sttp.stmc.delay(post_reset_assert_period)

    debug_print(cores_debug,"  # 7. deassert trstn")
    dbu.set_trst(False, (core,))
    
    sttp.stmc.delay(post_reset_assert_period)

def setup_st40s(parameters, st40_maps):
    
    coreList = []

    if parameters.has_key("active_cores"):
        coreList = parameters["active_cores"].split(":")
    coreList.append( sttp.targetinfo.get_targetstring_core() )
    
    st40_list = []
    for c in st40_maps.keys():
        if c in coreList:
            st40_list.append(c)

    if st40_list == []:
        # If the connect core is ST40 or it is in the active cores list setup the ST40
        return
    
    # If we are generating pokes for A9 ROM bootstrap do not try and and add ST40 core specific pokes
    if "ROMGEN" == sttp.stmc.get_operating_mode() and \
       sttp.targetinfo.get_targetstring_core() not in st40_list:
        return
    
    # default : enable se mode
    se_mode = "se"
    enable_se_mode = True

    se_mode_explicit = False
    se_pokes_only    = False

    if parameters.has_key("seuc"):
        se_mode_explicit = True
        if int(parameters["seuc"]) != 0:
            # seuc configuration explicitly requested
            se_mode = "seuc" 
            enable_se_mode = True
        else:
            enable_se_mode = False
        
    if parameters.has_key("se"):
        if se_mode_explicit:  
            raise sttp.STTPException("Cannot use TargetString option se=1 with seuc=1")
    
        se_mode_explicit = True
        if int(parameters["se"]) != 0:  
            # se configuration explicitly requested
            se_mode = "se"
            enable_se_mode = True
        else:
            enable_se_mode = False
        
    if parameters.has_key("se_pokes_only") and (int(parameters["se_pokes_only"]) == 1):
        if not se_mode_explicit:
            raise sttp.STTPException("Must use TargetString option se=1 or seuc=1 with se_pokes_only=1")
        enable_se_mode = True
        se_pokes_only  = True
            
    if (not se_pokes_only) and parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1):
        if se_mode_explicit:
            raise sttp.STTPException("Cannot use TargetString option no_pokes=1 with se or seuc: use se_pokes_only=1")
        enable_se_mode = False
    
    if enable_se_mode:
        for c in st40_list:
            sttp.pokepeek.enable_core(c)
            switch_on_se(se_mode, c, st40_maps[c])

            # Enable caches (except for romgen case where boot code must do this).
            # Note: We will not enable caches where <se_mode>=0 was a parameter
            #       either.
            if "ROMGEN" != sttp.stmc.get_operating_mode():
                cpu_root = sttp.targetpack.find_cpu(sttp.targetpack.get_tree(), c)
                ccn = cpu_root.parent.CCN.st40_ccn_regs
                ccn.CCN_CCR.poke(0x8000090d)


pmb_id = {}

# ST40 PMB setup
def set_pmb(st40_core, virt_addr, phys_addr, size, cached):
                
    global pmb_id
        
    # Convention is to map 
    #  LMI0 => P1 => 0x80
    #  LMI1 => P2 => 0xA0
    # limited to 512MB per LMI 
    if size > 512:
        size = 512 
    while size > 0:
        if size < 64:
            inc = 16
        elif size < 128:
            inc = 64
        elif size < 512:
            inc = 128
        else:
            inc = 512

        if cached == 1:
            if cores_debug:
                sttp.logging.print_out(" -> Cached PMB: %u, 0x%x, 0x%x, %u" % (pmb_id[st40_core], virt_addr, phys_addr, inc))
            st40_pmb_regs.st40_set_pmb(pmb_id[st40_core], virt_addr, phys_addr, inc)
        else:
            if cores_debug:
                sttp.logging.print_out(" -> Uncached PMB: %u, 0x%x, 0x%x, %u, 0, 0, 1" % (pmb_id[st40_core], virt_addr, phys_addr, inc))
            st40_pmb_regs.st40_set_pmb(pmb_id[st40_core], virt_addr, phys_addr, inc, 0, 0, 1) # Uncached
        virt_addr += (inc >> 4)
        phys_addr += (inc >> 4)
        size -= inc
        pmb_id[st40_core] += 1


# Switching to 32-bits mode
def switch_on_se(mode, st40_core, virt_phys_list):
    
    # Configure the PMBs
    st40_pmb_regs.st40_pmb_control_initialise(st40_core)

    st40_pmb_regs.st40_clear_all_pmbs()

    pmb_id[st40_core] = 0    

    if mode == "se":   
        sttp.logging.print_out("Enabling SE (32 bit) mode on ST40 core");
        for i in virt_phys_list:
            set_pmb(st40_core, i["virtual"], i["physical"], i["size"], 1)
    elif mode == "seuc":
        sttp.logging.print_out("Enabling SEUC (32 bit) mode on ST40 core");
        for i in virt_phys_list:
            set_pmb(st40_core, i["virtual"], i["physical"], i["size"], 0)
    else:
        raise "ERROR: switch_on_se() called without a valid mode"
    
    # Switch to 32-bit SE mode
    st40_pmb_regs.st40_enhanced_mode(1, st40_core)

def debug_print(debug, s):

    if debug:
        sttp.logging.print_out(s)

