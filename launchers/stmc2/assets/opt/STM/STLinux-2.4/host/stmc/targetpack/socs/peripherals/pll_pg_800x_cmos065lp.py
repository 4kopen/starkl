""" pll_pg_800x_cmos065lp """

__f_ref = 27000  # 27MHz
__f_min = 200000 # 200MHz
__f_max = 800000 # 800MHz

def setup(ref=27):
    """
    Setup the frequency of reference clock(MHz), the default value is 27MHz
    """
    global __f_ref
    
    __f_ref = ref
    
    return

  
def check(freq):
    """
    Return True if freq(MHz) is in range [200MHz, 800MHz]
    """
    global __f_in
    global __f_max
    if (freq < __f_min) or (freq > __f_max):
        return False
    else:
        return True


def transfer(config0, config1):
    """
    Get the PLL frequency(KHz) with the input register config0 and config1
    """
    m = config0 & 0xFF
    n = (config0 >> 8) & 0xFF
    p = config1 & 0x7
    
    return fout(m, n, p)

def fout(m, n, p):
    """
    fout = 2 * n * __f_ref / (m * pow(2, p))
    """
    return 2 * n * __f_ref * 1.0 / (m * (1 << p) * 1.0)


def calculate(f):
    """
    Calculate M, N (P = 0) for the appointed frequency -- f
    return (f_out, m, n, p)
    """
    global __f_ref
    global __f_max
  
    freq = f * 1.0
    f_out = 0
    m = 0
    n = 0
    p = 0
    tmp = __f_max * 1.0 # the max freq
    m_save = 0
    n_save = 0
    p_save = 0
    
    for m in range(1, 256, 1):
        for n in range(1, 256, 1):
            for p in (0, 6, 1):
                f_out = 2 * n * __f_ref * 1.0 / (m * (1 << p) * 1.0)
                if f_out == freq:
                    return (freq, m, n, p)
                else:
                    if f_out > freq:
                        if (f_out - freq) < tmp:
                          m_save = m
                          n_save = n
                          p_save = p
                          tmp = f_out - freq
                    else:
                      if (freq - f_out) < tmp:
                          m_save = m
                          n_save = n
                          p_save = p
                          tmp = freq - f_out
  
    return (fout(m_save, n_save, p_save), m_save, n_save, p_save)


if __name__ == "__main__":
    setup(27000)
    freq = 700000 # 700MHz
    if not check(freq):
        print("Invalid frequency = %dMHz" % freq)
    f, m, n, p = calculate(freq)
    print(freq, f, m, n, p)
    print("want %.3f, get %.3f, m=0x08%x, n=0x08%x, p=0x08%x" % (freq, f, m, n, p))
