import sttp
import sttp.logging
import sttp.pokepeek
import sttp.stmc
import sttp.targetpack
import arm_a9
import convertor
import dbu
import g4soc
import g4tap
import clk_common2
import stxh410_uart
import stxh410_pio
import flexgen

# Global variables
global sttp_root
sttp_root = sttp.targetpack.get_tree()

# SOC parameters default values
clocks_debug = 0    # Clocks debug. 0=disable
cores_debug = 0     # Cores debug. 0=disable
ddr_debug = 0       # DDR debug. 0=disable
osc_freq = 30       # Onboard ref oscillator = 30Mhz
on_emu = 0          # If set, indicate that we are running on emulator (=no PLL), do not modify value here, but in board targetpack or in command line
crypto_en = 0       # If set, means CC is the booting core
uart_debug = 0      # UART debug. 0=disable. Allows TP execution tracing.
ONEMEG = 1000000    # Constant for Mhz
mixer_bypass = 0    # Set to 1 to bypass DDR-SS mixer (debug only)

chain_ctrl_mappings = {
    "ext_die"         : 1 << 0,
    "dbu"             : 1 << 1,
    "xp70_lpm"        : 1 << 2,
    "switchable"      : 1 << 3,
}

chain_ctrl_mappings_sa = {
    "dbu_sa"        : 1 << 0,
    "video"         : 1 << 1,
    "hades_dtu"     : 1 << 2,
    "hades_fc"      : 1 << 2,
    "hades_pe0"     : 1 << 2,
    "hades_pe1"     : 1 << 2,
    "gp1"           : 1 << 3,
    "a9"            : 1 << 4,
    "a9_0"          : 1 << 4,
    "a9_1"          : 1 << 4,
    "gp0"           : 1 << 5,
    "audio"         : 1 << 6,
    "xp70_vdp"      : 1 << 7,
    "xp70_tango_sc" : 1 << 8,
    "xp70_tango_tp" : 1 << 9,
}


# stxh410 boot mode is defined on mode[6:2]
ref_clock_descriptions =  [
        "Internal",
        "External",
]

boot_master_descriptions =  [
        "A9",
        "ST231",
]

boot_dev_descriptions =  [
        "0",
        "1",
]

reserved_descriptions =  [
        "Reserved",
        "Reserved",
]

boot_dev_voltage_descriptions =  [
        "3V3",
        "1V8",
]

fpga_controlbit_descriptions =  [
        "0",
        "1",
]

stxh410_modepins = {
    "mode0_ref_clock" :
                    { "alt_name"     : "mode0",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["INTERNAL", "EXTERNAL"],
                      "values"       : [0, 1],
                      "descriptions" : ref_clock_descriptions,
                    },

    "mode1_boot_master" :
                    { "alt_name"     : "mode1",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["A9", "ST231"],
                      "values"       : [0, 1],
                      "descriptions" : boot_master_descriptions,
                    },

    "mode2_boot_dev0" :
                    { "alt_name"     : "mode2",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["0", "1"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_descriptions,
                    },

    "mode3_boot_dev1" :
                    { "alt_name"     : "mode3",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["0", "1"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_descriptions,
                    },

    "mode4_boot_dev2" :
                    { "alt_name"     : "mode4",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["0", "1"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_descriptions,
                    },

    "mode5_boot_dev3" :
                    { "alt_name"     : "mode5",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["0", "1"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_descriptions,
                    },

    "mode6_boot_dev4" :
                    { "alt_name"     : "mode6",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["0", "1"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_descriptions,
                    },

    "mode7_reserved" :
                    { "alt_name"     : "mode7",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Reserved", "Reserved"],
                      "values"       : [0, 1],
                      "descriptions" : reserved_descriptions,
                    },

    "mode8_boot_dev_volt" :
                    { "alt_name"     : "mode8",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["3V3", "1V8"],
                      "values"       : [0, 1],
                      "descriptions" : boot_dev_voltage_descriptions,
                    },

    "mode9_reserved" :
                    { "alt_name"     : "mode9",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Reserved", "Reserved"],
                      "values"       : [0, 1],
                      "descriptions" : reserved_descriptions,
                    },

    "mode10_reserved" :
                    { "alt_name"     : "mode10",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Reserved", "Reserved"],
                      "values"       : [0, 1],
                      "descriptions" : reserved_descriptions,
                    },

    "mode11_reserved" :
                    { "alt_name"     : "mode11",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Reserved", "Reserved"],
                      "values"       : [0, 1],
                      "descriptions" : reserved_descriptions,
                    },

    "fpga_controlbit0" :
                    { "alt_name"     : "fpga_control0",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Low", "High"],
                      "values"       : [0, 1],
                      "descriptions" : fpga_controlbit_descriptions,
                    },

    "fpga_controlbit1" :
                    { "alt_name"     : "fpga_control1",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Low", "High"],
                      "values"       : [0, 1],
                      "descriptions" : fpga_controlbit_descriptions,
                    },

    "fpga_controlbit2" :
                    { "alt_name"     : "fpga_control2",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Low", "High"],
                      "values"       : [0, 1],
                      "descriptions" : fpga_controlbit_descriptions,
                    },

    "fpga_controlbit3" :
                    { "alt_name"     : "fpga_control3",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Low", "High"],
                      "values"       : [0, 1],
                      "descriptions" : fpga_controlbit_descriptions,
                    },

    "fpga_controlbit4" :
                    { "alt_name"     : "fpga_control4",
                      "width"        : 1,
                      "register"     : -1,
                      "offset"       : -1,
                      "sym_values"   : ["Low", "High"],
                      "values"       : [0, 1],
                      "descriptions" : fpga_controlbit_descriptions,
                    },
}

stxh410_modepins_sa = {
}
#define DBU_SA_ID  0x18400d02
#define DBU_SBC_ID 0x24103d02

stxh410_info = {
    "tmc"        : "tmc_stxh410",
    "dbu_info"   : [ # Always on DBU
                     {
                       "chain_ctrl" : chain_ctrl_mappings,
                       "dbu_id"     : 0x24103d02,
                       "index"      : 1,
                       "modepins"   : stxh410_modepins,
                       "name"       : "dbu",
                     },
                     # Switchable DBU
                     {
                       "chain_ctrl" : chain_ctrl_mappings_sa,
                       "dbu_id"     : 0x18400d02,
                       "index"      : 0,
                       "modepins"   : stxh410_modepins_sa,
                       "name"       : "dbu_sa",
                     },
                   ]
}

# Flexiclockgens declaration
clocks_general = {
    "debug" : 0,            # 1=debug ON
    "osc"   : 30,
    "on_emu": 0,            # 1=on emulator
}
clkgenA0 = {
    "name"   : "A0",         # clockgen name
    "base"   : 0x90ff000,    # base addr
    "gfgs"   :
        [
            {"type"      : "PLL3200",
             "spreadtype": "down",
             "fmod"      : 30,
             "md"        : 1
            }
        ],
    "outputs": ["CLK_IC_LMI0"],
    "nominal": [ 466000000 ]
}
clkgenC0 = {
    "name"   : "C0",         # clockgen name
    "base"   : 0x9103000,    # base addr
    "outputs": ["CLK_ICN_GPU", "CLK_FDMA", "CLK_NAND", "CLK_HVA", "CLK_PROC_STFE",
                "CLK_TP", "CLK_RX_ICN_DMU", "CLK_RX_ICN_HVA", "CLK_ICN_CPU",
                "CLK_TX_ICN_DMU", "CLK_MMC_0", "CLK_MMC_1", "CLK_JPEGDEC",
                "CLK_EXT2F_A9", "CLK_IC_BDISP_0", "CLK_IC_BDISP_1", "CLK_PP_DMU",
                "CLK_VID_DMU", "CLK_DSS_LPC", "CLK_ST231_AUD_0", "CLK_ST231_GP_1",
                "CLK_ST231_DMU", "CLK_ICN_LMI", "CLK_TX_ICN_DISP_0", "CLK_ICN_SBC",
                "CLK_STFE_FRC2", "CLK_ETH_PHY", "CLK_ETH_PHYREF", "CLK_FLASH_PROMIP",
                "CLK_MAIN_DISP", "CLK_AUX_DISP", "CLK_COMPO_DVP", "CLK_TX_ICN_HADES",
                "CLK_RX_ICN_HADES", "CLK_ICN_REG_16", "CLK_PP_HADES", "CLK_CLUST_HADES",
                "CLK_HWPE_HADES", "CLK_FC_HADES"],
    "nominal": [ 355000000, 400000000, 266000000, 300000000, 225000000, 400000000,
                 450000000, 450000000, 533000000, 333000000, 200000000, 50000000,
                 333000000, 200000000, 400000000, 400000000, 266000000, 533000000,
                 36864, 600000000, 600000000, 600000000, 600000000, 300000000, 200000000,
                 50000000, 125000000, 125000000, 100000000, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0 ]
}

def connect(targetstring_parameters):

    global cores_debug, clocks_debug, uart_debug, ddr_debug
    global parameters, ddr_mbps, ca9_freq
    global on_emu, crypto_en, mixer_bypass
    global corecfg, ckga0, ckgc0

    parameters = targetstring_parameters

    # Parameters analysis
    if parameters.has_key("debug"):
        debugList = parameters["debug"].split(":")
        if "uart" in debugList: # UART debug is allowed from flash
            uart_debug = 1
        if sttp.stmc.get_operating_mode() != "ROMGEN":
            if "cores" in debugList:
                cores_debug = 1
            if "clocks" in debugList:
                clocks_debug = 1
            if "ddr" in debugList:
                ddr_debug = 1

    if parameters.has_key("on_emu"):
        on_emu = int(parameters["on_emu"])
    if parameters.has_key("crypto_en"):
        crypto_en = int(parameters["crypto_en"])
    if parameters.has_key("mixer_bypass"):
        mixer_bypass = int(parameters["mixer_bypass"])
    if parameters.has_key("lmi_freq"):
        ddr_mbps = int(parameters["lmi_freq"])
    else:
        raise sttp.STTPException("'lmi_freq' not set by '<board>.py'")
    if (not parameters.has_key("ca9_freq")):
        if (not parameters.has_key("a9_freq_c1")):
            raise sttp.STTPException("'a9_freq_c1' not set by '<board>.py'")
        if (not parameters.has_key("a9_freq_c2")):
            raise sttp.STTPException("'a9_freq_c2' not set by '<board>.py'")

    corecfg = sttp_root.stxh410.core_sysconf_regs.sysconf_regs
    ckga0 = sttp_root.stxh410.CLOCKGENA_0.clockgen_regs
    ckgc0 = sttp_root.stxh410.CLOCKGENC_0.clockgen_regs

    # This defines the bit which must be set in the DBU SBC if any cores in the DBU SA are used
    stxh410_info["chain_activate"] = { "switchable" : stxh410_info["dbu_info"][1] }

    die_info = [ stxh410_info ]

    connect_info = {
        "active_dbus"        : ["dbu",],
        "cti_callback"       : define_ctis,
        "dbu_pattern"        : g4soc.DBU_IR_PATTERN,
        "die_info"           : die_info,
        "poke_peek_dbus"     : ["dbu"],
        "reset_tmc_pattern"  : g4soc.TMC_IR_PATTERN,
    }

    # Unlocking (of 1st JPU) is done inside here
    g4soc.connect(parameters, connect_info)

    g4tap.autodetect_chain(True)

    if (parameters.has_key("reset_only") and (int(parameters["reset_only"]) == 1)):
        sttp.logging.print_out("Board reset ONLY ... exiting\n")
        return

    # We must activate the other DBU at this stage so we can setup registers
    # in it which affect/control the final cores to be used
    # A real hack because there is only 1 DBU in the chain
    # but the following causes STMC 2012.2.1 to put out data for a chain of 2.
    # The dbu index values setup makes the order right for this crucial step

    dbu.setup_chain_items(0, {"dbu": ("switchable", "dbu") })
    dbu.setup_chain_items(0, {"dbu": ("switchable", "dbu"), "dbu_sa" : ("dbu_sa",) })

    g4tap.autodetect_chain(True)
    dbu.idcode()
    dbu.setup_dbu_pokepeek(["dbu", "dbu_sa"])

    dbu.set_active(("dbu",))

def complete_connect(parameters):

    if sttp.pokepeek.isEnabled():
        sttp.pokepeek.disable()
    dbu.set_active(("dbu", "dbu_sa"))

    g4soc.complete_connect(parameters)

    # Hades configuration
    coreList = []
    if parameters.has_key("active_cores"):
        coreList = parameters["active_cores"].split(":")
    coreList.append( sttp.targetinfo.get_targetstring_core() )

    # Code to select all the Processing Elements

    if "hades_fc" in coreList or "hades_dtu" in coreList or "hades_pe0" in coreList or "hades_pe1" in coreList:
        def create_seq():
            return { 'tms' : "", 'td_to_target': "" }

        def append_rti(seq, count):
            seq['tms']          += '0' * count
            seq['td_to_target'] += "0" * count
            return seq

        dtu_p = sttp.stmc.get_core_params("hades_dtu")

        PEs = 2

        ir_bits_before = int(dtu_p["ir_bits_before"]) - 5*PEs  # -10 for the pe0 and pe1
        taps_before    = int(dtu_p["taps_before"]) - PEs       # -2  for the pe0 and pe1

        fc_p = sttp.stmc.get_core_params("hades_fc")

        ir_bits_after = int(fc_p["ir_bits_after"])
        taps_after    = int(fc_p["taps_after"])

        # Check the devids

        import tap
        import tap.jtag

        # All cores before and after get IR all 1s (bypass) and DR of 1
        irv = [0xffffffffffffffff,   6,  4, 0xffffffffffffffff]
        irl = [ir_bits_before,       6,  5, ir_bits_after]
        drv = [0xffffffff,           0,  0, 0xffffffff]
        drl = [taps_before,         32, 40, taps_after]

        tap.jtag.shift_ir_set(irv, irl)
        devids = tap.jtag.shift_dr_set(drv, drl)

        devid_dtu = devids[1]
        devid_fc  = devids[2]

        sttp.logging.print_out("Devid Hades dtu: 0x%08x\n" % (devid_dtu,))

        sttp.logging.print_out("Devid Hades fc: 0x%08x\n" % (devid_fc,))

        g4tap.autodetect_chain(True)


        # Set all PEs INCLUDED - must put the FC in BYPASS
        # Use the ALL_INCLUDED instruction
        DTU_IR = 0x3b
        irv = [0xffffffffffffffff, DTU_IR, 0xff, 0xffffffffffffffff]
        irl = [ir_bits_before,          6,    5, ir_bits_after]
        drv = [0xffffffff,              0,    0, 0xffffffff]
        drl = [taps_before,             0,    1, taps_after]

        tap.jtag.shift_ir_set(irv, irl)
        tap.jtag.shift_dr_set(drv, drl)

        # Some extra RTIs needed to sync all Hades TAPs
        vals2 = create_seq()
        vals2 = append_rti(vals2, 20)
        inputs = sttp.jtag.sequence( vals2 )

        # Bypass trial
        irv = [0xffffffffffffffff,   0xff, 0xff, 0xffffffffffffffff]
        irl = [ir_bits_before,          6,    5, ir_bits_after]
        drv = [0xffffffff,              0,    0, 0xffffffff]
        drl = [taps_before,             1,    1, taps_after]

        tap.jtag.shift_ir_set(irv, irl)
        tap.jtag.shift_dr_set(drv, drl)

        sttp.logging.print_out("Chain after including all Hades PEs")
        g4tap.autodetect_chain(True)

def _boot_a9(core, soft_reset_bitfield):
    # The "official" boot sequence from Martin Hummel, via Olivier Roulenq:
    # 1.     hard_reset_n = 0
    # 2.     pll_disable = 1 (mpe sysconf "a9 pll config" 0xFDDE00D8 set bit0 to '1' )
    # 3.     presetdbg_n = 0
    # 4.     presetdbg_n = 1
    # 5.     halt core through DBGDSCR
    # 6.     pll_disable = 0 (0xFDDE00D8 set bit0 to '0' )
    # 7.     hard_reset_n = 1
    # Step 2 : pll_disable = 1
    #systemcfg.A9_PLL.DISABLE.write(1)

    # Step 3 & 4 : reset pulse on A9 coresight logic (presetdbg_n)
    debug_print(cores_debug,"  Pulse a9 presetdbg_n")

    dbu.set_cpu_debug_enable(True, (core + "_0",))
    sttp.stmc.delay(1000)
    dbu.set_cpu_debug_enable(False, (core + "_0",))

    # dbu.set_cpu_debug_enable etc calls dbu.set_all_active - need to undo this
    dbu.set_active(("dbu",))

    arm_a9.boot_a9(core + "_0")
    arm_a9.boot_a9(core + "_1")
    sttp.stmc.delay(1000)

    # Step 6 : pll_disable = 0
    #systemcfg.A9_PLL.DISABLE.write(0)
    debug_print(cores_debug,"  Releasing CA9 soft reset " )
    soft_reset_bitfield.write(1)

    arm_a9.check_a9(core + "_0")
    arm_a9.check_a9(core + "_1")

def boot_companions(parameters):

    if parameters.has_key("reset_only"):
        debug_print(cores_debug, "Skipping boot_companions() : reset_only")
        return

    if g4soc.no_sys_reset:
        debug_print(cores_debug, "Skipping boot_companions() : no_sys_reset")
        return

    coreList = []
    if parameters.has_key("active_cores"):
        coreList = parameters["active_cores"].split(":")
    coreList.append(sttp.targetinfo.get_targetstring_core())
    debug_print(cores_debug, "Starting cores ... %s" % coreList)

    # may be called with "no_pokes=1" and we need to enable pokepeek mode
    if not sttp.pokepeek.isEnabled():
        sttp.pokepeek.enable()

    # Check we can read the DBU SAS
    dbu_base = sttp_root.stxh410.dbu.parameters["baseAddress"]
    dbu_id_add  = (int(dbu_base,0)+0x08)
    dbu_id_read = sttp.pokepeek.peek(dbu_id_add)
    debug_print(cores_debug, "  dbu value = 0x%08x" % dbu_id_read)
    dbu_id_expected = stxh410_info["dbu_info"][0]["dbu_id"]
    if dbu_id_expected != dbu_id_read:
        raise sttp.STTPException("  Cannot read stxh410 DBU registers - something wrong. Expected 0x%08x, got 0x%08x (from @ 0x%08x)" % (dbu_id_expected, dbu_id_read, dbu_id_add))

    # To be sure that a9 are hold under reset [2] a9_wdg [1] a9_hardreset
    corecfg.CORE_SOFT_RESET.A9_HARDRESET_SOFT_RST_N.write(0)
    corecfg.CORE_SOFT_RESET.A9_WDG_SOFT_RST_N.write(1)

    # Configure A9 to boot at 0xFFFF0000
    corecfg.SYSTEM_CONFIG5104.or_const(1)

    # TAP reset to all cores
    debug_print(cores_debug,"  Resetting all cores (assert trstn)")

    all_cores = g4tap.get_all_cores()

    dbu.set_trst(True, all_cores)
    sttp.stmc.delay(g4soc.tap_reset_assert_period)
    dbu.set_trst(False, all_cores)
    sttp.stmc.delay(g4soc.tap_post_reset_assert_period)

##    dbu.set_active("dbu_sas")

    if "xp70_lpm" in coreList:
        # Do nothing
        pass

    if "xp70_vdp" in coreList:
        vdp = sttp_root.stxh410.vdp.vip
        debug_print(cores_debug,"  Enabling xp70_vip fetch")
        vdp.CORE_RUN_CONFIG.ENABLE.write(1)

    for c in ("xp70_tango_tp", "xp70_tango_sc"):
        if c not in coreList:
            continue
        debug_print(cores_debug, "  Enable fetch %s" % c)
        tango_tp = sttp_root.stxh410.tango.tango_tp
        tango_sc = sttp_root.stxh410.tango.tango_sc
        n = c.replace("xp70_", "")
        tango_core = eval("sttp_root.stxh410.tango." + n)
        tango_core.FETCH.ENABLE.write(1)
        #sttp.logging.print_out("Done fetch enable to %s" % ( tango_core.FETCH.ENABLE, ) )

    for c in ("gp1", "gp0", "audio", "video"):
        if c not in coreList:
            continue
        debug_print(cores_debug,"  Releasing %s from reset" % c)
        dbu.set_cpu_debug_enable(True, c)

        if "gp1" == c:
            field = "ST231_GP_11_SOFT_RESET_N"
            s = eval("corecfg.CORE_SOFT_RESET." + field)
        else:
            if "gp0" == c:
                field = "ST231_GP_10_SOFT_RESET_N"
            elif "audio" == c:
                field = "ST231_AUD_SOFT_RESET_N"
            elif "video" == c:
                field = "ST231_DMU_SOFT_RESET_N"
            else:
                raise sttp.STTPException("Unknown %s LX core" % c)
            s = eval("corecfg.RESETGEN_CONF0_1." + field)
        s.write(1)

    if "a9" in coreList or "a9_0" in coreList or "a9_1" in coreList:
        _boot_a9("a9", corecfg.CORE_SOFT_RESET.A9_HARDRESET_SOFT_RST_N)

    debug_print(cores_debug," => boot_companions() completed")

def debug_print(debug, s):

    if debug:
        sttp.logging.print_out(s)

# Clocks configuration
def configure_clocks(parameters):

    global ddr_mbps

    sttp.logging.print_out("Configuring clocks ...")
    if uart_debug:
        stxh410_uart.write("TP: Configuring clocks\n\r")

    # Lib flexiclockgen init
    if (clocks_debug):
        clocks_general["debug"] = 1
    clocks_general["on_emu"] = on_emu
    clocks_general["osc"] = osc_freq
    flexgen.init(clocks_general)

    # Clocks sources
    # Note: OFF is set with div=0
    src_pll0 = 0
    src_pll1 = 1
    src_fs0 = 2
    src_fs1 = 3
    src_fs2 = 4
    src_fs3 = 5
    src_osc = 6

    # Clockgen A0
    debug_print(clocks_debug,"  Configuring A0")
    if uart_debug:
        stxh410_uart.write("TP:   Configuring A0\n\r")
    flexgen.set_pll3200(clkgenA0, 0, ((ddr_mbps / 2) * ONEMEG))

    flexgen.set_div(clkgenA0, 0, src_pll0, 2)      # CLK_IC_LMI = 466Mhz

    # Clockgen C0
    debug_print(clocks_debug,"  Configuring C0")
    if uart_debug:
        stxh410_uart.write("TP:   Configuring C0-PLL0\n\r")
    flexgen.set_pll3200(clkgenC0, 0, 1066000000)
    if uart_debug:
        stxh410_uart.write("TP:   Configuring C0-PLL1\n\r")
    flexgen.set_pll3200(clkgenC0, 1, 1200000000)
    if uart_debug:
        stxh410_uart.write("TP:   Configuring C0-FS\n\r")
    vco = 660 * ONEMEG
    flexgen.set_fs660_vco(clkgenC0, 2, vco)
    flexgen.set_fs660(clkgenC0, 2, vco, 0, 659000000) # 660Mhz looks KO at present
    flexgen.set_fs660(clkgenC0, 2, vco, 1, 450000000)
    flexgen.set_fs660(clkgenC0, 2, vco, 2, 737280) # 36864 * 20
    flexgen.set_fs660(clkgenC0, 2, vco, 3, 125000000)

    if uart_debug:
        stxh410_uart.write("TP:   Configuring C0\n\r")
    flexgen.set_div(clkgenC0, 0, src_pll0, 3)      # CLK_ICN_GPU = 355.3Mhz
    flexgen.set_div(clkgenC0, 1, src_pll1, 3)      # CLK_FDMA = 400Mhz
    flexgen.set_div(clkgenC0, 2, src_pll0, 4)      # CLK_NAND = 266.5Mhz
    flexgen.set_div(clkgenC0, 3, src_pll1, 4)      # CLK_HVA = 300Mhz
    flexgen.set_div(clkgenC0, 4, src_fs1,  2)      # CLK_PROC_STFE = 225Mhz
    flexgen.set_div(clkgenC0, 5, src_pll1, 3)      # CLK_TP = 400Mhz
    flexgen.set_div(clkgenC0, 6, src_fs1,  1)      # CLK_RX_ICN_DMU = 450Mhz
    flexgen.set_div(clkgenC0, 7, src_fs1,  1)      # CLK_RX_ICN_HVA = 450Mhz
    flexgen.set_div(clkgenC0, 8, src_pll0, 2)      # CLK_ICN_CPU = 533Mhz
    flexgen.set_div(clkgenC0, 9, src_fs0,  2)      # CLK_TX_ICN_DMU = 330Mhz
    flexgen.set_div(clkgenC0, 10, src_pll1, 24)    # CLK_MMC_0 = 50Mhz
    flexgen.set_div(clkgenC0, 11, src_pll1, 24)    # CLK_MMC_1 = 50Mhz
    flexgen.set_div(clkgenC0, 12, src_fs0,  2)     # CLK_JPEGDEC = 330Mhz
    flexgen.set_div(clkgenC0, 13, src_pll1, 6)     # CLK_EXT2F_A9 = 200Mhz
    flexgen.set_div(clkgenC0, 14, src_pll1, 3)     # CLK_IC_BDISP_0 = 400Mhz
    flexgen.set_div(clkgenC0, 15, src_pll1, 3)     # CLK_IC_BDISP_1 = 400Mhz
    flexgen.set_div(clkgenC0, 16, src_pll0, 4)     # CLK_PP_DMU = 266.5Mhz
    if parameters.has_key("profile") and (int(parameters["profile"]) == 2):
        flexgen.set_div(clkgenC0, 17, src_fs0, 1)     # CLK_VID_DMU = 659Mhz
    else:
        flexgen.set_div(clkgenC0, 17, src_pll0, 2)     # CLK_VID_DMU = 533Mhz
    flexgen.set_div(clkgenC0, 18, src_fs2,  20)    # CLK_DSS_LPC = 36.864Khz
    flexgen.set_div(clkgenC0, 19, src_pll1, 2)     # CLK_ST231_AUD_0 = 600Mhz
    flexgen.set_div(clkgenC0, 20, src_pll1, 2)     # CLK_ST231_GP_1 = 600Mhz
    flexgen.set_div(clkgenC0, 21, src_pll1, 2)     # CLK_ST231_DMU = 600Mhz
    flexgen.set_div(clkgenC0, 22, src_pll1, 2)     # CLK_ICN_LMI = 600Mhz
    flexgen.set_div(clkgenC0, 23, src_pll1, 4)     # CLK_TX_ICN_DISP_0 = 300Mhz
    flexgen.set_div(clkgenC0, 24, src_pll1, 6)     # CLK_ICN_SBC = 200Mhz
    flexgen.set_div(clkgenC0, 25, src_pll1, 24)    # CLK_STFE_FRC2 = 50Mhz
    flexgen.set_div(clkgenC0, 26, src_fs3,  1)     # CLK_ETH_PHY = 125Mhz
    flexgen.set_div(clkgenC0, 27, src_fs3,  1)     # CLK_ETH_PHYREF = 125Mhz
    flexgen.set_div(clkgenC0, 28, src_pll1, 12)    # CLK_FLASH_PROMIP = 100Mhz
    flexgen.set_div(clkgenC0, 29, src_osc, 1)      # CLK_MAIN_DISP
    flexgen.set_div(clkgenC0, 30, src_osc, 0)      # CLK_AUX_DISP
    flexgen.set_div(clkgenC0, 31, src_osc, 0)      # CLK_COMPO_DVP
    flexgen.set_div(clkgenC0, 32, src_fs0,  2)     # CLK_TX_ICN_HADES = 330Mhz
    flexgen.set_div(clkgenC0, 33, src_fs1,  1)     # CLK_RX_ICN_HADES = 450Mhz
    flexgen.set_div(clkgenC0, 35, src_pll1, 5)     # CLK_PP_HADES = 240 MHz
    flexgen.set_div(clkgenC0, 36, src_pll1, 3)     # CLK_CLUST_HADES = 400Mhz
    flexgen.set_div(clkgenC0, 38, src_pll1, 6)     # CLK_FC_HADES = 200Mhz
    if corecfg.SYSTEM_STATUS5568.if_eq(0xf << 28, 0): # cut1
        flexgen.set_div(clkgenC0, 34, src_pll1, 24)    # CLK_ICN_REG_16 = 50MHz # W/A for PP hang issue
        if parameters.has_key("hades_overclk") and (int(parameters["hades_overclk"]) == 1):
            flexgen.set_div(clkgenC0, 37, src_pll1, 3)     # CLK_HWPE_HADES = 400Mhz # enables UHDp30
        else:
            flexgen.set_div(clkgenC0, 37, src_pll1, 4)     # CLK_HWPE_HADES = 300Mhz
    else: # cut2
        flexgen.set_div(clkgenC0, 34, src_pll1, 6)        # CLK_ICN_REG_16 = 200MHz
        if parameters.has_key("profile") and (int(parameters["profile"]) == 0):
            flexgen.set_div(clkgenC0, 37, src_pll1, 6)    # CLK_HWPE_HADES = 200Mhz
        elif parameters.has_key("profile") and (int(parameters["profile"]) == 2):
            flexgen.set_div(clkgenC0, 37, src_pll1, 3)    # CLK_HWPE_HADES = 400Mhz (note: for ddr_mbps <= 1600, CLK_HWPE_HADES has to be set to 300Mhz)
        else: #profile = 1
            flexgen.set_div(clkgenC0, 37, src_pll1, 4)    # CLK_HWPE_HADES = 300Mhz

    # Configuring CA9 clock
    # The A9 clock is defined
    #  - thru 'ca9_freq' cmd line param
    # or be set by '<board>.py'
    #  - thru 'a9_freq_c1' param if cut1
    #  - thru 'a9_freq_c2' param if cut2 and followings
    if parameters.has_key("ca9_freq"):
        clk_ca9 = int(parameters["ca9_freq"])
        ca9_set_freq(clk_ca9)
    else:
        # If no param (JTAG mode) checking device cut
        if corecfg.SYSTEM_STATUS5568.if_eq(0xf << 28, 0): # cut1
            clk_ca9 = int(parameters["a9_freq_c1"])
            ca9_set_freq(clk_ca9)
        else:
            clk_ca9 = int(parameters["a9_freq_c2"])
            ca9_set_freq(clk_ca9)
    debug_print(clocks_debug,"  Configured CA9 PLL for %dMhz" % clk_ca9)
    if uart_debug:
        stxh410_uart.write("TP:   Configured CA9 PLL for @ %dMhz\n\r" % clk_ca9)

    # Update proper delay scaling based on crypto or CA9 boot
    if (on_emu == 0): # Some issues with delay scaking on emulator
        if (crypto_en == 1):
            sttp.pokepeek.rom_delay_scale(50)
        else:
            sttp.pokepeek.rom_delay_scale(clk_ca9)

# Enable SSCG on DDR clock
def sscg_enable_ddr(enable):
    flexgen.enable_sscg(clkgenA0, 0, enable)

# Clockgen CA9 setup function
def ca9_set_freq(freq):

    # WARNING: before calling this function the user
    #          must be sure that PLL is not used (bypassed).

    # Computing parameters: min PLL3200 freq is 800Mhz, then using ODF
    if freq < 800:
        odf = 800 / freq
        if 800 % freq:
            odf = odf + 1
    else:
        odf = 1
    vcoby2 = freq * odf
    err, idf, ndiv, cp = clk_common2.pll3200c32_get_params(osc_freq * 1000000,vcoby2 * 1000000)
    if clocks_debug:
        sttp.logging.print_out("    in=%uMhz requested=%uMhz => vcoby2=%uMhz, odf=%u" % (osc_freq, freq, vcoby2, odf))
        sttp.logging.print_out("    CA9 PLL3200 params: err=%d, idf=%u, ndiv=%u, cp=%u" % (err,idf,ndiv,cp))
    if (err != 0):
        raise sttp.STTPException("Invalid frequency requested for CA9 PLL3200")

    # Updating HW
    corecfg.SYSTEM_CONFIG5105.or_const(1 << 1)                   # Bypassing PLL and select ext clock
    corecfg.SYSTEM_CONFIG5106.or_const(1 << 0)                   # Disabling PLL
    val = (idf << 25) | (cp << 1)
    corecfg.SYSTEM_CONFIG5106.read_modify_write(~((0x7 << 25) | (0x1f << 1)), 0, val)
    val = (odf << 8) | (ndiv << 0)
    corecfg.SYSTEM_CONFIG5108.read_modify_write(~(0x3fff << 0), 0, val)
    corecfg.SYSTEM_CONFIG5106.and_const(~(1 << 0))               # Enabling PLL
    debug_print(clocks_debug,"    Waiting for CA9 PLL lock")
    if (on_emu == 0): # No lock on emulator
        corecfg.SYSTEM_STATUS5543.while_and_ne(1 << 0, 1 << 0)       # Wait for PLL to lock...
    debug_print(clocks_debug,"    => ok, CA9 PLL locked")
    corecfg.SYSTEM_CONFIG5105.and_const(~(1 << 1))               # Selecting internal PLL

    return 0

# CA9 frequency compute function
def ca9_get_freq():

    syscfg5105 = corecfg.SYSTEM_CONFIG5105.peek()
    syscfg5106 = corecfg.SYSTEM_CONFIG5106.peek()
    syscfg5108 = corecfg.SYSTEM_CONFIG5108.peek()
    sscg_st = ""

    # Is it sourced by CA9 PLL or clockgen C0 ?
    if syscfg5105 & (1 << 1):    # Clockgen C0-13
        res = 12
        src = "C0-13"
        if (syscfg5105 & (1 << 0)) == 0: # ext clock / 2
            res = res / 2
            src = "C0-13/2"
    else:                       # Source is CA9 PLL3200
        src = "CA9 PLL"
        idf = (syscfg5106 >> 25) & 0x7
        ndiv = (syscfg5108 >> 0) & 0xff
        odf = (syscfg5108 >> 8) & 0x3f
        if odf == 0:
            odf = 1
        if clocks_debug:
            sttp.logging.print_out("    idf=0x%x, ndiv=%u, odf=%u" % (idf, ndiv, odf))

        if idf == 0:
            sttp.logging.print_out("ca9_get_freq() ERROR: IDF=0")
            res = 0
        else:
            res = ((2 * osc_freq * ONEMEG * ndiv) / idf) / odf

        sscg_on = (syscfg5106 >> 29) & 1
        if (sscg_on): # Is SSCG started ?
            sscg_st = ", SSCG ENABLED"

    if (res != 1200000000):
        diff = "**"
    else:
        diff = ""

    return res, src, sscg_st, diff

# HACK by vf21: use native print method to display the device id
# for tests/targetpack - (Cannes2)
def display_device_id():
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return
    print("Device ID infos ...")
    deviceid = corecfg.SYSTEM_STATUS5568.peek()
    if ((deviceid & 0x0fffffff) != 0x0d456041):
        print("  Device ID = 0x%08X ==> ERROR !! NOT a STih410" % (deviceid))
        raise sttp.STTPException("NOT IDENTIFIED as STih410 chip")

    chipcut = ((deviceid>>28)&0x0F) + 1
    minor = 0
    print("  Device ID = 0x%08X ==> STih410 cut %d.%d" % (deviceid, chipcut, minor))

# Display chip and mode pins config
def display_chip():

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    sttp.logging.print_out("Chip infos ...")
    deviceid = corecfg.SYSTEM_STATUS5568.peek()
    if ((deviceid & 0x0fffffff) != 0x0d456041):     # Not a STih410
        sttp.logging.print_out("  Device ID = 0x%08X ==> ERROR !! NOT a STih410" % (deviceid))
        raise sttp.STTPException("NOT IDENTIFIED as STih410 chip")

    chipcut = ((deviceid>>28)&0x0F) + 1
    minor = 0
    sttp.logging.print_out("  Device ID = 0x%08X ==> STih410 cut %d.%d" % (deviceid, chipcut, minor))

    modepins = corecfg.SYSTEM_STATUS5561.peek()
    sttp.logging.print_out("  Mode-pins = 0x%03x" % modepins)
    bootdev = (modepins >> 2) & 0x1f

    if modepins & 0x1:
        sttp.logging.print_out("    Ref clock (mode0=1)                     = External reference clock")
    else:
        sttp.logging.print_out("    Ref clock (mode0=0)                     = Clock sourced from MIPHY internal oscillator")
    if modepins & 0x2:
        sttp.logging.print_out("    Boot master (mode1=1)                   = ST231_GP_1")
    else:
        sttp.logging.print_out("    Boot master (mode1=0)                   = Cortex A9")

    if bootdev == 1:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 2K page, 4 addr cycles, error-free (BCH Controller)" % bootdev)
    elif bootdev == 2:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 2K page, 4 addr cycles, 18-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 3:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 2K page, 4 addr cycles, 30-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 4:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 2K page, 5 addr cycles, error-free (BCH Controller)" % bootdev)
    elif bootdev == 5:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 2K page, 5 addr cycles, 18-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 6:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 4K page, 5 addr cycles, 30-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 8:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 4K page, 5 addr cycles, error-free (BCH Controller)" % bootdev)
    elif bootdev == 9:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 4K page, 5 addr cycles, 18-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 10:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 4K page, 5 addr cycles, 30-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 12:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 8K page, 5 addr cycles, error-free (BCH Controller)" % bootdev)
    elif bootdev == 13:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 8K page, 5 addr cycles, 18-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 14:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = NAND, 8K page, 5 addr cycles, 30-bit error-prone (BCH Controller)" % bootdev)
    elif bootdev == 17:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = 8-bit NAND, small page, short address (Hamming Controller)" % bootdev)
    elif bootdev == 18:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = 8-bit NAND, small page, long address (Hamming Controller)" % bootdev)
    elif bootdev == 19:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = 8-bit NAND, large page, short address (Hamming Controller)" % bootdev)
    elif bootdev == 20:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = 8-bit NAND, large page, long address (Hamming Controller)" % bootdev)
    elif bootdev == 26:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = SPI NOR (clock div = 4)" % bootdev)
    elif bootdev == 27:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = SPI NOR (clock div = 2)" % bootdev)
    elif bootdev == 30:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = MMC v4.4 or v4.5" % bootdev)
    else:
        sttp.logging.print_out("    Boot device (mode[6:2]=0x%02x)            = ERROR - 0x%x is a reserved value!" % bootdev)

    if modepins & (1 << 7):
        sttp.logging.print_out("    Boot device ERROR (mode7=1)             = Should be set to 0")
    if modepins & (1 << 8):
        sttp.logging.print_out("    Boot device operating voltage (mode8=1) = 1V8")
    else:
        sttp.logging.print_out("    Boot device operating voltage (mode8=1) = 3V3")
    if modepins & (1 << 9):
        sttp.logging.print_out("    Boot device ERROR (mode9=1)             = Should be set to 0")
    if modepins & (1 << 10):
        sttp.logging.print_out("    Boot device ERROR (mode10=1)            = Should be set to 0")
    if modepins & (1 << 11):
        sttp.logging.print_out("    Boot device ERROR (mode11=1)            = Should be set to 0")

    sttp.logging.print_out("")

# Clocks display function
def display_clocks():

    if (sttp.stmc.get_operating_mode() == "ROMGEN"):
        return

    # Computing max name length for name vertical alignment
    max = 0
    for out in clkgenA0["outputs"]:
        if (len(out) > max):
            max = len(out)
    for out in clkgenC0["outputs"]:
        if (len(out) > max):
            max = len(out)

    sttp.logging.print_out("  Clockgen A0")
    pll0, sscg_st = flexgen.get_pll3200(clkgenA0, 0)
    sttp.logging.print_out("    PLL0              = %u Hz %s" % (pll0, sscg_st))
    xbar_src_freqs = [pll0, osc_freq * ONEMEG]
    flexgen.display_freq(clkgenA0, xbar_src_freqs, max)

    sttp.logging.print_out("  Clockgen C0")
    pll0, sscg_st = flexgen.get_pll3200(clkgenC0, 0)
    sttp.logging.print_out("    PLL0              = %u Hz %s" % (pll0, sscg_st))
    pll1, sscg_st = flexgen.get_pll3200(clkgenC0, 1)
    sttp.logging.print_out("    PLL1              = %u Hz %s" % (pll1, sscg_st))
    vco_freq = flexgen.get_fs660_vco(clkgenC0, 2)
    sttp.logging.print_out("    FS660 VCO         = %u Hz "% vco_freq)
    fs0 = flexgen.get_fs660(clkgenC0, 2, vco_freq, 0)
    sttp.logging.print_out("      FS0             = %u Hz" % fs0)
    fs1 = flexgen.get_fs660(clkgenC0, 2, vco_freq, 1)
    sttp.logging.print_out("      FS1             = %u Hz" % fs1)
    fs2 = flexgen.get_fs660(clkgenC0, 2, vco_freq, 2)
    sttp.logging.print_out("      FS2             = %u Hz" % fs2)
    fs3 = flexgen.get_fs660(clkgenC0, 2, vco_freq, 3)
    sttp.logging.print_out("      FS3             = %u Hz" % fs3)
    xbar_src_freqs = [pll0, pll1, fs0, fs1, fs2, fs3, osc_freq * ONEMEG]
    flexgen.display_freq(clkgenC0, xbar_src_freqs, max)

    sttp.logging.print_out("  Clockgen CA9")
    freq, src, sscg_st, diff = ca9_get_freq()
    sttp.logging.print_out("    CA9               = %u Hz (src=%s%s) %s" % (freq, src, sscg_st, diff))

    sttp.logging.print_out("")

# DDR frequency return function
def ddr_get_mbps():
    return ddr_mbps

# Memory checking function
# Params: Start addr, size (in B), increment (in B)
def check_mem(start, size, inc):

    sttp.logging.print_out("  Testing memory @ 0x%x: size=%dB, inc=%dB" % (start,size,inc))

    count = 0
    while(count < size):
        value = start + count
        sttp.pokepeek.poke(value,value)
        count = count + inc

    count = 0
    err = 0
    while(count < size and err < 4):
        value = start + count
        read_value = sttp.pokepeek.peek(value)
        if (read_value != value):
            sttp.logging.print_out("  ERROR: Exp=0x%08x @ 0x%08x. Got=0x%08x" % (value,value,read_value))
            err += 1
        count = count + inc
    if err:
        sttp.logging.print_out("  => 4 or more ERRORS found")
    else:
        sttp.logging.print_out("  => OK")

def define_ctis():

    try:
        # Only works for > STMC 2011.2.1
        import cti
    except:
        return

    a9core = sttp.targetpack.find_cpu(sttp_root, "a9")
    container = a9core.parent.parent

    c0_info = {
      "instance" :  eval("container.coresight_cti_c0"),
      "tins" : (
                 { "name" : "a9_dbgack",       "short_name" : "a9",   "bit" : 0, },
                 { "name" : "a9_0_dbgack",     "short_name" : "a9_0", "bit" : 0, },
               ),
      "touts" : (
                 { "name" : "a9_egdbrq",       "short_name" : "a9",   "bit" : 0, },
                 { "name" : "a9_0_egdbrq",     "short_name" : "a9_0", "bit" : 0, },
                 { "name" : "a9_dbgrestart",                                 "bit" : 7, },
                 { "name" : "a9_0_dbgrestart",                               "bit" : 7, },
               )
    }

    c1_info = {
      "instance" :  eval("container.coresight_cti_c1"),
      "tins" : (
                 { "name" : "a9_1_dbgack",     "short_name" : "a9_1", "bit" : 0, },
               ),
      "touts" : (
                 { "name" : "a9_1_egdbrq",     "short_name" : "a9_1", "bit" : 0, },
                 { "name" : "a9_1_dbgrestart",                               "bit" : 7, },
               )
    }

    glue_base = sttp_root.stxh410.coresight_cti_soc.coresight_cti.GetBase() + 0x1000
    glue_csem = glue_base
    glue_soc  = glue_base + 4
    csem_info = {
      "instance" :  eval("container.coresight_cti_csem"),
      "glue" :  { "type"    : "Type_A",
                  "address" : glue_csem,
                },
      "tins" :  (
                  { "name" : "pio_trigout",            "short_name" : "pio",           "bit" : 0, "invert" : True, "edge" : True },
                  { "name" : "gp1_trigout",            "short_name" : "gp1",           "bit" : 1, "invert" : True, "edge" : True },
                  { "name" : "audio_trigout",          "short_name" : "audio",         "bit" : 2, "invert" : True, "edge" : True },
                  { "name" : "video_trigout",          "short_name" : "video",         "bit" : 3, "invert" : True, "edge" : True },
                  { "name" : "gp0_trigout",            "short_name" : "gp0",           "bit" : 4, "invert" : True, "edge" : True },
                  { "name" : "xp70_tango_sc_trigout",  "short_name" : "xp70_tango_sc", "bit" : 5, "invert" : True, "edge" : True },
                  { "name" : "xp70_tango_tp_trigout",  "short_name" : "xp70_tango_tp", "bit" : 6 },
                  { "name" : "a9_depack_trigout",      "short_name" : "a9_depack",     "bit" : 7 },
                ),
      "touts" : (
                  { "name" : "pio_trigin",             "short_name" : "pio",           "bit" : 0 },
                  { "name" : "gp1_trigin",             "short_name" : "gp1",           "bit" : 1 },
                  { "name" : "audio_trigin",           "short_name" : "audio",         "bit" : 2 },
                  { "name" : "video_trigin",           "short_name" : "video",         "bit" : 3 },
                  { "name" : "gp0_trigin",             "short_name" : "gp0",           "bit" : 4 },
                  { "name" : "xp70_tango_sc_trigin",   "short_name" : "xp70_tango_sc", "bit" : 5 },
                  { "name" : "xp70_tango_tp_trigin",   "short_name" : "xp70_tango_tp", "bit" : 6 },
                ),
    }

    soc_info = {
     "instance" :  eval("container.coresight_cti_soc"),
      "glue" :  { "type"    : "Type_A",
                  "address" : glue_soc,
                },
      "tins" :  (
                  { "name" : "hades_trigout",     "short_name" : "hades",    "bit" : 0, "invert" : True, "edge" : True },
                  { "name" : "xp70_lpm_trigout",  "short_name" : "xp70_lpm", "bit" : 2, "invert" : True, "edge" : True },
                  { "name" : "mpe_sbag_trigout",  "short_name" : "mpe_sbag", "bit" : 3, "invert" : True, "edge" : True },
                  { "name" : "stm_busy_trigout",  "short_name" : "stm_busy", "bit" : 5, "invert" : True, "edge" : True },
                  { "name" : "dbu_trigout",       "short_name" : "dbu",      "bit" : 7 },
                ),
      "touts" : (
                  { "name" : "hades_trigin",      "short_name" : "hades",    "bit" : 0 },
                  { "name" : "xp70_lpm_trigin",   "short_name" : "xp70_lpm", "bit" : 2 },
                  { "name" : "mpe_sbag_trigin",   "short_name" : "mpe_sbag", "bit" : 3 },
                  { "name" : "event0_trigin",     "short_name" : "event0",   "bit" : 4 },
                  { "name" : "event1_trigin",     "short_name" : "event1",   "bit" : 5 },
                  { "name" : "event2_trigin",     "short_name" : "event2",   "bit" : 6 },
                  { "name" : "dbu_trigin",        "short_name" : "dbu",      "bit" : 7 },
                ),
    }

    cti.init((soc_info, csem_info, c0_info, c1_info))

def get_modepin_definitions():
    return stxh410_modepins

# IRQ pack config
# base    = IRQ pack base address
# input   = IRQ pack input
# channel = IRQ depack number
# output  = IRQ number at depack side
# inv     = invert IRQ input if set
def configure_irqpack(base, input, channel, output, inv):

    addr = base + input * 0x4
    sttp.pokepeek.read_modify_write(addr, addr, ~(0x1fff), 0, (inv << 12) | (output << 4) | channel)

# Reconfigure some IRQs routings
def configure_irqs():

    IRQ_PACK_11 = 0x8F14000
    IRQ_PACK_12 = 0x8F15000
    IRQ_PACK_13 = 0x8F16000
    IRQ_DEPACK_A9_10 = 0

    # RnDHV00054145 bug fix
    configure_irqpack(IRQ_PACK_13, 61, IRQ_DEPACK_A9_10, 75, 0)  # fc_it_host
    configure_irqpack(IRQ_PACK_13, 62, IRQ_DEPACK_A9_10, 76, 0)  # fc_it_host_error
    configure_irqpack(IRQ_PACK_13, 63, IRQ_DEPACK_A9_10, 77, 0)  # fc_mb_it
    configure_irqpack(IRQ_PACK_13, 64, IRQ_DEPACK_A9_10, 78, 0)  # pp_irq
    configure_irqpack(IRQ_PACK_13, 65, IRQ_DEPACK_A9_10, 79, 0)  # rab_it

    # USB fix, for Monaco2 cut2 and following
    configure_irqpack(IRQ_PACK_11, 92, IRQ_DEPACK_A9_10, 180, 0) # usb2_10_ohci
    configure_irqpack(IRQ_PACK_11, 93, IRQ_DEPACK_A9_10, 181, 0) # usb2_11_ohci
    configure_irqpack(IRQ_PACK_11, 82, IRQ_DEPACK_A9_10, 151, 0) # usb2_10_ehci (connected to OTG at wrapper)
    configure_irqpack(IRQ_PACK_11, 84, IRQ_DEPACK_A9_10, 153, 0) # usb2_11_ehci (connected to OTG at wrapper)

    # IRQ inversion for LPC10 & LPC11
    configure_irqpack(IRQ_PACK_12, 106, IRQ_DEPACK_A9_10, 129, 1)
    configure_irqpack(IRQ_PACK_12, 107, IRQ_DEPACK_A9_10, 130, 1)

# Workarounds needed for either some issues or generic soc support
def configure_soc_wa():

    # Enable Interconnect auto clock gating
    sttp.pokepeek.poke(0x92c00a8, 0x0)

    # A9 PL310 AXI_1 filter range update for DDR 2GB addressability
    sttp.pokepeek.read_modify_write(0x092B01BC, 0x092B01BC, ~(3 << 23), 0, (3 << 23))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        sttp.pokepeek.read_modify_write(0x08762C04, 0x08762C04, ~(3 << 30), 0, (3 << 30))

    # LPM_CONFIG_1: SBC UART0/1 enable needed for linux/uboot not to miss early debug prints
    sttp.pokepeek.or_const(0x94B5104, 0x94B5104, (1 << 12) | (1 << 11))

    # Force USB2 PicoPHYs into reset by default
    corecfg.SYSTEM_CONFIG5061.or_const(0x7 << 5)

# Workaround for disable boot from eMMC in JTAG mode if eMMC0 boot selected
def configure_mmc_wa():

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        if corecfg.SYSTEM_STATUS5561.if_eq(0x78, 0xf << 3): # eMMC0 boot
            sttp.logging.print_out("Disable boot from eMMC ...")
            mmc = sttp.pokepeek.peek(0x0) # timeout due to FW load
            if (mmc == 0):
                mmc = sttp.pokepeek.peek(0x0) # dummy request to be able to disable boot
            if (mmc == 0): # Device not bootable wait 2s for timeout
                sttp.stmc.delay(2000000)
            sttp.pokepeek.poke(0x9061000,0x0) # FlashSS_TOP

# DDR-SS mixer configuration. Called from <board>.py
# Returns: updated 'ddrparams'
def configure_mixer(ddrparams):

    debug_print(ddr_debug, "Configuring mixer...")
    if uart_debug:
        stxh410_uart.write("TP: Configuring mixer\n\r")

    ddr3ss = sttp_root.stxh410.DDR3_SS.ddr3_ss_regs
    mixer = sttp_root.stxh410.DDR3_MIXER.ddr3_mixer_regs

    mem_lower_base_addr = ddrparams["lmi_start"]
    mem_upper_base_addr = mem_lower_base_addr + (ddrparams["lmi_size"]/16)
    debug_print(ddr_debug,"mem_lower_base_addr=0x%x, mem_upper_base_addr=0x%x" %(mem_lower_base_addr, mem_upper_base_addr))
    mixer.DDR3MIXER_DDR_BASE_ADDR.poke((mem_upper_base_addr << 16) | mem_lower_base_addr)

    # Set DDR parameters
    bus_turn_around                   = (ddrparams["TRTW"]+1)/2       # in system clock which equal DDR_Clock / 2
    min_time_frame                    = ((ddrparams["TFAW"]+1)/2)/4   # in system clock and by step of 4
    diff_bank_activate                = (ddrparams["TRRD"]+1)/2       # in system clock
    pre_to_next_read_or_write         = (ddrparams["TRP"]+ddrparams["TRCD"]+1)/2   # Trp + Trcd in system clock
    bank_activate_to_activate_timing  = (ddrparams["TRC"]+1)/2        # Trc in system clock
    ddr_min_burst_size                = 2
    if (ddrparams["lmi_width"] == 8):
        ddr_bus_size                  = 0
    elif (ddrparams["lmi_width"] == 16):
        ddr_bus_size                  = 1
    else:
        ddr_bus_size                  = 2
    ddr_type                          = 1   # 0=DDR2, 1=DDR3
    write_posting_en                  = 0

    mixer.DDR3MIXER_DDR_PARAMETER.poke((bus_turn_around << 28) | (min_time_frame << 24) | (diff_bank_activate << 20) | (pre_to_next_read_or_write << 14) | (bank_activate_to_activate_timing << 8) | (ddr_min_burst_size << 4) | (ddr_bus_size << 2) | ddr_type)

    if (ddrparams["lmi_width"] == 16):
        mixer.DDR3MIXER_GEN_PURPOSE_REG0.poke(0x104020c9) #Column size = 9 bits
    else:
        mixer.DDR3MIXER_GEN_PURPOSE_REG0.poke(0x104020ca) #Column size = 10 bits

    if (mixer_bypass == 0):
        mixer.DDR3MIXER_GEN_CTRL.poke(0x00080710 | (write_posting_en << 16))
    else:
        ddr3ss.DDR_SYSTEM_CONFIG_801.or_const(0x00000008)
        mixer.DDR3MIXER_GEN_CTRL.poke(0x00080711 | (write_posting_en << 16))

    #
    # Setup from CANNES_DDRconfig_setting_v1.0.pdf
    #

    mixer_base = mixer.baseAddress

    # Logical DDR page configuration
    #mixer.DDR3MIXER_ROW_ADDR_MASK.poke(0xFFFFF800) # 256B
    #mixer.DDR3MIXER_ROW_ADDR_MASK.poke(0xFFFFE000) # 1KB
    mixer.DDR3MIXER_ROW_ADDR_MASK.poke(0xFFFF8000) # 4KB
    MIXER0_BANK = mixer_base + 0x200
    count = 0
    bank_bits_value = 0x800A0908
    while(count < 64):
        address = MIXER0_BANK + (count * 4)
        sttp.pokepeek.poke(address, bank_bits_value)
        if (count==((ddrparams["DTUAddress"]>>22) & 0x3F)):         # retrieve the Bank bit position inside phisical address for the fixed DTUAddress
            ddrparams["MixerBankBitReg"]=bank_bits_value
        count = count + 1

    # Mixer response fifo
    mixer_default = 0x2E2E2E2E # RespFifo1 PageInt MsgAtom MsgSF
    mixer_gpu = 0x0E0E0E0E # RespFifo0 PageInt MsgAtom MsgSF
    mixer_dmu_dly = 0x2E2E2F2F # RespFifo1 PageInt MsgAtom MsgSF no WP
    mixer_bdisp = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_capture = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_dmu = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_dvp = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_hqvdp = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_hva = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_jpeg = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_vdpaux = 0x2E2E2E2E # RespFifo1 PageInt MsgAtom MsgSF
    mixer_hades_pp = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    mixer_hades_dch = 0x2F2F2F2F # RespFifo1 PageInt MsgAtom MsgSF WP
    # src id 128 to 143
    sttp.pokepeek.poke(mixer_base + 0x0480, mixer_bdisp) # BDISP
    sttp.pokepeek.poke(mixer_base + 0x0484, mixer_bdisp) # BDISP
    sttp.pokepeek.poke(mixer_base + 0x0488, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x048C, mixer_bdisp) # BDISP
    # src id 144 to 159
    sttp.pokepeek.poke(mixer_base + 0x0490, mixer_bdisp) # BDISP
    sttp.pokepeek.poke(mixer_base + 0x0494, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0498, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x049C, mixer_default)
    # src id 160 to 175
    sttp.pokepeek.poke(mixer_base + 0x04A0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04A4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04A8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04AC, mixer_default)
    # src id 176 to 191
    sttp.pokepeek.poke(mixer_base + 0x04B0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04B4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04B8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04BC, mixer_default)
    # src id 192 to 207
    sttp.pokepeek.poke(mixer_base + 0x04C0, mixer_capture) # CAPTURE_MP0
    sttp.pokepeek.poke(mixer_base + 0x04C4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04C8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04CC, mixer_default)
    # src id 208 to 223
    sttp.pokepeek.poke(mixer_base + 0x04D0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04D4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04D8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04DC, mixer_default)
    # src id 224 to 239
    sttp.pokepeek.poke(mixer_base + 0x04E0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04E4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04E8, mixer_capture) # CAPTURE_MP1
    sttp.pokepeek.poke(mixer_base + 0x04EC, mixer_default)
    # src id 240 to 255
    sttp.pokepeek.poke(mixer_base + 0x04F0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04F4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04F8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x04FC, mixer_default)
    # src id 256 to 271
    sttp.pokepeek.poke(mixer_base + 0x0500, mixer_dvp) # DVP0
    sttp.pokepeek.poke(mixer_base + 0x0504, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0508, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x050C, mixer_default)
    # src id 272 to 287
    sttp.pokepeek.poke(mixer_base + 0x0510, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0514, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0518, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x051C, mixer_default)
    # src id 288 to 303
    sttp.pokepeek.poke(mixer_base + 0x0520, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0524, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0528, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x052C, mixer_default)
    # src id 304 to 319
    sttp.pokepeek.poke(mixer_base + 0x0530, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0534, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0538, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x053C, mixer_default)
    # src id 320 to 335
    sttp.pokepeek.poke(mixer_base + 0x0540, mixer_dvp) # DVP1
    sttp.pokepeek.poke(mixer_base + 0x0544, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0548, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x054C, mixer_default)
    # src id 336 to 351
    sttp.pokepeek.poke(mixer_base + 0x0550, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0554, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0558, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x055C, mixer_default)
    # src id 352 to 367
    sttp.pokepeek.poke(mixer_base + 0x0560, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0564, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0568, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x056C, mixer_default)
    # src id 368 to 383
    sttp.pokepeek.poke(mixer_base + 0x0570, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0574, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0578, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x057C, mixer_default)
    # src id 384 to 399
    sttp.pokepeek.poke(mixer_base + 0x0580, mixer_dmu) # DMU MP0
    sttp.pokepeek.poke(mixer_base + 0x0584, mixer_dmu) # DMU MP0
    sttp.pokepeek.poke(mixer_base + 0x0588, mixer_dmu_dly) # DMU MP0
    sttp.pokepeek.poke(mixer_base + 0x058C, mixer_dmu) # DMU MP0
    # src id 400 to 415
    sttp.pokepeek.poke(mixer_base + 0x0590, mixer_dmu) # DMU MP1
    sttp.pokepeek.poke(mixer_base + 0x0594, mixer_dmu) # DMU MP1
    sttp.pokepeek.poke(mixer_base + 0x0598, mixer_dmu_dly) # DMU MP1
    sttp.pokepeek.poke(mixer_base + 0x059C, mixer_dmu) # DMU MP1
    # src id 416 to 431
    sttp.pokepeek.poke(mixer_base + 0x05A0, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05A4, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05A8, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05AC, mixer_hva) # HVA
    # src id 432 to 447
    sttp.pokepeek.poke(mixer_base + 0x05B0, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05B4, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05B8, mixer_hva) # HVA
    sttp.pokepeek.poke(mixer_base + 0x05BC, mixer_hva) # HVA
    # src id 448 to 463
    sttp.pokepeek.poke(mixer_base + 0x05C0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05C4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05C8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05CC, mixer_default)
    # src id 464 to 479
    sttp.pokepeek.poke(mixer_base + 0x05D0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05D4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05D8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05DC, mixer_default)
    # src id 480 to 495
    sttp.pokepeek.poke(mixer_base + 0x05E0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05E4, mixer_jpeg) # JPEG
    sttp.pokepeek.poke(mixer_base + 0x05E8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05EC, mixer_default)
    # src id 496 to 511
    sttp.pokepeek.poke(mixer_base + 0x05F0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05F4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05F8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x05FC, mixer_default)
    # src id 512 to 527
    sttp.pokepeek.poke(mixer_base + 0x0600, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0604, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0608, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x060C, mixer_default)
    # src id 528 to 543
    sttp.pokepeek.poke(mixer_base + 0x0610, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0614, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0618, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x061C, mixer_default)
    # src id 544 to 559
    sttp.pokepeek.poke(mixer_base + 0x0620, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0624, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0628, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x062C, mixer_default)
    # src id 560 to 575
    sttp.pokepeek.poke(mixer_base + 0x0630, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0634, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0638, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x063C, mixer_default)
    # src id 576 to 591
    sttp.pokepeek.poke(mixer_base + 0x0640, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0644, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0648, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x064C, mixer_default)
    # src id 592 to 607
    sttp.pokepeek.poke(mixer_base + 0x0650, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0654, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0658, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x065C, mixer_default)
    # src id 608 to 623
    sttp.pokepeek.poke(mixer_base + 0x0660, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0664, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0668, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x066C, mixer_default)
    # src id 624 to 639
    sttp.pokepeek.poke(mixer_base + 0x0670, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0674, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0678, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x067C, mixer_default)
    # src id 640 to 655
    sttp.pokepeek.poke(mixer_base + 0x0680, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0684, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0688, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x068C, mixer_default)
    # src id 656 to 671
    sttp.pokepeek.poke(mixer_base + 0x0690, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0694, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0698, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x069C, mixer_default)
    # src id 672 to 687
    sttp.pokepeek.poke(mixer_base + 0x06A0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06A4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06A8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06AC, mixer_default)
    # src id 688 to 703
    sttp.pokepeek.poke(mixer_base + 0x06B0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06B4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06B8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06BC, mixer_default)
    # src id 704 to 719
    sttp.pokepeek.poke(mixer_base + 0x06C0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06C4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06C8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06CC, mixer_default)
    # src id 720 to 735
    sttp.pokepeek.poke(mixer_base + 0x06D0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06D4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06D8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06DC, mixer_default)
    # src id 736 to 751
    sttp.pokepeek.poke(mixer_base + 0x06E0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06E4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06E8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06EC, mixer_default)
    # src id 752 to 767
    sttp.pokepeek.poke(mixer_base + 0x06F0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06F4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06F8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x06FC, mixer_default)
    # src id 768 to 783
    sttp.pokepeek.poke(mixer_base + 0x0700, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0704, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0708, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x070C, mixer_default)
    # src id 784 to 799
    sttp.pokepeek.poke(mixer_base + 0x0710, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0714, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0718, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x071C, mixer_default)
    # src id 800 to 815
    sttp.pokepeek.poke(mixer_base + 0x0720, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0724, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0728, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x072C, mixer_default)
    # src id 816 to 831
    sttp.pokepeek.poke(mixer_base + 0x0730, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0734, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0738, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x073C, mixer_default)
    # src id 832 to 847
    sttp.pokepeek.poke(mixer_base + 0x0740, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0744, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0748, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x074C, mixer_default)
    # src id 848 to 863
    sttp.pokepeek.poke(mixer_base + 0x0750, mixer_hades_pp) #HADES_PP
    sttp.pokepeek.poke(mixer_base + 0x0754, mixer_hades_pp) #HADES_PP
    sttp.pokepeek.poke(mixer_base + 0x0758, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x075C, mixer_default)
    # src id 864 to 879
    sttp.pokepeek.poke(mixer_base + 0x0760, mixer_hades_dch) #HADES_DCH
    sttp.pokepeek.poke(mixer_base + 0x0764, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0768, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x076C, mixer_default)
    # src id 880 to 895
    sttp.pokepeek.poke(mixer_base + 0x0770, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0774, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0778, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x077C, mixer_default)
    # src id 896 to 911
    sttp.pokepeek.poke(mixer_base + 0x0780, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0784, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0788, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x078C, mixer_default)
    # src id 912 to 927
    sttp.pokepeek.poke(mixer_base + 0x0790, mixer_hqvdp) # HQVDP
    sttp.pokepeek.poke(mixer_base + 0x0794, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x0798, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x079C, mixer_default)
    # src id 928 to 943
    sttp.pokepeek.poke(mixer_base + 0x07A0, mixer_hqvdp) # HQVDP
    sttp.pokepeek.poke(mixer_base + 0x07A4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07A8, mixer_vdpaux) # VDPAUX
    sttp.pokepeek.poke(mixer_base + 0x07AC, mixer_default)
    # src id 944 to 959
    sttp.pokepeek.poke(mixer_base + 0x07B0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07B4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07B8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07BC, mixer_default)
    # src id 960 to 975
    sttp.pokepeek.poke(mixer_base + 0x07C0, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07C4, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07C8, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07CC, mixer_gpu) # GPU
    # src id 976 to 991
    sttp.pokepeek.poke(mixer_base + 0x07D0, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07D4, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07D8, mixer_gpu) # GPU
    sttp.pokepeek.poke(mixer_base + 0x07DC, mixer_gpu) # GPU
    # src id 992 to 1007
    sttp.pokepeek.poke(mixer_base + 0x07E0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07E4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07E8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07EC, mixer_default)
    # src id 1008 to 1023
    sttp.pokepeek.poke(mixer_base + 0x07F0, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07F4, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07F8, mixer_default)
    sttp.pokepeek.poke(mixer_base + 0x07FC, mixer_default)

    mixer.DDR3MIXER_THR_BW_LIMITER1.poke(0x0006004A) # 603 MB/s
    mixer.DDR3MIXER_THR_BW_LIMITER2.poke(0x0019000D)
    mixer.DDR3MIXER_BUS_DIR_LOCK.poke(0x00000717)
    mixer.DDR3MIXER_BUS_DIR_LATENCY.poke(0x7F7F5F1F)
    mixer.DDR3MIXER_BUS_DIR_HP_PORT.poke(0x00000707)
    mixer.DDR3MIXER_BUS_DIR_QUEUE_THRESHOLD.poke(0x00000002)
    mixer.DDR3MIXER_INPUT_FLOW_REGULATION.poke(0x1F2F3F7F)
    mixer.DDR3MIXER_ARBITER_FLOW_REGULATION.poke(0x2F070003)
    mixer.DDR3MIXER_HI_PRI_PORT_FLOW_REG1.poke(0x1F032080)
    mixer.DDR3MIXER_HI_PRI_PORT_FLOW_REG2.poke(0x0030004C)
    mixer.DDR3MIXER_RES_UNIT0_MEM_RES.poke(0x00E80080)
    mixer.DDR3MIXER_RES_UNIT1_MEM_RES.poke(0x00E80080)

    return ddrparams
