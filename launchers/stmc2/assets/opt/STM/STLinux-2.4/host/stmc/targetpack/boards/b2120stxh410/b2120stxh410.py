###
### STih410 specific "targetpack" for B2120
### (C) M. Phillips, R. Singh, F. Besson, L.Vachon, F. Charpentier, A. Proust
###

# Targetpack version
tpversion = "14"

import sttp
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import stxh410
import stxh410_hades
import upctl_ddrsetup
import stxh410_ddrparams
import stxh410_interco_init
import utilities
import stxh410_uart
import upctl_ddrdbg
import stxh410_pio
import stxh410_avs_abb
import stxh410_powerdown

# Default parameter's values for this board
global sttp_root
sttp_root = sttp.targetpack.get_tree()
osc_freq = 0        # Onboard ref oscillator frequency. Set from <board>.xml
ddr_debug = 0       # DDR debug. 0=disable
lmi_width = 32
lmi_size = 2048
lmi_start = 0x40
on_emu = 0          # If set, indicate that we are running on emulator (=no PLL)
sscg_ddr = 1
boardrev = 0        # Needed for DDR chip speed grade definition, either to be passed through the command line (ROMGEN/JTAG) or is set automatically with the pull-up pull-down resistors on UART (JTAG only)
uart_debug = 0      # UART debug. 0=disable. Trace TP execution thru UART.
ddr_test = 0        # Run a quick DDR test after DDR config if set
uart_setup = 0      # UART presetup: 0=disable. Allows kernel predebug
pio_debug = 0       # 1= Example for toggle pio1[3] 10 times.
avs_debug = 0       # AVS debug. 0=disable.
dtu_fixed = 0       # Use fix DTU training address. 0=use address stored by application in HoM structure, 1=use fixed address
dtu_address = 0     # Fixed DTU address. If empty, DTU address is calculated as per lmi_size
a9_freq_c1 = 1200   # A9 default freq for cut1
a9_freq_c2 = 1500   # A9 default freq for cut2 and followings
profile = 1         # default profile

# Script for board with typical single core jtag device
def connect(parameters):

    global ddr_debug, on_emu, uart_debug, uart_setup, avs_debug
    global lmi_width, lmi_size, lmi_start, lmi_freq, sscg_ddr
    global osc_freq, ddr_test
    global boardrev
    global dtu_fixed, dtu_address
    global profile

    sttp.logging.print_out("\n***\n*** 'b2120stxh410' targetpack v%s starting\n***\n" % tpversion)
    sttp.logging.print_out("Parameters: ", parameters)

    # is there a user supplied module and function to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    if parameters.has_key("debug"):
        debugList = parameters["debug"].split(":")
        if "uart" in debugList: # UART debug is allowed from flash
            uart_setup = 1
            uart_debug = 1
        if sttp.stmc.get_operating_mode() != "ROMGEN":
            if "ddr" in debugList:
                ddr_debug = 1
            if "avs" in debugList:
                avs_debug = 1
    if parameters.has_key("uart_setup"):
        uart_setup = int(parameters["uart_setup"], 0)
    if parameters.has_key("on_emu"):
        on_emu = int(parameters["on_emu"])
    if (on_emu == 1) and not(parameters.has_key("on_emu")):  # needed to propagate on_emu value to other python files.
        parameters["on_emu"]=1
    if parameters.has_key("sscg_ddr"):
        sscg_ddr = int(parameters["sscg_ddr"], 0)
    if (parameters.has_key("lmi_size")):
        lmi_size = int(parameters["lmi_size"])
    if parameters.has_key("lmi_16bits") and (int(parameters["lmi_16bits"]) == 1):
        lmi_width = 16
        lmi_size = lmi_size / 2   # Half size available when 16bits
    if parameters.has_key("lmi_start"):
        lmi_start = int(parameters["lmi_start"], 0)
    t = sttp.stmc.get_target_params()
    osc_freq = int(t["osc_main"], 0)
    parameters["main_osc"] = osc_freq
    if parameters.has_key("ddr_test"):
        ddr_test = int(parameters["ddr_test"], 0)
    if parameters.has_key("boardrev"):
        boardrev=int(parameters["boardrev"], 0)
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if ((boardrev!=1) and (boardrev!=2) and (boardrev!=3) and (boardrev!=4)):
            raise sttp.STTPException("Parameters boardrev (=1 for B2120A, =2 for B2120B, =3 for B2120C, =4 for B2120D) must be defined in the command line for ROMGEN\n")

    if parameters.has_key("lmi_freq") :
        lmi_freq = int(parameters["lmi_freq"])
        sttp.logging.print_out("\nForcing LMI data rate to: %d mbps\n" %(lmi_freq))
    else:
        if (parameters.has_key("profile")):
            profile = int(parameters["profile"])
        sttp.logging.print_out("\nSelected profile: %d" %(profile))
        if (profile == 0):
            sttp.logging.print_out(" DDR@1600Mbps")
            sttp.logging.print_out(" HEVC HD only (1080p60) : HADES HWPE@200MHz")
            sttp.logging.print_out(" H264 HD only (1080p60) : DELTA VID_DMU@533MHz\n")
            lmi_freq = 1600
        else:
            if (profile == 1):
                sttp.logging.print_out(" DDR@1866Mbps")
                sttp.logging.print_out(" HEVC HD only (1080p90) : HADES HWPE@300MHz")
                sttp.logging.print_out(" H264 HD only (1080p90) : DELTA VID_DMU@533MHz\n")
                lmi_freq = 1866
            else:
                sttp.logging.print_out(" DDR@2133Mbps")
                sttp.logging.print_out(" HEVC UHDp30 : HADES HWPE@400MHz")
                sttp.logging.print_out(" H264 UHDp24 : DELTA VID_DMU@660MHz\n")
                lmi_freq = 2133
        parameters["lmi_freq"] = lmi_freq

    parameters["profile"] = profile
    parameters["a9_freq_c1"] = a9_freq_c1
    parameters["a9_freq_c2"] = a9_freq_c2

    if parameters.has_key("dtu_fixed") and (int(parameters["dtu_fixed"]) == 1):
        dtu_fixed = 1
    if parameters.has_key("dtu_address"):
        dtu_address = int(parameters["dtu_address"],0)
    else:
        dtu_address = (lmi_start<<24) + (lmi_size * 1024 * 1024) - (508 * 1024)
    real_tck = utilities.set_tck_frequency(False, parameters)

    stxh410.connect(parameters)

    # If board reset only, exiting
    if (parameters.has_key("reset_only") and (int(parameters["reset_only"]) == 1)):
        return

    # Poke/peek initialization
    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        sttp.logging.print_out("")
        sttp.logging.print_out("B2120 connect() initialization start ...")
        setup_hw(parameters)

    # Default boot_companions to 0 if using romgen and it was not specified
    if "ROMGEN" == sttp.stmc.get_operating_mode() and (not parameters.has_key("boot_companions")):
        parameters["boot_companions"] = 0
    if parameters.has_key("boot_companions") and (int(parameters["boot_companions"]) == 0):
        pass
    else:
        stxh410.boot_companions(parameters)

    if uart_debug:
        stxh410_uart.write("TP: 'b2120stxh410' targetpack v%s completed\n\r" % tpversion)
    stxh410.complete_connect(parameters)

    sttp.logging.print_out("\n***\n*** 'b2120stxh410' targetpack v%s completed\n***\n" % tpversion)

# JTAG connection established; configuring SOC
def setup_hw(parameters):
    global boardrev

    # Do poke/peek stuff here

    sttp.pokepeek.enable()
    corecfg = sttp_root.stxh410.core_sysconf_regs.sysconf_regs

    if (on_emu == 0): # Some issues with delay scaling on emulator
        sttp.pokepeek.rom_delay_scale(osc_freq)  # Default delay scaling

    # UART presetup if required
    if uart_setup:
        sttp.pokepeek.or_const(0x94B5104, 0x94B5104, (1 << 12) | (1 << 11))
        stxh410_uart.init(4, 115200, osc_freq * 1000000)

    # For JTAG connection only, retrieve board revision
    if sttp.stmc.get_operating_mode() != "ROMGEN":
        if (boardrev == 0):
            if ((sttp.pokepeek.peek(0x09613000+0x10) & (1<<4)) ==0):        # if UART10_TXD has a pull-down
                if ((sttp.pokepeek.peek(0x09612000+0x10) & (1<<6)) ==0):        # and if UART11_TXD has a pull-down => board rev A
                    boardrev=1
                else:                                                           # if UART11_TXD has a pull-up => board rev B
                    boardrev=2
            else:                                                           # if UART10_TXD has a pull-up
                if ((sttp.pokepeek.peek(0x09612000+0x10) & (1<<6)) ==0):        # and if UART11_TXD has a pull-down => board rev D!!!
                    boardrev=4
                else:                                                           # if UART11_TXD has a pull-up => board rev C
                    boardrev=3
    boardrevstr=["A","B","C","D"]
    sttp.logging.print_out("\nBoard infos ...")
    sttp.logging.print_out("  Board = B2120 rev%s " %(boardrevstr[boardrev-1]))
    if not parameters.has_key("boardrev"):
        parameters["boardrev"] = boardrev

    # PIO4AO[4] used for AVS control of DVDD_1V0 on first revision of board is by default an output for debug purpose => DVDD_1V0 is at 0V85.
    # Place it back as soon as possible as input to increase the Core power supply to 1V05
    sttp.pokepeek.poke(0x096200A4, 0x00000000)  # SYSCFG_SBC_SYSTEM_CONFIG41: Output Enable pad control for PIO alternate function

    # Configuring AVS/ABB
    # Unfortunately needs to precompute DDR & A9 freqs which should
    # normally be done by SOC part during "configure_clocks()" step.
    if parameters.has_key("ca9_freq"):
        configure_avs_abb(parameters)
    else:
        if corecfg.SYSTEM_STATUS5568.if_eq(0xf << 28, 0): # cut1
            parameters["ca9_freq"] =  int(parameters["a9_freq_c1"])
            configure_avs_abb(parameters)
        else:
            parameters["ca9_freq"] = int(parameters["a9_freq_c2"])
            configure_avs_abb(parameters)
        del parameters["ca9_freq"] # To force <soc>.py to auto detect

    # Some generic w/a needed
    stxh410.configure_soc_wa()

    cb = sttp.targetpack.get_callback("pre_poke")
    if cb:
        cb(parameters)

    cb = sttp.targetpack.get_callback("setup_pokes")
    if cb:
        cb(parameters)
    else:
        sttp.logging.print_out("")
        if pio_debug:
            stxh410_pio.toggle(1, 3, 10)
        stxh410.display_chip()
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            if corecfg.SYSTEM_STATUS5561.if_eq(0x78, 0xd << 3): # If SPI-div4 or SPI-div2 boot
                configure_spi(parameters)
        stxh410.configure_clocks(parameters)
        stxh410.display_clocks()
        stxh410.configure_mmc_wa()
        # DDR sub-system configuration
        if not (parameters.has_key("no_lmi_config") and (int(parameters["no_lmi_config"]) == 1)):
            configure_ddrss()
        if (sscg_ddr == 1):
            stxh410.sscg_enable_ddr(1)

        # Interco/plugs default settings
        if (on_emu == 0): # Not all IP's are present on emulator
            if not (parameters.has_key("no_icinit") and (int(parameters["no_icinit"]) == 1)):
                if uart_debug:
                    stxh410_uart.write("TP: Setting interco\n\r")
                stxh410_interco_init.init(parameters)

        # Configure IRQ pack when required
        stxh410.configure_irqs()

        # Configure HADES RAB/STM
        if parameters.has_key("hades_rab_init") and 1==int(parameters["hades_rab_init"]):
            sttp.logging.print_out( " Configure RAB\n ")
            stxh410_hades.setup_rab(0x80000000, 0x40000000, 0x10000000)
            stxh410_hades.setup_stm_port()
            sttp.logging.print_out( " Config RAB End\n ")

        # Powering down unused IP that are not OFF by default
        if (parameters.has_key("powerdown") and (1 == int(parameters["powerdown"]))):
            stxh410_powerdown.powerdown(parameters)

    cb = sttp.targetpack.get_callback("post_poke")
    if cb:
        cb(parameters)

# SPI configuration. To be done before clockgens to prevent non working default speed. Numonyx SPI flash on B2120 board.
def configure_spi(parameters):

    sttp.logging.print_out("Configuring SPI-Flash ...")
    if uart_debug:
        stxh410_uart.write("TP: Configuring SPI\n\r")

    spi = sttp_root.stxh410.SPI.spi_regs

    if parameters.has_key("spi_4x") and (int(parameters["spi_4x"]) == 1):
        sttp.pokepeek.poke(0x902210c, 0x000000c6)   # SPI_FAST_SEQ_ADD_CFG      spi_base + 0x10c
        sttp.pokepeek.poke(0x9022110, 0x00010806)   # SPI_FAST_SEQ_OPC1         spi_base + 0x110
        sttp.pokepeek.poke(0x9022114, 0x00000881)   # SPI_FAST_SEQ_OPC2         spi_base + 0x114
        sttp.pokepeek.poke(0x9022118, 0x000008EB)   # SPI_FAST_SEQ_OPC3         spi_base + 0x118
        sttp.pokepeek.poke(0x902211c, 0x00000861)   # SPI_FAST_SEQ_OPC4         spi_base + 0x11c
        sttp.pokepeek.poke(0x9022120, 0x00000000)   # SPI_FAST_SEQ_OPC5         spi_base + 0x120
        sttp.pokepeek.poke(0x9022148, 0x41132111)   # SPI_QUAD_BOOT_SEQ_INIT1   spi_base + 0x148
        sttp.pokepeek.poke(0x902214c, 0x00001823)   # SPI_QUAD_BOOT_SEQ_INIT2   spi_base + 0x14c
        sttp.pokepeek.poke(0x9022150, 0x05041231)   # SPI_QUAD_BOOT_READ_SEQ1   spi_base + 0x150
        sttp.pokepeek.poke(0x9022154, 0x00000F26)   # SPI_QUAD_BOOT_READ_SEQ2   spi_base + 0x154
        sttp.pokepeek.poke(0x9022124, 0x00C20010)   # SPI_MODE_BITS             spi_base + 0x124
        sttp.pokepeek.poke(0x9022128, 0x00C60000)   # SPI_DUMMY_BITS            spi_base + 0x128
        sttp.pokepeek.poke(0x902212c, 0x0010cb8b)   # SPI_FAST_SEQ_FLASH_STA_DATA   spi_base + 0x12c
        sttp.pokepeek.poke(0x9022164, 0x00042710)   # SPI_STATUS_WR_TIME        spi_base + 0x164
        sttp.pokepeek.poke(0x9022140, 0x000300C1)   # SPI_FAST_SEQ_CFG          spi_base + 0x140

        spi.SPI_CLOCKDIV.poke(0x00000002)
        spi.SPI_CONFIGDATA.poke(0x00080041)
        spi.SPI_STATUSMODECHANGE.while_and_ne(0x00000001, 0x1)
        spi.SPI_MODESELECT.poke(0x00000018) # FSM mode booting
    else:
        spi.SPI_CLOCKDIV.poke(0x00000002)
        spi.SPI_CONFIGDATA.poke(0x00020011)
        spi.SPI_STATUSMODECHANGE.while_and_ne(0x00000001, 0x1)
        spi.SPI_MODESELECT.poke(0x00000002) # fast + contig

    return

################################################
#                                              #
#              DDRSS CONFIGURE                 #
#                                              #
################################################
def configure_ddrss():

    sttp.logging.print_out("Configuring DDR sub-system ...")
    if uart_debug:
         stxh410_uart.write("TP: Configuring DDR\n\r")
    sttp.logging.print_out("  LMI Width = %dbits" % (lmi_width))
    sttp.logging.print_out("  LMI Size  = %dMBytes" % (lmi_size))

    ddrparams = upctl_ddrsetup.ddr_struct_def()
    ddrparams["on_emu"]=on_emu
    # This parameter is defined only for Cannes 2 as some modifications done in stxh410_ddrparams.correct_upctlphy_reg_values are cut dependent
    ddrparams = stxh410_ddrparams.set_ddr_params(ddrparams)
    ddrparams["lmi_width"] = lmi_width
    ddrparams["lmi_start"] = lmi_start
    ddrparams["lmi_size"] = lmi_size
    ddrparams["ddr_debug"] = ddr_debug
    ddrparams["ddrchip_density"] = 4 # can be 512, 1, 2, 4, 8 for 512Mb, 1Gb, 2Gb, 4Gb, 8Gb

    # The next parameter define the speed grade of the DDR chip(s) used on the lmi interface:
    # "800-5" for DDR3-800D (5-5-5)
    # "800-6" for DDR3-800E (6-6-6)
    # "1066-6" for DDR3-1066E (6-6-6)
    # "1066-7" for DDR3-1066F (7-7-7)
    # "1066-8" for DDR3-1066G (8-8-8)
    # "1333-7" for DDR3-1333F (7-7-7)
    # "1333-8" for DDR3-1333G (8-8-8)
    # "1333-9" for DDR3-1333H (9-9-9)
    # "1333-10" for DDR3-1333J (10-10-10)
    # "1600-8" for DDR3-1600G (8-8-8)
    # "1600-9" for DDR3-1600H (9-9-9)
    # "1600-10" for DDR3-1600J (10-10-10)
    # "1600-11" for DDR3-1600K (11-11-11)
    # "1866-10" for DDR3-1866J (10-10-10)
    # "1866-11" for DDR3-1866K (11-11-11)
    # "1866-12" for DDR3-1866L (12-12-12)
    # "1866-13" for DDR3-1866M (13-13-13)
    # "2133-11" for DDR3-2133K (11-11-11)
    # "2133-12" for DDR3-2133L (12-12-12)
    # "2133-13" for DDR3-2133M (13-13-13)
    # "2133-14" for DDR3-2133N (14-14-14)
    if(boardrev>=3):                        # B2120C is mounted with DDR3-2133 memory devices
        ddrparams["ddrchip_speedgrade"] = "2133-14"
        ddrparams["TCLMAX"]=14          # Those 3 parameters are only needed for DDR overclocking
        ddrparams["TCWLMAX"]=10
        ddrparams["MAXTRPTCLDIFF"]=0
    else:                                   # previous versions of boards are with DDR3-1866
        ddrparams["ddrchip_speedgrade"] = "1866-13"
        ddrparams["TCLMAX"]=13          # Those 3 parameters are only needed for DDR overclocking
        ddrparams["TCWLMAX"]=10
        ddrparams["MAXTRPTCLDIFF"]=1
    ddrparams["ddrchip_tcase"] = 85 # can be 85 or 95 for 85 Celcius degree or 95 Celcius degree (half refresh interval for high temperature range
    ddrparams["ddrchip_ZQcalFreq"] = 10 # specify the number of time per second the impedance calibration is done at DDR chip device side, 0 to disable
    ddrparams["vdd_ddr"] = 0            # 0 for DDR3 (1V5) and 1 for DDR3L (1V35)

    ############### IMPEDANCE SETTINGS controller side ##################
    #SELECT impedance values (For impedance dividing)
    ddrparams["ODT_DIV"] = ddrparams["ODT_60_OHMS"]     # set to ODT_xx_OMHS where xx= 40, 60 or 120
    ddrparams["ZO_DIV"]  = ddrparams["ZO_40_OHMS"]      # set to ZO_xx_OHMS where xx= 34 or 40
    #SELECT Pull-up or Pull-down resistor values on DQS/DQSN
    ddrparams["DQSRES"]  = ddrparams["PD_500_OHMS"]     # set to PD_xxx_OHMS where xxx= 344, 393, 458, 500, 550, 611 or 688 or PD_OPEN
    ddrparams["DQSNRES"] = ddrparams["PU_500_OHMS"]     # set to PU_xxx_OHMS where xxx= 344, 393, 458, 500, 550, 611 or 688 or PU_OPEN

    ############# IMPEDANCE SETTINGS for DDR device ################
    #SELECT impedance dividers for DDR device
    ddrparams["RTT_NOM"] = ddrparams["RTT_NOM_60_OHMS"]     # set to RTT_NOM_xx_OHMS where xx= 20, 30, 40, 60, 120 or RTT_NOM_DISBALED. RTT_NOM set to have RTT activated during write leveling Synopsys Case 8000697597
    ddrparams["RTT_WR"]  = ddrparams["RTT_WR_60_OHMS"]      # set to RTT_WR_xx_OHMS where xx= 60 or 120 or RTT_WR_DYN_ODT_DIS
    ddrparams["ZO_DDR"]  = ddrparams["ZO_DDR_40_OHMS"]      # set to ZO_DDR_xx_OHMS where xx= 34, 40 or 60

    ############# Parameters for exit CPS ################
    # All those parameters have to be updated when the HOM structure for CPS management will be well defined!!!!!
    ddrparams["retn_piobankadd"] = 0x09615000   # Address of the Pout register that controls the bank of PIO where LMI_NOTRETENTION signal is connected
    ddrparams["retn_pionumber"] = 2             # PIO number in the bank on which LMI_NOTRETENTION signal is connected
    ddrparams["retn_piotype"] = "PUSH_PULL"     # If the PIO is connected to a pull-up to VDD_DDR => "OPEN_DRAIN", if the PIO is connected to a resistor divider bridge => "PUSH_PULL"
    ddrparams["HoMStruct_Add"] = 0x09400000     # Address in SBC RAM of the data structure containing all information needed for wake-up from suspend to RAM
    ddrparams["HoMTagOffset"] = 0x000B5120      # Offset of the Tag signaling exit from stand-by in the HoM structure
    ddrparams["HoMTagMask"] = 0x1FF             # Mask to be applied on the Tag to check its validity
    ddrparams["HoMTagValid"] = 0x09B            # Valid TAG value checked to enable the exit from stand-by functionality
    ddrparams["ZQ0SR0Offset"] = 0x80010098      # If ZQ0SR0 is saved by system before entering stand-by, put '1' on bit 31 and indicate the offset in the HoM Structure of the saved value in the LSB
    ddrparams["HoMTagUpdateAND"] = ~(0)         # At the end of the exit from stand-by function, the Tag may be updated, this value is ANDed with the existing value in the structure
    ddrparams["HoMTagUpdateOR"] = (0)           # and this value is ORed
    if (dtu_fixed == 1):
        ddrparams["DTUAddress"] = dtu_address   # Fixed Address to be used for the Data training
        ddrparams["DTUPCTLAddOffset"] = 0       # Discard DTU address from application: put '0' on bit 31
        ddrparams["DTUPHYAddOffset"] = 0        # Discard DTU address from application: put '0' on bit 31
    else:
        ddrparams["DTUAddress"] = lmi_start<<24     # Fixed Address to be used for the Data training (normally never used as address fixed by the application)
        ddrparams["DTUPCTLAddOffset"] = 0x80010094  # If the application fixes the DTU address, put '1' on bit 31 and indicate the offset in the HoM Structure of the address saved in DTUWACTL format
        ddrparams["DTUPHYAddOffset"] = 0x8001009C   # If the application fixes the DTU address, put '1' on bit 31 and indicate the offset in the HoM Structure of the address saved in DTAR0 format
    ddrparams = upctl_ddrsetup.setCHIPDDRddrparams(ddrparams)
    if (ddrparams["error"] <> ""):
        raise sttp.STTPException("ddrss_configure error\n%s\n" % ddrparams["error"])

    ddrparams = upctl_ddrsetup.set_upctlphy_reg_values(ddrparams)
    if (ddrparams["error"] <> ""):
        raise sttp.STTPException("ddrss_configure error\n%s\n" % ddrparams["error"])

    upctl_ddrsetup.reset_ddrss(ddrparams)
    ddrparams = stxh410.configure_mixer(ddrparams)  # init mixer
    upctl_ddrsetup.configure_ddrss(ddrparams)

    # Let's test DDR if requested
    if (ddr_test):
        upctl_ddrdbg.PCTLDTU_test(ddrparams,1)
    if (ddrparams["error"] <> ""):
        raise sttp.STTPException("ddrss_configure error\n%s\n" % ddrparams["error"])

    return

# Configure Adaptative Voltage Scaling and Adaptative Body Biasing.
def configure_avs_abb(parameters):
    sttp.logging.print_out("\nConfiguring AVS and ABB ...")
    ca9_freq = int(parameters["ca9_freq"])


    # Parameters initiailization
    Core_OPTIONS_params = stxh410_avs_abb.OPTIONS_struct_def("Core",parameters)
    A9_OPTIONS_params= stxh410_avs_abb.OPTIONS_struct_def("A9",parameters)

    ##################### BOARD CUSTOMIZATION section ##############################
    # AVS PIO definition
    if (boardrev == 1):
        Core_OPTIONS_params["PWM"] = 11
    else:
        Core_OPTIONS_params["PWM"] = 10
    A9_OPTIONS_params["PWM"]  = 13

    if (boardrev == 4): # extended range
        A9_OPTIONS_params["Vmin"] = 784 # PWM code 255
        A9_OPTIONS_params["Vmax"] = 1299 # PWM code 0
    else:
        A9_OPTIONS_params["Vmin"] = 850 # PWM code 255
        A9_OPTIONS_params["Vmax"] = 1147 # PWM code 0
        # AVS tables modified for old board revision support. these values must not be used on customer platforms. Risk of instability for A9 frequency > 1.2 Ghz
        # A9    1500 MHz               AVS      1.150    1.150    1.150    1.150    1.150    1.140    1.100    1.070
        A9_OPTIONS_params["avs_a9_1500"] =  [ 1150, 1150, 1150, 1150, 1150, 1140, 1100, 1070 ]
        #                                    ABB      FBB 250  FBB 300  FBB 300  FBB 250  FBB 200  OFF      OFF      OFF
        A9_OPTIONS_params["abb_a9_1500"] = [ 0x344, 0x355, 0x355, 0x344, 0x333, 0x0, 0x0, 0x0 ]
        #Read process information from non volatile memory
        A9_OPTIONS_params["avs_a9_1200"] = [ 1112, 1150, 1100, 1080, 1040, 1020, 980, 930 ]
        #sttp.logging.print_out("\n")
        A9_OPTIONS_params["abb_a9_1200"] = [ 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 ]
        if ((ca9_freq > 1200) and (boardrev < 4)):
            sttp.logging.print_out("\t WARNING - SYSTEM MAY BE INSTABLE")
            sttp.logging.print_out("\t Boards b2120 version A, B and C are not able to provide the required voltages for slow parts with A9 frequency higher than 1.2Ghz.")

    if (parameters.has_key("vcore_hi_range") and (int(parameters["vcore_hi_range"]) == 1)):
        sttp.logging.print_out("Forcing vcore_hi_range") # 1070=>255, 1380=>0
        Core_OPTIONS_params["Vmin"] = 1070 # PWM code 255
        Core_OPTIONS_params["Vmax"] = 1380 # PWM code 0
    else:
        Core_OPTIONS_params["Vmin"] = 853 # PWM code 255
        Core_OPTIONS_params["Vmax"] = 1151 # PWM code 0

    ##################### end of BOARD CUSTOMIZATION section ###########################
    
   # AVS  configuration
    stxh410_avs_abb.set_avs(parameters, Core_OPTIONS_params)
    stxh410_avs_abb.set_avs(parameters, A9_OPTIONS_params)
    # ABB configuration
    stxh410_avs_abb.set_abb(parameters, A9_OPTIONS_params)

    # wait 10ms for voltages to stabilize
    sttp.stmc.delay(10000)

    if ((sttp.stmc.get_operating_mode() != "ROMGEN") and avs_debug):
        stxh410_avs_abb.AVS_ABB_debug(parameters)

    return

