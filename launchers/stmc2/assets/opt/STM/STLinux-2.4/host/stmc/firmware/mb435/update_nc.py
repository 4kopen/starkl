#!/usr/bin/python

# JFFS upgrade method is to flash new jffs image, and then
# duplicate current IP settings in the alternate, upgraded
# JFFS.

# This script checks current partition for network 
# configuration and duplicates it in the upgraded 
# partition.

import sys
import os
import os.path
import re

if os.path.exists("/etc/stmc/network_config.sh"):
    os.system("/usr/sbin/upgrade_copy.sh /etc/stmc/network_config.sh /mnt/network_config.sh")

# Create a persistent log.
os.system("tail -200 /var/log/messages > /tmp/upgrade/upgrade.log")
os.system("/usr/sbin/upgrade_copy.sh /tmp/upgrade/upgrade.log /mnt/upgrade.log")

