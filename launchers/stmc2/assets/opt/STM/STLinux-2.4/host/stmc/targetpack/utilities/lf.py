import sttp
import time

# maximum STDI buffer size is 0x200000
stdi_w_payload = 1024*1024*2 - 6*4


def lf(filename, addr):
    sttp.logging.print_out("Loading %s file to 0x%x" % (filename, addr))
    fd = open(filename, "rb")
    
    fd.seek(0, 2)   # Move to EOF
    size = fd.tell()
    sttp.logging.print_out("  Size : %d bytes " % size)
    
    fd.seek(0, 0)   # Move to BOF 

    start_global = time.time()
    while (size >= stdi_w_payload):
        buf = fd.read(stdi_w_payload)
        start = time.time()
        sttp.pokepeek.poke_bytes(addr, buf, stdi_w_payload)
        end = time.time()
        sttp.logging.print_out("    loaded %d bytes, took %f seconds" % (stdi_w_payload, end-start))

        size -= stdi_w_payload
        addr += stdi_w_payload
    
    if (size > 0):
        buf = fd.read(size)
        start = time.time()
        sttp.pokepeek.poke_bytes(addr, buf, size)
        end = time.time()
        sttp.logging.print_out("loaded %d bytes, took %f seconds" % (size, end-start))
 
    end_global = time.time()
    sttp.logging.print_out("Total time %f seconds" % (end_global-start_global))
    fd.close()
