import time

import sttp
import sttp.stmc
import sttp.targetpack

import arm_a9

import tap
import tap.jtag

def connect(parameters):

    tap.jtag.setup_jtag_debug_port(parameters)

    if parameters.has_key("post_reset_delay"):
        time.sleep(int(parameters["post_reset_delay"]))

    arm_a9.init({"arm_core_name"     : "a9", 
                 "index_prefix"      : "_",
                 "arm_cluster_count" : 2},
                 parameters)    


def complete_connect(parameters):

    pass
