# Author: David Roubinet
# Date  : Jun15
#
import urllib2
import base64
token = 'uA9f8qKoZSDKvvA59Huu' # Jani Thor

try: 
    import json
except:
    import re
    class json():
        def load(self,fileObj):
            txt = fileObj.read()
            txt = re.sub(r'\bnull\b' ,'None' ,txt)
            txt = re.sub(r'\bfalse\b','False',txt)
            txt = re.sub(r'\btrue\b' ,'True' ,txt)
            return eval(txt)
    json = json()

def get(cmd):
    req = urllib2.Request("http://smalldata.rou.st.com/api/v3/"+cmd)
    req.add_header('PRIVATE-TOKEN',token)
    rsp =  urllib2.urlopen(req)
    if rsp.info().type=='application/json': return json.load(rsp)
    else:
        print "Sorry! Command failed"
        return None

prjs = get('projects')
id = None
for prj in prjs:
    if prj['name']=='chips_seckeys':
        id = prj["id"]

blob = get('projects/%r/repository/files?ref=master&file_path=keyring.py'%(id))
code = base64.b64decode(blob['content'])
exec(code)