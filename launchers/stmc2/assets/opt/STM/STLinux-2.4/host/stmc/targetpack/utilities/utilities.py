import sys

import sttp
import sttp.stmc
import sttp.targetinfo

#
# Import a module passed in a specified parameter key
#

def import_user_module(parameters):

    k = "module_function"
    if parameters.has_key(k):
        # Import will search the search path - use sttp.xml to add extra directories

        n = parameters[k]

        mod_list = n.split(":")
        for mod in mod_list:
            x = __import__(mod)
            x.__dict__[mod](parameters)


def set_tck_frequency(initialize, parameters):
    # We cannot run the ST40 core's tck faster than the core
    # clock and when initializing in x1 mode, this may be as low 27MHz/8
    # In TAPmux mode the tck is divided by 8 to get the ST40 core tck.

    # On STMC2 use the API sttp.stmc.set_tck_frequency() to set the tck frequency based on the supplied
    # argument. The parameter "tck_frequency" if set will to override the supplied argument.
    # The STMC2 will choose the GREATEST frequency that LESS THAN OR EQUAL to the requested value.
    # and the chosen value is returned by sttp.stmc.set_tck_frequency().
    # The API sttp.stmc.set_tck_frequency_no_override() does NOT use the parameter "tck_frequency"
    # to override the supplied argument.

    # On STMC1 sttp.stmc.set_tck_frequency() does not work - we set the parameter "linkspeed"
    # No way of discovering what TCK has actually been selected - this is internal to the ST40 toolset

    def get_init_frequency():
        tck_init_param_key = "tck_init_frequency"
        tck_init_tp_key    = "tck_init_max"

        if parameters.has_key(tck_init_param_key):
            tck_init = int(parameters[tck_init_param_key])
        elif p.has_key(tck_init_tp_key):
            tck_init = int(p[tck_init_tp_key])
        else:            
            tck_init = 3000000
        return tck_init

    p = sttp.stmc.get_target_params()

    if parameters.has_key("use_external_clock") and 1==int(parameters["use_external_clock"]):
        if sttp.stmc.get_stmc_type() == "STMC1":
            sttp.logging.print_out("External clock option is not supported with a ST Micro Connect 1")
            raise sttp.STTPException("External clock option is not supported with a ST Micro Connect 1")
        else:
            sttp.stmc.use_external_clock()
            sttp.targetinfo.set_target_info("tck_frequency", 0)
            sttp.logging.print_out("Using external clock to generate tck")
            return

    if initialize:
        # TCK to ST40 core must be slow until ClockGen setup to run CPU at full speed
        # Assume worst case where system is in x1 mode (core clock == 27 Mhz)

        tck_init = get_init_frequency()

        if sttp.stmc.get_stmc_type() == "STMC1":
            p["linkspeed"] = str(tck_init)
            real_tck = tck_init
        else:
            real_tck = sttp.stmc.set_tck_frequency_no_override(tck_init)

        sttp.logging.print_out("Initialization TCK frequency set to %d Hz" % (real_tck,))

    else:
        # Note : Once In TAPmux mode NEVER EVER change tck
        # Obtain the preferred TCK frequency

        if ((parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)) and \
            (not parameters.has_key("no_reset")) ):
            # If clockgen etc not configured, Must TCK leave in "slow" mode for GDB to be able to communicate with SoC
            tck = get_init_frequency()
        elif sttp.stmc.get_stmc_type() == "STMC1":
            # Bypass with STMC1 mode
            tck = int(p["tck_stmc1_max"])
        elif parameters.has_key("tapmux_mux") and (int(parameters["tapmux_mux"]) == 1):
            # TAPmux mode
            tck = int(p["tck_tapmux_max"])
        elif p.has_key("tck_bypass_max"):
            # STMC2 Bypass mode
            tck = int(p["tck_bypass_max"])
        else:
            # STMC2 "normal (non TAPmux device)" mode
            tck = int(p["tck_max"])
        
        # Now set the frequency            
        if sttp.stmc.get_stmc_type() == "STMC1":
            if parameters.has_key("tck_frequency"):
                tck = int(parameters["tck_frequency"])
            p["linkspeed"] = str(tck)
            real_tck = tck
        else:
            real_tck = sttp.stmc.set_tck_frequency(tck)

        sttp.logging.print_out("TCK frequency set to %d Hz" % (real_tck,))

    sttp.targetinfo.set_target_info("tck_frequency", real_tck)
    sttp.stmc.set_target_params(p)

