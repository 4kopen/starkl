# Functions specific to the stv0498

# Import ST supplied libraries

import sttp
import sttp.pokepeek
import sttp.stmc
import sttp.targetpack

import tapmux

def connect(parameters):
    """
    Performs all stv0498 SoC connect actions
    Must be used prior to performing board-level intiialization
    """
    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    tapmux.connect(False, parameters)

