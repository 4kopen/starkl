# Script for mb421 with stx8010



import sttp
import sttp.logging

# TargetPack imports
import stx8010
import utilities

def mb421_connect(parameters):

    sttp.logging.print_out("mb421 connect start - parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    p = sttp.stmc.get_target_params()
    # Add the board TargetPack parameters
    p.update(sttp.targetpack.get_tree().parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(True, parameters)

    stx8010.connect(parameters)

    sttp.logging.print_out("Completed stx8010 connect")

    if parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1):
        return

    sttp.pokepeek.enable()

    sttp.logging.print_out("mb421 initialization start ...")
    mb421_initall()

    utilities.set_tck_frequency(False, parameters)

    # NEVER EVER change tck after this call is made
    stx8010.complete_connect(parameters)

    sttp.logging.print_out("mb421 initialization complete")

def mb421_disconnect():
    pass

def mb421_initall():
    """
    This function was hand-converted from 
    st200-r4.1-solaris/target/board/mb421/board.txt
    """

    mb421_init_from_toolset()

class ScriptException(Exception):
    pass

poke = sttp.pokepeek.poke
peek = sttp.pokepeek.peek

def mb421_init_from_toolset():
    """
    This is hand converted from st200-r4.1-linux/target/board/mb421_host/clock_mem.txt
    """
    # Value to bew returned by peeks when in an environment without a real target
    # sttp.pokepeek.debug_peek(0)

    # Setup Interface subsystem
    data = peek(0x57014000 + 0x10)
    data = ( data | (0x3FF<<16) | (0x17C<<6) | (0x1<<5) )
    poke(0x57014000 + 0x10, data)

    sttp.logging.print_out(" # Configuring PLL 1")

    data = peek(0x56000000 + 0x00)
    data = ( data | (1 << 29) )
    poke(0x56000000 + 0x00, data)

    setup = 0x000
    pdiv = 0x0
    ndiv = 0x28
    mdiv = 0x03

    data = ( data & 0xE0000000 )
    data = ( data | (mdiv << 0) )
    data = ( data | (ndiv << 8) )
    data = ( data | (pdiv << 16) )
    data = ( data | (setup << 19) )
    poke(0x56000000 + 0x00, data)

    data = peek(0x56000000 + 0x00)
    data = ( data | (1 << 24) )
    poke(0x56000000 + 0x00, data)

    data = peek(0x56000000 + 0x00)
    data = ( data & 0xFEFFFFFF )
    poke(0x56000000 + 0x00, data)

    data = peek(0x56000000 + 0x00)
    data = ( data & 0xDFFFFFFF )
    poke(0x56000000 + 0x00, data)

    data = 0
    timeout = 50
    while ((data & 0x80000000) == 0) and timeout:
        timeout = timeout - 1
        data = peek(0x56000000 + 0x00)

    if timeout == 0:
        sttp.logging.print_out("ERROR - Timeout waiting for PLL_1 to Lock")

    poke(0x56000000 + 0x48, 0x00000001)

    data = peek(0x56000000 + 0x48)
    data = ( data | (1 << 6) )
    poke(0x56000000 + 0x48, data)

    sttp.logging.print_out(" # Configuring PLL 2")
    data = peek(0x56001000 + 0x00)
    data = ( data | (1 << 29) )
    poke(0x56001000 + 0x00, data)

    setup = 0x000
    pdiv = 0x0
    ndiv = 0x28
    mdiv = 0x09

    data = ( data & 0xE0000000 )
    data = ( data | (mdiv << 0) )
    data = ( data | (ndiv << 8) )
    data = ( data | (pdiv << 16) )
    data = ( data | (setup << 19) )
    poke(0x56001000 + 0x00, data)

    data = peek(0x56001000 + 0x00)
    data = ( data | (1 << 24) )
    poke(0x56001000 + 0x00, data)

    data = peek(0x56001000 + 0x00)
    data = ( data & 0xFEFFFFFF )
    poke(0x56001000 + 0x00, data)

    data = peek(0x56001000 + 0x00)
    data = ( data & 0xDFFFFFFF )
    poke (0x56001000 + 0x00, data)

    data = 0
    timeout = 50
    while ((data & 0x80000000) == 0) and timeout:
        timeout = timeout - 1
        data = peek(0x56001000 + 0x00)

    if timeout == 0:
        sttp.logging.print_out("ERROR - Timeout waiting for PLL_2 to Lock ")

    poke(0x540A3000 + 0x20, 0x00)
    poke(0x540A3000 + 0x30, 0x80)
    poke(0x540A3000 + 0x40, 0x00)
    poke(0x540A3000 + 0x00, 0x00)


    sttp.logging.print_out(" # Configuring EMI for 16-bit data")

    poke(0x47000000 + 0x860, 0x00000002)
    poke(0x47000000 + 0x800, 0x00000000)
    poke(0x47000000 + 0x810, 0x00000014)

    sttp.logging.print_out("   - Configure Bank0 ")

    poke(0x47000000 + 0x100, 0x003017F1)
    poke(0x47000000 + 0x108, 0x9D112200)
    poke(0x47000000 + 0x110, 0x9D112200)
    poke(0x47000000 + 0x118, 0x00000000)

    sttp.logging.print_out("   - Configure Bank1 for Atapi")

    poke(0x47000000 + 0x140, 0x00138791)
    poke(0x47000000 + 0x148, 0x13123200)
    poke(0x47000000 + 0x150, 0x1F123202)
    poke(0x47000000 + 0x158, 0x00000000)

    data = peek( 0x47000000 + 0x028 )
    data = ( data | (1 << 9) | (1 << 8) )
    poke(0x47000000 + 0x028, data)

    poke(0x00900000, 0x000E0000)
    poke(0x540A3000 + 0x00, 0x80)

    sttp.logging.print_out(" # Configuring IFSS")
    data = peek(0x57014000 + 0x18)
    data = ( data | (0x2<<18) | (0x08<<10) | (0x04<<1) | (0x1<<0) )
    poke(0x57014000 + 0x18, data)

    sttp.logging.print_out(" # Reset DLL's ")
    data = peek( 0x5F000000 + 0x28 )
    data = ( data | (1 << 1) | (1 << 0) )
    poke(0x5F000000 + 0x28, data)

    data = peek( 0x5F000000 + 0x40 )
    if (data & 0x00000003) != 0x0:
        sttp.logging.print_out("ERROR - DLL's NOT Reset ")

    data = peek( 0x5F000000 + 0x28 )
    data = ( data & 0xFFFFFFFC )
    poke(0x5F000000 + 0x28, data)

    timeout = 50
    while ((data & 0x00000003) != 0x00000003) and timeout:
        timeout = timeout - 1
        data = peek( 0x5F000000 + 0x40 )

    if timeout == 0:
        sttp.logging.print_out("ERROR - Timeout waiting for DLL's to Lock ")

    sttp.logging.print_out(" # Configuring SYSCONF Registers ")
    data = peek(0x50003000 + 0x14)
    data = ( data | (1 << 9) )
    poke(0x50003000 + 0x14, data)

    data = peek(0x50003000 + 0x18)
    data = ( data | (1 << 23) | (1 << 18) )
    poke(0x50003000 + 0x18, data)

    data = peek(0x50003000 + 0x1C)
    data = ( data | (1 << 6) | (1 << 1) )
    poke(0x50003000 + 0x1C, data)

    sttp.logging.print_out(" # Configure LMI COC ")
    sttp.logging.print_out("   For Cut 1.0: Use MANUAL mode and Coarse Delay = 0.")

    data = peek( 0x5F000000 + 0x28 )
    data = ( data & 0xFFF01FFF )

    data = ( data | (3<<18) | (0<<13) )
    poke(0x5F000000 + 0x28, data)

    data = peek( 0x5F000000 + 0x28 )
    data = ( data & 0xFFFFFE03 )

    data = ( data | (3<<7) | (0<<2) )
    poke(0x5F000000 + 0x28, data)

    poke(0x00900000, 0x000D0000)

    sttp.logging.print_out(" # Configuring LMI for 32-bit data, CAS 3 ")
    poke(0x5F000000 + 0x08, 0x0514025B)
    poke(0x5F000000 + 0x0C, 0x00000054)
    poke(0x5F000000 + 0x18, 0x35B06455)
    poke(0x5F000000 + 0x30, 0x84001903)
    poke(0x5F000000 + 0x38, 0x84001903)

#   dpeek 0 32
#   dpeek 0 32
#   dpeek 0 32

    poke(0x5F000000 + 0x10, 0x3)
    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x2)
    poke(0x5F000000 + 0x10, 0x1)

    poke(0x5F000000 + 0x48, 0x400)
    poke(0x5F000000 + 0x50, 0x400)

    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x48, 0x0133)
    poke(0x5F000000 + 0x50, 0x0133)

    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x2)
    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x4)
  
    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x1)

    poke(0x5F000000 + 0x10, 0x4)


    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x1)
    poke(0x5F000000 + 0x10, 0x1)

    poke(0x5F000000 + 0x48, 0x0033)
    poke(0x5F000000 + 0x50, 0x0033)

    poke(0x5F000000 + 0x10, 0x1)

    poke(0x5F000000 + 0x10, 0x00040000)

    poke(0x00900000, 0x000B0000)

    sttp.logging.print_out(" # Configuring ST230")

    poke (0x52000000 + 0x0248, 132)

    sttp.logging.print_out(" # Configuring Frequency Synthesiser")

    poke(0x56002000 + 0x00, 0x1004)
    poke(0x56002000 + 0x00, 0x1F0D)

    ## For HD (HDMI)
    ## poke(0x56002000 + 0x28, 0x06224359)
    ## For SD (S-Video)
    poke(0x56002000 + 0x28, 0x06639091)

    sttp.logging.print_out(" # ClockOut can be found on J6 Or Con18,Pin25")
    sttp.logging.print_out(" # Setting STBus Priority")

    data = peek( (0x50000000 + 0x0000) + 0x000 )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (5 << 0) )
    poke((0x50000000 + 0x0000) + 0x000, data)

    data = peek( (0x50000000 + 0x0000) + 0x004 )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (3 << 0) )
    poke((0x50000000 + 0x0000) + 0x004, data)

    data = peek( (0x50000000 + 0x0000) + 0x008 )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (4 << 0) )
    poke((0x50000000 + 0x0000) + 0x008, data)

    data = peek( (0x50000000 + 0x0000) + 0x00C )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (2 << 0) )
    poke((0x50000000 + 0x0000) + 0x00C, data)

    data = peek( (0x50000000 + 0x0000) + 0x010 )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (1 << 0) )
    poke((0x50000000 + 0x0000) + 0x010, data)

    data = peek( (0x50000000 + 0x0000) + 0x014 )
    data = ( data & 0xFFFFFFF8 )
    data = ( data | (0 << 0) )
    poke((0x50000000 + 0x0000) + 0x014, data)

    poke(0x00900000, 0x000F0000)
    poke(0x540A3000 + 0x00, 0x00)

    sttp.logging.print_out(" # mb421 config complete")
