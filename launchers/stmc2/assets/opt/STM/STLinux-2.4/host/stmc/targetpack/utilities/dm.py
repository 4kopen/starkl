import sttp
import struct
import time

# Dump "size" bytes of target memory at "addr". 
# The read accesses are performed via DBU on the target.
# Current supported output unit is of 32bits.
# If the output file is omitted, the data read are printed on stdout.
def dm(addr, size, filename=None):
    global fd
    max_read_size = 1024*1024*2 - 8  # in byte
    aligned = size % 4
    if (aligned != 0):
        size = size + (4 - aligned)
        sttp.logging.print_out("Size is aligned to 0x%x " % size)

    if (filename != None):
        try:
            fd = open(filename, "wb")
        except Exception:
            raise sttp.STTPException("Error: Cannot open '%s'\n" % filename)

    sttp.logging.print_out("Dump %d bytes of target memory at @ 0x%x." % (size, addr))
        
    remain_size = size
    read_addr = addr
    start_global = time.time()
    while True:
        if (remain_size > max_read_size):  #if size > 2MB
            read_size = max_read_size
        else:
            read_size = remain_size
        
        start = time.time()
        bytes_str = sttp.pokepeek.peek_bytes(read_addr, read_size)
        
        for i in range(0,read_size,4):
            [v,] = struct.unpack("<L", bytes_str[i:i+4])
            if (filename != None):
                fd.write(struct.pack("L",v))
            else:
                sttp.logging.print_out("0x%08X : 0x%08X    " % (read_addr + i, v))
        
        end = time.time()
        sttp.logging.print_out("dumped %d bytes, took %f seconds" % (read_size, end-start))

        remain_size -= read_size
        
        if (remain_size <= 0):
            break

        read_addr += read_size
    
    end_global = time.time()
    sttp.logging.print_out("Total time %f seconds" % (end_global-start_global))

    if (filename != None):
        sttp.logging.print_out("Written to the file: %s" % filename)
        fd.close()
