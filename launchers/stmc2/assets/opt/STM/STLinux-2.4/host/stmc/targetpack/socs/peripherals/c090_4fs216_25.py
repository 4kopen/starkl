"""
c090_4fs216_25 (ADCS 7544946)

Description:
  The C090_4FS216_25 is a general purpose frequency synthesizer intended to be used for digital
clock generation from 843,75kHz to 216MHz. This cell generate four independent output frequency
Two input frequency clock is possible (27Mhz or 54Mhz) and two level 1.0v or 2.5v can be select.
A power down mode is offered to minimize the power consumption when the device is inactive.
Each output clocks is available at 1.0 volt and 2.5 volts.
The noise coming from the input clock can be reduce by select the PLL bandwidth
"""

f_in = 27000  # KHz
ndiv = 0      # v = 8 in function calculate

def setup(fin=27000):
    """
    Setup the frequency of the input clock for FS, the default value is 27000KHz(27MHz)
    """
    global f_in
  
    f_in = fin
    return


def check(freq):
    """
    Return True if freq in range[844KHz, v*f_in] (v = (ndiv==0) ? 8 : 4)
    """
    global ndiv
    global f_in
  
    v = (ndiv == 0) and 8 or 4
    if (freq < 844) or (freq > (v * f_in)):
        return False
  
    return True

  
def fout(pe, sdiv, md):
    """
    Calculate frequency as :
    pow(2, 15) * value * f_in / (funcSDIV(sdiv) * (pe * Decimal(1 + funcMD(md) / 32) - (pe - pow(2, 15)) * (1 + Decimal(funcMD(md) + 1) / 32)))
    """
    global ndiv
    global f_in
  
    v = (ndiv == 0) and 8 or 4
  
    funcSDIV = lambda x : 1 << (x + 1)
    funcMD = lambda x : x - 16
  
    # return pow(2, 15) * value * f_in / (funcSDIV(sdiv) * (pe * Decimal(1 + funcMD(md) / 32) - (pe - pow(2, 15)) * (1 + Decimal(funcMD(md) + 1) / 32)))
    # pow(2, 15) = 32768
    return 32768 * v * f_in / (funcSDIV(sdiv) * (pe * (1 + funcMD(md) * 1.0 / 32.0) - (pe - 32768) * (1 + (funcMD(md) + 1) * 1.0 / 32.0)))
    
def calculate(f):
    """
    Calculate PE, SDIV, MD for appointed frequency -- f
    return (f_out, pe, sdiv, md)
    md[4:0] = [-16, -1]
    sdiv[2:0] = [2, 256]
    pe[15:0] = [0, 2^15 - 1]
    """
    pe = 0
    sdiv = 0
    md = 0
    f_out = 0
    find = False
    md_save = 0
    sdiv_save = 0
    tmp = 0x10000000 # very huge
  
    global ndiv
    global f_in
  
    v = (ndiv == 0) and 8 or 4
  
    funcSDIV = lambda x : 1 << (x + 1)
    funcMD = lambda x : x - 16
    funcNoPE = lambda x, y : 32.0 * v * f_in / (funcSDIV(x) * 1.0 * (33 + funcMD(y) * 1.0))
  
    # calculating sdiv, md
    for md in range(0, 16, 1):
        sdiv = 0
        for sdiv in range(0, 8, 1):
            if find:
                break
            f_out = funcNoPE(sdiv, md)
            
            if(f_out == f):
                find = True
                break
            else:
                if f_out < f:
                    if (f - f_out) < tmp:
                        md_save = md
                        sdiv_save = sdiv
                        tmp = (f - f_out)
        if find:
            break
  
    if not find:
        # calculating pe
        for pe in range(0, 32768, 1):
            f_out = int(32768 * v * f_in / (funcSDIV(sdiv_save) * (pe * (1 + funcMD(md_save) * 1.0 / 32.0) - (pe - 32768) * (1 + (funcMD(md_save) * 1.0 + 1) / 32.0))))
            if (f_out == f) or ((f_out % f) <= 10):
                break
        sdiv = sdiv_save
        md = md_save
  
    return (f_out, pe, sdiv, md)


def transfer(setup0, setup1):
    """
    Get the frequency of clock from FS with the input registers setup0 and setup1
    """
    pe = setup1 & 0xFFFF
    sdiv = (setup0 >> 6) & 0x7
    md = setup0 & 0x0F # the bit 5 is '1'
  
    return fout(pe, sdiv, md)

  
if __name__ == "__main__":
    freq = 54000
    setup(30000)
    if not check(freq):
        print("Invalid frequency = %dKHz" % freq)
    f, pe, sdiv, md = calculate(freq)
    print("want %d, get %d, pe=%d, sdiv=%d, md=%d" % (freq, f, pe, sdiv, md))
    print("fout = %d" % (fout(pe, sdiv, md)))

   
