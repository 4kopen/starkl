import sys

import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

parameters = None

def get(i):
    v = sttp.jtag.sequence({})

    s = "lvds_in" + str(i)
    return v[s]

def set(i, v):
    s = "lvds_out" + str(i)
    l = str(v)
    sttp.jtag.sequence({s : l})

    return l

def do_power_up():
    sttp.logging.print_out("Enabling target power supply ...")
    set(12, 0)
    set(12, 1)
    sttp.stmc.delay(1000000)

def power_up(params):
    global parameters
    parameters = params

    sttp.targetpack.register_callback("post_init_jtag", do_power_up)
