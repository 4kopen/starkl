###
### Common clock functions (SOC independant)
### (C) F. Charpentier, CHD-Valid
###

# Revision history (most recent on top)
# 19/aug/13  FCh  FS660C32 parameters computation bug fix.
# 10/jul/13  FCh  Added FS660C32 parameters computation functions.
# 24/may/12  FCh  pll1600c45_get_phi_params() bug fix.
# 23/nov/11  FCh  Renamed to "clk_common2.py" due to API changes to avoid other TPs update.
#                 Purpose is to support algos changes for a same PLL/FS.
#                   pll800_get_params() => pll800c65_get_params()
#                   pll800_get_rate() => pll800c65_get_rate()
#                   clk_pll1600c45_get_params() => pll1600c45_get_params()
#                   clk_pll1600c45_get_phi_params() => pll1600c45_get_phi_params()
#                   pll3200_get_params() => pll3200c32_get_params()
# 07/nov/11  FCh  PLL1600C45 API change + clk_pll1600c45_get_phi_params() added.
# 04/nov/11  FCh  Added PLL1600 CMOS45 support

import sttp
import sttp.logging
import sttp.targetpack
import sttp.stmc

global sttp_root
sttp_root = sttp.targetpack.get_tree()

## PLL800: Compute parameters to get 'output' from 'input' (Hz)
# Returns: ErrCode, mdiv, ndiv, pdiv
#   Note: ErrCode=0 if OK, -1 if ERROR
#   Note: mdiv/ndiv/pdiv are REGISTERS VALUES
# Rules taken into account
#   6.25Mhz <= output <= 800Mhz
#   FS mode means 3 <= N <= 255
#   1 <= M <= 255
#   1Mhz <= PFDIN (input/M) <= 50Mhz
#   200Mhz <= FVCO (input*2*N/M) <= 800Mhz
#   For better long term jitter select M minimum && P maximum

def pll800c65_get_params(input, output):

    # Output clock range: 6.25Mhz to 800Mhz
    if (output < 6250000 or output > 800000000):
        return -1, 1, 1, 0

    input /= 1000
    output /= 1000

    deviation = output
    for pi in 5, 4, 3, 2, 1, 0:
        for m in range(255):
            m = m + 1
            n = (m * (1<<pi) * output) / (input * 2)

            # Checks
            if (n < 3):
                continue
            if (n > 255):
                break
            pfdin = input / m # 1Mhz <= PFDIN <= 50Mhz
            if (pfdin < 1000 or pfdin > 50000):
                continue
            fvco = (input * 2 * n) / m # 200Mhz <= FVCO <= 800Mhz
            if (fvco > 800000):
                continue
            if (fvco < 200000):
                break

            new_freq = (input * 2 * n) / (m * (1<<pi))
            new_deviation = new_freq - output
            if (new_deviation < 0):
                new_deviation = -new_deviation
            if ((new_deviation == 0) or (new_deviation < deviation)):
                best_m    = m
                best_n    = n
                best_p    = pi
                deviation = new_deviation
                if deviation == 0:
                    break
        if deviation == 0:
            break

    if deviation == output:     # No solution found
        return -1, 1, 1, 0
    return 0, best_m, best_n, best_p

## PLL800: Compute output frequency from parameters
# Note: mdiv/ndiv & pdiv have to be REGISTERS VALUES
def pll800c65_get_rate(input, mdiv, ndiv, pdiv):

    if (mdiv == 0):
        mdiv = 1  # mdiv=0 or 1 => MDIV=1

    # Note: input is divided by 1000 to avoid overflow
    output = (((2 * (input/1000) * ndiv) / mdiv) / (1 << pdiv)) * 1000

    return output


# PLL1600 CMOS45
# Description: FVCO freq to parameters computation function
# Input:       input,output=input/output freqs (Hz)
# Returns:     err, idf, ndiv and cp REGISTERS values
#              where err==0 if OK, -1 if ERROR

# Spec used: CMOS045_PLL_PG_1600X_A_SSCG_FR_LSHOD25_7M4X0Y2Z_SPECS_1.1.pdf
#
# Rules:
#   4Mhz <= input (INFF) <= 350Mhz
#   800Mhz <= VCO freq (FVCO) <= 1800Mhz
#   6.35Mhz <= output (PHI) <= 900Mhz
#   1 <= IDF (Input Div Factor) <= 7
#   8 <= NDIV (Loop Div Factor) <= 225
#   1 <= ODF (Output Div Factor) <= 63
#
# PHI = (INFF*LDF) / (2*IDF*ODF)
# FVCO = (INFF*LDF) / (IDF)
# LDF = 2*NDIV (if FRAC_CONTROL=L)
# => FVCO = INFF * 2 * NDIV / IDF

def pll1600c45_get_params(input, output):

    # Output clock range: 800Mhz to 1800Mhz
    if (output < 800000000) or (output > 1800000000):
        return -1, 12, 12, 12

    input /= 1000
    output /= 1000

    deviation = output
    for i in range(1, 8):   # 1 to 7 included
        n = (i * output) / (2 * input)

        # Checks
        if (n < 8):
            continue
        if (n > 225):
            break

        new_freq = (input * 2 * n) / i
        new_deviation = new_freq - output
        if new_deviation < 0:
            new_deviation = -new_deviation
        if (new_deviation == 0) or (new_deviation < deviation):
            idf = i
            ndiv = n
            deviation = new_deviation
        if deviation == 0:
            break

    if deviation == output:   # No solution found
        return -1, 12, 12, 12

    # Computing recommended charge pump value
    # Charge pump table: highest ndiv value for cp=7 to 27
    cp_table = [ 71, 79, 87, 95, 103, 111, 119, 127, 135, 143,
                151, 159, 167, 175, 183, 191, 199, 207, 215,
                223, 225 ]
    cp = 7
    while ndiv > cp_table[cp - 7]:
        cp = cp + 1

    return 0, idf, ndiv, cp

# Description: PLL1600 C45 - compute params for PHI output
# Input:       input,output=PLL input/PHI output freqs (Hz)
# Returns:     err, idf, ndiv, odf and cp REGISTERS values
#              where err==0 if OK, -1 if ERROR

def pll1600c45_get_phi_params(input, phi_out):

    # PHI output clock range: 6.35Mhz to 900Mhz
    if (phi_out < 6350000) or (phi_out > 900000000):
        return -1, 12, 12, 12, 12

    # Computing Output Division Factor
    if (phi_out < 400000000):
        odf = 400000000 / phi_out
        if (400000000 % phi_out):
            odf = odf + 1
    else:
        odf = 1

    # Computing FVCO freq
    fvco_out = 2 * phi_out * odf

    err, idf, ndiv, cp = pll1600c45_get_params(input, fvco_out)
    return err, idf, ndiv, odf, cp


## PLL1600 CMOS65: Compute parameters to get 'output' from 'input' (Hz)
# Returns: ErrCode, mdiv, ndiv for HS output
#   Note: ErrCode=0 if OK, -1 if ERROR
#   Note: mdiv & ndiv are REGISTERS VALUES
# Rules:
#   600Mhz <= output (FVCO) <= 1800Mhz
#   1 <= M (also called R) <= 7
#   4 <= N <= 255
#   4Mhz <= PFDIN (input/M) <= 75Mhz

def pll1600c65_get_params(input,output):

    # Output clock range: 600Mhz to 1800Mhz
    if (output<600000000 or output>1800000000):
        return -1, 1, 1

    input /= 1000
    output /= 1000

    deviation = output
    for m in range(1, 8):   # 1 to 7 included
        n = (m * output) / (input * 2)

        # Checks
        if (n < 4):
            continue
        if (n > 255):
            break
        pfdin = input / m # 4Mhz <= PFDIN <= 75Mhz
        if (pfdin < 4000 or pfdin > 75000):
            continue

        new_freq = (input * 2 * n) / m
        new_deviation = new_freq - output
        if (new_deviation < 0):
            new_deviation = -new_deviation
        if ((new_deviation == 0) or (new_deviation < deviation)):
            best_m    = m
            best_n    = n
            deviation    = new_deviation;
        if deviation == 0:
            break

    if deviation == output: # No solution found
        return -1, 1, 1
    return 0, best_m, best_n


## PLL3200: Compute parameters to get 'output' from 'input' (Hz)
# Returns: ErrCode, idf, ndiv, and cp (charge pump) value
#   Note: ErrCode=0 if OK, -1 if ERROR
#   Note: idf, ndiv, & cp are REGISTERS VALUES (not formula ones)
# Rules:
#   4Mhz <= input <= 350Mhz
#   800Mhz <= output (FVCOby2) <= 1600Mhz
#   1 <= i (register value for IDF) <= 7
#   8 <= n (register value for NDIV) <= 200

def pll3200c32_get_params(input, output):

    # Output clock range: 800Mhz to 1600Mhz
    if (output < 800000000 or output > 1600000000):
        return -1, 12, 12, 12

    input /= 1000
    output /= 1000

    deviation = output
    for i in range(1, 8):   # 1 to 7 included
        n = i * output / (2 * input)

        # Checks
        if (n < 8):
            continue
        if (n > 200):
            break

        new_freq = (input * 2 * n) / i
        new_deviation = new_freq - output
        if new_deviation < 0:
            new_deviation = -new_deviation
        if (new_deviation == 0) or (new_deviation < deviation):
            idf = i
            ndiv = n
            deviation = new_deviation
        if deviation == 0:
            break

    if deviation == output:   # No solution found
        return -1, 12, 12, 12

    # Computing recommended charge pump value
    cp_table = [ 48, 56, 64, 72, 80, 88, 96, 104, 112, 120,
                128, 136, 144, 152, 160, 168, 176, 184, 192 ]
    cp = 6
    while ndiv > cp_table[cp - 6]:
        cp = cp + 1

    return 0, idf, ndiv, cp

## FS660C32 algos

# VCO parameters compute function
# Returns: err, ndiv
#   err: 0=OK, -1=ERROR

def fs660c32_vco_get_params(input, output):
    # Formula
    # VCO frequency = (fin x ndiv) / pdiv
    # ndiv = VCOfreq * pdiv / fin
    pdiv = 1

    # Output clock range: 384Mhz to 660Mhz
    if ((output < 384000000) or (output > 660000000)):
        return -1, 0

    if (input > 40000000):
        # This means that PDIV would be 2 instead of 1.
        #  Not supported today. */
        return -1, 0

    n = output * pdiv / input
    if (n < 16):
        n = 16
    ndiv = n - 16 # Converting formula value to reg value

    return 0, ndiv

# Digital output, parameters compute function
# Inputs: vco_input (Hz), output (Hz), nsdiv
#   vco_input = VCO frequency
#   output    = requested channel output freq
#   nsdiv     = 0xff if programmable, else 0 or 1 if frozen
# Returns: err, mdiv, pe, sdiv, nsdiv
#   err = -1 if ERR, 0 else

def fs660c32_dig_get_params(vco_input, output, nsdiv):
    # ns = nsdiv value (1 or 3)
    # si = sdiv_reg (8 downto 0)
    # s = sdiv value = 1 << sdiv_reg
    # m = md value (0 to 31)
    # initial condition to say: "infinite deviation"
    deviation = 0xffffffff
    # p, p2 = pe value
    # ns_search_loop = How many ns search trials

    # nsdiv is a register value ('BIN') which is translated
    # to a decimal value according to following rules.
    # In case nsdiv is hardwired, it must be set to 0xff before calling.
    #
    #    nsdiv      ns.dec
    #      ff        computed by this algo
    #       0        3
    #       1        1
    if (nsdiv != 0xff):
        if (nsdiv == 1):
            ns = 1
        else:
            ns = 3
        ns_search_loop = 1
    else:
        ns = 3
        ns_search_loop = 2

    P20 = (1 << 20)
    while 1:
        for si in 8, 7, 6, 5, 4, 3, 2, 1, 0:
            s = (1 << si)
            for m in range(32): # m = 0 to 31
                p = vco_input - (s * ns * output) - (s * ns * output * m / 32)
                p = p * P20
                p = p / (s * ns * output)

                if ((p < 0) or (p > 32767)):
                    continue

                mdiv_tmp = m
                pe_tmp = int(p)
                sdiv_tmp = si
                if (ns == 1):
                    nsdiv_tmp = 1
                else:
                    nsdiv_tmp = 0
                err, new_freq = fs660c32_dig_get_rate(vco_input, mdiv_tmp, pe_tmp, sdiv_tmp, nsdiv_tmp)
                if (err != 0):
                    continue
                if (new_freq < output):
                    new_deviation = output - new_freq
                else:
                    new_deviation = new_freq - output
                if (new_deviation < deviation):
                    mdiv = m
                    pe = int(p)
                    sdiv = si
                    if (ns == 1):
                        nsdiv = 1
                    else:
                        nsdiv = 0
                    deviation = new_deviation
                if (deviation == 0):
                    break
            if (deviation == 0):
                break
        if (deviation == 0):
            break
        ns_search_loop = ns_search_loop - 1
        if (ns_search_loop == 0):
            break
        ns = ns - 2

    if (deviation == 0xffffffff): # No solution found
        return -1, 0, 0, 0, 0

    # pe fine tuning if deviation not 0: +/- 2 around computed pe value
    if (deviation != 0):
        mdiv_tmp = mdiv
        if (pe >= 2):
            pe_tmp = pe - 2
        else:
            pe_tmp = 0
        sdiv_tmp = sdiv
        nsdiv_tmp = nsdiv
        for p2 in range(4): # 0 to 3
            if (pe_tmp + p2 >= 32768):
                break
            err, new_freq = fs660c32_dig_get_rate(vco_input, mdiv_tmp, pe_tmp + p2, sdiv_tmp, nsdiv_tmp)
            if (err != 0):
                continue
            if (new_freq < output):
                new_deviation = output - new_freq
            else:
                new_deviation = new_freq - output
            # Check if this is a better solution
            if (new_deviation < deviation):
                pe = int(pe_tmp + p2)
                deviation = new_deviation
            if (deviation == 0):
                break

    return 0, mdiv, pe, sdiv, nsdiv

# Digital output rate compute function
# Inputs: VCO freq, mdiv, pe, sdiv, nsdiv
# Returns: err, rate
#   err = -1 if err, 0 else
#   rate = channel output freq
def fs660c32_dig_get_rate(vco_input, mdiv, pe, sdiv, nsdiv):
    # s = sdiv value = 1 << sdiv_reg_value = 1 << sdiv
    # ns = nsdiv value (1 or 3)

    s = (1 << sdiv)

    # 'nsdiv' is a register value ('BIN') which is translated
    # to a decimal value according to following rules.
    #
    #     nsdiv      ns.dec
    #       0        3
    #       1        1
    if (nsdiv == 1):
        ns = 1
    else:
        ns = 3

    P20 = (1 << 20)
    res = (P20 * (32 + mdiv) + 32 * pe) * s * ns
    rate = (vco_input * P20 * 32) / res
    
    return 0, rate
