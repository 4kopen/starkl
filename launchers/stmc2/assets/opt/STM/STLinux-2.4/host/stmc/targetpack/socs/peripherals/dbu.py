import sys
import time

import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import tap
import tap.jtag

import g4tap

# DBU reg defs
    
DBU_REG_RST      = 0x0c

DBU_REG_STATUS0  = 0x90

DBU_CHAIN0_INC   = 0xc0
DBU_CHAIN0_STA   = 0xc4
DBU_CHAIN1_INC   = 0xc8
DBU_CHAIN1_STA   = 0xcc

DBU_CPU_NTRST    = 0xe0
DBU_CPU_DEBUG_EN = 0xe4
DBU_CPU_PWR_STA  = 0xe8

dbu_var = { "sync_time"   : 10,
            "bus_access"  : 0,
            "ctrl_access" : 0,
            "shift_tdo"   : 0,
            "ir_len"      : 3,
            "dr_len"      : 40,
            "ir_bypass"     : 7 ,
            "ir_ctrl"       : 4 , 
            "ir_reg"        : 5 ,
            "ir_idcode"     : 3 ,
            "dr_bypass_len" : 1 ,
            "dr_ctrl_len"   : 2 ,
            "dr_reg_len"    : 40,
            "dr_id_len"     : 32,
            "dbu_nb"        : 1,
            "verbose"       : 0
         }

# foreach dbu instances (up to 5 arbitrary) this lists the activity (bypass or functionnal)
#dbu_bypass = {} #{0: "func", 1: "func", 2: "func", 3: "func", 4: "func", 5: "func"  } 
#dbu_name   = {} #{0: "(dbu_sas)", 1: "(dbu_mpe)", 2: "ndef", 3: "ndef", 4: "ndef", 5: "ndef"  } 
dbu_info   = {} 

# Array of True/False values - one item per TMC, indexed on TMC/DBU index value
tmc_bypass = []

tmc_bypass_inst = 0x1f
tmc_ir_len = 5

invalid_modepins = []

## dbu_init - first api to call ##
def init(die_info, targetstring_parameters):
    global dbu_name
    global dbu_var
    global dbu_info
    global parameters

    parameters = targetstring_parameters
    
    dbu_var["shift_tdo"] = 1

    ## assume we have a 1:1 mapping between dies and TMCs - i.e each die has a single TMC
    ## but a die may mave multiple DBUs (e.g. Cannes)

    def insert_dbu(index, name, chain_ctrl, debug_ctrl, trst_ctrl, modepins, tmc_name):
        if dbu_info.has_key(name):
            raise sttp.STTPException("Multiple instances for dbu %s" % (name,))

        for k,v in dbu_info.iteritems():
            if v["index"] == index:
                raise sttp.STTPException("Index %d for dbu %s already used by dbu %s" % (index, name, k))

        dbu_info[name] = {}
        dbu_info[name]["active"]     = True
        dbu_info[name]["chain_ctrl"] = chain_ctrl
        dbu_info[name]["debug_ctrl"] = debug_ctrl
        dbu_info[name]["index"]      = index
        dbu_info[name]["name"]       = name
        dbu_info[name]["tmc"]        = tmc_name
        dbu_info[name]["trst_ctrl"]  = trst_ctrl
        dbu_info[name]["modepins"]   = modepins


    for die in die_info:
        if die.has_key("index") and isinstance(die["index"], int):
            # a single DBU behind the TMC/die

            # old targetPacks do not have a tmc entry - fake it based on index
            if not die.has_key("tmc"):
                
                tmcs, num_tmc, master = tap.jtag.get_tmcs_by_index()
                die["tmc"] = tmcs[die["index"]]["name"]

            if die.has_key("debug_ctrl"):
                debug_ctrl = die["debug_ctrl"]
            else:
                debug_ctrl = die["chain_ctrl"]

            if die.has_key("trst_ctrl"):
                trst_ctrl = die["trst_ctrl"]
            else:
                trst_ctrl = die["chain_ctrl"]

            if die.has_key("modepins"):
                modepins = die["modepins"]
            else:
                modepins = {}

            insert_dbu(die["index"], die["name"], die["chain_ctrl"], debug_ctrl, trst_ctrl, modepins, die["tmc"])

        elif die.has_key("dbu_info") and isinstance(die["dbu_info"], list):
            # Multiple DBUs "under" a single TMC
            for d in die["dbu_info"]:

                if d.has_key("debug_ctrl"):
                    debug_ctrl = d["debug_ctrl"]
                else:
                    debug_ctrl = d["chain_ctrl"]

                if d.has_key("trst_ctrl"):
                    trst_ctrl = d["trst_ctrl"]
                else:
                    trst_ctrl = d["chain_ctrl"]

                if d.has_key("modepins"):
                    modepins = d["modepins"]
                else:
                    modepins = {}

                insert_dbu(d["index"], d["name"], d["chain_ctrl"], debug_ctrl, trst_ctrl, modepins, die["tmc"])
        else:
            raise sttp.STTPException("Invalid die description")

    dbu_var["dbu_nb"] = len(dbu_info.keys())
    dbu_var["verbose"] = dbu_var["verbose_modepins"] = dbu_var["debuglow"] = False

    if parameters.has_key("targetpack_debug"):
        tp_debug_list = parameters["targetpack_debug"].split(":")
        if "dbu" in tp_debug_list:
            dbu_var["verbose"] = True
        if "modepins" in tp_debug_list:
            dbu_var["verbose_modepins"] = True
        if "dbulow" in tp_debug_list:
            dbu_var["debuglow"] = True
        if "all" in tp_debug_list:
            dbu_var["verbose"] = dbu_var["verbose_modepins"] = dbu_var["debuglow"] = True

def get_tmc_info(dbu_name):
    for k, v in dbu_info.iteritems():
        if (k == dbu_name):
            return tap.jtag.get_tmc_by_name(v["tmc"])

    return None

## tmc2dbu  ##
#  This can only by done once 
#  To get the TMC(s) back in the chain, the a combination of nsysrst and ntrst must must be applied

def tmc2dbu(active_dbus = None):
    global parameters
    # hack

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> tmc2dbu()")

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< tmc2dbu()")
        return
    
    # backwards compatibility hack
    if None == active_dbus and parameters.has_key("active_dbus"):
        active_dbus = parameters["active_dbus"].split(":")

#    tmcs, num_tmc, master = tap.jtag.get_tmcs_by_index()
#
#    # First read out the TMC devids - not strictly necessary
#    ir = 0
#    l = 0
#    for i in range(0, num_tmc):
#        ir_len = int(tmcs[i]['ir_length'],0)
#        ir <<= ir_len
#        ir  |= 2
#        l   += ir_len
#
#    scan_ir(ir, l) # device id access IR 00010 x n
#    tdo = scan_dr(0, 32*num_tmc, 0)
#
#    for i in range(0, num_tmc):
#        v = hex( 0xFFFFFFFF & (int(tdo,2)>>(i*32)) )
#        sttp.logging.print_out("  TMC %d id = %s" % (i, v))

    # Last period of time where we can shift a key if required
    cb = sttp.targetpack.get_callback("pre_connect")
    if cb: cb()

    tmc2dbu_select(active_dbus)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< tmc2dbu()")


def tmc2dbu_secondary(die_indexes, activate_dbus=None):

    the_dbus = []

#    print "tmc2dbu_secondary(die_indexes %s, activate_dbus %s)" % (die_indexes, activate_dbus)

    if None == activate_dbus:
        for l, v in dbu_info.iteritems():
            tmc = get_tmc_info(l)

#            print "** tmc2dbu_secondary, dbu %s, dbu info %s" % (l, v["tmc"])
#            print "** tmc2dbu_secondary, tmc info %s" % (tmc,)
#            print "tap.jtag._the_tmcs", tap.jtag._the_tmcs

            if int(tmc["chainpos"]) in die_indexes:
                the_dbus.append(l)
    else:
        the_dbus = activate_dbus

    tmc2dbu_select(the_dbus)

    idcode()

    # If any DBUs were flagged inactive because their die was held in reset now flag active
    allCores = sttp.stmc.get_all_core_info()
    for c, info in allCores.iteritems():
        if (c in the_dbus) and info.has_key("parameters") and info["parameters"].has_key("chain_position") and \
           info["parameters"].has_key("inactive_dbu"):
            del info["parameters"]["inactive_dbu"]
            # Hack - set the ir_length to be that for the DBU (stored in irLength)
            info["parameters"]["ir_length"] = info["parameters"]["irLength"]


def tmc2dbu_select(active_dbus = None):

    global tmc_bypass

    # print ">>>> tmc2dbu_select() active_dbus = %s" % (active_dbus,)

    # Now decide which DBUs should be selected
    # For those not selected the TMC will be left in bypass for all following operations

    ir = 0
    l = 0

    tmc_to_dbu_ids = []

    tmcs, num_tmc, master = tap.jtag.get_tmcs_by_index()

    # Init the tmc_bypass array
    for i in range(num_tmc):
        if len(tmc_bypass) <= i:
            tmc_bypass.append(True)

    for i in range(len(dbu_info)):
                
        d = get_dbu_by_index(i)
        n = d["name"]

        if ((None == active_dbus) or (n in active_dbus)) and dbu_tmc_bypassed(i):

            t = get_tmc_info(d["name"])

            # print "TMC2dbu_select: adding DBU name %s (TMC %s)" % (d["name"], t["name"])

            id = int(t["chainpos"])
            tmc_bypass[id] = False
            tmc_to_dbu_ids.append(id)

#        else:
#            tmc_bypass[i] = False

#        tmc_to_dbu_ids.append(i)

#        if None != active_dbus:
#            tmc_bypass[i] = True
#            for k, v in dbu_info.iteritems():
#                if (k in active_dbus) and (v["tmc"] == tmcs[i]["name"]):
#                    tmc_bypass[i] = False
#                    tmc_to_dbu_ids.append(i)
#                    break


    for i in range(num_tmc):
        ir_len = int(tmcs[i]['ir_length'],0)
        l   += ir_len
        if tmc_bypass[i]:
            # TMC bypass
            ir |= ((1<<ir_len)-1 << (i*ir_len))
        else:
            # TMC Instruction to select DBU
            ir |= (0x10 << (i*ir_len))

    # print "TMC2dbu: IR 0x%08x IR_Length %d" % (ir, l)

    scan_ir(ir, l) # Insert chained IR

    tap.jtag.tmc_ir_length_change(tmc_to_dbu_ids, dbu_var["ir_len"])


def dbu_tmc_bypassed(dbu_id):
    d = get_dbu_by_index(dbu_id)
    t = get_tmc_info(d["name"])

#    if t["name"] == d["tmc"] and tmc_bypass[int(t["chainpos"])] and t["name"] not in tmcs_bypassed:
#    print "*** dbu_tmc_bypassed(dbu_id %d) : t %s, tmc_bypass %s" % (dbu_id, t, tmc_bypass) 
    return tmc_bypass[int(t["chainpos"])]


def dbu_active(dbu_name):
    return dbu_info[dbu_name]["active"]

def dbu_scan_ir_dr(ir_value, dr_value, dr_reg_len, tmc_callback, dbu_callback):
    ir_len  = 0
    dr_len  = 0
    ir_data = 0
    dr_data = 0

    tmcs_bypassed = []
    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        n = d["name"]
        t = get_tmc_info(d["name"])
        # Only insert a TMC bypass if we have not allready done do for this TMC

#        print "** get_tmc_info: ", t
#        print "** d name & tmc", d["name"], d["tmc"]
#        print "** tmc_bypass: ", tmc_bypass
#        print "** tmcs_bypassed: ", tmcs_bypassed

        if t["name"] == d["tmc"] and tmc_bypass[int(t["chainpos"])] and t["name"] not in tmcs_bypassed:
            tmcs_bypassed.append(t["name"])
            ir_data += (tmc_bypass_inst << ir_len)
            ir_len += tmc_ir_len
            dr_len  += dbu_var["dr_bypass_len"]

#            print "*** dbu_scan_ir_dr(): i %d, t[name] %s, adding tmc_bypass_inst" % (i, t["name"], )
        elif False == dbu_info[n]["active"]:
            ir_data += (dbu_var["ir_bypass"] << ir_len)
            ir_len += dbu_var["ir_len"]   
            dr_len  += dbu_var["dr_bypass_len"]

#            print "*** dbu_scan_ir_dr(): i %d, dbu %s, *inactive*" % (i, n, )
        elif False == tmc_bypass[int(t["chainpos"])]:
            ir_data += (ir_value << ir_len)
            dr_data += (dr_value << dr_len)
            ir_len += dbu_var["ir_len"]   
            dr_len += dr_reg_len

#            print "*** dbu_scan_ir_dr(): i %d, dbu %s, active" % (i, n, )

    scan_ir(ir_data, ir_len) # insert IR chain
    tdo = scan_dr(dr_data, dr_len, dbu_var["sync_time"]) # DR chain


    if None == tmc_callback and None == dbu_callback:
        return

    dr_max = dr_len
    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        t = get_tmc_info(d["name"])

        if t["name"] == d["tmc"] and tmc_bypass[int(t["chainpos"])] and t["name"] not in tmcs_bypassed:
            if tmc_callback:
                tmc_callback(t)
            dr_max -= dbu_var["dr_bypass_len"]
        elif False == dbu_info[n]["active"]:
            dr_max -= dbu_var["dr_bypass_len"]
        elif False == tmc_bypass[int(t["chainpos"])]:

            mytdo = tdo[dr_max-dr_reg_len:dr_max]
            if dbu_callback:
                dbu_callback(d, int(mytdo, 2) )

#            sttp.logging.print_out("  DBU %s id = %s" % (dbu_name[i], hex(int(mytdo, 2)) ) )
            dr_max -= dr_reg_len


##  DBU idcode display  ##
def idcode ():
    global dbu_var

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> idcode()")

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< idcode()")
        return


    def tmc_callback(t):
        sttp.logging.print_out("  TMC in bypass: ", t["name"])

    def dbu_callback(d, value):
        sttp.logging.print_out("  DBU %s id = %s" % (d["name"], hex(value) ) )

    dbu_scan_ir_dr(dbu_var["ir_idcode"], 0, dbu_var["dr_id_len"], tmc_callback, dbu_callback)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< idcode()")


## write   ##
def write_dbu_reg(add, data):
    global dbu_bypass 
    global dbu_var 
    global dbu_name 
    ir_len = 0 
    dr_len = 0 
    ir_data = 0
    dr_data = 0 
    ladd = add 
    ldata = data  

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> write_dbu_reg(add 0x%08x, data 0x%08x)" % (add, data))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["debuglow"]:
            sttp.logging.print_out("<<<< write_dbu_reg()")
        return


    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        n = d["name"]

        if dbu_tmc_bypassed(i):
            ir_data += (tmc_bypass_inst << ir_len)
            ir_len += tmc_ir_len
            dr_len  += dbu_var["dr_bypass_len"]
        elif False == dbu_info[n]["active"]:
            ir_data += (dbu_var["ir_bypass"] << ir_len)
            ir_len += dbu_var["ir_len"]
            dr_len  += dbu_var["dr_bypass_len"]
        else:
            ir_data += (dbu_var["ir_reg"] << ir_len)
            dr_data += ( (((ldata&0xFFFFFFFF)<<8) + (ladd&0xFC) + 1) << dr_len)
            dr_len  += dbu_var["dr_reg_len"]
            ir_len += dbu_var["ir_len"]
            ladd  = ladd  >> 8 
            ldata = ldata >> 32 

    scan_ir(ir_data,ir_len)    # shift ir
    scan_dr(dr_data,dr_len,dbu_var["sync_time"]) # shift dr 

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< write_dbu_reg()")

## end write ##

## dbu_write_reg ##
def OLD_dbu_write_reg(dbu_id, add, data):
    global dbu_bypass 
    global dbu_var 
    global dbu_name 

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> dbu_write_reg(dbu_id %d, add 0x%08x, data 0x%08x)" % (dbu_id, add, data))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< dbu_write_reg()")
        return

    ir_len = 0 
    dr_len = 0 
    ir_data = 0
    dr_data = 0 
    ladd = add 
    ldata = data  
    for i in range(0,dbu_var["dbu_nb"]):
       ir_len += dbu_var["ir_len"]
       if i != dbu_id: # Put all dbus not addressed in bypass - dbu_bypass[i] == "bypass" :   
           dr_len  += dbu_var["dr_bypass_len"]
           ir_data += (dbu_var["ir_bypass"] << (i*dbu_var["ir_len"]))
       else:
           if dbu_var["verbose"] == 1 :
             sttp.logging.print_out("dbu.write %s @0x%.2x = 0x%.8x" %(dbu_name[i],(ladd&0xFC),(ldata&0xFFFFFFFF)) )
           ir_data += (dbu_var["ir_reg"] << (i*dbu_var["ir_len"]))
           dr_data += ( (((ldata&0xFFFFFFFF)<<8) + (ladd&0xFC) + 1) << dr_len)
           dr_len  += dbu_var["dr_reg_len"]
           ladd  = ladd  >> 8 
           ldata = ldata >> 32 
    #sttp.logging.print_out("write dbu_nb  =", dbu_var["dbu_nb"]  )
    #sttp.logging.print_out("write ir_len  =", ir_len )
    #sttp.logging.print_out("write dr_len  =", dr_len )
    #sttp.logging.print_out("write ir_data = %x" %ir_data )
    #sttp.logging.print_out("write dr_data = %x" %dr_data )
    scan_ir(ir_data,ir_len)    # shift ir
    scan_dr(dr_data,dr_len,dbu_var["sync_time"]) # shift dr 

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< dbu_write_reg()")

## end write_dbu_reg ##

## read ##
def read_dbu_reg(add): 
    global dbu_name  
    global dbu_bypass 
    global dbu_var

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> read_dbu_reg(add 0x%08x)" % (add,))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["debuglow"]:
            sttp.logging.print_out("<<<< read_dbu_reg()")
        return 0

    ir_len = 0 
    dr_len = 0 
    ir_data = 0
    dr_data = 0
    ladd  = add 
    ldata = 0 
    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        n = d["name"]

        if dbu_tmc_bypassed(i):
            ir_data += (tmc_bypass_inst << ir_len)
            ir_len += tmc_ir_len
            dr_len  += dbu_var["dr_bypass_len"]
        elif False == dbu_info[n]["active"]:
            ir_data += (dbu_var["ir_bypass"] << ir_len)
            ir_len += dbu_var["ir_len"]
            dr_len  += dbu_var["dr_bypass_len"]
        else:
            ir_data += (dbu_var["ir_reg"] << ir_len)
            dr_data += ( ((ladd&0xFC) + 2) << dr_len)
            dr_len  += dbu_var["dr_reg_len"]
            ir_len += dbu_var["ir_len"]
            ladd  = ladd  >> 8 

    scan_ir(ir_data,ir_len)    # shift ir
    scan_dr(dr_data,dr_len,dbu_var["sync_time"]) # shift dr 
    tdo = scan_dr(0,dr_len,dbu_var["sync_time"])

    dr_max = dr_len    
    s = 0
    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        n = d["name"]

        if dbu_tmc_bypassed(i):
            dr_max -= dbu_var["dr_bypass_len"]   
        elif False == dbu_info[n]["active"]:
            dr_max -= dbu_var["dr_bypass_len"]   
        else:
            mytdo = tdo[dr_max-dbu_var["dr_reg_len"]:dr_max] 
            if dbu_var["debuglow"]:
                sttp.logging.print_out("dbu.read %s  @0x%.2x = 0x%.8x " % (n, int(mytdo, 2) & 0xFC, int(mytdo, 2)>>8) )
            if (int(mytdo,2)&3) != 0: 
                sttp.logging.print_out( "dbu.read synchronisation error : can't read at %.02x" % (add) )
            dr_max -= dbu_var["dr_reg_len"] 
            #ldata = (ldata << 32) + ((int(mytdo,2)>>8)&0x0ffffffff) 
            ldata = ldata + (((int(mytdo, 2) >> 8) & 0xffffffff) << (32*s))
            s += 1

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< read_dbu_reg()")

    return ldata  
## end read ##

## dbu_read_reg ##
def OLD_dbu_read_reg(dbu_id, add): 
    global dbu_name  
    global dbu_bypass 
    global dbu_var

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> dbu_read_reg(%d, 0x%08x)" % (dbu_id, add) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< dbu_read_reg()")
        return

    ir_len = 0 
    dr_len = 0 
    ir_data = 0
    dr_data = 0
    ladd  = add 
    ldata = 0 
    for i in range(0,dbu_var["dbu_nb"]):
        ir_len += dbu_var["ir_len"]
        if i != dbu_id: # dbu_bypass[i] == "bypass" :   
            dr_len  += dbu_var["dr_bypass_len"]
            ir_data += (dbu_var["ir_bypass"] << (i*dbu_var["ir_len"]))
        elif tmc_bypass[i]:
            dr_len  += dbu_var["dr_bypass_len"]
            ir_data += (0x1F << (i*5))
        else: 
            ir_data += (dbu_var["ir_reg"] << (i*dbu_var["ir_len"]))
            dr_data += ( ((ladd&0xFC) + 2) << dr_len)
            dr_len  += dbu_var["dr_reg_len"]
            ladd = add >> 8  
    #sttp.logging.print_out(" dbu_nb  =", dbu_var["dbu_nb"]  )
    #sttp.logging.print_out(" ir_len  =", ir_len )
    #sttp.logging.print_out(" dr_len  =", dr_len )
    #sttp.logging.print_out(" ir_data = %x" %ir_data )
    #sttp.logging.print_out(" dr_data = %x" %dr_data )
    scan_ir(ir_data,ir_len)    # shift ir
    scan_dr(dr_data,dr_len,dbu_var["sync_time"]) # shift dr 
    tdo = scan_dr(0,dr_len,dbu_var["sync_time"])
    #sttp.logging.print_out("  dbu.read  tdo = ", hex(int(tdo,2)) )
    #sttp.logging.print_out("  dbu.read  tdo.length = ", len(tdo) )
    dr_max = dr_len    
    for i in range(0,dbu_var["dbu_nb"]) :
        if dbu_bypass[i] == "bypass" :
            if dbu_var["verbose"] == 1 :
                sttp.logging.print_out("  dbu.bypass  ",dbu_name[i] )
            dr_max -= dbu_var["dr_bypass_len"]   
        elif tmc_bypass[i]:
            dr_len  += dbu_var["dr_bypass_len"]
            ir_data += (0x1F << (i*5))
        else:
            mytdo = tdo[dr_max-dbu_var["dr_reg_len"]:dr_max] 
            if dbu_var["debuglow"]:
                sttp.logging.print_out("dbu.read %s  @0x%.2x = 0x%.8x " %(dbu_name[i],int(mytdo,2)&0xFC,int(mytdo,2)>>8) )
            if (int(mytdo,2)&3) != 0: 
                sttp.logging.print_out( "dbu.read synchronisation error : can't read at %.02x" %(add) )
            dr_max -= dbu_var["dr_reg_len"] 
            ldata = (ldata << 32) + ((int(mytdo,2)>>8)&0x0ffffffff) 

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< dbu_read_reg()" )

    return ldata  
## end read_dbu_reg ##

## mask_reset : set the global nreset mask ##
def mask_reset(): 
    global dbu_var
    global dbu_name  
    global dbu_bypass 

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> mask_reset()" )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< mask_reset()")
        return

    dbu_scan_ir_dr(dbu_var["ir_ctrl"], 0x1, dbu_var["dr_ctrl_len"], None, None)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< mask_reset()" )

## end mask_reset ##


def write_xor_input(xor_bank, value_mapping):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> write_xor_input(bank 0x%08x, mapping %s)" % (xor_bank, value_mapping) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< write_xor_input()")
        return

    addr = 0
    data = 0

    #for i in range(0, len(dbu_info)):
    for i in range(len(dbu_info)-1, -1, -1):
       addr <<= 8
       addr += 0xa0 + (xor_bank * 4)
       data <<= 32

       for k, v in dbu_info.iteritems():
           if i == v["index"]:
               if value_mapping.has_key(k):
                   data += value_mapping[k]
               break

    write_dbu_reg(addr, data)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< write_xor_input() : written reg 0x%08x, val 0x%x" % (addr, data))

def set_read_xor_source(xor_source):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_read_xor_source(xor_source %s)" % (xor_source,))

    # to read xor must set control reg (0) appropriately - set bits 3&4 to 0 in both DBUs
    clear_mask = 0
    set_mask   = 0

    # clear_mask_bits = ( (~xor_source) & 3 ) << 3
    clear_mask_bits = ( 3 << 3 )

    for i in range(len(dbu_info)-1, -1, -1):
        clear_mask <<= 32
        set_mask   <<= 32
        clear_mask |= clear_mask_bits
        set_mask   |= ( 1 | (xor_source << 3))

    control = read_dbu_reg(0)

    control &= ~clear_mask
    control |= set_mask

    if dbu_var["verbose"]:
        sttp.logging.print_out("  set_read_xor_source() : current control val 0x%x" % (control))
        sttp.logging.print_out("  set_read_xor_source() : clear_mask 0x%x, set_mask 0x%x" % (clear_mask, set_mask))
        sttp.logging.print_out("  set_read_xor_source() : new control val 0x%x" % (control))

    write_dbu_reg(0x0, control)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_read_xor_source()")

def set_xor_dbu_input(dbu_name, xor_register, offset, width, value):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_xor_input(dbu_name %s, xor_register %d, offset %d, width %d, value %d)" % (dbu_name, xor_register, offset, width, value) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_xor_dbu_input()")
        return

    set_read_xor_source(0)

    addr = 0xa0 + (xor_register * 4)
    addr = addr + (addr << 8)

    current_data = read_dbu_reg(addr)
    if dbu_var["verbose"]:
        sttp.logging.print_out("  set_xor_dbu_input() : current reg 0x%08x, val 0x%x" % (addr, current_data))

    addr = 0
    data = 0

    # index over the DBUs
    for i in range(len(dbu_info)-1, -1, -1):

        addr <<= 8
        addr += 0xa0 + (xor_register * 4)
        data <<= 32

        for k, v in dbu_info.iteritems():
            if i == v["index"]:

                current_xor_register = ( current_data >> (i*32) ) & 0xffffffff

                if dbu_name == k:
                    # this is the dbu we want
                    new_xor_register = current_xor_register & ~((pow(2, width)-1)<<offset)
                    new_xor_register = new_xor_register | (value << offset)
                    data += new_xor_register
                else:
                    # insert the old data for this dbu
                    data += current_xor_register

    write_dbu_reg(addr, data)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_xor_dbu_input() : written reg 0x%08x, val 0x%x" % (addr, data))

def get_xor_dbu_output(dbu_name, xor_register, offset, width):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> get_xor_dbu_output(dbu_name %s, xor_register %d, offset %d, width %d)" % (dbu_name, xor_register, offset, width) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< get_xor_dbu_output()")
        return

    set_read_xor_source(3)

    addr = 0
    data = 0

    # index over the DBUs
    for i in range(len(dbu_info)-1, -1, -1):

        addr <<= 8
        addr += 0xa0 + (xor_register * 4)
        data <<= 32

        current_data = read_dbu_reg(addr)

    for k, v in dbu_info.iteritems():
        if dbu_name == k:
            # this is the dbu we want
            i = v["index"]
            current_xor_register = ( current_data >> (i*32) ) & 0xffffffff
            value = (current_xor_register >> offset) & (pow(2, width)-1)
            if dbu_var["verbose"]:
                sttp.logging.print_out("<<<< get_xor_dbu_output() : 0x%08x" % (value, ))
            return value

    value = 0    
    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< get_xor_dbu_output() : 0x%08x" % (value,))
    return value


def set_xor_dbu_output(dbu, xor_register, offset, width, requiredOutputValue):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_xor_dbu_output(dbu_name %s, xor_register %d, offset %d, width %d, requiredOutputValue %d)" % (dbu, xor_register, offset, width, requiredOutputValue) )

    currentOutputValue = get_xor_dbu_output(dbu, xor_register, offset, width)

    if requiredOutputValue != currentOutputValue:
        # xor the desired output with the current output to get the value to xor with the input

        inputValue = requiredOutputValue ^ currentOutputValue
        set_xor_dbu_input(dbu, xor_register, offset, width, inputValue)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_xor_dbu_output()")

def set_modepin_output(mname, mvalue):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_modepin_output()")

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_modepin_output()")
        return

    dbu = None
    for n, v in dbu_info.iteritems():
        if v.has_key("modepins"):
            if v["modepins"].has_key(mname):
                dbu = v["name"]
            else:
                # check for a match with "alt_name"
                for mp, mp_fields in v["modepins"].iteritems():
                    if mp_fields.has_key("alt_name") and \
                       mp_fields["alt_name"] == mname:
                        dbu = v["name"]
                        mname = mp
                        break
            if dbu != None:
                break    

    if None == dbu:
        raise sttp.STTPException("Invalid modepin %s" % (mname,))
        
    # is it a valid "value"
    if str == type(mvalue):
        try:
            imvalue = int(mvalue, 0)
        except ValueError:
            imvalue = -1
    else:
        imvalue = mvalue

    if dbu_info[dbu]["modepins"][mname].has_key("sym_values") and \
       (mvalue in dbu_info[dbu]["modepins"][mname]["sym_values"]):
        requiredOutputValue = dbu_info[dbu]["modepins"][mname]["sym_values"].index(mvalue)
    elif dbu_info[dbu]["modepins"][mname].has_key("values") and \
       (imvalue in dbu_info[dbu]["modepins"][mname]["values"]):
        requiredOutputValue = dbu_info[dbu]["modepins"][mname]["values"].index(imvalue)
    else:
        raise sttp.STTPException("Invalid value %s for modepin %s" % (mvalue, mname))

    xor_register = dbu_info[dbu]["modepins"][mname]["register"]
    width        = dbu_info[dbu]["modepins"][mname]["width"]
    offset       = dbu_info[dbu]["modepins"][mname]["offset"]

    if -1 == offset:
        raise sttp.STTPException("Mode pin %s cannot be manipulated" % (mname,))

    set_xor_dbu_output(dbu, xor_register, offset, width, requiredOutputValue)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_modepin_output()")

def add_invalid_modepin(name):
    invalid_modepins.append(name)

def set_modepin_overrides(parameters):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_modepin_overrides()")

    # get the list of modepins of set
    modePinList    = []
    dbuModePinList = []
    
    if parameters.has_key("modepins"):
        modePinList = parameters["modepins"].split(":")
    if parameters.has_key("dbu_modepins"): 
        dbuModePinList = parameters["dbu_modepins"].split(":")

    if (sttp.stmc.get_operating_mode() == "ROMGEN") or \
       ([] == modePinList and [] == dbuModePinList):
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_modepin_overrides()")
        return
    
    for m in modePinList + dbuModePinList:
        if -1 == m.find(";"):
            raise sttp.STTPException("Invalid modepin specifier %s" % (m,))
        mname, mvalue = m.split(";")

        if (m in modePinList) and \
           (mname in invalid_modepins):
            sttp.logging.print_out("Warning: DBU override of modepin %s is not active because it it controlled elsewhere (perhaps with a board-level device). Use the TargetString option dbu_modepins=<> to invoke the DBU override\n" % (mname,));
            continue

        set_modepin_output(mname, mvalue)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_modepin_overrides()")

def assert_reset_out(assertit):

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> assert_reset_out(assertit 0x%08x)" % (assertit,) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< assert_reset_out()")
        return

    addr = 0
    data = 0

    #for i in range(0,dbu_var["dbu_nb"]):
    for i in range(len(dbu_info)-1, -1, -1):
       addr <<= 8
       addr += DBU_REG_RST
       data <<= 32    
       if not assertit:
           data += 1
        
    write_dbu_reg(addr, data)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< assert_reset_out()")

def setup_chain_items(chain_id, dbu_config):

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> setup_chain_items(chain_id %d, dbu_config %s)" % (chain_id,dbu_config) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< setup_chain_items()")
        return

    chain_addr = DBU_CHAIN0_INC + (chain_id * 8)
    
    addr = 0
    data = 0
    
    #sttp.logging.print_out( "setup_chain(%s, %s)" % (chain_id, config) )

    values = {}
    for k, v in dbu_config.iteritems():

        chain_ctrl    = dbu_info[k]["chain_ctrl"]
        index         = dbu_info[k]["index"]
        values[index] = 0
       
        for item in v:
            values[index] |= chain_ctrl[item]
        # sttp.logging.print_out( "setup_chain_items: dbu %s, items %s, index %s, val %s\n" % (k, v$
    
    for i in range(len(dbu_info)-1, -1, -1):
       addr <<= 8
       addr += chain_addr
       data <<= 32
       if values.has_key(i):
           data |= values[i]
        
    set_all_active()

    if dbu_var["verbose"]:
        sttp.logging.print_out( "  setup_chain: addr 0x%08x, data 0x%08x" % (addr, data) )

    write_dbu_reg(addr, data)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< setup_chain_items()")


## write_reg ##
def write_reg(add, data):

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> write_reg(add 0x%08x, data 0x%08x)" % (add, data) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["debuglow"]:
            sttp.logging.print_out("<<<< write_reg()")
        return

    sttp.logging.print_out(" obsolete function - just here for backward compatibility     ")
    if dbu_var["bus_access"] == 0:
        scan_ir(0x5,dbu_var["ir_len"]) # internal bus access
    d = (data<<8) + (add&0xFC)
    scan_dr(d+1,dbu_var["dr_len"],dbu_var["sync_time"])    
    ## use code below if you want to check the synchronisation (not necessary if dbu.sync() have been call before) 
    #if int(scan_dr(0,dbu_var["dr_len"],dbu_var["sync_time"]),2)  != d:
    #    sttp.logging.print_out( "  dbu.write_reg - synchronisation error : can't write %.10x at %.02x" %(data,add) )

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< write_reg()")

## end write_reg ##


## read_reg ##
def read_reg(add): 

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> read_reg(add 0x%08x)" % (add,) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["debuglow"]:
            sttp.logging.print_out("<<<< read_reg()")
        return 0

    sttp.logging.print_out(" obsolete function - just here for backward compatibility     ")
    if dbu_var["bus_access"] == 0:
        scan_ir(0x5,dbu_var["ir_len"]) # internal bus access
    scan_dr(add+2,dbu_var["dr_len"],dbu_var["sync_time"])
    r = int(scan_dr(0,dbu_var["dr_len"],dbu_var["sync_time"]),2)
    ## use code below if you want to check the synchronisation (not necessary if dbu.sync() have been call before) 
    if (r&3) != 0:
        sttp.logging.print_out( "  dbu.read_reg - synchronisation error : can't read at %.02x" %(add) )

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< read_reg()")

    return r>>8

## end read_reg ##


## write_bus ##
def write_bus(parameters): #add,data,be):

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> write_bus(parameters %s)" % (parameters,) )

    if parameters.has_key("be"):
        write_dbu_reg(0x68,parameters['be']) # byte enable
    if parameters.has_key("add"):
        write_dbu_reg(0x6C,parameters['add'])  # address
    if parameters.has_key("data"):
        write_dbu_reg(0x70,parameters['data']) # data
    write_dbu_reg(0x60,0x0)  # write (+start)
    while ( read_dbu_reg(0x64) & 0x2 ) != 0:
        pass

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< write_bus")

## end write_bus ##


## read_bus ##
def read_bus(parameters): 
    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> read_bus(parameters %s)" % (parameters,) )

    if parameters.has_key("be"):
        write_dbu_reg(0x68,parameters['be']) # byte enable
    if parameters.has_key("add"):
        write_dbu_reg(0x6C,parameters['add'])  # address

    write_dbu_reg(0x60,0x1)  # read (+start)

    count = 0
    v = read_dbu_reg(0x64)
    while ( v & 0x2 ) != 0:
        count += 1
        v = read_dbu_reg(0x64)
        pass

    r = read_dbu_reg(0x74)

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< read_bus() = 0x%08x" % (r,) )

    return r

## end read_bus ##


## bus_check ##
def bus_check():
    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> bus_check()")
    
    d = read_dbu_reg(0x64)
    s = ('e','o','r')
    if ( d & 0x4 ) != 0 : # error flag
        s[0] = 'E'
    if ( d & 0x2 ) != 0 : # on-going flag
        s[1] = '0'
    if ( d & 0x2 ) != 0 : # ropc flag
        s[2] = 'R'

    if dbu_var["debuglow"]:
        sttp.logging.print_out( "  dbu.bus_check - flags : %s%s%s " %s) 
        sttp.logging.print_out("<<<< bus_check")

    return s 
## end bus_check ## 


def get_active():
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> get_active()" )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< get_active()")
        return
     
    active_list = []

    for k, v in dbu_info.iteritems():
        if v["active"]:
            active_list.append(k)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< get_active() = %s", active_list )

    return active_list

def set_active(items):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_active(%s)" % (items,))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_active()")
        return
     
    for k, v in dbu_info.iteritems():
        dbu_info[k]["active"] = False

    for k, v in dbu_info.iteritems():
        if k in items:
            dbu_info[k]["active"] = True

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_active()" )

def set_all_active():
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_all_active()")

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_all_active()")
        return

    # Set all dbus active
    a = []
    for i in range(len(dbu_info)):    # dbu_var["dbu_nb"]):
        a.append( get_dbu_name(i) )
    set_active(a)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_all_active()" )

def get_dbu_name(i):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> get_dbu_name(%s)" % (i,))    

    r = None
    for k, v in dbu_info.iteritems():
        if (i == v["index"]):
            r = k
            break

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< get_dbu_name() = %s" % (r,) )
    return r

def get_dbu_by_index(i):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> get_dbu_by_index(%s)" % (i,))    

    v = None
    for k, v in dbu_info.iteritems():
        if (i == v["index"]):
            break

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< get_dbu_by_index() = %s" % (v,) )
    return v

def display_modepins():

    if not dbu_var["verbose_modepins"]:
        return

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> display_modepins()")    

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< display_modepins()")
        return

    active_list = get_active()

    for i in range(len(dbu_info)):

        name = get_dbu_name(i)

        if dbu_tmc_bypassed(i):
            debug_print( "%s ID TMC in bypass" % (name,) )        
            continue

        set_active([name,])

        debug_print( "%s ID and mode pin status summary" % (name,) )
        debug_print( "    DBU ID  (@ reg offset 0x08): 0x%.8x\n" % read_dbu_reg(0x08) )
        #### debug_print( "    STATUS0 (@0x90): 0x%.10x\n" % read_dbu_reg(0x90) )
    
        write_dbu_reg(0x0,0xb) # (01)011
        debug_print( "    %s 0 modepins in   : 0x%.8x" % (name, read_dbu_reg(0xa0)) )
        debug_print( "    %s 1 modepins in   : 0x%.8x" % (name, read_dbu_reg(0xa4)) )
        debug_print( "    %s 2 modepins in   : 0x%.8x" % (name, read_dbu_reg(0xa8)) )

        write_dbu_reg(0x0,0x3) # (00)011
        debug_print( "    %s 0 modepins prog : 0x%.8x" % (name, read_dbu_reg(0xa0)) )
        debug_print( "    %s 1 modepins prog : 0x%.8x" % (name, read_dbu_reg(0xa4)) )
        debug_print( "    %s 2 modepins prog : 0x%.8x" % (name, read_dbu_reg(0xa8)) )

        write_dbu_reg(0x0,0x13) # (10)011
        debug_print( "    %s 0 modepins out  : 0x%.8x" % (name, read_dbu_reg(0xa0)) )
        debug_print( "    %s 1 modepins out  : 0x%.8x" % (name, read_dbu_reg(0xa4)) )
        debug_print( "    %s 2 modepins out  : 0x%.8x" % (name, read_dbu_reg(0xa8)) )
        debug_print( "" )

    set_active(active_list)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< display_modepins()")

def preserve_chain():
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> preserve_chain()")

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< preserve_chain()")
        return

    # Set DBU_REG_CTRL0[5] to 1, so on a subsequent reconnect we do not need to reprogram the chain
    write_dbu_reg(0x0,0x33) # 1(10)011

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< preserve_chain()")

def setup_chain(core_list):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> setup_chain(%s)" % (core_list,) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< setup_chain()")
        return

    dbu_unavilable_list = []

    for i in range(len(dbu_info)):

        d = get_dbu_by_index(i)
        n = d["name"]

        if dbu_tmc_bypassed(i):
            dbu_unavilable_list.append(n)

#        print >> sys.stderr, "*** setup_chain(): i %d, d %s, n %s, dbu_tmc_bypassed(i) %s" % (i, d, n, dbu_tmc_bypassed(i)) 


    for d in dbu_unavilable_list:
        if d not in core_list:
            core_list.append(d)

    if dbu_var["verbose"]:
        sttp.logging.print_out("    calling g4tap.setup_chain(%s, %s)" % (core_list, dbu_unavilable_list) )

    g4tap.setup_chain(core_list, dbu_unavilable_list)    

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< setup_chain()")

def setup_dbu_pokepeek(dbu_list):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> setup_dbu_pokepeek(%s)" % (dbu_list,) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< setup_dbu_pokepeek()")
        return

    # HACK to test coreAgent pokepeek
    p = sttp.targetinfo.get_targetstring_parameters()
    if p.has_key("host_jtag_pokepeek") and 1==int(p["host_jtag_pokepeek"]):
        pass

    elif (not p.has_key("host_jtag_pokepeek")) and \
         ("STMC1" == sttp.stmc.get_stmc_type()):
        # cludge to use host jtag code on STMCLITE
        pass
    else:
        # Take into account any TMCs which have not been switched to DBU mode 
        # and must be left in bypass

        setup_chain(dbu_list)

        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< setup_dbu_pokepeek()")
        return

    # Use the DBU for all memory operations

    def dbu_PokePeekOpen(batchThePokes, pokepeek_timeout, coreName, coreAgent, coreAgentEnv, crc, doAttach, targetConnect):
        # hack to enable
        v = read_dbu_reg(0)
        write_dbu_reg(0, v | (1<<1) | (1<<10) )

        # byte en
        write_dbu_reg(0x68, 0xf)

        # Write init reg value 0
        write_dbu_reg(0x78, 0)

    def dbu_PokePeekClose(doDetach):
        pass
    def dbu_Attach():
        pass
    def dbu_Detach():
        pass
    def dbu_Peek(address, size, comment):
        v =  read_bus({'add':address}) 
        return v
    def dbu_Poke(address, value, size, comment):
        write_bus({'add':address ,'data':value})        
        return 0
    def dbu_PeekBytes(address, size, comment):
        pass
    def dbu_PokeBytes(address, bytes, size, comment):
        pass
    def dbu_WhileAndNe(address, andMask, expectedValue, size, maxRepetitions, delayMicroSecs, comment):
        count = 0
        while None==maxRepetitions or count<maxRepetitions:
            v = dbu_Peek(address, size, None) & andMask
            if expectedValue == v:
                break           
            if delayMicroSecs: 
                time.sleep(float(delayMicroSecs)/1000000.0)
            count += 1
    def dbu_WhileAndEq(address, andMask, expectedValue, size, maxRepetitions, delayMicroSecs, comment):
        raise sttp.STTPException("sttp.pokepeek.while_and_eq() not supported")
    def dbu_DelayCalibrate(rom_loops_per_usec):
        pass
    def dbu_ReadModifyWrite(addressDest, addressSrc, andMask, shift, orMask, size, comment):
        v  = dbu_Peek(addressSrc, size, None)
        v2 = (v & andMask) << shift
        v3 = v2 | orMask
        dbu_Poke(addressDest, v3, size, None)
    def dbu_IfGt(address, andMask, value, size, comment):
        a = andMask & dbu_Peek(address, size, None)
        c = andMask & value
        return a > value
    def dbu_IfEq(address, andMask, value, size, comment):
        a = andMask & dbu_Peek(address, size, None)
        c = andMask & value
        return a == value

    pokepeek_server = sttp.pokepeek.server
    pokepeek_server.__dict__["PokePeekOpen"]    = dbu_PokePeekOpen
    pokepeek_server.__dict__["PokePeekClose"]   = dbu_PokePeekClose
    pokepeek_server.__dict__["Attach"]          = dbu_Attach
    pokepeek_server.__dict__["Detach"]          = dbu_Detach
    pokepeek_server.__dict__["Peek"]            = dbu_Peek
    pokepeek_server.__dict__["Poke"]            = dbu_Poke
    pokepeek_server.__dict__["PeekBytes"]       = dbu_PeekBytes
    pokepeek_server.__dict__["PokeBytes"]       = dbu_PokeBytes
    pokepeek_server.__dict__["WhileAndNe"]      = dbu_WhileAndNe
    pokepeek_server.__dict__["WhileAndEq"]      = dbu_WhileAndEq
    pokepeek_server.__dict__["DelayCalibrate"]  = dbu_DelayCalibrate
    pokepeek_server.__dict__["ReadModifyWrite"] = dbu_ReadModifyWrite
    pokepeek_server.__dict__["IfGt"]            = dbu_IfGt
    pokepeek_server.__dict__["IfEq"]            = dbu_IfEq

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< setup_dbu_pokepeek()")

SET   = 0
CLEAR = 1

def write_dbu_using_corelist(op, dbu_reg, core_list, core_bit_mapping = "chain_ctrl"):

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> write_dbu_using_corelist(op %s, dbu_reg 0x%08x, core_list %s)" % (op, dbu_reg, core_list) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< write_dbu_using_corelist()")
        return

    addr = 0
    data = 0  

    values = {}

    for k, v in dbu_info.iteritems():
        index = v["index"]        
        values[index] = 0
        chain_ctrl = v[core_bit_mapping]
        for c in core_list:
            if chain_ctrl.has_key(c):
                values[index] |= chain_ctrl[c]


    for i in range(len(dbu_info)-1, -1, -1):
       addr <<= 8
       addr += dbu_reg
       data <<= 32
       if values.has_key(i):
           data |= values[i]

       if dbu_var["verbose"]:
           sttp.logging.print_out("   write_dbu_using_corelist: dbu %d, value 0x%08x\n" % (i, values[i]))

    set_all_active()

    current = read_dbu_reg(addr)

    if CLEAR == op:
        new = current & ~data
    elif SET == op:
        new = current | data

    write_dbu_reg(addr, new)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< write_dbu_using_corelist()")
        sttp.logging.print_out("<<<< write_dbu_reg(addr=0x%x , data=0x%x)" %(addr, new))

def set_trst(do_assert, core_list):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_trst(do_assert %s, core_list %s)" % (do_assert, core_list) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_trst()")
        return

    if do_assert:
        op = CLEAR
    else:
        op = SET
    write_dbu_using_corelist(op, DBU_CPU_NTRST, core_list, "trst_ctrl")

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_trst()")

def set_cpu_debug_enable(do_enable, core_list):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> set_cpu_debug_enable(do_enable %s, core_list %s)" % (do_enable, core_list) )

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_cpu_debug_enable()")
        return

    if do_enable:
        op = SET
    else:
        op = CLEAR
    write_dbu_using_corelist(op, DBU_CPU_DEBUG_EN, core_list, "debug_ctrl")

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< set_cpu_debug_enable()")


def setup_reset_state(coreName):

    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> setup_reset_state(%s)" % (coreName,))

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        if dbu_var["verbose"]:
            sttp.logging.print_out("<<<< set_cpu_debug_enable()")
        return

    tp = sttp.targetinfo.get_targetstring_parameters() # Mastered on host.
    if tp.has_key("stmc_pokepeek_timeout"):
        timeout = tp["stmc_pokepeek_timeout"]

        try:
            c = sttp.stmc.get_core_params(coreName)
        except:
            c = dict()

        c["stmc_pokepeek_timeout"] = timeout

        sttp.stmc.set_core_params(coreName, c)

    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< setup_reset_state()")

def dbu_wait(idle_sync):
    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> dbu_wait(idle_sync %d)" % (idle_sync,) )
    sttp.jtag.sequence({'tms':          '0'*idle_sync, \
                        'td_to_target': '0'*idle_sync, \
                       })
    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< dbu_wait()")
 
def dbu_nrst():
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> dbu_nrst()")
    sttp.jtag.sequence({'nrst'        : '100011'  , \
                        'tms'         : '000000'  , \
                        'td_to_target': '000000'  , \
                       })
    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< dbu_nrst()")

## scan_dr : shift data in tap-DR (note that tapmux must be in autoclock mode) ##
def scan_dr(data,len,idle_sync):

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> scan_dr(0x%08x, %d)" % (data, len))

    tdi = ""
    for x in range(len):
        tdi += str((data>>x)&1)  
         
    r = sttp.jtag.sequence({'tms':          '100' + '0'*(len-1) + '1' + '10',  \
                            'td_to_target': '000' +        tdi        + '00',  \
                           })
    tdo = r['td_from_target']
    # set tdo in good order
    tdo_u = tdo[2+dbu_var["shift_tdo"]:2+dbu_var["shift_tdo"]+len]

    sttp.jtag.sequence({'tms':          '0'*idle_sync, \
                        'td_to_target': '0'*idle_sync, \
                       })

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< scan_dr()")

    return tdo_u[::-1]

## end scan_dr ##


## scan_ir : shift data in tap-IR (note that tapmux must be in autoclock mode) ##
def scan_ir(data,len):
    global dbu_var

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> scan_ir(0x%08x, %d)" % (data, len))

    dbu_var["bus_access"] = 0 
    dbu_var["ctrl_access"] = 0    
    if data == 5: 
        dbu_var["bus_access"] = 1
    if data == 4: 
        dbu_var["ctrl_access"] = 1          

    tdi = ""
    for x in range(len):
        tdi += str((data>>x)&1)  

    r = sttp.jtag.sequence({'tms':          '1100' + '0'*(len-1) + '1' + '10',  \
                            'td_to_target': '0000' +        tdi        + '00',  \
                           })
    tdo = r['td_from_target']
    tdo_u = tdo[2:2+dbu_var["ir_len"]]

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< scan_ir()")

    return tdo_u[::-1]
## end scan_ir ##

## sync : reset TAP then try to find a good value for sync_time to synchronyse TAP and dbu_reg ##
def sync():
    global dbu_var  

    if dbu_var["debuglow"]:
        sttp.logging.print_out(">>>> sync()" )

    sttp.logging.print_out(" obsolete function - just here for backward compatibility     ")
    # reset then go idle
    sttp.jtag.sequence({'tms':          '11111_00', \
                        'td_to_target': '00000_00', \
                       })

    if dbu_var["bus_access"] == 0:
        scan_ir(0x5,dbu_var["ir_len"]) # internal bus access

    for i in range(0,dbu_var["sync_time"]):
        scan_dr(0xDEADBEEFFF,dbu_var["dr_len"],i)
        tdo = scan_dr(0x00000000FF,dbu_var["dr_len"],i)
        #sttp.logging.print_out("  dbu.sync : tdo =", hex(int(tdo,2)) )
        if int(tdo,2) == 0xdeadbeeffc:
            dbu_var["sync_time"] = i + 2
            sttp.logging.print_out("  dbu.sync : find a good value for sync_time:",dbu_var["sync_time"])
            return 

    sttp.logging.print_out("  dbu.sync : could not find a good value for sync_time")

    if dbu_var["debuglow"]:
        sttp.logging.print_out("<<<< sync()" )
## end sync ##

def setup_no_reset_state(coreName):
    if dbu_var["verbose"]:
        sttp.logging.print_out(">>>> setup_no_reset_state(%s)" % (coreName,))
    pass
    if dbu_var["verbose"]:
        sttp.logging.print_out("<<<< setup_no_reset_state()")

def debug_print(p):
    sttp.logging.print_out(p)

#
# Return the set of modepins defined for all the DBUs
#

def get_modepin_definitions():
    modepins = {}    
    for n, v in dbu_info.iteritems():         
        if v.has_key("modepins"):
            modepins.update(v["modepins"])            
    return modepins

