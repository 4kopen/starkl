# TargetScript for connecting and disconnecting to a mb521 (std2000)

import time

import sttp
import sttp.stmc
import sttp.logging
import sttp.targetpack

# TargetPack imports
import stv0498
import utilities

global sttp_root
sttp_root = sttp.targetpack.get_tree()

def mb521_configure(parameters):
    # Apply core address offset.
    connect_core = sttp.targetinfo.get_targetstring_core()
    # Set a core specific view of the registers for features such as Memory Mapped Register Set
    if connect_core == "st40":
        sttp.targetpack.apply_address_offset("st40_offset")

def mb521_connect(parameters):
    sttp.logging.print_out("mb521 connect start - parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(True, parameters)

    # Connect to stv0498 SoC
    stv0498.connect(parameters)

    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        # Set ST40 view of register addresses during connect.
        sttp.targetpack.apply_address_offset("st40_offset")
        sttp.pokepeek.enable()

        sttp.logging.print_out( "mb521 initialization start ..." )
        mb521_setup(parameters)

    utilities.set_tck_frequency(False, parameters)

    sttp.logging.print_out( "mb521 initialization complete" )


def mb521_disconnect():
    pass

def mb521_sysconf_configure():
    sys_conf = sttp_root.stv0498.SYSCONF.stv0498_sysconf_regs

    sys_conf.SYSCONF_SYS_CFG11.poke(0x0000026f)
    sys_conf.SYSCONF_SYS_CFG12.poke(0x0000026f)
    sys_conf.SYSCONF_SYS_CFG08.poke(0x40000003)
    sys_conf.SYSCONF_SYS_CFG09.poke(0x00000000)
    sys_conf.SYSCONF_SYS_CFG07.poke(0x00004080)
    sys_conf.SYSCONF_SYS_CFG07.poke(0x000040c0)


# Configure the mb521
def mb521_setup(parameters):

#   gdb standard toolset does no clockgen_configure
    sttp.pokepeek.rom_delay_scale(333)

    mb521_sysconf_configure()
    mb521_emi_configure()
    mb521_lmi_configure()

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        ccn = sttp_root.stv0498.st40.CCN.st40_ccn_regs
        ccn.CCN_CCR.poke(0x8000090d)

#  mb521 EMI Configuration
def mb521_emi_configure():
    emi = sttp_root.stv0498.EMI4.st40_emi_regs

    emi.EMI_BANK_ENABLE.poke(0x00000005)
    emi.EMI_BANK0_BASEADDRESS.poke(0x00000000)
    emi.EMI_BANK1_BASEADDRESS.poke(0x00000002)
    emi.EMI_BANK2_BASEADDRESS.poke(0x00000006)
    emi.EMI_BANK3_BASEADDRESS.poke(0x0000000a)
    emi.EMI_BANK4_BASEADDRESS.poke(0x0000000e)
#   "Bank 0: Configured for on-board Flash 8Mb"
    emi.EMI_BANK0_EMICONFIGDATA0.poke(0x001016d1)
    emi.EMI_BANK0_EMICONFIGDATA1.poke(0x9d200000)
    emi.EMI_BANK0_EMICONFIGDATA2.poke(0x9d220000)
    emi.EMI_BANK0_EMICONFIGDATA3.poke(0x00000000)
#   "Bank 1: Configured for cable card 16Mb"
    emi.EMI_BANK1_EMICONFIGDATA0.poke(0x001016d1)
    emi.EMI_BANK1_EMICONFIGDATA1.poke(0x9d200000)
    emi.EMI_BANK1_EMICONFIGDATA2.poke(0x9d220000)
    emi.EMI_BANK1_EMICONFIGDATA3.poke(0x00000000)
#   "Bank 2: Configured for on-board FPGA 16Mb"
    emi.EMI_BANK2_EMICONFIGDATA0.poke(0x04310ef1)
    emi.EMI_BANK2_EMICONFIGDATA1.poke(0x8a222000)
    emi.EMI_BANK2_EMICONFIGDATA2.poke(0x8a224200)
    emi.EMI_BANK2_EMICONFIGDATA3.poke(0x00000000)
#   "Bank 3: Undefined 16Mb" Unused (reset configuration not changed)

# mb521 LMI Configuration
def mb521_lmi_configure():                      ## PAH Check values
    # Configuring LMI- for DDR SDRAM
#  "Configuring LMI for DDR SDRAM (16 bit, CAS 2.5 @ 133MHz-150MHz)"
    lmi = sttp_root.stv0498.LMI.st40_lmi3_regs
    # SDRAM Mode Register
    lmi.LMI_MIM_0.poke(0x04100203)

    # SDRAM Timing Register
    lmi.LMI_STR_0.poke(0x352b4235)

    # SDRAM Row Attribute 0
    lmi.LMI_SDRA0_0.poke(0x10001a00)

    # SDRAM Row Attribute 1
    lmi.LMI_SDRA1_0.poke(0x10001a00)

    # SDRAM Control Register
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000003)
    lmi.LMI_SCR_0.poke(0x00000001)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SDMR0.poke(0x00000402)
    lmi.LMI_SDMR1.poke(0x00000402)
    sttp.stmc.delay(200)
    lmi.LMI_SDMR0.poke(0x00000163)
    lmi.LMI_SDMR1.poke(0x00000163)
    lmi.LMI_SCR_0.poke(0x00000002)
    lmi.LMI_SCR_0.poke(0x00000004)
    lmi.LMI_SCR_0.poke(0x00000004)
    lmi.LMI_SCR_0.poke(0x00000004)
    lmi.LMI_SDMR0.poke(0x00000063)
    lmi.LMI_SDMR1.poke(0x00000063)
    lmi.LMI_SCR_0.poke(0x00000000)

