"""
This is a simple TAPLink Poke/Peek SessionApp
It takes commands on stdin to poke, peek or quit.

It is started on one of two ways. These take arguments
to either test or to operate.

Form 1

  ... -test {driver}

Form 2
 

  ... -driver {driver}

In addition the following test mode is supported for testing
the script standalone 

Form 3
 
  ... -drivertest {driver}

The poke/peek DSU functionality has been taken from
  
  ADCS 7511658C - ST230 Core and instruction set architecture manual 
  /vob/mcdtgnb-src-st200emu/hti/dsu.cxx 


"""
import os
import re
import select
import struct
import sys
import time

logfile = None
driver  = None
driverTest = False

 # DSU Register locations

DSX_VERSION = 0
DSX_STATUS  = 1
DSX_ARG2    = 10
DSX_ARG1    = 11
DSX_RESPONSE = 13

# DSU Commands

DSU_PEEKED = 1
DSU_FLUSH  = 2
DSU_POKE   = 3
DSU_PEEK   = 4
DSU_GOT_EXCEPTION = 5

# TAP Commands

TAP_POKE   = 0
TAP_PEEK   = 1
TAP_PEEKED = 2
TAP_EVENT  = 3
TAP_OPCODE_MASK = 3

TAP_EVENT_REASON_LO = 2
TAP_EVENT_DEFAULT = TAP_EVENT + (1 << TAP_EVENT_REASON_LO)
TAP_HEADER_BITS = 2

def TAP_Poke(address, words):
    """
    DSU register write or a set of words
    TAPlink poke of one word
    """
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_Poke(0x%08x, 0x%08x)\n" % (address, words[0]) )
#        logfile.flush()

    size = len(words)

    if size<1 or size>63:
        sys.stderr.write("TAP_Poke packet too large - address 0x%08x, length %d\n" % (address, size))
        sys.exit(1)

    # Build the taplink poke command
    tapCmd = (size<<2) | TAP_POKE;
    packFormat = "<BB" + "L" * len(words)

    # build the call to eval with each element of words as an argument

    callStr = "struct.pack('%s', tapCmd, address" % packFormat
    for i in range(0,size):
        callStr = callStr + ", words[%d]" % i
    callStr += ")"

    bytes = eval(callStr)

#    if logfile:
#        logfile.write("TAP_Poke(): writing %d bytes\n" % (len(bytes),) )
#        logfile.flush()

    result = os.write(driver, bytes);
    if result != len(bytes):
        sys.stderr.write("TAP_Poke - could not write bytes\n")
        sys.exit(1)

#    if logfile:
#        logfile.write("<<<< TAP_Poke(%d)\n" % (result,) )
#        logfile.flush()
    

def TAP_Peek(address):
    """
    DSU registers read one word
    TAPlink peek one word
    """
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_Peek(0x%08x)\n" % (address,) )
#        logfile.flush()

    # Build the taplink peek command
    cmd = (1<<2) | TAP_PEEK;
    bytes = struct.pack("<BB", cmd, address)

    result = os.write(driver, bytes);
    if result != len(bytes):
        sys.stderr.write("TAP_Peek - could not write bytes\n")
        sys.exit(1)

    peeked_length = TAP_GetPeeked()
    if peeked_length != 1:
        # DSU_FlushTap(peeked_byte_length)
        sys.stderr.write("TAP_Peek - invalid peeked_length %d\n" % peeked_length)
        sys.exit(1)
 
    data = os.read(driver, 4*peeked_length);
    if driverTest:
        data = struct.pack("<I", 0)

#    data = os.read(driver, 5);
    [value] = struct.unpack("<L", data)

#    if logfile:
#        logfile.write( "dPeek: len(data) %d, value %s\n" % (len(data), value) )
#        logfile.flush()
    
#    if logfile:
#        logfile.write("<<<< TAP_Peek(0x%08x)\n" % (value,) )
#        logfile.flush()

    return value

def TAP_GetPeeked():
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_GetPeeked()\n" )
#        logfile.write("     time %s\n" % (time.time(),) )
#        logfile.flush()

    if driverTest:
        return 1

    while 1:
        # wait 10 seconds 

        r = []; w = []; e = []
        r.append(driver);
        r, w, e = select.select(r, w, e, 10)
        if len(r)==0:
#            if logfile:
#                logfile.write("TAP_GetPeeked: target failed to respond to peek request after 10 seconds\n")
#                logfile.flush()
            sys.stderr.write("TAP_GetPeeked - target failed to respond to peek request after 10 seconds\n")
            sys.exit(1)

        data = os.read(driver, 1)
        [byte] = struct.unpack("<B", data)
 
        count = (byte & ~3) >> TAP_HEADER_BITS

        if ((byte & 3) == TAP_PEEKED) and (count == 1):
            break

        sys.stderr.write("TAP_GetPeeked - unexpected byte reply %d\n" % byte)

    if (byte & 3) != TAP_PEEKED:
        sys.stderr.write("TAP_GetPeeked - unexpected byte reply %d\n" % byte)
        sys.exit(1)

    retVal = (byte & ~3) >> TAP_HEADER_BITS

#    if logfile:
#        logfile.write("     time %s\n" % (time.time(),) )
#        logfile.write("<<<< TAP_GetPeeked(%d)\n" % (retVal,) )
#        logfile.flush()

    return retVal


def TAP_WaitByte(expectedByte):
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_WaitByte()\n" )
#        logfile.write("     time %s\n" % (time.time(),) )
#        logfile.flush()

    while 1:
        # wait 10 seconds
        r = []; w = []; e = []
        r.append(driver);
        r, w, e = select.select(r, w, e, 10)
        if len(r)==0:
#            if logfile:
#                logfile.write("TAP_WaitByte: target failed to respond to peek request after 10 seconds\n")
#                logfile.flush()
            sys.stderr.write("TAP_WaitByte: target failed to respond to peek request after 10 seconds\n")
            sys.exit(1)
        

        data = os.read(driver, 1)
        [byte] = struct.unpack("<B", data)

        if byte == expectedByte:
            break

        sys.stderr.write("TAP_WaitByte - unexpected byte reply %d\n" % byte)

#    if logfile:
#        logfile.write("     time %s\n" % (time.time(),) )
#        logfile.write("<<<< TAP_WaitByte()\n" )
#        logfile.flush()

def TAP_Flush():
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_Flush()\n" )
#        logfile.flush()

    count = 0
    while 1:
        r = []; w = []; e = []
        r.append(driver);
        r, w, e = select.select(r, w, e, 0)
        if len(r) == 0:
            # Nothing to read
            break;

        # read a single byte
        data = os.read(driver, 1)
        count = count + 1

#        if logfile:
#            [byte] = struct.unpack("<B", data)
#            logfile.write("<<<< TAP_Flush() - byte read 0x%08x\n" % (byte,) )
#            logfile.flush()

#    if logfile:
#        logfile.write("<<<< TAP_Flush() flushed %d bytes\n" % (count,) )
#        logfile.flush()

def TAP_SendEvent(event):
    global logfile

#    if logfile:
#        logfile.write(">>>> TAP_SendEvent(%d)\n" % (event,))
#        logfile.flush()

    bytes = struct.pack("<B", event)
    os.write(driver, bytes)

#    if logfile:
#        logfile.write("<<<< TAP_SendEvent()\n" )
#        logfile.flush()

def DSU_IsHalted():
    global logfile

#    if logfile:
#        logfile.write(">>>> DSU_IsHalted()\n" )
#        logfile.flush()

    status = TAP_Peek(DSX_STATUS)
    if status & 0x1:
        return True
    else:
        return False

#    if logfile:
#        logfile.write("<<<< DSU_IsHalted()\n" )
#        logfile.flush()

def DSU_ForceHalt():
    """

    """
    global logfile

#    if logfile:
#        logfile.write(">>>> DSU_ForceHalt()\n" )
#        logfile.flush()

#    if False == DSU_IsHalted():
#        bytes = struct.pack("<B", TAP_EVENT_DEFAULT)
#        os.write(driver, bytes)

#        response = DSU_GetResponse()

#        if response == DSU_GOT_EXCEPTION:
#            sys.stderr.write("DSU_ForceHalt - Exception when getting response\n")
#            sys.exit(1)

#        DSU_ClearResponse()

#        if False == DSU_IsHalted():
#            sys.stderr.write("DSU_ForceHalt - failed to force halt\n")
#            sys.exit(1)
 
    TAP_SendEvent(TAP_EVENT_DEFAULT)

#    bytes = struct.pack("<B", TAP_EVENT_DEFAULT)
#    os.write(driver, bytes)       

#    status = TAP_Peek(DSX_STATUS)
    status = 1

#    if logfile:
#        logfile.write("<<<< DSU_ForceHalt() status 0x%08x\n" % (status,) )
#        logfile.flush()

def DSU_Reset():
    """`
    This sets up the DSU for subsequent pokes and peeks
    It assumes the CPU has not been running with any kind of breakpoints         
    and that the default DSU ROM monitor will be used for pokes and peek
    """
#    if logfile:
#        logfile.write(">>>> DSU_Reset()\n" )
#        logfile.flush()

    TAP_Flush()

    version = TAP_Peek(DSX_VERSION)
    status = TAP_Peek(DSX_STATUS)

    # Assume in DSU loop from reset?
    DSU_ForceHalt()

    TAP_Flush()

    # Set bit #16 to 1 to indicate the application that the adaptor is present
    status = status | 1<<16;
    # Clean bit #17 to 0 because we are now halted in debug ROM */
    status = status & ~(1<<17);   
    TAP_Poke(DSX_STATUS, (status,));

    # This peek may seem to flush out something and make subsequent peeks reliable!
#    response = TAP_Peek(DSX_RESPONSE)
#    if logfile:
#        logfile.write("<<<< DSU_Reset DSX_RESPONSE - 0x%08x\n" % (response,) )
#        logfile.flush()   

#    if logfile:
#        logfile.write("<<<< DSU_Reset()\n" )
#        logfile.flush()

def DSU_GetResponse():
    """
    After The ST200/Lx has executed each command a response is sent
    with TAPLINK_EVENT_DEFAULT (response 7)
    Read the response
    """

#    if logfile:
#        logfile.write(">>>> DSU_GetResponse()\n" )
#        logfile.flush()

    if driverTest:
        return 0

    TAP_WaitByte(TAP_EVENT_DEFAULT)    

    response = TAP_Peek(DSX_RESPONSE)

    # The ST200 monitor waits for reponce to be cleared before executing another command
    if response == DSU_GOT_EXCEPTION:
        sys.stderr.write("DSU_getResponse - Exception when getting response\n")
        sys.exit(1)

    DSU_ClearResponse()

#    if logfile:
#        logfile.write("<<<< DSU_GetResponse(%d)\n" % (response,) )
#        logfile.flush()

    return response;
        
def DSU_ClearResponse():
#    if logfile:
#        logfile.write(">>>> DSU_ClearResponse()\n" )
#        logfile.flush()

    TAP_Poke(DSX_RESPONSE,(0,)); 

#    if logfile:
#        logfile.write("<<<< DSU_ClearResponse()\n" )
#        logfile.flush()


def Poke(addr, value):
    """
    This function performs a target poke using the DSU
    """
    global driver
    global logfile

#    if logfile:
#        logfile.write(">>>> Poke(0x%08x, %d)\n" % (addr, value) )
#        logfile.flush()

    if addr & 3:
        sys.stderr.write("Unaligned poke address 0x%08x\n" % addr)
        sys.exit(1)

    words = (value, addr, DSU_POKE)
    TAP_Poke(DSX_ARG2, words)
    # wait for completion
    DSU_GetResponse() 

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

    if logfile:
        logfile.write( "Poke: addr: " + hex(addr) + " value: " + hex(value) + "\n")
        logfile.flush()

#    if logfile:
#        logfile.write("<<<< Poke()\n" )
#        logfile.flush()

    return True

def Peek(address):
    """
    This function performs a target peek using the DSU
    """
    global logfile

#    if logfile:
#        logfile.write(">>>> Peek(0x%08x)\n" % (address,) )
#        logfile.flush()

    if address & 3:
        sys.stderr.write("Unaligned peek address 0x%08x\n" % address)
        sys.exit(1)

    words = (address, DSU_PEEK)
    TAP_Poke(DSX_ARG1, words)
    response = DSU_GetResponse()
    if False == driverTest and response != DSU_PEEKED:
        sys.stderr.write("Unexpected peek response %d\n" % response)
        sys.exit(1)
 
    value = TAP_Peek(DSX_ARG1)

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

    if logfile:
        logfile.write( "Peek: addr: " + hex(address) + " value: " + hex(value) + "\n")
        logfile.flush()

#    if logfile:
#        logfile.write("<<<< Peek()\n" )
#        logfile.flush()

    return True

def DPoke(addr, value):
    """
    This function performs a target poke using the DSU
    """
    global driver
    global logfile

    # TODO - error handling should not terminate
    if addr & 3:
        sys.stderr.write("Unaligned poke address 0x%08x\n" % addr)
        sys.exit(1)

    words = (value,)
    TAP_Poke(addr, words)

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

#    if logfile:
#        logfile.write( "Poke: addr: " + hex(addr) + " value: " + hex(value) + "\n")
#        logfile.flush()
    return True

def DPeek(address):
    """
    This function performs a target peek using the DSU
    """
    global driver
    global logfile

    # TODO - error handling should not terminate
#    if address & 3:
#        sys.stderr.write("Unaligned peek address 0x%08x\n" % address)
#        sys.exit(1)

    value = TAP_Peek(address)

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

#    if logfile:
#        logfile.write( "Peek: addr: " + hex(address) + " value: " + hex(value) + "\n")
#        logfile.flush()
    return True

def main():
    global driver
    global logfile
    global driverTest

    b = time.time()
    pTime = 0.0

    sys.stdout.write("Started " + sys.argv[0] + " - args: " + str(sys.argv) + "\n")
    sys.stdout.flush();

    if len(sys.argv)>=3 and sys.argv[1] == "-test":
        driver = os.open(sys.argv[2], os.O_RDWR)
        os.close(driver)
        sys.exit(0)

    if len(sys.argv)>=3 and sys.argv[1] == "-driver":
        driver = os.open(sys.argv[2], os.O_RDWR)

    # test mode - write to file
    if len(sys.argv)>=3 and sys.argv[1] == "-drivertest":
        driver = os.open(sys.argv[2], os.O_RDWR | os.O_CREAT | os.O_TRUNC)
        driverTest = True

    if len(sys.argv)>=5 and sys.argv[3] == "-log":
        logfile = file(sys.argv[4], "w")

    if logfile:
        logfile.write("Done stdout write Started ... " + sys.argv[0] + ", driver " + str(driver) + "\n")
        logfile.flush()

    DSU_Reset()

    if logfile:
        logfile.write("Done DSU reset -waiting for commands")
        logfile.flush()

    while 1:
        done = False
        line = sys.stdin.readline()

        if logfile:
            logfile.write("stdin command: %s\n" % (line,)) 
            logfile.flush()

        print >> sys.stderr, "Command:", line

        # quit command
        if re.match(r'^quit', line):
            os.close(driver)
            d = time.time() - b
            print >> sys.stderr, "Time in poke/peek %s, time running sessionApp %s" % (pTime, d)
            sys.exit(0)

        # poke command
        m = re.match(r'^poke ([0-9]+|0x[0-9A-Fa-f]+)L*\s+([0-9]+|0x[0-9A-Fa-f]+)L*$', line)
        if m:
            bp = time.time()
            done = Poke(int(m.group(1), 0), int(m.group(2), 0))
            pTime = pTime + (time.time()-bp)

        # peek command
        m = re.match(r'^peek ([0-9]+|0x[0-9A-Fa-f]+)L*$', line)
        if m:
            bp = time.time()
            done = Peek(int(m.group(1), 0))
            pTime = pTime + (time.time()-bp)

        if False == done:
            sys.stderr.write("Failed to handle command %s\n" % line)
            sys.exit(1)

if __name__ == "__main__":
    main()
