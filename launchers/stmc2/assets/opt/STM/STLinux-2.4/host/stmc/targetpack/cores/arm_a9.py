import time

import sttp
import sttp.stmc

import st200_nomux
import utilities
import coresight
import core_utils

# import tap
# import tap.jtag

target_timeout = 15

ReadWriteMemAP = coresight.ReadWriteMemAP_core
Read = coresight.Read
Write = coresight.Write

"""
version of 2015 4th March:
dump_rom_info(parameters, core) is replaced by coresight.parse_rom_table(cp, ap, base_addr)
dump_ap_info(parameters, core) is replaced by coresight.ap_discovery(core)
"""
    
def dump_arm_state(parameters, core):
    cp = sttp.stmc.get_core_params(core)    
    
#    sttp_root = sttp.targetpack.get_tree()
#    soc = sttp.targetpack.get_soc()
#    evalStr = "sttp_root." + soc.name + "." + core + ".arm_cortex_core"
#    armInfo = eval(evalStr)
    
    coresight.dump_arm_dp_info(cp)

#    coresight_base   = int(armInfo.get_parameters()["coresight_debug"], 0)
    coresight_base = int(cp['coresight_debug'], 0)
#    coresight_mem_ap = int(armInfo.get_parameters()["coresight_mem_ap"], 0)
    coresight_mem_ap = int(cp["coresight_mem_ap"], 0)
   
    coresight.dump_arm_debug_logic_regs(cp, coresight_mem_ap, coresight_base)

def put_arm_in_reset(parameters, core):

    #if ( parameters.has_key("no_reset") and 1 == int(parameters["no_reset"]) ) or \
    #   ( parameters.has_key("no_sys_reset") and 1 == int(parameters["no_sys_reset"]) ):
    #    return

    cp = sttp.stmc.get_core_params(core)    

#    sttp_root = sttp.targetpack.get_tree()
#    soc = sttp.targetpack.get_soc()
#    evalStr = "sttp_root." + soc.name + "." + core + ".arm_cortex_core"
#    armInfo = eval(evalStr)

#    coresight_base   = int(armInfo.get_parameters()["coresight_debug"], 0)
    coresight_base = int(cp['coresight_debug'], 0)
#    coresight_mem_ap = int(armInfo.get_parameters()["coresight_mem_ap"], 0)
    coresight_mem_ap = int(cp["coresight_mem_ap"])

    coresight.power_up_debug(cp)

    DBGDRCR = coresight_base + 0x090
    DBGPRCR = coresight_base + 0x310   
    DBGLAR  = coresight_base + 0xFB0

    # Unlock
    Write(cp, core, coresight_mem_ap, DBGLAR, 0xC5ACCE55)

    # Set the hold in reset bit
    r = Read(cp, core, coresight_mem_ap, DBGPRCR)
    sttp.logging.print_out("Current DBGPRCR: 0x08%x\n" % r)
    r = r | (1 << 2)
    sttp.logging.print_out("Holding ARM in reset: setting DBGPRCR: 0x%08x\n" % r)
    Write(cp, core, coresight_mem_ap, DBGPRCR, r)

    # Set the halt request bit so when sys reset is deasserted, we are in debug mode
    r = Read(cp, core, coresight_mem_ap, DBGDRCR)
    sttp.logging.print_out("Current DBGDRCR: 0x08%x\n" % r)
    r = r | (1 << 0)
    Write(cp, core, coresight_mem_ap, DBGDRCR, r)


def a9romtable (base, indent=0):
    cid = 0
    for i in range(base+0xffc, base+0xfec, -4):
        value = sttp.pokepeek.peek(i)
        cid <<= 8
        cid |= value & 0xff
            
    pid = 0
    for i in range(base+0xfec, base+0xfdc, -4):
        value = sttp.pokepeek.peek(i)
        pid <<= 8
        pid |= value & 0xff
                    
    sttp.logging.print_to_out( "%s0x%08x: Comp ID 0x%08x, Perip ID 0x%08x:" % (" "*indent, base, cid, pid) )
                        
    if (0x9000 == (cid & 0xf000)):
        # this is a CoreSight Component
        val = sttp.pokepeek.peek(base + 0xfcc)
                        
        lookups_debug1 = [
                    "Coresight component: Debug control - other",
                    "Coresight component: Debug control - Trigger Matrix",
                    "Coresight component: Debug control - Debug authentication",
                    "Coresight component: Debug control - Reserved",
                  ]

        lookups_debug2 = [
                    "Coresight component: Debug logic - other",
                    "Coresight component: Debug logic - processor core",
                    "Coresight component: Debug logic - DSP",
                    "Coresight component: Debug logic - Data engine or coprocessor",
                    "Coresight component: Debug logic - Reserved",
                  ] 
        
        lookups = [ "Coresight component: Miscellaneous",
                    "Coresight component: Trace Sink",
                    "Coresight component: Trace Link",
                    "Coresight component: Trace Source",
                    lookups_debug1,
                    lookups_debug2,
                  ]

        val_lo = val & 0xf

        if val_lo < len(lookups):
            info = lookups[val_lo]
            if isinstance(info, str):
                sttp.logging.print_out( "  %s" % (info,))
            else:
                val_hi = ( val & 0xf0 ) >> 4

                if val_hi < len(info):
                    info2 = info[val_hi]
                    sttp.logging.print_out( "  %s" % (info2,))
                else:
                    sttp.logging.print_out( "  Coresight component: Reserved")
        else:
            sttp.logging.print_out( "  Coresight component: Reserved")
    elif (0x1000 == (cid & 0xf000)):
        # A ROM table
        sttp.logging.print_out( "  ROM Table" )
        for i in range(0,31):
            entry =  sttp.pokepeek.peek(base+i*4);
            if (entry & 0x3) == 0x3 :
                table_base = base + (entry & 0xFFFFFF00)
                a9romtable(table_base, indent+2)
    else:
        sttp.logging.print_out( "  Unknown CID" )


do_debug_print = False

def debug_print(p):
    if do_debug_print:
        sttp.logging.print_out(p)

core_info = {}

def init(arm_core_info, parameters):

    global do_debug_print
    global core_info

    if parameters.has_key("targetpack_debug") and parameters["targetpack_debug"].find("arm_a9")!=-1:
        do_debug_print = True

    if parameters.has_key("targetpack_debug") and parameters["targetpack_debug"].find("all")!=-1:
        do_debug_print = True
    
    sttp_root = sttp.targetpack.get_tree()
    # soc = sttp.targetpack.get_soc()

    # Support "legacy" init() API - assumes a cluster of two
    if isinstance(arm_core_info, str):
        arm_core_name_base = arm_core_info
        arm_core_name      = arm_core_name_base
        if arm_core_name.endswith("_"):
            arm_core_name = arm_core_name[0:len(arm_core_name)-1]
        arm_cluster_count  = 2
    else:
        arm_core_name = arm_core_info["arm_core_name"]
        if arm_core_info.has_key("index_prefix"):
            arm_core_name_base = arm_core_name + arm_core_info["index_prefix"]
        arm_cluster_count = arm_core_info["arm_cluster_count"]

    arm_core = core_utils.find_cpu(sttp_root, arm_core_name)
    a9core_clusters = {}

    # Legacy mode where we have not defined a "a9" core but only "a9_0" and "a9_1"
    # c.f. "/opt/STM/STLinux-2.4/host/stmc/targetpack/boards/fli76xxboard/fli76xxev01"
    if None == arm_core:
        arm_core = core_utils.find_cpu(sttp_root, arm_core_name_base + "0")
    else:
        # Auto populate the a9, a9_0, a9_1, etc data structures

        # The "a9" name
        arm_core.parameters.update( {"core_id": str(0) } )
        arm_core.parent.parameters.update( {"core_id": str(0) } )
        a9core_clusters[arm_core_name] = arm_core.parameters

        # The "a9_0", "a9_1 ..."
        for i in range(arm_cluster_count):
            core = core_utils.find_cpu(sttp_root, arm_core_name + "_" + str(i) )
            core.parameters = dict( arm_core.parameters )
            core.parameters.update( {"core_id": str(i) } )
            core.parent.parameters = dict( arm_core.parent.parameters )
            core.parent.parameters.update( {"core_id": str(i) } )

            a9core_clusters[arm_core_name + "_" + str(i)] = core.parameters

    if arm_core:
        container = arm_core.parent.parent
    else:
        raise sttp.STTPException("Failed to find cpu name %s" % (arm_core_name,))

    coresight_cti_base = [None] * arm_cluster_count
    coresight_debug_base = [None] * arm_cluster_count
    coresight_amba_apb = None
    l2cache_base = None
    coresight_stbus_base = [None] * arm_cluster_count

    # Iterate of all components in the "container" which may be the "soc"
    for d in container.__dict__:
        # See if the component name starts with "a9ss_l2cache..."
        if (None == l2cache_base) and (0 == d.find("a9ss_l2cache")):
            e = eval("container." + d)
            l2cache_base = e.a9ss_l2cache.baseAddress

            # print "Found l2cache_base: ", l2cache_base

        if (None == coresight_amba_apb) and (0 == d.find("coresight_amba_apb")):
            e = eval("container." + d)
            coresight_amba_apb = str(e.parameters["memap"])

        for i in range(arm_cluster_count):
            if (None == coresight_cti_base[i] and (0 == d.find("coresight_cti_c" + str(i)))):
                e = eval("container." + d)
                coresight_cti_base[i] = str(e.parameters["APB_baseAddress"])

            if (None == coresight_debug_base[i] and (0 == d.find("coresight_debug_c" + str(i)))):
                e = eval("container." + d)
                coresight_debug_base[i] = str(e.parameters["APB_baseAddress"])
                core_info[arm_core_name_base + str(i)] = {
                    "coresight_debug_base"       : long(coresight_debug_base[i], 0),
                    "coresight_debug_stbus_base" : e.coresight_debug.baseAddress,
                }

    if None == coresight_amba_apb:
        raise sttp.STTPException("TargetPack error - component coresight_amba_apb not defined")
    for i in range(arm_cluster_count):
        if None == coresight_cti_base[i]:
            raise sttp.STTPException("TargetPack error - component coresight_cti_c%d not defined" % (i,))
        if None == coresight_debug_base[i]:
            raise sttp.STTPException("TargetPack error - component coresight_debug_c%d not defined" % (i,))

    names = list()
    names.append(arm_core_name)
    for i in range(arm_cluster_count):
        names.append(arm_core_name_base + str(i))

    tp = sttp.stmc.get_target_params()
    for c in names:
        cp = sttp.stmc.get_core_params(c)

        if l2cache_base:
            cp["a9ss_l2cache"] = str(l2cache_base)

        jtag_timeout = None
        if parameters.has_key("jtag_timeout"):
            #  Pick up jtag_timeout passed as TargetString parameter
            jtag_timeout = str(parameters["jtag_timeout"])       
        elif tp.has_key("jtag_timeout"): 
            # Pick up jtag_timeout if specified in TargetPack top-level XML
            jtag_timeout = tp["jtag_timeout"]

        if jtag_timeout:
            # If the value has not been passed as stmc_core_param_target_timeout set the core param
            if not cp.has_key("target_timeout"):
                cp["target_timeout"] = jtag_timeout
            if not cp.has_key("jtag_timeout"):
                cp["jtag_timeout"]   = jtag_timeout
            cp["linktimeout"] = jtag_timeout

        if parameters.has_key("smp_mode"):
            cp["smp_mode"] = parameters["smp_mode"]

        cp["coresight_memap"] = str(coresight_amba_apb)
        if arm_core_name==c:
            cp["coresight_debug"] = str(coresight_debug_base[0])
            cp["coresight_cti"] = str(coresight_cti_base[0])
        else:
            cp["coresight_debug"] = str(coresight_debug_base[int(c[len(arm_core_name_base):])])
            cp["coresight_cti"] = str(coresight_cti_base[int(c[len(arm_core_name_base):])])

        if arm_core_name==c or '0'==c[len(arm_core_name_base):]:
            for i in range(arm_cluster_count):
                cp["coresight_debug_c" + str(i)] = str(coresight_debug_base[i])
                cp["coresight_cti_c" + str(i)] = str(coresight_cti_base[i])

        # Add any parameterization which was constructed earlier
        if a9core_clusters.has_key(c):
            cp.update(a9core_clusters[c])

        sttp.stmc.set_core_params(c, cp)


def boot_a9(core):
        coresight_dbg_base = core_info[core]["coresight_debug_base"]
        stbus_dbg_base     = core_info[core]["coresight_debug_stbus_base"]

        debug_print( "  Core %s: coresight debug base: 0x%08x, stbus debug base: 0x%08x" % (core, coresight_dbg_base, stbus_dbg_base) )

        DBGDIDR       = stbus_dbg_base + 0x000 
        DBGPCSR       = stbus_dbg_base + 0x084
        DBGDSCR       = stbus_dbg_base + 0x088
        DBGDRCR       = stbus_dbg_base + 0x090
        DBGDPRSR      = stbus_dbg_base + 0x314
        DBGLAR        = stbus_dbg_base + 0xFB0
        DBGAUTHSTATUS = stbus_dbg_base + 0xFB8

        if "ROMGEN" != sttp.stmc.get_operating_mode():
            dbgdidr =  sttp.pokepeek.peek(DBGDIDR); 
            debug_print( "  %s debug id reg 0x%.8x\n " % (core, dbgdidr)); 

        # Unlock      
        sttp.pokepeek.poke(DBGLAR, 0xC5ACCE55);

        # enable halting mode debug
        sttp.pokepeek.or_const(DBGDSCR, DBGDSCR, 1<<14);

        # Check the DSCR halted bit
        sttp.stmc.delay(2000)

        if "ROMGEN" != sttp.stmc.get_operating_mode():
            dbgdscr =   sttp.pokepeek.peek(DBGDSCR)
            debug_print( "  %s DSCR (Debug Status and Control): 0x%08x\n" % (core, dbgdscr,))

            dbgprsr =   sttp.pokepeek.peek(DBGDPRSR)
            debug_print( "  %s PRSR (Power and Reset Status) before releasing reset : 0x%08x\n" % (core, dbgprsr,))

        # set Halt Request Bit(0) in DRCR - when core non-debug logic reset is deasserted, debug mode is entered
        debug_print( "  %s debug run control reg  => setting halt bit  " % (core,)) 
        sttp.pokepeek.or_const(DBGDRCR, DBGDRCR, 0x00000001)
        sttp.stmc.delay(1000)

        if "ROMGEN" != sttp.stmc.get_operating_mode():
            dbgdscr =   sttp.pokepeek.peek(DBGDSCR); 
            debug_print( "  %s halted : %.1d \n" % (core, dbgdscr&0x1))
            debug_print( "  %s secure priviledged invasive debug disabled : %.1d \n" % (core, (dbgdscr>>16)&0x1))
            debug_print( "  %s secure priviledged noninvasive debug disabled : %.1d \n" % (core, (dbgdscr>>17)&0x1))

            dbgauthstatus =  sttp.pokepeek.peek(DBGAUTHSTATUS);
            debug_print( "  %s authentication status reg 0x%.8x\n " % (core, dbgauthstatus))

def check_a9(core):

        coresight_dbg_base = core_info[core]["coresight_debug_base"]
        stbus_dbg_base     = core_info[core]["coresight_debug_stbus_base"]

        debug_print( "  Core %s: coresight debug base: 0x%08x, stbus debug base: 0x%08x" % (core, coresight_dbg_base, stbus_dbg_base) )

        DBGDIDR       = stbus_dbg_base + 0x000 
        DBGPCSR       = stbus_dbg_base + 0x084
        DBGDSCR       = stbus_dbg_base + 0x088
        DBGDPRSR      = stbus_dbg_base + 0x314
        DBGLAR        = stbus_dbg_base + 0xFB0
        DBGAUTHSTATUS = stbus_dbg_base + 0xFB8

        # Check the DSCR halted bit
        sttp.stmc.delay(1000)

        if "ROMGEN" != sttp.stmc.get_operating_mode():
            dbgdscr =   sttp.pokepeek.peek(DBGDSCR)
            debug_print( "  %s DSCR: 0x%08x\n" % (core, dbgdscr,))
            debug_print( "  %s halted : %.1d \n" % (core, dbgdscr&0x1))

            dbgprsr =   sttp.pokepeek.peek(DBGDPRSR)
            debug_print( "  %s PRSR (Power and Reset Status) after releasing reset : 0x%08x\n" % (core, dbgprsr,))

            dbgpcsr = sttp.pokepeek.peek(DBGPCSR)
            debug_print( "  %s PCSR (Program Counter) : 0x%08x\n" % (core, dbgpcsr))


