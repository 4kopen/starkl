# Script for board with typical single core jtag device

def jtag_connect(parameters):
    sttp.stmc.check_convertor_code(parameters["convertor_type"])

    try:
        frequency = int(parameters["tck_frequency"])
    except LookupError, e:
        raise sttp.STTPException("Parameter tck_frequency must be supplied")

    if frequency < 1:
        raise sttp.STTPException("tck_frequency must be greater than 0")

    real_tck = sttp.stmc.set_tck_frequency_no_override(frequency)

    sttp.logging.print_out( "jtag_connect: tck frequency requested %d Hz, set to %d Hz" % (frequency, real_tck) )

    for k, v in parameters["core_parameters"].iteritems():
        sttp.stmc.set_core_params(k, v)
        
    sttp.stmc.set_target_params(parameters["target_parameters"])

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
    
    if parameters["soc_info"]["debug"] == "tapmux":
        # Manual control of all JTAG lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        # Need this for TAPmux FPGA
        sttp.stmc.core_map({"tmc":"0"})
    else:
        # assume all initialized to 0 in driver
        p = "use_rtck"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_USE_RTCK", [ v ])
        
        p = "rtck_timeout"
        # default to 1
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 1 
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_RTCK_TIMEOUT", [ v ])

        p = "tdo_sample_delay"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_TDO_SAMPLE_DELAY", [ v ])
  
    inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1"})

    # Now enable outputs
    sttp.stmc.output_enable(True)
    
    p = "vtref_check"
    # default to 1
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 1

    if parameters["soc_info"]["debug"] == "jtagstreamer" and v:
        # Check vtref - note VTREF can be read when LVDS outputs are disabled
        if 0 == int(inputs["vtref"]):
            raise sttp.STTPException("vtref signal on target JTAG connector is low - target may be disconnected or not powered")

    tap_reset = True
    sys_reset = True

    if parameters.has_key("no_sys_reset") and int(parameters["no_sys_reset"])==1: 
        sys_reset = False

    if parameters.has_key("no_tap_reset") and int(parameters["no_tap_reset"])==1:
        tap_reset = False

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1:
        sys_reset = False
        tap_reset = False

    if sys_reset:
        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        sttp.jtag.sequence({'nrst' : "0"})
        # What should the min delay be? Currently 100mS
        time.sleep(0.1)
        sttp.jtag.sequence({'nrst' : "1"})    

    if tap_reset:
        sttp.jtag.sequence({'ntrst' : "1"})
        sttp.jtag.sequence({'ntrst' : "0"})
        # What should the min delay be? Currently 100mS
        time.sleep(0.1)
        sttp.jtag.sequence({'ntrst' : "1"})    
        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        # Move to RTI
        sttp.jtag.sequence({"tms": "111110",  "td_to_target": "000000"})

    if parameters["soc_info"]["debug"] == "tapmux":
        # Need this for TAPmux FPGA
        # Plumb the 1st JTAG module to the output lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        c = sttp.targetinfo.get_targetstring_core()
        sttp.stmc.core_map({c:"0"})

def jtag_disconnect():
    pass

def _jtag_get_topology(core_string):
    
    def add_core(hash, core_id, name, position, ir_length, bits_before):
        hash[name] = {"id"            : str(core_id),
                      "core_name"     : name, 
                      "chain_position": str(pos), 
                      "ir_length"     : str(length), 
                      "ir_bits_before": str(totalIrBits), 
                      "ir_bits_after" : str(-1),
                      "taps_before"   : str(pos),
                      "taps_after"    : str(-1) }
                      
    errString = "Invalid format for core_set - expecting core_name:ir_length{,core_name2:ir_length2} or [core_name1,core_name2:ir_length]..."
         
    coreDetails = {}
    pos = 0
    totalIrBits = 0
    coreId = 0
    
    while len(core_string)>0:
        if core_string[0] == "[":
            # A set of cores in a STAR
            f = core_string.find("]")
            if -1 == f:
                raise sttp.STTPException(errString)
        
            star_set = core_string[1:f]
            # there may be a trailing comman after the closing ]
            f += 1
            try:
                starCores, length = star_set.split(":")
                cores = starCores.split(",")
            except ValueError, e:
                raise sttp.STTPException(errString)
        
            irLength = int(length)
            
            for name in cores:
                add_core(coreDetails, coreId, name, pos, length, totalIrBits);
                coreId += 1
        else:
            f = core_string.find(",") 
            if (f == -1):
                # Assume only a single core specified on remainder of string
                f = len(core_string)
            x = core_string[0:f]

            try:
                name, length = x.split(":")
            except ValueError, e:
                raise sttp.STTPException(errString)
                
            irLength = int(length)
            add_core(coreDetails, coreId, name, pos, length, totalIrBits);
            coreId += 1

        pos += 1
        totalIrBits += irLength
        core_string = core_string[f+1:]
            
    # Fixup "items" after    
    for k, v in coreDetails.iteritems():
        c = v
        c["ir_bits_after"] = str(totalIrBits - int(c["ir_bits_before"]) - int(c["ir_length"]) )
        c["taps_after"]    = str(pos - int(c["chain_position"]) - 1)

    """    
    for k, v in coreDetails.iteritems():
        print "Core: ", k
        for k2, v2 in v.iteritems():
            print "    %s = > %s" % (k2, v2)
    """
    return coreDetails


def jtag_configuration(parameters):
    coreParams = {}
    targetParams = {}

    coreList = {}
    soc = {}

    d = os.getenv("TAPMUX_DRIVER")
    if d:
        print "WARNING: Using TAPMUX DRIVER"
        soc["debug"]     = "tapmux"
    else:
        soc["debug"]     = "jtagstreamer"
    
    firstCore = None

    # Handle the parameter "core_set"
    if parameters.has_key("core_set"):
        core_string = parameters["core_set"]
        
        coreParams = _jtag_get_topology(core_string)

        for k, v in coreParams.iteritems():
            irLength = int(coreParams[k]["ir_length"])
            id       = int(coreParams[k]["id"])
            coreList[k] = {"core_index"  : id,
                           "type"        : "jtag", 
                           "debug"       : "jtag", 
                           "resetSelect" : "true", 
                           "parameters"  :  {"ir_length" : irLength } }

        targetParams["core_set"] = parameters["core_set"]

        ac = ""
        for k in coreParams.keys():
           if ac != "":
               ac += ":"
           ac += str(k)
        parameters["active_cores"] = ac
    else:
        # This allows another connection to a target that has already had a TargetConnect
        # with full configuration details
        pass

    # Handle the parameter "ir_debug_value"
    if parameters.has_key("ir_debug_value"):
        c, v = parameters["ir_debug_value"].split(":")
        if not c or not v:
            raise sttp.STTPException("Invalid format for ir_debug_value - expected core_name:value")
        try:
            coreParams[c]["ir_debug_value"] = v
        except LookupError, e:
            raise sttp.STTPException("Invalid core specified in ir_debug_value: " + c)
        
    if parameters.has_key("gpio_permit"):
        gpio = parameters["gpio_permit"]
        coreSignals = gpio.split(",")
        for c in coreSignals:
            coreName, sigName = c.split(":")
            if not coreList.has_key(coreName):
                raise sttp.STTPException("Invalid core %s specified in gpio_permit." % (coreName) )
        
        targetParams["gpio_permit"] = parameters["gpio_permit"]

    # Backward compatability
    if parameters.has_key("fsm_state_sequence"): 
        del parameters["fsm_state_sequence"]
        
    if parameters.has_key("fsm_state_validate"):
        targetParams["fsm_state_validate"] = parameters["fsm_state_validate"]

    if parameters.has_key("use_fpga"):
        soc["debug_driverdata"] = parameters["use_fpga"].strip()
        
    # These are "system" parameters required by the host dll to do setup]
    parameters["core_list"] = coreList
    parameters["soc_info"] = soc
    
    # Core and target parameters to be set by "connect" function
    if not parameters.has_key("convertor_type"):
        parameters["convertor_type"] = "STMC_Type_C"
    parameters["core_parameters"] = coreParams
    parameters["target_parameters"] = targetParams

   
