#!/bin/sh

NET_CONF_DIR=/etc/sysconfig/network-scripts
HOTPLUG_CONF_DIR=/etc/hotplug.d/net

# Check this is Red Hat as we know it.
if [ -d $NET_CONF_DIR ] 
    then
    if [ ! -d $HOTPLUG_CONF_DIR ]
	then
	mkdir -p $HOTPLUG_CONF_DIR
    fi
    # Install the hotplug USB network configuration utility.
    cp -f hotplug/stmc_usb_config.hotplug $HOTPLUG_CONF_DIR
else
    echo Unable to install Hotplug utility.
fi
