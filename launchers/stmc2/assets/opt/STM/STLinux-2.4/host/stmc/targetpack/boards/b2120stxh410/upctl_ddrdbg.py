###
### uPCTL DDR debug functions
### (C) C. Bakan
###
import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import utilities

global sttp_root
global error_count
sttp_root = sttp.targetpack.get_tree()
fill_memory_value = 0xA5A5A5A5

def print_lock_status(phy):


    sttp.logging.print_out("Lock status")
    STATUS = phy.DDR3PHY_DX0GSR0.peek()
    sttp.logging.print_out("  PHY_DX0GSR0 : 0x%x => DAT0 Lock Status = %d" %(STATUS,((STATUS >> 14) & 0x1)))
    STATUS = phy.DDR3PHY_DX1GSR0.peek()
    sttp.logging.print_out("  PHY_DX1GSR0 : 0x%x => DAT1 Lock Status = %d" %(STATUS,((STATUS >> 14) & 0x1)))
    STATUS = phy.DDR3PHY_DX2GSR0.peek()
    sttp.logging.print_out("  PHY_DX2GSR0 : 0x%x => DAT2 Lock Status = %d" %(STATUS,((STATUS >> 14) & 0x1)))
    STATUS = phy.DDR3PHY_DX3GSR0.peek()
    sttp.logging.print_out("  PHY_DX3GSR0 : 0x%x => DAT3 Lock Status = %d" %(STATUS,((STATUS >> 14) & 0x1)))
    STATUS = phy.DDR3PHY_PGSR0.peek()
    sttp.logging.print_out("  PHY_PGSR0 : 0x%x => AC Lock Status = %d\n" %(STATUS,((STATUS >> 5) & 0x1)))

def print_local_delays(phy,lmiwidth):

    sttp.logging.print_out("")
    if(lmiwidth==8):
        sttp.logging.print_out("                        BYTE0 :")
        sttp.logging.print_out("Read DQS delays:         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 8) & 0xff))
        sttp.logging.print_out("Read DQSn delays:        %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 16) & 0xff))
        sttp.logging.print_out("Write DQ delays:         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek()) & 0xff))
        sttp.logging.print_out("Read DQS gating delays:  %3d"%((phy.DDR3PHY_DX0LCDLR2.peek()) & 0xff))
        sttp.logging.print_out("Write levelling delays:  %3d"%((phy.DDR3PHY_DX0LCDLR0.peek()) & 0xff))
        sttp.logging.print_out("DQ0WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR0.peek()) & 0x3f))
        sttp.logging.print_out("DQ1WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7WBD:                  %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMWBD:                   %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSWBD:                   %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DSOEBD:                  %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQOEBD:                  %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ0RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ1RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7RBD:                  %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMRBD:                   %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSRBD:                   %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DSNRBD:                  %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 18) & 0x3f))
    elif(lmiwidth==16):
        sttp.logging.print_out("                        BYTE0 :     BYTE1 :")
        sttp.logging.print_out("Read DQS delays:         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 8) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek() >> 8) & 0xff))
        sttp.logging.print_out("Read DQSn delays:        %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 16) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek() >> 16) & 0xff))
        sttp.logging.print_out("Write DQ delays:         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek()) & 0xff))
        sttp.logging.print_out("Read DQS gating delays:  %3d         %3d"%((phy.DDR3PHY_DX0LCDLR2.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR2.peek()) & 0xff))
        sttp.logging.print_out("Write levelling delays:  %3d         %3d"%((phy.DDR3PHY_DX0LCDLR0.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR0.peek()) & 0xff))
        sttp.logging.print_out("DQ0WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek()) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek()) & 0x3f))
        sttp.logging.print_out("DQ1WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7WBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMWBD:                   %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSWBD:                   %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DSOEBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQOEBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ0RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ1RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7RBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMRBD:                   %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSRBD:                   %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DSNRBD:                  %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 18) & 0x3f))
    else:
        sttp.logging.print_out("                        BYTE0 :     BYTE1 :     BYTE2 :     BYTE3 :")
        sttp.logging.print_out("Read DQS delays:         %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 8) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek() >> 8) & 0xff,(phy.DDR3PHY_DX2LCDLR1.peek() >> 8) & 0xff,(phy.DDR3PHY_DX3LCDLR1.peek() >> 8) & 0xff))
        sttp.logging.print_out("Read DQSn delays:        %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek() >> 16) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek() >> 16) & 0xff,(phy.DDR3PHY_DX2LCDLR1.peek() >> 16) & 0xff,(phy.DDR3PHY_DX3LCDLR1.peek() >> 16) & 0xff))
        sttp.logging.print_out("Write DQ delays:         %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR1.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR1.peek()) & 0xff,(phy.DDR3PHY_DX2LCDLR1.peek()) & 0xff,(phy.DDR3PHY_DX3LCDLR1.peek()) & 0xff))
        sttp.logging.print_out("Read DQS gating delays:  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR2.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR2.peek()) & 0xff,(phy.DDR3PHY_DX2LCDLR2.peek()) & 0xff,(phy.DDR3PHY_DX3LCDLR2.peek()) & 0xff))
        sttp.logging.print_out("Write levelling delays:  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0LCDLR0.peek()) & 0xff,(phy.DDR3PHY_DX1LCDLR0.peek()) & 0xff,(phy.DDR3PHY_DX2LCDLR0.peek()) & 0xff,(phy.DDR3PHY_DX3LCDLR0.peek()) & 0xff))
        sttp.logging.print_out("DQ0WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek()) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek()) & 0x3f,(phy.DDR3PHY_DX2BDLR0.peek()) & 0x3f,(phy.DDR3PHY_DX3BDLR0.peek()) & 0x3f))
        sttp.logging.print_out("DQ1WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX2BDLR0.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX3BDLR0.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX2BDLR0.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX3BDLR0.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX2BDLR0.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX3BDLR0.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR0.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR0.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX2BDLR0.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX3BDLR0.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX2BDLR1.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX3BDLR1.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX2BDLR1.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX3BDLR1.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7WBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX2BDLR1.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX3BDLR1.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMWBD:                   %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX2BDLR1.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX3BDLR1.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSWBD:                   %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR1.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR1.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX2BDLR1.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX3BDLR1.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DSOEBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX2BDLR2.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX3BDLR2.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQOEBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX2BDLR2.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX3BDLR2.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ0RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX2BDLR3.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX3BDLR3.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ1RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX2BDLR3.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX3BDLR3.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ2RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX2BDLR3.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX3BDLR3.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DQ3RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX2BDLR3.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX3BDLR3.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DQ4RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR3.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX1BDLR3.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX2BDLR3.peek() >> 24) & 0x3f,(phy.DDR3PHY_DX3BDLR3.peek() >> 24) & 0x3f))
        sttp.logging.print_out("DQ5RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX2BDLR4.peek() >> 0) & 0x3f,(phy.DDR3PHY_DX3BDLR4.peek() >> 0) & 0x3f))
        sttp.logging.print_out("DQ6RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX2BDLR4.peek() >> 6) & 0x3f,(phy.DDR3PHY_DX3BDLR4.peek() >> 6) & 0x3f))
        sttp.logging.print_out("DQ7RBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX2BDLR4.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX3BDLR4.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DMRBD:                   %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR4.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR4.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX2BDLR4.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX3BDLR4.peek() >> 18) & 0x3f))
        sttp.logging.print_out("DSRBD:                   %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX2BDLR2.peek() >> 12) & 0x3f,(phy.DDR3PHY_DX3BDLR2.peek() >> 12) & 0x3f))
        sttp.logging.print_out("DSNRBD:                  %3d         %3d         %3d         %3d"%((phy.DDR3PHY_DX0BDLR2.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX1BDLR2.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX2BDLR2.peek() >> 18) & 0x3f,(phy.DDR3PHY_DX3BDLR2.peek() >> 18) & 0x3f))
    sttp.logging.print_out("ACBDLR : %d" %(phy.DDR3PHY_ACBDLR.peek()))

##################################################################################################################################
def SweepParameter(parameter_select, phy):

#parameter_select = 0 => Read DQS
#parameter_select = 1 => Write DQS
#parameter_select = 2 => Read bit delay
#parameter_select = 3 => Write bit delay
#parameter_select = 4 => CLK delay
#parameter_select = 5 => ADDR delay
#parameter_select = 6 => DQS gating delay


    if(parameter_select == 0):
    
        print "READ DQS DELAY SWEEP for LMI"
        
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------"  

        while(count < 256):
    
            phy.DDR3PHY_DX0LCDLR1.and_const(~(0xffff << 8))
            phy.DDR3PHY_DX1LCDLR1.and_const(~(0xffff << 8))
            phy.DDR3PHY_DX2LCDLR1.and_const(~(0xffff << 8))
            phy.DDR3PHY_DX3LCDLR1.and_const(~(0xffff << 8))
        
            phy.DDR3PHY_DX0LCDLR1.or_const((count << 16) | (count << 8))
            phy.DDR3PHY_DX1LCDLR1.or_const((count << 16) | (count << 8))
            phy.DDR3PHY_DX2LCDLR1.or_const((count << 16) | (count << 8))
            phy.DDR3PHY_DX3LCDLR1.or_const((count << 16) | (count << 8))
            
            print "Delay = ",count,

            if(count < 10):
                print " =>",   
            else:
                print "=>",
                
            TestMemoryForSweep()
            count =count + 1
            
    if(parameter_select == 1):

        print "WRITE DQS DELAY SWEEP for LMI"
        
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
        
        while(count < 256):
    
            phy.DDR3PHY_DX0LCDLR1.and_const(~(0xff))
            phy.DDR3PHY_DX1LCDLR1.and_const(~(0xff))
            phy.DDR3PHY_DX2LCDLR1.and_const(~(0xff))
            phy.DDR3PHY_DX3LCDLR1.and_const(~(0xff))
        
            phy.DDR3PHY_DX0LCDLR1.or_const((count))
            phy.DDR3PHY_DX1LCDLR1.or_const((count))
            phy.DDR3PHY_DX2LCDLR1.or_const((count))
            phy.DDR3PHY_DX3LCDLR1.or_const((count))
            
            print "Delay = ",count,

            if(count < 10):
                print " =>",   
            else:
                print "=>",
                
            TestMemoryForSweep()
            count =count + 1
            
    if(parameter_select == 2):

        print "READ BIT DELAY SWEEP for LMI"
        
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
        
        while(count < 64):
    
           phy.DDR3PHY_DX0BDLR3.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX0BDLR4.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX1BDLR3.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX1BDLR4.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX2BDLR3.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX2BDLR4.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX3BDLR3.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX3BDLR4.poke((count << 18) | (count << 12) | (count << 6) | count )
            
           print "Delay = ",count,

           if(count < 10):
               print " =>",   
           else:
               print "=>",
                
           TestMemoryForSweep()
           count =count + 1
            
    if(parameter_select == 3):
 
        print "WRITE BIT DELAY SWEEP for LMI"
         
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
         
        while(count < 64):
     
           phy.DDR3PHY_DX0BDLR0.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX0BDLR1.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX1BDLR0.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX1BDLR1.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX2BDLR0.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX2BDLR1.poke((count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX3BDLR0.poke((count << 24) | (count << 18) | (count << 12) | (count << 6) | count )
           phy.DDR3PHY_DX3BDLR1.poke((count << 18) | (count << 12) | (count << 6) | count )
             
           print "Delay = ",count,
 
           if(count < 10):
               print " =>",   
           else:
               print "=>",
                 
           TestMemoryForSweep()
           count =count + 1

    if(parameter_select == 4):
 
        print "CLOCK DELAY SWEEP for LMI"
         
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
         
        while(count < 64):
     
           phy.DDR3PHY_ACBDLR.and_const(~(0x3ffff))
           
           phy.DDR3PHY_ACBDLR.or_const((count << 12) | (count << 6) | count)
             
           print "Delay = ",count,
 
           if(count < 10):
               print " =>",   
           else:
               print "=>",
                 
           TestMemoryForSweep()
           count =count + 1
            
    if(parameter_select == 5):
 
        print "ADDRESS DELAY SWEEP for LMI"
         
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
         
        while(count < 64):
     
           phy.DDR3PHY_ACBDLR.and_const(~(0x3f << 18))
           
           phy.DDR3PHY_ACBDLR.or_const((count << 18))
             
           print "Delay = ",count,
 
           if(count < 10):
               print " =>",   
           else:
               print "=>",
                 
           TestMemoryForSweep()
           count =count + 1
          
    if(parameter_select == 6):
 
        print "DQS GATING DELAY SWEEP for LMI"
         
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
        
        count = 100
        while(count < 256):
     
           phy.DDR3PHY_DX0LCDLR2.poke(count)
           phy.DDR3PHY_DX1LCDLR2.poke(count)
           phy.DDR3PHY_DX2LCDLR2.poke(count)
           phy.DDR3PHY_DX3LCDLR2.poke(count)
             
           print "Delay = ",count,
 
           if(count < 10):
               print " =>",   
           else:
               print "=>",
                 
           TestMemoryForSweep()
           count =count + 1
            
    if(parameter_select == 7):
 
        print "WRITE LEVELLING DELAY SWEEP for LMI"
         
        print "Bits           31                23                15                7                "    
        print "               -----------------------------------------------------------------------" 
        
        count = 0
        while(count < 256):
     
           phy.DDR3PHY_DX0LCDLR0.poke(count)
           phy.DDR3PHY_DX1LCDLR0.poke(count)
           phy.DDR3PHY_DX2LCDLR0.poke(count)
           phy.DDR3PHY_DX3LCDLR0.poke(count)
             
           print "Delay = ",count,
 
           if(count < 10):
               print " =>",   
           else:
               print "=>",
                 
           TestMemoryForSweep()
           count =count + 4
            
##################################################################################################################################

def TestMemoryForSweep():

#Set base address

    LMI_BASE = 0x40000000

    step_size = 1
    max = 128*step_size
    
#define memory test pattern

    #mem_test_pattern = [0x00000000,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0x00000000,0x55555555,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0x55555555,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xFF00FF00,0xFF00FF00,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x00000000,0x01010101,0x00000000,0x01010101,0x00000000,0x02020202,0x00000000,0x02020202,0x00000000,0x04040404,0x00000000,0x04040404,0x00000000,0x08080808,0x00000000,0x08080808,0x00000000,0x10101010,0x00000000,0x10101010,0x00000000,0x20202020,0x00000000,0x20202020,0x00000000,0x40404040,0x00000000,0x40404040,0x00000000,0x80808080,0x00000000,0x80808080]

    mem_test_pattern = [0x00000000,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0x00000000,0x55555555,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0x55555555,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xFF00FF00,0xFF00FF00,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x00000000,0x01010101,0x00000000,0x01010101,0x00000000,0x02020202,0x00000000,0x02020202,0x00000000,0x04040404,0x00000000,0x04040404,0x00000000,0x08080808,0x00000000,0x08080808,0x00000000,0x10101010,0x00000000,0x10101010,0x00000000,0x20202020,0x00000000,0x20202020,0x00000000,0x40404040,0x00000000,0x40404040,0x00000000,0x80808080,0x00000000,0x80808080,0xA492B491,0x3D27523,0x476EE822,0x5923085A,0x9E44048B,0x5A33013D,0x27ADDF58,0x2BDB6F40,0xEE177D5D,0xC542662F,0xEBF00F2C,0x45FBB321,0xD1727D60,0x52574B39,0xD13D7121,0x555D0528,0x5A9D474D,0x79A9ECD,0x2B89B395,0xB1C8C01C,0xDBF41A02,0x8EE07E1F,0x6B329851,0xA2E21017,0xCE199435,0xA264ACC9,0xF31EE382,0x1C12C303,0x43FEE2AA,0x27B5009C,0x3B0D094A,0x51A1DCCB,0xE2DED8B1,0x248E9ADF,0xD2992D8E,0xA219F729,0x8D1555D5,0xAB37F317,0xC6947178,0x2975E6,0x810CFC9B,0x4321B26D,0xC387923B,0xA46689A4,0xDABD38DA,0x61D85C9D,0x117AD744,0x91A100D5,0xFB729634,0x3F90CC1E,0x2E5D5202,0x61ABEBC7,0x58DB6658,0xB1D97B6D,0x5DA56AD5,0xE3C90E07,0x9992B7D1,0xBE3AA032,0xFD04C080,0x5120220F,0xB2A39D26,0x45CF156E,0xCF4955B0,0x3EEBEA38]

#define bit error pattern

    bit_error_pattern = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

#initialize the memory with 0
    count = 0
    
    while(count < max):
    
        sttp.pokepeek.poke(LMI_BASE + (4*count),0)
        count = count + step_size

#fill the memory with test pattern

    count = 0
    
    while(count < max):
    
        sttp.pokepeek.poke(LMI_BASE + (4*count),mem_test_pattern[count/step_size])
        count = count + step_size
        
#read back the memory

    count  = 0
    count1 = 0
    err_count = 0
    while(count < max):
    
        value = sttp.pokepeek.peek(LMI_BASE + (4*count))
        
        #compare with test pattern        
        result = value ^ mem_test_pattern[count/step_size]
        
        if(value != mem_test_pattern[count/step_size]):
            err_count = err_count + 1
            
        #store the result to bit error array
        while(count1 < 32):
        
            if(((value >> count1) & 0x00000001) != ((mem_test_pattern[count/step_size] >> count1) & 0x00000001)):
                bit_error_pattern[count1] = bit_error_pattern[count1] + 1 
                
            count1 = count1 + 1
        
        count = count + step_size
        count1 = 0
    #sttp.logging.print_out("err_count: %d" %(err_count))
    #print bit error pattern
        
    count1 = 31        
    while(count1 > -1):
    
       if(bit_error_pattern[count1] == 0):
           print "-",
       elif((bit_error_pattern[count1] < 10)):
           print "+",
       elif((bit_error_pattern[count1] < 20)):
           print "x",
       else: 
           print "*",
     
       if(((count1) % 8) == 0):       
           print "|",
       count1 = count1 - 1
       
    print ""
    
    
##################################################################################################################################

def TestMemoryNew(add):
        
    LMI_BASE = (add<<24)
    step_size = 1
    max = 128*step_size
    
#define memory test pattern
    mem_test_pattern = [0x00000000,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0xFFFFFFFF,0x00000000,0xFFFFFFFF,0x00000000,0x55555555,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0xAAAAAAAA,0x55555555,0xAAAAAAAA,0x55555555,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xFF00FF00,0xFF00FF00,0x00FF00FF,0xFF00FF00,0x00FF00FF,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0x0F0F0F0F,0xF0F0F0F0,0x0F0F0F0F,0xF0F0F0F0,0x00000000,0x01010101,0x00000000,0x01010101,0x00000000,0x02020202,0x00000000,0x02020202,0x00000000,0x04040404,0x00000000,0x04040404,0x00000000,0x08080808,0x00000000,0x08080808,0x00000000,0x10101010,0x00000000,0x10101010,0x00000000,0x20202020,0x00000000,0x20202020,0x00000000,0x40404040,0x00000000,0x40404040,0x00000000,0x80808080,0x00000000,0x80808080,0xA492B491,0x3D27523,0x476EE822,0x5923085A,0x9E44048B,0x5A33013D,0x27ADDF58,0x2BDB6F40,0xEE177D5D,0xC542662F,0xEBF00F2C,0x45FBB321,0xD1727D60,0x52574B39,0xD13D7121,0x555D0528,0x5A9D474D,0x79A9ECD,0x2B89B395,0xB1C8C01C,0xDBF41A02,0x8EE07E1F,0x6B329851,0xA2E21017,0xCE199435,0xA264ACC9,0xF31EE382,0x1C12C303,0x43FEE2AA,0x27B5009C,0x3B0D094A,0x51A1DCCB,0xE2DED8B1,0x248E9ADF,0xD2992D8E,0xA219F729,0x8D1555D5,0xAB37F317,0xC6947178,0x2975E6,0x810CFC9B,0x4321B26D,0xC387923B,0xA46689A4,0xDABD38DA,0x61D85C9D,0x117AD744,0x91A100D5,0xFB729634,0x3F90CC1E,0x2E5D5202,0x61ABEBC7,0x58DB6658,0xB1D97B6D,0x5DA56AD5,0xE3C90E07,0x9992B7D1,0xBE3AA032,0xFD04C080,0x5120220F,0xB2A39D26,0x45CF156E,0xCF4955B0,0x3EEBEA38]

#initialize the memory with 0
    sttp.logging.print_out("Filling the memory with 0")
    count = 0    
    while(count < max):
    
        sttp.pokepeek.poke(LMI_BASE + (4*count),0)
        count = count + step_size

#fill the memory with test pattern
    sttp.logging.print_out("Filling the memory with test pattern")
    count = 0    
    while(count < max):
    
        sttp.pokepeek.poke(LMI_BASE + (4*count),mem_test_pattern[count/step_size])
        count = count + step_size
        
#read back the memory
    sttp.logging.print_out("Reading back the memory")
    count  = 0
    while(count < max):
    
        value = sttp.pokepeek.peek(LMI_BASE + (4*count))
        
        #compare with test pattern
        if(value != mem_test_pattern[count/step_size]):
            sttp.logging.print_out("ERROR: Exp=0x%08x @ 0x%08x. Got=0x%08x" % (mem_test_pattern[count/step_size],LMI_BASE + (4*count),value)) 
        count = count + step_size       
    sttp.logging.print_out("Test finished")

##################################################################################################################################

def PrintPHYDXnGSR0reg(phy,bytelane):
    if(bytelane==0):
        regvalue=phy.DDR3PHY_DX0GSR0.peek()
    elif(bytelane==1):
        regvalue=phy.DDR3PHY_DX1GSR0.peek()
    elif(bytelane==2):
        regvalue=phy.DDR3PHY_DX2GSR0.peek()
    else:
        regvalue=phy.DDR3PHY_DX3GSR0.peek()
    sttp.logging.print_out("DATX8 General Status Register 0 for byte %d: 0x%08x"%(bytelane,regvalue))
    if(((regvalue>>0)&1)==0):
        sttp.logging.print_out("Write DQ calibration NOT DONE")
    else:
        sttp.logging.print_out("Write DQ calibration done")
    if(((regvalue>>1)&1)==0):
        sttp.logging.print_out("Read DQS calibration NOT DONE")
    else:
        sttp.logging.print_out("Read DQS calibration done")
    if(((regvalue>>2)&1)==0):
        sttp.logging.print_out("Read DQS# calibration NOT DONE")
    else:
        sttp.logging.print_out("Read DQS# calibration done")
    if(((regvalue>>3)&1)==0):
        sttp.logging.print_out("Read DQS gating calibration NOT DONE")
    else:
        sttp.logging.print_out("Read DQS gating calibration done")
    if(((regvalue>>4)&1)==0):
        sttp.logging.print_out("Write Leveling calibration NOT DONE")
    else:
        sttp.logging.print_out("Write Leveling calibration done")
    if(((regvalue>>5)&1)==0):
        sttp.logging.print_out("Write Leveling NOT DONE")
    else:
        sttp.logging.print_out("Write Leveling done")
    if(((regvalue>>6)&1)==0):
        sttp.logging.print_out("Write Leveling without error")
    else:
        sttp.logging.print_out("Write Leveling ERROR")
    sttp.logging.print_out("Write Leveling Period: %d"%(((regvalue>>7)&0xFF)))
    if(((regvalue>>15)&1)==0):
        sttp.logging.print_out("DATX8 PLL NOT LOCKED")
    else:
        sttp.logging.print_out("DATX8 PLL locked")
    sttp.logging.print_out("Read DQS Gating Period: %d"%(((regvalue>>16)&0xFF)))
    if(((regvalue>>24)&1)==0):
        sttp.logging.print_out("DQS Gate Training on rank 0 with no error")
    else:
        sttp.logging.print_out("DQS Gate Training on rank 0 WITH ERROR")
    if(((regvalue>>25)&1)==0):
        sttp.logging.print_out("DQS Gate Training on rank 1 with no error")
    else:
        sttp.logging.print_out("DQS Gate Training on rank 1 WITH ERROR")
    if(((regvalue>>26)&1)==0):
        sttp.logging.print_out("DQS Gate Training on rank 2 with no error")
    else:
        sttp.logging.print_out("DQS Gate Training on rank 2 WITH ERROR")
    if(((regvalue>>27)&1)==0):
        sttp.logging.print_out("DQS Gate Training on rank 3 with no error")
    else:
        sttp.logging.print_out("DQS Gate Training on rank 3 WITH ERROR")

def checkPGSR(phy,pir,lmi_width):
    skip = 0
    run = 1
    
    init     = (pir&(1<<0))>>0
    zcal     = (pir&(1<<1))>>1
    pllinit  = (pir&(1<<4))>>4
    dcal     = (pir&(1<<5))>>5
    phyrst   = (pir&(1<<6))>>6
    dramrst  = (pir&(1<<7))>>7
    draminit = (pir&(1<<8))>>8
    wl       = (pir&(1<<9))>>9
    qsgate   = (pir&(1<<10))>>10
    wladj    = (pir&(1<<11))>>11
    rddskw   = (pir&(1<<12))>>12
    wrdskw   = (pir&(1<<13))>>13
    rdeye    = (pir&(1<<14))>>14
    wreye    = (pir&(1<<15))>>15
    
    if(init==run):
        if(pllinit==run):
            sttp.logging.print_out("--------- Wait for PLL lock")
            phy.DDR3PHY_PGSR0.while_and_ne(0x2,0x2)
            dplock0=(phy.DDR3PHY_DX0GSR0.peek()&(1<<15))>>15
            if(lmi_width>8):
                dplock1=(phy.DDR3PHY_DX1GSR0.peek()&(1<<15))>>15
            else:
                dplock1=1
            if(lmi_width>16):
                dplock2=(phy.DDR3PHY_DX2GSR0.peek()&(1<<15))>>15
                dplock3=(phy.DDR3PHY_DX3GSR0.peek()&(1<<15))>>15
            else:
                dplock2=1
                dplock3=1
            aplock=(phy.DDR3PHY_PGSR0.peek()&(1<<31))>>31
            pldone=(phy.DDR3PHY_PGSR0.peek()&(1<<28))>>28
            if((dplock0*dplock1*dplock2*dplock3*aplock*pldone)==1):
                sttp.logging.print_out("----------- PLL lock done")
            else:
                sttp.logging.print_out("----------- PLL lock done WITH ERROR:")
                if(pldone==0):
                    sttp.logging.print_out("---------------- ERROR PLL on channel 0 unlocked")
                if(aplock==0):
                    sttp.logging.print_out("---------------- ERROR AC PLL unlocked")
                if(dplock3==0):
                    sttp.logging.print_out("---------------- ERROR DX3 PLL unlocked")
                if(dplock2==0):
                    sttp.logging.print_out("---------------- ERROR DX2 PLL unlocked")
                if(dplock1==0):
                    sttp.logging.print_out("---------------- ERROR DX1 PLL unlocked")
                if(dplock0==0):
                    sttp.logging.print_out("---------------- ERROR DX0 PLL unlocked")
        if(dcal==run):
            sttp.logging.print_out("--------- Wait for Digital Delay Line Calibration")
            phy.DDR3PHY_PGSR0.while_and_ne(0x4,0x4)
            sttp.logging.print_out("----------- DDL Calibration done")
        if(zcal==run):
            sttp.logging.print_out("--------- Wait for Impedance Calibration")
            phy.DDR3PHY_PGSR0.while_and_ne(0x8,0x8)
            zerr=(phy.DDR3PHY_ZQ0SR0.peek()&(1<<30))>>30
            zdone=(phy.DDR3PHY_ZQ0SR0.peek()&(1<<31))>>31
            zpd=(phy.DDR3PHY_ZQ0SR1.peek()&(3<<0))>>0
            zpu=(phy.DDR3PHY_ZQ0SR1.peek()&(3<<2))>>2
            opd=(phy.DDR3PHY_ZQ0SR1.peek()&(3<<4))>>4
            opu=(phy.DDR3PHY_ZQ0SR1.peek()&(3<<6))>>6
            zcerr=(phy.DDR3PHY_PGSR0.peek()&(1<<20))>>20
            if( (zerr+zpd+zpu+opd+opu+zcerr==0) and (zdone==1) ):
                sttp.logging.print_out("----------- Impedance Calibration done")
            else:
                sttp.logging.print_out("----------- Impedance Calibration done WITH ERROR:")
                if(zpd==1):
                    sttp.logging.print_out("---------------- Overflow error on Output impedance pull-down calibration")
                if(zpd==2):
                    sttp.logging.print_out("---------------- Underflow error on Output impedance pull-down calibration")
                if(zpd==3):
                    sttp.logging.print_out("---------------- Callibration in progress for Output impedance pull-down")
                if(zpu==1):
                    sttp.logging.print_out("---------------- Overflow error on Output impedance pull-up calibration")
                if(zpu==2):
                    sttp.logging.print_out("---------------- Underflow error on Output impedance pull-up calibration")
                if(zpu==3):
                    sttp.logging.print_out("---------------- Callibration in progress for Output impedance pull-up")
                if(opd==1):
                    sttp.logging.print_out("---------------- Overflow error on ODT impedance pull-down calibration")
                if(opd==2):
                    sttp.logging.print_out("---------------- Underflow error on ODT impedance pull-down calibration")
                if(opd==3):
                    sttp.logging.print_out("---------------- Callibration in progress for ODT impedance pull-down")
                if(opu==1):
                    sttp.logging.print_out("---------------- Overflow error on ODT impedance pull-up calibration")
                if(opu==2):
                    sttp.logging.print_out("---------------- Underflow error on ODT impedance pull-up calibration")
                if(opu==3):
                    sttp.logging.print_out("---------------- Callibration in progress for ODT impedance pull-up")

        if(draminit==run):
            sttp.logging.print_out("--------- Wait for DRAM Initialization")
            phy.DDR3PHY_PGSR0.while_and_ne(0x10,0x10)
            sttp.logging.print_out("----------- DRAM Initialization done")
        if(wl==run):
            sttp.logging.print_out("--------- Wait for Write Leveling")
            phy.DDR3PHY_PGSR0.while_and_ne(0x20,0x20)
            wlerr=(phy.DDR3PHY_PGSR0.peek()&(1<<21))>>21
            d0wldone=(phy.DDR3PHY_DX0GSR0.peek()&(1<<5))>>5
            d0wlerr=(phy.DDR3PHY_DX0GSR0.peek()&(1<<6))>>6
            if(lmi_width>8):
                d1wldone=(phy.DDR3PHY_DX1GSR0.peek()&(1<<5))>>5
                d1wlerr=(phy.DDR3PHY_DX1GSR0.peek()&(1<<6))>>6
            else:
                d1wldone=1
                d1wlerr=0
            if(lmi_width>16):
                d2wldone=(phy.DDR3PHY_DX2GSR0.peek()&(1<<5))>>5
                d2wlerr=(phy.DDR3PHY_DX2GSR0.peek()&(1<<6))>>6
                d3wldone=(phy.DDR3PHY_DX3GSR0.peek()&(1<<5))>>5
                d3wlerr=(phy.DDR3PHY_DX3GSR0.peek()&(1<<6))>>6
            else:
                d2wldone=1
                d2wlerr=0
                d3wldone=1
                d3wlerr=0
            if((d0wldone*d1wldone*d2wldone*d3wldone)==1 and (wlerr+d0wlerr+d1wlerr+d2wlerr+d3wlerr)==0):
                sttp.logging.print_out("----------- Write Leveling done")
            else:
                sttp.logging.print_out("----------- Write Leveling done WITH ERROR")
                if(d0wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX0 not done")
                if(d1wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX1 not done")
                if(d2wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX2 not done")
                if(d3wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX3 not done")
                if(d0wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX0 with error")
                if(d1wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX1 with error")
                if(d2wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX2 with error")
                if(d3wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX3 with error")
        if(qsgate==run):
            sttp.logging.print_out("--------- Wait for DQS Gate Training")
            phy.DDR3PHY_PGSR0.while_and_ne(0x40,0x40)
            qsgerr=(phy.DDR3PHY_PGSR0.peek()&(1<<22))>>22
            d0qsgerr=(phy.DDR3PHY_DX0GSR0.peek()&(1<<24))>>24
            if(lmi_width>8):
                d1qsgerr=(phy.DDR3PHY_DX1GSR0.peek()&(1<<24))>>24
            else:
                d1qsgerr=0
            if(lmi_width>16):
                d2qsgerr=(phy.DDR3PHY_DX2GSR0.peek()&(1<<24))>>24
                d3qsgerr=(phy.DDR3PHY_DX3GSR0.peek()&(1<<24))>>24
            else:
                d2qsgerr=0
                d3qsgerr=0
            if((qsgerr+d0qsgerr+d1qsgerr+d2qsgerr+d3qsgerr)==0):
                sttp.logging.print_out("----------- DQS Gate Training done")
            else:
                sttp.logging.print_out("----------- DQS Gate Training done WITH ERROR")
                if(d0qsgerr==1):
                    sttp.logging.print_out("---------------- DQS Gate Training on DX0 with error")
                if(d1qsgerr==1):
                    sttp.logging.print_out("---------------- DQS Gate Training on DX1 with error")
                if(d2qsgerr==1):
                    sttp.logging.print_out("---------------- DQS Gate Training on DX2 with error")
                if(d3qsgerr==1):
                    sttp.logging.print_out("---------------- DQS Gate Training on DX3 with error")
        if(wladj==run):
            sttp.logging.print_out("--------- Wait for Write Leveling Adjustment")
            phy.DDR3PHY_PGSR0.while_and_ne(0x80,0x80)
            wlaerr=(phy.DDR3PHY_PGSR0.peek()&(1<<23))>>23
            d0wldone=(phy.DDR3PHY_DX0GSR0.peek()&(1<<5))>>5
            d0wlerr=(phy.DDR3PHY_DX0GSR0.peek()&(1<<6))>>6
            if(lmi_width>8):
                d1wldone=(phy.DDR3PHY_DX1GSR0.peek()&(1<<5))>>5
                d1wlerr=(phy.DDR3PHY_DX1GSR0.peek()&(1<<6))>>6
            else:
                d1wldone=1
                d1wlerr=0
            if(lmi_width>16):
                d2wldone=(phy.DDR3PHY_DX2GSR0.peek()&(1<<5))>>5
                d2wlerr=(phy.DDR3PHY_DX2GSR0.peek()&(1<<6))>>6
                d3wldone=(phy.DDR3PHY_DX3GSR0.peek()&(1<<5))>>5
                d3wlerr=(phy.DDR3PHY_DX3GSR0.peek()&(1<<6))>>6
            else:
                d2wldone=1
                d2wlerr=0
                d3wldone=1
                d3wlerr=0
            if((d0wldone*d1wldone*d2wldone*d3wldone)==1 and (wlaerr+d0wlerr+d1wlerr+d2wlerr+d3wlerr)==0):
                sttp.logging.print_out("----------- Write Leveling Adjustment done")
            else:
                sttp.logging.print_out("----------- Write Leveling Adjustment done WITH ERROR")
                if(d0wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX0 not done")
                if(d1wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX1 not done")
                if(d2wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX2 not done")
                if(d3wldone==0):
                    sttp.logging.print_out("---------------- Write Leveling on DX3 not done")
                if(d0wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX0 with error")
                if(d1wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX1 with error")
                if(d2wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX2 with error")
                if(d3wlerr==1):
                    sttp.logging.print_out("---------------- Write Leveling on DX3 with error")
        if(rddskw==run):
            sttp.logging.print_out("--------- Wait for Read Bit Deskew")
            phy.DDR3PHY_PGSR0.while_and_ne(0x100,0x100)
            rderr=(phy.DDR3PHY_PGSR0.peek()&(1<<24))>>24
            d0rderr=(phy.DDR3PHY_DX0GSR2.peek()&(1<<0))>>0
            d0rdwn=(phy.DDR3PHY_DX0GSR2.peek()&(1<<1))>>1
            if(lmi_width>8):
                d1rderr=(phy.DDR3PHY_DX1GSR2.peek()&(1<<0))>>0
                d1rdwn=(phy.DDR3PHY_DX1GSR2.peek()&(1<<1))>>1
            else:
                d1rderr=0
                d1rdwn=0
            if(lmi_width>16):
                d2rderr=(phy.DDR3PHY_DX2GSR2.peek()&(1<<0))>>0
                d2rdwn=(phy.DDR3PHY_DX2GSR2.peek()&(1<<1))>>1
                d3rderr=(phy.DDR3PHY_DX3GSR2.peek()&(1<<0))>>0
                d3rdwn=(phy.DDR3PHY_DX3GSR2.peek()&(1<<1))>>1
            else:
                d2rderr=0
                d2rdwn=0
                d3rderr=0
                d3rdwn=0
            if(rderr+d0rderr+d0rdwn+d1rderr+d1rdwn+d2rderr+d2rdwn+d3rderr+d3rdwn==0):
                sttp.logging.print_out("----------- Read Bit Deskew done")
            else:
                sttp.logging.print_out("----------- Read Bit Deskew done WITH ERROR or WARNING")
                if(d0rderr==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew ERROR on DX0, DX0GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX0GSR2.peek()&(0xf<<8))>>8))
                if(d1rderr==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew ERROR on DX1, DX1GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX1GSR2.peek()&(0xf<<8))>>8))
                if(d2rderr==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew ERROR on DX2, DX2GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX2GSR2.peek()&(0xf<<8))>>8))
                if(d3rderr==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew ERROR on DX3, DX3GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX3GSR2.peek()&(0xf<<8))>>8))
                if(d0rdwn==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew WARNING on DX0")
                if(d1rdwn==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew WARNING on DX1")
                if(d2rdwn==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew WARNING on DX2")
                if(d3rdwn==1):
                    sttp.logging.print_out("---------------- Read Bit Deskew WARNING on DX3")
        if(wrdskw==run):
            sttp.logging.print_out("--------- Wait for Write Bit Deskew")
            phy.DDR3PHY_PGSR0.while_and_ne(0x200,0x200)
            wderr=(phy.DDR3PHY_PGSR0.peek()&(1<<25))>>25
            d0wderr=(phy.DDR3PHY_DX0GSR2.peek()&(1<<2))>>2
            d0wdwn=(phy.DDR3PHY_DX0GSR2.peek()&(1<<3))>>3
            if(lmi_width>8):
                d1wderr=(phy.DDR3PHY_DX1GSR2.peek()&(1<<2))>>2
                d1wdwn=(phy.DDR3PHY_DX1GSR2.peek()&(1<<3))>>3
            else:
                d1wderr=0
                d1wdwn=0
            if(lmi_width>16):
                d2wderr=(phy.DDR3PHY_DX2GSR2.peek()&(1<<2))>>2
                d2wdwn=(phy.DDR3PHY_DX2GSR2.peek()&(1<<3))>>3
                d3wderr=(phy.DDR3PHY_DX3GSR2.peek()&(1<<2))>>2
                d3wdwn=(phy.DDR3PHY_DX3GSR2.peek()&(1<<3))>>3
            else:
                d2wderr=0
                d2wdwn=0
                d3wderr=0
                d3wdwn=0
            if(wderr+d0wderr+d0wdwn+d1wderr+d1wdwn+d2wderr+d2wdwn+d3wderr+d3wdwn==0):
                sttp.logging.print_out("----------- Write Bit Deskew done")
            else:
                sttp.logging.print_out("----------- Write Bit Deskew done WITH ERROR or WARNING")
                if(d0wderr==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew ERROR on DX0, DX0GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX0GSR2.peek()&(0xf<<8))>>8))
                if(d1wderr==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew ERROR on DX1, DX1GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX1GSR2.peek()&(0xf<<8))>>8))
                if(d2wderr==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew ERROR on DX2, DX2GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX2GSR2.peek()&(0xf<<8))>>8))
                if(d3wderr==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew ERROR on DX3, DX3GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX3GSR2.peek()&(0xf<<8))>>8))
                if(d0wdwn==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew WARNING on DX0")
                if(d1wdwn==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew WARNING on DX1")
                if(d2wdwn==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew WARNING on DX2")
                if(d3wdwn==1):
                    sttp.logging.print_out("---------------- Write Bit Deskew WARNING on DX3")
        if(rdeye==run):
            sttp.logging.print_out("--------- Wait for Read Eye Training")
            phy.DDR3PHY_PGSR0.while_and_ne(0x400,0x400)
            reerr=(phy.DDR3PHY_PGSR0.peek()&(1<<26))>>26
            d0reerr=(phy.DDR3PHY_DX0GSR2.peek()&(1<<4))>>4
            d0rewn=(phy.DDR3PHY_DX0GSR2.peek()&(1<<5))>>5
            if(lmi_width>8):
                d1reerr=(phy.DDR3PHY_DX1GSR2.peek()&(1<<4))>>4
                d1rewn=(phy.DDR3PHY_DX1GSR2.peek()&(1<<5))>>5
            else:
                d1reerr=0
                d1rewn=0
            if(lmi_width>16):
                d2reerr=(phy.DDR3PHY_DX2GSR2.peek()&(1<<4))>>4
                d2rewn=(phy.DDR3PHY_DX2GSR2.peek()&(1<<5))>>5
                d3reerr=(phy.DDR3PHY_DX3GSR2.peek()&(1<<4))>>4
                d3rewn=(phy.DDR3PHY_DX3GSR2.peek()&(1<<5))>>5
            else:
                d2reerr=0
                d2rewn=0
                d3reerr=0
                d3rewn=0
            if(reerr+d0reerr+d0rewn+d1reerr+d1rewn+d2reerr+d2rewn+d3reerr+d3rewn==0):
                sttp.logging.print_out("----------- Read Eye Training done")
            else:
                sttp.logging.print_out("----------- Read Eye Training done WITH ERROR or WARNING")
                if(d0reerr==1):
                    sttp.logging.print_out("---------------- Read Eye Training ERROR on DX0, DX0GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX0GSR2.peek()&(0xf<<8))>>8))
                if(d1reerr==1):
                    sttp.logging.print_out("---------------- Read Eye Training ERROR on DX1, DX1GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX1GSR2.peek()&(0xf<<8))>>8))
                if(d2reerr==1):
                    sttp.logging.print_out("---------------- Read Eye Training ERROR on DX2, DX2GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX2GSR2.peek()&(0xf<<8))>>8))
                if(d3reerr==1):
                    sttp.logging.print_out("---------------- Read Eye Training ERROR on DX3, DX3GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX3GSR2.peek()&(0xf<<8))>>8))
                if(d0rewn==1):
                    sttp.logging.print_out("---------------- Read Eye Training WARNING on DX0")
                if(d1rewn==1):
                    sttp.logging.print_out("---------------- Read Eye Training WARNING on DX1")
                if(d2rewn==1):
                    sttp.logging.print_out("---------------- Read Eye Training WARNING on DX2")
                if(d3rewn==1):
                    sttp.logging.print_out("---------------- Read Eye Training WARNING on DX3")
        if(wreye==run):
            sttp.logging.print_out("--------- Wait for Write Eye Training")
            phy.DDR3PHY_PGSR0.while_and_ne(0x800,0x800)
            weerr=(phy.DDR3PHY_PGSR0.peek()&(1<<27))>>27
            d0weerr=(phy.DDR3PHY_DX0GSR2.peek()&(1<<6))>>6
            d0wewn=(phy.DDR3PHY_DX0GSR2.peek()&(1<<7))>>7
            if(lmi_width>8):
                d1weerr=(phy.DDR3PHY_DX1GSR2.peek()&(1<<6))>>6
                d1wewn=(phy.DDR3PHY_DX1GSR2.peek()&(1<<7))>>7
            else:
                d1weerr=0
                d1wewn=0
            if(lmi_width>16):
                d2weerr=(phy.DDR3PHY_DX2GSR2.peek()&(1<<6))>>6
                d2wewn=(phy.DDR3PHY_DX2GSR2.peek()&(1<<7))>>7
                d3weerr=(phy.DDR3PHY_DX3GSR2.peek()&(1<<6))>>6
                d3wewn=(phy.DDR3PHY_DX3GSR2.peek()&(1<<7))>>7
            else:
                d2weerr=0
                d2wewn=0
                d3weerr=0
                d3wewn=0
            if(weerr+d0weerr+d0wewn+d1weerr+d1wewn+d2weerr+d2wewn+d3weerr+d3wewn==0):
                sttp.logging.print_out("----------- Write Eye Training done")
            else:
                sttp.logging.print_out("----------- Write Eye Training done WITH ERROR or WARNING")
                if(d0weerr==1):
                    sttp.logging.print_out("---------------- Write Eye Training ERROR on DX0, DX0GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX0GSR2.peek()&(0xf<<8))>>8))
                if(d1weerr==1):
                    sttp.logging.print_out("---------------- Write Eye Training ERROR on DX1, DX1GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX1GSR2.peek()&(0xf<<8))>>8))
                if(d2weerr==1):
                    sttp.logging.print_out("---------------- Write Eye Training ERROR on DX2, DX2GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX2GSR2.peek()&(0xf<<8))>>8))
                if(d3weerr==1):
                    sttp.logging.print_out("---------------- Write Eye Training ERROR on DX3, DX3GSR2.ESTAT=0x%x"%((phy.DDR3PHY_DX3GSR2.peek()&(0xf<<8))>>8))
                if(d0wewn==1):
                    sttp.logging.print_out("---------------- Write Eye Training WARNING on DX0")
                if(d1wewn==1):
                    sttp.logging.print_out("---------------- Write Eye Training WARNING on DX1")
                if(d2wewn==1):
                    sttp.logging.print_out("---------------- Write Eye Training WARNING on DX2")
                if(d3wewn==1):
                    sttp.logging.print_out("---------------- Write Eye Training WARNING on DX3")
                
        #sttp.logging.print_out("PGSR0: 0x%08x" %(phy.DDR3PHY_PGSR0.peek()))
        #sttp.logging.print_out("PGSR1: 0x%08x" %(phy.DDR3PHY_PGSR1.peek()))
        #sttp.logging.print_out("DX0GSR0: 0x%08x" %(phy.DDR3PHY_DX0GSR0.peek()))
        #sttp.logging.print_out("DX0GSR1: 0x%08x" %(phy.DDR3PHY_DX0GSR1.peek()))
        #sttp.logging.print_out("DX0GSR2: 0x%08x" %(phy.DDR3PHY_DX0GSR2.peek()))
        
    
        
        
def PrintPHYRegisters(phy,lmi_width):

    sttp.logging.print_out("DDR PHY registers:")
    sttp.logging.print_out("---------------------------")
    
    sttp.logging.print_out("DDR3PHY_RIDR                    : 0x%08x" %(phy.DDR3PHY_RIDR.peek()))
    sttp.logging.print_out("DDR3PHY_PIR                     : 0x%08x" %(phy.DDR3PHY_PIR.peek()))
    sttp.logging.print_out("DDR3PHY_PGCR0                   : 0x%08x" %(phy.DDR3PHY_PGCR0.peek()))
    sttp.logging.print_out("DDR3PHY_PGCR1                   : 0x%08x" %(phy.DDR3PHY_PGCR1.peek()))
    sttp.logging.print_out("DDR3PHY_PGSR0                   : 0x%08x" %(phy.DDR3PHY_PGSR0.peek()))
    sttp.logging.print_out("DDR3PHY_PGSR1                   : 0x%08x" %(phy.DDR3PHY_PGSR1.peek()))
    sttp.logging.print_out("DDR3PHY_PLLCR                   : 0x%08x" %(phy.DDR3PHY_PLLCR.peek()))
    sttp.logging.print_out("DDR3PHY_PTR0                    : 0x%08x" %(phy.DDR3PHY_PTR0.peek()))
    sttp.logging.print_out("DDR3PHY_PTR1                    : 0x%08x" %(phy.DDR3PHY_PTR1.peek()))
    sttp.logging.print_out("DDR3PHY_PTR2                    : 0x%08x" %(phy.DDR3PHY_PTR2.peek()))
    sttp.logging.print_out("DDR3PHY_PTR3                    : 0x%08x" %(phy.DDR3PHY_PTR3.peek()))
    sttp.logging.print_out("DDR3PHY_PTR4                    : 0x%08x" %(phy.DDR3PHY_PTR4.peek()))
    sttp.logging.print_out("DDR3PHY_ACMDLR                  : 0x%08x" %(phy.DDR3PHY_ACMDLR.peek()))
    sttp.logging.print_out("DDR3PHY_ACBDLR                  : 0x%08x" %(phy.DDR3PHY_ACBDLR.peek()))
    sttp.logging.print_out("DDR3PHY_ACIOCR                  : 0x%08x" %(phy.DDR3PHY_ACIOCR.peek()))
    sttp.logging.print_out("DDR3PHY_DXCCR                   : 0x%08x" %(phy.DDR3PHY_DXCCR.peek()))
    sttp.logging.print_out("DDR3PHY_DSGCR                   : 0x%08x" %(phy.DDR3PHY_DSGCR.peek()))
    sttp.logging.print_out("DDR3PHY_DCR                     : 0x%08x" %(phy.DDR3PHY_DCR.peek()))
    sttp.logging.print_out("DDR3PHY_DTPR0                   : 0x%08x" %(phy.DDR3PHY_DTPR0.peek()))
    sttp.logging.print_out("DDR3PHY_DTPR1                   : 0x%08x" %(phy.DDR3PHY_DTPR1.peek()))
    sttp.logging.print_out("DDR3PHY_DTPR2                   : 0x%08x" %(phy.DDR3PHY_DTPR2.peek()))
    sttp.logging.print_out("DDR3PHY_MR0                     : 0x%08x" %(phy.DDR3PHY_MR0.peek()))
    sttp.logging.print_out("DDR3PHY_MR1                     : 0x%08x" %(phy.DDR3PHY_MR1.peek()))
    sttp.logging.print_out("DDR3PHY_MR2                     : 0x%08x" %(phy.DDR3PHY_MR2.peek()))
    sttp.logging.print_out("DDR3PHY_MR3                     : 0x%08x" %(phy.DDR3PHY_MR3.peek()))
    sttp.logging.print_out("DDR3PHY_ODTCR                   : 0x%08x" %(phy.DDR3PHY_ODTCR.peek()))
    sttp.logging.print_out("DDR3PHY_DTCR                    : 0x%08x" %(phy.DDR3PHY_DTCR.peek()))
    sttp.logging.print_out("DDR3PHY_DTAR0                   : 0x%08x" %(phy.DDR3PHY_DTAR0.peek()))
    sttp.logging.print_out("DDR3PHY_DTAR1                   : 0x%08x" %(phy.DDR3PHY_DTAR1.peek()))
    sttp.logging.print_out("DDR3PHY_DTAR2                   : 0x%08x" %(phy.DDR3PHY_DTAR2.peek()))
    sttp.logging.print_out("DDR3PHY_DTAR3                   : 0x%08x" %(phy.DDR3PHY_DTAR3.peek()))
    sttp.logging.print_out("DDR3PHY_DTDR0                   : 0x%08x" %(phy.DDR3PHY_DTDR0.peek()))
    sttp.logging.print_out("DDR3PHY_DTDR1                   : 0x%08x" %(phy.DDR3PHY_DTDR1.peek()))
    sttp.logging.print_out("DDR3PHY_DTEDR0                  : 0x%08x" %(phy.DDR3PHY_DTEDR0.peek()))
    sttp.logging.print_out("DDR3PHY_DTEDR1                  : 0x%08x" %(phy.DDR3PHY_DTEDR1.peek()))
    sttp.logging.print_out("DDR3PHY_PGCR2                   : 0x%08x" %(phy.DDR3PHY_PGCR2.peek()))
    sttp.logging.print_out("DDR3PHY_RDIMMGCR0               : 0x%08x" %(phy.DDR3PHY_RDIMMGCR0.peek()))
    sttp.logging.print_out("DDR3PHY_RDIMMGCR1               : 0x%08x" %(phy.DDR3PHY_RDIMMGCR1.peek()))
    sttp.logging.print_out("DDR3PHY_RDIMMCR0                : 0x%08x" %(phy.DDR3PHY_RDIMMCR0.peek()))
    sttp.logging.print_out("DDR3PHY_RDIMMCR1                : 0x%08x" %(phy.DDR3PHY_RDIMMCR1.peek()))
    sttp.logging.print_out("DDR3PHY_DCUAR                   : 0x%08x" %(phy.DDR3PHY_DCUAR.peek()))
    sttp.logging.print_out("DDR3PHY_DCUDR                   : 0x%08x" %(phy.DDR3PHY_DCUDR.peek()))
    sttp.logging.print_out("DDR3PHY_DCURR                   : 0x%08x" %(phy.DDR3PHY_DCURR.peek()))
    sttp.logging.print_out("DDR3PHY_DCULR                   : 0x%08x" %(phy.DDR3PHY_DCULR.peek()))
    sttp.logging.print_out("DDR3PHY_DCUGCR                  : 0x%08x" %(phy.DDR3PHY_DCUGCR.peek()))
    sttp.logging.print_out("DDR3PHY_DCUTPR                  : 0x%08x" %(phy.DDR3PHY_DCUTPR.peek()))
    sttp.logging.print_out("DDR3PHY_DCUSR0                  : 0x%08x" %(phy.DDR3PHY_DCUSR0.peek()))
    sttp.logging.print_out("DDR3PHY_DCUSR1                  : 0x%08x" %(phy.DDR3PHY_DCUSR1.peek()))
    sttp.logging.print_out("DDR3PHY_BISTRR                  : 0x%08x" %(phy.DDR3PHY_BISTRR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTWCR                 : 0x%08x" %(phy.DDR3PHY_BISTWCR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTMSKR0               : 0x%08x" %(phy.DDR3PHY_BISTMSKR0.peek()))
    sttp.logging.print_out("DDR3PHY_BISTMSKR1               : 0x%08x" %(phy.DDR3PHY_BISTMSKR1.peek()))
    sttp.logging.print_out("DDR3PHY_BISTMSKR2               : 0x%08x" %(phy.DDR3PHY_BISTMSKR2.peek()))
    sttp.logging.print_out("DDR3PHY_BISTLSR                 : 0x%08x" %(phy.DDR3PHY_BISTLSR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTAR0                 : 0x%08x" %(phy.DDR3PHY_BISTAR0.peek()))
    sttp.logging.print_out("DDR3PHY_BISTAR1                 : 0x%08x" %(phy.DDR3PHY_BISTAR1.peek()))
    sttp.logging.print_out("DDR3PHY_BISTAR2                 : 0x%08x" %(phy.DDR3PHY_BISTAR2.peek()))
    sttp.logging.print_out("DDR3PHY_BISTUDPR                : 0x%08x" %(phy.DDR3PHY_BISTUDPR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTGSR                 : 0x%08x" %(phy.DDR3PHY_BISTGSR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTWER                 : 0x%08x" %(phy.DDR3PHY_BISTWER.peek()))
    sttp.logging.print_out("DDR3PHY_BISTBER0                : 0x%08x" %(phy.DDR3PHY_BISTBER0.peek()))
    sttp.logging.print_out("DDR3PHY_BISTBER1                : 0x%08x" %(phy.DDR3PHY_BISTBER1.peek()))
    sttp.logging.print_out("DDR3PHY_BISTBER2                : 0x%08x" %(phy.DDR3PHY_BISTBER2.peek()))
    sttp.logging.print_out("DDR3PHY_BISTBER3                : 0x%08x" %(phy.DDR3PHY_BISTBER3.peek()))
    sttp.logging.print_out("DDR3PHY_BISTWCSR                : 0x%08x" %(phy.DDR3PHY_BISTWCSR.peek()))
    sttp.logging.print_out("DDR3PHY_BISTFWR0                : 0x%08x" %(phy.DDR3PHY_BISTFWR0.peek()))
    sttp.logging.print_out("DDR3PHY_BISTFWR1                : 0x%08x" %(phy.DDR3PHY_BISTFWR1.peek()))
    sttp.logging.print_out("DDR3PHY_BISTFWR2                : 0x%08x" %(phy.DDR3PHY_BISTFWR2.peek()))
    sttp.logging.print_out("DDR3PHY_GPR0                    : 0x%08x" %(phy.DDR3PHY_GPR0.peek()))
    sttp.logging.print_out("DDR3PHY_GPR1                    : 0x%08x" %(phy.DDR3PHY_GPR1.peek()))
    sttp.logging.print_out("DDR3PHY_ZQ0CR0                  : 0x%08x" %(phy.DDR3PHY_ZQ0CR0.peek()))
    sttp.logging.print_out("DDR3PHY_ZQ0CR1                  : 0x%08x" %(phy.DDR3PHY_ZQ0CR1.peek()))
    sttp.logging.print_out("DDR3PHY_ZQ0SR0                  : 0x%08x" %(phy.DDR3PHY_ZQ0SR0.peek()))
    sttp.logging.print_out("DDR3PHY_ZQ0SR1                  : 0x%08x" %(phy.DDR3PHY_ZQ0SR1.peek()))
    sttp.logging.print_out("DDR3PHY_DX0GCR                  : 0x%08x" %(phy.DDR3PHY_DX0GCR.peek()))
    sttp.logging.print_out("DDR3PHY_DX0GSR0                 : 0x%08x" %(phy.DDR3PHY_DX0GSR0.peek()))
    sttp.logging.print_out("DDR3PHY_DX0GSR1                 : 0x%08x" %(phy.DDR3PHY_DX0GSR1.peek()))
    sttp.logging.print_out("DDR3PHY_DX0BDLR0                : 0x%08x" %(phy.DDR3PHY_DX0BDLR0.peek()))
    sttp.logging.print_out("DDR3PHY_DX0BDLR1                : 0x%08x" %(phy.DDR3PHY_DX0BDLR1.peek()))
    sttp.logging.print_out("DDR3PHY_DX0BDLR2                : 0x%08x" %(phy.DDR3PHY_DX0BDLR2.peek()))
    sttp.logging.print_out("DDR3PHY_DX0BDLR3                : 0x%08x" %(phy.DDR3PHY_DX0BDLR3.peek()))
    sttp.logging.print_out("DDR3PHY_DX0BDLR4                : 0x%08x" %(phy.DDR3PHY_DX0BDLR4.peek()))
    sttp.logging.print_out("DDR3PHY_DX0LCDLR0               : 0x%08x" %(phy.DDR3PHY_DX0LCDLR0.peek()))
    sttp.logging.print_out("DDR3PHY_DX0LCDLR1               : 0x%08x" %(phy.DDR3PHY_DX0LCDLR1.peek()))
    sttp.logging.print_out("DDR3PHY_DX0LCDLR2               : 0x%08x" %(phy.DDR3PHY_DX0LCDLR2.peek()))
    sttp.logging.print_out("DDR3PHY_DX0MDLR                 : 0x%08x" %(phy.DDR3PHY_DX0MDLR.peek()))
    sttp.logging.print_out("DDR3PHY_DX0GTR                  : 0x%08x" %(phy.DDR3PHY_DX0GTR.peek()))
    sttp.logging.print_out("DDR3PHY_DX0GSR2                 : 0x%08x" %(phy.DDR3PHY_DX0GSR2.peek()))
    if lmi_width>8:
        sttp.logging.print_out("DDR3PHY_DX1GCR                  : 0x%08x" %(phy.DDR3PHY_DX1GCR.peek()))
        sttp.logging.print_out("DDR3PHY_DX1GSR0                 : 0x%08x" %(phy.DDR3PHY_DX1GSR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX1GSR1                 : 0x%08x" %(phy.DDR3PHY_DX1GSR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX1BDLR0                : 0x%08x" %(phy.DDR3PHY_DX1BDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX1BDLR1                : 0x%08x" %(phy.DDR3PHY_DX1BDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX1BDLR2                : 0x%08x" %(phy.DDR3PHY_DX1BDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX1BDLR3                : 0x%08x" %(phy.DDR3PHY_DX1BDLR3.peek()))
        sttp.logging.print_out("DDR3PHY_DX1BDLR4                : 0x%08x" %(phy.DDR3PHY_DX1BDLR4.peek()))
        sttp.logging.print_out("DDR3PHY_DX1LCDLR0               : 0x%08x" %(phy.DDR3PHY_DX1LCDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX1LCDLR1               : 0x%08x" %(phy.DDR3PHY_DX1LCDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX1LCDLR2               : 0x%08x" %(phy.DDR3PHY_DX1LCDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX1MDLR                 : 0x%08x" %(phy.DDR3PHY_DX1MDLR.peek()))
        sttp.logging.print_out("DDR3PHY_DX1GTR                  : 0x%08x" %(phy.DDR3PHY_DX1GTR.peek()))
        sttp.logging.print_out("DDR3PHY_DX1GSR2                 : 0x%08x" %(phy.DDR3PHY_DX1GSR2.peek()))
    if lmi_width>16:
        sttp.logging.print_out("DDR3PHY_DX2GCR                  : 0x%08x" %(phy.DDR3PHY_DX2GCR.peek()))
        sttp.logging.print_out("DDR3PHY_DX2GSR0                 : 0x%08x" %(phy.DDR3PHY_DX2GSR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX2GSR1                 : 0x%08x" %(phy.DDR3PHY_DX2GSR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX2BDLR0                : 0x%08x" %(phy.DDR3PHY_DX2BDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX2BDLR1                : 0x%08x" %(phy.DDR3PHY_DX2BDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX2BDLR2                : 0x%08x" %(phy.DDR3PHY_DX2BDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX2BDLR3                : 0x%08x" %(phy.DDR3PHY_DX2BDLR3.peek()))
        sttp.logging.print_out("DDR3PHY_DX2BDLR4                : 0x%08x" %(phy.DDR3PHY_DX2BDLR4.peek()))
        sttp.logging.print_out("DDR3PHY_DX2LCDLR0               : 0x%08x" %(phy.DDR3PHY_DX2LCDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX2LCDLR1               : 0x%08x" %(phy.DDR3PHY_DX2LCDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX2LCDLR2               : 0x%08x" %(phy.DDR3PHY_DX2LCDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX2MDLR                 : 0x%08x" %(phy.DDR3PHY_DX2MDLR.peek()))
        sttp.logging.print_out("DDR3PHY_DX2GTR                  : 0x%08x" %(phy.DDR3PHY_DX2GTR.peek()))
        sttp.logging.print_out("DDR3PHY_DX2GSR2                 : 0x%08x" %(phy.DDR3PHY_DX2GSR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX3GCR                  : 0x%08x" %(phy.DDR3PHY_DX3GCR.peek()))
        sttp.logging.print_out("DDR3PHY_DX3GSR0                 : 0x%08x" %(phy.DDR3PHY_DX3GSR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX3GSR1                 : 0x%08x" %(phy.DDR3PHY_DX3GSR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX3BDLR0                : 0x%08x" %(phy.DDR3PHY_DX3BDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX3BDLR1                : 0x%08x" %(phy.DDR3PHY_DX3BDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX3BDLR2                : 0x%08x" %(phy.DDR3PHY_DX3BDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX3BDLR3                : 0x%08x" %(phy.DDR3PHY_DX3BDLR3.peek()))
        sttp.logging.print_out("DDR3PHY_DX3BDLR4                : 0x%08x" %(phy.DDR3PHY_DX3BDLR4.peek()))
        sttp.logging.print_out("DDR3PHY_DX3LCDLR0               : 0x%08x" %(phy.DDR3PHY_DX3LCDLR0.peek()))
        sttp.logging.print_out("DDR3PHY_DX3LCDLR1               : 0x%08x" %(phy.DDR3PHY_DX3LCDLR1.peek()))
        sttp.logging.print_out("DDR3PHY_DX3LCDLR2               : 0x%08x" %(phy.DDR3PHY_DX3LCDLR2.peek()))
        sttp.logging.print_out("DDR3PHY_DX3MDLR                 : 0x%08x" %(phy.DDR3PHY_DX3MDLR.peek()))
        sttp.logging.print_out("DDR3PHY_DX3GTR                  : 0x%08x" %(phy.DDR3PHY_DX3GTR.peek()))
        sttp.logging.print_out("DDR3PHY_DX3GSR2                 : 0x%08x" %(phy.DDR3PHY_DX3GSR2.peek()))
    
    
def PrintPCTLRegisters(pctl):

    
    sttp.logging.print_out("DDR PCTL registers:")
    sttp.logging.print_out("---------------------------")
    
    sttp.logging.print_out("DDR3PCTL_SCFG                     : 0x%08x" %(pctl.DDR3PCTL_SCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_SCTL                     : 0x%08x" %(pctl.DDR3PCTL_SCTL.peek()))
    sttp.logging.print_out("DDR3PCTL_STAT                     : 0x%08x" %(pctl.DDR3PCTL_STAT.peek()))
    sttp.logging.print_out("DDR3PCTL_MCMD                     : 0x%08x" %(pctl.DDR3PCTL_MCMD.peek()))
    sttp.logging.print_out("DDR3PCTL_POWCTL                   : 0x%08x" %(pctl.DDR3PCTL_POWCTL.peek()))
    sttp.logging.print_out("DDR3PCTL_POWSTAT                  : 0x%08x" %(pctl.DDR3PCTL_POWSTAT.peek()))
    sttp.logging.print_out("DDR3PCTL_CMDTSTAT                 : 0x%08x" %(pctl.DDR3PCTL_CMDTSTAT.peek()))
    sttp.logging.print_out("DDR3PCTL_CMDTSTATEN               : 0x%08x" %(pctl.DDR3PCTL_CMDTSTATEN.peek()))
    sttp.logging.print_out("DDR3PCTL_MRRCFG0                  : 0x%08x" %(pctl.DDR3PCTL_MRRCFG0.peek()))
    sttp.logging.print_out("DDR3PCTL_MRRSTAT0                 : 0x%08x" %(pctl.DDR3PCTL_MRRSTAT0.peek()))
    sttp.logging.print_out("DDR3PCTL_MRRSTAT1                 : 0x%08x" %(pctl.DDR3PCTL_MRRSTAT1.peek()))
    sttp.logging.print_out("DDR3PCTL_MCFG1                    : 0x%08x" %(pctl.DDR3PCTL_MCFG1.peek()))
    sttp.logging.print_out("DDR3PCTL_MCFG                     : 0x%08x" %(pctl.DDR3PCTL_MCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_PPCFG                    : 0x%08x" %(pctl.DDR3PCTL_PPCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_MSTAT                    : 0x%08x" %(pctl.DDR3PCTL_MSTAT.peek()))
    sttp.logging.print_out("DDR3PCTL_LPDDR2ZQCFG              : 0x%08x" %(pctl.DDR3PCTL_LPDDR2ZQCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUPDES                  : 0x%08x" %(pctl.DDR3PCTL_DTUPDES.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUNA                    : 0x%08x" %(pctl.DDR3PCTL_DTUNA.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUNE                    : 0x%08x" %(pctl.DDR3PCTL_DTUNE.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUPRD0                  : 0x%08x" %(pctl.DDR3PCTL_DTUPRD0.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUPRD1                  : 0x%08x" %(pctl.DDR3PCTL_DTUPRD1.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUPRD2                  : 0x%08x" %(pctl.DDR3PCTL_DTUPRD2.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUPRD3                  : 0x%08x" %(pctl.DDR3PCTL_DTUPRD3.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUAWDT                  : 0x%08x" %(pctl.DDR3PCTL_DTUAWDT.peek()))
    sttp.logging.print_out("DDR3PCTL_TOGCNT1U                 : 0x%08x" %(pctl.DDR3PCTL_TOGCNT1U.peek()))
    sttp.logging.print_out("DDR3PCTL_TINIT                    : 0x%08x" %(pctl.DDR3PCTL_TINIT.peek()))
    sttp.logging.print_out("DDR3PCTL_TRSTH                    : 0x%08x" %(pctl.DDR3PCTL_TRSTH.peek()))
    sttp.logging.print_out("DDR3PCTL_TOGCNT100N               : 0x%08x" %(pctl.DDR3PCTL_TOGCNT100N.peek()))
    sttp.logging.print_out("DDR3PCTL_TREFI                    : 0x%08x" %(pctl.DDR3PCTL_TREFI.peek()))
    sttp.logging.print_out("DDR3PCTL_TMRD                     : 0x%08x" %(pctl.DDR3PCTL_TMRD.peek()))
    sttp.logging.print_out("DDR3PCTL_TRFC                     : 0x%08x" %(pctl.DDR3PCTL_TRFC.peek()))
    sttp.logging.print_out("DDR3PCTL_TRP                      : 0x%08x" %(pctl.DDR3PCTL_TRP.peek()))
    sttp.logging.print_out("DDR3PCTL_TRTW                     : 0x%08x" %(pctl.DDR3PCTL_TRTW.peek()))
    sttp.logging.print_out("DDR3PCTL_TAL                      : 0x%08x" %(pctl.DDR3PCTL_TAL.peek()))
    sttp.logging.print_out("DDR3PCTL_TCL                      : 0x%08x" %(pctl.DDR3PCTL_TCL.peek()))
    sttp.logging.print_out("DDR3PCTL_TCWL                     : 0x%08x" %(pctl.DDR3PCTL_TCWL.peek()))
    sttp.logging.print_out("DDR3PCTL_TRAS                     : 0x%08x" %(pctl.DDR3PCTL_TRAS.peek()))
    sttp.logging.print_out("DDR3PCTL_TRC                      : 0x%08x" %(pctl.DDR3PCTL_TRC.peek()))
    sttp.logging.print_out("DDR3PCTL_TRCD                     : 0x%08x" %(pctl.DDR3PCTL_TRCD.peek()))
    sttp.logging.print_out("DDR3PCTL_TRRD                     : 0x%08x" %(pctl.DDR3PCTL_TRRD.peek()))
    sttp.logging.print_out("DDR3PCTL_TRTP                     : 0x%08x" %(pctl.DDR3PCTL_TRTP.peek()))
    sttp.logging.print_out("DDR3PCTL_TWR                      : 0x%08x" %(pctl.DDR3PCTL_TWR.peek()))
    sttp.logging.print_out("DDR3PCTL_TWTR                     : 0x%08x" %(pctl.DDR3PCTL_TWTR.peek()))
    sttp.logging.print_out("DDR3PCTL_TEXSR                    : 0x%08x" %(pctl.DDR3PCTL_TEXSR.peek()))
    sttp.logging.print_out("DDR3PCTL_TXP                      : 0x%08x" %(pctl.DDR3PCTL_TXP.peek()))
    sttp.logging.print_out("DDR3PCTL_TXPDLL                   : 0x%08x" %(pctl.DDR3PCTL_TXPDLL.peek()))
    sttp.logging.print_out("DDR3PCTL_TZQCS                    : 0x%08x" %(pctl.DDR3PCTL_TZQCS.peek()))
    sttp.logging.print_out("DDR3PCTL_TZQCSI                   : 0x%08x" %(pctl.DDR3PCTL_TZQCSI.peek()))
    sttp.logging.print_out("DDR3PCTL_TDQS                     : 0x%08x" %(pctl.DDR3PCTL_TDQS.peek()))
    sttp.logging.print_out("DDR3PCTL_TCKSRE                   : 0x%08x" %(pctl.DDR3PCTL_TCKSRE.peek()))
    sttp.logging.print_out("DDR3PCTL_TCKSRX                   : 0x%08x" %(pctl.DDR3PCTL_TCKSRX.peek()))
    sttp.logging.print_out("DDR3PCTL_TCKE                     : 0x%08x" %(pctl.DDR3PCTL_TCKE.peek()))
    sttp.logging.print_out("DDR3PCTL_TMOD                     : 0x%08x" %(pctl.DDR3PCTL_TMOD.peek()))
    sttp.logging.print_out("DDR3PCTL_TRSTL                    : 0x%08x" %(pctl.DDR3PCTL_TRSTL.peek()))
    sttp.logging.print_out("DDR3PCTL_TZQCL                    : 0x%08x" %(pctl.DDR3PCTL_TZQCL.peek()))
    sttp.logging.print_out("DDR3PCTL_TMRR                     : 0x%08x" %(pctl.DDR3PCTL_TMRR.peek()))
    sttp.logging.print_out("DDR3PCTL_TCKESR                   : 0x%08x" %(pctl.DDR3PCTL_TCKESR.peek()))
    sttp.logging.print_out("DDR3PCTL_TDPD                     : 0x%08x" %(pctl.DDR3PCTL_TDPD.peek()))
    sttp.logging.print_out("DDR3PCTL_DWLCFG0                  : 0x%08x" %(pctl.DDR3PCTL_DWLCFG0.peek()))
    sttp.logging.print_out("DDR3PCTL_DWLCFG1                  : 0x%08x" %(pctl.DDR3PCTL_DWLCFG1.peek()))
    sttp.logging.print_out("DDR3PCTL_DWLCFG2                  : 0x%08x" %(pctl.DDR3PCTL_DWLCFG2.peek()))
    sttp.logging.print_out("DDR3PCTL_DWLCFG3                  : 0x%08x" %(pctl.DDR3PCTL_DWLCFG3.peek()))
    sttp.logging.print_out("DDR3PCTL_ECCCFG                   : 0x%08x" %(pctl.DDR3PCTL_ECCCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_ECCTST                   : 0x%08x" %(pctl.DDR3PCTL_ECCTST.peek()))
    sttp.logging.print_out("DDR3PCTL_ECCCLR                   : 0x%08x" %(pctl.DDR3PCTL_ECCCLR.peek()))
    sttp.logging.print_out("DDR3PCTL_ECCLOG                   : 0x%08x" %(pctl.DDR3PCTL_ECCLOG.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWACTL                 : 0x%08x" %(pctl.DDR3PCTL_DTUWACTL.peek()))
    sttp.logging.print_out("DDR3PCTL_DTURACTL                 : 0x%08x" %(pctl.DDR3PCTL_DTURACTL.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUCFG                   : 0x%08x" %(pctl.DDR3PCTL_DTUCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUECTL                  : 0x%08x" %(pctl.DDR3PCTL_DTUECTL.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWD0                   : 0x%08x" %(pctl.DDR3PCTL_DTUWD0.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWD1                   : 0x%08x" %(pctl.DDR3PCTL_DTUWD1.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWD2                   : 0x%08x" %(pctl.DDR3PCTL_DTUWD2.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWD3                   : 0x%08x" %(pctl.DDR3PCTL_DTUWD3.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUWDM                   : 0x%08x" %(pctl.DDR3PCTL_DTUWDM.peek()))
    sttp.logging.print_out("DDR3PCTL_DTURD0                   : 0x%08x" %(pctl.DDR3PCTL_DTURD0.peek()))
    sttp.logging.print_out("DDR3PCTL_DTURD1                   : 0x%08x" %(pctl.DDR3PCTL_DTURD1.peek()))
    sttp.logging.print_out("DDR3PCTL_DTURD2                   : 0x%08x" %(pctl.DDR3PCTL_DTURD2.peek()))
    sttp.logging.print_out("DDR3PCTL_DTURD3                   : 0x%08x" %(pctl.DDR3PCTL_DTURD3.peek()))
    sttp.logging.print_out("DDR3PCTL_DTULFSRWD                : 0x%08x" %(pctl.DDR3PCTL_DTULFSRWD.peek()))
    sttp.logging.print_out("DDR3PCTL_DTULFSRRD                : 0x%08x" %(pctl.DDR3PCTL_DTULFSRRD.peek()))
    sttp.logging.print_out("DDR3PCTL_DTUEAF                   : 0x%08x" %(pctl.DDR3PCTL_DTUEAF.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITCTRLDELAY            : 0x%08x" %(pctl.DDR3PCTL_DFITCTRLDELAY.peek()))
    sttp.logging.print_out("DDR3PCTL_DFIODTCFG                : 0x%08x" %(pctl.DDR3PCTL_DFIODTCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_DFIODTCFG1               : 0x%08x" %(pctl.DDR3PCTL_DFIODTCFG1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFIODTRANKMAP            : 0x%08x" %(pctl.DDR3PCTL_DFIODTRANKMAP.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYWRDATA            : 0x%08x" %(pctl.DDR3PCTL_DFITPHYWRDATA.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYWRLAT             : 0x%08x" %(pctl.DDR3PCTL_DFITPHYWRLAT.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRDDATAEN             : 0x%08x" %(pctl.DDR3PCTL_DFITRDDATAEN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYRDLAT             : 0x%08x" %(pctl.DDR3PCTL_DFITPHYRDLAT.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYUPDTYPE0          : 0x%08x" %(pctl.DDR3PCTL_DFITPHYUPDTYPE0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYUPDTYPE1          : 0x%08x" %(pctl.DDR3PCTL_DFITPHYUPDTYPE1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYUPDTYPE2          : 0x%08x" %(pctl.DDR3PCTL_DFITPHYUPDTYPE2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITPHYUPDTYPE3          : 0x%08x" %(pctl.DDR3PCTL_DFITPHYUPDTYPE3.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITCTRLUPDMIN           : 0x%08x" %(pctl.DDR3PCTL_DFITCTRLUPDMIN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITCTRLUPDMAX           : 0x%08x" %(pctl.DDR3PCTL_DFITCTRLUPDMAX.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITCTRLUPDDLY           : 0x%08x" %(pctl.DDR3PCTL_DFITCTRLUPDDLY.peek()))
    sttp.logging.print_out("DDR3PCTL_DFIUPDCFG                : 0x%08x" %(pctl.DDR3PCTL_DFIUPDCFG.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITREFMSKI              : 0x%08x" %(pctl.DDR3PCTL_DFITREFMSKI.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITCTRLUPDI             : 0x%08x" %(pctl.DDR3PCTL_DFITCTRLUPDI.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRCCFG0               : 0x%08x" %(pctl.DDR3PCTL_DFITRCCFG0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITSTAT0                : 0x%08x" %(pctl.DDR3PCTL_DFITSTAT0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITWRLVLEN              : 0x%08x" %(pctl.DDR3PCTL_DFITWRLVLEN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRDLVLEN              : 0x%08x" %(pctl.DDR3PCTL_DFITRDLVLEN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRDLVLGATEEN          : 0x%08x" %(pctl.DDR3PCTL_DFITRDLVLGATEEN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTSTAT0               : 0x%08x" %(pctl.DDR3PCTL_DFISTSTAT0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTCFG0                : 0x%08x" %(pctl.DDR3PCTL_DFISTCFG0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTCFG1                : 0x%08x" %(pctl.DDR3PCTL_DFISTCFG1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITDRAMCLKEN            : 0x%08x" %(pctl.DDR3PCTL_DFITDRAMCLKEN.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITDRAMCLKDIS           : 0x%08x" %(pctl.DDR3PCTL_DFITDRAMCLKDIS.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTCFG2                : 0x%08x" %(pctl.DDR3PCTL_DFISTCFG2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTPARCLR              : 0x%08x" %(pctl.DDR3PCTL_DFISTPARCLR.peek()))
    sttp.logging.print_out("DDR3PCTL_DFISTPARLOG              : 0x%08x" %(pctl.DDR3PCTL_DFISTPARLOG.peek()))
    sttp.logging.print_out("DDR3PCTL_DFILPCFG0                : 0x%08x" %(pctl.DDR3PCTL_DFILPCFG0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLRESP0          : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLRESP0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLRESP1          : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLRESP1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLRESP2          : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLRESP2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLRESP0          : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLRESP0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLRESP1          : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLRESP1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLRESP2          : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLRESP2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLDELAY0         : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLDELAY0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLDELAY1         : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLDELAY1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRWRLVLDELAY2         : 0x%08x" %(pctl.DDR3PCTL_DFITRWRLVLDELAY2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLDELAY0         : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLDELAY0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLDELAY1         : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLDELAY1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLDELAY2         : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLDELAY2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLGATEDELAY0     : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLGATEDELAY0.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLGATEDELAY1     : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLGATEDELAY1.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRRDLVLGATEDELAY2     : 0x%08x" %(pctl.DDR3PCTL_DFITRRDLVLGATEDELAY2.peek()))
    sttp.logging.print_out("DDR3PCTL_DFITRCMD                 : 0x%08x" %(pctl.DDR3PCTL_DFITRCMD.peek()))
    sttp.logging.print_out("DDR3PCTL_IPVR                     : 0x%08x" %(pctl.DDR3PCTL_IPVR.peek()))
    sttp.logging.print_out("DDR3PCTL_IPTR                     : 0x%08x" %(pctl.DDR3PCTL_IPTR.peek()))
def PrintMixerRegisters(mixer):
    sttp.logging.print_out("Mixer registers:")
    sttp.logging.print_out("---------------------------")
    sttp.logging.print_out("DDR3MIXER_GEN_CTRL                          : 0x%08x" %(mixer.DDR3MIXER_GEN_CTRL.peek()))
    sttp.logging.print_out("DDR3MIXER_INT_MASK                          : 0x%08x" %(mixer.DDR3MIXER_INT_MASK.peek()))
    sttp.logging.print_out("DDR3MIXER_INT                               : 0x%08x" %(mixer.DDR3MIXER_INT.peek()))
    sttp.logging.print_out("DDR3MIXER_ROW_ADDR_MASK                     : 0x%08x" %(mixer.DDR3MIXER_ROW_ADDR_MASK.peek()))
    sttp.logging.print_out("DDR3MIXER_DDR_BASE_ADDR                     : 0x%08x" %(mixer.DDR3MIXER_DDR_BASE_ADDR.peek()))
    sttp.logging.print_out("DDR3MIXER_DDR_PARAMETER                     : 0x%08x" %(mixer.DDR3MIXER_DDR_PARAMETER.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_LOCK                      : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_LOCK.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_LATENCY                   : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_LATENCY.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_CRIT_SITUATION            : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_CRIT_SITUATION.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_HP_PORT                   : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_HP_PORT.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_QUEUE_THRESHOLD           : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_QUEUE_THRESHOLD.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_LOAD_INEFF                : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_LOAD_INEFF.peek()))
    sttp.logging.print_out("DDR3MIXER_BUS_DIR_STORE_INEFF               : 0x%08x" %(mixer.DDR3MIXER_BUS_DIR_STORE_INEFF.peek()))
    sttp.logging.print_out("DDR3MIXER_INPUT_FLOW_REGULATION             : 0x%08x" %(mixer.DDR3MIXER_INPUT_FLOW_REGULATION.peek()))
    sttp.logging.print_out("DDR3MIXER_ARBITER_FLOW_REGULATION           : 0x%08x" %(mixer.DDR3MIXER_ARBITER_FLOW_REGULATION.peek()))
    sttp.logging.print_out("DDR3MIXER_BANK_FLOW_REGULATION              : 0x%08x" %(mixer.DDR3MIXER_BANK_FLOW_REGULATION.peek()))
    sttp.logging.print_out("DDR3MIXER_HI_PRI_PORT_FLOW_REG1             : 0x%08x" %(mixer.DDR3MIXER_HI_PRI_PORT_FLOW_REG1.peek()))
    sttp.logging.print_out("DDR3MIXER_HI_PRI_PORT_FLOW_REG2             : 0x%08x" %(mixer.DDR3MIXER_HI_PRI_PORT_FLOW_REG2.peek()))
    sttp.logging.print_out("DDR3MIXER_REQ_UNIT_MEM_RESOURCE             : 0x%08x" %(mixer.DDR3MIXER_REQ_UNIT_MEM_RESOURCE.peek()))
    sttp.logging.print_out("DDR3MIXER_RES_UNIT_MEM_RESOURCE             : 0x%08x" %(mixer.DDR3MIXER_RES_UNIT_MEM_RESOURCE.peek()))
    sttp.logging.print_out("DDR3MIXER_RES_UNIT0_MEM_RES                 : 0x%08x" %(mixer.DDR3MIXER_RES_UNIT0_MEM_RES.peek()))
    sttp.logging.print_out("DDR3MIXER_RES_UNIT1_MEM_RES                 : 0x%08x" %(mixer.DDR3MIXER_RES_UNIT1_MEM_RES.peek()))
    sttp.logging.print_out("DDR3MIXER_RES_UNIT2_MEM_RES                 : 0x%08x" %(mixer.DDR3MIXER_RES_UNIT2_MEM_RES.peek()))
    sttp.logging.print_out("DDR3MIXER_THR_BW_LIMITER1                   : 0x%08x" %(mixer.DDR3MIXER_THR_BW_LIMITER1.peek()))
    sttp.logging.print_out("DDR3MIXER_THR_BW_LIMITER2                   : 0x%08x" %(mixer.DDR3MIXER_THR_BW_LIMITER2.peek()))
    sttp.logging.print_out("DDR3MIXER_DEBUG_RES_OPCODE_ERR_SRC          : 0x%08x" %(mixer.DDR3MIXER_DEBUG_RES_OPCODE_ERR_SRC.peek()))
    sttp.logging.print_out("DDR3MIXER_DEBUG_LOW_PRI_ADDR_RANGE          : 0x%08x" %(mixer.DDR3MIXER_DEBUG_LOW_PRI_ADDR_RANGE.peek()))
    sttp.logging.print_out("DDR3MIXER_DEBUG_HIGH_PRI_ADDR_RANGE         : 0x%08x" %(mixer.DDR3MIXER_DEBUG_HIGH_PRI_ADDR_RANGE.peek()))
    sttp.logging.print_out("DDR3MIXER_DEBUG_ADDR_RANGE_ERR_SRC          : 0x%08x" %(mixer.DDR3MIXER_DEBUG_ADDR_RANGE_ERR_SRC.peek()))
    sttp.logging.print_out("DDR3MIXER_GEN_PURPOSE_REG0                  : 0x%08x" %(mixer.DDR3MIXER_GEN_PURPOSE_REG0.peek()))
    sttp.logging.print_out("DDR3MIXER_GEN_PURPOSE_REG1                  : 0x%08x" %(mixer.DDR3MIXER_GEN_PURPOSE_REG1.peek()))
    sttp.logging.print_out("DDR3MIXER_GEN_PURPOSE_REG2                  : 0x%08x" %(mixer.DDR3MIXER_GEN_PURPOSE_REG2.peek()))
    sttp.logging.print_out("DDR3MIXER_LOW_HIGH_PRIORITY_BANK            : 0x%08x" %(mixer.DDR3MIXER_LOW_HIGH_PRIORITY_BANK.peek()))
    sttp.logging.print_out("DDR3MIXER_LOW_PRIORITY_SRC_REG              : 0x%08x" %(mixer.DDR3MIXER_LOW_PRIORITY_SRC_REG.peek()))
    
def PCTLDTU_test(ddrparams,simpledisplay):
    pctl=ddrparams["pctl"]
    sttp.logging.print_out("\n------- PCTLDTU TEST launch")
    dtu_row_increment   = 127
    dtu_wr_multi_rd     = 0
    dtu_data_mask_en    = 0
    dtu_target_lane     = 0
    dtu_generate_random = 1
    dtu_incr_banks      = 1
    dtu_incr_cols       = 1
    dtu_nalen           = 63
    dtu_enable          = 1
    pctl.DDR3PCTL_DTUCFG.poke((dtu_row_increment << 16) | (dtu_wr_multi_rd << 15) | (dtu_data_mask_en << 14) | (dtu_target_lane << 10) | (dtu_generate_random << 9) | (dtu_incr_banks << 8) | (dtu_incr_cols << 7) | (dtu_nalen << 1) | (dtu_enable << 0))
    
    number_ranks        = 1 - 1   # number of available ranks minus 1 with a maximum of 4
    row_addr_width      = ddrparams["ddrchip_rowbits"] - 13 # number of row address lines minus 13 with a maximum of 16
    bank_addr_width     = 3 - 2   # number of bank address lines minus 2 with a maximum of 3
    column_addr_width   = ddrparams["ddrchip_colbits"] - 7  # number of column address lines minus 7 with a maximum of 10
    pctl.DDR3PCTL_DTUAWDT.poke((number_ranks << 9) | (row_addr_width << 6) | (bank_addr_width << 3) | (column_addr_width << 0))
    
    seed = 0x35269897
    pctl.DDR3PCTL_DTULFSRWD.poke(seed)    # Initial seed for the random write data generation
    pctl.DDR3PCTL_DTULFSRRD.poke(seed)    # Initial seed for the random read data comparison generation
        
    pctl.DDR3PCTL_DTUWACTL.poke(0)
    pctl.DDR3PCTL_DTURACTL.poke(0)
    
    pctl.DDR3PCTL_DTUECTL.poke(1)           # run the DTU Random test
    pctl.DDR3PCTL_DTUECTL.while_and_ne(1,0) # wait for the test to be done

    pdes=pctl.DDR3PCTL_DTUPDES.peek()
    nberrorstotal = pctl.DDR3PCTL_DTUNE.peek()
    
    if(simpledisplay==0):
        sttp.logging.print_out("%d addresses have been created during the test" %(pctl.DDR3PCTL_DTUNA.peek()))
        sttp.logging.print_out("%d errors have been detected during the test" %(nberrorstotal))
        sttp.logging.print_out("DDR3PCTL_DTUPDES                  : 0x%08x" %(pdes))
        if (((pdes>>13)&1)==1):
            sttp.logging.print_out("Error: One or more Read beats of data did not return from memory!!!!!")
        nblogerror=((pdes>>9)&0xF)
        if (nblogerror>0):
            sttp.logging.print_out("Error: %d errors are logged in the FIFO error"%(nblogerror))
        if (((pdes>>8)&1)==1):
            sttp.logging.print_out("Error: Random Data generated had some failure when written and read to the memories")
        biterrors=(pdes&0xFF)
        if (biterrors>0):
            sttp.logging.print_out("Errors on bits of byte lane %d"%(dtu_target_lane))
            sttp.logging.print_out("b7 b6 b5 b4 b3 b2 b1 b0")
            sttp.logging.print_out(" %d  %d  %d  %d  %d  %d  %d  %d"%(((biterrors>>7)&1),((biterrors>>6)&1),((biterrors>>5)&1),((biterrors>>4)&1),((biterrors>>3)&1),((biterrors>>2)&1),((biterrors>>1)&1),((biterrors>>0)&1)))
    
        errornb=0
        while(errornb<nblogerror):
            errornb=errornb+1
            eaf=pctl.DDR3PCTL_DTUEAF.peek()
            sttp.logging.print_out("Error nb%d detected on rank %d, row %d, bank %d, column %d" %(errornb,((eaf>>30)&3),((eaf>>13)&0xFFFF),((eaf>>10)&7),((eaf>>0)&0x3FF)))
    if((pdes+nberrorstotal)==0):    
        sttp.logging.print_out("------- PCTLDTU test finished SUCCESSFULLY\n\n")
    else:
        sttp.logging.print_out("------- PCTLDTU test finished with ERRORS\n\n")
        ddrparams["error"]="DDR interface test failled. Please, try again"

    pctl.DDR3PCTL_DTUCFG.and_const(~(1 << 0))

def PrintDataTrainingResults(phy):
    dtw = phy.DDR3PHY_DTEDR0.peek()
    dtr = phy.DDR3PHY_DTEDR1.peek()
    sttp.logging.print_out("\n---------- Data Training WDQ (min,max)             = (%d,%d)"%((dtw&0xFF),((dtw>>8)&0xFF)))
    sttp.logging.print_out("---------- Data Training Write BDL Shift (min,max) = (%d,%d)"%(((dtw>>16)&0xFF),((dtw>>24)&0xFF)))
    sttp.logging.print_out("---------- Data Training RDQS LCDL (min,max)       = (%d,%d)"%(((dtr>>0)&0xFF),((dtr>>8)&0xFF)))
    sttp.logging.print_out("---------- Data Training Read BDL Shift (min,max)  = (%d,%d)"%(((dtr>>16)&0xFF),((dtr>>24)&0xFF)))

def PHY_RAMBIST_Test(ddrparams,simpledisplay): # nbbytes is the number of Byte Lanes, size, is the memory size to be checked in MB
    sttp.logging.print_out("\n------- PHY RAM_BIST TEST launch")
    phy=ddrparams["phy"]
    nbbytes=ddrparams["lmi_width"]/8
    size=ddrparams["lmi_size"]
    
    # setup mask bit registers to 0 (no bit masked)
    phy.DDR3PHY_BISTMSKR0.poke(0)
    phy.DDR3PHY_BISTMSKR1.poke(0)
    phy.DDR3PHY_BISTMSKR2.poke(0)
    
    # setup the LFSR Seed Register to default value
    phy.DDR3PHY_BISTLSR.poke(0x1234ABCD)
    
    # Specify the starting address of the bist (0 in our case), the end address and the increment
    bcol    = 0         # starting collumn address
    brow    = 0         # starting row address
    bbank   = 0         # starting bank address
    brank   = 0         # starting rank
    bmrank  = 0         # Maximum rank used for BIST (in our case, single rank => 0)
    bainc   = 8         # address increment for each burst, as we are in BL8 => 8
    if(ddrparams["ddrchip_colbits"]==10):
        bmcol   = 0x3F8     # maximum collumn address before incrementing row address (in this case, we have 10 bits for collumn address bus)
        bmrow   = ((size*128)/nbbytes)-1 # maximum collumn address before incrementing bank address ( = size in B / nb bytes, / 1024 (page size in Word count) / 8 (nb of banks))
    else:
        bmcol   = 0x7F8     # maximum collumn address before incrementing row address (in this case, we have 11 bits for collumn address bus)
        bmrow   = ((size*64)/nbbytes)-1 # maximum collumn address before incrementing bank address ( = size in B / nb bytes, / 2048 (page size in Word count) / 8 (nb of banks))
    bmbank  = 7         # maximum bank address
    phy.DDR3PHY_BISTAR0.poke((bbank << 28) | (brow << 12) | (bcol << 0))
    phy.DDR3PHY_BISTAR1.poke((bainc << 4) | (bmrank << 2) | (brank << 0))
    phy.DDR3PHY_BISTAR2.poke((bmbank << 28) | (bmrow << 12) | (bmcol << 0))
    
    # setup BISTRR first to reset all counters and then to run the RAM bist
    bmode     = 1       # define RAM bist mode
    binf      = 0       # do not run infinite
    nfail     = 100     # Stop bist after nfail errors, if bsonf is set
    bsonf     = 1       # Stop bist after nfail errors, if set
    bdxen     = 1       # Run the bist on Data PHY lane
    bacen     = 0       # don't run the bist on AC PHY lane (incompatible with RAM bist)
    bdmen     = 0       # don't run the Data Mask bist (incompatible with RAM bist)
    bdpat     = 2       # use random data generation for bist
    bdxsel    = 0       # select the Byte Lane used for data comparison
    bcksel    = 0       # select the clock used to latch AC data in loopback mode (not used in RAM bist)
    bccsel    = 0       # select the clock edge for latch AC data in loopback mode (not used in RAM bist)

    # setup the BIST Word Count Register to the maximum number of word count
    phy.DDR3PHY_BISTWCR.poke(0xFFF8)
#    phy.DDR3PHY_BISTWCR.poke(0x80)

    errors=0

    while (bdxsel < nbbytes):
        binst     = 3       # reset all bist run-time registers such as counters
        phy.DDR3PHY_BISTRR.poke((bccsel << 25) | (bcksel << 23) | (bdxsel << 19) | (bdpat << 17) | (bdmen << 16) | (bacen << 15) | (bdxen << 14) | (bsonf << 13) | (nfail << 5) | (binf << 4) | (bmode << 3) | (binst << 0))
        if(simpledisplay==0):
            sttp.logging.print_out("----------- run the PHY RAM_BIST TEST on byte lane %d"%(bdxsel))
        binst     = 1       # run the bist
        phy.DDR3PHY_BISTRR.poke((bccsel << 25) | (bcksel << 23) | (bdxsel << 19) | (bdpat << 17) | (bdmen << 16) | (bacen << 15) | (bdxen << 14) | (bsonf << 13) | (nfail << 5) | (binf << 4) | (bmode << 3) | (binst << 0))
        phy.DDR3PHY_BISTGSR.while_and_ne(0x1,0x1) # Wait for the command to be run
        errorcur=phy.DDR3PHY_BISTGSR.peek()&0x4
        if(errorcur==0x4):
            errors=errors+(phy.DDR3PHY_BISTWER.peek()>>16)
        if(simpledisplay==0):
            sttp.logging.print_out("----------- PHY RAM_BIST TEST on byte lane %d done with status 0x%08X"%(bdxsel,phy.DDR3PHY_BISTGSR.peek()))
            sttp.logging.print_out("------------------- number of words recieved: 0x%04X"%(phy.DDR3PHY_BISTWCSR.peek()>>16))
            if(errorcur==0x4):
                sttp.logging.print_out("----------------------- number of words with error: 0x%04X"%(phy.DDR3PHY_BISTWER.peek()>>16))
                bistber2=phy.DDR3PHY_BISTBER2.peek()
                bistber3=phy.DDR3PHY_BISTBER3.peek()
                sttp.logging.print_out("----------------------- number of errors on bits: 0x%08X   0x%08X"%(bistber2,bistber3))
                nbbit=0
                while (nbbit<8):
                    nberrorbit = ((bistber2 >> (2*nbbit)) & 0x3) + ((bistber2 >> (16 + 2*nbbit)) & 0x3) + ((bistber3 >> (2*nbbit)) & 0x3) + ((bistber3 >> (16 + 2*nbbit)) & 0x3)
                    if(nberrorbit > 0):
                        sttp.logging.print_out("--------------------------- number of errors on bit%d: %d"%(nbbit,nberrorbit))
                    nbbit = nbbit + 1
        
        bdxsel = bdxsel + 1

    if(errors==0):
        sttp.logging.print_out("------- PHY RAM_BIST TEST done SUCCESSFULLY\n")
    elif(errors<10):
        sttp.logging.print_out("------- PHY RAM_BIST TEST done almost SUCCESSFULLY\n")    
    else:
        sttp.logging.print_out("------- PHY RAM_BIST TEST done with ERRORS\n")
    if(errors!=0):
        ddrparams["error"]="PHY RAM BIST Test error"
    return ddrparams
def EnterSelfRefresh(ddrparams):
    #Offset of PIO control registers
    PoutSet     = 0x04
    PoutReset   = 0x08
    PC0Set      = 0x24
    PC0Reset    = 0x28
    PC1Set      = 0x34
    PC1Reset    = 0x38
    PC2Set      = 0x44
    PC2Reset    = 0x48
    # place uPCTL in low-power (self-refresh) mode
    SCTL_STATE_CMD_SLEEP = 3
    STAT_CTL_STAT_LOW_POWER = 5
    ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_SLEEP)
    ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_LOW_POWER)
    # Assert LMI_NOTRETENTION        
    sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PoutReset,1<<ddrparams["retn_pionumber"])	# Set lmi_notretention PIO at 1
    sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC0Reset,1<<ddrparams["retn_pionumber"])	# Configure lmi_notretention PIO as output push-pull
    sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC1Set,1<<ddrparams["retn_pionumber"])
    sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC2Reset,1<<ddrparams["retn_pionumber"])
    sttp.stmc.delay(10000) # wait 10ms for PIO value propagation
def FillMemory(ddrparams, inc):
    start =ddrparams["lmi_start"] << 24
    size = ddrparams["lmi_size"]*1024*1024
    sttp.logging.print_out("  Fill memory @ 0x%x: size=%dB, inc=%dB" % (start,size,inc))
    count = 0
    while(count < size):
        value = start + count
        sttp.pokepeek.poke(value,fill_memory_value)
        count = count + inc
def ReadMemory(ddrparams, inc):
    start =ddrparams["lmi_start"] << 24
    size = ddrparams["lmi_size"]*1024*1024
    sttp.logging.print_out("  Read memory @ 0x%x: size=%dB, inc=%dB" % (start,size,inc))
    count = 0
    err = 0
    ok=0
    while(count < size and err < 4):
        add = start + count
        read_value = sttp.pokepeek.peek(add)
        if ((read_value != fill_memory_value) & ((add < ddrparams["DTUAddress"]) | (add > ddrparams["DTUAddress"] + 4*32))):
            sttp.logging.print_out("  ERROR: Exp=0x%08x @ 0x%08x. Got=0x%08x" % (fill_memory_value,add,read_value))
            err += 1
        if (read_value == fill_memory_value):
            ok += 1
        count = count + inc
    if err > 0:
        sttp.logging.print_out("  => Some ERRORS found")
        ddrparams["error"]="Read Memory Test error"
    else:
        sttp.logging.print_out("  => OK (%d addresses tested)" %(ok))
    return ddrparams
