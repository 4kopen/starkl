import sttp


def setup_stm_port():
    sttp.logging.print_out( " Configure STM Access\n")
    
    # PIO
    sttp.logging.print_out( " PIO-Alt5 (0x09280004)=0x%x\n" %(sttp.pokepeek.peek(0x09280004)))
    sttp.logging.print_out( " Out-En   (0x092800a0)=0x%x\n" %(sttp.pokepeek.peek(0x092800a0)))
    #sttp.logging.print_out( " PullUp   (0x092800C0)=0x%x\n" %(sttp.pokepeek.peek(0x092800C0)))
    sttp.logging.print_out( " Op-drain (0x092800F0)=0x%x\n" %(sttp.pokepeek.peek(0x092800F0)))
    sttp.pokepeek.poke(0x09280004,  0x55555000)  # Alt5 for PIO11
    sttp.pokepeek.poke(0x092800a0,  0x0000F800)  # Output Enable
    #sttp.pokepeek.poke(0x092800C0,  0x00000000)  # Pullup 
    sttp.pokepeek.poke(0x092800F0,  0x00000000)  # Open drain
    sttp.logging.print_out( " PIO-Alt5 (0x09280004)=0x%x\n" %(sttp.pokepeek.peek(0x09280004)))
    sttp.logging.print_out( " Out-En   (0x092800a0)=0x%x\n" %(sttp.pokepeek.peek(0x092800a0)))
    #sttp.logging.print_out( " PullUp   (0x092800C0)=0x%x\n" %(sttp.pokepeek.peek(0x092800C0)))
    sttp.logging.print_out( " Op-drain (0x092800F0)=0x%x\n" %(sttp.pokepeek.peek(0x092800F0)))

    # STM Source Master Range
    sttp.logging.print_out( " ARM      (0x092B02F8)=0x%x\n" %(sttp.pokepeek.peek(0x092B02F8)))
    sttp.logging.print_out( " 231-GP11 (0x092B02FC)=0x%x\n" %(sttp.pokepeek.peek(0x092B02FC)))
    sttp.logging.print_out( " XP70-LPM (0x092B0300)=0x%x\n" %(sttp.pokepeek.peek(0x092B0300)))
    sttp.logging.print_out( " 231-AUD  (0x092B0304)=0x%x\n" %(sttp.pokepeek.peek(0x092B0304)))
    sttp.logging.print_out( " 231-DMU  (0x092B0308)=0x%x\n" %(sttp.pokepeek.peek(0x092B0308)))
    sttp.logging.print_out( " 231-GP10 (0x092B030C)=0x%x\n" %(sttp.pokepeek.peek(0x092B030C)))
    sttp.logging.print_out( " Hades    (0x092B0310)=0x%x\n" %(sttp.pokepeek.peek(0x092B0310)))
    sttp.pokepeek.poke(0x092B02F8,  0x003f0000)  # ARM is Initiator0
    sttp.pokepeek.poke(0x092B02FC,  0x00580058)  # 231-GP11 is Initiator1
    sttp.pokepeek.poke(0x092B0300,  0x00990098)  # XP70-LPM is Initiator2
    sttp.pokepeek.poke(0x092B0304,  0x00530050)  # 231-AUD is Initiator3
    sttp.pokepeek.poke(0x092B0308,  0x00550054)  # 231-DMU is Initiator4
    sttp.pokepeek.poke(0x092B030C,  0x00570056)  # 231-GP10 is Initiator5
    #sttp.pokepeek.poke(0x092B0310,  0x037F0348)  # Hades is Initiator6 #Only PEs (not the FC)
    sttp.pokepeek.poke(0x092B0310,  0x03FF0000)  # All sources #Allow FC + PEs
    sttp.logging.print_out( " ARM      (0x092B02F8)=0x%x\n" %(sttp.pokepeek.peek(0x092B02F8)))
    sttp.logging.print_out( " 231-GP11 (0x092B02FC)=0x%x\n" %(sttp.pokepeek.peek(0x092B02FC)))
    sttp.logging.print_out( " XP70-LPM (0x092B0300)=0x%x\n" %(sttp.pokepeek.peek(0x092B0300)))
    sttp.logging.print_out( " 231-AUD  (0x092B0304)=0x%x\n" %(sttp.pokepeek.peek(0x092B0304)))
    sttp.logging.print_out( " 231-DMU  (0x092B0308)=0x%x\n" %(sttp.pokepeek.peek(0x092B0308)))
    sttp.logging.print_out( " 231-GP10 (0x092B030C)=0x%x\n" %(sttp.pokepeek.peek(0x092B030C)))
    sttp.logging.print_out( " Hades    (0x092B0310)=0x%x\n" %(sttp.pokepeek.peek(0x092B0310)))

    # STM Configuration
    sttp.logging.print_out( " STM_CR   (0x09190000)=0x%x\n" %(sttp.pokepeek.peek(0x09190000)))
    sttp.logging.print_out( " STM_MMC  (0x09190008)=0x%x\n" %(sttp.pokepeek.peek(0x09190008)))
    sttp.logging.print_out( " STM_TER  (0x09190010)=0x%x\n" %(sttp.pokepeek.peek(0x09190010)))
    sttp.logging.print_out( " STM_FTR  (0x09190080)=0x%x\n" %(sttp.pokepeek.peek(0x09190080)))
    sttp.logging.print_out( " STM_CTR  (0x09190088)=0x%x\n" %(sttp.pokepeek.peek(0x09190088)))
    sttp.pokepeek.poke(0x09190000,  0x00000600)  # Clockspeed: 25MHz
    sttp.pokepeek.poke(0x09190008,  0x00000000)  # All Initiator SW
    sttp.pokepeek.poke(0x09190010,  0x000003ff)  # Enable everybody
    sttp.pokepeek.poke(0x09190080,  0x00000001)  # Sync at every messge
    sttp.pokepeek.poke(0x09190088,  0x00000001)  # Sync at every messge
    sttp.logging.print_out( " STM_CR   (0x09190000)=0x%x\n" %(sttp.pokepeek.peek(0x09190000)))
    sttp.logging.print_out( " STM_MMC  (0x09190008)=0x%x\n" %(sttp.pokepeek.peek(0x09190008)))
    sttp.logging.print_out( " STM_TER  (0x09190010)=0x%x\n" %(sttp.pokepeek.peek(0x09190010)))
    sttp.logging.print_out( " STM_FTR  (0x09190080)=0x%x\n" %(sttp.pokepeek.peek(0x09190080)))
    sttp.logging.print_out( " STM_CTR  (0x09190088)=0x%x\n" %(sttp.pokepeek.peek(0x09190088)))

    sttp.logging.print_out( " Config STM End\n ")

## End setup_stm_port()
def setup_rab(hades_start, ddr_start, ddr_size):
    # sets up DDR & STM mappings
    sttp.logging.print_out( " Configure RABs\n")

    hades_end = hades_start + ddr_size - 1

    STM_hades_base = 0xf0000000
    STM_soc_base   = 0x09180000
    STM_size       = 0x00020000
    STM_hades_end  = STM_hades_base + STM_size - 1

    control_rw = 7 # RAB_CTRL__VALID_ON, RAB_READ_WRITE, RAB_WITHOUT_COHERENCY
    control_ro = 3 # RAB_CTRL__VALID_ON, RAB_READ_ONLY , RAB_WITHOUT_COHERENCY

    # program RAB CIC, entry 0: DDR mapping for fabric controller instruction fetch
    sttp.pokepeek.poke(0xa012000 + 4,  hades_start)  # source min
    sttp.pokepeek.poke(0xa012000 + 8,  hades_end  )  # source max
    sttp.pokepeek.poke(0xa012000 + 12, ddr_start  )  # destination
    #sttp.pokepeek.poke(0xa012000     , control_ro )  # control_ro
    sttp.pokepeek.poke(0xa012000     , control_rw )  
    
    # program RAB DIC, entry 1: STM mapping for programming the STM registers by the fabric controller 
    sttp.pokepeek.poke(0xa012010 + 4 , STM_hades_base) # source min
    sttp.pokepeek.poke(0xa012010 + 8 , STM_hades_end ) # source max
    sttp.pokepeek.poke(0xa012010 + 12, STM_soc_base  ) # destination
    sttp.pokepeek.poke(0xa012010     , control_rw    ) # control
    
    # activate RAB CIC
    sttp.pokepeek.poke(0xa013000, 0)

    # program RAB DIC, entry 0: DDR mapping for fabric controller data access
    sttp.pokepeek.poke(0xa014000 + 4,  hades_start)  # source min
    sttp.pokepeek.poke(0xa014000 + 8,  hades_end  )  # source max
    sttp.pokepeek.poke(0xa014000 + 12, ddr_start  )  # destination
    sttp.pokepeek.poke(0xa014000     , control_rw )  # control_ro

    # program RAB DIC, entry 1: STM mapping for programming the STM registers by the fabric controller 
    sttp.pokepeek.poke(0xa014010 + 4 , STM_hades_base) # source min
    sttp.pokepeek.poke(0xa014010 + 8 , STM_hades_end ) # source max
    sttp.pokepeek.poke(0xa014010 + 12, STM_soc_base  ) # destination
    sttp.pokepeek.poke(0xa014010     , control_rw    ) # control

    # activate RAB DIC
    sttp.pokepeek.poke(0xa015000, 0)

    sttp.logging.print_out( " Config RAB End\n ")


# Note: use only one of the following 2 mappings !!

# RAB programming for sample XP70 program loaded at 80000000 (code + data < 256 MB)
L3_start  = 0x80000000 # in Hades space
DDR_start = 0x40000000 # in interco/DDR space
DDR_size  = 0x10000000 # 256 MB

# RAB programming for hades firmware loaded at 9DDDD000 (code + data = 32 MB)
#L3_start  = 0x9DDDD000 # in Hades space
#DDR_start = 0x40000000 # in interco/DDR space
#DDR_size  = 0x02000000 # 32 MB