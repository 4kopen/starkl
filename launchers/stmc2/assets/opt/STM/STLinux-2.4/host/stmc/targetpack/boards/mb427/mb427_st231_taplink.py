import sttp
import sttp.stmc

import st200_nomux
import utilities

# Script for board with typical single core jtag device

def mb427_st231_taplink_connect(parameters):

    sttp.logging.print_out("mb427_st231_taplink_connect() parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(False, parameters)

    st200_nomux.setup_taplink_debug_port(parameters)

    # Poke/peek initialization not currently supported
    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        sttp.logging.print_out("mb427_st231_taplink_connect() initialization start ...")
        sttp.pokepeek.enable()
        mb427_st231_taplink_initall()

def mb427_st231_taplink_initall():
    sttp.logging.print_out("mb427_st231_taplink_initall()")

    sttp.pokepeek.poke(0xfd000078, 0x40000031)
    sttp.pokepeek.poke(0xfd000088, 0x001F0000)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x00BF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x0DF0400)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd000088, 0x00FF0030)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.peek(0xfd000088)
    sttp.pokepeek.poke(0xfd00008C, 0x02401942)
    sttp.pokepeek.poke(0xfd000090, 0x00007c91)
    sttp.pokepeek.poke(0xfd000094, 0x00000000)
    sttp.pokepeek.poke(0xfd000098, 0x10007c91)
    sttp.pokepeek.poke(0xfd00009c, 0x00000000)
    sttp.pokepeek.poke(0xfd0000a0, 0x08000060)
    sttp.pokepeek.poke(0xfd0000a4, 0x0a000060)
    sttp.pokepeek.poke(0xfd0000a8, 0x0c000060)
    sttp.pokepeek.poke(0xfd100028, 0xcafca015)
    sttp.pokepeek.peek(0x40000000)
    sttp.pokepeek.peek(0x40000000)

