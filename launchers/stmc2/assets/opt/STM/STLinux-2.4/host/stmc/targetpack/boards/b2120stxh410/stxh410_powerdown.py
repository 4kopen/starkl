# H410/Monaco2 power OFF code
# Power down any unused IP that are not by default OFF.
# F. Charpentier, UPD Functional Validation

import sttp
import stxh410_uart

def powerdown(parameters):

    syscfg_5xxx_base = 0x92B0000

    uart_debug = 0
    if parameters.has_key("debug"):
        debugList = parameters["debug"].split(":")
        if "uart" in debugList: # UART debug is allowed from flash
            uart_debug = 1

    if uart_debug:
        stxh410_uart.write("TP: Powering down unused IPs\n\r")

    # Audio DAC switch off
    #   SYSCFG_5042
    #   setting AUDIO_DAC_MUTE, AUDIO_DAC_STANDBY_ANA and AUDIO_DAC_STANDBY
    sttp.pokepeek.poke(syscfg_5xxx_base + 0xa8, 0x7)

    # Video DAC switch off
    #   SYSCFG_5072
    #   Setting DAC_HD_HZX and DAC_HD_HZUVW
    sttp.pokepeek.poke(syscfg_5xxx_base + 0x120 + 0xa8, 0x3)
