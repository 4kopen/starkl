# Script for mb424 with stx5301

import sttp
import sttp.logging
import sttp.pokepeek
import sttp.targetpack

# TargetPack imports
import stx5301
import utilities

global sttp_root
sttp_root = sttp.targetpack.get_tree()

def mb424_connect(parameters):

    sttp.logging.print_out("mb424 connect start - parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(False, parameters)

    stx5301.connect(parameters)

    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        sttp.logging.print_out("mb424 initialization start ...")
        sttp.pokepeek.enable()

        cb = sttp.targetpack.get_callback("pre_poke")
        if cb:
            cb(parameters)

        cb = sttp.targetpack.get_callback("setup_pokes")
        if cb:
            cb(parameters)
        else:
            mb424_initall()

        cb = sttp.targetpack.get_callback("post_poke")
        if cb:
            cb(parameters)

    stx5301.complete_connect(parameters)
    
    sttp.logging.print_to_out("mb424 initialization complete\n")
    
def mb424_disconnect():
    pass

def mb424_initall():
    """
    This function was hand-converted from 
    st200-r4.1-solaris/target/board/mb424/board.txt
    """

    mb424_init_interconnect()    

    mb424_init_clocks()

    mb424_init_fmi()    

    mb424_init_lmi()

class ScriptException(Exception):
    pass

def poke(x, y): 
#    sttp.logging.print_out("Poking(addr, data)", hex(x), hex(y))
    sttp.pokepeek.poke(x, y)

def peek(x):
#    sttp.logging.print_out("Peeking(addr)", hex(x))
    value = sttp.pokepeek.peek(x)
#    sttp.logging.print_out("                       = ", hex(value))
    return value;

def mb424_init_interconnect():
    """
    # Name    : mb424_init_interconnect
    # Purpose : Modify the interconnect priorities
    """
    global sttp_root

    Interconnect = sttp_root.stx5301.Interconnect.Interconnect
    
    Interconnect.NHS3_INIT_0_PRIORITY.poke(0x00000006)
    Interconnect.NHS3_INIT_1_PRIORITY.poke(0x00000006)
    Interconnect.NHS3_INIT_2_PRIORITY.poke(0x00000006)
    Interconnect.NHS3_INIT_3_PRIORITY.poke(0x00000006)
    Interconnect.NHS3_INIT_4_PRIORITY.poke(0x00000007)
    
    Interconnect.NHS3_INIT_0_LIMIT.poke(0x00000001)
    Interconnect.NHS3_INIT_1_LIMIT.poke(0x00000001)
    Interconnect.NHS3_INIT_2_LIMIT.poke(0x00000001)
    Interconnect.NHS3_INIT_3_LIMIT.poke(0x00000001)
    Interconnect.NHS3_INIT_4_LIMIT.poke(0x00000001)
    
    Interconnect.NHS4_INIT_0_PRIORITY.poke(0x0000000F)
    Interconnect.NHS4_INIT_1_PRIORITY.poke(0x00000006)
    Interconnect.NHS4_INIT_2_PRIORITY.poke(0x00000005)
    Interconnect.NHS4_INIT_3_PRIORITY.poke(0x00000007)
    
    Interconnect.NHS4_INIT_0_LIMIT.poke(0x00000001)
    Interconnect.NHS4_INIT_1_LIMIT.poke(0x00000001)
    Interconnect.NHS4_INIT_2_LIMIT.poke(0x00000001)
    Interconnect.NHS4_INIT_3_LIMIT.poke(0x00000001)
    
    Interconnect.NHD1_INIT_0_PRIORITY.poke(0x00000008)
    Interconnect.NHD1_INIT_1_PRIORITY.poke(0x00000002)
    Interconnect.NHD1_INIT_2_PRIORITY.poke(0x00000005)
    Interconnect.NHD1_INIT_3_PRIORITY.poke(0x00000003)
    Interconnect.NHD1_INIT_4_PRIORITY.poke(0x00000001)
    Interconnect.NHD1_INIT_5_PRIORITY.poke(0x00000004)
    Interconnect.NHD1_INIT_6_PRIORITY.poke(0x00000007)
    Interconnect.NHD1_INIT_7_PRIORITY.poke(0x00000008)
    
    Interconnect.NHD1_INIT_0_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_1_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_2_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_3_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_4_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_5_LIMIT.poke(0x00000001)
    Interconnect.NHD1_INIT_6_LIMIT.poke(0x00000001)
    
    Interconnect.NHD2_INIT_0_PRIORITY.poke(0x00000007)
    Interconnect.NHD2_INIT_1_PRIORITY.poke(0x00000006)
    Interconnect.NHD2_INIT_2_PRIORITY.poke(0x00000005)
    Interconnect.NHD2_INIT_3_PRIORITY.poke(0x00000004)
    Interconnect.NHD2_INIT_4_PRIORITY.poke(0x00000002)
    
    Interconnect.NHD2_INIT_0_LIMIT.poke(0x00000001)
    Interconnect.NHD2_INIT_1_LIMIT.poke(0x00000001)
    Interconnect.NHD2_INIT_2_LIMIT.poke(0x00000001)
    Interconnect.NHD2_INIT_3_LIMIT.poke(0x00000001)
    Interconnect.NHD2_INIT_4_LIMIT.poke(0x00000001)
    
    Interconnect.CPU_LIMIT.poke(10)
    Interconnect.CPU_FRAME.poke(100)
    
    
def mb424_init_clocks():
    """
    Initialisation of STb5301 Clock Generator
    """

    sttp.logging.print_out("Setup Clock Generator for STb5301")
    
    SysServ = sttp_root.stx5301.SysServ.SysServ

    PLL_A = 0
    PLL_B = 1
    
    PLL_A_FREQUENCY = 533250
    PLL_B_FREQUENCY = 600000
    
    HS_INTERCONNECT_SOURCE = PLL_A
    LS_INTERCONNECT_SOURCE = PLL_A
    VIDEO_SOURCE           = PLL_A
    FDMA_SOURCE            = PLL_A
    LMI_SOURCE             = PLL_A
    BLITTER_SOURCE         = PLL_A
    CPU_SOURCE             = PLL_B
    
    #  Unlock Clock Generator registers
    SysServ.REGISTER_LOCK_CFG.poke(0x00ff)
    SysServ.REGISTER_LOCK_CFG.poke(0x0000)
    
    # Setup PLL sources for clock
    Source = ((HS_INTERCONNECT_SOURCE) << 8) | ((VIDEO_SOURCE) << 7) | (FDMA_SOURCE << 6) | (LMI_SOURCE << 5)
    Source = (Source) | ((LS_INTERCONNECT_SOURCE) << 4) | ((BLITTER_SOURCE) << 3) | ((LMI_SOURCE) << 2)
    Source = (Source) | ((CPU_SOURCE) << 1)

    SysServ.CLOCK_SELECT_CFG.poke(Source)
        
    # Setup PLL_A and PLL_B
    # PLLA
    Nfactor = 237
    Mfactor = 24
    Pfactor = 0

    SysServ.PLLA_CONFIG0.poke(((Nfactor-1)<<8) | (Mfactor-1))
    SysServ.PLLA_CONFIG1.poke(0x4838 | Pfactor)

    # PLLB
    Nfactor = 200
    Mfactor = 18
    Pfactor = 0

    SysServ.PLLB_CONFIG0.poke(((Nfactor-1)<<8) | (Mfactor-1))
    SysServ.PLLB_CONFIG1.poke(0x4838 | Pfactor)

    CPU             = 1
    LMI             = 2
    BLITTER         = 3
    LS_INTERCONNECT = 4
    FDMA            = 5
    VIDEO           = 6
    HS_INTERCONNECT = 7
    FLASH           = 8

    
    # Set PLL Clock Divider (CPU)   (CPU_DIV = 2)  (WHOLE_VALUE)
    # (CPU_SOURCE: PLL_B)
    # CPU = 300 MHz Phase = 0
    offset = CPU * 0x10
    config_0 = 0x00000aaa
    config_1 = 0x00000000
    config_2 = 0x00000071

    SysServ.CPU_CLKDIV_CONFIG0.poke(config_0)
    SysServ.CPU_CLKDIV_CONFIG1.poke(config_1)
    SysServ.CPU_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (LMI)    (LMI_DIV=4)  (WHOLE_VALUE)
    # (LMI_SOURCE: PLL_A)
    # LMI  = 133 MHz Phase = 0
    offset = LMI * 0x10
    config_0 = 0x0000cccc
    config_1 = 0x00000000
    config_2 = 0x00000075

    SysServ.LMI_CLKDIV_CONFIG0.poke(config_0)
    SysServ.LMI_CLKDIV_CONFIG1.poke(config_1)
    SysServ.LMI_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (BLITTER)  (BLITTER_DIV: 3.0)  (WHOLE_VALUE)
    # (BLITTER_SOURCE: PLL_A)
    # BLITTER         = 177 MHz Phase = 0
    offset = BLITTER * 0x10
    config_0 = 0x00000db6
    config_1 = 0x00000000
    config_2 = 0x00000011

    SysServ.BLT_CLKDIV_CONFIG0.poke(config_0)
    SysServ.BLT_CLKDIV_CONFIG1.poke(config_1)
    SysServ.BLT_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (LS_INTERCONNECT)  (LS_INTERCONNECT_DIV: 5.0)
    # (WHOLE_VALE)  (LS_INTERCONNECT_SOURCE: PLL_A)
    # LS_INTERCONNECT = 106 MHz Phase = 0
    offset = LS_INTERCONNECT  * 0x10
    config_0 = 0x0000739c
    config_1 = 0x00000000
    config_2 = 0x00000014

    SysServ.LS_IC_CLKDIV_CONFIG0.poke(config_0)
    SysServ.LS_IC_CLKDIV_CONFIG1.poke(config_1)
    SysServ.LS_IC_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (FDMA)   (FDMA_DIV: 2.0)  (WHOLE_VALUE)
    # (FDMA_SOURCE: PLL_A)
    # FDMA            = 266 MHz Phase = 0
    offset = FDMA * 0x10
    config_0 = 0x00000aaa
    config_1 = 0x00000000
    config_2 = 0x00000071

    SysServ.FDMA_CLKDIV_CONFIG0.poke(config_0)
    SysServ.FDMA_CLKDIV_CONFIG1.poke(config_1)
    SysServ.FDMA_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (VIDEO)   (VIDEO_DIV: 5.0)  (WHOLE_VALUE)
    # (VIDEO_SOURCE: PLL_A)
    # VIDEO           = 88 MHz Phase = 0
    offset = VIDEO * 0x10
    config_0 = 0x00000e38
    config_1 = 0x00000000
    config_2 = 0x00000071

    SysServ.VID_CLKDIV_CONFIG0.poke(config_0)
    SysServ.VID_CLKDIV_CONFIG1.poke(config_1)
    SysServ.VID_CLKDIV_CONFIG2.poke(config_2)

    
    # Set PLL Clock Divider (HS_INTERCONNECT)  (HS_INTERCONNECT_DIV:4.0)
    # (HS_INTERCONNECT_SOURCE: PLL_A)
    # HS_INTERCONNECT = 133 MHz Phase = 0
    offset = HS_INTERCONNECT  * 0x10
    config_0 = 0x0000cccc
    config_1 = 0x00000000
    config_2 = 0x00000075

    SysServ.HS_IC_CLKDIV_CONFIG0.poke(config_0)
    SysServ.HS_IC_CLKDIV_CONFIG1.poke(config_1)
    SysServ.HS_IC_CLKDIV_CONFIG2.poke(config_2)
    
    # Set PLL Clock Divider (FLASH)   (FLASH_DIV: 5.0)  (WHOLE_VALUE)
    # (LS_INTERCONNCT_SOURCE: PLL_A)
    # FLASH           = 106 MHz Phase = 0
    offset = FLASH * 0x10
    config_0 = 0x0000739c
    config_1 = 0x00000000
    config_2 = 0x00000014

    SysServ.FLASH_CLKDIV_CONFIG0.poke(config_0)
    SysServ.FLASH_CLKDIV_CONFIG1.poke(config_1)
    SysServ.FLASH_CLKDIV_CONFIG2.poke(config_2)
    
    # End of file
    
  
    # Configure the Frequency Synthesizers for Pixel, PCM, SPDIF,
    # SmartCard, DAA, USB, Auxiliary, and Audio Clock
    
    SysServ.FSA_SETUP.poke(0x4)
    SysServ.FSB_SETUP.poke(0x4)

    # Pixel clock = 27 MHz
    MD   = 0x1f
    SDIV = 0x2
    PE   = 0x0000
    SysServ.PIX_CLK_SETUP0.poke((0xE00 | (SDIV)<<6 | (MD)))
    SysServ.PIX_CLK_SETUP1.poke((PE))
    
    # PCM clock = 24.576 MHz
    SysServ.PCM_CLK_SETUP0.poke(0x0ef1)
    SysServ.PCM_CLK_SETUP1.poke(0x3600)
    
    # SPDIF clock = 27 MHz
    SysServ.SPDIF_CLK_SETUP0.poke(0x0EBF)
    SysServ.SPDIF_CLK_SETUP1.poke(0x0000)
    
    # Smart Card clock = 27 MHz
    SysServ.SC_CLK_SETUP0.poke(0x0EBF)
    SysServ.SC_CLK_SETUP1.poke(0x0000)
    
    # DAA clock = 32.4 MHz
    SysServ.DAA_CLK_SETUP0.poke(0x0EBA)
    SysServ.DAA_CLK_SETUP1.poke(0x2AAA)
    
    # USB clock = 48 MHz
    SysServ.USB_CLK_SETUP0.poke(0x0EB1)
    SysServ.USB_CLK_SETUP1.poke(0x0000)
    
    # AUX clock = 27 MHz
    SysServ.AUX_CLK_SETUP0.poke(0x0EBF)
    SysServ.AUX_CLK_SETUP1.poke(0x0000)
    
    # AUDIO clock = 160 MHz
    SysServ.AUDIO_CLK_SETUP0.poke(0x0E35)
    SysServ.AUDIO_CLK_SETUP1.poke(0x3333)
    
    SysServ.CLOCK_OBSERVATION_CFG.poke(0x21)
        
    #  Check PLL_A and PLL_B has locked
    LockA = 0
    LockB = 0
    count = 0

    sttp.pokepeek.debug_peek(0x4000)  
    if not SysServ.PLLA_CONFIG1.while_and_ne(0x4000, 0x4000, 20):
        raise sttp.STTPException("PLL_A failed to LOCK!")

    sttp.pokepeek.debug_peek(0x4000)  
    if not SysServ.PLLB_CONFIG1.while_and_ne(0x4000, 0x4000, 20):
        raise sttp.STTPException("PLL_B failed to LOCK!")
    
    # Now Transistion to programmed mode
    SysServ.MODE_CONTROL.poke(0x2)
    
    #  Lock Clock Generator registers
    SysServ.REGISTER_LOCK_CFG.poke(0x00FF)
    sttp.logging.print_out("STb5301 Clock Generator Successfully setup")
    

def mb424_init_fmi():
    #######################
    # FMI CONFIGURATION ##
    #######################

    FMI = sttp_root.stx5301.FMI.FMI
    FMIBuffer = sttp_root.stx5301.FMIBuffer.FMIBuffer
        
    # Ensure all FMI control registers are unlocked
    # at reset the state of these regs is 'undefined'
    
    FMI.FMI_LOCK.poke(0x00000000)
    FMI.FMI_STATUSLOCK.poke(0x00000000)
    
    # Number of FMI Banks : Enable all banks
    FMIBuffer.BANKS_ENABLED.poke(0x00000004)
    
    # NOTE: bits [0,7] define bottom address bits [22,29] of bank
    # Bank 0 - Ethernet controller - LAN9118 16 bit
    # bits [22:29] = 00000011
    FMIBuffer.BANK_0_TOP_ADDRESS.poke(0x00000003)
    # 0x40000000 - 0x40FFFFFF
    
    # Bank 1 - 32MBytes Stem0/DVBCI/EPLD Configured as 16-bit peripheral
    # bits [22:29] = 00001011
    FMIBuffer.BANK_1_TOP_ADDRESS.poke(0x0000000B)
    # 0x41000000 - 0x42FFFFFF
    
    # Bank 2 - 32MBytes Stem1 Configured as 16-bit peripheral
    # bits [22:29] = 00010011
    FMIBuffer.BANK_2_TOP_ADDRESS.poke(0x00000013)
    # 0x43000000 - 0x44FFFFFF
    
    # Bank 3 - 8MBytes ST M58LW064D Flash
    # Note only the top 8Mbytes is populated from 0x7F800000
    # bits [22:29] = 11111111
    FMIBuffer.BANK_3_TOP_ADDRESS.poke(0x000000FF)
    # 0x45000000 - 0x7FFFFFFF
    
    # ----------------------------------------------------------------------------
    # Program bank functions
    # ----------------------------------------------------------------------------
    
    # ----------------------------------------------------------------------------
    # Bank 0 - Ethernet controller - LAN9118 16 bit
    # ----------------------------------------------------------------------------
    
    FMI.FMICONFIGDATA0_BANK0.poke(0x001016D1)
    FMI.FMICONFIGDATA1_BANK0.poke(0x9d200000)
    FMI.FMICONFIGDATA2_BANK0.poke(0x9d220000)
    FMI.FMICONFIGDATA3_BANK0.poke(0x00000000)
    
    # ----------------------------------------------------------------------------
    # Bank 1 - Not used 
    # ----------------------------------------------------------------------------
    
    # ----------------------------------------------------------------------------
    # Bank 2 - Not Used
    # ----------------------------------------------------------------------------
    
    # FMI Bank 3 8MBytes ST M58LW064D Flash
    # ----------------------------------------------------------------------------
    # Bank 3 - 8MBytes ST M58LW064D Flash
    # ----------------------------------------------------------------------------
    
    FMI.FMICONFIGDATA0_BANK3.poke(0x001016D1)
    FMI.FMICONFIGDATA1_BANK3.poke(0x9d200000)
    FMI.FMICONFIGDATA2_BANK3.poke(0x9d220000)
    FMI.FMICONFIGDATA3_BANK3.poke(0x00000000)
    
    #  ------- Program Other FMI Registers --------  ##
    # Synchronous Flash runs @ 1/3 bus clk
    
    FMI.FMI_GENCFG.poke(0x00000000)
    # Flash clock 1/3 STbus CLK
    FMI.FMI_FLASHCLKSEL.poke(0x00000002)
    # Enable Flash
    FMI.FMI_CLKENABLE.poke(0x00000001)
        
def mb424_init_lmi():

    #######################
    # LMI CONFIGURATION ##
    #######################
    LMI = sttp_root.stx5301.LMI.LMI
        
    # Configuring LMI for 16-bit data CAS 2.0 @ 133MHz
    
    # Set LMI_COC_UPPER Register, bits [63:32]   (LMI Pad logic)  +0x2C
    LMI.LMI_COC_UPPER.poke(0x000C6750)
    
    
    # Set LMI_COC_LOWER Register, bits [31:0]  (LMI Pad logic)
    # Bits [19:18] Compensation mode DDR
    # Bits [17:16] Pad strenght    (0x0:5pF, 0x1:15pF, 0x2:25pF, Ox3:35pF)
    # Bits [15:14] output Impedance (0x0:25Ohm, 0x1:40Ohm, 0x2:55Ohm, Ox3:70Ohm)
    # Bit  [13]    DLL preset reset value enable
    
    LMI.LMI_COC_LOWER.poke(0x00002000)
    # What is this - u-boot does not seem to need it!
    #data = LMI.LMI_COC_LOWER.peek()
    #data = (data | 0x0<<16 | 0x0<<14)
    LMI.LMI_COC_LOWER.poke(0x00002000)
    
    # SDRAM Mode Register
    # Set Refresh Interval, Enable Refresh, 16-bit bus,
    # Grouping Disabled, DDR-SDRAM, Enable.
    # Bits[27:16]: Refresh Interval = 7.8 microseconds (8K/64ms)
    # @  50MHz =  390 clk cycles -> 0x186
    # @  75MHz =  585 clk cycles -> 0x249
    # @ 100MHz =  780 clk cycles -> 0x30C
    # @ 125MHz =  975 clk cycles -> 0x3CF
    # @ 133MHz = 1040 clk cycles -> 0x410  <--
    # @ 166MHz = 1300 clk cycles -> 0x514
    # poke -d (LMI_MIM)
    LMI.LMI_MIM0.poke(0x04100203)
    
    # SDRAM Timing Register
    # For 133MHz (7.5ns) operation:
    #  3 clks RAS_precharge, Trp;
    #  3 clks RAS_to_CAS_delay, Trcd-r;
    #  8 clks RAS cycle time, Trc;
    #  6 clks RAS Active time, Tras;
    #  2 clks RAS_to_RAS_Active_delay, Trrd;
    #  2 clks Last write to PRE/PALL period SDRAM, Twr;
    #  2 clks CAS Latency;
    # 10 clks Auto Refresh RAS cycle time, Trfc;
    #  Enable Write to Read interruption;
    #  1 clk  Write to Read interruption, Twtr;
    #  3 clks RAS_to_CAS_delay, Trcd-w;
    # (200/16)=3 clks Exit self-refresh to next command, Tsxsr;
    LMI.LMI_STR.poke(0x35085225)
    
    # SDRAM Row Attribute 0 & 1 Registers
    # UBA = 32MB + Base Adr, Quad-bank, Shape 13x9,
    # Bank Remapping Disabled
    ##
    #  LMI base address  0xC0000000
    #  Memory size 32MB  0x02000000
    #  Row UBA value     0xC200
    ##
    # write set memory size
    # if (SDRAM_CONFIG == "BUILT_FOR_256MBIT") {
    LMI.LMI_SDRA0.poke(0xC2001900)
    
    #  We just have one Row connected to cs0, so we must program UBA0 = UBA1,
    #  following LMI specification
    # if (SDRAM_CONFIG == "BUILT_FOR_256MBIT") {
    LMI.LMI_SDRA1.poke(0xC9001900)
    
    
    # ---------------------------------------------------------------------------
    # Initialisation Sequence for LMI & DDR-SDRAM Device
    # ---------------------------------------------------------------------------
        
    # Simudelay (100)

    # SDRAM Control Register
    # Clock enable
    LMI.LMI_SCR.poke(0x3)
        
    # NOP enable
    LMI.LMI_SCR.poke(0x1)
    
    # Precharge all banks
    LMI.LMI_SCR.poke(0x2)
    
    # LMI_SDRAM_ROW_MODE0 & LMI_SDRAM_ROW_MODE1 Registers
    # EMRS Row 0 & 1: Weak Drive : Enable DLL
    LMI.LMI_SDMR0.poke(0x0402)
    LMI.LMI_SDMR1.poke(0x0402)
    
    # Simudelay (100)
    #fg  file simudelay.txt
    
    # MRS Row 0 & 1 : Reset DLL - /CAS = 2.0, Mode Sequential, Burst Length 8
    LMI.LMI_SDMR0.poke(0x0123)
    LMI.LMI_SDMR1.poke(0x0123)
    
    # 200 clock cycles required to lock DLL
    #fg  # Simudelay (100)

    # Precharge all banks
    LMI.LMI_SCR.poke(0x2)
    
    # CBR enable (auto-refresh)
    LMI.LMI_SCR.poke(0x4)
    LMI.LMI_SCR.poke(0x4)
    LMI.LMI_SCR.poke(0x4)
    
    
    # LMI_SDRAM_ROW_MODE0 & 1 Registers
    # MRS Row 0 & 1 : Normal - /CAS = 2.0, Mode Sequential, Burst Length 8
    LMI.LMI_SDMR0.poke(0x0023)
    LMI.LMI_SDMR1.poke(0x0023)
    
    # Normal SDRAM operation, No burst Refresh after standby
    LMI.LMI_SCR.poke(0x0)
