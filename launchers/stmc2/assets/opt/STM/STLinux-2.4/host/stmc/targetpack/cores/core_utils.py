"""
Utilities for SoCs with st40/st231 cores

    coreParams is a reference to a data structure. it is
    synchronised between host and stmc2. The caller is responsible
    for retrieval and save.
     
    If this core is the poke peek configuration core, then
    add a core specific poke peek timeout configuration.
    For the st231 core agent, the parameter is "stmc_pokepeek_timeout".
    The default unit is millisecond.
    The default timeout is 5 seconds.
"""
"""
Functions for ARM cores are added in this file.
"""

import os
import sys

import sttp
import sttp.stmc
import sttp.targetpack
import sttp.targetinfo
import arm_a9
import arm_a5x
import arm_cm4

def setup_debug(prefix):
    if os.getenv("TARGETSCRIPT_DEBUG"):
        return prefix
    else:
        return None

def debug(debugPrefix, m):
    if debugPrefix != None:
        print >> sys.stderr, debugPrefix, m

def coreIsPokePeek(debugPrefix, coreName):
    debug(debugPrefix, "coreIsPokePeek(%s)" % (coreName))
    ci = sttp.stmc.get_core_info(coreName)
    debug(debugPrefix, "Core info: %s" % (ci))
    if ci.has_key("poke_peek") and ci["poke_peek"]:
        return True
    return False


def setup_default_timeout(debugPrefix, coreParams):

    # Note stmc timeouts in "ms", linktimeout in "s"

    tp = sttp.stmc.get_target_params()

    tsp = sttp.targetinfo.get_targetstring_parameters()

    jtag_timeout = None
    if tsp.has_key("jtag_timeout"):
        # if jtag_timeout defined in TargetString
        jtag_timeout = tsp["jtag_timeout"]
    elif tp.has_key("jtag_timeout"): 
        # if jtag_timeout defined in TargetPack
        jtag_timeout = tp["jtag_timeout"]

    if jtag_timeout:
        # if jtag_timeout defined in TargetPack
        if not coreParams.has_key("linktimeout"):
            # linktimeout in secs
            coreParams["linktimeout"] = jtag_timeout + "ms"

    if coreParams.has_key("linktimeout"):
        coreParams["linktimeout_original"] = coreParams["linktimeout"]

def set_pokepeek_coreagent_timeout(debugPrefix, coreName, coreParams):
    debug(debugPrefix, "setup_pokepeek_coreagent_timeout(%s)" % (coreName))

    if not coreIsPokePeek(debugPrefix, coreName):
        return

    # The stmc timeout unit is "ms", the "linktimeout" is "s"
    timeout = 5000 # Default

    tp = sttp.stmc.get_target_params()
    debug(debugPrefix, "set_pokepeek_coreagent_timeout, Target params tp: %s" % (tp,))

    if tp.has_key("jtag_timeout"):
        timeout = int(tp["jtag_timeout"])

    tsp = sttp.targetinfo.get_targetstring_parameters() # Mastered on host.
    debug(debugPrefix, "set_pokepeek_coreagent_timeout, TargetString params tsp: %s" % (tsp,))

    if tsp.has_key("jtag_timeout"):
        timeout = int(tsp["jtag_timeout"])

    if tsp.has_key("stmc_pokepeek_timeout"):
        timeout = int(tsp["stmc_pokepeek_timeout"])

    coreParams["stmc_pokepeek_timeout"] = str(timeout) # Will be saved by caller.

    # linktimeout specified in secs by default : add "ms"
    coreParams["linktimeout"] = str(timeout) + "ms"
    debug(debugPrefix, "Core agent timeout (linktimeout) set to %s ms" % (timeout))

# find core instance by its name
def find_cpu(root, name):
    for c in root.get_children():
        if type(c) == sttp.targetpack.Cpu and str(c.parent.name) == str(name):
            return c
        if type(c) == sttp.targetpack.Component:
            r = find_cpu(c, name)
            if r != None:
                return r
    return None

def find_cluster_count(parameters, coreName):
    sttp_root = sttp.targetpack.get_tree()
    core = find_cpu(sttp_root, coreName)
    if (core is None) or (not core.parameters.has_key("coreType")):
        sttp.logging.print_out("Fatal error: non ""coreType"" defined for the core %s." % coreNmae)
        return 0
    coretype = core.parameters['coreType']

    core_list = []
    def find_all_cores_by_coretype(parent):
        if isinstance(parent, sttp.targetpack.Cpu) and parent.parameters.has_key("coreType") and parent.parameters["coreType"].find(coretype)==0:
            core_list.append(parent.parent.name)
        
        for c in parent.get_children():
            find_all_cores_by_coretype(c)

    find_all_cores_by_coretype(sttp_root)
    return (len(core_list)-1)

a9_init_done = False
a5x_init_done = False
cm4_init_done = False
def _arm_core_module_init(parameters, coreName):
    global a9_init_done
    global a5x_init_done
    global cm4_init_done

    index_prefix = "_"
    if not a9_init_done and (coreName[0:2] == "a9"):
        arm_cluster_count = find_cluster_count(parameters, coreName)
        coreNameWoIndex = coreName
        i = coreName.rfind(index_prefix)
        if i != -1 and coreName[i+1:].isdigit():
            coreNameWoIndex = coreName[0:i]
        arm_a9.init({"arm_core_name" : coreNameWoIndex, "index_prefix" : index_prefix, "arm_cluster_count" : arm_cluster_count}, parameters)    
        a9_init_done = True
    elif not a5x_init_done and coreName[0:3] == "a5x":
        arm_cluster_count = find_cluster_count(parameters, coreName)
        arm_a5x.init({"arm_core_name" : "a5x", "index_prefix" : index_prefix, "arm_cluster_count" : arm_cluster_count}, parameters)
        a5x_init_done = True
    elif not cm4_init_done and coreName[0:3] == "cm4":
        arm_cm4.init({"arm_core_name" : coreName}, parameters)
        cm4_init_done = True

def arm_core_module_init(parameters):
    coreName = sttp.targetinfo.get_targetstring_core()
    sttp.logging.print_out("coreName = %s" % coreName)

    # Initialize the arm_core module 
    if len(coreName)>=2:
        _arm_core_module_init(parameters, coreName)
    else:
        raise sttp.STTPException("Unknown core name: %s" % coreName)

    activeCoresList = []
    if parameters.has_key("active_cores"):
        activeCoresList = parameters["active_cores"].split(":")

    for core in activeCoresList:
        if core != coreName:
            _arm_core_module_init(parameters, core)
