# board_i2c_control: 
# a module which provides convenience functions
# for board control using the underlying i2c module
#

import dbu
import i2c
import os
import struct
import sys
import time

import sttp
import sttp.targetinfo

global _all_config_tables
global _all_modepins
global _allow_read_only_modification
global _all_resistors
global _all_voltages
global _field_defs
global _the_configuration
global _the_parameters

_all_config_tables = []
_all_modepins      = {}
_all_resistors     = []
_all_voltages      = {}

_allow_read_only_modification = False

_do_debug = False
_do_default_modepin_setup     = True
_do_default_voltage_setup     = True

_i2c_timeout                  = 1000 # ms

_voltages_maximum_validate    = True


def _set_debug(state):
    global _do_debug
    _do_debug = state

#
# Config table support functions and data structure
#

def _table_cmp(a, b):
    # each item is an array (name, sort_order_value, ...)

    a_s = a[1]
    b_s = b[1]
    if a_s == b_s:
        return 0
    elif a_s < b_s:
        return -1
    else:
        return 1

def _get_entry_info(name, index, config_table_id, config_table_version):

    # build up a table of entries of the form
    #       entry[], sequence_id, count_field
    # e.g.  "ddr_entry[]", 12, "num_lmis"

    # then sort by sequence_id

    offset = None
    table_entry_refs = []
    for k, v in _field_defs[config_table_version].iteritems():
        if k.rfind("[]") != len(k)-2:
            continue
        table_entry_refs.append((k, v["seq"], v["count"]))
    table_entry_refs.sort(_table_cmp)
    # offset set to 1st item - must be defined in the table
    offset =  _field_defs[config_table_version][table_entry_refs[0][0]]["offset"]

    for e in table_entry_refs:
        if name == e[0]:
            break                
        num  = _get_field(e[2], config_table_id)
        size = _field_defs[config_table_version][e[0]]["size"] 

        offset += (size * num)

    # do we have any entries for the name we passed in
    count = _get_field(e[2], config_table_id)

    if index == -1: index = 0

    base_offset = offset
    size = _field_defs[config_table_version][name]["size"]
    offset += (index * size)

    r = { "offset" : offset, "entries" : count, "base_offset" : base_offset, "index" : index, "size" : size }

    return r
    
   
_field_defs = {
  "config_table_version" : { "offset" : 0x00, "size" : 4,  "type" : "i", "value" : 1 },
  1 : {
    "board_code"     : { "seq" : 0, "offset" : 0x04, "size" : 8,   "type" : "s", "access" : "read-only" },
    "serial_number"  : { "seq" : 1, "offset" : 0x0c, "size" : 4,   "type" : "s", "access" : "read-only" },
    "cfg_features"   : { "seq" : 2, "offset" : 0x10, "size" : 4,   "type" : "i" },
    "i2c_router"     : { "seq" : 3, "offset" : 0x14, "size" : 1,   "type" : "i" },
    "reserved0"      : { "seq" : 4, "offset" : 0x15, "size" : 1,   "type" : "r" },
    "reserved1"      : { "seq" : 5, "offset" : 0x16, "size" : 1,   "type" : "r" },
    "reserved2"      : { "seq" : 6, "offset" : 0x17, "size" : 1,   "type" : "r" },
    "num_lmis"       : { "seq" : 7, "offset" : 0x18, "size" : 1,   "type" : "i"  },
    "num_spis"       : { "seq" : 8, "offset" : 0x19, "size" : 0.5, "type" : "il" },
    "num_nands"      : { "seq" : 9, "offset" : 0x19, "size" : 0.5, "type" : "ih" },
    "num_nors"       : { "seq" :10, "offset" : 0x1a, "size" : 0.5, "type" : "il" },
    "num_i2cs"       : { "seq" :11, "offset" : 0x1a, "size" : 0.5, "type" : "ih" },
    "num_emmcs"      : { "seq" :12, "offset" : 0x1b, "size" : 0.5, "type" : "il" },
    "reserved3"      : { "seq" :13, "offset" : 0x1b, "size" : 0.5, "type" : "rh" },
    "num_macs"       : { "seq" :14, "offset" : 0x1c, "size" : 1,   "type" : "i" },
    "reserved4"      : { "seq" :15, "offset" : 0x1d, "size" : 3,   "type" : "r" },
    "reserved5"      : { "seq" :16, "offset" : 0x20, "size" : 32,  "type" : "r" },
    "ddr_entry[]"    : { "seq" :17, "offset" : 0x40, "size" : 4,   "type" : "i", "count" : "num_lmis"  },
    "spi_entry[]"    : { "seq" :18,                  "size" : 4,   "type" : "i", "count" : "num_spis"  },
    "nand_entry[]"   : { "seq" :19,                  "size" : 4,   "type" : "i", "count" : "num_nands" },
    "nor_entry[]"    : { "seq" :20,                  "size" : 4,   "type" : "i", "count" : "num_nors"  },
    "i2c_entry[]"    : { "seq" :21,                  "size" : 4,   "type" : "i", "count" : "num_i2cs"  },
    "emmc_entry[]"   : { "seq" :22,                  "size" : 4,   "type" : "i", "count" : "num_emmcs" },
    "mac_entry[]"    : { "seq" :23,                  "size" : 8,   "type" : "i", "count" : "num_macs"  },
  }
}


#
#  Configuration table functions
#

def _get_board_id(n, config_table_id, suppress_config_error = False):
    board, name = _split_qualified_name(n)

    if board and config_table_id and config_table_id != board:
        raise sttp.STTPException("Mismatching config_table_id %s and field specifier %s" % (config_table_id, n))

    if None == board and None == config_table_id:
        if len(_all_config_tables)>1:
            if suppress_config_error:
                raise sttp.STTPException("Must specify qualified name for field %s" % (n, ))
            else:
                raise sttp.STTPException("Must specify qualified name or config_table name for field %s" % (n, ))
        else:
            b = _all_config_tables[0]["config_table_id"]
    elif board:
        b = board
    elif config_table_id:
        b = config_table_id
    else:
        raise sttp.STTPException("Internal error board %s config_table_id %s" % (board, config_table_id ))

    return b, name 

def _lookup_device(config_table_id):
    global _all_config_tables
    device = None 
    table_index = None

    # Work out which physical device we should use based on the config_table_id
    if None == config_table_id:
        if len(_all_config_tables) > 1:
            raise sttp.STTPException("Must specify the config table to use because multiple tables exists")
        cid = _all_config_tables[0]["config_table_id"]
        device = _all_config_tables[0]["device"]
        table_index = 0
    else:
        for i in range(len(_all_config_tables)):
            t = _all_config_tables[i]
            if t["config_table_id"] == config_table_id:
                 device = t["device"]
                 cid = config_table_id
                 table_index = i
                 break

    if None == device:
        raise sttp.STTPException("Could not find unique config table identifier")

    return cid, device, table_index

def _MakeInt(self, value):
    s = struct.pack("L", value)
    [v] = struct.unpack("i", s)
    return v

def _pack_field(name, value, info, existing_byte):

    if "i" == info["type"][0]:
        if 4 == info["size"]:
            if 2147483647 == sys.__dict__["maxint"]:
                # 32 bit machine and Python build
                s = struct.pack("L", value)
            else:
                # On a 64 bit machine
                s = struct.pack("i", value)
                s = s[0:4]
        elif 8 == info["size"]:
            s = struct.pack("Q", value)
        elif 1 == info["size"]:
            s = struct.pack("B", value)
        elif 0.5 == info["size"]:
            byte = existing_byte

            if "h" == info["type"][1]:
                 byte &= 0x0f
                 byte |= (value << 4)
            else:
                 byte &= 0xf0
                 byte |= value

            s = struct.pack("B", byte)
            return s
    elif "s" == info["type"]:
        count = len(value)
        s = value
    else:
        raise sttp.STTPException("Internal error: invalid table definition")

    return s


def _lookup_config_table_version(config_table_id):
    return _the_configuration["boards"][config_table_id]["config_table"]["version"]

def _get_field(name, config_table_id):
    cid, device, table_index = _lookup_device(config_table_id)

    ee_mem_size = device.ee_mem.get_size()
    
    if "config_table_version" == name:
        # special hardcoded place
        offset = 0
        size   = 4
        v      = { "size" : 4, "type" : "i" }
    else:
        v = _lookup_field(name, cid)
        if None == v:
            return None

        offset = v["offset"]
        size   = v["size"]

    if 0.5 == size:
         count = 1
    else:
         count = size

    if "r" == v["type"][0]:
        return None

    if _all_config_tables[table_index].has_key("cached_read"):        
        read_buffer = _all_config_tables[table_index]["cached_read"]
    else:
        read_buffer = device.ee_mem.read_byte_string(0, ee_mem_size)
        _all_config_tables[table_index]["cached_read"] = read_buffer

    read_data_bytes = read_buffer[offset:offset+count]

    if "i" == v["type"][0]:
        if 4 == v["size"]:
            # Unpack into a long
            s = struct.unpack("L", read_data_bytes)
            return s[0]
        elif 8 == v["size"]:
            s = struct.unpack("Q", read_data_bytes)
            return s[0]
        elif 1 == v["size"]:
            s = struct.unpack("B", read_data_bytes)
            return int(s[0])
        elif 0.5 == v["size"]:
            s = struct.unpack("B", read_data_bytes)            
            if "h" == v["type"][1]:
                val = s[0] >> 4
            else:
                val = s[0]
            return (val & 0xf)
    elif "s" == v["type"]:
        return read_data_bytes
    else:
        raise sttp.STTPException("Internal error: invalid table definition")

def _invalidate_config_table_caches(table_index=None):
    for i in range(len(_all_config_tables)):
        if table_index != None and table_index != i:
            continue
        t = _all_config_tables[i]
        if t.has_key("cached_read"):
            del t["cached_read"]


def _lookup_field(name, config_table_id, stop_on_index_error=True):
    config_version = _lookup_config_table_version(config_table_id)

    # See if we are accessing an indexed item
    # For example: "ddr_entry[1]"

    close_pos = name.rfind("]")

    if (-1 != close_pos) and (len(name)-1 == close_pos):
        open_pos = name[:close_pos].rfind("[")

        if (-1 == open_pos):
            raise sttp.STTPException("Invalid field name: %s" % (name,))

        lookup = name[:open_pos] + "[]"

        if _field_defs[config_version].has_key(lookup):

            if close_pos == open_pos+1:
                #  we were passed 'field[]' (without an index)
                index = -1
            else:
                s = name[open_pos+1:close_pos]
                try:
                    index = int( s, 0 )
                except ValueError, e:
                    raise sttp.STTPException("Invalid field name: %s" % (name,))

                countName = _field_defs[config_version][lookup]["count"]
                if isinstance(countName, str):
                    count = _get_field(countName, config_table_id)
                else:
                    # assume an int value
                    count = countName

                if stop_on_index_error and index >= count:
                    max = count-1
                    if max < 0 : max = 0
                    raise sttp.STTPException("Invalid index %d for field %s (max %d)" % (index, name, max))

            if _field_defs[config_version][lookup].has_key("func"):
                func = _field_defs[config_version][lookup]["func"]
            else:
                func = _get_entry_info

            info = dict(_field_defs[config_version][lookup])
            info.update(func(lookup, index, config_table_id, config_version))
            info["base_name"] = name[:open_pos]
            return info

    # See if there is an exact match
    if _field_defs[config_version].has_key(name):
        return _field_defs[config_version][name]

    return None

#
# Get fields
#
def get_config_table_fields(fields, config_table_id=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return {}

    values = {}

    for n in fields:
        board, name  = _get_board_id(n, config_table_id)

        v = _get_field(name, board)
        if None != v:
            values[n] = v
       
    return values


def register_config_table(version, info):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    _field_defs[version] = info


def report_config_table(config_table_id=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    global _all_config_tables

    for t in _all_config_tables:        
        config_version = None
        if config_table_id and config_table_id == t["config_table_id"]:
            cid = config_table_id
        elif None == config_table_id:
            cid = t["config_table_id"]
        else:
            cid = None
        if cid:
            config_version = _get_config_table_version(cid)

            expected = _the_configuration["boards"][cid]["config_table"]["version"] 

            if expected != config_version:
                raise sttp.STTPException("Invalid config table version. Got 0x%08x, expected 0x%08x" % (config_version, expected))

            if not _field_defs.has_key(config_version):
                raise sttp.STTPException("Invalid config table version %d. Desriptors not available for this version"% (config_version,))

            # Sort the table entries based on their "seq" value
            sorted = []
            for k, v in _field_defs[config_version].iteritems():
                sorted.append([k, v["seq"]])

            sorted.sort(_table_cmp)

            # Iterate over the sorted items expanding any "array" items
            i = 0
            while 1:
                if i >= len(sorted):
                    break
                (k, x) = sorted[i]
                p = k.find("[]")
                if p != -1:
                    # an arrayed item
                    count_name = _field_defs[config_version][k]["count"]
                    count = _get_field(count_name, cid)
                    del sorted[i]
                    for j in range(count-1, -1, -1):
                        name = k[0:p+1] + str(j) + k[p+1:]
                        sorted.insert(i, (name, -1))
                    i += count
                else:
                    i += 1

            # Iterate over the sorted items
            for (k, x) in sorted:
                name = k
                value = _get_field(name, cid)
                if isinstance(value, str):
                    v = ""
                    if chr(0) in value:
                        v += "Invalid string: "

                    for c in value:
                        if chr(0) == c:
                            v += "."
                        else:
                            v += c

                    #sttp.logging.print_out("%10s %20s = %s"  % (cid, name, v))
                    s = "Config :%8s %38s = %s" % (cid, name, v,)   
                    sttp.logging.print_out(s)
                        
                elif isinstance(value, long):
                    #sttp.logging.print_out("%10s %20s = 0x%x" % (cid, name, value) )
                    s = "Config :%8s %38s = 0x%x" % (cid, name, value,)   
                    sttp.logging.print_out(s)
                elif isinstance(value, int):
                    #sttp.logging.print_out("%10s %20s = 0x%08x" % (cid, name, value) )
                    s = "Config :%8s %38s = 0x%x" % (cid, name, value,)   
                    sttp.logging.print_out(s)
                else:
                    # Undefined entry
                    # sttp.logging.print_out("%10s %20s : Undefined entry" % (cid, name,))
                    pass

def set_config_table_fields(name_values, config_table_id=None):
    global _allow_read_only_modification
    global _do_debug

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    update_field_list = {}

    for n, v in name_values.iteritems():
        board_id, name  = _get_board_id(n, config_table_id)

        field = _lookup_field(name, board_id, False)
        if None == field:
            raise sttp.STTPException("Invalid field %s" % (n,))

        if field.has_key("access") and field["access"] == "read-only" and \
           False == _allow_read_only_modification:
            raise sttp.STTPException("Field %s is read-only" % (n,))

        if field["type"] == "r":
            raise sttp.STTPException("Field %s is reserved" % (n,))

        if (field["type"]    == "s" and not isinstance(v, str)) or \
           (field["type"][0] == "i" and not isinstance(v, int) and not isinstance(v, long)):
            raise sttp.STTPException("Type mismatch for %s" % (n,))

        if field["type"] == "s" and len(v) > field["size"]:
            raise sttp.STTPException("Invalid length for %s (max %d chars)" % (n, field["size"]))

        if (field["type"][0] == "i" and field["size"]==0.5 and v>0xf) or \
           (field["type"][0] == "i" and field["size"]==1 and v>0xff) or \
           (field["type"][0] == "i" and field["size"]==2 and v>0xffff) or \
           (field["type"][0] == "i" and field["size"]==4 and v>0xffffffff):
            raise sttp.STTPException("Invalid value for %s. (max %s bytes)" % (n, field["size"]))

        if not update_field_list.has_key(board_id):
            update_field_list[board_id] = {}
        update_field_list[board_id][name] = v

    for board_id in update_field_list.keys():
        all_names = []
        version = _the_configuration["boards"][board_id]["config_table"]["version"] 
        names = _field_defs[version].keys()
        for n in names:
            item = [ n, _field_defs[version][n]["seq"] ]
            all_names.append(item)
        all_names.sort(_table_cmp)

        cid, device, table_index = _lookup_device(board_id)

        ee_mem_size = device.ee_mem.get_size()

        # copy across the ID field - need to read the version to get the cache updated
        _get_config_table_version(board_id)
        read_buffer = _all_config_tables[table_index]["cached_read"]
        write_buffer = read_buffer[0:4]

        for item in all_names:
            n = item[0]
            info = _lookup_field(n, board_id)

            value = None
            s     = None

            if _do_debug: print "Field  %s, offset %s" % (n, info["offset"])

            if info.has_key("count"):
                # This is an arrayed field with an associated count field
                count_name = info["count"]

                new_entries = []

                extra_values = 0
                count_value  = _get_field(count_name,  board_id)

                if count_name in update_field_list[board_id].keys():
                    # we have updated the count earlier in this pass
                    new_count_value = update_field_list[board_id][count_name]          
                    if new_count_value < count_value:
                        # reduce the count to copy across
                        count_value = new_count_value
                    elif new_count_value > count_value:
                        # Add some extra blank values
                        extra_values = new_count_value - count_value
                    
                # get the values from the original table
                base_name = n[0:-2]              
                for i in range(count_value):
                    full_name = "%s[%s]" % (base_name, i)
                    new_entries.append( _get_field(full_name,  board_id) )

                for i in range(extra_values):
                    new_entries.append(0)
                                
                # are we also updating individual entries in this "counted" array
                for un, uv in update_field_list[board_id].iteritems():
                    uinfo = _lookup_field(un, board_id, False)
                    if uinfo.has_key("base_name") and base_name == uinfo["base_name"]:
                        if uinfo["index"] > len(new_entries) - 1:
                            raise sttp.STTPException("Invalid index passed in: %s. Maximum entries %s" % (un, len(new_entries)) )
                        new_entries[uinfo["index"]] = uv

                # now convert to bytes
                s = ""
                for i in range(len(new_entries)):
                    full_name = "%s[%s]" % (base_name, i)
                    s += _pack_field(full_name, new_entries[i], info, 0)

                if _do_debug: print "  array %s created s len: %s " % (base_name, len(s))

            elif (n not in update_field_list[board_id].keys()):
                # copy existing data
                value = _get_field(n, board_id)
                if _do_debug: print "  Using existing value: ", value
            else:
                # write new data
                value = update_field_list[board_id][n]
                if _do_debug: print "  Using users new value: ", value

            existing_byte = 0
            if 0.5 == _field_defs[version][n]["size"]:
                # a half byte
                # did we already put in write_buffer a byte for this

                # "il" or "rl" must always be sequenced before "ih" or "rh"
                offset = info["offset"]

                if "h" == info["type"][1]:
                    # get the byte (a 0.5 sized, "l" type) and strip it from the write buffer
                    existing_byte = ord(write_buffer[-1])
                    write_buffer = write_buffer[:-1]
                    if _do_debug: print "  read existing byte from write_buffer %s, len(write_buffer) %s" % (hex(existing_byte), len(write_buffer))
                else:
                    existing_byte = ord(read_buffer[offset])
                    if _do_debug: print "  read existing byte from read_buffer: %s, offset %s" % ( hex(existing_byte), offset)

            #if s != None and info["offset"] != len(write_buffer):
            #    # Only check for fixed-offset items prior to arrayed items
            #    raise sttp.STTPException("Error in config table definition prior to item %s" % (n))

            if info["type"][0] == "r":
                if  0.5 != info["size"]:
                    # add blank bytes
                    s = chr(0) * info["size"]
                    if _do_debug: print "  type r, added chr(0) * ", info["size"]
                elif info["type"][1] == "h":
                    # put back the byte taken from write buffer
                    s = chr(existing_byte)
                    if _do_debug: print "  type r, put back: ", hex(existing_byte)
                elif info["type"][1] == "l":
                    # assume we have not modded write_buffer above - add a blank
                    s = chr(0)
                    if _do_debug: print "  type r, added blank byte"
            elif value != None:
                # If we did not create "s" by packing an arrayed item above, pack the single value
                s = _pack_field(n, value, info, existing_byte)
                if _do_debug: print "  pack field: len(s):", len(s), " size: ", info["size"]

            write_buffer += s

            if _do_debug: print "  len new write_buffer: ", len(write_buffer)
 
        if ee_mem_size != len(write_buffer):                 
            write_buffer += chr(0) * (ee_mem_size - len(write_buffer)) 

        _invalidate_config_table_caches(table_index)
        device.ee_mem.write_byte_string(0, write_buffer, ee_mem_size)                 

#
# Modepin functions
#

def _read_modepin_stored(name):
    details = _all_modepins[name]
    value = 0
    offset = 0

    # Iterate over all the devices used to make up the "group" of modepins
    for device in details["devices"]:
        ic = device["ic"]
        outputs = device["outputs"]

        # read the 6 bit value describing MUX_OUT_E ... MUX_OUT_A
        read_value = ic.ee6b.read_byte()

#        print "_read_modepin_stored: ", ic.info["name"], " read_value: ", hex(read_value)
        # build the actual value
        v = 0 
        for i in range(len(outputs)):
            if read_value & (2 ** outputs[i]):
                v |= (2 ** i) 
      
        value |= ( v << offset )
        offset += len(outputs)

    return value


def _write_modepin_stored(mp2change):
    ic_dict = {}
    #For each mode pin find the device it is on
    for name in mp2change:
        details = _all_modepins[name]
        for device in details["devices"]:
            ic_dict[device["ic"]] = 0

    # for each device that must be changed read the current data
    for dev in ic_dict:
        ic_dict[dev] = dev.ee6b.read_byte()


    #Now to work out the new values to write back
    for name in mp2change:
        value = mp2change[name]
        offset = 0
        details = _all_modepins[name]
        for device in details["devices"]:
            ic = device["ic"]
            ic_dict[device["ic"]]
            outputs = device["outputs"]
            mask = 0
            for i in range(len(outputs)):
                mask = (2 ** outputs[i]) 
                ic_dict[device["ic"]] &= ~mask
                if ((value & 1) == 1):
                    ic_dict[device["ic"]] |= mask
                value = value >> 1


    #For each device needing a change write out the new data
    for dev in ic_dict:
        dev.ee6b.write_byte( ic_dict[dev])

def get_modepins(modepins=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    if None == modepins:
        modepins = _all_modepins.keys()
        modepins.sort()
    else:
        # if a user supplied name is in the complete list prefix it with board name
        modepins = _get_modepin_list_qualified(modepins)

    current_mode_pins = {}
    # Iterate over all defined names
    for name in modepins:
        current_mode_pins[name] = _read_modepin_stored(name)
    return current_mode_pins

def get_modepin_defaults(modepins=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    if None == modepins:
        modepins = _all_modepins.keys()
        modepins.sort()
    else:
        # if a user supplied name is in the complete list prefix it with board name
        modepins = _get_modepin_list_qualified(modepins)

    default_mode_pins = {}
    # Iterate over all defined names
    for name in modepins:
        details = _all_modepins[name]
        if details.has_key("default"):
            default_mode_pins[name] = details["default"]

    return default_mode_pins


def _get_list_qualified(items, names):
    board_ids = _the_configuration["boards"].keys()
    for i in range(len(names)):
        n = names[i]
        found = 0
        for b in board_ids:
            f = b + "_" + n 
            if items.has_key(f):
                found += 1

        if found>1:
            raise sttp.STTPException("More than one board with the name %s" % (n,))
        elif 1 == found:        
            names[i] = f

    valid_names = []
    for n in names:
       if items.has_key(n):
           valid_names.append(n)
            
    return valid_names

def _get_modepin_list_qualified(names):
    return _get_list_qualified(_all_modepins, names)
        
def _get_voltages_list_qualified(names):
    global _all_voltages
    return _get_list_qualified(_all_voltages, names)
    
def _split_qualified_name(name):
    board_ids = _the_configuration["boards"].keys()
    board_name = None
    for b in board_ids:
        f = b + "_"
        if 0 == name.find(f):
            item_name = name[len(f):]
            board_name = b
            return board_name, item_name

    return None, name

def report_modepin_defaults(names=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    if None == names:
        names = _all_modepins.keys()
        names.sort()
    else:
        # if a user supplied name is in the complete list prefix it with board name
        names = _get_modepin_list_qualified(names)

    # Iterate over all defined names
    for name in names:
        details = _all_modepins[name]

        if details.has_key("default"):
            default_value = hex(details["default"])
        else:
            default_value = "undefined default"

        (board_name, pin_name) = _split_qualified_name(name)

        if details.has_key("alt_name"):
            pin_name += " (" + details["alt_name"] + ")"

        s = "Modepin:%8s %38s default = %s%10s" % (board_name, pin_name, default_value, "")   

        if "undefined default" != default_value and \
           details.has_key("descriptions") and int(default_value, 0) < len(details["descriptions"]):
            s += details["descriptions"][int(default_value, 0)]

        sttp.logging.print_out(s)

#
# Report all the modepins (sorted by name)
# unless a list of names is passed in
#

def report_modepins(names=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    if None == names:
        names = _all_modepins.keys()
        names.sort()
    else:
        # if a user supplied name is in the complete list prefix it with board name
        names = _get_modepin_list_qualified(names)
    
    # Iterate over all defined names
    for name in names:        
        details = _all_modepins[name]

        value = _read_modepin_stored(name)

        (board_name, pin_name) = _split_qualified_name(name)

        if details.has_key("alt_name"):
            pin_name += " (" + details["alt_name"] + ")"

        s = "Modepin:%8s %38s = 0x%x%10s" % (board_name, pin_name, value, "")   

        if details.has_key("descriptions") and value < len(details["descriptions"]):
            s += details["descriptions"][value]

        sttp.logging.print_out(s)

def _get_modepin_altname_key(mpboard, mpname):
    names = _all_modepins.keys()

    # Iterate over all defined names
    for name in names:        
        details = _all_modepins[name]

        (board_name, pin_name) = _split_qualified_name(name)
        if (board_name != mpboard):
            continue 
        if details.has_key("alt_name"):
            if(details["alt_name"] == mpname ):
                return name

    return None


def _check_valid_modepin_value(name, value):
    if isinstance(value, str):
        ivalue = int(value, 0)
    else:
        ivalue = value

    try:
        details = _all_modepins[name]
    except LookupError:
        raise sttp.STTPException("Invalid modepin %s" % (name))
    max = (2 ** details["width"]) -1 

    if ivalue>max or ivalue<0:
        raise sttp.STTPException("Invalid %s value %s" % (name, value))

def setup_default_modepins_with_overrides(overrides = None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    ## report_modepin_defaults()
    # Handle defaults
    default_mode_pins = get_modepin_defaults()
    my_mode_pins = default_mode_pins
    my_current_mode_pins = get_modepins()

    # Handle function param overrides
    if None == overrides:
        pass
    else:
        # if a user supplied some overrides process them
        for override_name, override_value in overrides.iteritems():

            board, name = _get_board_id(override_name, None, True)
            qname = _get_modepin_altname_key(board, name)
            if(None == qname): 
                fullname = board + "_" + name
            else:
                fullname = qname

            if not my_mode_pins.has_key(fullname):
                raise sttp.STTPException("Invalid modepin specifier %s" % (fullname))

            _check_valid_modepin_value(fullname, override_value)

            my_mode_pins[fullname] = override_value

    # Handle TargetString  overrides
    if (_the_parameters.has_key("modepins")):
        # get the list of modepins of set
        modeList = _the_parameters["modepins"].split(":")
        for m in modeList:
            if -1 == m.find(";"):
                raise sttp.STTPException("Invalid modepin specifier %s" % (m,))
            mname, mvalue = m.split(";")
            board, name = _get_board_id(mname, None, True)
            qname = _get_modepin_altname_key(board, name)
            if(None == qname): 
                fullname = board + "_" + name
            else:
                fullname = qname

            if not my_mode_pins.has_key(fullname):
                raise sttp.STTPException("Invalid modepin specifier %s" % (fullname))

            _check_valid_modepin_value(fullname, mvalue)

            my_mode_pins[fullname] = int(mvalue, 0)

    mp2change = {}
    for mp in my_mode_pins:
        if(my_mode_pins[mp] != my_current_mode_pins[mp]):
            mp2change[mp] = my_mode_pins[mp]

    if mp2change != {}:
        _write_modepin_stored(mp2change)

#
# Voltage functions
#

_a2d_init_done = {}
def _a2d_init(device):
    global _a2d_init_done

    if _a2d_init_done.has_key(device):
        return

    # Default for all channels
    device.a2d.write_byte_array(0x6, 0x00)
    device.a2d.write_byte_array(0x7, 0x00)
    device.a2d.write_byte_array(0x8, 0x00)

    _a2d_init_done[device] = True


def _read_voltage(device, v):

#    print >> sys.stderr, ">>> _read_voltage( device %s, v %s)" % (device, v)
 
    _a2d_init(device)
    
    enable = (1 << (v-1)/2) << 4      # 0xf8

#    print >> sys.stderr, "enable 0x%08x" % (enable,)

    device.a2d.write_byte_array(0x1, enable) # enable v1 ... v7

    end_time = (time.time() * 1000) + _i2c_timeout
    while 1:
        offset = 0x0
        rdata = device.a2d.read_byte_array(offset, 2)
#        print >> sys.stderr, "    ready 0x%08x, channel enable 0x%08x\n" % (rdata[0], rdata[1])

        if 0 != rdata[0] & (1 << (v-1)):
            break

        if (time.time() * 1000) > end_time:
            raise sttp.STTPException("Failed to read i2c a2d within timeout of %s ms" % (_i2c_timeout,))       

    offset = (2 * (v-1)) + 0xa
    vdata = device.a2d.read_byte_array(offset, 2)

    if(vdata[0] & 0x40 ):
        sign = -1
    else:
        sign = 1
    val_i = (( vdata[0] & 0x3F ) << 8 ) + vdata[1]
    val   = float(sign) * float(val_i) * 305.18e-6

#    print >> sys.stderr, "<<< _read_voltage() = sign %s, val_i %s" % (sign, val_i )

    return val

def get_voltages(namelist=None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return 0

    values = {}

    if namelist == None:
        namelist = _all_voltages.keys()
    else:
        namelist = _get_voltages_list_qualified(namelist)

    for voltage in namelist:
        info = _all_voltages[voltage] 
        ic = info["device"]["ic"]
        input = info["device"]["input"]
        value = _read_voltage(ic, input+1)
        values[voltage] = value 
    
    return values

def report_voltages_defaults(boardid = None, namelist = None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    global _all_voltages
    if namelist == None:
        namelist = _all_voltages.keys()
    else:
        namelist = _get_voltages_list_qualified(namelist)
    namelist.sort()
    for voltage in namelist:
        info = _all_voltages[voltage] 
        nominal = info["nominal"]
        (board_name, v_name) = _split_qualified_name(voltage)

        s = "Voltage:%8s %38s default = %g" % (board_name, v_name, nominal)   

        sttp.logging.print_out(s)
   
def report_voltages(boardid = None, namelist = None):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    global _all_voltages
    if namelist == None:
        namelist = _all_voltages.keys()
    else:
        namelist = _get_voltages_list_qualified(namelist)
    namelist.sort()
    for voltage in namelist:
        info = _all_voltages[voltage] 
        ic = info["device"]["ic"]
        input = info["device"]["input"]
        value = _read_voltage(ic, input+1)
        (board_name, v_name) = _split_qualified_name(voltage)
        s = "Voltage:%8s %38s = %g" % (board_name, v_name, value)   

        sttp.logging.print_out(s)

def setup_default_voltages_with_overrides(name_values=None, store=True):
    global _all_voltages

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return
    targetstring_voltages =  {}
    # Handle TargetString  overrides
    if (_the_parameters.has_key("voltages")):
        voltageList = _the_parameters["voltages"].split(":")
        for v in voltageList:
            if -1 == v.find(";"):
                raise sttp.STTPException("Error: Invalid voltage specifier %s" % (v,))
            vname, vvalue = v.split(";")
            board, name = _get_board_id(vname, None, True)
            fullname = board + "_" + name
            targetstring_voltages[fullname] =  float(vvalue)

    # Argument overrides
    overrides = {}
    if name_values != None:
        for a, b in name_values.iteritems():
            board, name = _get_board_id(a, None, True)
            fullname = board + "_" + name
            overrides[fullname] = b

    for v in targetstring_voltages.keys() + overrides.keys():
        if v not in _all_voltages.keys():
            raise sttp.STTPException("Error: Invalid voltage specifier %s" % (v,))
        

    for voltage in _all_voltages.keys():
        info = _all_voltages[voltage]         

        if info.has_key("access") and info["access"] == "read-only" and \
           False == _allow_read_only_modification and \
           (voltage in targetstring_voltages.keys() or voltage in overrides.keys()) and \
           not (_the_parameters.has_key("no_voltage_error") and int(_the_parameters["no_voltage_error"])>0):
            raise sttp.STTPException("Error: Cannot modify read-only voltage %s" % (voltage,))

        if not info.has_key("set_function"):
            continue

        if voltage in targetstring_voltages.keys():
            value = targetstring_voltages[voltage]
        elif voltage in overrides.keys():
            value = overrides[voltage]
        else:
            value = info["nominal"]

        info["store"] = store
        info["set_value"] = value
        info["maximum_validate"] = _voltages_maximum_validate

        # And make the call
        info["set_function"]( voltage, info)

def _get_resistor_info(ic, instance):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return 0

    resistor_info = ic.get_resistor_info(instance)

    return resistor_info
    
def _set_resistor_non_volatile(ic, instance, data):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return 0

    ic.set_resistor_non_volatile(instance, data, 1)

def _set_resistor_volatile(ic, instance, data):
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return 0

    ic.set_resistor_volatile(instance, data, 1)


def report_resistors():
    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    # TODO once we define the structure

    #global _all_resistors
    #for device in _all_resistors:
    #    ic = device["ic"]
    #    for i in range(2):
    #        v = _get_resistor_memory(ic, i)
    #        sttp.logging.print_out("VR %12s:%d = 0x%02x" % (ic.info["name"], i, v) )
    #        _get_resistor_info(ic, i)


def handle_report_and_setup_options(do_exit=True):
    global _do_default_modepin_setup
    global _do_default_voltage_setup

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    modepins_set = False
    voltages_set = False

    if (_the_parameters.has_key("board_report") and (_the_parameters["board_report"] == "1" )):
        report_config_table()
        report_modepins()
        report_voltages()
        if do_exit: sys.exit(0)
    if (_the_parameters.has_key("board_report_defaults") and (_the_parameters["board_report_defaults"] == "1" )):
        report_modepin_defaults()
        report_voltages_defaults()
        if do_exit: sys.exit(0)
    if (_the_parameters.has_key("board_restore_defaults") and (_the_parameters["board_restore_defaults"] == "modepins" )):
        setup_default_modepins_with_overrides()
        modepins_set = True
        sttp.logging.print_out("Completed default modepin restore after board_restore_defaults=modepins was specified")
        if do_exit: sys.exit(0)
    elif (_the_parameters.has_key("board_restore_defaults") and (_the_parameters["board_restore_defaults"] == "voltages" )):
        setup_default_voltages_with_overrides()
        voltages_set = True
        sttp.logging.print_out("Completed default voltage restore after board_restore_defaults=voltages was specified")
        if do_exit: sys.exit(0)
    elif (_the_parameters.has_key("board_restore_defaults") and (_the_parameters["board_restore_defaults"] == "1" )):
        setup_default_modepins_with_overrides()
        setup_default_voltages_with_overrides()
        modepins_set = True
        voltages_set = True
        sttp.logging.print_out("Completed default restore after board_restore_defaults=1 was specified")
        if do_exit: sys.exit(0)

    # Allow user to prevent setting of default modepin values
    if (_the_parameters.has_key("no_modepin_change") and (_the_parameters["no_modepin_change"] == "1" )):
        _do_default_modepin_setup = False

    if (_the_parameters.has_key("no_voltage_change") and (_the_parameters["no_voltage_change"] == "1" )):
        _do_default_voltage_setup = False

    # Setup default modepins values and override with any TargetString overrides
    if _do_default_modepin_setup and not modepins_set:
        setup_default_modepins_with_overrides()
    
    # Setup default voltage values and override with any TargetString overrides
    if _do_default_voltage_setup and not voltages_set:
        setup_default_voltages_with_overrides()
        

#
# Initialization
#

def _check_type_i_code(stop_on_err):
    # This is called from stmc.check_convertor_code

    global _the_configuration

    for board, info in _the_configuration["boards"].iteritems():

        config_version = _get_config_table_version(board, False)
        expected = _the_configuration["boards"][board]["config_table"]["version"] 

        if expected != config_version:
            msg = "Invalid configuration table version on board %s: Expected 0x%08x. Got 0x%08x." % (board, expected, config_version)
            if stop_on_err:
                raise sttp.STTPException(msg)
            else:
                sttp.logging.print_out(msg)


def _get_config_table_version(config_table_id=None, generate_error=True):
    global _field_defs
    f = _get_field("config_table_version", config_table_id)

    if generate_error and (f not in _field_defs.keys()):
        raise sttp.STTPException("Invalid config table version %s read from board %s" % (f, config_table_id))

    return f

def get_configuration():
    global _the_configuration
    return _the_configuration


def _modepin_sanity():
    modepins = _all_modepins.keys()
    # Check that the width and total outputs defined are consistent
    for name in modepins:
        details = _all_modepins[name]
        bits = details["width"]
        total_bits_defined = 0
        for device in details["devices"]:
            total_bits_defined += len(device["outputs"])

        if bits != total_bits_defined :
            raise sttp.STTPException("Invalid mode pin width definition for %s: soc width definition %s, board width definition %s " % (name, bits, total_bits_defined))

    # Check for the same bit defined for different purposes
    ic_dict = {}
    total_bits_defined = 0
    total_bits_used = 0
    dodgy_dict = {}
    #For each mode pin find the device it is on
    for name in modepins:
        details = _all_modepins[name]
        for device in details["devices"]:
            ic_dict[device["ic"]] = 0

    #Now to work out if a particular bit is defined more than once 
    for name in modepins:
        details = _all_modepins[name]
        bits = details["width"]
        value = (2 ** bits) - 1
        offset = 0
        for device in details["devices"]:
            ic = device["ic"]
            outputs = device["outputs"]
            total_bits_defined += len(outputs)
            mask = 0
            for i in range(len(outputs)):
                mask = (2 ** outputs[i]) 
                if ((ic_dict[device["ic"]] & mask) != 0 ):
                    dodgy_dict[device["ic"]] = mask-1
                if ((value & 1) == 1):
                    ic_dict[device["ic"]] |= mask
                value = value >> 1

    for b in ic_dict:
        data = ic_dict[b]
        for i in range(5):
            if ((data & 1) == 1):
                total_bits_used += 1
            data = data >> 1

    if(total_bits_defined != total_bits_used):
        name = ""
        output = ""
        for dodgy in dodgy_dict:
            for name in modepins:
                details = _all_modepins[name]
                for device in details["devices"]:
                    if dodgy == device["ic"]:
                        outputs = device["outputs"]
                        for i in range(len(outputs)):
                            if dodgy_dict[dodgy] == outputs[i]:
                                output = outputs[i]
        raise sttp.STTPException("Duplicate mode pin definition found %s on %s:%s" % (name, device["ic"].info["name"], output) )


def init(configuration, parameters):
    global _all_config_tables
    global _all_modepins
    global _all_resistors
    global _all_voltages
    global _do_debug
    global _i2c_timeout
    global _the_configuration
    global _the_parameters

    if sttp.stmc.get_operating_mode() == "ROMGEN":
        return

    _the_configuration = configuration
    _the_parameters = parameters

    if parameters.has_key("targetpack_debug"):
        debug_list = parameters["targetpack_debug"].split(":")
        if "board_i2c" in debug_list:
            _set_debug(True)

    if parameters.has_key("i2c_timeout"):
        _i2c_timeout = int(parameters["i2c_timeout"])

    # Get the configuration tables for each board
    for a, b in configuration["boards"].iteritems():
         if isinstance(b, dict):
             for c, d in b.iteritems():
                 if "config_table" == c:
                     config_table = d
                     # add a unique identifier to the config table (typically the board name)
                     config_table["config_table_id"] = a
                     _all_config_tables.append(config_table)

    sttp.targetinfo.set_target_info("stmc_type_i_callback", _check_type_i_code)

    i2c_root = configuration["i2c_root"]

    # Detect check for devices here - is this the right place?
    def do_detect(root):
        for k, v in root.__dict__.iteritems():
            if isinstance(v, i2c.I2C) and ("parent" != k):
                if "detect" in dir(v):
                    present = v.detect()
                    if False == present:
                        raise sttp.STTPException("Cannot detect device: %s" % (k,))
                do_detect(v)

    if not parameters.has_key("no_i2c_detect"):             
        do_detect(i2c_root)

    # Get all I2C defined modepins
    for a, b in configuration["boards"].iteritems():
         if isinstance(b, dict):
             for c, d in b.iteritems():
                 if "modepin_connections" == c:
                     modepin_connections = d
                     for j, k in modepin_connections.iteritems():
                         name = a + "_" + j
                         _all_modepins[name] = k
   
    soc_modepins = []

    # Associate  SoC modepins with I2C devices 
    # Get a log of all the SoC modepins - TODO - Multiple SoCs
    for a, b in configuration["boards"].iteritems():
         if isinstance(b, dict):
             for c, d in b.iteritems():
                 if "modepins" == c:
                     modepins = d

                     for j, k in modepins.iteritems():
                         name = a + "_" + j                       
                         soc_modepins.append(name)
                         if _all_modepins.has_key(name) and ((False == k.has_key("internal")) or (False==k["internal"])):
                             _all_modepins[name].update(k)                        
                             # Tell the DBU code not to allow control of this modepin with "modepins=<>"
                             # must add the SoC modepin name and the board-qualified name
                             dbu.add_invalid_modepin(j)
                             dbu.add_invalid_modepin(name)

    # Ensure the any fully qualified board mode pin matches a fully qualified soc modepin
    for k in _all_modepins.keys():
        if k not in soc_modepins:
             if _do_debug: print "checking k:", k
             if _do_debug: print "soc_modepins:", soc_modepins
             raise sttp.STTPException('Invalid modepin %s - not defined in SoC or defined as "internal"' % (k,))


    # setup voltages
    for a, b in configuration["boards"].iteritems():
         if isinstance(b, dict):
             for c, d in b.iteritems():
                 if "voltages" == c:
                     voltages = d
                     for j, k in voltages.iteritems():
                         name = a + "_" + j
                         _all_voltages[name] = k


    # setup resitors
    for a, b in configuration["boards"].iteritems():
         if isinstance(b, dict):
             for c, d in b.iteritems():
                 if "resistors" == c:                     
                     _all_resistors = d

    # Test that mode pin definitions are sane
    _modepin_sanity()
    
