###
### STxh410 PIO toggle function
### (C) R. Singh, F. Charpentier
###

import sttp

# STxh410 PIO reminder
#  1: PIO_1 (SBC) => currently supported


# Supports only few PIO's at present
pios = [
    {"desc":""},
    {
    "desc"      : "PIO_1(SBC)",
    "base"      : 0x09611000,
    },
    {
    "desc"      : "PIO_2(SBC)",
    "base"      : 0x09612000,
    },
    {
    "desc"      : "PIO_3(SBC)",
    "base"      : 0x09613000,
    },
    {
    "desc"      : "PIO_4(SBC)",
    "base"      : 0x09614000,
    },
]

max_pio = 4
pio_base = 0   # base addr of PIO in use

def toggle(pio_nb, bit, count):

    global pio_base

    if (pio_nb > max_pio):
        sttp.logging.print_out("\n toggle_pio() ERROR: Unexpected PIO number")
        return -1

    desc = pios[pio_nb]["desc"]
    if (desc == ""):
        sttp.logging.print_out("\n toggle_pio() ERROR: Unsupported PIO number")
        return -1

    sttp.logging.print_out(" \n Toggling PIO%d[%s] for %d times \r\n" % (pio_nb, bit, count))

    pio_base = pios[pio_nb]["base"]

    #PC2-PC1-PC0 = 0-1-0 => output push pull
    sttp.pokepeek.poke(pio_base + 0x28, 1 << bit) #PC0 reset
    sttp.pokepeek.poke(pio_base + 0x34, 1 << bit) #PC1 set
    sttp.pokepeek.poke(pio_base + 0x48, 1 << bit) #PC2 reset

    # count =0 (pio-OFF) , =1 (pio-ON) , toggle count starts from 2.
    if (count == 0):
        sttp.pokepeek.poke(pio_base + 0x08, 1 << bit) #Pout reset
    elif (count == 1):
        sttp.pokepeek.poke(pio_base + 0x04, 1 << bit) #Pout set
    elif (count > 1):
        i = 0
        while(i < count):
            sttp.pokepeek.poke(pio_base + 0x08, 1 << bit) #Pout reset
            sttp.stmc.delay(1000)  #wait 1mS
            sttp.pokepeek.poke(pio_base + 0x04, 1 << bit) #Pout set
            sttp.stmc.delay(1000)  #wait 1mS
            i = i + 1

    #sttp.stmc.delay(3000) # if required add 3ms of delay buffer b/w each toggle point