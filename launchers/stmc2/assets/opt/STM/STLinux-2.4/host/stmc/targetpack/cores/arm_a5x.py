import time
import sttp
import sttp.stmc
import tap.jtag
import coresight
import core_utils

ReadMemAPLocation = coresight.ReadMemAPLocation
WriteMemAPLocation = coresight.WriteMemAPLocation
ReadWriteMemAP = coresight.ReadWriteMemAP

def init_arm_debug(parameters, core):
    cp = sttp.stmc.get_core_params(core)

    coresight.power_up_debug(cp)

    coresight.dump_arm_dp_info(cp)
    memap = int(cp["coresight_memap"], 0)
    coresight.dump_arm_mem_ap_info(cp, memap)
    #stxd325_check_state(cp)

def stxd325_rom_table_discovery(parameters, core):

    cp = sttp.stmc.get_core_params(core)
    coresight_memap = int(cp["coresight_memap"], 0)

    ROM_BASE = (ReadWriteMemAP(cp, coresight_memap, coresight.JTAG_MEM_AP_BASE_0, 0, coresight.JTAG_READ) & ~0x03)

# MAIN ROM TABLE
    sttp.logging.print_out("\nMain ROM Table discovery")

    r = 0xffffffff
    index = 0
    # find out all entry tables
    while (r !=0):
       r = ReadMemAPLocation(cp, coresight_memap, ROM_BASE +(index*0x04))
       sttp.logging.print_out("Main ROM Table entry %.2d: 0x%08x" % (index, r))
       index +=1

    sttp.logging.print_out("\nSOC Coresight elements discovery")
    for i in range(index-5):
       r = ReadMemAPLocation(cp, coresight_memap, ROM_BASE +(i*0x04))
       table_entry = ( r & 0xfffffffc ) + ROM_BASE
       r = ReadMemAPLocation(cp, coresight_memap, table_entry + 0xfcc)
       sttp.logging.print_out("--> DEVTYPE at address 0x%08x is 0x%08x" %(table_entry, r,))

# This discovery algorithm knows(!) about the location for A57 (entry 15) and A53 (entry 16) cluster sub-tables

    r = ReadMemAPLocation(cp, coresight_memap, ROM_BASE +(16*0x04))
    A53_CLUSTER_BASE = ROM_BASE + ( r & 0xfffffffc )


# **********************************************************************************
# A53 CLUSTERS ROM TABLE
# **********************************************************************************
    sttp.logging.print_out("\nA53 ROM Table discovery")
    sttp.logging.print_out("CLUSTER_A53 ROM Table address: 0x%08x" % A53_CLUSTER_BASE)

    r = 0xffffffff
    index = 0

    while (r !=0):
       r = ReadMemAPLocation(cp, coresight_memap, A53_CLUSTER_BASE +(index*0x04))
       sttp.logging.print_out("Main A53 ROM Table entry %.2d: 0x%08x" % (index, r))
       index +=1

    sttp.logging.print_out("\nA53 CLUSTER Coresight elements discovery")
    for i in range(index-1):
       r = ReadMemAPLocation(cp, coresight_memap, A53_CLUSTER_BASE +(i*0x04))
       table_entry = ( r & 0xfffffffc ) + A53_CLUSTER_BASE
       r = ReadMemAPLocation(cp, coresight_memap, table_entry + 0xfcc)
       sttp.logging.print_out("--> DEVTYPE at address 0x%08x is 0x%08x" %(table_entry, r,))

# This discovery algorithm knows about the location for A53 sub-systems (entry 3) sub-tables

    r = ReadMemAPLocation(cp, coresight_memap, A53_CLUSTER_BASE +(3*0x04))
    A53_SS_BASE = A53_CLUSTER_BASE + ( r & 0xfffffffc )
    # sttp.logging.print_out("A53_SS ROM Table address: 0x%08x" % A53_SS_BASE)

# A53 SUB-SYSTEM ROM TABLE
    sttp.logging.print_out("\nA53_SS ROM Table discovery")

    r = 0xffffffff
    index = 0

    while (r !=0):
       r = ReadMemAPLocation(cp, coresight_memap, A53_SS_BASE +(index*0x04))
       sttp.logging.print_out("Sub A53 ROM Table entry %.2d: 0x%08x" % (index, r))
       index +=1

    sttp.logging.print_out("\nA53_SS Coresight elements discovery")

    for i in range(index-1):
       r = ReadMemAPLocation(cp, coresight_memap, A53_SS_BASE +(i*0x04))
       table_entry = ( r & 0xfffffffc ) + A53_SS_BASE
       r = ReadMemAPLocation(cp, coresight_memap, table_entry + 0xfcc)
       sttp.logging.print_out("--> DEVTYPE at address 0x%08x is 0x%08x" %(table_entry, r,))

# **********************************************************************************
# DCC operation check
# **********************************************************************************

    debug_base = int(cp["coresight_debug"], 0)
    sttp.logging.print_out("coresight_debug base: 0x%08x" % debug_base)

    # Access to some CPU internal registers
    DBGAUTHSTATUS = debug_base + 0xfb8
    r = ReadMemAPLocation(cp, coresight_memap, DBGAUTHSTATUS)
    sttp.logging.print_out("\nDBGAUTHSTATUS: 0x%08x" % r)
    if (r & 0xff) == 0xff:
        sttp.logging.print_out("  --> Both invasive and secure debug are authorized")

    DBGEDPRSR = debug_base + 0x314
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDPRSR)
    sttp.logging.print_out("DBGEDPRSR init value: 0x%08x" % r)

    # Need to unlock the OS lock bit to access all the registers
    DBGOSLAR = debug_base + 0x300
    WriteMemAPLocation(cp, coresight_memap, DBGOSLAR, ~0xC5ACCE55)

    DBGEDPRSR = debug_base + 0x314
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDPRSR)
    sttp.logging.print_out("DBGEDPRSR  new value: 0x%08x" % r)
    if (r & 0x61) == 0x01:
        sttp.logging.print_out("  --> OSLK and DLK are both cleared; Power is up")

    EDLSR = debug_base + 0xfb4
    r = ReadMemAPLocation(cp, coresight_memap, EDLSR)
    sttp.logging.print_out("EDLSR value: 0x%08x" % r)

    DBGEDSCR = debug_base + 0x088
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDSCR)
    sttp.logging.print_out("DBGEDSCR: 0x%08x" % r)
    if (r & 0x13) == 0x13:
        sttp.logging.print_out("  --> External Debug Request received")

    # Clear all sticky bits if any
    EDRCR = debug_base + 0x090
    WriteMemAPLocation(cp, coresight_memap, EDRCR, 0x0c)
    # check error status
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDSCR)
    if (r & 0x1E000040) != 0:
       sttp.logging.print_out("DBGEDSCR sticky bits not all cleared: 0x%08x" % r)

    return
    
    # Next lines are for Aarch32 only
    # Write a value in R0 and read it back to verify the Debug Comms Channel
    EDITR       = debug_base + 0x084
    DBGDTRRXint = debug_base + 0x080
    DBGDTRTXint = debug_base + 0x08C
    DCCValue    = 0xdeadc0de
    # Store value in DCC input buffer
    WriteMemAPLocation(cp, coresight_memap, DBGDTRRXint, DCCValue)

    # Perform CP14 instruction to write into R0: MRC 14, 0, r0, cr0, cr5, {0}
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0e15ee10) # DBRTRRXint -> R0

    # Perform CP14 instruction to read from R0:  MCR 14, 0, r0, cr0, cr5, {0}
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0e15ee00)  # R0 -> DBGDTRTXint
    r = ReadMemAPLocation(cp, coresight_memap, DBGDTRTXint)
    if r == DCCValue:
       sttp.logging.print_out("DCC verified ok\n")
    else:
       raise sttp.STTPException("  DCC error: read back 0x%08x" % r)

    # check various error flags
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDSCR)
    if (r & 0x7D000040) != 0x01000000:
       sttp.logging.print_out("DCC error - DBGEDSCR 0x%08x" % r)

    # Check the function arm_core_WriteCoProcessor(0, 15, 1, 0, 0, 2, 0x00f00000)
    # cp=15    crn=1   op1=0   crm=0   op2=2
    WriteMemAPLocation(cp, coresight_memap, DBGDTRRXint, 0x00f00000)

    # ARM_INST_MRC_DBGDTRRX(0) = 0xee100e15  /* mrc DBGDTRRX , R0 */
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0e15ee10)   # DBGDTRRXint -> R0
    sttp.logging.print_out("DBGEDSCR: 0x%08x" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check TXfull

    # ARM_INST_DSB   = 0xf3bf8f4f dsb 32bit aarch
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x8f4ff3bf )
    sttp.logging.print_out("DBGEDSCR: 0x%08x" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check errors

    # ARM_INST_MCR_PATCH_REG(ARM_INST_MCR(0), cp, op1, crn, crm, op2) = 0xee010f50
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0f50ee01)   # R0 -> CPACR
    sttp.logging.print_out("DBGEDSCR: 0x%08x" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check errors

    # ARM_INST_ISB   = 0xf3bf8f6f  isb 32bit aarch
    #WriteMemAPLocation(cp, coresight_memap, EDITR, 0x8f6ff3bf)
    sttp.logging.print_out("DBGEDSCR: 0x%08x\n" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check errors

    # Read DFSR register
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0f10ee15)   # DFSR -> R0
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0e15ee00)   # R0 -> DBGDTRTX
    dfsr = ReadMemAPLocation(cp, coresight_memap, DBGDTRTXint)
    sttp.logging.print_out("DFSR: 0x%08x" % dfsr)
    sttp.logging.print_out("DBGEDSCR: 0x%08x\n" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check errors

    # Read IFSR register
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0f30ee15)   # IFSR -> R0
    WriteMemAPLocation(cp, coresight_memap, EDITR, 0x0e15ee00)   # R0 -> DBGDTRTX
    ifsr = ReadMemAPLocation(cp, coresight_memap, DBGDTRTXint)
    sttp.logging.print_out("IFSR: 0x%08x" % ifsr)
    sttp.logging.print_out("DBGEDSCR: 0x%08x\n" % ReadMemAPLocation(cp, coresight_memap, DBGEDSCR))    # check errors

core_info = {}

def init(arm_core_info, parameters):
    sttp_root = sttp.targetpack.get_tree()

    arm_core_name = arm_core_info["arm_core_name"]
    if arm_core_info.has_key("index_prefix"):
        arm_core_name_base = arm_core_name + arm_core_info["index_prefix"]
    arm_cluster_count = arm_core_info["arm_cluster_count"]

#    def find_cpu(root, name):
#        for c in root.get_children():
#            if type(c) == sttp.targetpack.Cpu and str(c.parent.name) == str(name):
#                return c
#            if type(c) == sttp.targetpack.Component:
#                r = find_cpu(c, name)
#                if r != None:
#                    return r
#        return None

    arm_core = core_utils.find_cpu(sttp_root, arm_core_name)
    if arm_core:
        container = arm_core.parent.parent
    else:
        raise sttp.STTPException("Failed to find cpu name %s" % (arm_core_name,))

    coresight_amba_apb = None
    coresight_cti_base = [None] * arm_cluster_count
    coresight_debug_base = [None] * arm_cluster_count

    # Iterate of all components in the "container" which may be the "soc"
    for d in container.__dict__:
        if (None == coresight_amba_apb) and (0 == d.find("coresight_amba_apb")):
            e = eval("container." + d)
            coresight_amba_apb = str(e.parameters["memap"])

        for i in range(arm_cluster_count):
            if (None == coresight_cti_base[i] and (0 == d.find("coresight_cti_c" + str(i)))):
                e = eval("container." + d)
                coresight_cti_base[i] = str(e.parameters["APB_baseAddress"])

            if (None == coresight_debug_base[i] and (0 == d.find("coresight_debug_c" + str(i)))):
                e = eval("container." + d)
                coresight_debug_base[i] = str(e.parameters["APB_baseAddress"])

    if None == coresight_amba_apb:
        raise sttp.STTPException("TargetPack error - component coresight_amba_apb not defined")
    for i in range(arm_cluster_count):
        if None == coresight_cti_base[i]:
            raise sttp.STTPException("TargetPack error - component coresight_cti_c%d not defined" % (i,))
        if None == coresight_debug_base[i]:
            raise sttp.STTPException("TargetPack error - component coresight_debug_c%d not defined" % (i,))

    names = list()
    names.append(arm_core_name)
    for i in range(arm_cluster_count):
        names.append(arm_core_name_base + str(i))

    tp = sttp.stmc.get_target_params()
    for c in names:
        cp = sttp.stmc.get_core_params(c)

        jtag_timeout = None
        if parameters.has_key("jtag_timeout"):
            #  Pick up jtag_timeout passed as TargetString parameter
            jtag_timeout = str(parameters["jtag_timeout"])
        elif tp.has_key("jtag_timeout"):
            # Pick up jtag_timeout if specified in TargetPack top-level XML
            jtag_timeout = tp["jtag_timeout"]

        if jtag_timeout:
            # If the value has not been passed as stmc_core_param_target_timeout set the core param
            if not cp.has_key("target_timeout"):
                cp["target_timeout"] = jtag_timeout
            if not cp.has_key("jtag_timeout"):
                cp["jtag_timeout"]   = jtag_timeout
            cp["linktimeout"] = jtag_timeout

        if parameters.has_key("smp_mode"):
            cp["smp_mode"] = parameters["smp_mode"]

        cp["coresight_memap"] = str(coresight_amba_apb)
        if arm_core_name==c:
            cp["coresight_debug"] = str(coresight_debug_base[0])
            cp["coresight_cti"] = str(coresight_cti_base[0])
        else:
            cp["coresight_debug"] = str(coresight_debug_base[int(c[len(arm_core_name_base):])])
            cp["coresight_cti"] = str(coresight_cti_base[int(c[len(arm_core_name_base):])])

        if arm_core_name==c or '0'==c[len(arm_core_name_base):]:
            for i in range(arm_cluster_count):
                cp["coresight_debug_c" + str(i)] = str(coresight_debug_base[i])
                cp["coresight_cti_c" + str(i)] = str(coresight_cti_base[i])

        sttp.stmc.set_core_params(c, cp)


def stxd325_check_state(cp):

    coresight_memap = int(cp["coresight_memap"], 0)

    debug_base = int(cp["coresight_debug"], 0)
    sttp.logging.print_out("coresight_debug base: 0x%08x" % debug_base)

    # Access to some CPU internal registers
    DBGEDPRSR = debug_base + 0x314
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDPRSR)
    sttp.logging.print_out("DBGEDPRSR  new value: 0x%08x" % r)
    if (r & 0x61) == 0x01:
        sttp.logging.print_out("  --> OSLK and DLK are both cleared; Power is up")

    DBGAUTHSTATUS = debug_base + 0xfb8
    r = ReadMemAPLocation(cp, coresight_memap, DBGAUTHSTATUS)
    sttp.logging.print_out("\nDBGAUTHSTATUS: 0x%08x" % r)
    if (r & 0xff) == 0xff:
        sttp.logging.print_out("  --> Both invasive and secure debug are authorized")

    EDLSR = debug_base + 0xfb4
    r = ReadMemAPLocation(cp, coresight_memap, EDLSR)
    sttp.logging.print_out("EDLSR value: 0x%08x" % r)

    DBGEDSCR = debug_base + 0x088
    r = ReadMemAPLocation(cp, coresight_memap, DBGEDSCR)
    sttp.logging.print_out("DBGEDSCR: 0x%08x" % r)
    if (r & 0x13) == 0x13:
        sttp.logging.print_out("  --> External Debug Request received")

def boot_a5x(coreList):
    for c in ("a5x", "a5x_0", "a5x_1", "a5x_2", "a5x_3", "a5x_4"):
        if c not in coreList:
            continue

        # When SMP_MODE=0, all the A53 cores must be kept in reset but the active one
        if parameters.has_key("smp_mode") and (0 == int(parameters["smp_mode"])):
            # Enforce all reset for A53 CPUs through their SSC registers
            sttp.pokepeek.poke(0x06E50108, 0x040)
            sttp.pokepeek.poke(0x06E50118, 0x040)
            sttp.pokepeek.poke(0x06E50128, 0x040)
            sttp.pokepeek.poke(0x06E50138, 0x040)
            # !!!! Next line to be commented out when the A53MP1 will be present on chip  !!!!
            # sttp.pokepeek.poke(0x06E40108, 0x040)
            
        # Release the whole SOC CoreSight structure from reset
        ARM_Debug_Enable_bitfield |= ( 0x01 << 24)

        # All the A5x cores should have their DBGREQ signal asserted prior to releasing their reset
        ARM_Debug_Enable_bitfield |= (( 0x01 << 17) | ( 0x01 << 20) | ( 0x01 << 21)| ( 0x01 << 22)| ( 0x01 << 23))
        sttp.pokepeek.poke(dbu_sa_dbgen, ARM_Debug_Enable_bitfield)

        
    # A53MP4 cluster management
    for c in ("a5x", "a5x_0", "a5x_1", "a5x_2", "a5x_3"):
        if c not in coreList:
            continue

        if parameters.has_key("smp_mode") and (1 == int(parameters["smp_mode"])) and (c != "a5x"):
           raise sttp.STTPException(" In SMP mode, the only legal core name that can be specified by the user in the TargetString is a5x\n")

        # Perform an access to the sub-system to ensure it is up and running
        A53MP4_SSC_REVISION_OFFSET = 0x06E503F0
        dbu_read = sttp.pokepeek.peek(A53MP4_SSC_REVISION_OFFSET)
        if (dbu_read != 0x10013A02):
            raise sttp.STTPException("  Cannot read A53MP4_SSC_REVISION_OFFSET register:  Expected 0x10013a02, got 0x%08x" % (dbu_read))

        debug_print(cores_debug,"  ENTRY: check A5X_Debug_Enable_bitfield: 0x%x" %ARM_Debug_Enable_bitfield)

        # Release the A53MP4 cluster CoreSight structure from reset
        ARM_Debug_Enable_bitfield |= ( 0x01 << 19 )
        sttp.pokepeek.poke(dbu_sa_dbgen, ARM_Debug_Enable_bitfield)

        debug_print(cores_debug,"  Releasing ARM cores %s from reset" % (c,) )

        # Release only the active one(s) - Skipped over in SMP mode as the core is identified as "a5x"
        if "a5x_0" == c:
            sttp.pokepeek.poke(0x06E50104, 0x040)
        if "a5x_1" == c:
            sttp.pokepeek.poke(0x06E50114, 0x040)
        if "a5x_2" == c:
            sttp.pokepeek.poke(0x06E50124, 0x040)
        if "a5x_3" == c:
            sttp.pokepeek.poke(0x06E50134, 0x040)

        systemcfg = sttp_root.stxd325.A5X_SS.syscfg_backbone
        systemcfg.A53MP4_HARDRESET.SOFT_RST_N.write(1)
        sttp.stmc.delay(1000)

        debug_print(cores_debug,"  --> Checking back A53MP4 SYSCONF = 0x%x" %systemcfg.A53MP4_HARDRESET.read())

        debug_print(cores_debug,"  Deassert debug request signals on core %s" % (c,) )
        sttp.pokepeek.poke(dbu_sa_dbgen, ARM_Debug_Enable_bitfield & ~(( 0x01 << 20) | ( 0x01 << 21)| ( 0x01 << 22)| ( 0x01 << 23)))

        debug_print(cores_debug,"  EXIT: check A5X_Debug_Enable_bitfield: 0x%x" %ARM_Debug_Enable_bitfield)
        debug_print(cores_debug,"  Done startup %s with smp_mode=%d" % (c, int(parameters["smp_mode"])) )

        
    # Specifics for A53MP1
    if "a5x_4" in coreList:

        # Perform an access to the sub-system to ensure it is up and running
        A53MP1_SSC_REVISION_OFFSET = 0x06E403F0
        dbu_read = sttp.pokepeek.peek(A53MP1_SSC_REVISION_OFFSET)
        if (dbu_read != 0x10013A02):
            raise sttp.STTPException("  Cannot read A53MP1_SSC_REVISION_OFFSET register:  Expected 0x10013a02, got 0x%08x" % (dbu_read))

        # Release the A53MP1 CoreSight structure from reset
        debug_print(cores_debug,"  ENTRY: check A5X_Debug_Enable_bitfield: 0x%x" %ARM_Debug_Enable_bitfield)
        ARM_Debug_Enable_bitfield |= ( 0x01 << 16)
        sttp.pokepeek.poke(dbu_sa_dbgen, ARM_Debug_Enable_bitfield)

        debug_print(cores_debug,"  Releasing ARM cores %s from reset" % (c,) )

        sttp.pokepeek.poke(0x06E40104, 0x040)

        systemcfg = sttp_root.stxd325.A5X_SS.syscfg_backbone
        systemcfg.A53MP1_HARDRESET.SOFT_RST_N.write(1)
        sttp.stmc.delay(1000)

        debug_print(cores_debug,"  --> Checking back A53MP1 SYSCONF = 0x%x" %systemcfg.A53MP1_HARDRESET.read())

        # Release A53MP1 DBGREQ signal after the reset exit
        ARM_Debug_Enable_bitfield &= ~(0x01 << 17)
        sttp.pokepeek.poke(dbu_sa_dbgen, ARM_Debug_Enable_bitfield)

        debug_print(cores_debug,"  EXIT: check A5X_Debug_Enable_bitfield: 0x%x" %ARM_Debug_Enable_bitfield)
        debug_print(cores_debug,"  Done startup %s in smp_mode=%d" % (c, int(parameters["smp_mode"])) )
