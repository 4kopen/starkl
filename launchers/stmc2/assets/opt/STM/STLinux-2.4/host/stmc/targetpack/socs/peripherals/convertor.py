import sttp
import sttp.stmc
import sttp.targetpack

def check_convertor(parameters):
    root = sttp.targetpack.get_tree()

    convertor_list = []
    try:
        convertor_type_lvds = root.parameters["jtagpinout_lvds"]
        convertor_list.append(convertor_type_lvds)
    except LookupError, e:
        pass

    try:
        convertor_type = root.parameters["jtagpinout"]
        convertor_list.append(convertor_type)
        # An exception is raised if the attached convertor does not suit "convertor_type"
    except LookupError, e:
        pass

    if not (parameters.has_key("no_convertor_abort") and 1==int(parameters["no_convertor_abort"])):
        sttp.stmc.check_convertor_code(convertor_list)

