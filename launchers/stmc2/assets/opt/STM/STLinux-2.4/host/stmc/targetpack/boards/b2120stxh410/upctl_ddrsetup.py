import sttp
import upctl_ddrdbg

PoutSet     = 0x04
PoutReset   = 0x08
PC0Set      = 0x24
PC0Reset    = 0x28
PC1Set      = 0x34
PC1Reset    = 0x38
PC2Set      = 0x44
PC2Reset    = 0x48
def ns2tck(timing,bitrate):     #function to convert a timing expressed in ns to a number of clock periods
    nbthtck = timing * bitrate  #bitrate is in Mbps, so twice the clock frequency
    nbtck = int((nbthtck+1999) // 2000)     # 10e-9 * 10e6 we have to divide by 1000 the result and then by 2
    return nbtck

def selectTTABentry(table,bitrate):  #function to select the timing value in the table, corresponding to the given bitrate
    if bitrate > 1866:
        return table[0]
    elif bitrate > 1600:
        return table[1]
    elif bitrate > 1333:
        return table[2]
    elif bitrate > 1066:
        return table[3]
    elif bitrate > 800:
        return table[4]
    elif bitrate > 666:
        return table[5]
    else:
        return table[6]

def hz2trefi(freq, trefi):
    return ((10000000//freq)//trefi)


def ddr_struct_def():
    ddrparams = {
        "error"             : "",
        "on_emu"            : 0,
        "pctl"              : 0,
        "phy"               : 0,
        "BITRATE"           : 0,
        "lmi_width"         : 0,
        "lmi_IP_width"      : 0,
        "lmi_start"         : 0,
        "lmi_size"          : 0,
        "ddr_debug"         : 0,
        "IOTYPE"            : "",
        "vdd_ddr"           : 0,
        "pctlresetreg"      : 0,
        "pctlresetbit"      : 0,
        "pctlresetpol"      : 2,
        "rrmodereg"         : 0,
        "rrmodebit"         : 0,
        "rrmodevalue"       : 2,
    #Parameter related to Controller Passive Stand-by
        "retn_piobankadd"   : 0,            # Address of the Pout register that controls the bank of PIO where LMI_NOTRETENTION signal is connected
        "retn_pionumber"    : 0,            # PIO number in the bank on which LMI_NOTRETENTION signal is connected
        "retn_piotype"      : "OPEN_DRAIN", # If the PIO is connected to a pull-up to VDD_DDR => "OPEN_DRAIN", if the PIO is connected to a resistor divider bridge => "PUSH_PULL"
        "HoMStruct_Add"     : 0,            # Address in SBC RAM of the data structure containing all information needed for wake-up from suspend to RAM
        "HoMTagOffset"      : 0,            # Offset of the Tag signaling exit from stand-by in the HoM structure
        "HoMTagMask"        : 0x0FFFFFFF,   # Mask to be applied on the Tag to check its validity
        "HoMTagValid"       : 0x0CAFE000,   # Valid TAG value checked to enable the exit from stand-by functionality
        "ZQ0SR0Offset"      : 0x00000000,   # If ZQ0SR0 is saved by system before entering stand-by, put '1' on bit 31 and indicate the offset in the HoM Structure of the saved value in the LSB
        "HoMTagUpdateAND"   : 0xFFFFFFFF,   # At the end of the exit from stand-by function, the Tag may be updated, this value is ANDed with the existing value in the structure
        "HoMTagUpdateOR"    : 0x00000000,   # and this value is ORed
        "DTUAddress"        : 0x40000000,   # Address to be used for the Data training
        "MixerBankBitReg"   : 0x00000000,   # MIXER_LOW_HIGH_PRIORITY_BANKn register value corresponding to the DTUAddress, used to convert DTUAddress Physical address to Bank Row Column address
        "DataRecovery"      : 0x00000000,   # If the application data have been saved by the system before entering stand-by, put '1' on bit 31 and indicate the offset in the HoM Structure of the saved data
        "DTUPCTLAddOffset"  : 0x00000000,   # If the application fixes the DTU address, put '1' on bit 31 and indicate the offset in the HoM Structure of the address saved in DTUWACTL format
        "DTUPHYAddOffset"   : 0x00000000,   # If the application fixes the DTU address, put '1' on bit 31 and indicate the offset in the HoM Structure of the address saved in DTAR0 format
    #Impedance programmation values dependent on the SOC
        "OP_IMP_15_OHMS"    : 0,
        "OP_IMP_20_OHMS"    : 0,
        "OP_IMP_25_OHMS"    : 0,
        "OP_IMP_30_OHMS"    : 0,
        "OP_IMP_35_OHMS"    : 0,
        "OP_IMP_40_OHMS"    : 0,
        "OP_IMP_45_OHMS"    : 0,
        "OP_IMP_50_OHMS"    : 0,
        "OP_IMP_55_OHMS"    : 0,
        "OP_IMP_65_OHMS"    : 0,
        "OP_IMP_75_OHMS"    : 0,

        "ODT_IMP_15_OHMS"   : 0,
        "ODT_IMP_20_OHMS"   : 0,
        "ODT_IMP_25_OHMS"   : 0,
        "ODT_IMP_30_OHMS"   : 0,
        "ODT_IMP_35_OHMS"   : 0,
        "ODT_IMP_40_OHMS"   : 0,
        "ODT_IMP_50_OHMS"   : 0,
        "ODT_IMP_60_OHMS"   : 0,
        "ODT_IMP_90_OHMS"   : 0,
        "ODT_IMP_110_OHMS"  : 0,
        "ODT_IMP_170_OHMS"  : 0,
        "ODT_IMP_340_OHMS"  : 0,
    #Predefined pull-up or pull-down resistors on DQS / DQSN, controller side
        "PU_OPEN"           : 8,
        "PU_688_OHMS"       : 9,
        "PU_611_OHMS"       : 10,
        "PU_550_OHMS"       : 11,
        "PU_500_OHMS"       : 12,
        "PU_458_OHMS"       : 13,
        "PU_393_OHMS"       : 14,
        "PU_344_OHMS"       : 15,
        "PD_OPEN"           : 0,
        "PD_688_OHMS"       : 1,
        "PD_611_OHMS"       : 2,
        "PD_550_OHMS"       : 3,
        "PD_500_OHMS"       : 4,
        "PD_458_OHMS"       : 5,
        "PD_393_OHMS"       : 6,
        "PD_344_OHMS"       : 7,
    #Predifined impedance divider values for SOC
        "ODT_120_OHMS"      : 0,
        "ODT_80_OHMS"       : 0,
        "ODT_60_OHMS"       : 0,
        "ODT_40_OHMS"       : 0,
        "ZO_40_OHMS"        : 0,
        "ZO_34_OHMS"        : 0,
    #Predefined impedance divider values for DDR Device (For RZQ = 240 ohms)
        "RTT_NOM_DISABLED"  : 0,
        "RTT_NOM_120_OHMS"  : 2,
        "RTT_NOM_60_OHMS"   : 1,
        "RTT_NOM_40_OHMS"   : 3,
        "RTT_NOM_30_OHMS"   : 5,
        "RTT_NOM_20_OHMS"   : 4,
        "RTT_WR_DYN_ODT_DIS": 0,
        "RTT_WR_120_OHMS"   : 2,
        "RTT_WR_60_OHMS"    : 1,
        "ZO_DDR_40_OHMS"    : 0,
        "ZO_DDR_34_OHMS"    : 1,
    #Override impedance values for drive and ODT SOC side
        "ODT_PULL_UP"       : 0,
        "ODT_PULL_DOWN"     : 0,
        "DRV_PULL_UP"       : 0,
        "DRV_PULL_DOWN"     : 0,
    #Impedance values for dividing mode for SOC and DDR chips
        "ODT_DIV"           : 0,
        "ZO_DIV"            : 0,
        "DQSRES"            : 0,
        "DQSNRES"           : 0,
        "RTT_NOM"           : 0,
        "RTT_WR"            : 0,
        "ZO_DDR"            : 0,
        
    #Parameters related to ddr chip devices
        "ddrchip_density"   : 0,    # can be 512, 1, 2, 4, 8 for 512Mb, 1Gb, 2Gb, 4Gb, 8Gb
        "ddrchip_buswidth"  : 0,    # can be 8 or 16
        "ddrchip_number"    : 0,    # number of DDR chip connected on the interface, 1, 2 or 4
        "ddrchip_pagesize"  : 0,    # can be 1 or 2 for 1KB page or 2KB page
        "ddrchip_colbits"   : 0,    # number of column address bits, can be 10 or 11 (only for 8Gb x8 device)
        "ddrchip_rowbits"   : 0,    # number of row address bits, can be 12,13,14,15 or 16
        "ddrchip_speedgrade": "",   # in the format "FREQ-CL" where FREQ is the maximum bit rate and CL is the CAS latency
        "ddrchip_tcase"     : 0,    # can be 85 or 95 for 85C or 95C (half refresh interval for high temperature range
        "ddrchip_ZQcalFreq" : 0,
        "TRFC"              : 0,
        "TREFI"             : 0,
        "TREFI_MEM_DDR3"    : 0,
        "TCLTAB"            : [0,0,0,0,0,0,0],
        "TCWLTAB"           : [0,0,0,0,0,0,0],
        "TCL"               : 0,
        "TCLMAX"            : 16,
        "TCWL"              : 0,
        "TCWLMAX"           : 10,
        "TRP"               : 0,
        "MAXTRPTCLDIFF"     : 1,
        "TRAS"              : 0,
        "TRC"               : 0,
        "TRCD"              : 0,
        "TRRD"              : 0,
        "TRTP"              : 0,
        "TWR"               : 0,
        "TWTR"              : 0,
        "TXP"               : 0,
        "TXPDLL"            : 0,
        "TZQCS"             : 0,
        "TZQCSI"            : 0,
        "TCKSRE"            : 0,
        "TCKSRX"            : 0,
        "TCKE"              : 0,
        "TMOD"              : 0,
        "TRSTL"             : 0,
        "TZQINIT"           : 0,
        "TZQOPER"           : 0,
        "TFAW"              : 0,
        "TXS"               : 0,
        
    #Parameters common to all DDR chip devices (not updated in real time)    
        "TMRD"              : 4,    # MRS to MRS command
        "TRTW"              : 2,    # bus turn arround timing between 2 and 7
        "TAL"               : 0,    # additive latency, must be 0, TCL-1 or TCL-2
        "TDQS"              : 3,    # TDQS is used only on multi-rank configuration, it's not our case, so no need...
        "TEXSR"             : 512,  # TEXSR equivalent for DDR3 at tXSDLL and tDLLK
        "TMRR"              : 1,    # New timing required by the new PCTL. for DDR3 it's equivalent to tMPRR for multipurpose Register Read.
        
    # Other parameters that can be modified for test purpose only
        "DDR_BURST_LENGTH"  : 8,    # define the data burst length, 8 or 4, used in MCFG register and in MR0
        "ENABLE2TMODE"      : 0,    # if = 1, enable the 2T mode for memory command
        "POWERDOWNEXITFAST" : 1,    # if = 1, define fast power down exit mode, used in MCFG register and in MR0
        "DYNAMICODTENABLE"  : 1,    # if = 1, enable the dynamic ODT on controller side
        "ZDEN"              : 0,    #SELECT impedance setting method on controller side (1 = Impedance overriding values, 0 = Impedance divider values)
    
    # DDR Mode registers
        "MR0reg"            : 0,
        "MR1reg"            : 0,
        "MR2reg"            : 0,
        "MR3reg"            : 0,
        
    # uPCTL register values
        "PCTL_MCFG1"        : 0,
        "PCTL_MCFG"         : 0,
        "PCTL_PPCFG"        : 0,
        "PCTL_TOGCNT1U"     : 0,
        "PCTL_TINIT"        : 0,
        "PCTL_TRSTH"        : 0,
        "PCTL_TOGCNT100N"   : 0,
        "PCTL_DFIODTCFG"    : 0,
        "PCTL_DFIODTCFG1"   : 0,
        "PCTL_DFIODTRANKMAP": 0,
        "PCTL_DFITCTRLDELAY": 0,
        "PCTL_DFITPHYWRDATA": 0,
        "PCTL_DFITPHYWRLAT" : 0,
        "PCTL_DFITRDDATAEN" : 0,
        "PCTL_DFITPHYRDLAT" : 0,
        "PCTL_DFISTCFG0"    : 0,
    # PHY/PUB register values
        "PHY_DXnGCREN"      : 0,
        "PHY_DXnGCRDIS"     : 0,
        "PHY_DXnGTR"        : 0x00055000,
        "PHY_DXnLCDLR0"     : 0x00000000,
        "PHY_DXnLCDLR1"     : 0x00000000,
        "PHY_DXnLCDLR2"     : 0x00000000,
        "PHY_DXnBDLR0"      : 0x00000000,
        "PHY_DXnBDLR1"      : 0x00000000,
        "PHY_DXnBDLR2"      : 0x00000000,
        "PHY_DXnBDLR3"      : 0x00000000,
        "PHY_DXnBDLR4"      : 0x00000000,
        "PHY_ZQ0CR0"        : 0,
        "PHY_ZQ0CR1"        : 0,
        "PHY_DTDR0"         : 0xDD22EE11,
        "PHY_DTDR1"         : 0x7788BB44,
        "PHY_DTAR0"         : 0x00000000,
        "PHY_DTAR1"         : 0x00000008,
        "PHY_DTAR2"         : 0x00000010,
        "PHY_DTAR3"         : 0x00000018,
        "PHY_DTCR"          : 0,
        "PHY_ODTCR"         : 0x00010000,
        "PHY_DTPR0"         : 0,
        "PHY_DTPR1"         : 0,
        "PHY_DTPR2"         : 0,
        "PHY_DCR"           : 0,
        "PHY_DSGCR"         : 0,
        "PHY_DXCCR"         : 0,
        "PHY_ACIOCR"        : 0,
        "PHY_ACBDLR"        : 0,
        "PHY_PTR0"          : 0,
        "PHY_PTR1"          : 0,
        "PHY_PTR2"          : (16<<15) | (15<<10) | (15<<5) | (15<<0),
        "PHY_PTR3"          : 0,
        "PHY_PTR4"          : 0,
        "PHY_PLLCR"         : (0xE<<13),
        "PHY_PGCR0"         : 0,
        "PHY_PGCR1"         : 0,
        "PHY_PGCR2"         : 0
    }
    return ddrparams

def setCHIPDDRddrparams(ddrparams):

    if(ddrparams["ddrchip_density"]<512):
        ddrparams["ddrchip_density"]=ddrparams["ddrchip_density"]*1024

    ddrparams["ddrchip_number"]=ddrparams["lmi_size"]*8/ddrparams["ddrchip_density"]
    ddrparams["ddrchip_buswidth"]=ddrparams["lmi_width"]/ddrparams["ddrchip_number"]

    if(ddrparams["ddrchip_number"]<>1 and ddrparams["ddrchip_number"]<>2 and ddrparams["ddrchip_number"]<>4) or (ddrparams["ddrchip_buswidth"]<>8 and ddrparams["ddrchip_buswidth"]<>16):
        ddrparams["error"]="Incompatibility between lmi_width, lmi_size and ddrchip_density parameters"
        return ddrparams
    
    if(ddrparams["ddrchip_density"]>512):
        ddrparams["ddrchip_density"]=ddrparams["ddrchip_density"]/1024

    if(ddrparams["ddrchip_buswidth"]==16):
        ddrparams["ddrchip_pagesize"]=2
    else:
        ddrparams["ddrchip_pagesize"]=1
        
    if(ddrparams["ddrchip_density"]==8) and (ddrparams["ddrchip_buswidth"]==8):
        ddrparams["ddrchip_colbits"]=11
        ddrparams["ddrchip_pagesize"]=2
    else:
        ddrparams["ddrchip_colbits"]=10

    
    if(ddrparams["ddrchip_density"]==512):
        ddrparams["TRFC"] = ns2tck(90,ddrparams["BITRATE"])
        ddrparams["ddrchip_rowbits"]=14-ddrparams["ddrchip_pagesize"]
    elif(ddrparams["ddrchip_density"]==1):
        ddrparams["TRFC"] = ns2tck(110,ddrparams["BITRATE"])
        ddrparams["ddrchip_rowbits"]=15-ddrparams["ddrchip_pagesize"]
    elif(ddrparams["ddrchip_density"]==2):
        ddrparams["TRFC"] = ns2tck(160,ddrparams["BITRATE"])
        ddrparams["ddrchip_rowbits"]=16-ddrparams["ddrchip_pagesize"]
    elif(ddrparams["ddrchip_density"]==4):
        ddrparams["TRFC"] = ns2tck(260,ddrparams["BITRATE"])
        ddrparams["ddrchip_rowbits"]=17-ddrparams["ddrchip_pagesize"]
    elif(ddrparams["ddrchip_density"]==8):
        ddrparams["TRFC"] = ns2tck(350,ddrparams["BITRATE"])
        ddrparams["ddrchip_rowbits"]=16
    else:
        ddrparams["error"]="Incorrect DDR chip density defined, should be 512, 1, 2, 4 or 8"
        return ddrparams

    if(ddrparams["ddrchip_tcase"]==85):
        ddrparams["TREFI"] = 78
        ddrparams["TREFI_MEM_DDR3"]=ns2tck(7800,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_tcase"]==95):
        ddrparams["TREFI"] = 39
        ddrparams["TREFI_MEM_DDR3"]=ns2tck(3900,ddrparams["BITRATE"])
    else:    
        ddrparams["error"]="Incorrect DDR chip TCase defined, should be 85 or 95"
        return ddrparams
    
    if(ddrparams["ddrchip_speedgrade"]=="800-5"):
        ddrparams["TCLTAB"]=[14,12,10,9,7,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(12.5,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(37.5,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(50,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(50,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="800-6"):
        ddrparams["TCLTAB"]=[17,14,12,10,8,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(15,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(37.5,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(52.5,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(50,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1066-6"):
        ddrparams["TCLTAB"]=[13,11,9,8,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(11.25,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(37.5,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(48.75,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(37.5,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(50,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1066-7"):
        ddrparams["TCLTAB"]=[15,13,11,9,7,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(13.125,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(37.5,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(50.625,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(37.5,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(50,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1066-8"):
        ddrparams["TCLTAB"]=[17,14,12,10,8,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(15,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(37.5,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(52.5,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(37.5,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(10,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(50,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1333-7"):
        ddrparams["TCLTAB"]=[12,10,9,7,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(10.5,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(36,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(46.5,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(45,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1333-8"):
        ddrparams["TCLTAB"]=[13,12,10,8,7,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(12,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(36,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(48,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(45,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1333-9"):
        ddrparams["TCLTAB"]=[15,13,11,9,8,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(13.5,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(36,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(49.5,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"]= max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(45,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1333-10"):
        ddrparams["TCLTAB"]=[17,14,12,10,8,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(15,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(36,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(51,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5.625,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(45,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1600-8"):
        ddrparams["TCLTAB"]=[11,10,8,7,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(10,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(35,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(45,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1600-9"):
        ddrparams["TCLTAB"]=[13,11,9,8,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(11.25,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(35,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(46.25,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1600-10"):
        ddrparams["TCLTAB"]=[14,12,10,9,7,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(12.5,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(35,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(47.5,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1600-11"):
        ddrparams["TCLTAB"]=[15,13,11,10,8,6,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(13.75,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(35,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(48.75,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(30,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(40,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1866-10"):
        ddrparams["TCLTAB"]=[12,10,9,8,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(10.7,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(34,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(44.7,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(27,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1866-11"):
        ddrparams["TCLTAB"]=[13,11,10,8,7,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(11.77,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(34,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(45.77,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(27,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"]= max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1866-12"):
        ddrparams["TCLTAB"]=[14,12,11,9,7,6,6]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(12.84,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(34,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(46.84,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(27,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="1866-13"):
        ddrparams["TCLTAB"]=[16,13,12,10,8,6,6]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(13.91,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(34,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(47.91,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(27,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="2133-11"):
        ddrparams["TCLTAB"]=[11,10,9,7,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(10.285,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(33,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(43.285,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(25,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="2133-12"):
        ddrparams["TCLTAB"]=[12,11,9,8,6,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(11.22,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(33,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(44.22,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(25,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="2133-13"):
        ddrparams["TCLTAB"]=[13,12,10,9,7,5,5]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(12.155,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(33,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(45.155,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(25,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    elif(ddrparams["ddrchip_speedgrade"]=="2133-14"):
        ddrparams["TCLTAB"]=[14,13,11,9,7,6,6]
        ddrparams["TCWLTAB"]=[10,9,8,7,6,5,5]
        ddrparams["TCL"] = selectTTABentry(ddrparams["TCLTAB"],ddrparams["BITRATE"])
        ddrparams["TCWL"] = selectTTABentry(ddrparams["TCWLTAB"],ddrparams["BITRATE"])
        ddrparams["TRP"] = ns2tck(13.09,ddrparams["BITRATE"])
        ddrparams["TRAS"] = ns2tck(33,ddrparams["BITRATE"])
        ddrparams["TRC"] = ns2tck(46.09,ddrparams["BITRATE"])
        ddrparams["TXP"] = max(ns2tck(6,ddrparams["BITRATE"]),3)
        ddrparams["TCKE"] = max(ns2tck(5,ddrparams["BITRATE"]),3)
        if(ddrparams["ddrchip_pagesize"]==1):
            ddrparams["TRRD"] = max(ns2tck(5,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(25,ddrparams["BITRATE"])
        else:
            ddrparams["TRRD"] = max(ns2tck(6,ddrparams["BITRATE"]),4)
            ddrparams["TFAW"] = ns2tck(35,ddrparams["BITRATE"])
    else:
        ddrparams["error"]="Incorrect DDR chip speed grade defined"
        return ddrparams
    if(ddrparams["TCL"]>ddrparams["TCLMAX"]):
        ddrparams["TCL"]=ddrparams["TCLMAX"]
    if(ddrparams["TCWL"]>ddrparams["TCWLMAX"]):
        ddrparams["TCWL"]=ddrparams["TCWLMAX"]
    if(ddrparams["TRP"]-ddrparams["TCL"]>ddrparams["MAXTRPTCLDIFF"]):
        ddrparams["TRP"]=ddrparams["TCL"]+ddrparams["MAXTRPTCLDIFF"]
    ddrparams["TRCD"] = ddrparams["TRP"]
    ddrparams["TRTP"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
    ddrparams["TWR"] = ns2tck(15,ddrparams["BITRATE"])
    if(ddrparams["TWR"]>16):
        ddrparams["TWR"]=16
    ddrparams["TWTR"] = max(ns2tck(7.5,ddrparams["BITRATE"]),4)
    ddrparams["TXPDLL"] = max(ns2tck(24,ddrparams["BITRATE"]),10)
    ddrparams["TZQCS"] = max(ns2tck(80,ddrparams["BITRATE"]),64)
    ddrparams["TCKSRE"] = max(ns2tck(10,ddrparams["BITRATE"]),5)
    ddrparams["TCKSRX"] = max(ns2tck(10,ddrparams["BITRATE"]),5)
    ddrparams["TMOD"] = max(ns2tck(15,ddrparams["BITRATE"]),12)
    ddrparams["TRSTL"] = ns2tck(100,ddrparams["BITRATE"])   # minimum duration of a RESET# pulse (with a minimum of 100ns)
    ddrparams["TZQINIT"] = max(ns2tck(640,ddrparams["BITRATE"]),512)  # Long calibration time after power-up and reset tZQinit
    ddrparams["TZQOPER"] = max(ns2tck(320,ddrparams["BITRATE"]),256)  # Long calibration time in Normal Operation tZQoper
    ddrparams["TXS"] = max(ddrparams["TRFC"]+ns2tck(10,ddrparams["BITRATE"]),5)
    
    if ddrparams["ddrchip_ZQcalFreq"]==0:
        ddrparams["TZQCSI"]=0
    else:
        ddrparams["TZQCSI"]=hz2trefi(ddrparams["ddrchip_ZQcalFreq"],ddrparams["TREFI"])
    return ddrparams

# define all upctl and phy register values that will be used during DDRSS configuration
def set_upctlphy_reg_values(ddrparams):

    ########################
    # Define the MR values:
    ddrparams["MR3reg"]   = 0
    srt      = (ddrparams["ddrchip_tcase"]-85)/10 # selfrefresh temperature range 0=85, 1=95
    asr      = 0 # auto-selfrefresh mode
    pasr     = 0 # 0=Full array self refresh
    ddrparams["MR2reg"]   = (ddrparams["RTT_WR"] << 9) | (srt << 7) | (asr << 6) | ((ddrparams["TCWL"]-5) << 3) | (pasr << 0)
    qoff     = 0 # when set to 1, disable the output buffers
    tdqs     = 0 # when set to 1, enable the TDQS function only valid for x8 DDR devices, and disable the Data Mask function.
    level    = 0 # when set, enable the Write Leveling function. This bit is managed by the PUB state machine during DDR initialization
    if (ddrparams["TAL"] == ddrparams["TCL"] - 1):
        al = 1
    elif (ddrparams["TAL"] == ddrparams["TCL"] - 2):
        al = 2
    else:
        al = 0
    rtt9     = (ddrparams["RTT_NOM"] & 4) >> 2
    rtt6     = (ddrparams["RTT_NOM"] & 2) >> 1
    rtt2     = (ddrparams["RTT_NOM"] & 1) >> 0
    dic5     = (ddrparams["ZO_DDR"] & 2) >> 1
    dic1     = (ddrparams["ZO_DDR"] & 1) >> 0
    de       = 0 # When set, disable the DDR device DLL
    ddrparams["MR1reg"]   = (qoff << 12) | (tdqs << 11) | (level << 7) | (al << 3) | (rtt9 << 9) | (rtt6 << 6) | (rtt2 << 2) | (dic5 << 5) | (dic1 << 1) | (de << 0)
    pd       = ddrparams["POWERDOWNEXITFAST"]
    twr_tab  = [ 1, 2, 3, 4, 5, 5, 6, 6, 7, 7, 0, 0 ]
    dr       = 0 # When set, launch a DLL Reset
    tm       = 0 # When set, enable the Test Mode of the DDR device
    m2m6m5m4 = ddrparams["TCL"] - 4
    bt       = 0 # Burst Read type, if =0: Sequential, if =1: Interleaved
    if (ddrparams["DDR_BURST_LENGTH"]==8):
        bl   = 0
    else:
        bl   = 2
    ddrparams["MR0reg"]   = (pd << 12) | (twr_tab[ddrparams["TWR"]-5] << 9) | (dr << 8) | (tm << 7) | ((m2m6m5m4 & 7) << 4) | ((m2m6m5m4 & 8) >> 1) | (bt << 3) | (bl << 0)

    #define uPCTL register values
    ##############################
    
    #define uPCTL timing registers
    
    nclk = ddrparams["BITRATE"]/4
    ddrparams["PCTL_TOGCNT1U"]    = nclk
    ddrparams["PCTL_TINIT"]       = 200 # minimum value of 200us
    ddrparams["PCTL_TOGCNT100N"]  = nclk / 10
    ddrparams["PCTL_TRSTH"]       = 500 # minimum value of 500us

    #define uPCTL MCFG registers
    if(ddrparams["DDR_BURST_LENGTH"]==4):
        mem_bl=0
    else:
        mem_bl=1
    ddr3_en=1 # if =1, DDR3 protocol rules, otherwise DDR2
    pd_idle=0 # number of n_clk cycle during which NIF is idle to place memory in power down mode. disabled if =0
    pd_type=0 # Sets the power down type to Precharge Power Down (0) or Active Power Down (1)
    if (ddrparams["TFAW"] <= (4 * ddrparams["TRRD"])):
        tfaw_cfg_offset = (4 * ddrparams["TRRD"]) - ddrparams["TFAW"]
        tfaw_cfg = 0
    elif (ddrparams["TFAW"] <= (5 * ddrparams["TRRD"])):
        tfaw_cfg_offset = (5 * ddrparams["TRRD"]) - ddrparams["TFAW"]
        tfaw_cfg = 1
    elif (ddrparams["TFAW"] <= (6 * ddrparams["TRRD"])):
        tfaw_cfg_offset = (6 * ddrparams["TRRD"]) - ddrparams["TFAW"]
        tfaw_cfg = 2
    else:
        ddrparams["error"] = "TFAW and TRRD relation is not supported. Please check these parameters"
        return ddrparams
    sr_idle=0 # number of 32*n_clk during which NIF is idle to place memory in self_refresh mode. disabled if =0
    hw_idle=0 # number of 32*n_clk during which NIF is idle to output c_active low (hardware idle), disabled if =0
    hw_exit_idle_en=0 # When this bit is 1, the c_active_in pin can be used to exit from the automatic clock stop, power down or self-refresh modes.

    ddrparams["PCTL_MCFG"] = (tfaw_cfg << 18) | (ddrparams["POWERDOWNEXITFAST"] << 17) | (pd_type << 16) | (pd_idle << 8) | (ddr3_en<<5) | (ddrparams["ENABLE2TMODE"] << 3) | (mem_bl << 0)
    ddrparams["PCTL_MCFG1"] = (hw_exit_idle_en << 31) | (hw_idle << 16) | (tfaw_cfg_offset << 8) | (sr_idle << 0)

    #Define partially populated memories configuration register
    if (ddrparams["lmi_width"]==ddrparams["lmi_IP_width"]):
        ppmem_en = 0
    elif (ddrparams["lmi_width"]==ddrparams["lmi_IP_width"]/2):
        ppmem_en = 1
    else:
        ddrparams["error"] = "Incorrect relationship between lmi_width and lmi_IP_width"
        return ddrparams
    ddrparams["PCTL_PPCFG"] = (ppmem_en<<0)
    
    #Define DFI timing registers
    rank0_odt_read_nsel  = 0 # Enable/disable ODT for memory rank 0 when a read access is occurring on a different rank
    rank0_odt_read_sel   = 0 # Enable/disable ODT for memory rank 0 when a read access is occurring on this rank
    rank0_odt_write_nsel = 0 # Enable/disable ODT for memory rank 0 when a write access is occurring on a different rank
    rank0_odt_write_sel  = 1 # Enable/disable ODT for memory rank 0 when a write access is occurring on this rank
    rank0_odt_default    = 0 # Default ODT value of rank 0 when there is no read/write activity
    ddrparams["PCTL_DFIODTCFG"] = (rank0_odt_default << 4) | (rank0_odt_write_sel << 3) | (rank0_odt_write_nsel << 2) | (rank0_odt_read_sel << 1) | (rank0_odt_read_nsel << 0)

    #recommended values for ODT output timings are taken from Table 2-16 of uPCTL databook
    odt_lat_w     = 0          # (5 bits) Latency after a write command that ODT is set in terms of SDR cycles
    odt_lat_r     = ddrparams["TCL"] - ddrparams["TCWL"] # (5 bits) Latency after a read command that ODT is set in terms of SDR cycles
    odt_len_bl8_w = 6          # (3 bits) Length of ODT signal for BL8 writes, in terms of SDR cycles
    odt_len_bl8_r = 6          # (3 bits) Length of ODT signal for BL8 reads, in terms of SDR cycles
    ddrparams["PCTL_DFIODTCFG1"] = (odt_len_bl8_r << 24) | (odt_len_bl8_w << 16) | (odt_lat_r << 8) | (odt_lat_w << 0)

    ddrparams["PCTL_DFIODTRANKMAP"] = 1 # used to remap ODT signals in the case of multi ranks, as we are single rank => no interest
    
    # These latency computations come not from PCTL spec, but from PUB spec
    # section 4.3.12 "DFI Timing Parameters" HDR mode, RR mode

    ddrparams["PCTL_DFITCTRLDELAY"] = 3 # (4 bits) Specifies the number of DFI clock cycles after an assertion or deassertion of the DFI control signals that the control signals at the PHY-DRAM interface reflect the assertion.
    ddrparams["PCTL_DFITPHYWRDATA"] = 1
    ddrparams["PCTL_DFITPHYWRLAT"] = ((ddrparams["TCWL"] + ddrparams["TAL"] + 1) >> 1) - 2
    ddrparams["PCTL_DFITRDDATAEN"] = ((ddrparams["TCL"] + ddrparams["TAL"] + 1) >> 1) - 2
    ddrparams["PCTL_DFITPHYRDLAT"] = 14
    ddrparams["PCTL_DFISTCFG0"] = 3

    #define PHY register values
    ############################
    
    # setup DATX8 General Configuration Register for each byte lane
    dxen   = 1 # data byte lane enabled
    dsen   = 1 # (2 bits) DQS enabled with normal polarity
    dqsrtt = ddrparams["DYNAMICODTENABLE"] # Enable dynamic ODT on DQS and DQSN signals
    dqrtt  = ddrparams["DYNAMICODTENABLE"] # Enable dynamic ODT on DQ and DM signals
    rttoh  = 1 # (2 bits) internal RTT output hold
    rttoal = 0 # 0: internal RTT control is set almost 2 cycles beafore read data preamble, 1: internal RTT control is set almost 1 cycle before read data preamble
    wlrken = 1 # (4 bits, one for each rank) when set to 1, the corresponding rank is enabled for write leveling
    mdlen  = 1 # enable master delay line for this lane
    ddrparams["PHY_DXnGCREN"] = ((mdlen << 30) | (wlrken << 26) | (rttoal << 13) | (rttoh << 11) | (dqrtt << 10) | (dqsrtt << 9) | (dsen << 7) | (dxen << 0))
    dxen = 0
    dxppd = 1
    dxpdr = 1
    dqsrpd = 1
    dxoeo = 2
    pllpd = 1
    wlrken = 0
    mdlen = 0
    calbyp = 1
    ddrparams["PHY_DXnGCRDIS"] = ((calbyp << 31) | (mdlen << 30) | (wlrken << 26) | (pllpd <<17) | (dxoeo << 14) | (dqsrpd << 6) | (dxpdr <<5) | (dxppd << 4) | (dxen << 0))

    # setup controller pads impedance
    zcalen = (~ddrparams["ZDEN"])&1 # enable the calibration of impedances only if ZDEN = 0
    zcalbyp = ddrparams["ZDEN"]
    if(ddrparams["ZDEN"]==0):
        if((ddrparams["ODT_PULL_UP"]==0) or (ddrparams["ODT_PULL_DOWN"]==0)):
            if(ddrparams["ODT_DIV"]==ddrparams["ODT_120_OHMS"]):
                ddrparams["ODT_PULL_UP"]=ddrparams["ODT_IMP_110_OHMS"]
                ddrparams["ODT_PULL_DOWN"]=ddrparams["ODT_IMP_110_OHMS"]
            if(ddrparams["ODT_DIV"]==ddrparams["ODT_80_OHMS"]):
                ddrparams["ODT_PULL_UP"]=ddrparams["ODT_IMP_90_OHMS"]
                ddrparams["ODT_PULL_DOWN"]=ddrparams["ODT_IMP_90_OHMS"]
            if(ddrparams["ODT_DIV"]==ddrparams["ODT_60_OHMS"]):
                ddrparams["ODT_PULL_UP"]=ddrparams["ODT_IMP_60_OHMS"]
                ddrparams["ODT_PULL_DOWN"]=ddrparams["ODT_IMP_60_OHMS"]
            if(ddrparams["ODT_DIV"]==ddrparams["ODT_40_OHMS"]):
                ddrparams["ODT_PULL_UP"]=ddrparams["ODT_IMP_40_OHMS"]
                ddrparams["ODT_PULL_DOWN"]=ddrparams["ODT_IMP_40_OHMS"]
        if((ddrparams["DRV_PULL_UP"]==0) or (ddrparams["DRV_PULL_DOWN"]==0)):
            if(ddrparams["ZO_DIV"]==ddrparams["ZO_40_OHMS"]):
                ddrparams["DRV_PULL_UP"]=ddrparams["OP_IMP_40_OHMS"]
                ddrparams["DRV_PULL_DOWN"]=ddrparams["OP_IMP_40_OHMS"]
            if(ddrparams["ZO_DIV"]==ddrparams["ZO_34_OHMS"]):
                ddrparams["DRV_PULL_UP"]=ddrparams["OP_IMP_35_OHMS"]
                ddrparams["DRV_PULL_DOWN"]=ddrparams["OP_IMP_35_OHMS"]
    if(ddrparams["IOTYPE"]=="D3R"):
        ddrparams["PHY_ZQ0CR0"] = ((zcalen << 30) | (zcalbyp << 29) | (ddrparams["ZDEN"] << 28) | (ddrparams["ODT_PULL_UP"] << 15) | (ddrparams["ODT_PULL_DOWN"] << 10) | (ddrparams["DRV_PULL_UP"] << 5) | (ddrparams["DRV_PULL_DOWN"] << 0))
    elif(ddrparams["IOTYPE"]=="D3F"):
        ddrparams["PHY_ZQ0CR0"] = ((zcalen << 30) | (zcalbyp << 29) | (ddrparams["ZDEN"] << 28) | (ddrparams["ODT_PULL_UP"] << 21) | (ddrparams["ODT_PULL_DOWN"] << 14) | (ddrparams["DRV_PULL_UP"] << 7) | (ddrparams["DRV_PULL_DOWN"] << 0))
    dfipu1 = 0
    dfipu0 = 0
    dficcu = 0
    dficu1 = 0
    dficu0 = 1
    ddrparams["PHY_ZQ0CR1"] = ((dfipu1 << 17) | (dfipu0 << 16) | (dficcu << 14) | (dficu1 << 13) | (dficu0 << 12) | (ddrparams["ODT_DIV"] << 4) | (ddrparams["ZO_DIV"] << 0))
    
    #setup initial value of write path BDLR to avoid any warning after data training (solvnet 8000640555)
    wbdlinit = ((43000-13*ddrparams["BITRATE"])/1000)&0x3F
    #wbdlinit = 18
    ddrparams["PHY_DXnBDLR0"] = ((wbdlinit<<24) |(wbdlinit<<18) | (wbdlinit<<12) |(wbdlinit<<6) | (wbdlinit<<0))
    ddrparams["PHY_DXnBDLR1"] = ((wbdlinit<<24) |(wbdlinit<<18) | (wbdlinit<<12) |(wbdlinit<<6) | (wbdlinit<<0))
    ddrparams["PHY_DXnBDLR2"] = ((wbdlinit<<6) | (wbdlinit<<0))

    # setup Data Training Configuration register
    rfshdt  = 1
    ranken  = 1 # (4 bits) one bit per rank, enable the data training for the corresponding rank
    dtexg   = 0
    dtexd   = 0
    dtdstp  = 0
    dtden   = 0
    dtdbs   = 0
    dtwdqmo = 0
    dtdbc   = 1
    dtwbddm = 1
    dtwdqm  = 5
    dtcmpd  = 1
    if(ddrparams["on_emu"]==0):
        dtmpr   = 1
    else:
        dtmpr   = 0
    dtrank  = 0
    dtrptn  = 0xF
    ddrparams["PHY_DTCR"] = ((rfshdt << 28) | (ranken << 24) | (dtexg << 23) | (dtexd << 22) | (dtdstp << 21) | (dtden << 20) | (dtdbs << 16) | (dtwdqmo << 14) | (dtdbc << 13) | (dtwbddm << 12) | (dtwdqm << 8) | (dtcmpd << 7) | (dtmpr << 6) | (dtrank << 4) | (dtrptn << 0))

    # setup DRAM Timing Parameter Registers for the PHY
    tccdb   = 0
    trtwb   = 0
    trtodtb = 0
    tdllk   = ddrparams["TEXSR"]
    tcke    = ddrparams["TCKE"]
    txp     = max(ddrparams["TXPDLL"],ddrparams["TXP"])
    txs     = max(ddrparams["TXS"],ddrparams["TEXSR"])
    ddrparams["PHY_DTPR2"] = ((tccdb << 31) | (trtwb << 30) | (trtodtb << 29) | (tdllk << 19) | (tcke << 15) | (txp << 10) | (txs << 0))
    twlo    = (8 & 0xF)
    twlmrd  = (40 & 0x3F)
    trfc    = (ddrparams["TRFC"] & 0x1FF)
    tfaw    = (ddrparams["TFAW"] & 0x3F)
    tmod    = ((ddrparams["TMOD"] - 12) & 0x7)
    tmrd    = ((ddrparams["TMRD"] - 4) & 0x3)
    ddrparams["PHY_DTPR1"] = ((twlo << 26) | (twlmrd << 20) | (trfc << 11) | (tfaw << 5) | (tmod << 2) | (tmrd << 0))
    trc     = (ddrparams["TRC"] & 0x3F)
    trrd    = (ddrparams["TRRD"] & 0xF)
    tras    = (ddrparams["TRAS"] & 0x3F)
    trcd    = (ddrparams["TRCD"] & 0xF)
    trp     = (ddrparams["TRP"] & 0xF)
    twtr    = (ddrparams["TWTR"] & 0xF)
    trtp    = (ddrparams["TRTP"] & 0xF)
    ddrparams["PHY_DTPR0"] = ((trc << 26) | (trrd << 22) | (tras << 16) | (trcd << 12) | (trp << 8) | (twtr << 4) | (trtp << 0))
    
    # setup DRAM configuration Regiter for PHY
    ddr2t    = ddrparams["ENABLE2TMODE"]
    bytemask = 0x01
    mprdq    = 0
    pdq      = 0
    ddr8bnk  = 1
    ddrmd    = 3
    ddrparams["PHY_DCR"] = ((ddr2t << 28) | (bytemask << 10) | (mprdq << 7) | (pdq << 4) | (ddr8bnk << 3) | (ddrmd << 0))
    
    # setup DDR System General Configuration Register
    ckeoe   = 1
    rstoe   = 1
    odtoe   = 1
    ckoe    = 1
    odtpdd  = 0xE
    ckepdd  = 0xE
    sdrmode = 0
    rrmode  = 1
    atoae   = 0
    dtooe   = 0
    dtoiom  = 0
    dtopdr  = 1
    dtopdd  = 1
    dtoodt  = 0
    puad    = 4
    brrmode = 0
    dqsgx   = 0
    cuaen   = 0
    lpplpd  = 1
    lpiopd  = 1
    zuen    = 0
    bdisen  = 1
    puren   = 1
    ddrparams["PHY_DSGCR"] = ((ckeoe << 31) | (rstoe << 30) | (odtoe << 29) | (ckoe << 28 ) | (odtpdd << 24) | (ckepdd << 20) | (sdrmode << 19) | (rrmode << 18) | (atoae << 17) | (dtooe << 16) | (dtoiom << 15) | (dtopdr << 14) | (dtopdd << 13) | (dtoodt << 12) | (puad << 8) | (brrmode << 7) | (dqsgx << 6) | (cuaen << 5) | (lpplpd << 4) | (lpiopd << 3) | (zuen << 2) | (bdisen << 1) | (puren << 0))
    
    # setup DATX8 Common Configuration Register
    udqiom  = 0
    udqpdr  = 1
    udqpdd  = 1
    udqodt  = 0
    dqsnres = ddrparams["DQSNRES"]
    dqsres  = ddrparams["DQSRES"]
    dxpdr   = 0
    dxpdd   = 0
    mdlen   = 1
    dxiom   = 0
    dxodt   = 1 # FB: set to enable ODT on DQ, DM and DQS pads. By default it is not set...
    ddrparams["PHY_DXCCR"] = ((udqiom << 21) | (udqpdr << 20) | (udqpdd << 19) | (udqodt << 18) | (dqsnres << 9) | (dqsres << 5) | (dxpdr << 4) | (dxpdd << 3) | (mdlen << 2) | (dxiom << 1) | (dxodt << 0))
    
    # setup AC IO Configuration Register
    rstiom  = 1
    rstpdr  = 1
    rstpdd  = 0
    rstodt  = 0
    rankpdr = 0xF
    cspdd   = 0xE
    rankodt = 0
    ckpdr   = 0x7
    ckpdd   = 0x6
    ckodt   = 0
    acpdr   = 1
    acpdd   = 0
    acodt   = 0
    acoe    = 1
    aciom   = 0
    ddrparams["PHY_ACIOCR"] = ((rstiom << 29) | (rstpdr << 28) | (rstpdd << 27) | (rstodt << 26) | (rankpdr << 22) | (cspdd << 18) | (rankodt << 14) | (ckpdr << 11) | (ckpdd << 8) | (ckodt << 5) | (acpdr << 4) | (acpdd << 3) | (acodt << 2) | (acoe << 1) | (aciom << 0))
    
    # setup PHY Timing registers
    dinit3  = 640 # in ns
    dinit2  = 200 # in us
    ddrparams["PHY_PTR4"] = (((ns2tck(dinit3,ddrparams["BITRATE"])) << 18) | ((ns2tck(dinit2*1000,ddrparams["BITRATE"])) << 0))
    dinit1  = ddrparams["TXS"]
    dinit0  = 500 # in us
    ddrparams["PHY_PTR3"] = ((dinit1 << 20) | ((ns2tck(dinit0*1000,ddrparams["BITRATE"])) <<0))
    tplllock = 100 # in us
    tpllrst  = 9   # in us
    ddrparams["PHY_PTR1"] = ((((tplllock*ddrparams["BITRATE"]+3)//4) << 16) | (((tpllrst*ddrparams["BITRATE"]+3)//4) << 0))
    tpllpd   = 1   # in us
    tpllgs   = 4   # in us
    tphyrst  = 16  # in APB clock cycles
    ddrparams["PHY_PTR0"] = ((((tpllpd*ddrparams["BITRATE"]+3)//4) << 21) | (((tpllgs*ddrparams["BITRATE"]+3)//4) << 6) | (tphyrst <<0))
    
    if(ddrparams["BITRATE"]>1400):
        ddrparams["PHY_PLLCR"]=(0x0<<18)|(0xE<<13)
    else:
        if (ddrparams["BITRATE"]>1000):
            ddrparams["PHY_PLLCR"]=(0x1<<18)|(0xE<<13)
        else:
            ddrparams["PHY_PLLCR"]=(0x3<<18)|(0xE<<13)
    
    pubmode = 0x0F
    fxdlat = 0
    nobub = 0
    trefprd = (9*ddrparams["TREFI"]*ddrparams["BITRATE"])//20-400
    ddrparams["PHY_PGCR2"] = (pubmode<<20) | (fxdlat<<19) | (nobub<<18) | (trefprd<<0)
    
    lbmode = 0
    lbgdqs = 0
    lbdqss = 0
    iolb = 0
    inhvt = 0
    dxhrst = 1
    zcksel = 2
    dldlmt = 1
    fdepth = 2
    lpfdepth = 0
    lpfen = 1
    mdlen = 0
    if ((ddrparams["vdd_ddr"]==0) or (ddrparams["vdd_ddr"]==1)):
        ioddrm = 1 + ddrparams["vdd_ddr"] # only for D3F (Cannes) IO type, 0 for DDR2, 1 for DDR3 and 2 for DDR3L
    else:
        ddrparams["error"]="Bad vdd_ddr parameter, should be 0 for 1.5V or 1 for 1.35V."
        return ddrparams
    wlselt = 1 # recommended by SNPS case 8000640555
    achrst = 1
    wslopt = 0
    wlstep = 1 # recommended by SNPS case 8000640555
    wlmode = 0
    pddisdx = 1
    ddrparams["PHY_PGCR1"] = (lbmode<<31) | (lbgdqs<<29) | (lbdqss<<28) | (iolb<<27) | (inhvt<<26) | (dxhrst<<25) | (zcksel<<23) | (dldlmt<<15) | (fdepth<<13) | (lpfdepth<<11) | (lpfen<<10) | (mdlen<<9) | (ioddrm<<7) | (wlselt<<6) | (achrst<<5) | (wslopt<<4) | (wlstep<<2) | (wlmode<<1) | (pddisdx<<0)
    
    cken = 0x02 # only 1 clock used on Alicante, need to check for Cannes
    dtosel = 0
    oscwdl = 3
    oscdiv = 7
    oscen = 0
    dltst = 0
    dltmode = 0
    rdbvt = 1
    wdbvt = 1
    rglvt = 1
    rdlvt = 1
    wdlvt = 1
    wllvt = 1
    ddrparams["PHY_PGCR0"] = (cken<<26) | (dtosel<<14) | (oscwdl<<12) | (oscdiv<<9) | (oscen<<8) | (dltst<<7) | (dltmode<<6) | (rdbvt<<5) | (wdbvt<<4) | (rglvt<<3) | (rdlvt<<2) | (wdlvt<<1) | (wllvt<<0)
    return ddrparams
    
    


def reset_ddrss(ddrparams):
# Step 1.1: Assert and release uPCTL reset pins
################################################
    if(ddrparams["on_emu"]==0):
        if(ddrparams["pctlresetpol"]==0):
                # Assert uPCTL reset
            ddrparams["pctlresetreg"].and_const(~(1 << ddrparams["pctlresetbit"]))
                #wait 10mS
            sttp.stmc.delay(10000)
                # De-assert uPCTL reset
            ddrparams["pctlresetreg"].or_const((1 << ddrparams["pctlresetbit"]))
        elif(ddrparams["pctlresetpol"]==1):
                # Assert uPCTL reset
            ddrparams["pctlresetreg"].or_const((1 << ddrparams["pctlresetbit"]))
                #wait 10mS
            sttp.stmc.delay(10000)
                # De-assert uPCTL reset
            ddrparams["pctlresetreg"].and_const(~(1 << ddrparams["pctlresetbit"]))
        if( (ddrparams["ddr_debug"]==1) and ( (ddrparams["pctlresetpol"]==0) or (ddrparams["pctlresetpol"]==1) ) ):
            sttp.logging.print_out("----- DDR Configuration: DDRSS reset done")
        

    
################################################
#                                              #
#              DDRSS CONFIGURE                 #
#                                              #
################################################
def configure_ddrss(ddrparams):

# Define constant values used by init procedure, DO NOT MODIFY THEM
####################################################################

    # SCTL.state_cmd Values
    SCTL_STATE_CMD_INIT = 0
    SCTL_STATE_CMD_CFG = 1
    SCTL_STATE_CMD_GO = 2
    SCTL_STATE_CMD_SLEEP = 3
    SCTL_STATE_CMD_WAKEUP = 4

    # STAT.ctl_stat Values
    STAT_CTL_STAT_INIT_MEM = 0
    STAT_CTL_STAT_CONFIG = 1
    STAT_CTL_STAT_CONFIG_REQ = 2
    STAT_CTL_STAT_ACCESS = 3
    STAT_CTL_STAT_ACCESS_REQ = 4
    STAT_CTL_STAT_LOW_POWER = 5
    STAT_CTL_STAT_LOW_POWER_ENTRY_REQ = 6
    STAT_CTL_STAT_LOW_POWER_EXIT_REQ = 7

    # PHY PIR step run or skip
    skip = 0
    run = 1

##########################################################################################################################################################################################
# DDR interface initialization, following Table P2-3 page 79/467 of DesignWare Core SDRAM Universal DDR Protocol Control (uPCTL) / Memory Controller (uMCTL) Version 2.30a August 5, 2011
##########################################################################################################################################################################################

    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("\n-- Setup DDR subsytem at %d Mbps" %(ddrparams["BITRATE"]))
        sttp.logging.print_out("-- DDR chip number: %d" %(ddrparams["ddrchip_number"]))
        sttp.logging.print_out("-- LMI size: %d\n" %(ddrparams["lmi_size"]))
    # Bypass first automatic init phase. This init may stall if some byte lanes are not present (Palma2). This init is done again manually in step 4.
    if(ddrparams["on_emu"]==0):
        ddrparams["phy"].DDR3PHY_PIR.or_const((1 << 30) | (1 << 29) | (1 << 28))

    # Set rr_mode between mixer and uPCTL
    if(ddrparams["rrmodevalue"]==0) and (ddrparams["rrmodereg"]<>0):
        ddrparams["rrmodereg"].and_const(~(1 << ddrparams["rrmodebit"]))
    elif(ddrparams["rrmodevalue"]==1) and (ddrparams["rrmodereg"]<>0):
        ddrparams["rrmodereg"].or_const((1 << ddrparams["rrmodebit"]))

    # PCTL is in Init_mem state after reset

# Step 1.2: Set-up TOGCNT1U, TINIT, TOGCNT100N and TRSTH
#########################################################

    ddrparams["pctl"].DDR3PCTL_TOGCNT1U.poke(ddrparams["PCTL_TOGCNT1U"])
    ddrparams["pctl"].DDR3PCTL_TINIT.poke(ddrparams["PCTL_TINIT"]*((~ddrparams["on_emu"])&1)+ddrparams["on_emu"])
    ddrparams["pctl"].DDR3PCTL_TOGCNT100N.poke(ddrparams["PCTL_TOGCNT100N"])
    ddrparams["pctl"].DDR3PCTL_TRSTH.poke(ddrparams["PCTL_TRSTH"]*((~ddrparams["on_emu"])&1)+ddrparams["on_emu"])	
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 1 done")

# Step 2: Configure uPCTL's MCFG register
##########################################

    ddrparams["pctl"].DDR3PCTL_MCFG.poke(ddrparams["PCTL_MCFG"])
    ddrparams["pctl"].DDR3PCTL_MCFG1.poke(ddrparams["PCTL_MCFG1"])
    ddrparams["pctl"].DDR3PCTL_PPCFG.poke(ddrparams["PCTL_PPCFG"])

    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 2 done")

# Step 3: Configure the DFI timing parameter registers
#######################################################

    ddrparams["pctl"].DDR3PCTL_DFIODTCFG.poke(ddrparams["PCTL_DFIODTCFG"])
    ddrparams["pctl"].DDR3PCTL_DFIODTCFG1.poke(ddrparams["PCTL_DFIODTCFG1"])
    ddrparams["pctl"].DDR3PCTL_DFIODTRANKMAP.poke(ddrparams["PCTL_DFIODTRANKMAP"])
    ddrparams["pctl"].DDR3PCTL_DFITCTRLDELAY.poke(ddrparams["PCTL_DFITCTRLDELAY"])
    ddrparams["pctl"].DDR3PCTL_DFITPHYWRDATA.poke(ddrparams["PCTL_DFITPHYWRDATA"])
    ddrparams["pctl"].DDR3PCTL_DFITPHYWRLAT.poke(ddrparams["PCTL_DFITPHYWRLAT"])
    ddrparams["pctl"].DDR3PCTL_DFITRDDATAEN.poke(ddrparams["PCTL_DFITRDDATAEN"])
    ddrparams["pctl"].DDR3PCTL_DFITPHYRDLAT.poke(ddrparams["PCTL_DFITPHYRDLAT"])
    
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 3 done")

# Step 4: Start PHY initialization by accessing relevant PUB registers
########################################################################
    if(ddrparams["on_emu"]==0):
        if ddrparams["lmi_width"] == 32:
            ddrparams["phy"].DDR3PHY_DX3GCR.poke(ddrparams["PHY_DXnGCREN"])
            ddrparams["phy"].DDR3PHY_DX3GTR.poke(ddrparams["PHY_DXnGTR"])
            ddrparams["phy"].DDR3PHY_DX3LCDLR0.poke(ddrparams["PHY_DXnLCDLR0"])
            ddrparams["phy"].DDR3PHY_DX3LCDLR1.poke(ddrparams["PHY_DXnLCDLR1"])
            ddrparams["phy"].DDR3PHY_DX3LCDLR2.poke(ddrparams["PHY_DXnLCDLR2"])
            ddrparams["phy"].DDR3PHY_DX3BDLR0.poke(ddrparams["PHY_DXnBDLR0"])
            ddrparams["phy"].DDR3PHY_DX3BDLR1.poke(ddrparams["PHY_DXnBDLR1"])
            ddrparams["phy"].DDR3PHY_DX3BDLR2.poke(ddrparams["PHY_DXnBDLR2"])
            ddrparams["phy"].DDR3PHY_DX3BDLR3.poke(ddrparams["PHY_DXnBDLR3"])
            ddrparams["phy"].DDR3PHY_DX3BDLR4.poke(ddrparams["PHY_DXnBDLR4"])        
            ddrparams["phy"].DDR3PHY_DX2GCR.poke(ddrparams["PHY_DXnGCREN"])
            ddrparams["phy"].DDR3PHY_DX2GTR.poke(ddrparams["PHY_DXnGTR"])
            ddrparams["phy"].DDR3PHY_DX2LCDLR0.poke(ddrparams["PHY_DXnLCDLR0"])
            ddrparams["phy"].DDR3PHY_DX2LCDLR1.poke(ddrparams["PHY_DXnLCDLR1"])
            ddrparams["phy"].DDR3PHY_DX2LCDLR2.poke(ddrparams["PHY_DXnLCDLR2"])
            ddrparams["phy"].DDR3PHY_DX2BDLR0.poke(ddrparams["PHY_DXnBDLR0"])
            ddrparams["phy"].DDR3PHY_DX2BDLR1.poke(ddrparams["PHY_DXnBDLR1"])
            ddrparams["phy"].DDR3PHY_DX2BDLR2.poke(ddrparams["PHY_DXnBDLR2"])
            ddrparams["phy"].DDR3PHY_DX2BDLR3.poke(ddrparams["PHY_DXnBDLR3"])
            ddrparams["phy"].DDR3PHY_DX2BDLR4.poke(ddrparams["PHY_DXnBDLR4"])
        else:
            if ddrparams["lmi_IP_width"] == 32:
                ddrparams["phy"].DDR3PHY_DX3GCR.poke(ddrparams["PHY_DXnGCRDIS"])
                ddrparams["phy"].DDR3PHY_DX2GCR.poke(ddrparams["PHY_DXnGCRDIS"])
        if ddrparams["lmi_width"]>8:
            ddrparams["phy"].DDR3PHY_DX1GCR.poke(ddrparams["PHY_DXnGCREN"])
            ddrparams["phy"].DDR3PHY_DX1GTR.poke(ddrparams["PHY_DXnGTR"])
            ddrparams["phy"].DDR3PHY_DX1LCDLR0.poke(ddrparams["PHY_DXnLCDLR0"])
            ddrparams["phy"].DDR3PHY_DX1LCDLR1.poke(ddrparams["PHY_DXnLCDLR1"])
            ddrparams["phy"].DDR3PHY_DX1LCDLR2.poke(ddrparams["PHY_DXnLCDLR2"])
            ddrparams["phy"].DDR3PHY_DX1BDLR0.poke(ddrparams["PHY_DXnBDLR0"])
            ddrparams["phy"].DDR3PHY_DX1BDLR1.poke(ddrparams["PHY_DXnBDLR1"])
            ddrparams["phy"].DDR3PHY_DX1BDLR2.poke(ddrparams["PHY_DXnBDLR2"])
            ddrparams["phy"].DDR3PHY_DX1BDLR3.poke(ddrparams["PHY_DXnBDLR3"])
            ddrparams["phy"].DDR3PHY_DX1BDLR4.poke(ddrparams["PHY_DXnBDLR4"])
        else:
            ddrparams["phy"].DDR3PHY_DX1GCR.poke(ddrparams["PHY_DXnGCRDIS"])
        ddrparams["phy"].DDR3PHY_DX0GCR.poke(ddrparams["PHY_DXnGCREN"])
        ddrparams["phy"].DDR3PHY_DX0GTR.poke(ddrparams["PHY_DXnGTR"])
        ddrparams["phy"].DDR3PHY_DX0LCDLR0.poke(ddrparams["PHY_DXnLCDLR0"])
        ddrparams["phy"].DDR3PHY_DX0LCDLR1.poke(ddrparams["PHY_DXnLCDLR1"])
        ddrparams["phy"].DDR3PHY_DX0LCDLR2.poke(ddrparams["PHY_DXnLCDLR2"])
        ddrparams["phy"].DDR3PHY_DX0BDLR0.poke(ddrparams["PHY_DXnBDLR0"])
        ddrparams["phy"].DDR3PHY_DX0BDLR1.poke(ddrparams["PHY_DXnBDLR1"])
        ddrparams["phy"].DDR3PHY_DX0BDLR2.poke(ddrparams["PHY_DXnBDLR2"])
        ddrparams["phy"].DDR3PHY_DX0BDLR3.poke(ddrparams["PHY_DXnBDLR3"])
        ddrparams["phy"].DDR3PHY_DX0BDLR4.poke(ddrparams["PHY_DXnBDLR4"])

        # setup controller pads impedance
        ddrparams["phy"].DDR3PHY_ZQ0CR0.poke(ddrparams["PHY_ZQ0CR0"])
        ddrparams["phy"].DDR3PHY_ZQ0CR1.poke(ddrparams["PHY_ZQ0CR1"])

        # setup data used for training
        ddrparams["phy"].DDR3PHY_DTDR0.poke(ddrparams["PHY_DTDR0"])
        ddrparams["phy"].DDR3PHY_DTDR1.poke(ddrparams["PHY_DTDR1"])
    
        #setup address used for data training
        ddrparams["phy"].DDR3PHY_DTAR0.poke(ddrparams["PHY_DTAR0"])
        ddrparams["phy"].DDR3PHY_DTAR1.poke(ddrparams["PHY_DTAR1"])
        ddrparams["phy"].DDR3PHY_DTAR2.poke(ddrparams["PHY_DTAR2"])
        ddrparams["phy"].DDR3PHY_DTAR3.poke(ddrparams["PHY_DTAR3"])
    
    # setup Data Training Configuration register
    ddrparams["phy"].DDR3PHY_DTCR.poke(ddrparams["PHY_DTCR"])

    # setup ODT configuration register
    ddrparams["phy"].DDR3PHY_ODTCR.poke(ddrparams["PHY_ODTCR"])
    
    # setup MRx registers for PHY
    ddrparams["phy"].DDR3PHY_MR3.poke(ddrparams["MR3reg"])
    ddrparams["phy"].DDR3PHY_MR2.poke(ddrparams["MR2reg"])
    ddrparams["phy"].DDR3PHY_MR1.poke(ddrparams["MR1reg"])
    ddrparams["phy"].DDR3PHY_MR0.poke(ddrparams["MR0reg"])
    
    # setup DRAM Timing Parameter Registers for the PHY
    ddrparams["phy"].DDR3PHY_DTPR2.poke(ddrparams["PHY_DTPR2"])
    ddrparams["phy"].DDR3PHY_DTPR1.poke(ddrparams["PHY_DTPR1"])
    ddrparams["phy"].DDR3PHY_DTPR0.poke(ddrparams["PHY_DTPR0"])
    
    # setup DRAM configuration Regiter for PHY
    ddrparams["phy"].DDR3PHY_DCR.poke(ddrparams["PHY_DCR"])
    
    # setup DDR System General Configuration Register
    ddrparams["phy"].DDR3PHY_DSGCR.poke(ddrparams["PHY_DSGCR"])
    
    if(ddrparams["on_emu"]==0):
        # setup DATX8 Common Configuration Register
        if (ddrparams.has_key("phydxccr_callback")):
            # Calling SOC specific function to work around potential bug
            phydxccr_callback = ddrparams["phydxccr_callback"]
            phydxccr_callback(ddrparams)
        else:
            ddrparams["phy"].DDR3PHY_DXCCR.poke(ddrparams["PHY_DXCCR"])

        # setup AC IO Configuration Register
        ddrparams["phy"].DDR3PHY_ACIOCR.poke(ddrparams["PHY_ACIOCR"])
        ddrparams["phy"].DDR3PHY_ACBDLR.poke(ddrparams["PHY_ACBDLR"])
    
    # setup PHY Timing registers
    ddrparams["phy"].DDR3PHY_PTR4.poke(ddrparams["PHY_PTR4"])
    ddrparams["phy"].DDR3PHY_PTR3.poke(ddrparams["PHY_PTR3"])
    ddrparams["phy"].DDR3PHY_PTR2.poke(ddrparams["PHY_PTR2"])
    ddrparams["phy"].DDR3PHY_PTR1.poke(ddrparams["PHY_PTR1"])
    ddrparams["phy"].DDR3PHY_PTR0.poke(ddrparams["PHY_PTR0"])
    
    if(ddrparams["on_emu"]==0):
        ddrparams["phy"].DDR3PHY_PLLCR.poke(ddrparams["PHY_PLLCR"])
    
    ddrparams["phy"].DDR3PHY_PGCR2.poke(ddrparams["PHY_PGCR2"])
    ddrparams["phy"].DDR3PHY_PGCR1.poke(ddrparams["PHY_PGCR1"])
    ddrparams["phy"].DDR3PHY_PGCR0.poke(ddrparams["PHY_PGCR0"])
    
    if(ddrparams["on_emu"]==0):
        # setup PHY Initialization Register
        ctldinit  = 0
        wreye     = skip
        rdeye     = skip
        wrdskw    = skip
        rddskw    = skip
        wladj     = skip
        qsgate    = skip
        wl        = skip
        draminit  = skip
        dramrst   = skip
        phyrst    = run
        dcal      = run
        pllinit   = run
        init      = run
        ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1) # be sure that IDONE is at 1 before running a new PHY initialization
        if sttp.pokepeek.if_eq(ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMTagMask"],ddrparams["HoMTagValid"]):     # If we are in wake-up from stand-by
            zcal      = skip
            pir = (ctldinit << 18) | (wreye << 15) | (rdeye << 14) | (wrdskw << 13) | (rddskw << 12) | (wladj << 11) | (qsgate << 10) | (wl << 9) | (draminit << 8) | (dramrst << 7) | (phyrst << 6) | (dcal << 5) | (pllinit << 4) | (zcal << 1) | (init << 0)
            ddrparams["phy"].DDR3PHY_PIR.poke(pir&0xFFFFFFFE)
            ddrparams["phy"].DDR3PHY_PIR.poke(pir)
        else:
            zcal      = ((ddrparams["PHY_ZQ0CR0"]>>30)&1)
            pir = (ctldinit << 18) | (wreye << 15) | (rdeye << 14) | (wrdskw << 13) | (rddskw << 12) | (wladj << 11) | (qsgate << 10) | (wl << 9) | (draminit << 8) | (dramrst << 7) | (phyrst << 6) | (dcal << 5) | (pllinit << 4) | (zcal << 1) | (init << 0)
            ddrparams["phy"].DDR3PHY_PIR.poke(pir&0xFFFFFFFE)
            ddrparams["phy"].DDR3PHY_PIR.poke(pir)
        sttp.stmc.delay(10)
        if(ddrparams["ddr_debug"]==1):
            sttp.logging.print_out("----- DDR Configuration: Step 4 done")

# Step 5: Monitor PHY initialization status polling the PUB register PHY_PGSR0.IDONE
    if(ddrparams["on_emu"]==0):
        if(ddrparams["ddr_debug"]==1):
            upctl_ddrdbg.checkPGSR(ddrparams["phy"],pir,ddrparams["lmi_width"])
        ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1)
        if(ddrparams["ddr_debug"]==1):
            sttp.logging.print_out("----- DDR Configuration: Step 5 done")

# Step 6: Monitor DFI initialization status polling DFISTSTAT0.dfi_init_complete
    ddrparams["pctl"].DDR3PCTL_DFISTCFG0.poke(ddrparams["PCTL_DFISTCFG0"])
    ddrparams["pctl"].DDR3PCTL_DFISTSTAT0.while_and_ne(0x1,0x1)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 6 done")

# Step 7: Start Memory power-up sequence by writing POWCTL.power_up_start = 1 and wait for POWSTAT.power_up_done = 1
    ddrparams["pctl"].DDR3PCTL_POWCTL.poke(1)
    ddrparams["pctl"].DDR3PCTL_POWSTAT.while_and_ne(0x1,0x1)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 7 done")

# Step 8: Configure rest of uPCTL. Program all timing T* registers
    ddrparams["pctl"].DDR3PCTL_TCKESR.poke(ddrparams["TCKE"]+1)
    ddrparams["pctl"].DDR3PCTL_TMRR.poke(ddrparams["TMRR"])
    ddrparams["pctl"].DDR3PCTL_TZQCL.poke(ddrparams["TZQINIT"])
    ddrparams["pctl"].DDR3PCTL_TRSTL.poke(ddrparams["TRSTL"])
    ddrparams["pctl"].DDR3PCTL_TMOD.poke(ddrparams["TMOD"])
    ddrparams["pctl"].DDR3PCTL_TCKE.poke(ddrparams["TCKE"])
    ddrparams["pctl"].DDR3PCTL_TCKSRX.poke(ddrparams["TCKSRX"])
    ddrparams["pctl"].DDR3PCTL_TCKSRE.poke(ddrparams["TCKSRE"])
    ddrparams["pctl"].DDR3PCTL_TDQS.poke(ddrparams["TDQS"])
    ddrparams["pctl"].DDR3PCTL_TZQCSI.poke(ddrparams["TZQCSI"]*((~ddrparams["on_emu"])&1))
    ddrparams["pctl"].DDR3PCTL_TZQCS.poke(ddrparams["TZQCS"])
    ddrparams["pctl"].DDR3PCTL_TXPDLL.poke(ddrparams["TXPDLL"])
    ddrparams["pctl"].DDR3PCTL_TXP.poke(ddrparams["TXP"])
    ddrparams["pctl"].DDR3PCTL_TEXSR.poke(ddrparams["TEXSR"])
    ddrparams["pctl"].DDR3PCTL_TWTR.poke(ddrparams["TWTR"])
    ddrparams["pctl"].DDR3PCTL_TWR.poke(ddrparams["TWR"])
    ddrparams["pctl"].DDR3PCTL_TRTP.poke(ddrparams["TRTP"])
    ddrparams["pctl"].DDR3PCTL_TRRD.poke(ddrparams["TRRD"])
    ddrparams["pctl"].DDR3PCTL_TRCD.poke(ddrparams["TRCD"])
    ddrparams["pctl"].DDR3PCTL_TRC.poke(ddrparams["TRC"])
    ddrparams["pctl"].DDR3PCTL_TRAS.poke(ddrparams["TRAS"])
    ddrparams["pctl"].DDR3PCTL_TCWL.poke(ddrparams["TCWL"])
    ddrparams["pctl"].DDR3PCTL_TCL.poke(ddrparams["TCL"])
    ddrparams["pctl"].DDR3PCTL_TAL.poke(ddrparams["TAL"])
    ddrparams["pctl"].DDR3PCTL_TRTW.poke(ddrparams["TRTW"])
    ddrparams["pctl"].DDR3PCTL_TRP.poke(ddrparams["TRP"])
    ddrparams["pctl"].DDR3PCTL_TRFC.poke(ddrparams["TRFC"])
    ddrparams["pctl"].DDR3PCTL_TMRD.poke(ddrparams["TMRD"]*((~ddrparams["on_emu"])&1))
    ddrparams["pctl"].DDR3PCTL_TREFI.poke((ddrparams["TREFI"] | (1<<31))*((~ddrparams["on_emu"])&1))   # bit 31 set to 1 to update the internal logic with the new t_refi value
    if ddrparams["pctl"].DDR3PCTL_IPVR.if_gt(0xFFFFFF00,0x32343000):         # If uPCTL version > 2.40
        sttp.pokepeek.poke(ddrparams["pctl"].baseAddress+0x148,ddrparams["TREFI_MEM_DDR3"]) # Set new register TREFI_MEM_DDR3 value
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 8 done")

# Step 9: memory initialization by PUB state machine

    ctldinit  = 0
    wreye     = skip
    rdeye     = skip
    wrdskw    = skip
    rddskw    = skip
    wladj     = skip
    qsgate    = skip
    wl        = skip
    draminit  = run
    dramrst   = run
    phyrst    = skip
    dcal      = skip
    pllinit   = skip
    zcal      = skip
    init      = run
    pir = (ctldinit << 18) | (wreye << 15) | (rdeye << 14) | (wrdskw << 13) | (rddskw << 12) | (wladj << 11) | (qsgate << 10) | (wl << 9) | (draminit << 8) | (dramrst << 7) | (phyrst << 6) | (dcal << 5) | (pllinit << 4) | (zcal << 1) | (init << 0)
    ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1) # be sure that IDONE is at 1 before running a new PHY initialization
    ddrparams["phy"].DDR3PHY_PIR.poke(pir&0xFFFFFFFE)
    ddrparams["phy"].DDR3PHY_PIR.poke(pir)
    if(ddrparams["on_emu"]==0):
        sttp.stmc.delay(10)
    if(ddrparams["ddr_debug"]==1):
        upctl_ddrdbg.checkPGSR(ddrparams["phy"],pir,ddrparams["lmi_width"])
    
    ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 9 done")

# Step 10: Write CFG to SCTL.state_cmd register and poll STAT.ctl_stat = Config
    ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_CFG)
    ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_CONFIG)
    # PCTL is in Config state
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 10 done")

# Step 11: Configure uPCTL to refine configuration. Optional step only necessary if changing settings programmed in step 7
    ddrparams["pctl"].DDR3PCTL_TZQCL.poke(ddrparams["TZQOPER"])
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 11 done")

# Step 12: Enable CMDTSTAT register by writing CMDTSTAT.cmd_tstat_en=1'b1. Monitor command timers expiration by polling CMDTSTAT.cmd_tstat = 1'b1.
    ddrparams["pctl"].DDR3PCTL_CMDTSTATEN.poke(1)
    ddrparams["pctl"].DDR3PCTL_CMDTSTAT.while_and_ne(1,1)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 12 done")
    if sttp.pokepeek.if_eq(ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMTagMask"],ddrparams["HoMTagValid"]):     # If we are in wake-up from stand-by
        ddrparams["phy"].DDR3PHY_ZQ0CR0.read_modify_write(0xFFF003FF, 0, 0x00000000)    # Case 8000680095: To avoid error during ZQ calibration in wake-up from stand-by
        if((ddrparams["ZQ0SR0Offset"]&0x80000000)==0x80000000):           # if ZQ0SR0 has been saved by application before going to stand-by
            sttp.pokepeek.read_modify_write(ddrparams["phy"].DDR3PHY_ZQ0CR0, ddrparams["HoMStruct_Add"]+(ddrparams["ZQ0SR0Offset"]&0x7FFFFFFF), 0x0FFFFFFF, 0, 0x10000000)     # Override calibration values with saved ZData ZDEN=1 and no calibration required.
        else:
            ddrparams["phy"].DDR3PHY_ZQ0CR0.read_modify_write(0x0FFFFFFF, 0, 0x10000000)    # Modify ZQ0CR0 with ZDEN=1 and no calibration required.
        ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_GO)                             # place uPCTL in access mode to be able to place it in low-power (self-refresh) later
        ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_ACCESS)
        ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_SLEEP)                          # place uPCTL in low-power (self-refresh) mode
        ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_LOW_POWER)
    sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PoutSet,1<<ddrparams["retn_pionumber"])     # Set lmi_notretention PIO at 1
    if(ddrparams["retn_piotype"]=="PUSH_PULL"):                                                 # If lmi_notretention PIO is implemented as push-pull (external resistor divider bridge)
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC0Reset,1<<ddrparams["retn_pionumber"])    # Configure lmi_notretention PIO as output push-pull
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC1Set,1<<ddrparams["retn_pionumber"])
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC2Reset,1<<ddrparams["retn_pionumber"])
    else:                                                                                       # If lmi_notretention PIO is implemented as open-drain (external pull-up to VDD_DDR)
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC0Set,1<<ddrparams["retn_pionumber"])    # Configure lmi_notretention PIO as output open-drain
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC1Set,1<<ddrparams["retn_pionumber"])
        sttp.pokepeek.poke(ddrparams["retn_piobankadd"]+PC2Reset,1<<ddrparams["retn_pionumber"])
    sttp.stmc.delay(10000) # wait 10ms for PIO value propagation
    if sttp.pokepeek.if_eq(ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMTagMask"],ddrparams["HoMTagValid"]):     # If we are in wake-up from stand-by
        ddrparams["phy"].DDR3PHY_ZQ0CR0.poke(ddrparams["PHY_ZQ0CR0"])                               # Come back to original ZQ0CR0 value
        # setup PHY Initialization Register to realize an impedance calibration
        ctldinit  = 0
        wreye     = skip
        rdeye     = skip
        wrdskw    = skip
        rddskw    = skip
        wladj     = skip
        qsgate    = skip
        wl        = skip
        draminit  = skip
        dramrst   = skip
        phyrst    = skip
        dcal      = skip
        pllinit   = skip
        zcal      = ((ddrparams["PHY_ZQ0CR0"]>>30)&1)
        init      = run
        pir = (ctldinit << 18) | (wreye << 15) | (rdeye << 14) | (wrdskw << 13) | (rddskw << 12) | (wladj << 11) | (qsgate << 10) | (wl << 9) | (draminit << 8) | (dramrst << 7) | (phyrst << 6) | (dcal << 5) | (pllinit << 4) | (zcal << 1) | (init << 0)
        ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1) # be sure that IDONE is at 1 before running a new PHY initialization
        ddrparams["phy"].DDR3PHY_PIR.poke(pir&0xFFFFFFFE)
        ddrparams["phy"].DDR3PHY_PIR.poke(pir)
        if(ddrparams["on_emu"]==0):
            sttp.stmc.delay(10)
        if(ddrparams["ddr_debug"]==1):
            upctl_ddrdbg.checkPGSR(ddrparams["phy"],pir,ddrparams["lmi_width"])
        ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1)
        ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_WAKEUP)                         # Wake-up uPCTL to place it in access mode (end of self-refresh)
        ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_ACCESS)
        ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_CFG)                            # place uPCTL in config mode for data training
        ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_CONFIG)
        if(ddrparams["DTUPHYAddOffset"]>0x80000000):        # If the DTU address is defined by the application
            sttp.pokepeek.read_modify_write(ddrparams["phy"].DDR3PHY_DTAR0, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPHYAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000000)     # define DDR addresses used for data training
            sttp.pokepeek.read_modify_write(ddrparams["phy"].DDR3PHY_DTAR1, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPHYAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000008)     # define DDR addresses used for data training
            sttp.pokepeek.read_modify_write(ddrparams["phy"].DDR3PHY_DTAR2, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPHYAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000010)     # define DDR addresses used for data training
            sttp.pokepeek.read_modify_write(ddrparams["phy"].DDR3PHY_DTAR3, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPHYAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000018)     # define DDR addresses used for data training
        else:                                               # or use a fixed address
            address=ddrparams["DTUAddress"]                 # and convert it to Bank Row Column
            Bank = 0;
            i=0
            while(i<3):
                BankBitPos = (ddrparams["MixerBankBitReg"] >> (i * 8)) & 0x1F               # Get back the bank address bit positions depending on the Mixer configuration
                Bank = Bank | ((address >> (BankBitPos - i)) & 0x1) << i                    # Retrieve each bank address bit and construct Bank Bus by concatenating them
                address = (address & ((1 << (BankBitPos - i)) - 1)) | ((address >> 1) & (~((1 << (BankBitPos - i)) -1)))    # Remove the bank bit from the address and concatenate the remaining bits
                i=i+1
            if(ddrparams["lmi_width"]==32):
                address = address >> 2                                                      # Remove the trailing Byte address bits
            if(ddrparams["lmi_width"]==16):
                address = address >> 1                                                      # Remove the trailing Byte address bits
            Row = address >> ddrparams["ddrchip_colbits"]                                   # Get the Row address bus from the high part of the remaining address
            Col = address & ((1 << ddrparams["ddrchip_colbits"]) - 1)                       # Get the Col address bus from the low part of the remaining address
            ddrparams["phy"].DDR3PHY_DTAR0.poke( ((Bank & 0x7) << 28) | ((Row & 0xFFFF) << 12) | ((Col & 0xFE0) << 0) | 0x00000000)
            ddrparams["phy"].DDR3PHY_DTAR1.poke( ((Bank & 0x7) << 28) | ((Row & 0xFFFF) << 12) | ((Col & 0xFE0) << 0) | 0x00000008)
            ddrparams["phy"].DDR3PHY_DTAR2.poke( ((Bank & 0x7) << 28) | ((Row & 0xFFFF) << 12) | ((Col & 0xFE0) << 0) | 0x00000010)
            ddrparams["phy"].DDR3PHY_DTAR3.poke( ((Bank & 0x7) << 28) | ((Row & 0xFFFF) << 12) | ((Col & 0xFE0) << 0) | 0x00000018)

# Step 13: Configure PUB's PIR register to specify which training steps to run
    # setup PHY Initialization Register
    ctldinit  = 0
    wreye     = run*((~ddrparams["on_emu"])&1)
    rdeye     = run*((~ddrparams["on_emu"])&1)
    wrdskw    = run*((~ddrparams["on_emu"])&1)
    rddskw    = run*((~ddrparams["on_emu"])&1)
    wladj     = run*((~ddrparams["on_emu"])&1)
    qsgate    = run
    wl        = run*((~ddrparams["on_emu"])&1)
    draminit  = skip
    dramrst   = skip
    phyrst    = skip
    dcal      = skip
    pllinit   = skip
    zcal      = skip
    init      = run
    pir = (ctldinit << 18) | (wreye << 15) | (rdeye << 14) | (wrdskw << 13) | (rddskw << 12) | (wladj << 11) | (qsgate << 10) | (wl << 9) | (draminit << 8) | (dramrst << 7) | (phyrst << 6) | (dcal << 5) | (pllinit << 4) | (zcal << 1) | (init << 0)
    ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1) # be sure that IDONE is at 1 before running a new PHY initialization
    ddrparams["phy"].DDR3PHY_PIR.poke(pir&0xFFFFFFFE)
    ddrparams["phy"].DDR3PHY_PIR.poke(pir)
    if(ddrparams["on_emu"]==0):
        sttp.stmc.delay(10)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 13 done")

# Step 14: Monitor PUB's PHY_PGSR0.IDONE register to poll for completion of training sequence
    if(ddrparams["ddr_debug"]==1):
        upctl_ddrdbg.checkPGSR(ddrparams["phy"],pir,ddrparams["lmi_width"])
    
    ddrparams["phy"].DDR3PHY_PGSR0.while_and_ne(0x1,0x1)
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("PGSR0= 0x%08x"%(ddrparams["phy"].DDR3PHY_PGSR0.peek()))
        sttp.logging.print_out("----- DDR Configuration: Step 14 done")

# Step 15: Write GO into SCTL.state_cmd register and poll STAT.ctl_stat = Access
    ddrparams["pctl"].DDR3PCTL_SCTL.poke(SCTL_STATE_CMD_GO)
    ddrparams["pctl"].DDR3PCTL_STAT.while_and_ne(0x7,STAT_CTL_STAT_ACCESS)

    # PCTL is in Access state
    if(ddrparams["ddr_debug"]==1):
        sttp.logging.print_out("----- DDR Configuration: Step 15 done")

    if (ddrparams["ddr_debug"]==1):
        upctl_ddrdbg.PrintPHYDXnGSR0reg(ddrparams["phy"],0)
        if (ddrparams["lmi_width"] > 8):
            upctl_ddrdbg.PrintPHYDXnGSR0reg(ddrparams["phy"],1)
        if (ddrparams["lmi_width"] > 16):
            upctl_ddrdbg.PrintPHYDXnGSR0reg(ddrparams["phy"],2)
            upctl_ddrdbg.PrintPHYDXnGSR0reg(ddrparams["phy"],3)
        upctl_ddrdbg.PrintPHYRegisters(ddrparams["phy"],ddrparams["lmi_width"])
        upctl_ddrdbg.PrintPCTLRegisters(ddrparams["pctl"])
        upctl_ddrdbg.print_local_delays(ddrparams["phy"],ddrparams["lmi_width"])
        upctl_ddrdbg.PrintDataTrainingResults(ddrparams["phy"])
        if(ddrparams["on_emu"]==0):
            upctl_ddrdbg.PCTLDTU_test(ddrparams,0)
            #upctl_ddrdbg.PHY_RAMBIST_Test(ddrparams,0)        
            # testnb=0
            # while(ddrparams["error"]==""):
                # testnb=testnb+1
                # sttp.logging.print_out("DDRTest number:%d"%(testnb))
                # ddrparams=upctl_ddrdbg.PHY_RAMBIST_Test(ddrparams,0)        
            # sttp.logging.print_out("Error at test number:%d"%(testnb))
            # upctl_ddrdbg.PrintPHYRegisters(ddrparams["phy"],ddrparams["lmi_width"])
            # upctl_ddrdbg.PrintPCTLRegisters(ddrparams["pctl"])
    if sttp.pokepeek.if_eq(ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMTagMask"],ddrparams["HoMTagValid"]):     # If we are in wake-up from stand-by
        if(ddrparams["DataRecovery"]>0x80000000):               # if the data erased by DTU have to be restored
            i=0
            while(i<32):
                sttp.pokepeek.read_modify_write(ddrparams["DTUAddress"]+i*4,ddrparams["HoMStruct_Add"]+(ddrparams["DataRecovery"]&0x7FFFFFFF)+i*4,0xFFFFFFFF,0,0)
                i=i+1
        else:                                                   # otherwise use uPCTL DTU to clear memory used
            if(ddrparams["DTUPCTLAddOffset"]>0x80000000):       # If the DTU address is defined by the application
                sttp.pokepeek.read_modify_write(ddrparams["pctl"].DDR3PCTL_DTUWACTL, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPCTLAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000000)     # define DDR addresses used for data training
                sttp.pokepeek.read_modify_write(ddrparams["pctl"].DDR3PCTL_DTURACTL, ddrparams["HoMStruct_Add"]+(ddrparams["DTUPCTLAddOffset"]&0x7FFFFFFF), 0xFFFFFFE0, 0, 0x00000000)     # define DDR addresses used for data training
            else:                                               # or use a fixed address
                address=ddrparams["DTUAddress"]                 # and convert it to Bank Row Column
                Bank = 0;
                i=0
                while(i<3):
                    BankBitPos = (ddrparams["MixerBankBitReg"] >> (i * 8)) & 0x1F               # Get back the bank address bit positions depending on the Mixer configuration
                    Bank = Bank | ((address >> (BankBitPos - i)) & 0x1) << i                    # Retrieve each bank address bit and construct Bank Bus by concatenating them
                    address = (address & ((1 << (BankBitPos - i)) - 1)) | ((address >> 1) & (~((1 << (BankBitPos - i)) -1)))    # Remove the bank bit from the address and concatenate the remaining bits
                    i=i+1
                if(ddrparams["lmi_width"]==32):
                    address = address >> 2                                                      # Remove the trailing Byte address bits
                if(ddrparams["lmi_width"]==16):
                    address = address >> 1                                                      # Remove the trailing Byte address bits
                Row = address >> ddrparams["ddrchip_colbits"]                                   # Get the Row address bus from the high part of the remaining address
                Col = address & ((1 << ddrparams["ddrchip_colbits"]) - 1)                       # Get the Col address bus from the low part of the remaining address
                ddrparams["pctl"].DDR3PCTL_DTUWACTL.poke( ((Bank & 0x7) << 10) | ((Row & 0xFFFF) << 13) | ((Col & 0x3E0) << 0))
                ddrparams["pctl"].DDR3PCTL_DTURACTL.poke( ((Bank & 0x7) << 10) | ((Row & 0xFFFF) << 13) | ((Col & 0x3E0) << 0))
            # Run PCTL DTU to clear memory content
            ddrparams["pctl"].DDR3PCTL_DTUWD0.poke(0)
            ddrparams["pctl"].DDR3PCTL_DTUWD1.poke(0)
            ddrparams["pctl"].DDR3PCTL_DTUWD2.poke(0)
            ddrparams["pctl"].DDR3PCTL_DTUWD3.poke(0)
            ddrparams["pctl"].DDR3PCTL_DTUAWDT.poke((0<<9) | (3<<6) | (1<<3) | (3<<0))
            ddrparams["pctl"].DDR3PCTL_DTUCFG.poke((127<<16) | (1<<8) | (1<<7) | (63<<1) | (1<<0))
            ddrparams["pctl"].DDR3PCTL_DTUECTL.poke(1)
            ddrparams["pctl"].DDR3PCTL_DTUECTL.while_and_ne(0x1,0x0)
            ddrparams["pctl"].DDR3PCTL_DTUCFG.poke(0)
            ddrparams["pctl"].DDR3PCTL_DTUWACTL.read_modify_write(0xFFFFFFE0,0,0x10)
            ddrparams["pctl"].DDR3PCTL_DTURACTL.read_modify_write(0xFFFFFFE0,0,0x10)
            ddrparams["pctl"].DDR3PCTL_DTUCFG.poke((127<<16) | (1<<8) | (1<<7) | (63<<1) | (1<<0))
            ddrparams["pctl"].DDR3PCTL_DTUECTL.poke(1)
            ddrparams["pctl"].DDR3PCTL_DTUECTL.while_and_ne(0x1,0x0)
            ddrparams["pctl"].DDR3PCTL_DTUCFG.poke(0)
        # at the end, update the HoM Tag
        sttp.pokepeek.read_modify_write(ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMStruct_Add"]+ddrparams["HoMTagOffset"],ddrparams["HoMTagUpdateAND"],0,ddrparams["HoMTagUpdateOR"])
# End
    return
