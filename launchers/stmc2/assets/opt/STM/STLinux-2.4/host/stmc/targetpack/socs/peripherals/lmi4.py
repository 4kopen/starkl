import sttp
import sttp.logging
import sttp.stmc

# Deafult parameters for LMI4
SDRAM_DEVICE_TYPE = "ddr"
SDRAM_SIZE_CONFIG = "256"
SDRAM_CAS_CONFIG = "3.0"
SDRAM_FREQ_CONFIG = "166"

MEMORY_0_SIZE = 256
MEMORY_1_SIZE = 0

__mem_base_addr = None
ddr_output_is_normal_drive = True

def lmi4_setup(params):
    """
    device = ("ddr", "sdr", "elpida_64mbits")    
    the params is a dictionary,
    frequency = ("120", "133", "166", "200"),    unit = MHz
    mem_0_size = ("64", "128", "256", "512")     unit = Mbits
    mem_1_size = ("64", "128", "256", "512")     unit = Mbits
    cas = ("2.0", "2.5", "3.0")                  unit = Clocks
    mem_base_addr = {0x0C, 0x40, ...}            unit = the top 8 bits of memory base address
    ddr_output_is_normal_drive = ("strong", "weak")
    """
    global SDRAM_DEVICE_TYPE
    global SDRAM_SIZE_CONFIG
    global SDRAM_CAS_CONFIG
    global SDRAM_FREQ_CONFIG
    global MEMORY_0_SIZE
    global MEMORY_1_SIZE
    global __mem_base_addr
    global ddr_output_is_normal_drive
    
    if params.has_key("device"):
        SDRAM_DEVICE_TYPE = params["device"].lower()
    
    if params.has_key("mem_0_size"):
        SDRAM_SIZE_CONFIG = params["mem_0_size"]
        MEMORY_0_SIZE = int(SDRAM_SIZE_CONFIG)
        
    if params.has_key("mem_1_size"):
        MEMORY_1_SIZE = int(params["mem_1_size"])

    if params.has_key("cas"):
        SDRAM_CAS_CONFIG = params["cas"].lower()
    
    if params.has_key("frequency"):
        SDRAM_FREQ_CONFIG = params["frequency"].lower()

    if params.has_key("mem_base_addr"):
        __mem_base_addr = params["mem_base_addr"]
    
    if params.has_key("output_mode"):
        ddr_output_is_normal_drive = (params["output_mode"] == "strong")

    return


def lmi4_info(regs):
    """
    lmi4_info(regs)
    regs = sttp.targetpack.get_soc.LMISYS.st40_lmi4_regs
    """
    # sttp.logging.print_out("SDRAM_DEVICE_TYPE = %s, SDRAM_SIZE_CONFIG = %s, SDRAM_CAS_CONFIG = %s, SDRAM_FREQ_CONFIG = %s"  %(SDRAM_DEVICE_TYPE, SDRAM_SIZE_CONFIG, SDRAM_CAS_CONFIG, SDRAM_FREQ_CONFIG))
    global __mem_base_addr
    
    sttp.logging.print_out("\n ==Informations of SDRAM==")
    
    ##
    ## info -- SDRAM DEVICE TYPE
    ##
    if SDRAM_DEVICE_TYPE == "ddr":
        sttp.logging.print_out(" SDRAM DEVICE TYPE = DDR SRAM")
    elif SDRAM_DEVICE_TYPE == "sdr":
        sttp.logging.print_out(" SDRAM DEVICE TYPE = SDR SRAM")
    elif SDRAM_DEVICE_TYPE == "elpida_64mbits":
        sttp.logging.print_out(" SDRAM DEVICE TYPE = Non-Standard ELPIDA 64MBits DDR")

    ##
    ## info -- SDRAM DEVICE FREQUENCY
    ##
    sttp.logging.print_out(" SDRAM DEVICE FREQUENCY = %sMHz" % SDRAM_FREQ_CONFIG)

    ##
    ## info -- SDRAM DEVICE SIZE
    ##
    if SDRAM_SIZE_CONFIG == "512":
        sttp.logging.print_out(" SDRAM DEVICE SIZE = 512 MBits (64 MBytes)")
    elif SDRAM_SIZE_CONFIG == "256":
        sttp.logging.print_out(" SDRAM DEVICE SIZE = 256 MBits (32 MBytes)")
    elif SDRAM_SIZE_CONFIG == "128":
        sttp.logging.print_out(" SDRAM DEVICE SIZE = 128 MBits (16 MBytes)")
    elif SDRAM_SIZE_CONFIG == "64":
        sttp.logging.print_out(" SDRAM DEVICE SIZE = 64 MBits (8 MBytes)")

    ##
    ## info -- SDRAM DEVICE REFRESH PERIOD
    ##
    if (SDRAM_SIZE_CONFIG == "512") or (SDRAM_SIZE_CONFIG == "256"):
        sttp.logging.print_out(" SDRAM DEVICE REFRESH PERIOD = 7.8 microseconds (8K/64ms)")
    elif SDRAM_SIZE_CONFIG == "128" or SDRAM_SIZE_CONFIG == "64":
        sttp.logging.print_out(" SDRAM DEVICE REFRESH PERIOD = 15.625 microseconds (4K/64ms)")

    ##
    ## info -- SDRAM DEVICE REFRESH CAS LATENCY
    ##
    sttp.logging.print_out(" SDRAM CAS CONFIG = %s" % SDRAM_CAS_CONFIG)
    
    ##
    ## info -- mem_base_addr, reg_base_addr from register LMI_VCR
    ##
    if __mem_base_addr != None:
        mem_base_addr = __mem_base_addr
    else:
        mem_base_addr = (regs.LMI_VCR1.peek() >> 16) & 0xFF
    
    reg_base_addr = (regs.LMI_VCR1.peek() >> 24) & 0xFF
    sttp.logging.print_out(" Base address of memory = 0x%08x, base address of LMI register = 0x%08x" % ((mem_base_addr<<24), (reg_base_addr<<24)))

    ##
    ## info -- Memory Size
    ##
    if MEMORY_1_SIZE == 0:
        sttp.logging.print_out(" There is only 1 memory, Top address of memory 0 = 0x%08X" % ((mem_base_addr << 24) + MEMORY_0_SIZE / 8 * 0x100000))
    else:
        sttp.logging.print_out(" There are 2 memories,\n Top address of memory 0 = 0x%08X, Top address of memory 1 = 0x%08X" % ((mem_base_addr << 24) + MEMORY_0_SIZE / 8 * 0x100000, mem_base_addr + MEMORY_1_SIZE / 8 * 0x100000))
        
    sttp.logging.print_out(" =========================\n")

    return


def init_ddr(regs):
    """
    init_ddr(regs)
    regs = sttp.targetpack.get_soc.LMISYS.st40_lmi4_regs
    Initialize DDR:
        1. initialize general LMI4 registers
        2. launch the initialisation sequence for ddr device
    """
    init_lmi4_regs(regs)
    
    ##---------------------------------------------------------------------------
    ## Initialisation Sequence for LMI & DDR-SDRAM Device
    ##---------------------------------------------------------------------------
    
    ## 200 microseconds to settle clocks
    sttp.stmc.delay(200)

    ## SDRAM Control Register
    ## Clock enable
    regs.LMI_SCR.poke(0x3)

    ## NOP enable
    regs.LMI_SCR.poke(0x1)

    ## Precharge all banks
    regs.LMI_SCR.poke(0x2)
    
    ## LMI_SDRAM_ROW_MODE0 & LMI_SDRAM_ROW_MODE1 Registers
    if ddr_output_is_normal_drive :
        ## EMRS Row 0 & 1: Normal Drive : Enable DLL
        regs.LMI_SDMR0.poke(0x400)
        regs.LMI_SDMR1.poke(0x400)
    else:
        ## EMRS Row 0 & 1: Weak Drive : Enable DLL
        regs.LMI_SDMR0.poke(0x402)
        regs.LMI_SDMR1.poke(0x402)
    
    sttp.stmc.delay(200)
    
    if SDRAM_CAS_CONFIG == "2.0":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 2.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0123)
        regs.LMI_SDMR1.poke(0x0123)
    elif SDRAM_CAS_CONFIG == "2.5":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 2.5, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0163)
        regs.LMI_SDMR1.poke(0x0163)
    elif SDRAM_CAS_CONFIG == "3.0":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 3.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0133)
        regs.LMI_SDMR1.poke(0x0133)
    else:
        sttp.logging.print_out("Unknow the value of CAS, exit ...")
        return
    
    ## 200 clock cycles required to lock DLL
    sttp.stmc.delay(200)

    ## Precharge all banks
    regs.LMI_SCR.poke(0x2)

    ## CBR enable (auto-refresh)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    
    if (SDRAM_CAS_CONFIG == "2.0"):
        ## MRS Row 0 & 1 : deassert DLL - /CAS = 2.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0023)
        regs.LMI_SDMR1.poke(0x0023)
    elif (SDRAM_CAS_CONFIG == "2.5"):
        ## MRS Row 0 & 1 : deassert DLL - /CAS = 2.5, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0063)
        regs.LMI_SDMR1.poke(0x0063)
    elif (SDRAM_CAS_CONFIG == "3.0"):
        ## MRS Row 0 & 1 : deassert DLL - /CAS = 3.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0033)
        regs.LMI_SDMR1.poke(0x0033)
    else:
        sttp.logging.print_out("Unknow the value of CAS, exit ...")
        return

    ## Normal SDRAM operation, No burst Refresh after standby
    regs.LMI_SCR.poke(0x0)
    
    return


def init_sdr(regs):
    """
    init_sdr(regs)
    regs = sttp.targetpack.get_soc.LMISYS.st40_lmi4_regs
    Initialize SDR:
        1. initialize general LMI4 registers
        2. launch the initialisation sequence for sdr device
    """
    init_lmi4_regs(regs)
    
    ##---------------------------------------------------------------------------
    ## Initialisation Sequence for LMI & SDR-SDRAM Device
    ##---------------------------------------------------------------------------
    
    ## 200 microseconds to settle clocks
    sttp.stmc.delay(200)

    ## SDRAM Control Register
    ## Clock enable
    regs.LMI_SCR.poke(0x3)

    ## 200 microseconds to settle clocks
    regs.LMI_SCR.poke(200)

    ## SDRAM Control Register
    ## percharge all banks (PALL)
    regs.LMI_SCR.poke(0x2)

    ##send auto-refresh (CBR enable) command 8 times
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    regs.LMI_SCR.poke(0x4)
    
    if SDRAM_CAS_CONFIG == "2.0":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 2.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0023)
        regs.LMI_SDMR1.poke(0x0023)
    elif SDRAM_CAS_CONFIG == "2.5":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 2.5, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0063)
        regs.LMI_SDMR1.poke(0x0063)
    elif SDRAM_CAS_CONFIG == "3.0":
        ## MRS Row 0 & 1 : Reset DLL - /CAS = 3.0, Mode Sequential, Burst Length 8
        regs.LMI_SDMR0.poke(0x0033)
        regs.LMI_SDMR1.poke(0x0033)
    else:
        sttp.logging.print_out("Unknow the value of CAS, exit ...")
        return
    
    return


def init_lmi4_regs(regs):
    """
    Initialize the LMI4 registers before the initialization for DDR or SDR
    """
    global __mem_base_addr
    
    value = None
    data = None
    print_register = False
    
    ## SDRAM Mode Register
    ## Set Refresh Interval, Enable Refresh, 16-bit bus,
    ## Grouping Disabled, DDR-SDRAM, Enable.
    ## Bits[27:16]: Refresh Interval
    ## == 7.8 microseconds (8K/64ms)
    ## @  50MHz =  390 clk cycles -> 0x186
    ## @  75MHz =  585 clk cycles -> 0x249
    ## @ 100MHz =  780 clk cycles -> 0x30C
    ## @ 125MHz =  975 clk cycles -> 0x3CF
    ## @ 133MHz = 1040 clk cycles -> 0x410
    ## @ 166MHz = 1300 clk cycles -> 0x514
    ## == 15.625 microseconds (4K/64ms)
    ## @ 120MHz = 1875 clk cycles -> 0x753
    ## @ 133MHz = 2078 clk cycles -> 0x81E
    ## @ 160MHz = 2500 clk cycles -> 0x9C4

    dt = 1
    if SDRAM_DEVICE_TYPE == "ddr":
        dt = 1   ## DT=DDR SDRAM
    elif SDRAM_DEVICE_TYPE == "sdr":
        dt = 0   ## DT=PC SDRAM
    elif SDRAM_DEVICE_TYPE == "elpida_64mbits":
        dt = 1   ## DT=DDR SDRAM
    else:
        sttp.logging.print_out("SDRAM Type Unknown !! exit ...")
        return
    
    dce = 1    ## SDRAM controller is enabled
    ret  = 0   ## Retiming stage OFF
    comb16 = 0 ## grouping disable
    comb8  = 0 ## grouping disable
    bw = 0     ## Bus width is 16 bits
    endian = 0 ## Little endian memory configuration
    dre = 1    ## DRAM refresh enable
    dimm = 0   ## Data output delayed by 0 memory clock cycle and 0 memory clock cycle added to CAS latency
    by32ap = 0 ## MA8 pin is not used when LMI issues PRE or PALL commands
    dri = 0x61a##the maximum number of memory clock cycles between row refreshes
    frame = 0
    if SDRAM_FREQ_CONFIG == "133":
        if (SDRAM_SIZE_CONFIG == "512") or (SDRAM_SIZE_CONFIG == "256"):
            ## == 7.8 microseconds (8K/64ms)
            dri = 0x0410
        elif (SDRAM_SIZE_CONFIG == "128") or (SDRAM_SIZE_CONFIG == "64"):
            ## == 15.625 microseconds (4K/64ms, or 2K/32ms for elpida_64mbits)
            dri = 0x0820
    elif SDRAM_FREQ_CONFIG == "120":
        if (SDRAM_SIZE_CONFIG == "512") or (SDRAM_SIZE_CONFIG == "256"):
            ## == 7.8 microseconds (8K/64ms)
            dri = 0x03AC
        elif (SDRAM_SIZE_CONFIG == "128") or (SDRAM_SIZE_CONFIG == "64"):
            ## == 15.625 microseconds (4K/64ms, or 2K/32ms for elpida_64mbits)
            dri = 0x0760
    elif SDRAM_FREQ_CONFIG == "166":
        if (SDRAM_SIZE_CONFIG == "512") or (SDRAM_SIZE_CONFIG == "256"):
            ## == 7.8 microseconds (8K/64ms)
            dri = 0x0514
        elif (SDRAM_SIZE_CONFIG == "128") or (SDRAM_SIZE_CONFIG == "64"):
            ## == 15.625 microseconds (4K/64ms, or 2K/32ms for elpida_64mbits)
            dri = 0x0A22
    elif SDRAM_FREQ_CONFIG == "200":
        if (SDRAM_SIZE_CONFIG == "512") or (SDRAM_SIZE_CONFIG == "256"):
            ## == 7.8 microseconds (8K/64ms)
            dri = 0x061C
        elif (SDRAM_SIZE_CONFIG == "128") or (SDRAM_SIZE_CONFIG == "64"):
            ## == 15.625 microseconds (4K/64ms)
            dri = 0x0C35
    else:
        sttp.logging.print_out("Unknown Frequency of SDRAM, exit...")
        return

    value = (dce<<0) | (dt<<1) | (ret<<2) | (comb16<<3) | (comb8<<4) | (bw<<6) | (endian<<8) | (dre<<9) | (dimm<<10) | (by32ap<<11) | (dri<<16) | (frame<<28)
    regs.LMI_MIM0.poke(value)
    if print_register:
        sttp.logging.print_out("regs.LMI_MIM0.poke(0x%08x)" % value)
    
    if SDRAM_FREQ_CONFIG == "133":
        tRP   = 1      ## 3 clocks, 18ns -- ,             RAS_precharge
        tRCDR = 1      ## 3 clocks, 18ns -- ,             RAS_to_CAS_delay
        tRC   = 3      ## 9 cloks,  60ns -- ,             RAS cycle time
        tRAS  = 2      ## 6 clocks, 42ns -- 120000ns ,    RAS Active time
        tRRD  = 0      ## 2 clocks, 12ns -- ,             RAS_to_RAS_Active_delay
        tWR   = 1      ## 2 clocks, 15ns -- ,             Last write to PRE/PALL period SDRAM
        tRFC  = 4      ## 10 clocks,72ns -- ,             Auto Refresh RAS cycle time
        tWTR  = 0      ## 1 clock,  1ns  -- ,             Write to Read interruption, Twtr
        tRCDW = 1      ## 3 clocks, 18ns -- ,             RAS_to_CAS_delay
        tSXSR = 13     ## 200ns/16 ,                      Exit self-refresh to next command
    elif SDRAM_FREQ_CONFIG == "120":
        tRP   = 1      ## 3 clocks, 18ns -- ,             RAS_precharge
        tRCDR = 1      ## 3 clocks, 18ns -- ,             RAS_to_CAS_delay
        tRC   = 2      ## 8 cloks,  60ns -- ,             RAS cycle time
        tRAS  = 2      ## 6 clocks, 42ns -- 120000ns ,    RAS Active time
        tRRD  = 0      ## 2 clocks, 12ns -- ,             RAS_to_RAS_Active_delay
        tWR   = 1      ## 2 clocks, 15ns -- ,             Last write to PRE/PALL period SDRAM
        tRFC  = 4      ## 10 clocks,72ns -- ,             Auto Refresh RAS cycle time
        tWTR  = 0      ## 1 clock,  1ns  -- ,             Write to Read interruption, Twtr
        tRCDW = 1      ## 3 clocks, 18ns -- ,             RAS_to_CAS_delay
        tSXSR = 13     ## 200*tCK/16 ,                    Exit self-refresh to next command
    elif SDRAM_FREQ_CONFIG == "166":
        tRP   = 1      ## 4 clocks, 18ns -- ,             RAS_precharge
        tRCDR = 1      ## 4 clocks, 18ns -- ,             RAS_to_CAS_delay
        tRC   = 4      ## 10 cloks,  60ns -- ,             RAS cycle time
        tRAS  = 3      ## 7 clocks, 42ns -- 120000ns ,    RAS Active time
        tRRD  = 1      ## 3 clocks, 12ns -- ,             RAS_to_RAS_Active_delay
        tWR   = 2      ## 3 clocks, 15ns -- ,             Last write to PRE/PALL period SDRAM
        tRFC  = 6      ## 12 clocks,72ns -- ,             Auto Refresh RAS cycle time
        tWTR  = 0      ## 1 clock,  1ns  -- ,             Write to Read interruption, Twtr
        tRCDW = 2      ## 4 clocks, 18ns -- ,             RAS_to_CAS_delay
        tSXSR = 15     ## 200*tCK/16 ,                    Exit self-refresh to next command
    elif SDRAM_FREQ_CONFIG == "200":
        tRP   = 2      ## 4 clocks, 18ns -- ,             RAS_precharge
        tRCDR = 2      ## 4 clocks, 18ns -- ,             RAS_to_CAS_delay
        tRC   = 7      ## 13 cloks,  60ns -- ,             RAS cycle time
        tRAS  = 6      ## 10 clocks, 42ns -- 120000ns ,    RAS Active time
        tRRD  = 1      ## 3 clocks, 12ns -- ,             RAS_to_RAS_Active_delay
        tWR   = 3      ## 4 clocks, 15ns -- ,             Last write to PRE/PALL period SDRAM
        tRFC  = 9      ## 15 clocks,70ns -- ,             Auto Refresh RAS cycle time
        tWTR  = 1      ## 2 clock,  2tCK -- ,             Write to Read interruption, Twtr
        tRCDW = 3      ## 5 clocks, 20ns -- ,             RAS_to_CAS_delay
        tSXSR = 15     ## 200*tCK/16 ,                    Exit self-refresh to next command
    else:
        sttp.logging.print_out("Unknown Frequency of SDRAM, exit...")
        return

    if SDRAM_CAS_CONFIG == "2.0":
        sCL   = 2 ## 2 clocks,                       CAS Latency
    elif SDRAM_CAS_CONFIG == "2.5":
        sCL   = 10 ## 2.5 clocks,                     CAS Latency
    elif SDRAM_CAS_CONFIG == "3.0":
        sCL   = 3 ## 3 clocks,                       CAS Latency
    else:
        sttp.logging.print_out("Unknown the value of CAS, exit ...")
        return


    data = (tRP<<0) | (tRCDR<<2) | (tRC<<4) | (tRAS<<8) | (tRRD<<11) | ( (tWR & 0x1) <<12) | (sCL<<13) | (tRFC<<17) | ( ((tWR & 0x2)>>1) <<21) | (tWTR<<23) | (tRCDW<<24) | (tSXSR<<26)
    value = data | (1 << 22) ## Enable Write to Read interruption
    
    regs.LMI_STR.poke(value)
    if print_register:
        sttp.logging.print_out("regs.LMI_STR.poke(0x%08x)" % value)

    ## SDRAM Row Attribute 0 & 1 Registers
    ## UBA = Memory Size + Base Adr, Quad-bank, Shape 13x9,
    ## Bank Remapping Disabled
    ##
    if __mem_base_addr != None:
        mem_base_addr = __mem_base_addr << 24
    else:
        mem_base_addr = regs.LMI_VCR1.MEM_BASE_ADDR.read() << 24

    setup_sdra(regs, mem_base_addr, print_register)
    
    return

def setup_sdra(regs, mem_base_addr, print_register=False):
    value = 1L
    sttp.logging.print_out("Reload the memory base address = 0x%08x" % mem_base_addr)
    if SDRAM_SIZE_CONFIG == "64":
        if SDRAM_DEVICE_TYPE == "elpida_64mbits":
            value = 0x01001500 + mem_base_addr
        else:
            value = 0x00801400 + mem_base_addr
    elif SDRAM_SIZE_CONFIG == "128":
        value = 0x01001500 + mem_base_addr
    elif SDRAM_SIZE_CONFIG == "256":
        value = 0x02001900 + mem_base_addr
    elif SDRAM_SIZE_CONFIG == "512":
        value = 0x04001a00 + mem_base_addr
        
    regs.LMI_SDRA0.poke(value)
    if print_register:
        sttp.logging.print_out("regs.LMI_SDRA0.poke(0x%08x)" % value)
    
    ##  If we just have one Row connected to cs0, so we must program UBA0 = UBA1,
    ##  following LMI specification
    if MEMORY_1_SIZE != 0:
        if SDRAM_SIZE_CONFIG == "64":
            if SDRAM_DEVICE_TYPE == "elpida_64mbits":
                value = 0x01001500 + mem_base_addr
            else:
                value = 0x00801400 + mem_base_addr
        elif SDRAM_SIZE_CONFIG == "128":
            value = 0x01001500 + mem_base_addr
        elif SDRAM_SIZE_CONFIG == "256":
            value = 0x02001900 + mem_base_addr
        elif SDRAM_SIZE_CONFIG == "512":
            value = 0x04001a00 + mem_base_addr
        
        value += ((MEMORY_1_SIZE / 8) >> 1) << 21
    
    regs.LMI_SDRA1.poke(value)
    if print_register:
        sttp.logging.print_out("regs.LMI_SDRA1.poke(0x%08x)" % value)
