# TargetScript for connecting and disconnecting to a mb411 (stx7100/stx7109)

import sttp
import sttp.logging
import sttp.targetpack
import sttp.stmc

# TargetPack imports
import st40_pmb_regs
import stx710x
import tap
import tap.jtag
import utilities

# The default clock crystal fitted to each board type
extclk_defaults = {"mb411" : 27, "mb442" : 30, "mb448" : 27 }

def mb411_mb442_mb448_configure(parameters):
    # Apply core address offset.
    connect_core = sttp.targetinfo.get_targetstring_core()
    # Set a core specific view of the registers for features such as Memory Mapped Register Set
    if connect_core == "st40":
        sttp.targetpack.apply_address_offset("st40_offset")

def mb411_connect(parameters):
    mb411_mb442_mb448_connect(parameters)

def mb442_connect(parameters):
    mb411_mb442_mb448_connect(parameters)

def mb448_connect(parameters):
    mb411_mb442_mb448_connect(parameters)

def mb411_mb442_mb448_connect(parameters):
    root = sttp.targetpack.get_tree()
    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()

    board = mb411_mb442_mb448_get_board()
    sttp.logging.print_out("%s (%s) connect start - parameters %s" % (root.name, board, parameters))

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    p = sttp.stmc.get_target_params()
    # Add the board TargetPack parameters
    p.update(root.parameters)

    # Set tck for initialization
    utilities.set_tck_frequency(True, parameters)

    # Connect to stx710x SoC
    stx710x.connect(parameters)

    tap.jtag.check_soc_device_id(soc_family, cut)

    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        # Set ST40 view of register addresses during connect.
        sttp.targetpack.apply_address_offset("st40_offset")
        sttp.pokepeek.enable()
        cb = sttp.targetpack.get_callback("pre_poke")
        if cb:
            cb(parameters)

        sttp.logging.print_out("%s initialization start with SoC %s ..." % (board, soc_name,))

        cb = sttp.targetpack.get_callback("setup_pokes")
        if cb:
            cb(parameters)
        else:
            mb411_mb442_mb448_setup(parameters, board)

            # Default boot_companions to 0 if using romgen and it was not specified
            if "ROMGEN" == sttp.stmc.get_operating_mode() and (not parameters.has_key("boot_companions")):
                parameters["boot_companions"] = 0

            if parameters.has_key("boot_companions") and (int(parameters["boot_companions"]) == 0):
                pass
            else:
                stx710x.stx710x_boot_companions(parameters)

        cb = sttp.targetpack.get_callback("post_poke")
        if cb:
            cb(parameters)

    # Set tck for post-initialization - debug tools operation
    utilities.set_tck_frequency(False, parameters)

    # NEVER, EVER change TCK after calling this
    stx710x.complete_connect(parameters)

    sttp.logging.print_out("%s initialization complete" % (board,))


def mb411_mb442_mb448_disconnect():
    pass

# Configure the mb411
def mb411_mb442_mb448_setup(parameters, board):
    # Handle board specific parameters
    if parameters.has_key("extclk"):
        extclk = int(parameters["extclk"])
    else:
        extclk = extclk_defaults[board]

    sttp.pokepeek.rom_delay_scale(extclk)

    if not (parameters.has_key("no_clockgen_config") and (int(parameters["no_clockgen_config"]) == 1)):
        mb411_mb442_mb448_clockgen_configure(parameters, extclk)
        sttp.pokepeek.rom_delay_scale(266)
    mb411_mb442_mb448_sysconf_configure(parameters)
    mb411_mb442_mb448_emi_configure(board)
    mb411_mb442_mb448_lmivid_configure()

    # The mb442 may have 128MBytes of System RAM
    sys128 = False
    if board == "mb442":
        if parameters.has_key("sys128"):
            sys128 = int(parameters["sys128"]) == 1

    mb411_mb442_mb448_lmisys_configure(sys128)

    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()

    # For stx7109 cut 3 and above we default to moving the LMI configuration
    # registers to their alternative locations, unless explicitly told not to.
    move_lmi_regs = False
    if soc_name == "stx7109_cut30":
        move_lmi_regs = True
        if parameters.has_key("move_lmi_regs"):
            move_lmi_regs = int(parameters["move_lmi_regs"]) == 1

    if move_lmi_regs:
        soc.SYSCONF.stx710x_sysconf_regs.SYSCONF_SYS_CFG11.poke(0xad7fd4ea)
        # Update the addresses of all registers in the LMI configuration
        # registers to reflect the "moved" base addresses
        soc.LMISYS.st40_lmi3_regs.baseAddress = 0x1a800000
        soc.LMIVID.st40_lmi3_regs.baseAddress = 0x1b000000

    # For stx7109 cut 3 and above we default to moving the LMIs to their
    # alternative locations if in 32 bit mode, unless explicitly told not to.
    move_lmi_bases = False
    if soc_name == "stx7109_cut30":
        move_lmi_bases = True
        if parameters.has_key("move_lmi_bases"):
            move_lmi_bases = int(parameters["move_lmi_bases"]) == 1

    if parameters.has_key("se") and (int(parameters["se"]) == 1):
        mb411_mb442_mb448_switch_on_se("se", sys128, move_lmi_regs, move_lmi_bases)
    elif parameters.has_key("seuc") and (int(parameters["seuc"]) == 1):
        mb411_mb442_mb448_switch_on_se("seuc", sys128, move_lmi_regs, move_lmi_bases)
    elif parameters.has_key("se29") and (int(parameters["se29"]) == 1):
        mb411_mb442_mb448_switch_on_se("se29", sys128, move_lmi_regs, move_lmi_bases)

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        ccn = soc.st40.CCN.st40_ccn_regs
        ccn.CCN_CCR.poke(0x8000090d)

    if soc_name == "stx7100_cut11" or soc_name == "stx7100_cut13" or soc_name == "stx7100_cut20" or \
       soc_name == "stx7109_cut11":
        pio5 = soc.PIO5.st40_pio

        # Configure PIO5[6] for output at zero (USB2_PRT_OVRCUR workaround)
        pio5.PIO_CLEAR_PC0.poke(0x40)
        pio5.PIO_SET_PC1.poke(0x40)
        pio5.PIO_CLEAR_PC2.poke(0x40)
        pio5.PIO_CLEAR_POUT.poke(0x40)

# mb411 CLOCKGEN Configuration
def mb411_mb442_mb448_clockgen_configure(parameters, extclk):
    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()
    clockgena = soc.CLOCKGENA.stx710x_clockgena_regs

    if extclk == 27:
        # 27MHz external clock (the default on mb411 and mb448)

        # Set PLL0 to 531MHz
        mdiv_0 = clockgena.CLOCKGENA_PLL0_CFG.PLL0_MDIV.CLOCKOUT_531
        ndiv_0 = clockgena.CLOCKGENA_PLL0_CFG.PLL0_NDIV.CLOCKOUT_531
        pdiv_0 = clockgena.CLOCKGENA_PLL0_CFG.PLL0_PDIV.CLOCKOUT_531

        if soc_name == "stx7100_cut11":
            # Set PLL1 to 266MHz
            mdiv_1 = 0x1b
            ndiv_1 = 0x85
            pdiv_1 = 0x00
        elif soc_name == "stx7100_cut13" or soc_name == "stx7100_cut20" or soc_name == "stx7109_cut11":
            # Set PLL1 to 384MHz
            mdiv_1 = 0x09
            ndiv_1 = 0x80
            pdiv_1 = 0x01
        else:
            # Set PLL1 to 400MHz
            mdiv_1 = clockgena.CLOCKGENA_PLL1_CFG.PLL1_MDIV.CLOCKOUT_400
            ndiv_1 = clockgena.CLOCKGENA_PLL1_CFG.PLL1_NDIV.CLOCKOUT_400
            pdiv_1 = clockgena.CLOCKGENA_PLL1_CFG.PLL1_PDIV.CLOCKOUT_400

    elif extclk == 30:
        # 30MHz external clock - use "munged mdiv, ndiv and pdiv values)

        ## Set PLL0 to 531MHz
        mdiv_0 = 0x14
        ndiv_0 = 0xb1
        pdiv_0 = 0x00

        if soc_name == "stx7100_cut11":
            # Set PLL1 to 266MHz
            mdiv_1 = 0x1e
            ndiv_1 = 0x85
            pdiv_1 = 0x00
        elif soc_name == "stx7100_cut13" or soc_name == "stx7100_cut20" or soc_name == "stx7109_cut11":
            # Set PLL1 to 384MHz
            mdiv_1 = 0x0a
            ndiv_1 = 0x80
            pdiv_1 = 0x01
        else:
            ## Set PLL1 to 400MHz
            mdiv_1 = 0x03
            ndiv_1 = 0x28
            pdiv_1 = 0x01
    else:
        raise sttp.STTPException("Invalid extclk value %s" % (extclk,))

    stx710x.stx710x_set_clockgen_a_pll0(clockgena, mdiv_0, ndiv_0, pdiv_0)
    stx710x.stx710x_set_clockgen_a_pll1(clockgena, mdiv_1, ndiv_1, pdiv_1)


# mb411 SYSCONF Configuration
def mb411_mb442_mb448_sysconf_configure(parameters):
    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()
    sysconf = soc.SYSCONF.stx710x_sysconf_regs
    stbus   = soc.STBUS

    if soc_name.find("stx7100") == 0:
        sysconf.SYSCONF_SYS_CFG11.poke(0x0d7f80c0)
    elif soc_name == "stx7109_cut11":
        sysconf.SYSCONF_SYS_CFG11.poke(0x0d7f80c0)
    elif soc_name == "stx7109_cut20":
        sysconf.SYSCONF_SYS_CFG11.poke(0x0d7f80c0)
    elif soc_name == "stx7109_cut30":
        sysconf.SYSCONF_SYS_CFG11.poke(0x0d7fd4ea)

    sttp.pokepeek.debug_peek((1 << 9) | (1 << 19))
    sysconf.SYSCONF_SYS_STA12.while_and_ne((1 << 9) | (1 << 19), (1 << 9) | (1 << 19))

    sttp.pokepeek.debug_peek((1 << 9) | (1 << 19))
    sysconf.SYSCONF_SYS_STA13.while_and_ne((1 << 9) | (1 << 19), (1 << 9) | (1 << 19))

    sysconf.SYSCONF_SYS_CFG12.poke(0x4000000f | (0xf << 12) | (0xf << 23))
    sysconf.SYSCONF_SYS_CFG13.poke(0x4000000f | (0xf << 12) | (0xf << 23))

    if soc_name == "stx7100_cut11":
        sysconf.SYSCONF_SYS_CFG14.poke((1 << 18) | (0x5a << 20))
        sysconf.SYSCONF_SYS_CFG15.poke((1 << 19) | (0xa9 << 20))

        sysconf.SYSCONF_SYS_CFG20.poke((1 << 18) | (0x5a << 20))
        sysconf.SYSCONF_SYS_CFG21.poke((1 << 19) | (0xa9 << 20))
    elif soc_name == "stx7100_cut13":
        sysconf.SYSCONF_SYS_CFG14.poke((1 << 18) | (0x50 << 20))
        sysconf.SYSCONF_SYS_CFG15.poke((1 << 19) | (0x40 << 20))

        sysconf.SYSCONF_SYS_CFG20.poke((1 << 18) | (0x50 << 20))
        sysconf.SYSCONF_SYS_CFG21.poke((1 << 19) | (0x40 << 20))

    if soc_name.find("stx7100") == 0:
        stbus.NODE04.STBUS_NODE04_N02_PRIORITY.poke(0x1)


#  mb411 EMI Configuration
def mb411_mb442_mb448_emi_configure(board):
    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()
    emi = soc.EMI.st40_emi_regs

    #------------------------------------------------------------------------------
    # Re-program bank addresses
    #------------------------------------------------------------------------------
    emi.EMI_BANK_ENABLE.poke(0x00000005)

    # NOTE: bits [0,5] define bottom address bits [22,27] of bank
    emi.EMI_BANK0_BASEADDRESS.poke(0x00000000)
    emi.EMI_BANK1_BASEADDRESS.poke(0x00000004)
    emi.EMI_BANK2_BASEADDRESS.poke(0x00000008)
    emi.EMI_BANK3_BASEADDRESS.poke(0x0000000a)
    emi.EMI_BANK4_BASEADDRESS.poke(0x0000000c)

    #------------------------------------------------------------------------------
    # Bank 0 - On-board Flash - 16Mb
    #------------------------------------------------------------------------------
    emi.EMI_BANK0_EMICONFIGDATA0.poke(0x001016d1)
    emi.EMI_BANK0_EMICONFIGDATA1.poke(0x9d200000)
    emi.EMI_BANK0_EMICONFIGDATA2.poke(0x9d220000)
    emi.EMI_BANK0_EMICONFIGDATA3.poke(0x00000000)

    #------------------------------------------------------------------------------
    # Bank 1 - STEM Module (reset configuration not changed) undefined 16Mb
    #------------------------------------------------------------------------------

    #------------------------------------------------------------------------------
    # Bank 2 -
    #------------------------------------------------------------------------------
    if board == "mb411":
        # Bank 2 - DVB-CI 8Mb
        emi.EMI_BANK2_EMICONFIGDATA0.poke(0x002046f9)
        emi.EMI_BANK2_EMICONFIGDATA1.poke(0xa5a00000)
        emi.EMI_BANK2_EMICONFIGDATA2.poke(0xa5a20000)
        emi.EMI_BANK2_EMICONFIGDATA3.poke(0x00000000)
    elif board == "mb442":
        # Bank 2 - LAN91C111
        emi.EMI_BANK2_EMICONFIGDATA0.poke(0x042086f1)
        emi.EMI_BANK2_EMICONFIGDATA1.poke(0x88112111)
        emi.EMI_BANK2_EMICONFIGDATA2.poke(0x88112211)
        emi.EMI_BANK2_EMICONFIGDATA3.poke(0x00000000)
    elif board == "mb448":
        # Bank 2 - LAN91C111 - 8MB
        emi.EMI_BANK2_EMICONFIGDATA0.poke(0x042086f1)
        emi.EMI_BANK2_EMICONFIGDATA1.poke(0x8a002200)
        emi.EMI_BANK2_EMICONFIGDATA2.poke(0x8a004200)
        emi.EMI_BANK2_EMICONFIGDATA3.poke(0x00000000)

    #------------------------------------------------------------------------------
    # Bank 3 - ATAPI 8 Mb
    #------------------------------------------------------------------------------
    if soc_name == "stx7100_cut11":
        emi.EMI_BANK3_EMICONFIGDATA0.poke(0x00021791)
        emi.EMI_BANK3_EMICONFIGDATA1.poke(0x08004141)
        emi.EMI_BANK3_EMICONFIGDATA2.poke(0x08004141)
        emi.EMI_BANK3_EMICONFIGDATA3.poke(0x00000000)
    else:
        emi.EMI_BANK3_EMICONFIGDATA0.poke(0x00200791)
        emi.EMI_BANK3_EMICONFIGDATA1.poke(0x0c006700)
        emi.EMI_BANK3_EMICONFIGDATA2.poke(0x0c006700)
        emi.EMI_BANK3_EMICONFIGDATA3.poke(0x00000000)

    #------------------------------------------------------------------------------
    # Bank 4 -
    #------------------------------------------------------------------------------
    if board == "mb411":
        # Bank 4 - EPLD Registers and LAN91C111 16Mb
        emi.EMI_BANK4_EMICONFIGDATA0.poke(0x042086f1)
        emi.EMI_BANK4_EMICONFIGDATA1.poke(0x88112111)
        emi.EMI_BANK4_EMICONFIGDATA2.poke(0x88112211)
        emi.EMI_BANK4_EMICONFIGDATA3.poke(0x00000000)
    elif board == "mb442":
        # Bank 4 - Unused
        pass

    #------------------------------------------------------------------------------
    # Program other EMI registers
    #------------------------------------------------------------------------------
    if board == "mb448":
        emi.EMI_GENCFG.poke(0x00000030)
    elif soc_name == "stx7100_cut11" or soc_name == "stx7100_cut13" or soc_name.find("stx7109") == 0:
        emi.EMI_GENCFG.poke(0x00000010)
    else:
        emi.EMI_GENCFG.poke(0x00000050)


# mb411 LMI Configuration
def mb411_mb442_mb448_lmisys_configure(sys128):

    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()
    lmisys = soc.LMISYS.st40_lmi3_regs

    #------------------------------------------------------------------------------
    # Program LMI registers
    #------------------------------------------------------------------------------
    lmisys.LMI_MIM_0.poke(0x861a025f)
    lmisys.LMI_MIM_1.poke(0x01010022)

    lmisys.LMI_STR_0.poke(0x35b06455)

    if sys128:
        lmisys.LMI_SDRA0_0.poke(0x0c001a00)
        lmisys.LMI_SDRA1_0.poke(0x0c001a00)
    else:
        lmisys.LMI_SDRA0_0.poke(0x08001900)
        lmisys.LMI_SDRA1_0.poke(0x08001900)

    sttp.stmc.delay(200)

    lmisys.LMI_SCR_0.poke(0x00000001)
    lmisys.LMI_SCR_0.poke(0x00000003)
    lmisys.LMI_SCR_0.poke(0x00000001)
    lmisys.LMI_SCR_0.poke(0x00000002)
    lmisys.LMI_SDMR0.poke(0x00000402)
    lmisys.LMI_SDMR0.poke(0x00000133)

    sttp.stmc.delay(200)

    lmisys.LMI_SCR_0.poke(0x00000002)
    lmisys.LMI_SCR_0.poke(0x00000004)
    lmisys.LMI_SCR_0.poke(0x00000004)
    lmisys.LMI_SCR_0.poke(0x00000004)
    lmisys.LMI_SDMR0.poke(0x00000033)
    lmisys.LMI_SCR_0.poke(0x00000000)


def mb411_mb442_mb448_lmivid_configure():
    soc, soc_name, soc_family, cut = stx710x.get_stx710x_soc()
    lmivid = soc.LMIVID.st40_lmi3_regs

    #------------------------------------------------------------------------------
    # Program LMI registers
    #------------------------------------------------------------------------------
    lmivid.LMI_MIM_0.poke(0x861a025f)
    lmivid.LMI_MIM_1.poke(0x01010022)

    lmivid.LMI_STR_0.poke(0x35b06455)

    lmivid.LMI_SDRA0_0.poke(0x14001900)

    lmivid.LMI_SDRA1_0.poke(0x14001900)

    sttp.stmc.delay(200)

    lmivid.LMI_SCR_0.poke(0x00000001)
    lmivid.LMI_SCR_0.poke(0x00000003)
    lmivid.LMI_SCR_0.poke(0x00000001)
    lmivid.LMI_SCR_0.poke(0x00000002)
    lmivid.LMI_SDMR0.poke(0x00000402)
    lmivid.LMI_SDMR0.poke(0x00000133)

    sttp.stmc.delay(200)

    lmivid.LMI_SCR_0.poke(0x00000002)
    lmivid.LMI_SCR_0.poke(0x00000004)
    lmivid.LMI_SCR_0.poke(0x00000004)
    lmivid.LMI_SCR_0.poke(0x00000004)
    lmivid.LMI_SDMR0.poke(0x00000033)
    lmivid.LMI_SCR_0.poke(0x00000000)

"""
Get the "base" board name from the TargetXML
"""
def mb411_mb442_mb448_get_board():
    root = sttp.targetpack.get_tree()
    u = root.name.find("_")
    if u>0:
        board = root.name[0:u]
    else:
        board = root.name
    return board

# SE mode functions setup the PMBs based on the RAM fitted to the board

# stx710x switch to 32-bit SE mode

def mb411_mb442_mb448_se_mode_prep(sys128, move_lmi_bases):
    soc = sttp.targetpack.get_soc()
    sysconf = soc.SYSCONF.stx710x_sysconf_regs
    lmisys = soc.LMISYS.st40_lmi3_regs
    lmivid = soc.LMIVID.st40_lmi3_regs

    # Set LMI SYS and VID base addresses (in 16MByte multiples)
    if move_lmi_bases:
        sysbase, vidbase = (0x40, 0x60)
        # Poke LMI control register to move LMI base addresses to SE mode addresses
        sysconf.SYSCONF_SYS_CFG36.poke((sysconf.SYSCONF_SYS_CFG36.peek() & 0xff00ff00) | ((vidbase << 16) | sysbase))
    else:
        return (0x04, 0x10) # No re-configuration required

    # Change LMI upper bound addresses
    if sys128:
        lmisys.LMI_SDRA0_0.poke(((sysbase + 0x08) << 24) | 0x00001a00)
        lmisys.LMI_SDRA1_0.poke(((sysbase + 0x08) << 24) | 0x00001a00)
    else:
        lmisys.LMI_SDRA0_0.poke(((sysbase + 0x04) << 24) | 0x00001900)
        lmisys.LMI_SDRA1_0.poke(((sysbase + 0x04) << 24) | 0x00001900)
    lmivid.LMI_SDRA0_0.poke(((vidbase + 0x04) << 24) | 0x00001900)
    lmivid.LMI_SDRA1_0.poke(((vidbase + 0x04) << 24) | 0x00001900)

    return (sysbase, vidbase)

def mb411_mb442_mb448_switch_on_se(mode, sys128, move_lmi_regs, move_lmi_bases):
    sysbase, vidbase = mb411_mb442_mb448_se_mode_prep(sys128, move_lmi_bases)

    # Configure the PMBs
    st40_pmb_regs.st40_pmb_control_initialise()
    st40_pmb_regs.st40_clear_all_pmbs()
    if mode=="seuc":
        if sys128 and not move_lmi_bases:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64, 0, 0, 1)
            st40_pmb_regs.st40_set_pmb(7, 0x84, sysbase + 4, 64, 0, 0, 1)
        else:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64 + (64 * sys128), 0, 0, 1)
        st40_pmb_regs.st40_set_pmb(1, 0xa0, vidbase, 64, 0, 0, 1)
    elif mode=="se29":
        if sys128 and not move_lmi_bases:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64)
            st40_pmb_regs.st40_set_pmb(7, 0x84, sysbase + 4, 64)
            st40_pmb_regs.st40_set_pmb(1, 0x88, vidbase, 64)
        else:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64 + (64 * sys128))
            st40_pmb_regs.st40_set_pmb(1, 0x84 + (4 * sys128), vidbase, 64)
        if sys128 and not move_lmi_bases:
            st40_pmb_regs.st40_set_pmb(2, 0xa0, sysbase, 64, 0, 0, 1)
            st40_pmb_regs.st40_set_pmb(8, 0xa4, sysbase + 4, 64, 0, 0, 1)
            st40_pmb_regs.st40_set_pmb(3, 0xa8, vidbase, 64, 0, 0, 1)
        else:
            st40_pmb_regs.st40_set_pmb(2, 0xa0, sysbase, 64 + (64 * sys128), 0, 0, 1)
            st40_pmb_regs.st40_set_pmb(3, 0xa4 + (4 * sys128), vidbase, 64, 0, 0, 1)
    else: # Default is normal SE mode
        if sys128 and not move_lmi_bases:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64)
            st40_pmb_regs.st40_set_pmb(7, 0x84, sysbase + 4, 64)
        else:
            st40_pmb_regs.st40_set_pmb(0, 0x80, sysbase, 64 + (64 * sys128))
        st40_pmb_regs.st40_set_pmb(1, 0xa0, vidbase, 64)

    # An identity mapping for 'region 6' (peripherals)
    st40_pmb_regs.st40_set_pmb(4, 0xb8, 0x18, 64, 0, 0, 1)

    # Mappings for LMI config registers if they have not been moved
    if not move_lmi_regs:
        st40_pmb_regs.st40_set_pmb(5, 0xaf, 0x0f, 16, 0, 0, 1)
        st40_pmb_regs.st40_set_pmb(6, 0xb7, 0x17, 16, 0, 0, 1)

    # Switch to 32-bit SE mode
    st40_pmb_regs.st40_enhanced_mode(1)
