"""
STi8010 SoC specific functions
"""

import sttp
import sttp.targetpack

import tapmux

def connect(parameters):
    """
    Performs all STi5301 SoC connect actions
    Must be used prior to performing board-level intiialization
    """
    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    tapmux.connect(True, parameters)

def complete_connect(parameters):
    tapmux.complete_connect(parameters)

    
