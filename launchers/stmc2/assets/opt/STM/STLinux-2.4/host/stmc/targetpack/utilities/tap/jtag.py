import sttp
import sttp.jtag
import sttp.targetinfo

import convertor

_devid_id_soc_mappings = [
    (0x2d42c041, ("sti5202", "")),

    (0x2d401041, ("sti5512", "")),

    (0x0d42a041, ("stx5301", "")),

    (0x0d43f041, ("sti5197", "cut10")),
    (0x1d43f041, ("sti5197", "cut20")),

    (0x0d424041, ("stx7100", "cut13")),
    (0x1d424041, ("stx7100", "cut20")),
    (0x2d424041, ("stx7100", "cut31")),

    (0x0d43e041, ("stx7105", "cut10")),
    (0x1d43e041, ("stx7105", "cut20")),

    (0x0d42c041, ("stx7109", "cut11")),
    (0x1d42c041, ("stx7109", "cut20")),
    (0x2d42c041, ("stx7109", "cut30")),
    (0x2d42c041, ("stx7109", "cut40")),

    (0x0d43b041, ("stx7111", "cut10")),
    (0x1d43b041, ("stx7111", "cut20")),

    (0x0d43c041, ("stx7141", "cut10")),
    (0x1d43c041, ("stx7141", "cut20")),

    (0x0d437041, ("sti7200", "cut10")),
    (0x1d437041, ("sti7200", "cut20")),
    
    (0x05403041, ("stx8010", ""))
]

def add_device_id(devid, soc_name, cut=""):
    item = (devid, (soc_name, cut))
    _devid_id_soc_mappings.append(item)

"""
check_soc_device_id()
Ensures specified device if matches specified device type
If cut is not supplied any cut will satisfy the match

"""
def check_soc_device_id(base_type, cut=""):
    p = sttp.targetinfo.get_targetstring_parameters() 
    do_check = not ( p.has_key("no_devid_validate") and int(p["no_devid_validate"]) == 1 )    

    if not do_check:
        return

    if cut == None:
        cut = ""

    matched = False
    socs_with_devid = []
    id = sttp.targetinfo.get_target_info("device_id")

    if cut == "":
        # ignore cut
        masked_id = id & 0x0fffffff
    else:
        masked_id = id

    for info in _devid_id_soc_mappings:
        if cut == "":
           test_id = info[0] & 0x0fffffff
        else:
           test_id = info[0]

        if test_id == masked_id and info[1][0] == base_type:
            if cut == "":
                matched = True
                break
            elif cut == info[1][1]:
                matched = True
                break

        if id == info[0]:
            socs_with_devid.append(info[1][0] + " " + info[1][1])

        # print "id 0x%08x, masked_id 0x%08x, test_id 0x%08x, info %s" % (id, masked_id, test_id, info)

    if not matched:
        msg = "SoC is not a %s %s. The device id read (0x%08x) matches the following SoCs: %s" % (base_type, cut, id, socs_with_devid)
        raise sttp.STTPException(msg, False)

"""
bitstring_to_value()
Converts a string of ASCII 1s and 0s (as returned by sttp.jtag.sequence API) to an integer
"""

def bitstring_to_value(bitString, dir=1):
    value = 0

    if dir == 1:
        # "little endian" - least significant bit is the most left bit in the string
        while len(bitString)>0:
            value = value << 1
            bit = bitString[-1]
            if bit == "1":
                value = value | 1
            bitString = bitString[:len(bitString)-1]
    else:
        while len(bitString)>0:
            value = value << 1
            bit = bitString[0]
            if bit == "1":
                value = value | 1
            bitString = bitString[1:]

    return value

"""
value_to_bitstring()
convert an integer to a bit string primarily for use with stmc.jtag.sequence API
"""
def value_to_bitstring(value, length, dir=1):
    s = ""
    l = length
    while value != 0 and l > 0:
        if value & 1:
            s = "1" + s
        else:
            s = "0" + s
        value >>= 1
        l -= 1

    len_s = len(s)
    if len_s < length:
        s = "0" * (length-len_s) + s

    if dir == 1:
        # "little endian" - least significant bit is the most left bit in the string
        bitString = ""
        for i in range(len(s)):
            bitString = s[i] + bitString
    else:
        bitString = s

    return bitString


def read_dr(inst, inst_length, data, data_length):
    inst_bits = value_to_bitstring(inst, inst_length)
    data_bits = value_to_bitstring(data, data_length)

    go_to_rti()
    _shift_instruction(inst_bits)
    inputs = _shift_data(data_bits)
    
    result = 0
    if inputs:
        result = bitstring_to_value(inputs)

    return result


"""
read_device_id()

Reads the 32 bit device identifier with the 5 bit instruction 01000
"""
def read_device_id():

    # read device id - leave in Shift-DR
    vals = {'td_to_target' : "00000_00000_01000_000" + \
                             "00000_00000_00000_00000_00000_00000_00" + \
                             "000",
                     'tms' : "11111_01100_00001_110" + \
                             "00000_00000_00000_00000_00000_00000_00" + \
                             "110" }


    # Now just arrange for the data to be clocked out 

    inputs = sttp.jtag.sequence( vals )

    result = 0
    # a bit of extra error checking here incase sequence() does not 
    # behave as expected. In fact the "romgen" tool causes 
    # sequence to return an empy str type
    if type(inputs) == dict and inputs.has_key("td_from_target"):
        f = 19 + sttp.stmc.get_td_from_offset()
        result = bitstring_to_value(inputs["td_from_target"][f:f+32])

    # We could use the following but it is slower than the single sequence above
    # (several RPC calls instead of one)
    #  result = read_dr(2, 5, 0, 32)

    params = sttp.targetinfo.get_targetstring_parameters()
    
    if not params.has_key("no_devid_abort") and (result == 0 or result == 0xffffffff):
        raise sttp.STTPException("Read a device id of 0x%08x. Please check the connection to the target and ensure jtagpinout is specified correctly" % (result,), False)

    return result


"""
st20_enter_taplink_mode()
Moves the ST20 TAP from JTAG mode to TAPlink mode
Places the 5 bit instruction 00001 in the intruction register.
"""
def st20_enter_taplink_mode():
    # Setup for going into taplink mode with RESET This is a bit like st20 and may be no reset is best

    vals = {'td_to_target' : "00000_00000_00001_000",
                     'tms' : "11111_01100_00001_110"}

    sttp.jtag.sequence(vals)

    # Note that the normal st20 taplink software wiggle set up does some more clocks
    # These seem to be essential.

    vals = {'td_to_target' : "00000_00000_00000_00000_00000_00000_00",
                     'tms' : "00000_00000_00000_00000_00000_00000_00" }

    inputs = sttp.jtag.sequence( vals )

def setup_jtag_debug_port(parameters):

    convertor.check_convertor(parameters)

    sttp.targetinfo.setup_cores()

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    if sttp.stmc.get_soc_info()["debug"] == "tapmux":
        # Manual control of all JTAG lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        # Need this for TAPmux FPGA
        sttp.stmc.core_map({"tmc":"0"})
    else:

        # assume all initialized to 0 in driver
        p = "use_rtck"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_USE_RTCK", [ v ])

        p = "rtck_timeout"
        # default to 1
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 1
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_RTCK_TIMEOUT", [ v ])

        p = "tdo_sample_delay"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_TDO_SAMPLE_DELAY", [ v ])

    inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1"})

    # Now enable outputs
    sttp.stmc.output_enable(True)

    p = "vtref_check"
    # default to 0
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 0

    if v:
        # Check vtref - note VTREF can be read when LVDS outputs are disabled
        if 0 == int(inputs["vtref"]):
            raise sttp.STTPException("vtref signal on target JTAG connector is low - target may be disconnected or not powered")

    tap_reset = True
    sys_reset = True

    if parameters.has_key("no_sys_reset") and int(parameters["no_sys_reset"])==1:
        sys_reset = False

    if parameters.has_key("no_tap_reset") and int(parameters["no_tap_reset"])==1:
        tap_reset = False

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1:
        sys_reset = False
        tap_reset = False

    if parameters.has_key("reset_assert_period"):
        reset_assert_period = int(parameters["reset_assert_period"])
    else:
        reset_assert_period = 300000

    if parameters.has_key("trst_assert_period"):
        trst_assert_period = int(parameters["trst_assert_period"])
    else:
        trst_assert_period = 300000

    # Note sys reset is asserted while tap reset is performed
    if sys_reset:
        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        sttp.jtag.sequence({'nrst' : "0"})

        # What should the min delay be?
        sttp.stmc.delay(reset_assert_period)

        sttp.targetinfo.target_in_reset_state()

        sttp.logging.print_out("Performed system reset")
    else:
        # No reset case
        sttp.targetinfo.target_in_no_reset_state()

        sttp.logging.print_out("*NOT* performed system reset")

    if tap_reset:
        sttp.jtag.sequence({'ntrst' : "1"})
        sttp.jtag.sequence({'ntrst' : "0"})

        # What should the min delay be?
        sttp.stmc.delay(trst_assert_period)

        sttp.jtag.sequence({'ntrst' : "1"})
        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        # Move to RTI
        sttp.jtag.sequence({"tms": "111110",  "td_to_target": "000000"})

        sttp.logging.print_out("Performed TAP reset and moved to RTI")
    else:
        sttp.logging.print_out("*NOT* performed TAP reset")

    # Now deassert sys-reset
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
    sttp.jtag.sequence({'nrst' : "1"})
    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    if sttp.stmc.get_soc_info()["debug"] == "tapmux":
        # Need this for TAPmux driver
        # Plumb the 1st JTAG module to the output lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        c = sttp.targetinfo.get_targetstring_core()
        sttp.stmc.core_map({c:"0"})


_the_tmcs = {}
_initialized = False

def init(parameters):

    global _initialized
    global _the_tmcs

    if _initialized:
        return

    def find_tmcs(parent):
        #print "find_tmcs %s" % (parent)
        # if 0 == parent.name.find("tmc_"):
        if parent.parameters.has_key("tmc_type") and "tmc_" == parent.parameters["tmc_type"][0:4]:
            #print "Found tmc %s, %s" % (parent.name, parent.parameters)
            # Take a copy because run-time will damage the original making a call to 
            # stmc_library_deinitialize() and second call to stmc_library_initialize() fail
            _the_tmcs[parent.name] = dict(parent.parameters)

        for c in parent.get_children():
            find_tmcs(c)

    find_tmcs(sttp.targetpack.get_tree())

    # Special case for a single tmc - no need for chainpos and master to be defined in XML
    if 1 == len(_the_tmcs):
        t = _the_tmcs[_the_tmcs.keys()[0]]
        t["chainpos"] = 0
        t["master"] = 1

    _initialized = True

def tmc_ir_length_change(tmc_ids, new_ir_length):
    for k,v in _the_tmcs.iteritems():
        pos = int(str(v["chainpos"]))
        if pos in tmc_ids:
            v["ir_length"] = new_ir_length


def get_tmcs_by_index():
    if {} == _the_tmcs:
        init({})

    tmcs = {}
    master = None
    num_tmc = 0

    for k, v in _the_tmcs.iteritems():
        
        pos = int(str(v["chainpos"]))
        tmcs[pos] = v
        # add the name as we have lost it
        tmcs[pos]["name"] = k

        if v.has_key("master") and int(str(v["master"])):
            master = pos
        num_tmc += 1

    return tmcs, num_tmc, master


def get_tmc_by_name(name):
    if {} == _the_tmcs:
        init({})

    if not _the_tmcs.has_key(name):
        raise sttp.STTPException("Invalid tmc %s. Valid tmcs: %s" % (name, _the_tmcs.keys()))

    return _the_tmcs[name]


def tmc_shift_ir(irval, tmc_index=None):
    tmcs, num_tmc, master = get_tmcs_by_index()
    if None == tmc_index:
        tmc_index = master

    vals    = []
    lengths = []
    for i in range(num_tmc):
        if i == tmc_index:
            vals.append(irval)
        else:
            vals.append(0xFF)
        lengths.append(int(str(tmcs[i]["ir_length"])))

    r = shift_ir_set(vals, lengths)
    return r[tmc_index]

def tmc_shift_dr(drval, numbits, tmc_index=None):
    tmcs, num_tmc, master = get_tmcs_by_index()
    if None == tmc_index:
        tmc_index = master

    vals    = []
    lengths = []
    for i in range(num_tmc):
        if i == tmc_index:
            vals.append(drval)
            lengths.append(numbits)
        else:
            vals.append(0)
            lengths.append(1)

    r = shift_dr_set(vals, lengths)
    return r[tmc_index]

def tmc_shift_dr_with_udr(drval, numbits, udr_offset_in_tmc, tmc_index=None):
    tmcs, num_tmc, master = get_tmcs_by_index()
    if None == tmc_index:
        tmc_index = master

    vals        = []
    lengths     = []
    bypass_bits = 0
    udr_offet   = None

    for i in range(num_tmc):
        if i == tmc_index:
            vals.append(drval)
            lengths.append(numbits)
            udr_offet = udr_offset_in_tmc + bypass_bits
        else:
            vals.append(0)
            lengths.append(1)
            bypass_bits += 1


    r = shift_dr_set(vals, lengths, udr_offet)
    return r[tmc_index]

def _build_to_target(vals, lengths):
    to_str = ""
    
    for i in range(len(lengths)):
        to_str += value_to_bitstring(vals[i], lengths[i])
    return to_str

def _build_from_target(from_str, lengths):
    from_list = []
    p = 0
    for i in range(len(lengths)):
        v = bitstring_to_value(from_str[p:p+lengths[i]])
        p += lengths[i]
        from_list.append(v)
    return from_list

# precond : in RTI, post cond : in RTI    
def shift_ir_set(irs, lengths):
    ir_str = _build_to_target(irs, lengths)
    l = len(ir_str)
    vals = {'td_to_target' : "0000" + ir_str     + "00",
                     'tms' : "1100" + "0"*(l-1) + "110" }

    # Now just arrange for the data to be clocked out
    inputs = sttp.jtag.sequence( vals )

    f = 4 + sttp.stmc.get_td_from_offset()
    ir_from = inputs["td_from_target"][f:f+l]

    ir_from_list = _build_from_target(ir_from, lengths)
    return ir_from_list

# precond : in RTI, post cond : in RTI    
def shift_dr_set(drs, lengths, udr_offet=None):
    dr_str = _build_to_target(drs, lengths)
    # print "shift_dr_set     len dr_str", len(dr_str)
    if udr_offet:
        #print "shift_dr_set     drs[]", drs
        #print "shift_dr_set lengths[]", lengths
       
        pre  = dr_str[0:udr_offet]
        post = dr_str[udr_offet:]
        len_pre  = len(pre)
        len_post = len(post) 
        l = len(dr_str) + 3
        vals = {'td_to_target' : "000" + pre             +  "000" + post             +  "00",
                         'tms' : "100" + "0"*(len_pre-1) + "1010" + "0"*(len_post-1) + "110" }
        
        #print "len_pre:  ", len_pre
        #print "len_post: ", len_post
    else:
        l = len(dr_str) 
        vals = {'td_to_target' : "000" + dr_str     + "00",
                         'tms' : "100" + "0"*(l-1) + "110" }

    # Now just arrange for the data to be clocked out
    inputs = sttp.jtag.sequence( vals )

    f = 3 + sttp.stmc.get_td_from_offset()
    dr_from = inputs["td_from_target"][f:f+l]

    if udr_offet:
        dr_from = dr_from[0:udr_offet] + dr_from[udr_offet+3:]

    #if udr_offet:
    #    print "to_target:\n", vals
    #    print "fr_target:\n", inputs["td_from_target"]

    dr_from_list = _build_from_target(dr_from, lengths)
    return dr_from_list


def go_to_rti():
    # move to tlr then rti
    vals = {'td_to_target' : "00000_0",
                     'tms' : "11111_0" }

    # Now just arrange for the data to be clocked out 
    inputs = sttp.jtag.sequence( vals )
    return inputs

def _shift_instruction(val):
    # shift in/out instruction - leave in rti
    # precondition - in rti

    l = len(val)
    vals = {'td_to_target' : "0000" + val + "00", 
                     'tms' : "1100" + "0"*(l-1) + "110" }

    # Now just arrange for the data to be clocked out 
    inputs = sttp.jtag.sequence( vals )
    if type(inputs) == dict and inputs.has_key("td_from_target"):
        f = 4 + sttp.stmc.get_td_from_offset()
        return inputs["td_from_target"][f:f+l]
    else:
        return None


def _shift_data(val):
    # shift in/out data - leave in rti
    l = len(val)
    vals = {'td_to_target' : "000" + val + "00",
                     'tms' : "100" + "0"*(l-1) + "110" }

    # Now just arrange for the data to be clocked out 
    inputs = sttp.jtag.sequence( vals )

    if type(inputs) == dict and inputs.has_key("td_from_target"):
        f = 3 + sttp.stmc.get_td_from_offset()
        return inputs["td_from_target"][f:f+l]
    else:
        return None
