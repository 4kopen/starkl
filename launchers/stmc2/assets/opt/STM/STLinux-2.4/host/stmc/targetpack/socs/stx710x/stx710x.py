# Functions specific to the stx710x family

# Import ST supplied libraries
import sttp
import sttp.pokepeek
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import tapmux

def connect(parameters):
    """
    Performs all STx710x SoC connect actions
    Must be used prior to performing board-level intiialization
    """
    p = sttp.stmc.get_target_params()
    o, s, f, c = get_stx710x_soc()
    p["soc_name"] = s

    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    tapmux.connect(False, parameters)

def complete_connect(parameters):
    tapmux.complete_connect(parameters)

def stx710x_set_clockgen_a_pll0(clockgen, mdiv, ndiv, pdiv):
    sttp.pokepeek.debug_peek(0)

    # Unlock clockgen...
    clockgen.CLOCKGENA_LOCK.LCK.write(clockgen.CLOCKGENA_LOCK.LCK.UNLOCK)

    # Set CLOCKGENA PLL0 into BYPASS...
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_BYPASS.write(1)

    # Disable CLOCKGENA PLL0...
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_EN.write(0)

    # Configure CLOCKGENA PLL0...
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_MDIV.write(mdiv)
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_NDIV.write(ndiv)
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_PDIV.write(pdiv)

    # Enable CLOCKGENA PLL0...
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_EN.write(1)

    sttp.pokepeek.debug_peek(1)

    # Wait for CLOCKGENA PLL0 to lock...
    clockgen.CLOCKGENA_PLL0_STATUS.while_and_ne(0x00000001, 0x00000001)

    # Clear CLOCKGENA PLL0 from BYPASS...
    clockgen.CLOCKGENA_PLL0_CFG.PLL0_BYPASS.write(0)

    # Lock clockgen...
    clockgen.CLOCKGENA_LOCK.LCK.write(0x0)


def stx710x_set_clockgen_a_pll1(clockgen, mdiv, ndiv, pdiv):
    # when debugging, peek returns 0
    sttp.pokepeek.debug_peek(0)

    # Unlock clockgen...
    clockgen.CLOCKGENA_LOCK.LCK.write(clockgen.CLOCKGENA_LOCK.LCK.UNLOCK)

    # Set CLOCKGENA PLL1 into BYPASS...
    clockgen.CLOCKGENA_PLL1_BYPASS.PLL1_BYPASS.write(clockgen.CLOCKGENA_PLL1_BYPASS.PLL1_BYPASS.PLL_CLKOUT_BYPASS)

    # Disable CLOCKGENA PLL1...
    clockgen.CLOCKGENA_PLL1_CFG.PLL1_EN.write(0)

    # Configure CLOCKGENA PLL1...
    clockgen.CLOCKGENA_PLL1_CFG.PLL1_MDIV.write(mdiv)
    clockgen.CLOCKGENA_PLL1_CFG.PLL1_NDIV.write(ndiv)
    clockgen.CLOCKGENA_PLL1_CFG.PLL1_PDIV.write(pdiv)

    # Enable CLOCKGENA PLL1...
    clockgen.CLOCKGENA_PLL1_CFG.PLL1_EN.write(1)

    sttp.pokepeek.debug_peek(1)

    # Wait for CLOCKGENA PLL1 to lock...
    clockgen.CLOCKGENA_PLL1_STATUS.while_and_ne(0x00000001, 0x00000001)

    # Clear CLOCKGENA PLL1 from BYPASS...
    clockgen.CLOCKGENA_PLL1_BYPASS.PLL1_BYPASS.write(clockgen.CLOCKGENA_PLL1_BYPASS.PLL1_BYPASS.PLL_CLKOUT)

    # Lock clockgen...
    clockgen.CLOCKGENA_LOCK.LCK.write(0x0)


def stx710x_boot_companions(parameters):
    soc, soc_name, soc_family, cut = get_stx710x_soc()
    sysconf = soc.SYSCONF.stx710x_sysconf_regs

    sysconf.SYSCONF_SYS_CFG09.or_const(0x10000000)

    sttp.pokepeek.read_modify_write(sysconf.SYSCONF_SYS_CFG26,
                                    sysconf.SYSCONF_SYS_CFG27,
                                    0x00001ffe, 19, 0x00004001)

    sysconf.SYSCONF_SYS_CFG27.or_const(0x00000001)

    sysconf.SYSCONF_SYS_CFG27.and_const(~0x00000001)

    sttp.logging.print_out("%s: booted audio companion" % (soc_name))

    sysconf.SYSCONF_SYS_CFG09.or_const(0x10000000)

    sttp.pokepeek.read_modify_write(sysconf.SYSCONF_SYS_CFG28,
                                    sysconf.SYSCONF_SYS_CFG29,
                                    0x00001ffe, 19, 0x00004001)

    sysconf.SYSCONF_SYS_CFG29.or_const(0x00000001)

    sysconf.SYSCONF_SYS_CFG29.and_const(~0x00000001)

    sttp.logging.print_out("%s: booted video companion" % (soc_name))

def get_stx710x_soc():
    """
    get_stx710x_soc()
    Returns a tuple of info about the SoC:
      the STx710x object instance
      the complete STx710x SoC name
      the cut name of the STx710x"""
    soc = sttp.targetpack.get_soc()
    socName = soc.name

    family = socName
    cut = None
    cutIndex = socName.find("cut")
    if cutIndex > -1:
        cut = socName[cutIndex:]
        family = socName[:cutIndex-1] # ignore _ seperator

    return (soc, socName, family, cut)


