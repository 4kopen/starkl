# Script for board with typical single core jtag device

# TargetPack imports
import tap
import tap.jtag
import utilities

def jtag_connect(parameters):

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    frequency = 20000000;    

    real_tck = sttp.stmc.set_tck_frequency(frequency)

    print "jtag_connect: tck frequency requested %d Hz, set to %d Hz" % (frequency, real_tck)


    # This is a nasty hack to prevent a check that the crossbar is set prior to starting the CoreAgent
    # The "crossbar" is normally set up by the TargetPack prior to the CoreAgent starting
    # But in this case the ST20 CoreAgent sets the crossbar itself then communicates 
    # with the TMC and taplink directly !
    # The target param no_channel_validate must be set to 1 before setting the crossbar with sttp.stmc.core_map()
  
    p = sttp.stmc.get_target_params()
    p["no_channel_validate"] = "1"
    sttp.stmc.set_target_params(p)

    # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
    sttp.stmc.core_map({"tmc":"0"})

    # Manual control of all JTAG lines
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    # Now enable outputs
    sttp.stmc.output_enable(True)
 
    # Do a reset
    sttp.jtag.sequence({'nrst' : "1"})
    sttp.jtag.sequence({'nrst' : "0"})
    # What should the min delay be? Currently 100mS
    time.sleep(0.1)
    sttp.jtag.sequence({'nrst' : "1"})
    sttp.jtag.sequence({'ntrst' : "1"})

    # Autoclock of tck
    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    # Verify device id
    id = tap.jtag.read_device_id()
    print "Device id ", hex(id)

    # For this board, the single CoreAgent uses the TMC - do not change the crossbar  


def jtag_disconnect():
    pass

