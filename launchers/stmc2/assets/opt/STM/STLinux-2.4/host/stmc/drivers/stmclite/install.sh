#!/bin/sh

PATH=/usr/bin:/bin:/sbin
export PATH

stmcliterules=99-stmclite.rules

if [ `id -u` -ne 0 ]
then
	echo "root privileges required" 1>&2
	exit 1
fi

binroot=`dirname $0` && binroot=`cd $binroot && pwd`

if [ -d /etc/udev/rules.d -a -d /dev/bus/usb ]
then
	if version=`udevinfo -V 2> /dev/null`
	then
		udevreload="udevcontrol reload_rules"
	elif version=`udevadm --version 2> /dev/null`
	then
		udevreload="udevadm control --reload-rules"
	else
		echo "udev utilities required" 1>&2
		exit 1
	fi

	version=`expr "$version" : "[^0-9]*0*\([0-9]*\)"`
else
	echo "unsupported system configuration" 1>&2
	exit 1
fi


# UDEV version where ATTR/ATTRS attributes types replaced SYSFS is 098
if [ "$version" -lt 98 ]
then
	srcrules=$binroot/$stmcliterules.sysfs
else
	srcrules=$binroot/$stmcliterules
fi

dstrules=/etc/udev/rules.d/$stmcliterules

if cp -f $srcrules $dstrules 2> /dev/null
then
	if $udevreload 2> /dev/null
	then
		echo "$dstrules installed"
		exit 0
	else
		echo "udev rules not reloaded" 1>&2
	fi
else
	echo "$dstrules not installed" 1>&2
fi

rm -f $dstrules

exit 1
