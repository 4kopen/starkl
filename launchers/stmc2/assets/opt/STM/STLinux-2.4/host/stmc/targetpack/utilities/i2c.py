import sys
import sys
import time
import struct
import binascii

import sttp
import sttp.logging
import sttp.stmc
import sttp.targetpack
#
# Method naming conventions
#
# read_byte_string
#    returns a python string of byte values
#
# write_byte_string
#    takes python string of byte values
#
# read_byte
#    returns a python int containing the byte value
#
# write_byte
#    takes a python int containing the byte value
#
#
# read_byte_array
#    returns a python integer array of byte values
#
# write_byte_array
#    takes a python integer array of byte values
#

global sttp_root

global time_out

# default time out of 10 secs
time_out = 10

def set_time_out(t):
    global time_out
    time_out = t

class I2C:
    # These are my expected bit positions
    SCLEN = 8
    SCLDAEN = 4
    SCL = 2
    SDA = 1

    #Enums used for command types
    E_I2C_GET_INFO = 1
    E_I2C_SET_INFO = 2
    E_I2C_SEQUENCE = 3

    START     = 0xCEFF
    STOP      = 0xFFEC
    ALLFFS    = 0xFFFF
    ONE       = 0xCDFD
    ZERO      = 0xCCEC
    CLKACK    = 0xDDFD
    CLKACKPAD = 0xDDDD
    SENDACK   = 0xDECC
#
# create I2C phase information byte only
# 
    def _Makephases(self, value):
        bp0 = value & 3
        bp1 = (value>>2) & 3
        bp2 = (value>>4) & 3
        bp3 = (value>>6) & 3
        v = [self._Makephase(bp3), self._Makephase(bp2), self._Makephase(bp1), self._Makephase(bp0)]
        return v

    def _Makephase(self, phasebits):
        if phasebits == 0:
            ph = self._MakeInt((self.ZERO) + (self.ZERO<<16))
        elif phasebits == 1:
            ph = self._MakeInt((self.ZERO) + (self.ONE<<16))
        elif phasebits == 2:
            ph = self._MakeInt((self.ONE)  + (self.ZERO<<16))
        elif phasebits == 3:
            ph = self._MakeInt((self.ONE)  + (self.ONE<<16))
        return ph

    def _MakeInt(self, value):
        if 2147483647 == sys.__dict__["maxint"]:
            s = struct.pack("L", value)
            [v] = struct.unpack("i", s)
        else:
            # On a 64 bit machine
            v &= 0xffffffff
        return v

    def _convert_bs_2_byte(self, bytestring):
        data = 0
        if( bytestring[0] == 1):
            data += 128
        if( bytestring[1] == 1):
            data += 64
        if( bytestring[2] == 1):
            data += 32
        if( bytestring[3] == 1):
            data += 16
        if( bytestring[4] == 1):
            data += 8
        if( bytestring[5] == 1):
            data += 4
        if( bytestring[6] == 1):
            data += 2
        if( bytestring[7] == 1):
            data += 1
        return data

    def _extractrxbyte(self, v,offset):
        data = []
        bytedata = []
        for i in v:
            data.append(self._data_from_rxnib(i & 0xf))
            data.append(self._data_from_rxnib((i>>4) & 0xf))
            data.append(self._data_from_rxnib((i>>8) & 0xf))
            data.append(self._data_from_rxnib((i>>12) & 0xf))
            data.append(self._data_from_rxnib((i>>16) & 0xf))
            data.append(self._data_from_rxnib((i>>20) & 0xf))
            data.append(self._data_from_rxnib((i>>24) & 0xf))
            data.append(self._data_from_rxnib((i>>28) & 0xf))
        bytedata = data[offset + 0],data[offset + 4],data[offset + 8],data[offset + 12],data[offset + 16],data[offset + 20],data[offset + 24],data[offset + 28]
        return bytedata


    def _buildrxbytes(self, v):
        data = []
        bytedata = []
        for i in v:
            data.append(self._data_from_rxnib(i & 0xf))
            data.append(self._data_from_rxnib((i>>4) & 0xf))
            data.append(self._data_from_rxnib((i>>8) & 0xf))
            data.append(self._data_from_rxnib((i>>12) & 0xf))
            data.append(self._data_from_rxnib((i>>16) & 0xf))
            data.append(self._data_from_rxnib((i>>20) & 0xf))
            data.append(self._data_from_rxnib((i>>24) & 0xf))
            data.append(self._data_from_rxnib((i>>28) & 0xf))
        return data

    def _getrxbyte(self, data, offset):
        bytedata = data[offset + 0],data[offset + 4],data[offset + 8],data[offset + 12],data[offset + 16],data[offset + 20],data[offset + 24],data[offset + 28]
        return bytedata

    def _Makereadphases(self):
        rph = [self._MakeInt((self.CLKACK)  + (self.CLKACK<<16)),self._MakeInt((self.CLKACK)  + (self.CLKACK<<16)), self._MakeInt((self.CLKACK)  + (self.CLKACK<<16)),  self._MakeInt((self.CLKACK)  + (self.CLKACK<<16))]
        return rph

    def _data_from_rxnib(self, nib):
        data = 0
        if (nib & 1) == 1 :
            data = 1
        return data


    # return the value of the data sample at offset for count data bits
    # Assumes the data is in normal return format and is large enough to contain all requested bits.
    def _i2c_data_from_offset(self, v,offset,count):
        data = []
        databits = []
        for i in v:
            data.append(self._data_from_rxnib(i & 0xf))
            data.append(self._data_from_rxnib((i>>4) & 0xf))
            data.append(self._data_from_rxnib((i>>8) & 0xf))
            data.append(self._data_from_rxnib((i>>12) & 0xf))
            data.append(self._data_from_rxnib((i>>16) & 0xf))
            data.append(self._data_from_rxnib((i>>20) & 0xf))
            data.append(self._data_from_rxnib((i>>24) & 0xf))
            data.append(self._data_from_rxnib((i>>28) & 0xf))
        for i in range(count):
           databits.append(data[offset + (i*4)])
        return data[offset]

    def _print_full_clkndata(self, full_cnd):
        clkens =""
        dataens =""
        clks =""
        datas ="" 
        for i in full_cnd:
            clken, dataen, clk, data = i
            sclken = "%d" % clken
            sdataen = "%d" % dataen
            sclk = "%d" % clk
            sdata = "%d" % data
            clkens = clkens + sclken
            dataens = dataens + sdataen
            clks = clks + sclk
            datas = datas + sdata

        ackbit0 = datas[54]
        pclks = clks[16:]
        pdatas = datas[16:]
        ackbit1 = pdatas[39]
        ackbit2 = pdatas[40]
        ackbit3 = pdatas[41]
        sttp.logging.print_out("clocks en  %s"  % clkens)
        sttp.logging.print_out("data   en  %s"  % dataens)
        sttp.logging.print_out("clocks     %s"  % clks)
        sttp.logging.print_out("data       %s"  % datas)
        sttp.logging.print_out("clocks     %s"  % pclks)
        sttp.logging.print_out("data       %s"  % pdatas)
        sttp.logging.print_out("ack bit0  %s "  %  ackbit0)
        sttp.logging.print_out("ack bit1  %s "  %  ackbit1)

    # returns clken, dataen, clk, data specified in inout nibble
    def _full_analyse_nib(self, nib):
        clken = 0
        dataen = 0
        clk = 0
        data = 0
        if (nib & 1) == 1 :
            data = 1
        if (nib & 2) == 2 :
            clk = 1
        if (nib & 4) == 4 :
            dataen = 1
        if (nib & 8) == 8 :
            clken = 1
        return clken, dataen, clk, data

    def _full_analyse_i2c(self, v):
        clkndata = []
        for i in v:
            clkndata.append(self._full_analyse_nib(i & 0xf))
            clkndata.append(self._full_analyse_nib((i>>4) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>8) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>12) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>16) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>20) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>24) & 0xf))
            clkndata.append(self._full_analyse_nib((i>>28) & 0xf))
        return clkndata

    def __init__(self, info):
        self.parent = None
        self.info = info

    def dump(self):
        sttp.logging.print_out("dump output for  name %s" % self.info["name"])
        sttp.logging.print_out(" __dict__" , self.__dict__)
        sttp.logging.print_out("End dump output for  name %s \n" % self.info["name"])

    def add(self, instance):
        self.__dict__[instance.info["name"]] = instance
 
        if instance.info.has_key("unique_name"):
            self.__dict__[instance.info["unique_name"]] = instance

        instance.parent=self

    def set_route(self):
        route=[]
        route.append(self)
        parent = self.parent
        while parent:
            route.insert(0,parent)
            parent = parent.parent
        i = 0
        while i < len(route) :
            # If the class has the method _connect_bus() and we are not the final object in the list
            # assume it is something like a PCA9544 which needs a channel connection made
            if ( "_connect_bus" in dir(route[i]) ) and ( (i+1)<len(route) ):
                c = route[i+1].id
                route[i]._connect_bus(c)
                i = i+2
            else :
                i = i+1
# end of class I2C

class I2CROOT(I2C):

    def __init__(self, info):
        I2C.__init__(self, info)
    def dump(self):
        sttp.logging.print_out("This is the i2c root")

# end of class I2CROOT(I2C)

class PCA9544(I2C):
    # PCA9544A addresses
    PCA9544ABASE = 0xE0
    PCA9544A0 = PCA9544ABASE
    PCA9544A1 = PCA9544ABASE + 2
    PCA9544A2 = PCA9544ABASE + 4
    PCA9544A3 = PCA9544ABASE + 6
    PCA9544A4 = PCA9544ABASE + 8
    PCA9544A5 = PCA9544ABASE + 10
    PCA9544A6 = PCA9544ABASE + 12
    PCA9544A7 = PCA9544ABASE + 14

    enabled = 0
    chan = 0
    route = []

    def _i2c_configure_pca9544(self, saddress, conf):
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(saddress) # as a write
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makephases(conf) # as a write
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        offset = xx[1] - 24
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        return  self._i2c_data_from_offset(cmdresult,55,1)

    def _i2c_read_pca9544(self, saddress):
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(saddress | 1) # as a read
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        offset = xx[1] - 24
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        bytevalue = self._extractrxbyte(cmdresult, offset)
        return self._convert_bs_2_byte(bytevalue)

    def detect(self):
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            return True
        self.set_route()
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(self.info["address"] | 1) # as a read
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        offset = xx[1] - 24
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        ack = ( self._i2c_data_from_offset(cmdresult,55,1))
        if 0==ack:
            return True
        else:
            return False

    def csr_read_byte(self):
        return self._i2c_read_pca9544(self.info["address"])

    class CHANNEL(I2C):
        def __init__(self, id):
            self.id = id
            I2C.__init__(self, {"name" : "ch"+str(id)})
        def dump(self):
            sttp.logging.print_out(" channel %d " % self.id)
    #end subclass CHANNEL(I2C):

    def __init__(self, info):
        I2C.__init__(self, info)
        self.devicename = "pca9544" 
        for i in range(4):
            self.add(self.CHANNEL(i))
        self.channel = None


    def _write_csr(self, value):
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            return 1
        self._i2c_configure_pca9544(self.info["address"], value)
        return 1
 
    def _connect_bus(self, channel):
        if None != self.channel and self.channel == channel :
            return
        enabled = 4
        csr = enabled + channel
        self._write_csr(csr)
        self.channel = channel

    def _disconnect_bus(self ):
        enabled = 0
        chan = 0

    def dumpstatus(self ):
        sttp.logging.print_out("Route " , self.route)
        sttp.logging.print_out("Device name %s" % self.devicename)
        if(self.enabled == 1) :
            sttp.logging.print_out("Device enabled connected to channel  %d " % self.chan)
        else :
            sttp.logging.print_out("Device disabled")

# end class PCA9544(I2C):

class PCA9558(I2C):
    # PCA9558 addresses
    PCA9558ABASE = 0x9C
    PCA9558A0 = PCA9558ABASE
    PCA9558A1 = PCA9558ABASE + 2

    PCA9558_CMD_READ_IP = 0x07
    PCA9558_CMD_OPR = 0x08
    PCA9558_CMD_PIR = 0x09
    PCA9558_CMD_IOC = 0x0A
    PCA9558_CMD_MUXCNTRL = 0x0B
    PCA9558_CMD_READ_MUXIN = 0x0C
    PCA9558_CMD_WRITE_256EE = 0x01
    PCA9558_CMD_READ_256EE = 0x03
    PCA9558_CMD_WRITE_6BEE = 0x04
    PCA9558_CMD_READ_6BEE = 0x06
    PCA9558_6BEE_ADDR = 0xFF

    MUX_OUT_A = 0
    MUX_OUT_B = 1
    MUX_OUT_C = 2
    MUX_OUT_D = 3
    MUX_OUT_E = 4
    NON_MUX   = 5
 
    MUX_IN_A  = 0
    MUX_IN_B  = 1
    MUX_IN_C  = 2
    MUX_IN_D  = 3
    MUX_IN_E  = 4        

    def detect(self):
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            return True
        self.set_route()
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(self.info["address"] | 1) # as a read
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        offset = xx[1] - 24
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        ack = ( self._i2c_data_from_offset(cmdresult,55,1))
        if 0==ack:
            return True
        else:
            return False

    class EE6B(I2C):

        # Write to 6 bit EEPROM in PCA9558
        # Only the data and device address are required
        def    _i2c_sequence_PCA9558_write_6BEE(self, saddress, databyte):
            xx = [self.E_I2C_SEQUENCE, 0]
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) # as a write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(self.parent.PCA9558_CMD_WRITE_6BEE)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(self.parent.PCA9558_6BEE_ADDR)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(databyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            xx[1] = (len(xx) -2) * 8
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
            return 0

        ######################################
        #  Read from 6 bit EEPROM in PCA9558
        #  Only the device address is required
        def    _i2c_sequence_PCA9558_read_6BEE(self, saddress):
            # Build transaction
            xx = [self.E_I2C_SEQUENCE, 0]
            # Slave address
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) # as a write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # Command byte
            mkp = self._Makephases(self.parent.PCA9558_CMD_READ_6BEE)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # 6BEE address
            mkp = self._Makephases(self.parent.PCA9558_6BEE_ADDR)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
            # Slave address
            mkp = self._Makephases(saddress +1 ) # as a read
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # Data from 6BEE
            mkp = self._Makereadphases()
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            # Set nibble count
            xx[1] = (len(xx) -2) * 8
            offset = xx[1] - 24
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )

            backward_bit_list = self._extractrxbyte(cmdresult, offset)

            bytevalue = 0
            for i in range(6):
                j = 6 - i - 1 + 2
                bytevalue |= backward_bit_list[j] * (2 ** i)
            return bytevalue

        def __init__(self):
            I2C.__init__(self, {"name" : "ee6b"})

        def dump(self):
            sttp.logging.print_out(" EE6B ")

        def write_byte(self, data):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            return self._i2c_sequence_PCA9558_write_6BEE(self.parent.info["address"], data)

        def read_byte(self):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return 0
            self.set_route()
            return self._i2c_sequence_PCA9558_read_6BEE(self.parent.info["address"])
    # end class EE6B(I2C):

    class GPIO(I2C):

        def _i2c_sequence_PCA9558_write_gpio(self, saddress, cmdbyte, databyte):
            xx = [self.E_I2C_SEQUENCE, 0]
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(cmdbyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(databyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            xx[1] = (len(xx) -2) * 8
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )


        def _i2c_sequence_PCA9558_read_gpio(self, saddress, cmdbyte):
            xx = [self.E_I2C_SEQUENCE, 0]
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) #as write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(cmdbyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
        # Slave address
            mkp = self._Makephases(saddress +1 ) # as a read
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        # Data from inputport
            mkp = self._Makereadphases()
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        # Set nibble count
            xx[1] = (len(xx) -2) * 8
            offset = xx[1] - 24
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
            bytevalue = self._extractrxbyte(cmdresult, offset)
            return bytevalue


        def __init__(self):
            I2C.__init__(self, {"name" : "gpio"})

        def dump(self):
            sttp.logging.print_out(" GPIO ")

        def write_reg_byte(self, reg, value):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return 
            self.set_route()
            return self._i2c_sequence_PCA9558_write_gpio(self.parent.info["address"], reg, value)

        def write_byte(self, value):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            return self._i2c_sequence_PCA9558_write_gpio(self.parent.info["address"], self.parent.PCA9558_CMD_OPR, value)

        def read_reg_byte(self, reg):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return 0
            self.set_route()
            return self._i2c_sequence_PCA9558_read_gpio(self.parent.info["address"], reg)

        def read_byte(self):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return 0
            self.set_route()
            return self._i2c_sequence_PCA9558_read_gpio(self.parent.info["address"], self.parent.PCA9558_CMD_READ_IP)

    # end subclass GPIO(I2C)

    class EE256(I2C):

        def __init__(self):
            I2C.__init__(self, {"unique_name" : "ee256", "name" : "ee_mem"})

        def dump(self):
            sttp.logging.print_out(" EE256 ")

        def get_size(self):
            return 256

        ######################################
        #  Read from 256 byte EEPROM in PCA9558
        #  The device address and the start address in 256 byte memory and the number of bytes to read are required
        def _i2c_sequence_PCA9558_read_256EE(self, saddress, address, count):

            if count < 1:
                raise sttp.STTPException("Error: count %d not permitted" % (count,))

            # Build transaction
            xx = [self.E_I2C_SEQUENCE, 0]
            # Slave address
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) # as a write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # Command byte
            mkp = self._Makephases(self.parent.PCA9558_CMD_READ_256EE)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # address
            mkp = self._Makephases(address)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
            # Slave address
            mkp = self._Makephases(saddress +1 ) # as a read
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

            # Data from 256EE
          
            if (count == 1):
                mkp = self._Makereadphases()
                for i in mkp:
                    xx.append(i)
                xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
                offsetincrement = 0
            else:
                for num in range(count):
                    mkp = self._Makereadphases()
                    for i in mkp:
                        xx.append(i)
                    if(num == count -1 ):
                        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                    else:
                        xx.append(self._MakeInt((self.SENDACK) + (self.CLKACKPAD<<16)  ))
                    if(num == 0):
                        offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
                    if(num == 1):
                        offset2 = ((len(xx) -2) * 8) -24   # grab next offset for return data bits
                offsetincrement = offset2 - offset

            xx[1] = (len(xx) -2) * 8

            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )

            resultStr = ""

            byteData = self._buildrxbytes(cmdresult)

            for i in range (0,count):
                bytevalue = self._getrxbyte(byteData, offset + (i*offsetincrement))
                resultStr += chr(self._convert_bs_2_byte(bytevalue))
            
            return resultStr

        ######################################
        #  Write to 256 byte EEPROM in PCA9558
        #  The device address and address in 256 byte memory and the data are required the number of bytes are obtained from the data len
        def _i2c_sequence_PCA9558_write_256EE(self, saddress, address, data, count):
            # Build transaction
            xx = [self.E_I2C_SEQUENCE, 0]
            # Slave address
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) # as a write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

            # Command byte
            mkp = self._Makephases(self.parent.PCA9558_CMD_WRITE_256EE)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # address
            mkp = self._Makephases(address)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

            # Data bytes
            for num in range(count):
                mkp = self._Makephases( ord(data[num]) )
                for i in mkp:
                    xx.append(i)
                if(num == count -1 ):
                    xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                else:
                    xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
 
            # Set nibble count
            xx[1] = (len(xx) -2) * 8
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )

        def write_simple(self, address, data, count):
            return self._i2c_sequence_PCA9558_write_256EE(self.parent.info["address"], address, data, count)

        # Write must take care of pages 
        def write_byte_string(self, address, data, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            bytestodo = count
            bytesdone = 0
            currentpage = address & 0xf0
            offsetinpage = address & 0x0f
            bytesinpage = 0
            pagesize = 16
            wdata = data
            waddress = address
            # align to page boundary
            if bytestodo > 0:
                if(bytestodo >= (pagesize - offsetinpage)):
                    bytesinpage = pagesize - offsetinpage
                    self._i2c_sequence_PCA9558_write_256EE(self.parent.info["address"], waddress, wdata, bytesinpage)
                    bytestodo = bytestodo - bytesinpage
                    bytesdone =  bytesinpage
                    wdata = data[bytesdone:]
                    waddress += bytesinpage

            start_t = time.time()
            while self.parent.detect() == False:
                now_t = time.time()
                if start_t + time_out > now_t: 
                    raise sttp.STTPException("Error: Write to ee256 failed to complete")

            # Any more bytes after aligning to page boundary
            while bytestodo >= pagesize :
                self._i2c_sequence_PCA9558_write_256EE(self.parent.info["address"], waddress, wdata, pagesize)
                bytestodo = bytestodo - pagesize
                bytesdone +=  pagesize
                wdata = data[bytesdone:]
                waddress += pagesize

                start_t = time.time()
                while self.parent.detect() == False:
                    now_t = time.time()
                    if start_t + time_out > now_t: 
                        raise sttp.STTPException("Error: Write to ee256 failed to complete")

            # Any more bytes after doing aligned writes
            if bytestodo > 0:
                self._i2c_sequence_PCA9558_write_256EE(self.parent.info["address"], waddress, wdata, bytestodo)
                bytesdone +=  bytestodo
                bytestodo = bytestodo - bytestodo

            start_t = time.time()
            while self.parent.detect() == False:
                now_t = time.time()
                if start_t + time_out > now_t: 
                    raise sttp.STTPException("Error: Write to ee256 failed to complete")

        def write_byte_array(self, address, data, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            sdata = ""
            for i in range(count):
                sdata += chr(data[i])
            self.write_byte_string(address, sdata, count)

        # Read can read all data in one go -- from device perspective may be python limitation if size of packet is too big
        def read_byte_string(self, address, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return chr(0) * count
            self.set_route()
            return self._i2c_sequence_PCA9558_read_256EE(self.parent.info["address"], address, count)

        def read_byte_array(self, address, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return [chr(0)] * count
            sdata = self._i2c_sequence_PCA9558_read_256EE(self.parent.info["address"], address, count)
            data = []
            for i in range (count):
                ord(data.append(sdata[i]))
            return data

    # end subclass EE256(I2C)


    class MUX_INX(I2C):
        def _i2c_sequence_PCA9558_read_mux_inx(self, saddress, cmdbyte):
            xx = [self.E_I2C_SEQUENCE, 0]
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress) #as write
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(cmdbyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
        # Slave address
            mkp = self._Makephases(saddress +1 ) # as a read
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        # Data from inputport
            mkp = self._Makereadphases()
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        # Set nibble count
            xx[1] = (len(xx) -2) * 8
            offset = xx[1] - 24
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
            bytevalue = self._extractrxbyte(cmdresult, offset)
            return bytevalue

        def read_byte(self):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return 0
            self.set_route()
            return self._i2c_sequence_PCA9558_read_mux_inx(self.parent.info["address"], self.parent.PCA9558_CMD_READ_MUXIN)
        def __init__(self):
            I2C.__init__(self, {"name" : "mux_inx"})

    # end subclass MUX_INX(I2C)

    def __init__(self, info):
        I2C.__init__(self, info)
        self.devicename = "pca9558" 
        self.add(self.EE6B())
        self.add(self.EE256())
        self.add(self.GPIO())
        self.add(self.MUX_INX())

# end class PCA9558(I2C):


class AD5252(I2C):
    #
    # AD5252 addresses
    #
    AD5252ABASE = 0x58
    AD5252A0 = AD5252ABASE
    AD5252A1 = AD5252ABASE + 2
    AD5252A2 = AD5252ABASE + 4
    AD5252A3 = AD5252ABASE + 6
    RDAC1 = 1
    RDAC3 = 3

    @staticmethod
    def get_instance(index):
        instances = [ AD5252.RDAC1, AD5252.RDAC3 ]
        return instances[index]

    def get_resistor_info(self, instance):

        toldata = []

        ee_addresses  = { AD5252.RDAC1 : AD5252.EEMEM.RDAC1_ADDR,
                          AD5252.RDAC3 : AD5252.EEMEM.RDAC3_ADDR
                        }

        tol_addresses = { AD5252.RDAC1 : AD5252.EEMEM.RDAC1_TOL_ADDR,
                          AD5252.RDAC3 : AD5252.EEMEM.RDAC3_TOL_ADDR
                        }

        reg_addresses = { AD5252.RDAC1 : AD5252.REG.RDAC1_ADDR,
                          AD5252.RDAC3 : AD5252.REG.RDAC3_ADDR
                        }
        ee_a  = ee_addresses[instance]
        reg_a = reg_addresses[instance]
        tol_a = tol_addresses[instance]

        ee      = self.eemem.read_byte_array(ee_a, 1)
        toldata = self.eemem.read_byte_array(tol_a, 2)
        reg     = self.reg.read_byte_array(reg_a, 1)

        sign    = 1.0
        if (toldata[0] & 0x80): 
            sign = -1.0

        tol = ((toldata[0] & 0x7F)*sign) + (toldata[1]*pow(2,-8))

        resistor_info = {}
        resistor_info["non_volatile"] = ee[0]
        resistor_info["volatile"]     = reg[0]
        resistor_info["tolerance"]    = tol

        return resistor_info


    def set_resistor_non_volatile(self, instance, data, length):

        addresses = { AD5252.RDAC1 : AD5252.EEMEM.RDAC1_ADDR,
                      AD5252.RDAC3 : AD5252.EEMEM.RDAC3_ADDR
                    }
        a = addresses[instance]  
  
        self.eemem.write_byte_array(a, [data], length)


    def set_resistor_volatile(self, instance, data, length):
        addresses = { AD5252.RDAC1 : AD5252.REG.RDAC1_ADDR,
                      AD5252.RDAC3 : AD5252.REG.RDAC3_ADDR
                    }
        a = addresses[instance]  

        self.reg.write_byte_array(a, [data], length)


    def detect(self ):
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            return True
        self.set_route()
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(self.info["address"] | 1) # as a read
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        ack = ( self._i2c_data_from_offset(cmdresult,55,1) )
        if 0 == ack:
            return True
        else:
            return False


    ######################################
    #  Read from AD5252
    #  The device address and the start address in memory and the number of bytes to read are required
    #  Note that the address contains extra high order information to distinguish between EE data and register data

    def _i2c_read_ad5252(self, saddress, address, count):
        resultdata = []
    # Build transaction
        xx = [self.E_I2C_SEQUENCE, 0]
    # Slave address
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp = self._Makephases(saddress) # as a write
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
    # Command byte includes address
        mkp = self._Makephases(address)
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
    # Slave address
        mkp = self._Makephases(saddress +1 ) # as a read
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

    # Data from device
        if (count == 1):
            mkp = self._Makereadphases()
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
            offsetincrement = 0
        else:
            for num in range(count):
                mkp = self._Makereadphases()
                for i in mkp:
                    xx.append(i)
                if(num == count -1 ):
                    xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                else:
                    xx.append(self._MakeInt((self.SENDACK) + (self.CLKACKPAD<<16)  ))
                if(num == 0):
                    offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
                if(num == 1):
                    offset2 = ((len(xx) -2) * 8) -24   # grab next offset for return data bits
            offsetincrement = offset2 - offset

        xx[1] = (len(xx) -2) * 8

        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )

        for i in range (0,count):
            bytevalue = self._extractrxbyte(cmdresult, offset + (i*offsetincrement))
            resultdata.append(self._convert_bs_2_byte(bytevalue))
        return resultdata


    def _i2c_write_ad5252(self, saddress, address, data, count) :
        if ( count != len(data)) :
            raise sttp.STTPException("Error: count and length of data not the same")
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(saddress ) # as a write
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makephases(0x00 + address )  # For write top 3 bits are 0
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

        count = len(data)

    # Data bytes
        for num in range(count):
            mkp = self._Makephases( data[num] )
            for i in mkp:
                xx.append(i)
            if(num == count -1 ):
                xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            else:
                xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

    # Set nibble count
        xx[1] = (len(xx) -2) * 8
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )

    class EEMEM(I2C):
        RDAC1_ADDR = 1
        RDAC3_ADDR = 3
        RDAC1_TOL_ADDR = 0x1A
        RDAC3_TOL_ADDR = 0x1E

        def write_byte_array(self, address, data, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            acmd = 0x20 + (address & 0x1F) 
            return self.parent._i2c_write_ad5252(self.parent.info["address"], acmd, data, count)

        def read_byte_array(self, address, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return [chr(0)] * count
            self.set_route()
            acmd = 0x20 + (address & 0x1F)
            return self.parent._i2c_read_ad5252(self.parent.info["address"], acmd, count)

        def __init__(self):
            I2C.__init__(self, {"name" : "eemem"})
    # end subclass EEMEM(I2C)

    class REG(I2C):
        RDAC1_ADDR = 1
        RDAC3_ADDR = 3
        def write_byte_array(self, address, data, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            acmd = 0x00 + (address & 0x1F) 
            return self.parent._i2c_write_ad5252(self.parent.info["address"], acmd, data, count)

        def read_byte_array(self, address, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return [chr(0)]*count
            self.set_route()
            acmd = 0x00 + (address & 0x1F)
            return self.parent._i2c_read_ad5252(self.parent.info["address"], acmd, count)

        def __init__(self):
            I2C.__init__(self, {"name" : "reg"})
    # end subclass REG(I2C)

    def __init__(self, info):
        I2C.__init__(self, info)
        self.devicename = "ad5252"
        self.add(self.EEMEM())
        self.add(self.REG())
# end class AD5252(I2C)

class LTC2991(I2C):
    # LTC2991  addresses
    LTC2991ABASE = 0x90
    LTC2991A0 = LTC2991ABASE
    LTC2991A1 = LTC2991ABASE + 2
    LTC2991A2 = LTC2991ABASE + 4
    LTC2991A3 = LTC2991ABASE + 6
    LTC2991A4 = LTC2991ABASE + 8
    LTC2991A5 = LTC2991ABASE + 10
    # addresses 0x9C and 0x9E conflict PCA9558
    V1 = 0
    V2 = 1
    V3 = 2
    V4 = 3
    V5 = 4
    V6 = 5
    V7 = 6
    V8 = 7

    def detect(self):
        if sttp.stmc.get_operating_mode() == "ROMGEN":
            return True
        self.set_route()
        xx = [self.E_I2C_SEQUENCE, 0]
        xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
        mkp_sa = self._Makephases(self.info["address"] | 1) # as a read
        for i in mkp_sa:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
        mkp = self._Makereadphases()
        for i in mkp:
            xx.append(i)
        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
        xx[1] = (len(xx) -2) * 8
        cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
        ack = ( self._i2c_data_from_offset(cmdresult,55,1) )
        if 0 == ack:
            return True
        else:
            return False

    def get_param(self, param):
        return  self.info[param]

    class A2D(I2C):
        ######################################
        #  Read from LTC2991 multiple bytes
        #  The device address, command, start address and the number of bytes to read are required
        def _i2c_sequence_LTC2991_read(self, saddress, address, count):
            resultdata = []
            # Build transaction
            xx = [self.E_I2C_SEQUENCE, 0]
            # Slave address
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp_sa = self._Makephases(saddress) # as a write
            for i in mkp_sa:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            # address
            mkp = self._Makephases(address)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.START<<16)  ))
            # Slave address
            mkp = self._Makephases(saddress + 1) # as read
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))

            # Data from LTC2991
            if (count == 1):
                mkp = self._Makereadphases()
                for i in mkp:
                    xx.append(i)
                xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
                offsetincrement = 0
            else:
                for num in range(count):
                    mkp = self._Makereadphases()
                    for i in mkp:
                        xx.append(i)
                    if(num == count -1 ):
                        xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
                    else:
                        xx.append(self._MakeInt((self.SENDACK) + (self.CLKACKPAD<<16)  ))
                    if(num == 0):
                        offset = ((len(xx) -2) * 8) -24   # grab starting offset for return data bits
                    if(num == 1):
                        offset2 = ((len(xx) -2) * 8) -24   # grab next offset for return data bits
                offsetincrement = offset2 - offset

            xx[1] = (len(xx) -2) * 8


            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )


            for i in range (0,count):
                bytevalue = self._extractrxbyte(cmdresult, offset + (i*offsetincrement))
                resultdata.append(self._convert_bs_2_byte(bytevalue))
            return resultdata

        def _i2c_sequence_LTC2991_write_command(self, saddress, cmdbyte, databyte):
            xx = [self.E_I2C_SEQUENCE, 0]
            xx.append(self._MakeInt((self.ALLFFS) + (self.START<<16) ))
            mkp = self._Makephases(saddress)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(cmdbyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.CLKACKPAD<<16)  ))
            mkp = self._Makephases(databyte)
            for i in mkp:
                xx.append(i)
            xx.append(self._MakeInt((self.CLKACK) + (self.STOP<<16)  ))
            xx[1] = (len(xx) -2) * 8
            cmdresult = sttp.stmc.ioctlBuffer("STMC_BASE_I2C_CONTROL", xx )
            ack = ( self._i2c_data_from_offset(cmdresult,55,1) )
            if ack == 0 :
                return 1
            else :
                return 0

        def write_byte_array(self, address, data):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return
            self.set_route()
            return self._i2c_sequence_LTC2991_write_command(self.parent.info["address"], address, data)

        def read_byte_array(self, address, count):
            if sttp.stmc.get_operating_mode() == "ROMGEN":
                return [chr(0)] * count
            self.set_route()
            return self._i2c_sequence_LTC2991_read(self.parent.info["address"], address, count)

        def __init__(self):
            I2C.__init__(self, {"name" : "a2d"})
    # end sub class A2D

    def __init__(self, info):
        I2C.__init__(self, info)
        self.devicename = "ltc2991"
        self.add(self.A2D())
#end class LTC2991(I2C):
