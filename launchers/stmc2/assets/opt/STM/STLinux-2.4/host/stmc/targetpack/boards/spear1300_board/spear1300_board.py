import sttp
import sttp.stmc

import st200_nomux

import utilities
import tap
import tap.jtag

import spear1300

# Script for board with typical single core jtag device


def spear1300_board_connect(parameters):

    sttp.logging.print_out("spear1300_board_connect() parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    utilities.set_tck_frequency(False, parameters)

    spear1300.connect(parameters)

    # Poke/peek initialization not currently supported
    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        sttp.logging.print_out("spear1300_board_connect() initialization start ...")
        spear1300_board_initall()

    spear1300.complete_connect(parameters)


def spear1300_board_initall():
    # Do poke/peek stuff here
    #    sttp.pokepeek.enable()
    pass
