"""
Utilities for SoCs with st40 cores
"""
import os

import sttp
import sttp.stmc
import sttp.targetpack
import sttp.targetinfo

import core_utils
    
"""
Note: the code in this file sets up core parameters that are read by the ST40
toolset and are specific in name and value to this toolset

The parameter "resettype" has the following possible values:

  jtag  - a "wire" reset sequence has been performed (with nAsebrk)
          the core is suspended awaiting JTAG "commands" to load code
          into ASE memory and execute it
  none  - the core is spinning in ASE mode code monitoring a control
          word
  break - the core is executing user code (possibly booted from FLASH)
  
"""

debugPrefix = None

def setup_reset_state(coreName):

    global debugPrefix
    debugPrefix = core_utils.setup_debug("st40 (%s)" % (coreName,))

    core_utils.debug(debugPrefix, ">>>> setup_reset_state(%s)" % (coreName,) )

    try:
        c = sttp.stmc.get_core_params(coreName)
    except:
        c = dict()

    # "jtag" - the JTAG reset sequence has been done (nRST/nASEBRK wiggles)
    c["resettype"] = "jtag"

    setup_udisync_cludge(c)
    
    core_utils.setup_default_timeout(debugPrefix, c)
    setup_pre_pokepeek_state(coreName)
    setup_post_pokepeek_state(coreName)
    
    sttp.stmc.set_core_params(coreName, c)

    core_utils.debug(debugPrefix, "    sttp.stmc.set_core_params(%s, %s)" % (coreName, c))
    core_utils.debug(debugPrefix, "<<<< setup_reset_state()")

def setup_no_reset_state(coreName):

    global debugPrefix
    debugPrefix = core_utils.setup_debug("st40 (%s)" % (coreName,) )

    core_utils.debug(debugPrefix, ">>>> setup_no_reset_state(%s)" % (coreName,))

    try:
        c = sttp.stmc.get_core_params(coreName)
    except:
        c = dict()

    # "break" - try and interrupt the application and place the core in ASE mode
    c["resettype"] = "break"

    setup_udisync_cludge(c)

    core_utils.setup_default_timeout(debugPrefix, c)
    setup_pre_pokepeek_state(coreName)
    setup_post_pokepeek_state(coreName)

    sttp.stmc.set_core_params(coreName, c)

    core_utils.debug(debugPrefix, "<<<< setup_no_reset_state()")

def setup_pre_pokepeek_state(coreName):
    core_utils.debug(debugPrefix, ">>>> setup_pre_pokepeek_state(%s)" % (coreName,))

    def PrePokePeekFn():
        core_utils.debug(debugPrefix, ">>>> PrePokePeekFn(%s)" % (coreName,))

        # If this core has not been used for poke/peek do not update state
        if not core_utils.coreIsPokePeek(debugPrefix, coreName):
            core_utils.debug(debugPrefix, "<<<< PrePokePeekFn()")
            return

        try:
            c = sttp.stmc.get_core_params(coreName)
        except:
            c = dict()

        if c["resettype"] == "break":
            # about to start pokepeek agent on a running system - leave it running after
            c["ondisconnect"] = "restart"

        # setup the timeout
        core_utils.set_pokepeek_coreagent_timeout(debugPrefix, coreName, c)

        setup_udisync_cludge(c, True)

        sttp.stmc.set_core_params(coreName, c)
        core_utils.debug(debugPrefix, "     sttp.stmc.set_core_params(%s, %s)" % (coreName, c))
        core_utils.debug(debugPrefix, "<<<< PrePokePeekFn()")

    core_utils.debug(debugPrefix, ">>>> setup_pre_pokepeek_state()")
    sttp.stmc.register_pokepeek_enable_callback(PrePokePeekFn)
    core_utils.debug(debugPrefix, "<<<< setup_pre_pokepeek_state()")


def setup_post_pokepeek_state(coreName):
    core_utils.debug(debugPrefix, ">>>> setup_post_pokepeek_state(%s)" % (coreName,))

    def PostPokePeekFn():
        core_utils.debug(debugPrefix, ">>>> PostPokePeekFn(%s)" % (coreName,))

        # If this core has not been used for poke/peek do not update state
        if not core_utils.coreIsPokePeek(debugPrefix, coreName):
            core_utils.debug(debugPrefix, "<<<< PostPokePeekFn()")
            return
        try:
            c = sttp.stmc.get_core_params(coreName)
        except:
            c = dict()

        # Timeout set for poke peek operation only.
        if c.has_key("linktimeout"):
            del c["linktimeout"]
        if c.has_key("linktimeout_original"):
            c["linktimeout"] = c["linktimeout_original"]

        # Do not need this param
        if c.has_key("ondisconnect"):
            del c["ondisconnect"]

        if c["resettype"] == "jtag":
            # We started the agent from a reset - now spinning in ASE mode
            c["resettype"] = "none"
        elif c["resettype"] == "none":
            # We started the agent when spinning in ASE mode - now spinning in ASE mode
            c["resettype"] = "none"
        else:
            # We started the agent on a running system and leave it running
            c["resettype"] = "break"

        setup_udisync_cludge(c)

        sttp.stmc.set_core_params(coreName, c)
        core_utils.debug(debugPrefix, "     sttp.stmc.set_core_params(%s, %s)" % (coreName, c))
        core_utils.debug(debugPrefix, "<<<< PostPokePeekFn()")
        
    core_utils.debug(debugPrefix, ">>>> setup_post_pokepeek_state()")
    sttp.stmc.register_pokepeek_disable_callback(PostPokePeekFn)
    core_utils.debug(debugPrefix, "<<<< setup_post_pokepeek_state()")


def setup_udisync_cludge(cp, poll=False):
    # Some cludgery for the STMC_Lite - after pokepeeks have completed
    tp = sttp.targetinfo.get_targetstring_parameters() # Mastered on host.
    if not tp.has_key("udisync_default"):
        if "STMC1" == sttp.stmc.get_stmc_type():
            # why oh why is this so horrid?
            if poll:
                cp["udisync"] = "poll"
            else:
                cp["udisync"] = "nopoll"
            cp["udisyncdelay"] = "96"
        else:
            cp["udisync"] = "autopoll"
    

