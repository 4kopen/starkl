import tap.jtag
import sttp.logging

DEBUG_IR_DR = 1 << 0
DEBUG_DP    = 1 << 1
DEBUG_MEMAP = 1 << 2
debug = 0 # DEBUG_MEMAP | DEBUG_DP | DEBUG_IR_DR

JTAG_WAIT     = 1
JTAG_OK_FAULT = 2
JTAG_INST_DPACC = 0xA
JTAG_INST_APACC = 0xB
JTAG_INST_IDCODE = 0xE
JTAG_INST_SIZE = 4
JTAG_WRITE = 0
JTAG_READ = 1

JTAG_DP_CTRL_STAT = 0x4
JTAG_DP_SELECT = 0x08
JTAG_DP_RDBUFF = 0x0C
JTAG_DP_TARGETID = 0x24
JTAG_DP_DLPIDR = 0x34
JTAG_DP_EVENTSTAT = 0x44

JTAG_MEM_AP_CSW = 0x00
JTAG_MEM_AP_TAR = 0x04
JTAG_MEM_AP_TAR_1 = 0x08
JTAG_MEM_AP_DRW = 0x0C
JTAG_MEM_AP_IDR = 0xFC
JTAG_MEM_AP_CFG = 0xF4
JTAG_MEM_AP_BASE_0 = 0xF8
JTAG_MEM_AP_BASE_1 = 0xF0

JTAG_MEM_BD0  = 0x10
JTAG_MEM_BD1  = 0x14
JTAG_MEM_BD2  = 0x18
JTAG_MEM_BD3  = 0x1C

lastIR = None
lastSELECT = 0xFF0000F0
lastTAR = None


def TapAccessIR_RW(cp, ir, ir_len):
    global lastIR

    if debug & DEBUG_IR_DR:
        sttp.logging.print_out(">>>> TapAccessIR_RW(ir=0x%x ir_len=%d)" % (ir, ir_len,))

    if lastIR == ir:
        return

    ir_return = tap.jtag.shift_ir_set([0, ir, 0], [int(cp["ir_bits_before"]), ir_len, int(cp["ir_bits_after"])])

    lastIR = ir

    return ir_return[1]

# Only for the compatibility with arm_a9.py
def TapAccessIR_RW_core(cp, core, ir, ir_len):
    return TapAccessIR_RW(cp, ir, ir_len)

def TapAccessDR_RW(cp, dr, dr_len):
    if debug & DEBUG_IR_DR:
        sttp.logging.print_out(">>>> TapAccessDR_RW(dr=0x%x dr_len=%d)" % (dr, dr_len,))

    dr_return = tap.jtag.shift_dr_set([0, dr, 0], [int(cp["taps_before"]), dr_len, int(cp["taps_after"])])

    return dr_return[1]

# Only for the compatibility with arm_a9.py
def TapAccessDR_RW_core(cp, core, dr, dr_len):
    return TapAccessDR_RW(cp, dr, dr_len)

""" TapAccessDR_RW_DAP
Perform the DR path for APACC/DPACC instructions inside the JTAG-DP.
"""
def TapAccessDR_RW_DAP(cp, regidx, value, read_not_write):
    dr = (value << 3) | (((regidx & 0xF) >> 2) << 1) | read_not_write
    dr_len = 35

    dr_return = 0
    #starttime = time.time()
    # while (((dr_return & 0x7) != JTAG_OK_FAULT) and (time.time() - starttime < target_timeout)):
    if 1:
        dr_return = TapAccessDR_RW(cp, dr, dr_len)

    if (dr_return & 0x7) != JTAG_OK_FAULT:
        raise sttp.STTPException("ERROR: Response != OK_FAULT: dr_return=0x%x, dr=0x%x" % (dr_return, dr,))

    return (dr_return >> 3) & 0xFFFFFFFF

""" ReadWriteDP
Access DP registers
"""
def ReadWriteDP(cp, regidx, value, read_not_write):
    global lastSELECT

    if debug & DEBUG_DP:
        sttp.logging.print_out(">>>> ReadWriteDP(regidx=0x%02x, value=0x%08x, read_not_write=%d)" % (regidx, value, read_not_write,))

    # Select the DP bank if regidx & 0x0F is 0x04
    DPBank = (regidx & 0xF0) >> 4
    if (regidx & 0x0F) == 0x04 and (lastSELECT & 0x0F) != DPBank: 
        w = (lastSELECT & ~0xF) | DPBank
        #WriteDP_SELECT(cp, w)
        if debug & DEBUG_DP: sttp.logging.print_out(">>>> WriteDP_SELECT(value=0x%08x)" % (w,))
        ir_return = TapAccessIR_RW(cp, JTAG_INST_DPACC, JTAG_INST_SIZE)
        dr_return = TapAccessDR_RW_DAP(cp, JTAG_DP_SELECT, w, JTAG_WRITE)
        lastSELECT = w

    # access DP register
    ir_return = TapAccessIR_RW(cp, JTAG_INST_DPACC, JTAG_INST_SIZE)
    dr_return = TapAccessDR_RW_DAP(cp, regidx, value, read_not_write)
    if read_not_write:
    #    dr_return = ReadDP_RDBUFF(cp)
        if debug & DEBUG_DP: sttp.logging.print_out(">>>> ReadDP_RDBUFF()")
        ir_return = TapAccessIR_RW(cp, JTAG_INST_DPACC, JTAG_INST_SIZE)
        dr_return = TapAccessDR_RW_DAP(cp, JTAG_DP_RDBUFF, 0, JTAG_READ)
        if debug & DEBUG_DP: sttp.logging.print_out("<<<< ReadDP_RDBUFF() dr=0x%08x" % (dr_return,))

    if debug & DEBUG_DP:
        sttp.logging.print_out("<<<< ReadWriteDP() dr=0x%08x" % (dr_return,))

    return dr_return

""" ReadWriteMemAP
Access AP registers.
"""
def ReadWriteMemAP(cp, ap, regidx, value, read_not_write, single_read=1):
    global lastSELECT

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out(">>>> ReadWriteMemAP(ap=%d, regidx=0x%02x, value=0x%08x, read_not_write=%d)" % (ap, regidx, value, read_not_write,))

    # Select the AP instance and bank
    w = (ap << 24) | (regidx & 0xF0) | (lastSELECT & 0xF)
    #WriteDP_SELECT(cp, w)
    if lastSELECT != w:
        if debug & DEBUG_DP: sttp.logging.print_out(">>>> WriteDP_SELECT(value=0x%08x)" % (value,))
        ir_return = TapAccessIR_RW(cp, JTAG_INST_DPACC, JTAG_INST_SIZE)
        dr_return = TapAccessDR_RW_DAP(cp, JTAG_DP_SELECT, w, JTAG_WRITE)
        lastSELECT = w

    ir_return = TapAccessIR_RW(cp, JTAG_INST_APACC, JTAG_INST_SIZE)
    dr_return = TapAccessDR_RW_DAP(cp, regidx, value, read_not_write)
    if (read_not_write and (0==single_read)):
        # /* Read twice ! */
        dr_return = TapAccessDR_RW_DAP(cp, regidx, value, read_not_write)
    
    if read_not_write:
        #dr_return = ReadDP_RDBUFF(cp)
        if debug & DEBUG_DP: sttp.logging.print_out(">>>> ReadDP_RDBUFF()")
        ir_return = TapAccessIR_RW(cp, JTAG_INST_DPACC, JTAG_INST_SIZE)
        dr_return = TapAccessDR_RW_DAP(cp, JTAG_DP_RDBUFF, 0, JTAG_READ)
        if debug & DEBUG_DP: sttp.logging.print_out("<<<< ReadDP_RDBUFF() dr=0x%08x" % (dr_return,))

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out("<<<< ReadWriteMemAP() dr=0x%08x" % (dr_return,))

    return dr_return

# Only for the compatibility with arm_a9.py
def ReadWriteMemAP_core(cp, core, ap, regidx, value, read_not_write, single_read=1):
        return ReadWriteMemAP(cp, ap, regidx, value, read_not_write, single_read)

""" ReadMemAPLocation
Read a 32bits memory location.
The memory is connected to the AP given by "ap".
"""
def ReadMemAPLocation(cp, ap, address):
    global lastTAR

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out(">>>> ReadMemAPLocation(ap=%d, address=0x%08x)" % (ap, address,))

    # Read the data
    # Update TAR if different from last address
    if lastTAR != address:
        ReadWriteMemAP(cp, ap, JTAG_MEM_AP_TAR, address, JTAG_WRITE)
        lastTAR = address
    result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_DRW, 0, JTAG_READ)

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out("<<< ReadMemAPLocation() = 0x%08x" % (result,))

    return result

""" WriteMemAPLocation
Write a 32bits memory location.
The memory is connected to the AP given by "ap".
"""
def WriteMemAPLocation(cp, ap, address, value):
    global lastTAR

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out(">>> WriteMemAPLocation(ap=%d, address=0x%08x, value=0x%08x)" % (ap, address, value,))

    # Write the data
    # Update TAR if different from last address
    if lastTAR != address:
        ReadWriteMemAP(cp, ap, JTAG_MEM_AP_TAR, address, JTAG_WRITE)
        lastTAR = address
    ReadWriteMemAP(cp, ap, JTAG_MEM_AP_DRW, value, JTAG_WRITE)

    if debug & DEBUG_MEMAP:
        sttp.logging.print_out("<<< WriteMemAPLocation()")

# Only for the compatibility with arm_a9.py
def Read(cp, core, ap, addr):
    if debug & DEBUG_MEMAP:
        sttp.logging.print_out(">>> Read(ap %s, addr 0x%x, cp %s, core %s)\n" % (ap, addr, cp, core))
    val = ReadMemAPLocation(cp, ap, addr);
    if debug & DEBUG_MEMAP:
        sttp.logging.print_out("<<< Read() = 0x%x\n" % (val,))
    return val

# Only for the compatibility with arm_a9.py
def Write(cp, core, ap, addr, value):
    if debug & DEBUG_MEMAP:
         sttp.logging.print_out(">>> Write(ap %s, addr 0x%x, value 0x%x, cp %s, core %s)\n" % (ap, addr, value, cp, core,))
    result = WriteMemAPLocation(cp, ap, addr, value)
    if debug & DEBUG_MEMAP:
        sttp.logging.print_out("<<< Write() = 0x%x\n" % result)

def read_cs(core, addr):
    cp = sttp.stmc.get_core_params(core)    

#    sttp_root = sttp.targetpack.get_tree()
#    soc = sttp.targetpack.get_soc()
#    evalStr = "sttp_root." + soc.name + "." + core 
#    armInfo = eval(evalStr)
#    #armInfo = core_utils.find_cpu(sttp_root, core)

#    coresight_mem_ap = int(armInfo.parameters["coresight_mem_ap"], 0)
    coresight_mem_ap = int(cp["coresight_mem_ap"], 0)

    val = Read(cp, core, coresight_mem_ap, addr)
    return val    

def write_cs(core, addr, val):
    cp = sttp.stmc.get_core_params(core)    

#    sttp_root = sttp.targetpack.get_tree()
#    soc = sttp.targetpack.get_soc()
#    evalStr = "sttp_root." + soc.name + "." + core 
#    armInfo = eval(evalStr)
#    #armInfo = core_utils.find_cpu(sttp_root, core)

#    coresight_mem_ap = int(armInfo.parameters["coresight_mem_ap"], 0)
    coresight_mem_ap = int(cp["coresight_mem_ap"], 0)

    Write(cp, core, coresight_mem_ap, addr, val)
    return val    

# Report useful DP diagnostic information
def dump_arm_dp_info(cp):
    sttp.logging.print_out("dump_arm_dp_info:")
    ir_return = TapAccessIR_RW(cp, JTAG_INST_IDCODE, JTAG_INST_SIZE)
    dr_return = TapAccessDR_RW(cp, 0, 32)
    sttp.logging.print_out("    IDCODE:    0x%08x" % (dr_return,))
    result = ReadWriteDP(cp, JTAG_DP_CTRL_STAT, 0, JTAG_READ)
    sttp.logging.print_out("    CTRL_STAT: 0x%x" % (result,))

# Report useful MEM-AP diagnostic information
def dump_arm_mem_ap_info(cp, ap):
    sttp.logging.print_out("dump_arm_mem_ap_info:")
    result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_CSW, 0, JTAG_READ)
    # ensure memory access size to be of 32 bits
    if (result & 7) != 2:
        result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_CSW, result | 2, JTAG_WRITE)
        result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_CSW, 0, JTAG_READ)
    sttp.logging.print_out("    CSW:  0x%x" % (result,))
    result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_CFG, 0, JTAG_READ)
    sttp.logging.print_out("    CFG: 0x%x" % (result,))
    result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_BASE_0, 0, JTAG_READ)
    sttp.logging.print_out("    BASE: 0x%x" % (result,))
    if (result == 0x2):
       sttp.logging.print_out("    MEM-AP Supports Large Physical Addresses")
    else:
       sttp.logging.print_out("    MEM-AP Supports legacy Physical Addresses")
    result = ReadWriteMemAP(cp, ap, JTAG_MEM_AP_IDR, 0, JTAG_READ)
    sttp.logging.print_out("    IDR:  0x%x" % (result,))
    if (result == 0x34770004):
       sttp.logging.print_out("    IDR 0x34770004 : ARM AMBA AXI3/4 Mem-AP r3\n")

def ap_discovery(core):
    cp = sttp.stmc.get_core_params(core)    

    for ap in range(3):
        bank = 0x0;
        for regidx in [0,4]:
            val = ReadWriteMemAP(cp, ap, bank, regidx, 0, JTAG_READ);
            sttp.logging.print_out("    AP %d, Bank %d, REG %d, val 0x%x" % (ap, bank, regidx, val))

        bank = 0xf;
        for regidx in [0, 4, 8, 12]:
            val = ReadWriteMemAP(cp, ap, bank, regidx, 0, JTAG_READ);
            sttp.logging.print_out("    AP %d, Bank %d, REG %d, val 0x%x" % (ap, bank, regidx, val))


def dump_arm_debug_logic_regs(cp, ap, cs_base):
    DBGDIDR  = cs_base + 0x000
    DBGDSCR  = cs_base + 0x088
    DBGPRCR  = cs_base + 0x310
    DBGPRSR  = cs_base + 0x314
    
    r = ReadMemAPLocation(cp, ap, DBGDIDR)
    sttp.logging.print_out("Current DBGDIDR: 0x%x\n" % r)
    
    r = ReadMemAPLocation(cp, ap, DBGDSCR)
    sttp.logging.print_out("Current DBGDSCR: 0x%x\n" % r)
    
    r = ReadMemAPLocation(cp, ap, DBGPRCR)
    sttp.logging.print_out("Current DBGPRCR: 0x%x\n" % r)
    
    r = ReadMemAPLocation(cp, ap, DBGPRSR)
    sttp.logging.print_out("Current DBGPRSR: 0x%x\n" % r)
    

def pc_sampling(cp, ap, cs_base, nb_iterations):
    DBGPCSR  = cs_base + 0x084
    DBGPCSR2 = cs_base + 0x0A0
    for i in range(nb_iterations):
        r1 = ReadMemAPLocation(cp, ap, DBGPCSR)
        r2 = ReadMemAPLocation(cp, ap, DBGPCSR2)
        sttp.logging.print_out("Current PC (DBGPCSR): 0x%x, (DBGPCSR2) 0x%x\n" % (r1, r2))
        #time.sleep(0.1)

def dump_component_id_regs(cp, ap, cs_base):
    compID0 = cs_base + 0xFF0
    compID1 = cs_base + 0xFF4
    compID2 = cs_base + 0xFF8
    compID3 = cs_base + 0xFFC

    r = ReadMemAPLocation(cp, ap, compID0)
    if r != 0xD:
        sttp.logging.print_out("Invalid Component ID0: 0x%x" % r)
    else:
        sttp.logging.print_out("Component ID0: 0x%x" % r)
    
    r = ReadMemAPLocation(cp, ap, compID1)
    if (r & 0x0F) != 0x0:
        sttp.logging.print_out("Invalid Component ID1: 0x%x" % r)
    else:
        sttp.logging.print_out("Component ID1: 0x%x" % r)

    r = ReadMemAPLocation(cp, ap, compID2)
    if r != 0x5:
        sttp.logging.print_out("Invalid Component ID2: 0x%x" % r)
    else:
        sttp.logging.print_out("Component ID2: 0x%x" % r)

    r = ReadMemAPLocation(cp, ap, compID3)
    if r != 0xB1:
        sttp.logging.print_out("Invalid Component ID3: 0x%x" % r)
    else:
        sttp.logging.print_out("Component ID3: 0x%x" % r)

"""
Power up debug logic and release debug logic reset.
Set CSYSPWRUPREQ, CDBGPWRUPREQ and CDBGRSTREQ bits in CTRL/STAT register
"""
global dap_powered_on
dap_powered_on = False
def power_up_debug(cp):
    #tap.jtag.go_to_rti()

    # Clear sticky err, reset and release debug logic
    ReadWriteDP(cp, JTAG_DP_CTRL_STAT, 0x54000020, JTAG_WRITE)
    result = ReadWriteDP(cp, JTAG_DP_CTRL_STAT, 0, JTAG_READ)
    dap_powered_on = True


def parse_rom_table(cp, ap, base_addr):
    entry = ReadMemAPLocation(cp, ap, base_addr)
    i = 0
    while entry != 0:
        if (entry & 1) == 1: # present
            offset = entry & 0xFFFFF000
            addr = base_addr + offset
            if (offset >> 31 & 1) == 1:
                addr = base_addr - (~offset + 1)
            sttp.logging.print_out("base : 0x%x, entry : 0x%x => addr: 0x%x" % (base_addr, entry, addr))
            comp_id0 = ReadMemAPLocation(cp, ap, addr + 0xFF0)
            comp_id1 = ReadMemAPLocation(cp, ap, addr + 0xFF4)
            if comp_id0 != 0xD or (comp_id1 & 0xF) != 0:
                sttp.logging.print_out("Invalid component at this address")
            elif comp_id1 == 0x10:
                sttp.logging.print_out("Romtable")
                parse_rom_table(cp, ap, addr)
        i += 4
        entry = ReadMemAPLocation(cp, ap, base_addr + i)


"""
test coresight.py
"""
def test(core):
    global cp
    global coresight_base
    global coresight_mem_ap

    cp = sttp.stmc.get_core_params(core)    

    #coresight_base   = int(armInfo.get_parameters()["coresight_debug"], 0)
    coresight_base = int(cp['coresight_debug'], 0)
    #coresight_mem_ap = int(armInfo.parameters["coresight_mem_ap"], 0)
    coresight_mem_ap = int(cp["coresight_memap"], 0)
   
#    sttp_root = sttp.targetpack.get_tree()
#    soc = sttp.targetpack.get_soc()
#    evalStr = "sttp_root." + soc.name + "." + core
#    armInfo = eval(evalStr)

    power_up_debug(cp)
    dump_arm_dp_info(cp)

    # test axi-ap
    aarch64 = True
    test_axi_ap(cp, aarch64)

    # test apb-ap
    test_apb_ap(core)

    # rom table
    base = ReadWriteMemAP(cp, coresight_mem_ap, JTAG_MEM_AP_BASE_0, 0, JTAG_READ)
    sttp.logging.print_out("base addr = 0x%x" % base)
    parse_rom_table(cp, coresight_mem_ap, base)
    
def test_apb_ap(core):
    cp = sttp.stmc.get_core_params(core)
    apb_ap = int(cp["coresight_memap"], 0)
    dump_arm_mem_ap_info(cp, apb_ap)
    coresight_base = int(cp['coresight_debug'], 0)
    dump_component_id_regs(cp, apb_ap, coresight_base)
    if (core[0:3] != "a5x"):
        dump_arm_debug_logic_regs(cp, coresight_mem_ap, coresight_base)

    addr = coresight_base+0xFF0
    val = read_cs(core, addr)
    sttp.logging.print_out("Read addr 0x%x = 0x%x in %s debug logic" % (addr, val, core))

def test_axi_ap(core, aarch64):
    cp = sttp.stmc.get_core_params(core)

    if (dap_powered_on == False):
        power_up_debug(cp)

    # Find out platform name
    tpname = sttp.stmc.get_target_params()["platform"]

    # Find out SOC name
    l = len(tpname)
    if tpname[l-4:].isdigit():  soc_id_str = tpname[l-5:]     # case where the SOC name has 4 digits
    else :                      soc_id_str = tpname[l-4:]     # case where it has only 3 digits
    # Ensure it is in lower case
    soc_id_str = soc_id_str.lower()
    # Build up relevant path to XML file containing the AP number for the AXI-AP
    sttp_root = sttp.targetpack.get_tree()
    cs_path = eval("sttp_root.stx" + soc_id_str)
    axi_ap_num = cs_path.coresight_amba_axi.parameters["memap"]
    dump_arm_mem_ap_info(cp, int(axi_ap_num))
    ReadWriteSystemMem(cp, axi_ap_num, aarch64)

def ReadWriteSystemMem(cp, ap, aarch64):
    coresight_memap = int(ap)
    offset  = 0x00012480
    DDRADDR = 0x80000000 + offset

    # Perform a simple 32 bits R/W
    WriteMemAPLocation(cp, coresight_memap, DDRADDR, 0xC5ACCE55)
    r = ReadMemAPLocation(cp, coresight_memap, DDRADDR)
    if (r !=  0xC5ACCE55):
        sttp.logging.print_out("DDRADDR read back wrong value: 0x%08x" % r)
        return

    # Perform multiple 32 bits R/W with address auto incrementing
    csw_data = ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_CSW, 0, JTAG_READ)
    csw_data |= 0x10
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_CSW, csw_data, JTAG_WRITE)

    index = 0
    data_value = 0xA5
    while (index <7):
        WriteMemAPLocation(cp, coresight_memap, DDRADDR, data_value)
        data_value = (data_value <<4)
        index +=1
    # Need to re-initialize write pointer as it incremented automatically
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR, DDRADDR, JTAG_WRITE)
    lastTAR = DDRADDR

    index = 0
    data_value = 0xA5
    while (index <7):
        r = ReadMemAPLocation(cp, coresight_memap, DDRADDR)
        if (r != data_value):
            sttp.logging.print_out("DDRADDR read back wrong value: 0x%08x" % r)
            return
        data_value = (data_value <<4)
        index +=1

    if (aarch64 ==0):
            sttp.logging.print_out("TEST_COMPLETE\n")
            return

    # Store particular flag and test result for a later usage
    WriteMemAPLocation(cp, coresight_memap, DDRADDR, 0xC5ACCE55)
    DDRADDR = 0x80000000
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR, DDRADDR, JTAG_WRITE)
    WriteMemAPLocation(cp, coresight_memap, DDRADDR, index)

    # Perform an access in LPAE mode (0x80000000 aliases with 0x100000000)
    EXTADDR = 0x00000001
    DDRADDR = offset + (index*4)
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR, DDRADDR, JTAG_WRITE)
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR_1, EXTADDR, JTAG_WRITE)

    r = ReadMemAPLocation(cp, coresight_memap, DDRADDR)
    if (r == 0xC5ACCE55):
        lpae_error = 0
    else:
        sttp.logging.print_out("    --> LPAE DDRADDR read back wrong value: 0x%08x\n" % r)
        lpae_error = 1

    # Perform an access to a large data extension (e.g. 64bits) in legacy mode
    EXTADDR = 0x00000000
    DDRADDR = 0x80000000 + offset
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR, DDRADDR, JTAG_WRITE)
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_TAR_1, EXTADDR, JTAG_WRITE)

    csw_data = ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_CSW, 0, JTAG_READ)
    csw_data |= 0x03
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_CSW, csw_data, JTAG_WRITE)

    data32  = ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_BD0, 0, JTAG_READ)
    data64  = ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_BD1, 0, JTAG_READ)

    if ((data32 == 0xA5) and (data64 == 0xA50)):
       ld_error =0
    else:
       sttp.logging.print_out("    Read back wrong 64bit data: 0x%08x, 0x%08x\n" % (data32, data64))
       ld_error =1

    # Store LPAE and LPData results (back to 32 bit mode)
    DDRADDR = 0x80000004
    csw_data &= ~0x01
    ReadWriteMemAP(cp, coresight_memap, JTAG_MEM_AP_CSW, csw_data, JTAG_WRITE)
    if ((lpae_error ==1) or (ld_error ==1)):
       WriteMemAPLocation(cp, coresight_memap, DDRADDR, 0xDEADC0DE)
       sttp.logging.print_out("\nTEST_FAILED")
    else:
       WriteMemAPLocation(cp, coresight_memap, DDRADDR, 0xC5ACCE55)
       sttp.logging.print_out("\nTEST_COMPLETE")
