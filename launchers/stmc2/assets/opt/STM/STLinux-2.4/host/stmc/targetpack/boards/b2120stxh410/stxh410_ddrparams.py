import sttp
import sttp.targetpack
import stxh410

global sttp_root
sttp_root = sttp.targetpack.get_tree() 

def set_ddr_params(ddrparams):
    sttp_root = sttp.targetpack.get_tree()
    ddrparams["pctlresetreg"] = sttp_root.stxh410.core_sysconf_regs.sysconf_regs.RESETGEN_CONF0_1  
    ddrparams["pctlresetbit"] = 1
    ddrparams["pctlresetpol"] = 0
    ddrparams["rrmodereg"] = 0
    ddrparams["rrmodebit"] = 0
    ddrparams["rrmodevalue"] = 2
    ddrparams["pctl"] = sttp_root.stxh410.DDR3_PCTL.ddr3_pctl_regs
    ddrparams["phy"] = sttp_root.stxh410.DDR3_PHY.ddr3_phy_regs 
    ddrparams["BITRATE"] = stxh410.ddr_get_mbps()

    #define the width of the data bus of the IP integrated in the die
    ddrparams["lmi_IP_width"]=32

    ddrparams["IOTYPE"] = "D3F"

    #Predefined impedance overwriting values, controller side for D3F IOs

    ddrparams["OP_IMP_15_OHMS"] = 0x7F
    ddrparams["OP_IMP_20_OHMS"] = 0x61
    ddrparams["OP_IMP_25_OHMS"] = 0x4E
    ddrparams["OP_IMP_30_OHMS"] = 0x41
    ddrparams["OP_IMP_35_OHMS"] = 0x37
    ddrparams["OP_IMP_40_OHMS"] = 0x31
    ddrparams["OP_IMP_45_OHMS"] = 0x2B
    ddrparams["OP_IMP_50_OHMS"] = 0x27
    ddrparams["OP_IMP_55_OHMS"] = 0x23
    ddrparams["OP_IMP_65_OHMS"] = 0x1E
    ddrparams["OP_IMP_75_OHMS"] = 0x1A

    ddrparams["ODT_IMP_15_OHMS"] = 0x75
    ddrparams["ODT_IMP_20_OHMS"] = 0x58
    ddrparams["ODT_IMP_25_OHMS"] = 0x46
    ddrparams["ODT_IMP_30_OHMS"] = 0x3B
    ddrparams["ODT_IMP_35_OHMS"] = 0x32
    ddrparams["ODT_IMP_40_OHMS"] = 0x2C
    ddrparams["ODT_IMP_50_OHMS"] = 0x23
    ddrparams["ODT_IMP_60_OHMS"] = 0x1D
    ddrparams["ODT_IMP_90_OHMS"] = 0x14
    ddrparams["ODT_IMP_110_OHMS"] = 0x0F
    ddrparams["ODT_IMP_170_OHMS"] = 0x0A
    ddrparams["ODT_IMP_340_OHMS"] = 0x06

    #Predefined impedance divider values (For RZQ = 240 ohms), controller side for D3F IOS
    ddrparams["ODT_120_OHMS"] = 3
    ddrparams["ODT_60_OHMS"] = 7
    ddrparams["ODT_40_OHMS"] = 11
    ddrparams["ZO_40_OHMS"] = 11
    ddrparams["ZO_34_OHMS"] = 13

    #SELECT impedance values (For impedance overriding)
    ddrparams["ODT_PULL_UP"]   = ddrparams["ODT_IMP_60_OHMS"]
    ddrparams["ODT_PULL_DOWN"] = ddrparams["ODT_IMP_60_OHMS"]
    ddrparams["DRV_PULL_UP"]   = ddrparams["OP_IMP_35_OHMS"]
    ddrparams["DRV_PULL_DOWN"] = ddrparams["OP_IMP_35_OHMS"]
    
    # Set SOC specific function to work-around DQS inv work around
    ddrparams["phydxccr_callback"] = phydxccr_callback
    
    return ddrparams

def phydxccr_callback(ddrparams):
   
    # DQS config depends on cut ID
    corecfg = sttp_root.stxh410.core_sysconf_regs.sysconf_regs
    if corecfg.SYSTEM_STATUS5568.if_eq(0xf << 28, 0): # Work around only for cut1
        dqsres=(ddrparams["PHY_DXCCR"]>>5)&0xF
        dqsnres=(ddrparams["PHY_DXCCR"]>>9)&0xF
        val = (ddrparams["PHY_DXCCR"] & 0xFFFFE01F) | (dqsres << 9) | (dqsnres << 5)
        ddrparams["phy"].DDR3PHY_DXCCR.poke(val)
    else:
        ddrparams["phy"].DDR3PHY_DXCCR.poke(ddrparams["PHY_DXCCR"])

    return
