#!/bin/sh

# Upgrade script for Release 0.4.1.

# This upgrade flashes new u-boot env.
# We must remember state of u-boot env variable 'current'.
# Variable 'current' will be initialised by u-boot env upgrade.
# A copy of 'current's state has been saved to, /tmp/upgrade/remember_curent.sh
# This script restores 'current's value.

echo Restoring U-Boot Env variable current...
/tmp/upgrade/remember_curent.sh
