"""
This is a simple TAPLink Poke/Peek SessionApp
It takes commands on stdin to poke, peek or quit.

It is started on one of two ways. These take arguments
to either test or to operate.

Form 1

  ... -test {driver}

Form 2
 

  ... -driver {driver}

In addition the following test mode is supported for testing
the script standalone 

Form 3
 
  ... -drivertest {driver}

The poke/peek DSU functionality has been taken from
  
  ADCS 7511658C - ST230 Core and instruction set architecture manual 
  /vob/mcdtgnb-src-st200emu/hti/dsu.cxx 


"""
import os
import re
import struct
import sys
import time

logfile = None
driver  = None
driverTest = False

# DSU Register locations

DSX_VERSION = 0
DSX_STATUS  = 1
DSX_ARG2    = 10
DSX_ARG1    = 11

# DSU Commands

DSU_PEEKED = 1
DSU_FLUSH  = 2
DSU_POKE   = 3
DSU_PEEK   = 4

# TAP Commands

TAP_POKE   = 0
TAP_PEEK   = 1
TAP_PEEKED = 2
TAP_EVENT  = 3
TAP_OPCODE_MASK = 3

TAP_EVENT_REASON_LO = 2
TAP_EVENT_DEFAULT = TAP_EVENT + (1 << TAP_EVENT_REASON_LO)


def doPoke(address, words):
    """
    DSU register write or a set of words
    TAPlink poke of one word
    """
    size = len(words)

    if size<1 or size>63:
        sys.stderr.write("poke packet too large - address 0x%08x, length %d\n" % (address, size))
        sys.exit(1)

    # Build the taplink poke command
    tapCmd = (size<<2) | TAP_POKE;
    packFormat = "<BL" + "L" * len(words)

    # build the call to eval with each element of words as an argument

    callStr = "struct.pack('%s', tapCmd, address" % packFormat
    for i in range(0,size):
        callStr = callStr + ", words[%d]" % i
    callStr += ")"

    bytes = eval(callStr)

    if logfile:
        logfile.write("_dPoke(): writing %d bytes %s\n" % (len(bytes), bytes) )
        logfile.flush()

    result = os.write(driver, bytes);
    if result != len(bytes):
        sys.stderr.write("Poke - could not write bytes\n")
        sys.exit(1)
    

def doPeek(address):
    """
    DSU registers read one word
    TAPlink peek one word
    """
    # Build the taplink peek command
    cmd = (1<<2) | TAP_PEEK;
    bytes = struct.pack("<BL", cmd, address)

    result = os.write(driver, bytes);
    if result != len(bytes):
        sys.stderr.write("_dPeek - could not write bytes\n")
        sys.exit(1)

    data = os.read(driver, 5);
    [type, value] = struct.unpack("<BL", data)

    global logfile
    if logfile:
        logfile.write( "doPeek: len(data) %d type %s, value %s\n" % (len(data), type, value) )
        logfile.flush()
    
    return value

def Poke(addr, value):
    """
    This function performs a target poke using the DSU
    """
    global driver

    # TODO - error handling should not terminate
    if addr & 3:
        sys.stderr.write("Unaligned poke address 0x%08x\n" % addr)
        sys.exit(1)

    words = (value,)
    doPoke(addr, words)

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

    global logfile
    if logfile:
        logfile.write( "Poke: addr: " + hex(addr) + " value: " + hex(value) + "\n")
        logfile.flush()
    return True

def Peek(address):
    """
    This function performs a target peek using the DSU
    """
    global driver

    # TODO - error handling should not terminate
    if address & 3:
        sys.stderr.write("Unaligned peek address 0x%08x\n" % address)
        sys.exit(1)

    value = doPeek(address)

    # write result to stdout - must flush
    print value
    sys.stdout.flush();

    global logfile
    if logfile:
        logfile.write( "Peek: addr: " + hex(address) + " value: " + hex(value) + "\n")
        logfile.flush()
    return True

def main():
    global driver
    global logfile
    global driverTest

    # Comment in the line below to log
    logfile = file("/tmp/stmc2_pokepeek_taplink.log", "w")

    sys.stdout.write("Started " + sys.argv[0] + " - args: " + str(sys.argv) + "\n")
    sys.stdout.flush();

    logfile.write("Started " + sys.argv[0] + "\n")
    logfile.write("args: " +  str(sys.argv) + "\n")
    logfile.flush();

    if len(sys.argv)==3 and sys.argv[1] == "-test":
        driver = os.open(sys.argv[2], os.O_RDWR)
        os.close(driver)
        sys.exit(0)

    if len(sys.argv)==3 and sys.argv[1] == "-driver":
        driver = os.open(sys.argv[2], os.O_RDWR)

    # test mode - write to file
    if len(sys.argv)==3 and sys.argv[1] == "-drivertest":
        driver = os.open(sys.argv[2], os.O_RDWR | os.O_CREAT | os.O_TRUNC)
        driverTest = True

    if logfile:
        logfile.write("Done stdout write Started ... " + sys.argv[0] + ", driver " + str(driver) + "\n")
        logfile.flush()

#    while 1:
#        # Hack here
#        _dForceHalt()
#    #    _dReset()

    if logfile:
        logfile.write("Done DSU reset -waiting for commands")
        logfile.flush()

    while 1:
        done = False
        line = sys.stdin.readline()

        if logfile:
            logfile.write("stdin command: %s\n" % (line,)) 
            logfile.flush()

        # quit command
        if re.match(r'^quit', line):
            os.close(driver)
	    sys.exit(0)

        # poke command
        m = re.match(r'^poke ([0-9]+|0x[0-9A-Fa-f]+)L*\s+([0-9]+|0x[0-9A-Fa-f]+)L*$', line)
        if m:
            done = Poke(int(m.group(1), 0), int(m.group(2), 0))

        # peek command
        m = re.match(r'^peek ([0-9]+|0x[0-9A-Fa-f]+)L*$', line)
        if m:
            done = Peek(int(m.group(1), 0))

        if False == done:
            sys.stderr.write("Failed to handle command %s\n" % line)
            sys.exit(1)

if __name__ == "__main__":
    main()
