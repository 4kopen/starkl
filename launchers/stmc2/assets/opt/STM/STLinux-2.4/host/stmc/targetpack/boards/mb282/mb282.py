# Script for mb424 with sti5301

import sttp
import sttp.stmc
import sttp.targetinfo

# TargetPack imports
import tap
import tap.jtag
import utilities

class ScriptException(Exception):
    pass

def poke(x, y): 
#    print "Poking(addr, data)", hex(x), hex(y)
    sttp.pokepeek.poke(x, y)

def peek(x):
#    print "Peeking(addr)", hex(x)
    return sttp.pokepeek.peek(x)


def st20_connect(parameters):

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    frequency = 20000000;

    real_tck = sttp.stmc.set_tck_frequency(frequency)

    print "st20_connect: tck frequency requested %d Hz, set to %d Hz" % (frequency, real_tck)

    sttp.stmc.core_map({"tmc":"0"})

    # Manual control of all JTAG lines
    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    # Do a reset
    sttp.jtag.sequence({"nrst" : "1", "ntrst" : "1", "tms" : "1", "tck" : "1", "td_to_target" : "1", "trig_to_target" : "1"})
    # Now enable outputs
    sttp.stmc.output_enable(True)
    sttp.jtag.sequence({'ntrst' : "0"})
    sttp.jtag.sequence({'nrst' : "0"})
    # What should the min delay be? Currently 100mS
    time.sleep(0.1)
    sttp.jtag.sequence({"nrst" : "1", "ntrst": "1", "trig_to_target" : "0", "tms": "0", "td_to_target": "0", "tck": "0"})

    # Autoclock of tck
    sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)

    # Verify device id
    id = tap.jtag.read_device_id()
    print "Device id ", hex(id)

    # Setup TAPlink mode
    tap.jtag.st20_enter_taplink_mode()

    # Autoclock to the target
    sttp.jtag.set_mode(sttp.jtag.CONTINUOUS_CLOCK)

    # Setup the STMC multiplexor side so that the st20 core is plumbed to debug channel 0
    bypass_core_name = sttp.targetinfo.get_targetstring_core()
    sttp.stmc.core_map({bypass_core_name:0})

    print "ST20 now in TAPlink mode"

    sttp.pokepeek.enable()
    
    # sttp.pokepeek.poke(0x00003004, 0x12345678)

    x = sttp.pokepeek.peek(0x00003000)
    print "peek 0x00003000 =:", hex(x)

    x = sttp.pokepeek.peek(0x00003004)
    print "peek 0x00003004 =:", hex(x)

def st20_disconnect():
    pass

