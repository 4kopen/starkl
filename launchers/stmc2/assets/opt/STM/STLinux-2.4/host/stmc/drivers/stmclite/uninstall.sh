#!/bin/sh

PATH=/usr/bin:/bin:/sbin
export PATH

stmcliterules=99-stmclite.rules

if [ `id -u` -ne 0 ]
then
	echo "root privileges required" 1>&2
	exit 1
fi

binroot=`dirname $0` && binroot=`cd $binroot && pwd`

dstrules=/etc/udev/rules.d/$stmcliterules

if [ -f $dstrules ]
then
	if version=`udevinfo -V 2> /dev/null`
	then
		udevreload="udevcontrol reload_rules"
	elif version=`udevadm --version 2> /dev/null`
	then
		udevreload="udevadm control --reload-rules"
	else
		echo "udev utilities required" 1>&2
		exit 1
	fi

	version=`expr "$version" : "[^0-9]*0*\([0-9]*\)"`
else
	echo "$dstrules not installed" 1>&2
	exit 1
fi


rm -f $dstrules

if [ -f $dstrules ]
then
	echo "$dstrules not removed" 1>&2
else
	if $udevreload 2> /dev/null
	then
		echo "$dstrules uninstalled"
		exit 0
	else
		echo "udev rules not reloaded" 1>&2
	fi
fi

exit 1
