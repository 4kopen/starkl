###
### Flexiclockgen lib for targetpacks
### (C) 2014, F. Charpentier, UPD - Functional Validation
###

# Revision history (most recent on top)
# 09/sep/14  FCh  PLL3200: SSCG disabled before setup in case already ON.
# 09/jun/14  FCh  FS660 & PLL3200 functions now require/return freq in Hz.
# 06/jun/14  FCh  set_div() fix. Note that use of 'prediv' is still not clean.
# 25/apr/14  FCh  Added SSCG support for PLL3200
# 24/apr/14  FCh  Added display_freq() support for final div outputs.
# 23/apr/14  FCh  'init' + 'get_pll3200' + 'get_freq' functions added.
# 15/apr/14  FCh  Initial version applied on H410 & Orly3

# Flexiclockgen declarations example (to be done in 'stx<soc>.py' file)
# clocks_general = {
#    "debug"   : 0,            # 1=debug ON
#    "osc"     : 30,
#    "on_emu"  : 0,            # 1=on emulator
# }
# clkgenA0 = {
    # "name"   : "A0",         # clockgen name
    # "base"   : 0x90ff000,    # base addr
    # "gfgs"   :
        # [
            # {"type"      : "PLL3200",
             # "spreadtype": "center",
             # "fmod"      : 30,
             # "md"        : 2
            # }
        # ],
    # "outputs": ["CLK_IC_LMI0"],
    # "nominal": [ 466 ]
# }
# clkgenB0 = {
#    "name"    : "B0",         # clockgen name
#    "base"    : 0x08010000,   # base addr
#    "outputs" : ["", "CLK_ICN_NET", "CLK_ICN_TS", "CLK_ICN_VIDIN",
#                "CLK_ICN_REG_4", "CLK_PROC_FVDP_MAIN", "CLK_PROC_FVDP_ENC_0"]
#    "nominal" : [0, 355, 400, 266, 300, 225, 400, 450, 450, 533]
# }

import sttp
import clk_common2

clocks_debug = 0    # Clocks debug. 0=disable
osc_freq = 0        # Board ref input clock (usually 30Mhz)
on_emu = 0          # Set to 1 if running on emulator (no PLL)
ONEMEG = 1000000    # Constant for Mhz

def debug_print(debug, s):

    if debug:
        sttp.logging.print_out(s)

# Spreadtype check
# Allowed: "center" or "down"
# Returns: 0 for center, 1 for down spread
def check_spreadtype(spreadtype):

    if (spreadtype == "center"):
        spread_val = 0
    elif (spreadtype == "down"):
        spread_val = 1
    else:
        raise sttp.STTPException("ERROR: Invalid spreadtype ('center' or 'down' allowed)")

    return spread_val

# Flexiclockgen lib init function
def init(general):

    global clocks_debug, osc_freq, on_emu

    clocks_debug = general["debug"]
    osc_freq = general["osc"]
    on_emu = general["on_emu"]

# Setting PLL3200 GFG
# 'fgen'  = flexiclockgen declaration
# 'gfgid' = GFG number
# 'freq'  = FVCOBy2/ODF (PHIx output) frequency in Hz
def set_pll3200(fgen, gfgid, freq):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  set_pll3200(%s, %d, %uHz)" % (name, gfgid, freq))

    # Computing parameters: min PLL3200 freq is 800Mhz
    if (freq < 800000000):
        odf = 800000000 / freq
        if (800000000 % freq):
            odf = odf + 1
    else:
        odf = 1
    vcoby2 = freq * odf
    err, idf, ndiv, cp = clk_common2.pll3200c32_get_params(osc_freq * 1000000, vcoby2)
    if (clocks_debug):
        sttp.logging.print_out("    in=%uMhz requested=%uHz => vcoby2=%uHz, odf=%u" % (osc_freq, freq, vcoby2, odf))
        sttp.logging.print_out("    PLL3200 params: err=%d, idf=%u ndiv=%u cp=%u" % (err, idf, ndiv, cp))
    if (err != 0):
        raise sttp.STTPException("Invalid frequency requested for PLL3200")

    # Registers offsets
    reg0 = base + 0x2a0 + (gfgid * 0x28)    # gfgX_config0
    reg1 = reg0 + 0x4                       # gfgX_config1
    reg2 = reg0 + (2 * 0x4)
    reg3 = reg0 + (3 * 0x4)
    reg4 = reg0 + (4 * 0x4)
    reg5 = reg0 + (5 * 0x4)

    # Updating HW
    sttp.pokepeek.or_const(reg0, reg0, (1 << 8))                # Power down PLL: config0[8]
    sttp.pokepeek.read_modify_write(reg0, reg0, ~(0x1f << 16), 0, cp << 16)    # cp: config0[20:16]
    sttp.pokepeek.read_modify_write(reg1, reg1, 0xff00fff8, 0, ((ndiv << 16) | idf)) # ndiv: config1[23:16], idf: config1[2:0]
    sttp.pokepeek.read_modify_write(reg5, reg5, ~0x7f, 0, odf)  # odf: config5[6:0]
    sttp.pokepeek.and_const(reg0, reg0, ~(1 << 8))              # Power up PLL: config0[8]
    if (on_emu == 0): # No lock on emulator
        debug_print(clocks_debug,"    Waiting for PLL lock")
        sttp.pokepeek.while_and_ne(reg0, 1 << 24, 1 << 24)      # Wait for lock: config0[24]
        debug_print(clocks_debug,"    => ok, PLL locked")

    if (not fgen.has_key("gfgs")):
        return
    gfg = fgen['gfgs'][gfgid]
    # If spreadtype defined, it means SSCG config must be done
    if (not gfg.has_key("spreadtype")):
        return

    # SSCG configuration
    spreadtype = gfg['spreadtype']  # "center"=0, "down"=1
    fmod = gfg['fmod']              # Modulation frequency (kHz)
    md = gfg['md']                  # Modulation depth: (pk-pk)/2

    # Check spreadtype
    spread = check_spreadtype(spreadtype)

    # Check that fmod is in PLL range
    if ((ndiv < 49) and (fmod > (100*48/ndiv)/3)):
        raise sttp.STTPException("ERROR: fmod ouside PLL specification (PLL bandwidth/3)")
    if ((ndiv > 48) and (fmod > 100/3)):
        raise sttp.STTPException("ERROR: fmod ouside PLL specification (PLL bandwidth/3)")
    debug_print(clocks_debug, "    fmod=%d kHz, modulation depth (pk-pk)=%d %%, spreadtype=%s" % (fmod, md * 2, spreadtype))

    fref = (osc_freq * 1000) / idf
    modperiod = int(round(float(fref / (4 * fmod))))
    incstep = int(round(float(((1 << 15) - 1) * md * ndiv) / float(100 * 5 * modperiod)))

    sttp.pokepeek.read_modify_write(reg4, reg4, ~(0xffffffff), 0, (incstep << 16) | modperiod)
    sttp.pokepeek.read_modify_write(reg2, reg2, ~((1<<22) | (1<<20)), 0, (spread << 20)) # SSCG is not enabled in this function
    sttp.pokepeek.read_modify_write(reg3, reg3, ~(0x3), 0, 1) #Rising edge on STRB to load modperiod, incstep and spreadtype paramters in SSCG configuration
    sttp.pokepeek.read_modify_write(reg3, reg3, ~(0x3), 0, 0) #Set STRB back to 0.
    debug_print(clocks_debug, "    => ok: incstep=%u, modperiod=%u" % (incstep, modperiod))

# Enable/disable PLL3200 SSCG
def enable_sscg(fgen, gfgid, enable):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  enable_sscg(%s, gfg%d, enable=%d)" % (name, gfgid, enable))

    reg2 = base + 0x2a0 + (gfgid * 0x28) + (2 * 0x4)
    sttp.pokepeek.or_const(reg2, reg2, enable << 22)

# Compute PLL3200 GFG output frequency
# Returns: PHI0 freq, status
def get_pll3200(fgen, gfgid):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  get_pll3200(%s, gfg%d)" % (name, gfgid))

    reg0 = base + 0x2a0 + (gfgid * 0x28)
    reg1 = reg0 + 4
    reg2 = reg0 + (2 * 4)
    reg5 = reg0 + (5 * 4)

    cfg0 = sttp.pokepeek.peek(reg0)
    if (cfg0 & (1 << 8)):
        debug_print(clocks_debug, "    => GFG%d in POWER DOWN state" % gfgid)
        return 0, "" # PD => power down

    cfg1 = sttp.pokepeek.peek(reg1)
    idf = cfg1 & 0x7                                # idf: config1[2:0]
    ndiv = (cfg1 >> 16) & 0xff                      # ndiv: config1[23:16]
    odf0 = sttp.pokepeek.peek(reg5) & 0x7f          # odf0: config5[6:0]
    sscg_on = (sttp.pokepeek.peek(reg2) >> 22) & 1  # sscg_control: config2[22]

    if (odf0 == 0):
        odf0 = 1

    debug_print(clocks_debug, "    ndiv=0x%x, idf=0x%x, odf=%u" % (ndiv, idf, odf0))

    if (idf == 0):
        phi0 = 0 # Would probably mean not programmed
    else:
        phi0 = (2 * osc_freq * ONEMEG * ndiv) / idf / odf0

    if (sscg_on):
        sscg_st = "(SSCG ENABLED)"
    else:
        sscg_st = ""

    return phi0, sscg_st

# Setting FS660 GFG VCO
# 'fgen'  = flexiclockgen declaration
# 'gfgid' = GFG number
# 'freq'  = VCO frequency in Hz
def set_fs660_vco(fgen, gfgid, freq):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  set_fs660_vco(%s, %d, %u Hz)" % (name, gfgid, freq))

    # Computing parameters
    err, ndiv = clk_common2.fs660c32_vco_get_params(osc_freq * 1000000, freq)
    if (clocks_debug):
        sttp.logging.print_out("    in=%uMhz requested=%uHz" % (osc_freq, freq))
        sttp.logging.print_out("    FS660-vco params: err=%d, ndiv=%u" % (err, ndiv))
    if (err != 0):
        raise sttp.STTPException("Invalid frequency requested for FS660-vco")

    # Updating HW
    reg0 = base + 0x2a0 + (gfgid * 0x28)    # gfgX_config0
    reg1 = reg0 + 0x4                       # gfgX_config1

    sttp.pokepeek.or_const(reg0, reg0, (1 << 12))               # NPDPLL
    sttp.pokepeek.read_modify_write(reg1, reg1, ~(0x7 << 16), 0, ndiv << 16) # ndiv
    sttp.pokepeek.and_const(reg0, reg0, ~(1 << 12))             # NPDPLL
    if (on_emu == 0): # No lock on emulator
        debug_print(clocks_debug, "    Waiting for VCO to lock")
        sttp.pokepeek.while_and_ne(reg0, 1 << 24, 1 << 24)      # Wait for lock: gfg2_config0[24]
        debug_print(clocks_debug,"    => ok, VCO locked")

# Setting FS660 GFG CHANNEL
# 'fgen'  = flexiclockgen declaration
# 'gfgid' = GFG number
# 'vco'   = VCO frequency in Hz
# 'freq'  = Channel frequency in Hz
def set_fs660(fgen, gfgid, vco, chan, freq):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  set_fs660(%s, gfg%d, chan%d, freq=%d)" % (name, gfgid, chan, freq))

    # Computing parameters
    err, mdiv, pe, sdiv, nsdiv = clk_common2.fs660c32_dig_get_params(vco, freq, 0xff)
    if (clocks_debug):
        sttp.logging.print_out("    in=%uMhz requested=%uHz" % (vco, freq))
        sttp.logging.print_out("    FS660-fs params: err=%d, mdiv=%u, pe=%u, sdiv=%u, nsdiv=%u" % (err, mdiv, pe, sdiv, nsdiv))
    if (err != 0):
        raise sttp.STTPException("Invalid frequency requested for FS660")

    # Updating HW
    reg0 = base + 0x2a0 + (gfgid * 0x28)    # gfgX_config0
    reg3 = reg0 + (3 * 0x4)                 # gfgX_config3
    regX = reg0 + ((5 + chan) * 0x4)        # gfgX_config5/6/7/8

    sttp.pokepeek.or_const(reg0, reg0, ((0x100 << chan) | (1 << chan)))# digital reset reset[3:0] and digital standby standby[11:8]
    sttp.pokepeek.and_const(reg3, reg3,~(1 << chan))           # disable channel EN_PROGx
    mask = ~(0x1 << 24 | 0xf << 20 | 0x1f << 15 | 0x7fff)      # setting up mask
    val = nsdiv << 24 | sdiv << 20 | mdiv << 15 | pe           # setting up FS parameters
    sttp.pokepeek.read_modify_write(regX, regX, mask, 0, val)  # FS programming
    sttp.pokepeek.and_const(reg0, reg0,~(1 << chan))           # release digital reset reset[3:0]
    sttp.pokepeek.and_const(reg0, reg0,~(0x100 << chan))       # release digital standby standby[4:0]
    sttp.pokepeek.or_const(reg3, reg3, (1 << chan))            # enable channel EN_PROGx
    debug_print(clocks_debug,"    => ok, FS channel programmed")

# FS660 VCO freq computation function
# 'fgen'  = flexiclockgen declaration
# 'gfgid' = GFG number
def get_fs660_vco(fgen, gfgid):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  get_f660_vco(%s, gfg%d)" % (name, gfgid))

    # Updating HW
    reg0 = base + 0x2a0 + (gfgid * 0x28)
    reg1 = reg0 + 0x4

    ndiv = (sttp.pokepeek.peek(reg1) >> 16) & 0x7        # ndiv: config1[18:16]
    ndiv = ndiv + 16
    rate = osc_freq * ndiv * 1000000

    return rate

# FS660 FS channel freq computation function
# 'fgen'  = flexiclockgen declaration
# 'gfgid' = GFG number
# 'vco_freq' = VCO frequency in Hz
def get_fs660(fgen, gfgid, vco_freq, fs):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  get_f660(%s, gfg%d, %uHz)" % (name, gfgid, vco_freq))

    # Reading HW
    reg0 = base + 0x2a0 + (gfgid * 0x28)
    reg1 = reg0 + 0x4
    reg3 = reg0 + (3 * 0x4)
    regX = reg0 + ((5 + fs) * 0x4)

    data = sttp.pokepeek.peek(regX)                     # FS programming
    mdiv = (data >> 15) & 0x1f                          # MD[19:15]
    pe = (data & 0x7fff)                                # PE[14:0]
    sdiv = (data >> 20) & 0xf                           # SDIV[23:20]
    nsdiv = (data >> 24) & 0x1                          # NSDIV[24]

    # Converting reg field to formula
    s = (1 << sdiv)
    if (nsdiv == 1):
        ns = 1
    else:
        ns = 3

    # Computing freq
    debug_print(clocks_debug, "    mdiv=0x%x, pe=0x%x, sdiv=0x%x, nsdiv=0x%x, s=%u, ns=%u" % (mdiv, pe, sdiv, nsdiv, s, ns))
    P20 = (1 << 20)
    res = (P20 * (32 + mdiv) + 32 * pe) * s * ns
    rate = (vco_freq * P20 * 32) / res

    return rate

# Flexiclockgen xbar & dividers setup function.
# 'fgen' = flexiclockgen declaration
# 'chan' = xbar output index
# 'src'  = xbar input index
# 'div'  = div value. 0=STOP clock (disable)
def set_div(fgen, chan, src, div):

    name = fgen["name"]
    base = fgen["base"]

    debug_print(clocks_debug, "  set_div(%s, chan=%d, src=%d, div=%d)" % (name, chan, src, div))

    reg0 = base + 0x018 + (int(chan / 4) * 4) # fcg Xbar config register offset
    reg1 = base + 0x058 + (chan * 4)          # pre-divider config (fcg_config(22+chan))
    reg2 = base + 0x164 + (chan * 4)          # fcg final divider config register offset
    shift = (chan % 4) * 8

    # Set the pre div, then final div & enable
    prediv = 1
    findiv = 1
    en = 1
    if (div == 0): # Clock OFF
        en = 0
    elif (div <= 64):
        findiv = div
    else:
        prediv = div
    debug_print(clocks_debug, "    en=%d, prediv=0x%x, findiv=0x%x" % (en, prediv, findiv))
    sttp.pokepeek.read_modify_write(reg1, reg1, ~0x3ff, 0, (prediv - 1))
    sttp.pokepeek.read_modify_write(reg2, reg2, ~0xff, 0, (1 << 7) | (en << 6) | (findiv - 1)) # ASYNC+EN+DIV

    # Program Xbar source
    sttp.pokepeek.read_modify_write(reg0, reg0, ~(0x3f << shift), 0, (((1 << 6) | src) << shift))

# Compute clock freq at final div output.
# 'fgen' =
# 'index' =
# 'xbar_src_freqs' = Cross bar sources frequencies in Hz
# Returns: frequency and clock status ("OFF" or "10 Mhz") + "**" if diff vs nominal
def get_freq(fgen, index, xbar_src_freqs):

    name = fgen["name"]
    base = fgen["base"]
    if (fgen.has_key("nominal")):
        nominal = fgen["nominal"][index]
    else:
        nominal = 0

    reg0 = base + 0x018 + (int(index / 4) * 4) # fcg_config6+: Xbar src select
    reg1 = base + 0x058 + index * 4            # fcg pre-divider config register offset
    reg2 = base + 0x164 + (index * 4)          # fcg final divider config register offset
    shift = (index % 4) * 8

    # Identifying xbar source & status
    xbar_stat = (sttp.pokepeek.peek(reg0) >> shift) & 0xff
    if (on_emu == 0):
        if ((xbar_stat & (1 << 6)) == 0):
            debug_print(clocks_debug,"    => Clock @ index %d is STOPPED at Xbar" % index)
            return 0, "OFF"

    src_sel = xbar_stat & 0x3f
    if (src_sel >= len(xbar_src_freqs)):
        return 0, "UNKNOWN XBAR SOURCE"
    src_frq = xbar_src_freqs[src_sel]

    # Prediv status
    pre_div = (sttp.pokepeek.peek(reg1) & 0x3ff) + 1

    # Final div status
    fin_div = sttp.pokepeek.peek(reg2)
    if (on_emu == 0):
        if ((fin_div & (1 << 6)) == 0):
            debug_print(clocks_debug,"    => Clock @ index %d is STOPPED at final div" % index)
            return 0, "OFF"
    fin_div = (fin_div & 0x3f) + 1

    # Now according to divider, let's get ratio
    freq = src_frq / (pre_div * fin_div)

    # Checking if difference vs expected nominal
    if (nominal and freq != nominal):
        diff = "**"
    else:
        diff = ""

    status = "%u Hz %s" % (freq, diff)
    return freq, status

# Flexiclockgen final divider outputs display
def display_freq(fgen, xbar_src_freqs, max):

    if (not fgen.has_key("outputs")):
        sttp.logging.print_out("INTERNAL ERROR: CAN'T DISPLAY clockgen %s freq. Missing 'outputs'" % fgen["name"])
        return

    outputs = fgen["outputs"]

    for out in outputs:
        # If spare or hidden clock... skip
        if (out == ""):
            continue
        # Get freq
        i = outputs.index(out)
        freq, status = get_freq(fgen, i, xbar_src_freqs)
        # Compute padding to align clocks vertically
        padding = ""
        p = max - len(out) + 1
        while (p > 0):
            padding = padding + " "
            p = p - 1
        sttp.logging.print_out("    %s%s= %s" % (out, padding, status))
