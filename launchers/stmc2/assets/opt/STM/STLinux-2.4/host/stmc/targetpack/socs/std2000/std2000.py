# Functions specific to the std2000

# Import ST supplied libraries

import sttp
import sttp.pokepeek
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import st40_nomux

def connect(parameters):
    """
    Performs all std2000 SoC connect actions
    Must be used prior to performing board-level intiialization
    """
    try:
        sttp.targetinfo.setup_cores()
    except AttributeError, e:
        sttp.targetinfo.setup_init_core()

    st40_nomux.connect(parameters)

def std2000_set_clockgen(ckgc, gen_conf, mdiv, ndiv, pdiv):

    ckgc.CTL_SEL.poke(0x00000005)
    
    ckgc.CTL_EN.EN_CLK_ADC.write(ckgc.CTL_EN.EN_CLK_ADC.CLK_ADC_ENABLED)
    ckgc.CTL_EN.EN_CLK_RTC.write(ckgc.CTL_EN.EN_CLK_RTC.CLK_RTC_ENABLED)
    ckgc.CTL_EN.EN_CLK_REF0.write(ckgc.CTL_EN.EN_CLK_REF0.CLK_REF0_ENABLED)
    ckgc.CTL_EN.EN_CLK_SAFETY.write(ckgc.CTL_EN.EN_CLK_SAFETY.CLK_XT_SAFETY_ENABLED)
    
    ckgc.CTL_PLL2.MDIV.write(mdiv)
    ckgc.CTL_PLL2.NDIV.write(ndiv)
    ckgc.CTL_PLL2.PDIV.write(pdiv)
    
    ckgc.CTL_PLL2.ENABLE.write(ckgc.CTL_PLL2.ENABLE.PLL2_IS_ENABLED)
    # In CMD file bit 29 is set too, but we don't know what that does
    
    # Wait for PLL2 to lock
    ckgc.STATUS_PLL.while_and_ne(ckgc.STATUS_PLL.PLL2_LOCK.PLL2_IS_LOCKED.value, ckgc.STATUS_PLL.PLL2_LOCK.PLL2_IS_LOCKED.value)

    ckgc.CTL_EN.EN_CLK_LMI_2X.write(ckgc.CTL_EN.EN_CLK_LMI_2X.CLK_LMI_2X_ENABLED)
    
    # Wait for DLLs to lock for LMI1 and LMI2
    gen_conf.LMI1_STATUS.DLL1_LOCK.while_and_ne(gen_conf.LMI1_STATUS.DLL1_LOCK.DLL1_LOCKED.value, gen_conf.LMI1_STATUS.DLL1_LOCK.DLL1_LOCKED.value)

    gen_conf.LMI1_STATUS.DLL2_LOCK.while_and_ne(gen_conf.LMI1_STATUS.DLL2_LOCK.DLL2_LOCKED.value, gen_conf.LMI1_STATUS.DLL2_LOCK.DLL2_LOCKED.value)

    gen_conf.LMI2_STATUS.DLL1_LOCK.while_and_ne(gen_conf.LMI2_STATUS.DLL1_LOCK.DLL1_LOCKED.value, gen_conf.LMI2_STATUS.DLL1_LOCK.DLL1_LOCKED.value)

    gen_conf.LMI2_STATUS.DLL2_LOCK.while_and_ne(gen_conf.LMI2_STATUS.DLL2_LOCK.DLL2_LOCKED.value, gen_conf.LMI2_STATUS.DLL2_LOCK.DLL2_LOCKED.value)
