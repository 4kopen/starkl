import sttp
import tap
import tap.jtag
import time
import string

sbag_debug = 0


def pre_connect_callback_fn():

    debug_print(sbag_debug, " Executing sbag_unlock callback")
    
    # Need to identify which SOC we are on
    # Find out platform name
    tpname = sttp.stmc.get_target_params()["platform"]

    # Find out SOC name   
    l = len(tpname)
    if tpname[l-4:].isdigit():  soc_id_str = tpname[l-5:]     # case where the SOC name has 4 digits
    else :                      soc_id_str = tpname[l-4:]     # case where it has only 3 digits
    
    # Ensure it is in upper case
    soc_id_str = soc_id_str.upper()
    debug_print(sbag_debug, " SOC_ID = %s" %soc_id_str)

    # Retrieve the unlock test key from secure GIT
    import keyfetch
    keystring =  (keyfetch.keyring[soc_id_str])
    debug_print (sbag_debug, " got 0x%s" % keystring)

    # Check for nvs_safmem_data_ready=1
    testkey = wait_nvs_ready()

    # Shift the key in
    shift_unlock_test_key(keystring[str(testkey)])
    
    # Verify that is went ok => bit 0 of status must be read at 1
    read_jtag_status_register()


def wait_nvs_ready():

    nvs_safmem_data_ready_bit = 0x0001
    status =0
    
    if not ( status & nvs_safmem_data_ready_bit ):
        loop_count = 0
        dr_timeout = 20
        sttp.logging.print_out( " Waiting for nvs_safmem_data_ready=1 . . " )

        while not ( status & nvs_safmem_data_ready_bit ):
            # sleep a while
            time.sleep(1)
            loop_count = loop_count + 1
            if ( loop_count == dr_timeout ):
                sttp.logging.print_out( "\n\n" )
                raise sttp.STTPException("\n\n\t >>> ERROR: Timeout waiting for nvs_safmem_data_ready=1 <<< \n")
            # Read JTAG System Register
            (status, extra_bits) = read_jtag_status_register()
    else :
        (status, extra_bits) = read_jtag_status_register()

    debug_print (sbag_debug, " nvs_safmem_data_ready is set" )
    return extra_bits
    

def shift_unlock_test_key(keystring):

    idle_sync = 10
    unlock_tst_cmd  = "0100"

    keyvalue = int(keystring, 16)
    key_str  = tap.jtag.value_to_bitstring(keyvalue, 128, 0)

    dr_str = unlock_tst_cmd + key_str
    l = len(dr_str)

    debug_print(sbag_debug, " tdi unlock test: %s " %dr_str )

    tap.jtag.tmc_shift_ir(0x04)
    
    r = sttp.jtag.sequence({'tms':          '100' + '0'*(l-1) + '1' + '10',  \
                            'td_to_target': '000' +     dr_str      + '00', })
                            
    sttp.jtag.sequence({'tms': '0'*idle_sync, 'td_to_target': '0'*idle_sync, })
    return 


def read_jtag_status_register():

    key_len = 128
    read_status_cmd = "0000"
    
    # Set Read Status
    #  Set instruction for tap_security_mode: code 00100(lsb first) -> 0x04 
    tap.jtag.tmc_shift_ir(0x04)

    #  add instruction (read_status_cmd=0000) for read JTAG System Register (from JPU block)
    dr_str = read_status_cmd

    #  add dummy data
    dr_str += "0" * key_len

    #  convert
    dr_val = tap.jtag.bitstring_to_value(dr_str)

    # execute command
    tap.jtag.tmc_shift_dr(dr_val, len(dr_str))

    # size of jtag system register
    data_len = 92

    #  set data to be all zero's
    dr_str = "0" * data_len

    #  convert
    dr_val = tap.jtag.bitstring_to_value(dr_str)

    # execute command and shift-out data
    read_reg_cmd = tap.jtag.tmc_shift_dr(dr_val, len(dr_str))

    # convert [MSB is on the right, hence dir=1 is passed to value_to_bitstring()]
    jtag_reg_data = tap.jtag.value_to_bitstring(read_reg_cmd, 92, 1)

    # JTAG Register[15:0] = Status
    status    = jtag_reg_data[0:-76]      # 1st 16-bits, so remove last 72-bits
    extra_bits  = jtag_reg_data[88:]      # last four bits are extras in post-Orly3 architecture

    # convert
    status    = tap.jtag.bitstring_to_value(status, 0)
    extra_bits  = tap.jtag.bitstring_to_value(extra_bits, 0)
    debug_print(sbag_debug, " read_jtag_status_register() = 0x%.4x" % status )
    debug_print(sbag_debug, " read_jtag_extrabits_register() = 0x%x" % extra_bits )

    return (status, extra_bits&0x03)


def reverse_string_by_bytes(in_str):

    length = len(in_str)
    op_str = ""
    l = 0

    while ( l < length ):
        l = l + 2
        
        str = in_str[0:2]     # take first 2 chars (8-bits)
        op_str = str + op_str
        in_str = in_str[2:]   # remove 2 chars (8-bits)

    return op_str


def debug_print(debug, s):

    if debug:
        sttp.logging.print_out(s)
