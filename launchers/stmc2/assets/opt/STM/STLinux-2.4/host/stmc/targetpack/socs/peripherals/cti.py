import sys

import sttp
import sttp.logging
import sttp.pokepeek
import sttp.targetpack

cti_list = list()

def init(ctis):
    global cti_list

    for c in ctis:
        cti_list.append(c)

def find_name(name, in_out_key_name):
    found = None

    for c in cti_list:        
        for i in c[in_out_key_name]:
            if (None != found) and (name == i["name"] or (i.has_key("short_name") and name == i["short_name"])):
                raise sttp.STTPException("Error: multiple definitions for CTI input: %s" % (name,))
            if (None == found) and (name == i["name"] or (i.has_key("short_name") and name == i["short_name"])):
                found = (c,i)

    return found

def ref_inc(cti, enabled):
    if cti.has_key("enable_count"):
        cti["enable_count"] += 1 
    else:
        cti["enable_count"] = 1

    # If already eanbled "by some other sw, (such as CA9 debug agent)  inc the ref count
    if 1 == cti["enable_count"] and enabled:
        cti["enable_count"] += 1

    return cti["enable_count"]

def ref_dec(cti):
    if cti.has_key("enable_count"):
        if cti["enable_count"] == 0:
            raise sttp.STTPException("Error: CTI not enabled")
        else:
            cti["enable_count"] -= 1
    else:
        raise sttp.STTPException("Error: CTI not enabled")
    return cti["enable_count"]

def enable_disable_ctis(doEnable, from_info, to_info, ch):
    from_cti, from_item = from_info
    to_cti,   to_item   = to_info

    if from_cti.has_key("glue"):
        if "Type_A" != from_cti["glue"]["type"]:
            raise sttp.STTPException("Error: Invalid STI type %s" % (from_cti["glue"]["type"],))
        glue = from_cti["glue"]["address"]

        if from_item.has_key("invert") and int(from_item["invert"]):
#            sttp.logging.print_out("*** Setting invert")
            sttp.pokepeek.or_const(glue, glue, 1 << from_item["bit"])
        else:
            sttp.pokepeek.and_const(glue, glue, ~(1 << from_item["bit"]) )

        if from_item.has_key("edge") and int(from_item["edge"]):
#            sttp.logging.print_out("*** Setting edge")
            sttp.pokepeek.or_const(glue, glue, 1 << (8 + from_item["bit"]))
        else:
            sttp.pokepeek.and_const(glue, glue, ~(1 << (8 + from_item["bit"])) )


        ## HACK
#        sttp.logging.print_out("*** from_item: %s" % (from_item,) )
#        sttp.logging.print_out("*** enable_disable_ctis(): glue: 0x%08x: = 0x%08x" % (glue, sttp.pokepeek.peek(glue)) )
#        sttp.pokepeek.or_const(glue, glue, 0xff000000)
#        sttp.pokepeek.and_const(glue, glue, 0xffff)


    # unlock
    from_instance = from_cti["instance"].coresight_cti

    from_instance.DBGLAR.poke(0xc5acce55)
    # enable
    ctiinen_base = from_instance.CTIINEN_BASE.get_address()
    ctiinen      = ctiinen_base + (4*from_item["bit"])

    if doEnable:
        sttp.pokepeek.or_const(ctiinen, ctiinen, 1<<ch)
        enabled = from_instance.CTICONTROL.peek()
        from_instance.CTICONTROL.or_const(1)
        ref_inc(from_cti, enabled)
    else:
        c = ref_dec(from_cti)
        sttp.pokepeek.and_const(ctiinen, ctiinen, ~(1<<ch))
        if 0 == c:
            from_instance.CTICONTROL.and_const(~1)

    # unlock
    to_instance = to_cti["instance"].coresight_cti
    to_instance.DBGLAR.poke(0xc5acce55)
    # enable
    ctiouten_base = to_instance.CTIOUTEN_BASE.get_address()
    ctiouten      = ctiouten_base + (4*to_item["bit"])

    if doEnable:
        sttp.pokepeek.or_const(ctiouten, ctiouten, 1<<ch)
        enabled = to_instance.CTICONTROL.peek()
        to_instance.CTICONTROL.or_const(1)
        ref_inc(to_cti, enabled)
    else:
        c = ref_dec(to_cti)
        sttp.pokepeek.and_const(ctiouten, ctiouten, ~(1<<ch))
        if 0 == c:
            to_instance.CTICONTROL.and_const(~1)

def ack_cti(from_info, to_info):
    from_cti, from_item = from_info
    to_cti,   to_item   = to_info
    to_instance = to_cti["instance"].coresight_cti
    to_instance.CTIINTACK.or_const(1<<(to_item["bit"]))
    to_instance.CTIINTACK.and_const(~(1<<(to_item["bit"])))

#    to_instance.CTIINTACK.or_const(0xff)


    """
    if to_cti.has_key("glue"):
        if "Type_A" != to_cti["glue"]["type"]:
            raise sttp.STTPException("Error: Invalid STI type %s" % (to_cti["glue"]["type"],))

        glue = to_cti["glue"]["address"]

        ## TODOif from_item.has_key("ack") and int(from_item["ack"]):
        sttp.pokepeek.or_const(glue, glue, 1 << (24 + to_item["bit"]))
    """

    """
    if from_cti.has_key("glue"):
        if "Type_A" != from_cti["glue"]["type"]:
            raise sttp.STTPException("Error: Invalid STI type %s" % (from_cti["glue"]["type"],))

        glue = from_cti["glue"]["address"]

        if from_item.has_key("ack") and int(from_item["ack"]):
            sttp.logging.print_out("*** ACKing bit %d" % (24 + from_item["bit"]) )

            sttp.pokepeek.or_const(glue, glue, 1 << (24 + from_item["bit"]))
            ##sttp.pokepeek.and_const(glue, glue, ~(1 << (24 + from_item["bit"])) )
     """

def enable_disable(doEnable, from_name, to_name, ch):

    # lookup from in all CTI tins

    from_item = find_name(from_name, "tins")

    if None == from_item:
        raise sttp.STTPException("Cannot find CTI trigin: %s" % (from_name,)) 

    # loopup to   in all CTI touts

    to_item = find_name(to_name, "touts")

    if None == to_item:
        raise sttp.STTPException("Cannot find CTI trigout: %s" % (to_name,)) 

    # connect
    enable_disable_ctis(doEnable, from_item, to_item, ch)

    return from_item, to_item


def enable(from_name, to_name, ch):
    return enable_disable(True, from_name, to_name, ch)


def disable(from_name, to_name, ch):
    return enable_disable(False, from_name, to_name, ch)


def ack(from_name, to_name, ch):
    to_item = find_name(to_name, "touts")

    if None == to_item:
        raise sttp.STTPException("Cannot find CTI trigout: %s" % (to_name,)) 
        return None

    from_item = find_name(from_name, "tins")

    if None == from_item:
        raise sttp.STTPException("Cannot find CTI trigout: %s" % (from_name,)) 
        return None

    # connect
    ack_cti(from_item, to_item)


def dump_status(cti):
    sttp.logging.print_out("CTI base:         0x%08x" % (cti.CTICONTROL.get_address(),))
    sttp.logging.print_out("CTICONTROL:       0x%08x" % (cti.CTICONTROL.peek(),))
    sttp.logging.print_out("CTISTATUS:        0x%08x" % (cti.CTISTATUS.peek(),))
    sttp.logging.print_out("CTITRIGINSTATUS:  0x%08x" % (cti.CTITRIGINSTATUS.peek(), ))
    sttp.logging.print_out("CTITRIGOUTSTATUS: 0x%08x" % (cti.CTITRIGOUTSTATUS.peek(), ))
    sttp.logging.print_out("CTICHINSTATUS:    0x%08x" % (cti.CTICHINSTATUS.peek(), ))
    sttp.logging.print_out("CTICHOUTSTATUS:   0x%08x" % (cti.CTICHOUTSTATUS.peek(), ))

