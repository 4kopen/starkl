# Functions for controlling the PMBs on a 32-bit SE mode capable ST40 core

import sttp
import sttp.targetpack

global _st40_pmb_control_initialised
_st40_pmb_control_initialised = 0

global pmb_arrr_array
pmb_addr_array = []

global pmb_data_array
pmb_data_array = []


# Initialise the PMB arrays
def st40_pmb_control_initialise(core=None):
    global _st40_pmb_control_initialised
    if _st40_pmb_control_initialised != 0:
        return

    root = sttp.targetpack.get_tree()
 
    if None == core:
        core = sttp.targetinfo.get_init_core()

    cpu_root = sttp.targetpack.find_cpu(root, core)

    for i in range(16):
        evalStr = "cpu_root.parent.PMB_ADDR_ARRAY.st40_pmb_addr_array_regs.PMB_ADDR_ARRAY_ENTRY%d" % (i,)
        x = eval(evalStr)
        pmb_addr_array.append(x)

        evalStr = "cpu_root.parent.PMB_DATA_ARRAY.st40_pmb_data_array_regs.PMB_DATA_ARRAY_ENTRY%d" % (i,)
        x = eval(evalStr)
        pmb_data_array.append(x)
    _st40_pmb_control_initialised = 1


# Set up and enable a single PMB entry
def st40_set_pmb(index, virtual, physical, size, cache = 1, wt = 0, ub = 0):
    """Enable a PMB entry
       Usage: st40_set_pmb(<index>, <virtual>, <physical>, <size>, [<cache> = 1, [<wt> = 0, [<ub> = 0]]]])
       where <index> is the PMB entry
       +     <virtual> is the virtual page number
       +     <physical> is the physical page number
       +     <size> is the page size in MBytes
       +     <cache> is optional and is the page cacheability (default: 1 [on])
       +     <wt> is optional and is the page cache mode (default: 0 [copy-back])
       +     <ub> is optional and is the page buffer mode (default: 0 [buffered])
       
       Returns -1 on error."""
    if _st40_pmb_control_initialised == 0:
        raise sttp.STTPException("Error st40_set_pmb(): Must call st40_pmb_control_initialise()")
    
    pmbdata = 0x00000000
  
    # PMB[n].SZ
    if size == 16:
        pmbdata |= 0x00000000
    elif size == 64:
        pmbdata |= 0x00000010
    elif size == 128:
        pmbdata |= 0x00000080
    elif size == 512:
        pmbdata |= 0x00000090
    else:
        ## Unsupported page size
        raise sttp.STTPException("Error st40_set_pmb(): Unsupported page size %s" % (size,))
  
    # PMB[n].C (default is 1)
    pmbdata |= ((cache & 0x1) << 3)
  
    # PMB[n].WT (default is 0)
    pmbdata |= ((wt & 0x1) << 0)
  
    # PMB[n].UB (default is 0)
    pmbdata |= ((ub & 0x1) << 9)
  
    pmb_addr_array[index].poke((virtual & 0xff) << 24)
    pmb_data_array[index].poke(((physical & 0xff) << 24) | (1 << 8) | pmbdata)
    return 0


# Clear a single PMB entry
def st40_clear_pmb(index):
    """Disable a PMB entry
       Usage: st40_clear_pmb <index>
       where <index> is the PMB entry"""
    if _st40_pmb_control_initialised == 0:
        raise sttp.STTPException("Error: Must call st40_pmb_control_initialise(<soc_name>)")

    if index < 0 or index > 15:
        raise sttp.STTPException("Error st40_clear_pmb(): PMB index %d out of range" % (index,))

    pmb_data_array[index].poke(0x00000000)
    pmb_addr_array[index].poke(0x80000000)
    return 0


# Clear all PMB entries
def st40_clear_all_pmbs():
    """Disable all PMB entries
       Usage: st40_clear_all_pmbs"""
    if _st40_pmb_control_initialised == 0:
        raise sttp.STTPException("Error st40_clear_all_pmbs(): Must call st40_pmb_control_initialise(<soc_name>)")

    for i in range(16):
        st40_clear_pmb(i)
    return 0


# Enable SH4 32-bit enhanced mode
def st40_enhanced_mode(mode, core=None):
    """Set or unset 32-bit space enhancement mode
       Usage: st40_enhanced_mode <mode>
       where <mode> enables or disables space enhancement mode"""

    root = sttp.targetpack.get_tree()

    if None == core:
        core = sttp.targetinfo.get_init_core()

    cpu_root = sttp.targetpack.find_cpu(root, core)
    ccn = cpu_root.parent.CCN.st40_ccn_regs

    # The SE bit in the MMUCR can only be set to 1 in ST40-200 and ST40-500.  We
    # assume we are SE capable if this is called, so if the bit cannot be set to
    # 0, this must be an ST40-300.

    ccn.CCN_MMUCR.or_const(1 << 4)

    if ccn.CCN_MMUCR.if_eq(1 << 4, 1 << 4):
        ccn.CCN_MMUCR.read_modify_write(~(1 << 4), 0, (mode & 0x1) << 4)
    else:
        ccn.CCN_PASCR.read_modify_write(~(1 << 31), 0, (mode & 0x1) << 31)

    return 0
