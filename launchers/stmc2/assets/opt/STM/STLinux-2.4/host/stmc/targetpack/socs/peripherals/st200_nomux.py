import sttp
import sttp.logging
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import convertor


#
# Setup for mb388 / mb427 FPGA platform to use JtagStreamer or TAPmux driver
#

def setup_jtag_debug_port(parameters):

    convertor.check_convertor(parameters)

    sttp.targetinfo.setup_cores()

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)

    if sttp.stmc.get_soc_info()["debug"] == "tapmux":
        # Manual control of all JTAG lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        # Need this for TAPmux FPGA
        sttp.stmc.core_map({"tmc":"0"})
    else:

        # assume all initialized to 0 in driver
        p = "use_rtck"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_USE_RTCK", [ v ])
        
        p = "rtck_timeout"
        # default to 1
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 1 
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_RTCK_TIMEOUT", [ v ])

        p = "tdo_sample_delay"
        # default to 0
        if parameters.has_key(p):
            v = int(parameters[p])
        else:
            v = 0
        sttp.stmc.ioctl("STMC_BASE_FPGA_SET_TDO_SAMPLE_DELAY", [ v ])
  
    inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1"})

    # Now enable outputs
    sttp.stmc.output_enable(True)
    
    p = "vtref_check"
    # default to 0
    if parameters.has_key(p):
        v = int(parameters[p])
    else:
        v = 0

    if v:
        # Check vtref - note VTREF can be read when LVDS outputs are disabled
        if 0 == int(inputs["vtref"]):
            raise sttp.STTPException("vtref signal on target JTAG connector is low - target may be disconnected or not powered")

    tap_reset = True
    sys_reset = True

    if parameters.has_key("no_sys_reset") and int(parameters["no_sys_reset"])==1: 
        sys_reset = False

    if parameters.has_key("no_tap_reset") and int(parameters["no_tap_reset"])==1:
        tap_reset = False

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1:
        sys_reset = False
        tap_reset = False

    if sys_reset:
        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        sttp.jtag.sequence({'nrst' : "0"})
        # What should the min delay be?

        sttp.stmc.delay(100000)

        sttp.jtag.sequence({'nrst' : "1"})    

        sttp.targetinfo.target_in_reset_state()

        sttp.logging.print_out("Performed system reset")
    else:
        # No reset case
        sttp.targetinfo.target_in_no_reset_state()  

        sttp.logging.print_out("*NOT* performed system reset")

    if tap_reset:
        sttp.jtag.sequence({'ntrst' : "1"})
        sttp.jtag.sequence({'ntrst' : "0"})
        # What should the min delay be?
        sttp.stmc.delay(100000)

        sttp.jtag.sequence({'ntrst' : "1"})    
        # Autoclock of tck
        sttp.jtag.set_mode(sttp.jtag.SINGLE_CLOCK)
        # Move to RTI
        sttp.jtag.sequence({"tms": "111110",  "td_to_target": "000000"})

        sttp.logging.print_out("Performed TAP reset and moved to RTI")
    else:
        sttp.logging.print_out("*NOT* performed TAP reset")

    if sttp.stmc.get_soc_info()["debug"] == "tapmux":
        # Need this for TAPmux driver
        # Plumb the 1st JTAG module to the output lines
        # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
        c = sttp.targetinfo.get_targetstring_core()
        sttp.stmc.core_map({c:"0"})

def setup_taplink_debug_port(parameters):

    convertor.check_convertor(parameters)

    sttp.targetinfo.setup_cores()

    # Setup the debug channel mappings so that tmc (jtag) is plumbed to debug channel 0
    sttp.stmc.core_map({"tmc":0})

    sttp.jtag.set_mode(sttp.jtag.MANUAL_CONTROL)
  
    inputs = sttp.jtag.sequence({"nrst": "1", "ntrst": "1", "tms" : "0", "td_to_target" : "0", "trig_to_target" : "1"})

    # Now enable outputs
    sttp.stmc.output_enable(True)

    tap_reset = True
    sys_reset = True

    if parameters.has_key("no_sys_reset") and int(parameters["no_sys_reset"])==1: 
        sys_reset = False

    if parameters.has_key("no_tap_reset") and int(parameters["no_tap_reset"])==1:
        tap_reset = False

    if parameters.has_key("no_reset") and int(parameters["no_reset"])==1:
        sys_reset = False
        tap_reset = False

    if sys_reset:
        # Do a reset
        sttp.jtag.sequence({'nrst' : "1"})
        sttp.jtag.sequence({'nrst' : "0"})
        # What should the min delay be?
        sttp.stmc.delay(100000)

        sttp.jtag.sequence({'nrst' : "1"})    

        sttp.targetinfo.target_in_reset_state()

    else:
        # No reset case
        sttp.targetinfo.target_in_no_reset_state()  

    if tap_reset:
        sttp.jtag.sequence({'ntrst' : "1"})
        sttp.jtag.sequence({'ntrst' : "0"})
        # What should the min delay be?
        sttp.stmc.delay(100000)
        
        sttp.jtag.sequence({'ntrst' : "1"})    

    # Setup the STMC multiplexor side so that the core is plumbed to the tapmux channel 0
    bypass_core_name = sttp.targetinfo.get_targetstring_core()
    sttp.stmc.core_map({bypass_core_name:0}) 


