# Interco/stbus recommended values setup
# WARNING: Will be applied on enabled IPs only or if settings match defined conditions
#          (ex: raster vs macroblock). Moreover could be overwritten by drivers.

import sttp
global sttp_root
sttp_root = sttp.targetpack.get_tree()
import stxh410_uart

def init(parameters):

    sttp.logging.print_out("Setting interco recommended values")

    ## Group: GPU
    # Nothing in here

    ## Group: HQVDPLite
    # RD_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9ce1004,0x9ce1004,0xfffffff0,0,0x2)
    # RD_MIN_OPC
    sttp.pokepeek.read_modify_write(0x9ce1008,0x9ce1008,0xfffffff8,0,0x3)
    # RD_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9ce100c,0x9ce100c,0xfffffff8,0,0x6)
    # RD_MAX_CHK
    sttp.pokepeek.read_modify_write(0x9ce1010,0x9ce1010,0xfffffff8,0,0x1)
    # RD_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9ce1014,0x9ce1014,0xfffffff8,0,0x0)
    # RD_MIN_SPACE
    sttp.pokepeek.read_modify_write(0x9ce1018,0x9ce1018,0xffff0000,0,0x0)
    # WR_PAGE
    sttp.pokepeek.read_modify_write(0x9ce3004,0x9ce3004,0xfffffff0,0,0x2)
    # WR_MIN_OPC
    sttp.pokepeek.read_modify_write(0x9ce3008,0x9ce3008,0xfffffff8,0,0x3)
    # WR_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9ce300c,0x9ce300c,0xfffffff8,0,0x6)
    # WR_MAX_CHK
    sttp.pokepeek.read_modify_write(0x9ce3010,0x9ce3010,0xfffffff8,0,0x1)
    # WR_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9ce3014,0x9ce3014,0xfffffff8,0,0x0)
    # WR_MIN_SPACE
    sttp.pokepeek.read_modify_write(0x9ce3018,0x9ce3018,0xffff0000,0,0x0)

    ## Group: VDP
    # DEI_T3I_CTL
    sttp.pokepeek.read_modify_write(0x9d10010,0x9d10010,0xfc00ff00,0,0x000a3)
    # DEI_T3I_MOTION_CTL  ==> not to be programmed ??
    #sttp.pokepeek.read_modify_write(0xfd5440b0,0xfd5440b0,0xfc00ff00,0,0x600a3)
    ## Group: COMPO
    # CUR_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d110ec,0x9d110ec,0xfffffff0,0,0x2)
    # CUR_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d110f0,0x9d110f0,0xfffffff8,0,0x6)
    # CUR_MAX_CHK
    sttp.pokepeek.read_modify_write(0x9d110f8,0x9d110f8,0xfffffff8,0,0x1)
    # CUR_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d110fc,0x9d110fc,0xfffffff0,0,0x0)
    # GDP1_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d111ec,0x9d111ec,0xfffffff0,0,0x2)
    # GDP1_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d111f0,0x9d111f0,0xfffffff8,0,0x6)
    # GDP1_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d111f8,0x9d111f8,0xfffffff8,0,0x1)
    # GDP1_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d111fc,0x9d111fc,0xfffffff0,0,0x0)
    # GDP2_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d112ec,0x9d112ec,0xfffffff0,0,0x2)
    # GDP2_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d112f0,0x9d112f0,0xfffffff8,0,0x6)
    # GDP2_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d112f8,0x9d112f8,0xfffffff8,0,0x1)
    # GDP2_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d112fc,0x9d112fc,0xfffffff0,0,0x0)
    # GDP3_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d113ec,0x9d113ec,0xfffffff0,0,0x2)
    # GDP3_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d113f0,0x9d113f0,0xfffffff8,0,0x6)
    # GDP3_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d113f8,0x9d113f8,0xfffffff8,0,0x1)
    # GDP3_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d113fc,0x9d113fc,0xfffffff0,0,0x0)
    # GDP4_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d114ec,0x9d114ec,0xfffffff0,0,0x2)
    # GDP4_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d114f0,0x9d114f0,0xfffffff8,0,0x6)
    # GDP4_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d114f8,0x9d114f8,0xfffffff8,0,0x1)
    # GDP4_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d114fc,0x9d114fc,0xfffffff0,0,0x0)
    # ALP_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d116ec,0x9d116ec,0xfffffff0,0,0x2)
    # ALP_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d116f0,0x9d116f0,0xfffffff8,0,0x6)
    # ALP_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d116f8,0x9d116f8,0xfffffff8,0,0x1)
    # ALP_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d116fc,0x9d116fc,0xfffffff0,0,0x0)
    # CAP_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d11eec,0x9d11eec,0xfffffff0,0,0x2)
    # CAP_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d11ef0,0x9d11ef0,0xfffffff8,0,0x6)
    # CAP_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d11ef8,0x9d11ef8,0xfffffff8,0,0x1)
    # CAP_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d11efc,0x9d11efc,0xfffffff0,0,0x8)
    ## Group: DELTA
    # DMU0_PP0_OPC
    sttp.pokepeek.read_modify_write(0x8c00030,0x8c00030,0xfffffff8,0,0x6)
    # DMU0_PP0_CHUNK
    sttp.pokepeek.read_modify_write(0x8c00034,0x8c00034,0xfffffff8,0,0x1)
    # DMU0_PP0_MSG
    sttp.pokepeek.read_modify_write(0x8c00038,0x8c00038,0x7ffffff8,0,0x80000000)
    # PP_READ_BW_LIMITER
    sttp.pokepeek.read_modify_write(0x8c0004c,0x8c0004c,0xfffffc00,0,0x0)
    # PP_WRITE_BW_LIMITER
    sttp.pokepeek.read_modify_write(0x8c00050,0x8c00050,0xfffffc00,0,0x0)
    # HWCFG_OPC
    sttp.pokepeek.read_modify_write(0x8c0010c,0x8c0010c,0xc0c0e0c0,0,0x35361e2e)
    # HWCFG_CHUNK
    sttp.pokepeek.read_modify_write(0x8c00110,0x8c00110,0xccc0f820,0,0x12080191)
    # HWCFG_MSG
    sttp.pokepeek.read_modify_write(0x8c00114,0x8c00114,0x07c0e0f8,0,0xf8000800)
    # MCC_CFG
    sttp.pokepeek.read_modify_write(0x8c00120,0x8c00120,0xfffffcfe,0,0x201)
    ## Group: BLITTER0
    # BDISP_0_PLUG_S1_OP2
    sttp.pokepeek.read_modify_write(0x9f10b04,0x9f10b04,0xfffffff8,0,0x6)
    # BDISP_0_PLUG_S1_CHZ
    sttp.pokepeek.read_modify_write(0x9f10b08,0x9f10b08,0xfffffff8,0,0x1)
    # BDISP_0_PLUG_S1_MSZ
    sttp.pokepeek.read_modify_write(0x9f10b0c,0x9f10b0c,0xfff8,0,0x0)
    # BDISP_0_PLUG_S1_PGZ
    sttp.pokepeek.read_modify_write(0x9f10b10,0x9f10b10,0xfffffff0,0,0x2)
    # BDISP_0_PLUG_S2_OP2
    sttp.pokepeek.read_modify_write(0x9f10b24,0x9f10b24,0xfffffff8,0,0x6)
    # BDISP_0_PLUG_S2_CHZ
    sttp.pokepeek.read_modify_write(0x9f10b28,0x9f10b28,0xfffffff8,0,0x1)
    # BDISP_0_PLUG_S2_MSZ
    sttp.pokepeek.read_modify_write(0x9f10b2c,0x9f10b2c,0xfff8,0,0x0)
    # BDISP_0_PLUG_S2_PGZ
    sttp.pokepeek.read_modify_write(0x9f10b30,0x9f10b30,0xfffffff0,0,0x2)
    # BDISP_0_PLUG_S3_OP2
    sttp.pokepeek.read_modify_write(0x9f10b44,0x9f10b44,0xfffffff8,0,0x6)
    # BDISP_0_PLUG_S3_CHZ
    sttp.pokepeek.read_modify_write(0x9f10b48,0x9f10b48,0xfffffff8,0,0x1)
    # BDISP_0_PLUG_S3_MSZ
    sttp.pokepeek.read_modify_write(0x9f10b4c,0x9f10b4c,0xfff8,0,0x0)
    # BDISP_0_PLUG_S3_PGZ
    sttp.pokepeek.read_modify_write(0x9f10b50,0x9f10b50,0xfffffff0,0,0x2)
    # BDISP_0_PLUG_T_OP2
    sttp.pokepeek.read_modify_write(0x9f10b84,0x9f10b84,0xfffffff8,0,0x6)
    # BDISP_0_PLUG_T_CHZ
    sttp.pokepeek.read_modify_write(0x9f10b88,0x9f10b88,0xfffffff8,0,0x1)
    # BDISP_0_PLUG_T_MSZ
    sttp.pokepeek.read_modify_write(0x9f10b8c,0x9f10b8c,0xfff0,0,0x8)
    # BDISP_0_PLUG_T_PGZ
    sttp.pokepeek.read_modify_write(0x9f10b90,0x9f10b90,0xfffffff0,0,0x2)
    ## Group: BLITTER1
    # BDISP_1_PLUG_S1_OP2
    sttp.pokepeek.read_modify_write(0x9f11b04,0x9f11b04,0xfffffff8,0,0x6)
    # BDISP_1_PLUG_S1_CHZ
    sttp.pokepeek.read_modify_write(0x9f11b08,0x9f11b08,0xfffffff8,0,0x1)
    # BDISP_1_PLUG_S1_MSZ
    sttp.pokepeek.read_modify_write(0x9f11b0c,0x9f11b0c,0xfff8,0,0x0)
    # BDISP_1_PLUG_S1_PGZ
    sttp.pokepeek.read_modify_write(0x9f11b10,0x9f11b10,0xfffffff0,0,0x2)
    # BDISP_1_PLUG_S2_OP2
    sttp.pokepeek.read_modify_write(0x9f11b24,0x9f11b24,0xfffffff8,0,0x6)
    # BDISP_1_PLUG_S2_CHZ
    sttp.pokepeek.read_modify_write(0x9f11b28,0x9f11b28,0xfffffff8,0,0x1)
    # BDISP_1_PLUG_S2_MSZ
    sttp.pokepeek.read_modify_write(0x9f11b2c,0x9f11b2c,0xfff8,0,0x0)
    # BDISP_1_PLUG_S2_PGZ
    sttp.pokepeek.read_modify_write(0x9f11b30,0x9f11b30,0xfffffff0,0,0x2)
    # BDISP_1_PLUG_S3_OP2
    sttp.pokepeek.read_modify_write(0x9f11b44,0x9f11b44,0xfffffff8,0,0x6)
    # BDISP_1_PLUG_S3_CHZ
    sttp.pokepeek.read_modify_write(0x9f11b48,0x9f11b48,0xfffffff8,0,0x1)
    # BDISP_1_PLUG_S3_MSZ
    sttp.pokepeek.read_modify_write(0x9f11b4c,0x9f11b4c,0xfff8,0,0x0)
    # BDISP_1_PLUG_S3_PGZ
    sttp.pokepeek.read_modify_write(0x9f11b50,0x9f11b50,0xfffffff0,0,0x2)
    # BDISP_1_PLUG_T_OP2
    sttp.pokepeek.read_modify_write(0x9f11b84,0x9f11b84,0xfffffff8,0,0x6)
    # BDISP_1_PLUG_T_CHZ
    sttp.pokepeek.read_modify_write(0x9f11b88,0x9f11b88,0xfffffff8,0,0x1)
    # BDISP_1_PLUG_T_MSZ
    sttp.pokepeek.read_modify_write(0x9f11b8c,0x9f11b8c,0xfff0,0,0x8)
    # BDISP_1_PLUG_T_PGZ
    sttp.pokepeek.read_modify_write(0x9f11b90,0x9f11b90,0xfffffff0,0,0x2)
    ## Group: PCMPLAYER
    # PCMP0_FORMAT
    sttp.pokepeek.read_modify_write(0x8d80040,0x8d80040,0xffff80ff,0,0x2800)
    # PCMP1_FORMAT
    sttp.pokepeek.read_modify_write(0x8d81040,0x8d81040,0xffff80ff,0,0x2800)
    # PCMP2_FORMAT
    sttp.pokepeek.read_modify_write(0x8d82040,0x8d82040,0xffff80ff,0,0x2800)
    ## Group: PCMREADER
    # PCMR0_FORMAT
    sttp.pokepeek.read_modify_write(0x8d83040,0x8d83040,0xffff80ff,0,0x2800)
    # PCMR0_FORMAT
    sttp.pokepeek.read_modify_write(0x8d84040,0x8d84040,0xffff80ff,0,0x2800)
    ## Group: SPDIFPLAYER
    # SPDIFP0_FORMAT
    sttp.pokepeek.read_modify_write(0x8d85040,0x8d85040,0xffff80ff,0,0x2800)
    ## Group: USB2_0
    if sttp.pokepeek.if_eq(0x92b0004,0x10,0x0):
        # USB2_0_AD_CONFIG
        sttp.pokepeek.read_modify_write(0x9a03f04,0x9a03f04,0xffc00000,0,0x264208)
        # USB2_0_IC_CONFIG
        sttp.pokepeek.read_modify_write(0x92c0018,0x92c0018,0xfffffffe,0,0x1)
    ## Group: USB2_1
    if sttp.pokepeek.if_eq(0x92b0004,0x20,0x0):
        # USB2_1_AD_CONFIG
        sttp.pokepeek.read_modify_write(0x9a83f04, 0x9a83f04,0xffc00000,0,0x264208)
        # USB2_1_IC_CONFIG
        sttp.pokepeek.read_modify_write(0x92c0018,0x92c0018,0xfffffffd,0,0x2)
    ## Group: USB3_0
    if sttp.pokepeek.if_eq(0x92b0004,0x40,0x0):
        # USB3_0_REG0
        sttp.pokepeek.read_modify_write(0x9901000, 0x9901000,0xfffffff7,0,0x8)
        # USB3_0_REG1
        sttp.pokepeek.read_modify_write(0x9901004, 0x9901004,0xfffff0ff,0,0xf00)
        # USB3_0_IC_CONFIG
        #sttp.pokepeek.read_modify_write(0x92c0014,0x92c0014,0x8080ffff,0,0x2)
    ## Group: ETH
    if sttp.pokepeek.if_eq(0x9600008,0x10,0x1):
        # ETH_REG0
        sttp.pokepeek.read_modify_write(0x9631000,0x9631000,0xfefec0ff,0,0x1000800)
        # ETH_REG10
        sttp.pokepeek.read_modify_write(0x9631028,0x9631028,0xff00fff0,0,0x770009)
    ## Group: PCIE  ==> ??
    ## Group: SATA  ==> ??
    # Group: TANGO
    sttp.pokepeek.read_modify_write(0x882a000, 0x882a000, 0xfffffff0, 0, 0x2)
    sttp.pokepeek.read_modify_write(0x882a008, 0x882a008, 0xfffffff8, 0, 0x3)
    sttp.pokepeek.read_modify_write(0x882a004, 0x882a004, 0xfffffff8, 0, 0x6)
    sttp.pokepeek.read_modify_write(0x882a00c, 0x882a00c, 0xfffffff8, 0, 0x1)
    sttp.pokepeek.read_modify_write(0x882a010, 0x882a010, 0xfffffff8, 0, 0x0)
    sttp.pokepeek.read_modify_write(0x8828000, 0x8828000, 0xfffffff0, 0, 0x2)
    sttp.pokepeek.read_modify_write(0x8828008, 0x8828008, 0xfffffff8, 0, 0x3)
    sttp.pokepeek.read_modify_write(0x8828004, 0x8828004, 0xfffffff8, 0, 0x6)
    sttp.pokepeek.read_modify_write(0x882800c, 0x882800c, 0xfffffff8, 0, 0x1)
    sttp.pokepeek.read_modify_write(0x8828010, 0x8828010, 0xfffffff8, 0, 0x0)
    sttp.pokepeek.read_modify_write(0x88aa000, 0x88aa000, 0xfffffff0, 0, 0x2)
    sttp.pokepeek.read_modify_write(0x88aa008, 0x88aa008, 0xfffffff8, 0, 0x3)
    sttp.pokepeek.read_modify_write(0x88aa004, 0x88aa004, 0xfffffff8, 0, 0x6)
    sttp.pokepeek.read_modify_write(0x88aa00c, 0x88aa00c, 0xfffffff8, 0, 0x1)
    sttp.pokepeek.read_modify_write(0x88aa010, 0x88aa010, 0xfffffff8, 0, 0x0)
    sttp.pokepeek.read_modify_write(0x88aa000, 0x88aa000, 0xfffffff0, 0, 0x2)
    sttp.pokepeek.read_modify_write(0x88a8008, 0x88a8008, 0xfffffff8, 0, 0x3)
    sttp.pokepeek.read_modify_write(0x88a8004, 0x88a8004, 0xfffffff8, 0, 0x6)
    sttp.pokepeek.read_modify_write(0x88a800c, 0x88a800c, 0xfffffff8, 0, 0x1)
    sttp.pokepeek.read_modify_write(0x88a8010, 0x88a8010, 0xfffffff8, 0, 0x0)
    ## Group: STFE_DMA
    # STFE_DMA_MAX_OPC
    sttp.pokepeek.read_modify_write(0x8a2ffb0,0x8a2ffb0,0xfffffff8,0,0x6)
    # STFE_DMA_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x8a2ffbc,0x8a2ffbc,0xfffffff0,0,0x1)
    ## Group: HVA
    # HVA_MIF_CFG
    # HVA needs a clock to be able to program its plug ??
    sttp.pokepeek.read_modify_write(0x8c85108,0x8c85108,0xc000c000,0,0x00408081)
    ## Group: JPEG DECODER
    # JPEG_DEC_MCU_PARAM
    sttp.pokepeek.read_modify_write(0x8c84004,0x8c84004,0xfffffe00,0,0xe)
    # JPEG_DEC_OST_PROG0
    sttp.pokepeek.read_modify_write(0x8c84080,0x8c84080,0xffffff7f,0,0x80)
    ## Group: VP8 DECODER
    # VP8_DEC_REG2
    sttp.pokepeek.read_modify_write(0x8c80008,0x8c80008,0xffffffe0,0,0x10)
    ## Group: MediaLB
    # MLB_AD_CONFIG
    sttp.pokepeek.read_modify_write(0x9b38804, 0x9b38804,0xffc00000,0,0x244004)

    ## Group:HADES
    sttp.pokepeek.read_modify_write(0x9d122d0,0x9d122d0,0xfffffff0,0,0x2)
    # DVP_CHR_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d122d4,0x9d122d4,0xfffffff8,0,0x6)
    # DVP_CHR_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d122d8,0x9d122d8,0xfffffff8,0,0x1)
    # DVP_CHR_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d122dc,0x9d122dc,0xfffffff0,0,0x8)
    # DVP_LUM_PAGE_SIZE
    sttp.pokepeek.read_modify_write(0x9d122ec,0x9d122ec,0xfffffff0,0,0x2)
    # DVP_LUM_MAX_OPC
    sttp.pokepeek.read_modify_write(0x9d122f0,0x9d122f0,0xfffffff8,0,0x6)
    # DVP_LUM_MAX_CHK,
    sttp.pokepeek.read_modify_write(0x9d122f8,0x9d122f8,0xfffffff8,0,0x1)
    # DVP_LUM_MAX_MSG
    sttp.pokepeek.read_modify_write(0x9d122fc,0x9d122fc,0xfffffff0,0,0x8)
    ## Group:HADES
    #sttp.logging.print_out("Configuring HADES PP PLUGS ...")
    sttp.pokepeek.or_const(0x0A000804, 0x0A000804, (1 << 0)) # HADES-PP_RD Lock Request
    sttp.pokepeek.while_and_ne(0x0A000808, 1 << 0, 1 << 0) # HADES-PP_RD Status Polling
    sttp.pokepeek.poke(0x0A000000 + 0x800, 0x00000002) # HADES-PP_RD G1
    sttp.pokepeek.poke(0x0A000000 + 0x820, 0x0000000E) # HADES-PP_RD C1
    sttp.pokepeek.and_const(0x0A000804, 0x0A000804, ~(1 << 0)) # HADES-PP_RD Unlock
    #sttp.logging.print_out("Done HADES-PP_RD ...")

    sttp.pokepeek.or_const(0x0A000004, 0x0A000004, (1 << 0)) # HADES-PP_WR Lock Request
    sttp.pokepeek.while_and_ne(0x0A000008, 1 << 0, 1 << 0) # HADES-PP_WR Status Polling
    sttp.pokepeek.poke(0x0A000000 + 0x000, 0x00000002) # HADES-PP_WR G1
    sttp.pokepeek.poke(0x0A000000 + 0x020, 0x0000000E) # HADES-PP_WR C1-CLIENT1
    sttp.pokepeek.poke(0x0A000000 + 0x030, 0x0000000E) # HADES-PP_WR C1-CLIENT2
    sttp.pokepeek.poke(0x0A000000 + 0x040, 0x0000000E) # HADES-PP_WR C1-CLIENT3
    sttp.pokepeek.poke(0x0A000000 + 0x050, 0x0000000E) # HADES-PP_WR C1-CLIENT4
    sttp.pokepeek.poke(0x0A000000 + 0x060, 0x0000000E) # HADES-PP_WR C1-CLIENT5
    sttp.pokepeek.and_const(0x0A000004, 0x0A000004, ~(1 << 0)) # HADES-PP_WR Unlock
    #sttp.logging.print_out("Done HADES-PP_WR ...")

    #sttp.logging.print_out("Configuring HADES XPRED PLUG ...")
    sttp.pokepeek.or_const(0x0A020004, 0x0A020004, (1 << 0)) # HADES-XPRED Lock Request
    sttp.pokepeek.while_and_ne(0x0A020008, 1 << 0, 1 << 0) # HADES-XPRED Status Polling
    sttp.pokepeek.poke(0x0A020000 + 0x000, 0x00000002) # HADES-XPRED G1
    sttp.pokepeek.poke(0x0A020000 + 0x020, 0x0000000D) # HADES-XPRED C1
    sttp.pokepeek.and_const(0x0A020004, 0x0A020004, ~(1 << 0)) # HADES-XPRED Unlock
    #sttp.logging.print_out("Done HADES-XPRED ...")

    #sttp.logging.print_out("Configuring HADES Slave RAB ...")
    # RAB entry 0: Fabric controller registers: Host offset 00100000 => Hades 50000000, size 80000
    sttp.pokepeek.poke(0x0A010000 + 0x04, 0x100000)   # host registers start offset
    sttp.pokepeek.poke(0x0A010000 + 0x08, 0x17ffff)   # host registers end offset
    sttp.pokepeek.poke(0x0A010000 + 0x0C, 0x50000000) # Fabric controller registers in Hades address space
    sttp.pokepeek.poke(0x0A010000 + 0x00, 0x000007)   # RAB_READ_WRITE + RAB_WITHOUT_COHERENCY
    # RAB entry 1: Cluster #0 peripheral registers: Host offset 00200000 => Hades 20000000, size 80000
    sttp.pokepeek.poke(0x0A010000 + 0x14, 0x200000)   # host registers start offset
    sttp.pokepeek.poke(0x0A010000 + 0x18, 0x27ffff)   # host registers end offset
    sttp.pokepeek.poke(0x0A010000 + 0x1C, 0x20000000) # Cluster #0 peripheral registers in Hades address space
    sttp.pokepeek.poke(0x0A010000 + 0x10, 0x000007)   # RAB_READ_WRITE + RAB_WITHOUT_COHERENCY
    #  RAB slave activate
    sttp.pokepeek.poke(0x0A011000 + 0x00, 0x000000)
    #sttp.logging.print_out("Done HADES slave RAB programming...")

    #sttp.logging.print_out("Configuring HADES DCHAN plugs ...")
    sttp.pokepeek.or_const(0x0A262004, 0x0A262004, (1 << 0)) # HADES-DCHAN_RD Lock Request
    sttp.pokepeek.while_and_ne(0x0A262008, 1 << 0, 1 << 0) # HADES-DCHAN_RD Status Polling
    #sttp.logging.print_out("   unlocked !")
    sttp.pokepeek.poke(0x0A262000 + 0x000, 0x00000002) # HADES-DCHAN_RD	G1
    sttp.pokepeek.poke(0x0A262000 + 0x020, 0x0000000E) # HADES-DCHAN_RD	C1
    sttp.pokepeek.and_const(0x0A262004, 0x0A262004, ~(1 << 0)) # HADES-DCHAN_RD Unlock
    #sttp.logging.print_out("Done HADES-DCHAN_RD w/o procedure...")

    sttp.pokepeek.or_const(0x0A262804, 0x0A262804, (1 << 0)) # HADES-DCHAN_WR Lock Request
    sttp.pokepeek.while_and_ne(0x0A262808, 1 << 0, 1 << 0) # HADES-DCHAN_WR Status Polling
    sttp.pokepeek.poke(0x0A262000 + 0x800, 0x00010002) # HADES-DCHAN_WR	G1
    sttp.pokepeek.poke(0x0A262000 + 0x820, 0x0000000E) # HADES-DCHAN_WR	C1
    sttp.pokepeek.and_const(0x0A262804, 0x0A262804, ~(1 << 0)) # HADES-DCHAN_WR Unlock
    #sttp.logging.print_out("Done HADES-DCHAN_WR w/o procedure...")

    # INI related.
    #stxh410_uart.write("TP: Configuring interco INI\n\r")
    sttp.pokepeek.poke(0x0933E000 + 0x14, 0x00000002)
    sttp.pokepeek.poke(0x0933E000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x1C, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x2C, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x0933E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0933F000 + 0x14, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x1C, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x2C, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x0933F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0x14, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x18, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x1C, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x20, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x24, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x28, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x2C, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0x30, 0x00000000)
    sttp.pokepeek.poke(0x0932A000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932C000 + 0x14, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x18, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x1C, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x20, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x24, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x28, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x2C, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0x30, 0x02000002)
    sttp.pokepeek.poke(0x0932C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932D000 + 0x14, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x18, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x1C, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x20, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x24, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x28, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x2C, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0x30, 0x02000002)
    sttp.pokepeek.poke(0x0932D000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09340000 + 0x14, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x18, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x1C, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x20, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x24, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x28, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x2C, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0x30, 0x01000001)
    sttp.pokepeek.poke(0x09340000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09341000 + 0x14, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x18, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x1C, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x20, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x24, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x28, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x2C, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0x30, 0x01000001)
    sttp.pokepeek.poke(0x09341000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963B000 + 0x14, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x18, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x1C, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x20, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x24, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x28, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x2C, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0x30, 0x00000002)
    sttp.pokepeek.poke(0x0963B000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963C000 + 0x14, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x18, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x1C, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x20, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x24, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x28, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x2C, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0x30, 0x00000002)
    sttp.pokepeek.poke(0x0963C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09305000 + 0x14, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x18, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x1C, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x20, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x24, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x28, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x2C, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0x30, 0x00000000)
    sttp.pokepeek.poke(0x09305000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09306000 + 0x14, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x18, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x1C, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x20, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x24, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x28, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x2C, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0x30, 0x00000000)
    sttp.pokepeek.poke(0x09306000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09309000 + 0x14, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x18, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x1C, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x20, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x24, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x28, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x2C, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0x30, 0x00000000)
    sttp.pokepeek.poke(0x09309000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09326000 + 0x14, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x1C, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x2C, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x09326000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963D000 + 0x14, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x18, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x1C, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x20, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x24, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x28, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x2C, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x30, 0x00000002)
    sttp.pokepeek.poke(0x0963D000 + 0x140, 0x00000001)
    sttp.pokepeek.poke(0x09345000 + 0x14, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x1C, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x2C, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x09345000 + 0xC8, 0x00000001)
    sttp.pokepeek.poke(0x09346000 + 0x14, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x18, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x1C, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x20, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x24, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x28, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x2C, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0x30, 0x01000001)
    sttp.pokepeek.poke(0x09346000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09347000 + 0x14, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x18, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x1C, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x20, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x24, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x28, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x2C, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0x30, 0x01000001)
    sttp.pokepeek.poke(0x09347000 + 0xB8, 0x00000001)

    sttp.pokepeek.poke(0x09345000 + 0x44, 0x10000003)
    sttp.pokepeek.poke(0x09345000 + 0xC8, 0x00000001)

    sttp.pokepeek.poke(0x0933E000 + 0x4, 0x08000003)
    sttp.pokepeek.poke(0x0933E000 + 0x8, 0x08000003)
    sttp.pokepeek.poke(0x0933E000 + 0xC, 0x08000003)
    sttp.pokepeek.poke(0x0933E000 + 0x10, 0x08000003)
    sttp.pokepeek.poke(0x0933E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0933F000 + 0x4,  0x08000003)
    sttp.pokepeek.poke(0x0933F000 + 0x8,  0x08000003)
    sttp.pokepeek.poke(0x0933F000 + 0xC,  0x08000003)
    sttp.pokepeek.poke(0x0933F000 + 0x10, 0x08000003)
    sttp.pokepeek.poke(0x0933F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09327000 + 0x4,  0x07000001)
    sttp.pokepeek.poke(0x09327000 + 0x8,  0x07000001)
    sttp.pokepeek.poke(0x09327000 + 0xC,  0x07000001)
    sttp.pokepeek.poke(0x09327000 + 0x10, 0x07000001)
    sttp.pokepeek.poke(0x09327000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09328000 + 0x4,  0x07000001)
    sttp.pokepeek.poke(0x09328000 + 0x8,  0x07000001)
    sttp.pokepeek.poke(0x09328000 + 0xC,  0x07000001)
    sttp.pokepeek.poke(0x09328000 + 0x10, 0x07000001)
    sttp.pokepeek.poke(0x09328000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0x4,  0x00000003)
    sttp.pokepeek.poke(0x09329000 + 0x8,  0x00000003)
    sttp.pokepeek.poke(0x09329000 + 0xC,  0x00000003)
    sttp.pokepeek.poke(0x09329000 + 0x10, 0x00000003)
    sttp.pokepeek.poke(0x09329000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0x4,  0x00000003)
    sttp.pokepeek.poke(0x0932A000 + 0x8,  0x00000003)
    sttp.pokepeek.poke(0x0932A000 + 0xC,  0x00000003)
    sttp.pokepeek.poke(0x0932A000 + 0x10, 0x00000003)
    sttp.pokepeek.poke(0x0932A000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0x4,  0x00000003)
    sttp.pokepeek.poke(0x0932B000 + 0x8,  0x00000003)
    sttp.pokepeek.poke(0x0932B000 + 0xC,  0x00000003)
    sttp.pokepeek.poke(0x0932B000 + 0x10, 0x00000003)
    sttp.pokepeek.poke(0x0932B000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932C000 + 0x4,  0x07000001)
    sttp.pokepeek.poke(0x0932C000 + 0x8,  0x07000001)
    sttp.pokepeek.poke(0x0932C000 + 0xC,  0x07000001)
    sttp.pokepeek.poke(0x0932C000 + 0x10, 0x07000001)
    sttp.pokepeek.poke(0x0932C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932D000 + 0x4,  0x07000001)
    sttp.pokepeek.poke(0x0932D000 + 0x8,  0x07000001)
    sttp.pokepeek.poke(0x0932D000 + 0xC,  0x07000001)
    sttp.pokepeek.poke(0x0932D000 + 0x10, 0x07000001)
    sttp.pokepeek.poke(0x0932D000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09340000 + 0x4,  0x08000003)
    sttp.pokepeek.poke(0x09340000 + 0x8,  0x08000003)
    sttp.pokepeek.poke(0x09340000 + 0xC,  0x08000003)
    sttp.pokepeek.poke(0x09340000 + 0x10, 0x08000003)
    sttp.pokepeek.poke(0x09340000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09341000 + 0x4,  0x08000003)
    sttp.pokepeek.poke(0x09341000 + 0x8,  0x08000003)
    sttp.pokepeek.poke(0x09341000 + 0xC,  0x08000003)
    sttp.pokepeek.poke(0x09341000 + 0x10, 0x08000003)
    sttp.pokepeek.poke(0x09341000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963B000 + 0x4,  0x08000002)
    sttp.pokepeek.poke(0x0963B000 + 0x8,  0x08000002)
    sttp.pokepeek.poke(0x0963B000 + 0xC,  0x08000002)
    sttp.pokepeek.poke(0x0963B000 + 0x10, 0x08000002)
    sttp.pokepeek.poke(0x0963B000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963C000 + 0x4,  0x08000002)
    sttp.pokepeek.poke(0x0963C000 + 0x8,  0x08000002)
    sttp.pokepeek.poke(0x0963C000 + 0xC,  0x08000002)
    sttp.pokepeek.poke(0x0963C000 + 0x10, 0x08000002)
    sttp.pokepeek.poke(0x0963C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09323000 + 0x4,  0x00000002)
    sttp.pokepeek.poke(0x09323000 + 0x8,  0x00000002)
    sttp.pokepeek.poke(0x09323000 + 0xC,  0x00000002)
    sttp.pokepeek.poke(0x09323000 + 0x10, 0x00000002)
    sttp.pokepeek.poke(0x09323000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09324000 + 0x4,  0x00000002)
    sttp.pokepeek.poke(0x09324000 + 0x8,  0x00000002)
    sttp.pokepeek.poke(0x09324000 + 0xC,  0x00000002)
    sttp.pokepeek.poke(0x09324000 + 0x10, 0x00000002)
    sttp.pokepeek.poke(0x09324000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09305000 + 0x4,  0x01000000)
    sttp.pokepeek.poke(0x09305000 + 0x8,  0x01000000)
    sttp.pokepeek.poke(0x09305000 + 0xC,  0x01000000)
    sttp.pokepeek.poke(0x09305000 + 0x10, 0x01000000)
    sttp.pokepeek.poke(0x09305000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09306000 + 0x4,  0x01000000)
    sttp.pokepeek.poke(0x09306000 + 0x8,  0x01000000)
    sttp.pokepeek.poke(0x09306000 + 0xC,  0x01000000)
    sttp.pokepeek.poke(0x09306000 + 0x10, 0x01000000)
    sttp.pokepeek.poke(0x09306000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09309000 + 0x4,  0x01000000)
    sttp.pokepeek.poke(0x09309000 + 0x8,  0x01000000)
    sttp.pokepeek.poke(0x09309000 + 0xC,  0x01000000)
    sttp.pokepeek.poke(0x09309000 + 0x10, 0x01000000)
    sttp.pokepeek.poke(0x09309000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09320000 + 0x4,  0x01000003)
    sttp.pokepeek.poke(0x09320000 + 0x8,  0x01000003)
    sttp.pokepeek.poke(0x09320000 + 0xC,  0x01000003)
    sttp.pokepeek.poke(0x09320000 + 0x10, 0x01000003)
    sttp.pokepeek.poke(0x09320000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09326000 + 0x4,  0x08000003)
    sttp.pokepeek.poke(0x09326000 + 0x8,  0x08000003)
    sttp.pokepeek.poke(0x09326000 + 0xC,  0x08000003)
    sttp.pokepeek.poke(0x09326000 + 0x10, 0x08000003)
    sttp.pokepeek.poke(0x09326000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963D000 + 0x4,  0x08000002)
    sttp.pokepeek.poke(0x0963D000 + 0x8,  0x08000002)
    sttp.pokepeek.poke(0x0963D000 + 0xC,  0x08000002)
    sttp.pokepeek.poke(0x0963D000 + 0x10, 0x08000002)
    sttp.pokepeek.poke(0x0963D000 + 0x140, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x4,  0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x8,  0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0xC,  0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x10, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x170, 0x00000001)
    sttp.pokepeek.poke(0x09345000 + 0x4,  0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0x8,  0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0xC,  0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0x10, 0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0xC8, 0x00000001)
    sttp.pokepeek.poke(0x0931F000 + 0x4,  0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x8,  0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0xC,  0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x10, 0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x120, 0x00000001)
    sttp.pokepeek.poke(0x09346000 + 0x4,  0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0x8,  0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0xC,  0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0x10, 0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09347000 + 0x4, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0x8, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0xC, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0xB8, 0x00000001)

    sttp.pokepeek.poke(0x0933E000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x0933E000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x0933E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0933F000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x0933F000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x0933F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09327000 + 0x38, 0x03000001)
    sttp.pokepeek.poke(0x09327000 + 0x40, 0x03000001)
    sttp.pokepeek.poke(0x09327000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09328000 + 0x38, 0x03000001)
    sttp.pokepeek.poke(0x09328000 + 0x40, 0x03000001)
    sttp.pokepeek.poke(0x09328000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932C000 + 0x38, 0x03000001)
    sttp.pokepeek.poke(0x0932C000 + 0x40, 0x03000001)
    sttp.pokepeek.poke(0x0932C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932D000 + 0x38, 0x03000001)
    sttp.pokepeek.poke(0x0932D000 + 0x40, 0x03000001)
    sttp.pokepeek.poke(0x0932D000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09340000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09340000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09340000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09341000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09341000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09341000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963B000 + 0x38, 0x06000003)
    sttp.pokepeek.poke(0x0963B000 + 0x40, 0x06000003)
    sttp.pokepeek.poke(0x0963B000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963C000 + 0x38, 0x06000003)
    sttp.pokepeek.poke(0x0963C000 + 0x40, 0x06000003)
    sttp.pokepeek.poke(0x0963C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09323000 + 0x38, 0x00000000)
    sttp.pokepeek.poke(0x09323000 + 0x40, 0x00000000)
    sttp.pokepeek.poke(0x09323000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09324000 + 0x38, 0x00000000)
    sttp.pokepeek.poke(0x09324000 + 0x40, 0x00000000)
    sttp.pokepeek.poke(0x09324000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09305000 + 0x38, 0x01000002)
    sttp.pokepeek.poke(0x09305000 + 0x40, 0x01000002)
    sttp.pokepeek.poke(0x09305000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09306000 + 0x38, 0x01000002)
    sttp.pokepeek.poke(0x09306000 + 0x40, 0x01000002)
    sttp.pokepeek.poke(0x09306000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09309000 + 0x38, 0x01000002)
    sttp.pokepeek.poke(0x09309000 + 0x40, 0x01000002)
    sttp.pokepeek.poke(0x09309000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0x38, 0x00000000)
    sttp.pokepeek.poke(0x0932E000 + 0x40, 0x00000000)
    sttp.pokepeek.poke(0x0932E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0x38, 0x00000000)
    sttp.pokepeek.poke(0x0932F000 + 0x40, 0x00000000)
    sttp.pokepeek.poke(0x0932F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09320000 + 0x38, 0x00000003)
    sttp.pokepeek.poke(0x09320000 + 0x40, 0x00000003)
    sttp.pokepeek.poke(0x09320000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09326000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09326000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09326000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963D000 + 0x38, 0x06000003)
    sttp.pokepeek.poke(0x0963D000 + 0x40, 0x06000003)
    sttp.pokepeek.poke(0x0963D000 + 0x140, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x38, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x40, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x170, 0x00000001)
    sttp.pokepeek.poke(0x09345000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0xC8, 0x00000001)
    sttp.pokepeek.poke(0x0931F000 + 0x38, 0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x40, 0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x120, 0x00000001)
    sttp.pokepeek.poke(0x09346000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09347000 + 0x38, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0x40, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0xB8, 0x00000001)

    sttp.pokepeek.poke(0x09301000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x1c, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x2c, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x34, 0x00000003)
    sttp.pokepeek.poke(0x09301000 + 0x178, 0x00000001)
    sttp.pokepeek.poke(0x09302000 + 0x18, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x1c, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x20, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x24, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x28, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x2c, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x30, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x34, 0x00000003)
    sttp.pokepeek.poke(0x09302000 + 0x178, 0x00000001)

    sttp.pokepeek.poke(0x0933E000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x0933E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0933F000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x0933F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09327000 + 0x34, 0x03000001)
    sttp.pokepeek.poke(0x09327000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09328000 + 0x34, 0x03000001)
    sttp.pokepeek.poke(0x09328000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0x34, 0x00000001)
    sttp.pokepeek.poke(0x09329000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0x34, 0x00000001)
    sttp.pokepeek.poke(0x0932A000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0x34, 0x00000001)
    sttp.pokepeek.poke(0x0932B000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x0932C000 + 0x34, 0x03000001)
    sttp.pokepeek.poke(0x0932C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932D000 + 0x34, 0x03000001)
    sttp.pokepeek.poke(0x0932D000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09340000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09340000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09341000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09341000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963B000 + 0x34, 0x06000003)
    sttp.pokepeek.poke(0x0963B000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963C000 + 0x34, 0x06000003)
    sttp.pokepeek.poke(0x0963C000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09323000 + 0x34, 0x00000000)
    sttp.pokepeek.poke(0x09323000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09324000 + 0x34, 0x00000000)
    sttp.pokepeek.poke(0x09324000 + 0xA0, 0x00000001)
    sttp.pokepeek.poke(0x09305000 + 0x34, 0x01000002)
    sttp.pokepeek.poke(0x09305000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09306000 + 0x34, 0x01000002)
    sttp.pokepeek.poke(0x09306000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09309000 + 0x34, 0x01000002)
    sttp.pokepeek.poke(0x09309000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932E000 + 0x34, 0x00000000)
    sttp.pokepeek.poke(0x0932E000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0932F000 + 0x34, 0x00000000)
    sttp.pokepeek.poke(0x0932F000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09320000 + 0x34, 0x00000003)
    sttp.pokepeek.poke(0x09320000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09326000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09326000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x0963D000 + 0x34, 0x06000003)
    sttp.pokepeek.poke(0x0963D000 + 0x140, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x44, 0x00000001)
    sttp.pokepeek.poke(0x09310000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x44, 0x00000001)
    sttp.pokepeek.poke(0x09311000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x44, 0x00000001)
    sttp.pokepeek.poke(0x09344000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x44, 0x00000001)
    sttp.pokepeek.poke(0x09312000 + 0x160, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x44, 0x00000001)
    sttp.pokepeek.poke(0x09313000 + 0x170, 0x00000001)
    sttp.pokepeek.poke(0x09345000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09345000 + 0xC8, 0x00000001)
    sttp.pokepeek.poke(0x0931F000 + 0x34, 0x00000003)
    sttp.pokepeek.poke(0x0931F000 + 0x120, 0x00000001)
    sttp.pokepeek.poke(0x09346000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09346000 + 0xB8, 0x00000001)
    sttp.pokepeek.poke(0x09347000 + 0x34, 0x0c000003)
    sttp.pokepeek.poke(0x09347000 + 0xB8, 0x00000001)

