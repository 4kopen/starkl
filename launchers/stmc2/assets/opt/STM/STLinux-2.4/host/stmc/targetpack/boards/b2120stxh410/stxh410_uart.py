###
### STxh410 UARTs init functions
### (C) R. Singh, F. Charpentier
###

# 02/jul/14  FCh Added UART2 from Marc C

import sttp

# STxh410 UARTs reminder
#  0: UART_0 (COMMS/PIO17) => currently unsupported
#  1: UART_1 (COMMS/PIO16) => TX/RX=PIO16[0]/[1] Alternate 1
#  2: UART_2 (COMMS/PIO15) => currently unsupported
#  3: UART_3 (COMMS/PIO31) => currently unsupported
#  4: SBC_UART_0 (SBC/PIO3) => TX/RX=PIO3[4]/[5] Alternate 1
#  5: SBC_UART_1 (SBC/PIO2) => TX/RX=PIO2[6]/[7] Alternate 3

SBC_LPM = 0x9400000
SBC_LPM_CFG = 0x0B5100
LPM_CONFIG_1 = SBC_LPM + SBC_LPM_CFG + 0x4
SYSCFG_000 = 0x09620000 # SYSCFG_PIO_SBC_000_999
SYSCFG_1000 = 0x9280000 # SYSCFG_PIO_FRONT

max_uart = 5
uarts = [
    {"desc":""},
    {
    "desc"      : "UART_1=COMMS/PIO16",
    "base"      : 0x9831000,
    "alt_reg"   : SYSCFG_1000 + 0x18,
    "alt_mask"  : ~((7 << 4) | (7 << 0)),
    "alt_val"   : (1 << 4) | (1 << 0),
    "oe_reg"    : SYSCFG_1000 + 0xa4,
    "oe_bit"    : 1 << 16
    },
    {
    "desc"      : "UART_2=COMMS/PIO15",
    "base"      : 0x9832000,
    "alt_reg"   : SYSCFG_1000 + 0x14,
    "alt_mask"  : ~((7 << 4) | (7 << 0)),
    "alt_val"   : (1 << 4) | (1 << 0),
    "oe_reg"    : SYSCFG_1000 + 0xa4,
    "oe_bit"    : 1 << 8
    },
    {"desc":""},
    {
    "desc"      : "SBC_UART_0=SBC/PIO3",
    "base"      : SBC_LPM + 0x130000,
    "alt_reg"   : SYSCFG_000 + 0xc,
    "alt_mask"  : ~((7 << 20) | (7 << 16)),
    "alt_val"   : (1 << 20) | (1 << 16),
    "oe_reg"    : SYSCFG_000 + 0xa0,
    "oe_bit"    : 1 << 28
    },
    {
    "desc"      : "SBC_UART_1=SBC/PIO2",
    "base"      : SBC_LPM + 0x131000,
    "alt_reg"   : 0x9620000 + 8,
    "alt_mask"  : ~((7 << 28) | (7 << 24)),
    "alt_val"   : (3 << 28) | (3 << 24),
    "oe_reg"    : 0x9620000 + 0xa0,
    "oe_bit"    : 1 << 22
    },
]

base = 0   # UART base addr of initialized ASC

# Initialize UART 'uart_nb'
# Returns: 0 if OK, -1 if ERROR/unsupported
def init(uart_nb, bps, clk):
    global base

    if (uart_nb > max_uart):
        sttp.logging.print_out("uart.init() ERROR: Unexpected UART number")
        return -1

    desc = uarts[uart_nb]["desc"]
    if (desc == ""):
        sttp.logging.print_out("uart.init() ERROR: Unsupported UART number")
        return -1
    base = uarts[uart_nb]["base"]
    alt_reg = uarts[uart_nb]["alt_reg"]
    alt_mask = uarts[uart_nb]["alt_mask"]
    alt_val = uarts[uart_nb]["alt_val"]
    oe_reg = uarts[uart_nb]["oe_reg"]
    oe_bit = uarts[uart_nb]["oe_bit"]

    fifo_en = 1

    sttp.logging.print_out("Configuring UART %d for bps=%d, clk=%d" % (uart_nb, bps, clk))

    # UART requires pokepeek mode even if 'no_pokes=1'
    if not sttp.pokepeek.isEnabled():
        sttp.pokepeek.enable()
        disable_pokepeek = 1
    else:
        disable_pokepeek = 0


    # Setting proper alternate mode
    sttp.pokepeek.read_modify_write(alt_reg, alt_reg, alt_mask, 0, alt_val)
    # Setting OE for TX
    sttp.pokepeek.or_const(oe_reg, oe_reg, oe_bit)

    # ASC registers pokes
    #sttp.pokepeek.or_const(LPM_CONFIG_1, LPM_CONFIG_1, (1 << 12) | (1 << 11)) # now these bits are set at global level
    sttp.pokepeek.poke(base + 0xc, (fifo_en << 10) | 0x1109)
    sttp.pokepeek.poke(base + 0, (((bps * (1 << 14)) + (1 << 13)) / (clk / (1 << 6))))
    sttp.pokepeek.poke(base + 0x1c, 0x14)
    sttp.pokepeek.poke(base + 0xc, 0x1189)

    # Writing some initial message on UART
    write("This UART (%d:%s) is preconfigured from targetpack\r\n" % (uart_nb, desc));

    # Restoring pokepeek mode
    if disable_pokepeek:
        sttp.pokepeek.disable()

    return 0

# Send to message to UART.
def write(message):

    # UART requires pokepeek mode even if 'no_pokes=1'
    if not sttp.pokepeek.isEnabled():
        sttp.pokepeek.enable()
        disable_pokepeek = 1
    else:
        disable_pokepeek = 0

    msg_len = len(message)
    for i in range(msg_len):
        sttp.pokepeek.while_and_ne(base + 0x14, 1 << 9, 0 << 9) # Wait TX buf empty
        val = ord(message[i])
        sttp.pokepeek.poke(base + 0x4, val)

    # Restoring pokepeek mode
    if disable_pokepeek:
        sttp.pokepeek.disable()
