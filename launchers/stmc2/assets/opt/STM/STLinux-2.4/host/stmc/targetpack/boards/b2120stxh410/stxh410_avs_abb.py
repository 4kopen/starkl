###
###  AVS and ABB configuration for STih410
### (C) L.Vachon
###

import sttp
import sttp.stmc
import sttp.targetinfo
import sttp.targetpack

import stxh410

# Global variables
global sttp_root
sttp_root = sttp.targetpack.get_tree()

############################################################################
#                                                                         AVS & ABB tables  (old process v12)                                          #
############################################################################
# AVS table format: code 0 to 7, value from 0 to 255 corresponding to AVS pwm_val duty cycle
# ABB table format: code 0 to 7, value: [3..0] VBBP , [7..4] VBBN
#---------------------------------------------------------------------------------------------------------------------
#Process Code                                     0        1        2        3        4        5        6        7
#---------------------------------------------------------------------------------------------------------------------
# Core  2133 Mbps       AVS      1.080    1.100    1.080    1.030    1.000    0.980    0.960    0.950
core_lmi_2133_old =                 [ 1080,    1100,    1080,    1030,   1000,     980,      960,      950]
#         1866 Mbps       AVS      1.030    1.050    1.030    0.980    0.960    0.940    0.930    0.920
core_lmi_1866_old =                 [ 1030,    1050,    1030,    980,     960,      940,      930,      920]
#         1600 Mbps       AVS      0.960    1.000    0.960    0.930    0.920    0.910    0.900    0.900
core_lmi_1600_old =                 [ 960,     1000,     960,      930,    920,      910,      900,      900]
#---------------------------------------------------------------------------------------------------------------------                                                    
# A9    1500 MHz         AVS      1.150    1.150    1.150    1.140    1.130    1.120    1.110    1.100
cpu_1500_old =                       [ 1150,   1150,    1150,    1140,   1130,     1120,    1110,    1100]
#                               ABB      FBB 150  FBB 300  FBB 200  FBB 150  OFF      OFF      OFF      OFF
abb_1500_old =                       [ 0x344,   0x355,    0x344,    0x0,       0x0,      0x0,      0x0 ,    0x0]
#        1200 MHz          AVS      1.110    1.150    1.100    1.050    1.040    1.030    1.020   1.000
cpu_1200_old =                       [ 1110,   1150,    1100,   1050,    1040,    1030,    1020,   1000]
#                               ABB      OFF      FBB 250      OFF      OFF      OFF      OFF      OFF      OFF
abb_1200_old =                      [ 0x0,     0x344,        0x0,     0x0,      0x0,     0x0,      0x0,     0x0]
############################################################################                           
############################################################################
#                                                                         AVS & ABB tables  (new process)                                              #
############################################################################
# AVS table format: code 0 to 7, value from 0 to 255 corresponding to AVS pwm_val duty cycle
# ABB table format: code 0 to 7, value: [3..0] VBBP , [7..4] VBBN
#---------------------------------------------------------------------------------------------------------------------
#Process Code                                     0        1        2        3        4        5        6        7
#---------------------------------------------------------------------------------------------------------------------
# Core  2133 Mbps         AVS      1.090    1.110    1.090    1.060    1.030    1.000    0.970    0.930
core_lmi_2133 =                       [ 1090,   1110,   1090,    1060,     1030,    1000,    970,     930]
#         1866 Mbps         AVS      1.090    1.110    1.090    1.060    1.030    1.000    0.970    0.930
core_lmi_1866 =                      [ 1040,    1060,    1040,    1020,    1000,    980,     950,      920]
#         1600 Mbps         AVS      1.000    1.020    1.000    0.980    0.960    0.920    0.920    0.920
core_lmi_1600 =                      [ 1000,    1020,    1000,    980,     960,      920,     920,      920]
#---------------------------------------------------------------------------------------------------------------------                                                      
# A9    1500 MHz           AVS      1.200    1.200    1.200    1.200    1.170    1.140    1.100    1.070
cpu_1500 =                            [ 1200,    1200,    1200,    1200,    1170,    1140,    1100,   1070]
#                                 ABB      FBB 150  FBB 300  FBB 200  FBB 150  OFF      OFF      OFF      OFF
abb_1500 =                            [ 0x322, 0x355, 0x333, 0x322, 0x0, 0x0, 0x0 , 0x0]
#        1200 MHz            AVS      1.112    1.150    1.100    1.080    1.040    1.020    0.980    0.930
cpu_1200 =                            [ 1110,    1150,    1100,    1080,    1040,     1020,    980,     930]
#                                 ABB      OFF      OFF      OFF      OFF      OFF      OFF      OFF      OFF
abb_1200 =                            [ 0x0,     0x0,      0x0,      0x0,      0x0,     0x0,      0x0,     0x0]                             
############################################################################
BB_level = [ "50", "100", "150" , "200", "250", "300", "350", "400" ]
BB_mode = [ "FBB" , "RBB" ]

# Old code process storage 
Core_Process_info_reg_old = 0x08A6583C
Core_Process_info_start_old = 10
A9_Process_info_reg_old = 0x08A6583C
A9_Process_info_start_old = 10
# New code process storage 
Core_Process_info_reg = 0x08A65840
Core_Process_info_start = 10
A9_Process_info_reg = 0x08A65840
A9_Process_info_start = 16

# PIO registers
corecfg = sttp_root.stxh410.core_sysconf_regs.sysconf_regs

SBC_PWM_CTRL = 0x09510050   # pwm_val control register
SBC_PWM10_VAL = 0x09510000   # pwm_val output 0 value register
SBC_PWM11_VAL = 0x09510004   # pwm_val output 1 value register
SBC_PWM13_VAL = 0x0951000C   # pwm_val output 3 value register

# SBC PIO control regiter
SBC_SYSTEM_CONFIG4 = 0x09620010

SBC_SYSTEM_CONFIG3 = 0x0962000C

#Output enable pad control for all PIO alternate functions
SBC_SYSTEM_CONFIG40 = 0x096200A0
SBC_SYSTEM_CONFIG41 = 0x096200A4

#Pull up pad control for all PIO alternate functions
SBC_SYSTEM_CONFIG50 = 0x096200C8
SBC_SYSTEM_CONFIG51 = 0x096200CC

#Open drain pad control for all PIO alternate functions
SBC_SYSTEM_CONFIG60 = 0x096200F0
SBC_SYSTEM_CONFIG61 = 0x096200F4

# PIO Alternate function offset
PIO3_0_ALT = 0
PIO4_4_ALT = 16
PIO4_7_ALT = 28

# PIO Ourpur enable offset
PIO3_0_PAD = 24
PIO4_4_PAD = 4
PIO4_7_PAD = 7
    
    
def set_avs(parameters, OPTIONS_params):

    ca9_freq = int(parameters["ca9_freq"])
    lmi_freq = int(parameters["lmi_freq"])
    
    enable_avs()

    # Parsing options
    if (OPTIONS_params["Partition"] == "Core"):
        Process_info_reg = Core_Process_info_reg
        Process_info_start = Core_Process_info_start
        Process_info_reg_old = Core_Process_info_reg_old
        Process_info_start_old = Core_Process_info_start_old
                           
        #Operating point selection
        if (lmi_freq > 1866):
            voltage_table = OPTIONS_params["avs_lmi_2133"]
            voltage_table_old = OPTIONS_params["avs_lmi_2133_old"]
        else:
            if (lmi_freq > 1600):
                voltage_table = OPTIONS_params["avs_lmi_1866"]
                voltage_table_old = OPTIONS_params["avs_lmi_1866_old"]
            else:
                voltage_table = OPTIONS_params["avs_lmi_1600"]
                voltage_table_old = OPTIONS_params["avs_lmi_1600_old"]
    else:
        if (OPTIONS_params["Partition"] == "A9"):
            Process_info_reg = A9_Process_info_reg
            Process_info_start = A9_Process_info_start
            Process_info_reg_old = A9_Process_info_reg_old
            Process_info_start_old = A9_Process_info_start_old
            
            if (ca9_freq > 1200):
                voltage_table =OPTIONS_params["avs_a9_1500"]
                voltage_table_old =OPTIONS_params["avs_a9_1500_old"]
            else:
                voltage_table =OPTIONS_params["avs_a9_1200"]
                voltage_table_old =OPTIONS_params["avs_a9_1200_old"]
        else:
            raise sttp.STTPException("Unknown partition\n")
            
    OPTIONS_params["A"]   = 255.0 / (OPTIONS_params["Vmin"] - OPTIONS_params["Vmax"] )     
    OPTIONS_params["B"]   =255.0  -  OPTIONS_params["A"]  * OPTIONS_params["Vmin"]
     
    if (OPTIONS_params["PWM"] == 10):
            pwm_val = SBC_PWM10_VAL
    else:
        if (OPTIONS_params["PWM"] == 11):
            pwm_val = SBC_PWM11_VAL
        else:
            if (OPTIONS_params["PWM"] == 13):
                pwm_val = SBC_PWM13_VAL
            else:
                sttp.logging.print_out("Only SBC PWM numbers [10, 11 & 13] are allowed\n");
                raise sttp.STTPException("SBC pwm_val2 is not recommended for AVS control.\n") 
 
 
    # Convert voltages to PWM duty cycle values
    pwm_table = [ 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(0,8): 
        code = int((OPTIONS_params["B"]  + OPTIONS_params["A"] * voltage_table[i] ))
        if code < 0:
            code = 0
        if code > 255:
            code = 255
        pwm_table[i] = code
        
    pwm_table_old = [ 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(0,8): 
        code = int((OPTIONS_params["B"]  + OPTIONS_params["A"] * voltage_table_old[i] ))
        if code < 0:
            code = 0
        if code > 255:
            code = 255
        pwm_table_old[i] = code    
    
    # Setting PWM duty cycle accoding to programmed process code
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 0 <<Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[1])
    else: # managing samples programmed at old NVRAM location
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 0 <<Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[1])
        else:
            sttp.pokepeek.poke(pwm_val,pwm_table_old[0])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 1 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[2])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 2 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[3])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 3 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[4])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 4 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[5])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 5 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[6])
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 6 << Process_info_start_old):
            sttp.pokepeek.poke(pwm_val,pwm_table_old[7])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 1 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[2])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 2 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[3])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 3 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[4])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 4 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[5])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 5 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[6])
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 6 << Process_info_start):
        sttp.pokepeek.poke(pwm_val,pwm_table[7])
        
    # Overidding programmed process code
    if (OPTIONS_params["Forced_voltage"] == 1) :
        if (OPTIONS_params["Voltage"] > OPTIONS_params["Vmax"]):
           OPTIONS_params["Voltage"] = OPTIONS_params["Vmax"]
           sttp.logging.print_out("%s voltage should be lower than Vmax = %d mV. Clamped to Vmax." % (OPTIONS_params["Partition"],OPTIONS_params["Vmax"]))
        if (OPTIONS_params["Voltage"] < OPTIONS_params["Vmin"]):
           OPTIONS_params["Voltage"] = OPTIONS_params["Vmin"]
           sttp.logging.print_out("%s voltage should be lower than Vmin= %d mV. Clamped to Vmin." % (OPTIONS_params["Partition"],OPTIONS_params["Vmin"]))
        code =  int((OPTIONS_params["B"]  + OPTIONS_params["A"] *  OPTIONS_params["Voltage"] ))
        if code < 0:
            code = 0
        if code > 255:
            code = 255
        forced_pwm = code
        sttp.pokepeek.poke(pwm_val, forced_pwm)
    if (OPTIONS_params["Forced_code"] == 1):
        sttp.pokepeek.poke(pwm_val,pwm_table[OPTIONS_params["Code"]])

    #PWM PIO configuration
    if (OPTIONS_params["PWM"] == 10):
        # SBC_SYSTEM_CONFIG3: PIO3_0 alt 1 = PWM10_OUT
        sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG3, SBC_SYSTEM_CONFIG3, 0xFFFFFFFF & (~(0x7 << PIO3_0_ALT)), 0, 0x1 << PIO3_0_ALT) 
        # SBC_SYSTEM_CONFIG40: output enable for PIO3_0 PWM10
        sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG40, SBC_SYSTEM_CONFIG40, 0xFFFFFFFF, 0,  0x1 << PIO3_0_PAD) 
        # SBC_SYSTEM_CONFIG51: PU disable for PIO3_0 PWM10
        sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG50, SBC_SYSTEM_CONFIG50, ~(0x1 << PIO3_0_PAD), 0, 0)     
        # SBC_SYSTEM_CONFIG61: PUSH PULL enable for PIO3_0 PWM10
        sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG60, SBC_SYSTEM_CONFIG60, ~(0x1 << PIO3_0_PAD), 0, 0)         
    else:
        if (OPTIONS_params["PWM"] == 11):
            # SBC_SYSTEM_CONFIG4: PIO4_4 alt 1 = PWM11_OUT
            sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG4, SBC_SYSTEM_CONFIG4, 0xFFFFFFFF & (~(0x7 << PIO4_4_ALT)) , 0, 0x1 << PIO4_4_ALT)  
            # SBC_SYSTEM_CONFIG41: output enable for PIO4_4 PWM11
            sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG41, SBC_SYSTEM_CONFIG41, 0xFFFFFFFF, 0, 0x1 << PIO4_4_PAD)      
            # SBC_SYSTEM_CONFIG51: PU disable for PIO4_4 PWM11
            sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG51, SBC_SYSTEM_CONFIG51, ~(0x1 << PIO4_4_PAD), 0, 0)     
            # SBC_SYSTEM_CONFIG61: PUSH PULL enable for PIO4_4 PWM11
            sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG61, SBC_SYSTEM_CONFIG61, ~(0x1 << PIO4_4_PAD), 0, 0)         
        else:
            if (OPTIONS_params["PWM"] == 13):
                # SBC_SYSTEM_CONFIG4: PIO4_7 alt 3 = PWM13_OUT
                sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG4, SBC_SYSTEM_CONFIG4, 0xFFFFFFFF & (~(0x7 << PIO4_7_ALT)) , 0, 0x3 << PIO4_7_ALT) 
                # SBC_SYSTEM_CONFIG41: output enable for PIO4_7 PWM13
                sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG41, SBC_SYSTEM_CONFIG41, 0xFFFFFFFF, 0, 0x1 << PIO4_7_PAD)      
                # SBC_SYSTEM_CONFIG51: PU disable for PIO4_7 PWM13
                sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG51, SBC_SYSTEM_CONFIG51, ~(0x1 << PIO4_7_PAD), 0, 0)     
                # SBC_SYSTEM_CONFIG61: PUSH PULL enable for PIO4_7 PWM13
                sttp.pokepeek.read_modify_write(SBC_SYSTEM_CONFIG61, SBC_SYSTEM_CONFIG61, ~(0x1 << PIO4_7_PAD), 0, 0)                      
            else:
                sttp.logging.print_out("Only SBC PWM numbers [10, 11 & 13] are allowed\n");
                raise sttp.STTPException("SBC pwm_val2 is not recommended for AVS control.\n")   

    # Display info  
    sttp.logging.print_out("   %s AVS configuration:" % (OPTIONS_params["Partition"]))   
    if (OPTIONS_params["Partition"] == "Core"):
        sttp.logging.print_out("\t Optimized for %d Mbps" %(lmi_freq))
    else:
        if (OPTIONS_params["Partition"] == "A9"):
            sttp.logging.print_out("\t Optimized for %d MHz" %(ca9_freq))
        else:
            raise sttp.STTPException("Unknown partition\n")
    
    voltage = 0  
    if (sttp.stmc.get_operating_mode() != "ROMGEN"):
        #Read process information from non volatile memory
        process = (sttp.pokepeek.peek(Process_info_reg) >> Process_info_start) & 0xf
        process_old = (sttp.pokepeek.peek(Process_info_reg_old) >> Process_info_start_old) & 0xf
        voltage = voltage_table[process]
        sttp.logging.print_out("\t Process code (old location): %d" % (process_old))
        sttp.logging.print_out("\t Process code (new location): %d" % (process)) 
        if (process == 0):
            voltage = voltage_table_old[process_old]
        
            
    if (OPTIONS_params["Forced_code"] == 1):
        sttp.logging.print_out("\t Process code forced to %d" % (OPTIONS_params["Code"]))
        voltage = voltage_table[OPTIONS_params["Code"]]
    if (OPTIONS_params["Forced_voltage"] == 1):
        sttp.logging.print_out("\t Voltage forced to %d mV" % (OPTIONS_params["Voltage"]))
        voltage = OPTIONS_params["Voltage"]
        
    if (sttp.stmc.get_operating_mode() != "ROMGEN"):    
         sttp.logging.print_out("\t Expected Vsense voltage value  %d mV +/- 2%%" %(voltage))
         
    if ((sttp.stmc.get_operating_mode() != "ROMGEN") and OPTIONS_params["Debug"] > 0): 
        sttp.logging.print_out("\t AVS tables (old process)")
        sttp.logging.print_out("\t    Code\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(0,1,2,3,4,5,6,7))
        sttp.logging.print_out("\t    Voltages mV\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(voltage_table_old[0],voltage_table_old[1],voltage_table_old[2],voltage_table_old[3],voltage_table_old[4],voltage_table_old[5],voltage_table_old[6],voltage_table_old[7]))
        sttp.logging.print_out("\t    PWM codes\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(pwm_table_old[0],pwm_table_old[1],pwm_table_old[2],pwm_table_old[3],pwm_table_old[4],pwm_table_old[5],pwm_table_old[6],pwm_table_old[7]))
        sttp.logging.print_out("\t AVS tables (new process)")
        sttp.logging.print_out("\t    Code\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(0,1,2,3,4,5,6,7))
        sttp.logging.print_out("\t    Voltages mV\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(voltage_table[0],voltage_table[1],voltage_table[2],voltage_table[3],voltage_table[4],voltage_table[5],voltage_table[6],voltage_table[7]))
        sttp.logging.print_out("\t    PWM codes\t: %4d  %4d  %4d  %4d  %4d  %4d  %4d  %4d" %(pwm_table[0],pwm_table[1],pwm_table[2],pwm_table[3],pwm_table[4],pwm_table[5],pwm_table[6],pwm_table[7]))
        sttp.logging.print_out("\t Using SBC PWM%d" %(OPTIONS_params["PWM"]))
        sttp.logging.print_out("\t Using PWMcode = %f x Voltage + %f  " %(OPTIONS_params["A"],OPTIONS_params["B"]))
        sttp.logging.print_out("\t Vmin = %d and Vmax = %d" %(OPTIONS_params["Vmin"],OPTIONS_params["Vmax"]))
        
    if ((sttp.stmc.get_operating_mode() != "ROMGEN") and OPTIONS_params["Debug"] == 2): 
        print_param(parameters, "Global")
        print_param(OPTIONS_params, "AVS hardware")
        print_param(OPTIONS_params, "AVS & ABB Options")
       
    return  
    
    
def set_abb(parameters, OPTIONS_params):    

    ca9_freq = int(parameters["ca9_freq"])    
        
    if (OPTIONS_params["Partition"] == "A9"):
        Process_info_reg = A9_Process_info_reg
        Process_info_start = A9_Process_info_start
        Process_info_reg_old = A9_Process_info_reg_old
        Process_info_start_old = A9_Process_info_start_old
        if (ca9_freq > 1200):
            abb_table = OPTIONS_params["abb_a9_1500"]
            abb_table_old = OPTIONS_params["abb_a9_1500_old"]
        else:
            abb_table = OPTIONS_params["abb_a9_1200"]
            abb_table_old = OPTIONS_params["abb_a9_1200_old"]
    else:         
       raise sttp.STTPException("The Body Biasing is not supported on this partition.\n")
       
    # step 1: Put the BBGen in bypass mode, i.e. vbb*_enable = 1, with vbb*_bypass_n = opp_data_bbsel[1:0] = 00
    vbbp_iout=0 # Configures the output current of the VBBP generator
    vbbp_bw=0 # Configures the bandwidth of the VBBP generator
    vbbp_compens=0 # Configures the compensation inside the VBBP generator
    vbbn_iout=0xf # Configures the output current of the VBBN generator
    vbbn_bw=0x3 # Configures the bandwidth of the VBBN generator
    vbbn_compens=0x3 # Configures the compensation inside the VBBN generator
    corecfg.SYSTEM_CONFIG5116.poke(0x0)
    corecfg.SYSTEM_CONFIG5117.poke(0x0)
    corecfg.SYSTEM_CONFIG5116.or_const(vbbp_iout<<12 | vbbp_bw<<8 | vbbp_compens<<4)
    corecfg.SYSTEM_CONFIG5117.or_const(vbbn_iout<<12 | vbbn_bw<<8 | vbbn_compens<<4)
    
    #opp_data_bbsel[1:0] = 00
    corecfg.SYSTEM_CONFIG5114.read_modify_write( ~(0x3 << 4),0,0)
    # step 2 Enable
    
    if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 1 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[1] >> 8) & 1) <<17)
        corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[1] >> 9) & 1) <<17)
    else:
        if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 2 << Process_info_start):
            corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[2] >> 8) & 1) <<17)
            corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[2] >> 9) & 1) <<17)
        else:
            if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 3 << Process_info_start):
                corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[3] >> 8) & 1) <<17)
                corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[3] >> 9) & 1) <<17)
            else:
                if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 4 << Process_info_start):
                    corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[4] >> 8) & 1) <<17)
                    corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[4] >> 9) & 1) <<17)
                else:
                    if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 5 << Process_info_start):
                        corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[5] >> 8) & 1) <<17)
                        corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[5] >> 9) & 1) <<17)
                    else:
                        if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 6 << Process_info_start):
                            corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[6] >> 8) & 1) <<17)
                            corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[6] >> 9) & 1) <<17)
                        else:
                            if sttp.pokepeek.if_eq(Process_info_reg, 0xf << Process_info_start, 7 << Process_info_start):
                                corecfg.SYSTEM_CONFIG5116.or_const(((abb_table[7] >> 8) & 1) <<17)
                                corecfg.SYSTEM_CONFIG5117.or_const(((abb_table[7] >> 9) & 1) <<17)
                            else: #old process code location management
                                if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 1 << Process_info_start_old):
                                    corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[1] >> 8) & 1) <<17)
                                    corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[1] >> 9) & 1) <<17)
                                else:
                                    if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 2 << Process_info_start_old):
                                        corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[2] >> 8) & 1) <<17)
                                        corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[2] >> 9) & 1) <<17)
                                    else:
                                        if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 3 << Process_info_start_old):
                                            corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[3] >> 8) & 1) <<17)
                                            corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[3] >> 9) & 1) <<17)
                                        else:
                                            if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 4 << Process_info_start_old):
                                                corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[4] >> 8) & 1) <<17)
                                                corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[4] >> 9) & 1) <<17)
                                            else:
                                                if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 5 << Process_info_start_old):
                                                    corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[5] >> 8) & 1) <<17)
                                                    corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[5] >> 9) & 1) <<17)
                                                else:
                                                    if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 6 << Process_info_start_old):
                                                        corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[6] >> 8) & 1) <<17)
                                                        corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[6] >> 9) & 1) <<17)
                                                    else:
                                                        if sttp.pokepeek.if_eq(Process_info_reg_old, 0xf << Process_info_start_old, 7 << Process_info_start_old):
                                                            corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[7] >> 8) & 1) <<17)
                                                            corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[7] >> 9) & 1) <<17)
                                                        else:
                                                            corecfg.SYSTEM_CONFIG5116.or_const(((abb_table_old[0] >> 8) & 1) <<17)
                                                            corecfg.SYSTEM_CONFIG5117.or_const(((abb_table_old[0] >> 9) & 1) <<17)
     
    # Overidding programmed process code
    if (OPTIONS_params["Forced_code"] == 1) :
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(1 << 17), 0,((abb_table[OPTIONS_params["Code"]] >> 8) & 1) <<17)
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(1 << 17), 0,((abb_table[OPTIONS_params["Code"]] >> 9) & 1) <<17)
    if (OPTIONS_params["Forced_abb"] == 1) :
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(1 << 17), 0,((OPTIONS_params["ABB"] >> 8) & 1) <<17)
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(1 << 17), 0,((OPTIONS_params["ABB"] >> 9) & 1) <<17)
        
    # wait 1ms
    sttp.stmc.delay(1000)
    
    # step 3 Update VBBN/P_VAL values
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 0 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[1] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[1] >> 4 & 0xf))
    else:
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 0 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[1] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[1] >> 4 & 0xf))
        else:
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[0] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[0] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 1 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[2] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[2] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 2 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[3] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[3] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 3 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[4] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[4] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 4 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[5] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[5] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 5 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[6] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[6] >> 4 & 0xf))
        if sttp.pokepeek.if_gt(Process_info_reg_old, 0xf << Process_info_start_old, 6 << Process_info_start_old):
            corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table_old[7] & 0xf))
            corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table_old[7] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 1 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[2] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[2] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 2 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[3] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[3] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 3 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[4] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[4] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 4 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[5] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[5] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 5 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[6] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[6] >> 4 & 0xf))
    if sttp.pokepeek.if_gt(Process_info_reg, 0xf << Process_info_start, 6 << Process_info_start):
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[7] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[7] >> 4 & 0xf))
        
     # Overidding programmed process code
    if (OPTIONS_params["Forced_code"] == 1) :
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (abb_table[OPTIONS_params["Code"]] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (abb_table[OPTIONS_params["Code"]] >> 4 & 0xf))
    if (OPTIONS_params["Forced_abb"] == 1) :
        corecfg.SYSTEM_CONFIG5116.read_modify_write(~(0xf), 0, (OPTIONS_params["ABB"] & 0xf))
        corecfg.SYSTEM_CONFIG5117.read_modify_write(~(0xf), 0, (OPTIONS_params["ABB"] >> 4 & 0xf))
   
    cp_clk = 1 # 1 interneal , 0 external
    cp_val = 15
    cp_compens = 3
    cp_en = 1
    
    # Charge pump needed for NMOS RBB
    if (corecfg.SYSTEM_CONFIG5117.if_eq( 1 << 3, 1 << 3  )): 
        corecfg.SYSTEM_CONFIG5117.read_modify_write(0xffffffff, 0, (cp_clk  << 29 | cp_val << 24 | cp_compens << 20 | cp_en << 28 )  )
    
    # step 4, toggle value update
    corecfg.SYSTEM_CONFIG5116.read_modify_write(~(1<<16), 0, 1<<16)
    corecfg.SYSTEM_CONFIG5117.read_modify_write(~(1<<16), 0, 1<<16)
    
    # wait 1ms
    sttp.stmc.delay(1000)
    
    corecfg.SYSTEM_CONFIG5116.read_modify_write(~(1<<16), 0, 0<<16)
    corecfg.SYSTEM_CONFIG5117.read_modify_write(~(1<<16), 0, 0<<16)
    
    # step 5 Set subsystem input opp_data_is_stopped to 1, otherwise updating opp_data_bbsel when the CPU is off would lead to a deadlock.
    corecfg.SYSTEM_CONFIG5114.read_modify_write( 0xffffffff,0,1 << 28)
    
    # step 4
    corecfg.SYSTEM_CONFIG5114.poke(0x1480002)
    
    # wait 1ms
    sttp.stmc.delay(1000)
    
    # step 5
    if (corecfg.SYSTEM_CONFIG5116.if_eq( 1 << 17, 1 << 17)):
        #Put the BBGen in ON mode, i.e. vbb*_enable = 1, with vbb*_bypass_n = opp_data_bbsel[1:0] = 11
        if (corecfg.SYSTEM_CONFIG5116.if_eq( 1 << 17, 1 << 17)):
            corecfg.SYSTEM_CONFIG5114.read_modify_write( 0xffffffff,0,0x1 << 4)
        if (corecfg.SYSTEM_CONFIG5117.if_eq( 1 << 17, 1 << 17)):
            corecfg.SYSTEM_CONFIG5114.read_modify_write( 0xffffffff,0,0x1 << 5)
        # wait 1ms
        sttp.stmc.delay(1000)
        corecfg.SYSTEM_CONFIG5115.poke(0x1)
        # step 6
        corecfg.SYSTEM_CONFIG5545.while_and_ne(0x1,0x1)

    else:
        if (corecfg.SYSTEM_CONFIG5117.if_eq( 1 << 17, 1 << 17)):
            #Put the BBGen in ON mode, i.e. vbb*_enable = 1, with vbb*_bypass_n = opp_data_bbsel[1:0] = 11
            if (corecfg.SYSTEM_CONFIG5116.if_eq( 1 << 17, 1 << 17)):
                corecfg.SYSTEM_CONFIG5114.read_modify_write( 0xffffffff,0,0x1 << 4)
            if (corecfg.SYSTEM_CONFIG5117.if_eq( 1 << 17, 1 << 17)):
                corecfg.SYSTEM_CONFIG5114.read_modify_write( 0xffffffff,0,0x1 << 5)
            # wait 100ms
            sttp.stmc.delay(1000)
            corecfg.SYSTEM_CONFIG5115.poke(0x1)
            # step 6
            corecfg.SYSTEM_CONFIG5545.while_and_ne(0x1,0x1)       
   
    # Display info  
    sttp.logging.print_out("   %s ABB configuration:" % (OPTIONS_params["Partition"]))   
    if (OPTIONS_params["Partition"] == "A9"):
        sttp.logging.print_out("\t Optimized for %d MHz" %(ca9_freq))
    else:
        raise sttp.STTPException("The Body Biasing is not supported on this partition.\n")
    
    abb = 0  
    if (sttp.stmc.get_operating_mode() != "ROMGEN"):
        #Read process information from non volatile memory
        process = (sttp.pokepeek.peek(Process_info_reg) >> Process_info_start) & 0xf
        process_old = (sttp.pokepeek.peek(Process_info_reg_old) >> Process_info_start_old) & 0xf
        abb = abb_table[process]
        sttp.logging.print_out("\t Process code (old location): %d" % (process_old))
        sttp.logging.print_out("\t Process code (new location): %d" % (process))
        if (process == 0):
            abb = abb_table_old[process_old]
                   
          
    if (OPTIONS_params["Forced_code"] == 1):
        sttp.logging.print_out("\t Process code forced to %d" % (OPTIONS_params["Code"]))
        abb = abb_table[OPTIONS_params["Code"]]
    if (OPTIONS_params["Forced_abb"] == 1):
        sttp.logging.print_out("\t ABB forced to 0x%x " % (OPTIONS_params["ABB"]))
        abb = OPTIONS_params["ABB"]
        
    if (sttp.stmc.get_operating_mode() != "ROMGEN"):
        if ((abb >> 8) & 1):
           Pabb = BB_mode[(abb >> 3) & 1] + " "  + BB_level[(abb >> 0) & 7]
        else:
           Pabb = "OFF" 
        sttp.logging.print_out("\t VBB P configuration:  %s" %(Pabb))
        
        if ((abb >> 9) & 1):
           Nabb = BB_mode[(abb >> 7) & 1] + " "  + BB_level[(abb >> 4) & 7]
        else:
           Nabb = "OFF"
        sttp.logging.print_out("\t VBB N configuration:  %s" %(Nabb))
        
    if ((sttp.stmc.get_operating_mode() != "ROMGEN") and OPTIONS_params["Debug"] > 0): 
        sttp.logging.print_out("\t ABB tables  (old process)")
        sttp.logging.print_out("\t    Code \t: %3d %3d %3d %3d %3d %3d %3d %3d" %(0,1,2,3,4,5,6,7))
        sttp.logging.print_out("\t    ABB (hex)\t: %3x %3x %3x %3x %3x %3x %3x %3x" %(abb_table_old[0],abb_table_old[1],abb_table_old[2],abb_table_old[3],abb_table_old[4],abb_table_old[5],abb_table_old[6],abb_table_old[7]))
        sttp.logging.print_out("\t ABB tables  (new process)")
        sttp.logging.print_out("\t    Code \t: %3d %3d %3d %3d %3d %3d %3d %3d" %(0,1,2,3,4,5,6,7))
        sttp.logging.print_out("\t    ABB (hex)\t: %3x %3x %3x %3x %3x %3x %3x %3x" %(abb_table[0],abb_table[1],abb_table[2],abb_table[3],abb_table[4],abb_table[5],abb_table[6],abb_table[7]))
       
        vbbn_cp_ok = (corecfg.SYSTEM_CONFIG5546.peek() >> 2) & 0x1
        vbbn_ok = (corecfg.SYSTEM_CONFIG5546.peek() >> 1) & 0x1
        vbbp_ok = corecfg.SYSTEM_CONFIG5546.peek() & 0x1
        sttp.logging.print_out("\t VBB status:")
        sttp.logging.print_out("\t    VBB N_CP_OK=%d " %(vbbn_cp_ok))
        sttp.logging.print_out("\t    VBB N_OK=%d  " %(vbbn_ok))
        sttp.logging.print_out("\t    VBBP_OK=%d" %(vbbp_ok))
        
    if ((sttp.stmc.get_operating_mode() != "ROMGEN") and OPTIONS_params["Debug"] == 2): 
        print_param(parameters, "Global")
        print_param(OPTIONS_params, "AVS hardware")
        print_param(OPTIONS_params, "AVS & ABB Options")
           
    return
    
def print_param(params, name):
    sttp.logging.print_out("\n**********************************************************")
    sttp.logging.print_out(" %s parameters" %name)
    for key,value in params.items():
        print "%s = %s" %(key, value)
    return
    
def enable_avs():
    # Set pwm_val frequency to its maximum
    sttp.pokepeek.read_modify_write(SBC_PWM_CTRL, SBC_PWM_CTRL, 0xFFF803F0, 0, 0 )  
    # pwm_val enable
    sttp.pokepeek.or_const(SBC_PWM_CTRL, SBC_PWM_CTRL, 1<< 9) 
    return    
    
    
def AVS_ABB_debug(parameters):
    sttp.logging.print_out("\n**********************************************************\n")
    sttp.logging.print_out("AVS and ABB configuration details")
    ca9_freq = int(parameters["ca9_freq"])
    lmi_freq = int(parameters["lmi_freq"])
    
    sttp.logging.print_out("\t    LMI data rate (Mbps)\t: %d"  %(lmi_freq))
    sttp.logging.print_out("\t    A9 frequency  (Mhz) \t: %d" %(ca9_freq))

    sttp.logging.print_out("\t SBC PWM")
    sttp.logging.print_out("\t    SBC_PWM_CTRL\t: 0x%x" % (sttp.pokepeek.peek(SBC_PWM_CTRL)))
    sttp.logging.print_out("\t    SBC_PWM10_VAL\t: 0x%x" % (sttp.pokepeek.peek(SBC_PWM10_VAL)))
    sttp.logging.print_out("\t    SBC_PWM11_VAL\t: 0x%x" % (sttp.pokepeek.peek(SBC_PWM11_VAL)))
    sttp.logging.print_out("\t    SBC_PWM13_VAL\t: 0x%x" % (sttp.pokepeek.peek(SBC_PWM13_VAL)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG3\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG3)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG4\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG4)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG40\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG40)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG41\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG41)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG50\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG50)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG51\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG51)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG60\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG60)))
    sttp.logging.print_out("\t    SYSTEM_CONFIG61\t: 0x%x" % (sttp.pokepeek.peek(SBC_SYSTEM_CONFIG61)))
    sttp.logging.print_out("\t ABB")
    sttp.logging.print_out("\t    SYSTEM_CONFIG5114\t: 0x%x" % (corecfg.SYSTEM_CONFIG5114.peek()))
    sttp.logging.print_out("\t    SYSTEM_CONFIG5115\t: 0x%x" % (corecfg.SYSTEM_CONFIG5115.peek()))
    sttp.logging.print_out("\t    SYSTEM_CONFIG5116\t: 0x%x" % (corecfg.SYSTEM_CONFIG5116.peek()))
    sttp.logging.print_out("\t    SYSTEM_CONFIG5117\t: 0x%x" % (corecfg.SYSTEM_CONFIG5117.peek()))
    sttp.logging.print_out("\t    SYSTEM_CONFIG5545\t: 0x%x" % (corecfg.SYSTEM_CONFIG5545.peek()))
    sttp.logging.print_out("\t    SYSTEM_CONFIG5546\t: 0x%x" % (corecfg.SYSTEM_CONFIG5546.peek()))
    sttp.logging.print_out("\n**********************************************************\n")
        
       

def OPTIONS_struct_def(partition,parameters):
    OPTIONS_params = {
        "Partition"                    : partition,
        "Forced_voltage"          : 0,
        "Voltage"                    : 0,
        "Forced_code"             : 0,
        "Code"                       : 0,
        "Forced_abb"              : 0,
        "ABB"                        : 0,
        "Debug"                    : 0,
        "A"                           : 0,
        "B"                           : 0,
        "Vmin"                      : 0,
        "Vmax"                     : 0,
        "PWM"                      : 0,
        "avs_lmi_2133"           : core_lmi_2133,
        "avs_lmi_1866"           : core_lmi_1866,
        "avs_lmi_1600"           : core_lmi_1600,
        "avs_a9_1500"           : cpu_1500,
        "avs_a9_1200"           : cpu_1200,
        "abb_a9_1500"           : abb_1500,
        "abb_a9_1200"           : abb_1200,
        "avs_lmi_2133_old"      : core_lmi_2133_old,
        "avs_lmi_1866_old"      : core_lmi_1866_old,
        "avs_lmi_1600_old"      : core_lmi_1600_old,
        "avs_a9_1500_old"      : cpu_1500_old,
        "avs_a9_1200_old"      : cpu_1200_old,
        "abb_a9_1500_old"      : abb_1500_old,
        "abb_a9_1200_old"      : abb_1200_old
        
    }

    
    if (OPTIONS_params["Partition"] == "Core"):
        if parameters.has_key("avs_core_code"):
            OPTIONS_params["Forced_code"]  = 1
            OPTIONS_params["Code"] = int(parameters["avs_core_code"],0)
        if parameters.has_key("vcore"):
            OPTIONS_params["Forced_voltage"]  = 1
            OPTIONS_params["Voltage"] =  int(parameters["vcore"], 0)    
    else:
        if (OPTIONS_params["Partition"] == "A9"):
            if parameters.has_key("avs_cpu_code"):
                OPTIONS_params["Forced_code"]  = 1
                OPTIONS_params["Code"] = int(parameters["avs_cpu_code"],0)
            if parameters.has_key("vcpu"):
                OPTIONS_params["Forced_voltage"]  = 1
                OPTIONS_params["Voltage"] =  int(parameters["vcpu"], 0)      
            if parameters.has_key("abb_cpu_value"):
                OPTIONS_params["Forced_abb"]  = 1
                OPTIONS_params["ABB"] = int(parameters["abb_cpu_value"],0)
        else:
            raise sttp.STTPException("Unknown partition\n")

            
    return OPTIONS_params
