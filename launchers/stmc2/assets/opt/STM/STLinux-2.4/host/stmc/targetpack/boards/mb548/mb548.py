# TargetScript for connecting and disconnecting to a mb548 (std1000)

import time

import sttp
import sttp.logging
import sttp.targetpack
import sttp.stmc
import sttp.targetinfo

# TargetPack imports
import std1000
import utilities

global sttp_root
sttp_root = sttp.targetpack.get_tree()

def mb548_configure(parameters):
    # Apply core address offset.
    connect_core = sttp.targetinfo.get_targetstring_core()
    # Set a core specific view of the registers for features such as Memory Mapped Register Set
    if connect_core == "st40":
        sttp.targetpack.apply_address_offset("st40_offset")

def mb548eval_connect(parameters):
    parameters["mb548eval"] = 1
    mb548_connect(parameters)


def mb548ssbe_connect(parameters):
    parameters["mb548ssbe"] = 1
    mb548_connect(parameters)


def mb548ssbu_connect(parameters):
    parameters["mb548ssbu"] = 1
    mb548_connect(parameters)


def mb548_connect(parameters):
    sttp.logging.print_out("mb548 connect start - parameters", parameters)

    # is there a user supplied module and funtion to call?
    utilities.import_user_module(parameters)

    if parameters.has_key("no_init") and (int(parameters["no_init"]) == 1):
        return

    # Before clockgen is set up, tck be "slow" (<1/3rd perihperal clock speed)
    utilities.set_tck_frequency(True, parameters)

    # Connect to std1000 SoC
    std1000.connect(parameters)

    if not (parameters.has_key("no_pokes") and (int(parameters["no_pokes"]) == 1)):
        # Set ST40 view of register addresses during connect.
        sttp.targetpack.apply_address_offset("st40_offset")
        sttp.pokepeek.enable()

        sttp.logging.print_out("mb548 initialization start ...")
        mb548_setup(parameters)

    utilities.set_tck_frequency(False, parameters)
    sttp.logging.print_out("mb548 initialization complete")

def mb548_disconnect():
    pass


# Configure the mb548
def mb548_setup(parameters):
    sttp.pokepeek.rom_delay_scale(27)
    if not (parameters.has_key("no_clockgen_config") and (int(parameters["no_clockgen_config"]) == 1)):
        mb548_clockgen_configure()
        sttp.pokepeek.rom_delay_scale(266)
    mb548_emi_configure()
    if (parameters.has_key("mb548eval") and (int(parameters["mb548eval"]) == 1)) or \
       (parameters.has_key("eval") and (int(parameters["eval"]) == 1)):
        sttp.logging.print_out("mb548: mb548eval lmi configuration in use")
        mb548eval_lmi_configure()
    elif (parameters.has_key("mb548ssbe") and (int(parameters["mb548ssbe"]) == 1)) or \
         (parameters.has_key("ssbe") and (int(parameters["ssbe"]) == 1)):
        sttp.logging.print_out("mb548: mb548ssbe lmi configuration in use")
        mb548ssbe_lmi_configure()
    elif (parameters.has_key("mb548ssbu") and (int(parameters["mb548ssbu"]) == 1)) or \
         (parameters.has_key("ssbu") and (int(parameters["ssbu"]) == 1)):
        sttp.logging.print_out("mb548: mb548ssbu lmi configuration in use")
        mb548ssbu_lmi_configure()
    else:
        mb548_lmi_configure()

    if "ROMGEN" != sttp.stmc.get_operating_mode():
        ccn = sttp_root.std1000.st40.CCN.st40_ccn_regs
        # We switch to write-through mode here to match what the GDB script does
        ccn.CCN_CCR.poke(0x8000090b)


# mb548 CLOCKGEN Configuration
def mb548_clockgen_configure():
    global sttp_root
    ckg = sttp_root.std1000.CKG.std1000_ckg_regs
    genconf = sttp_root.std1000.GEN_CONF.std1000_gen_conf_regs
    std1000.std1000_set_clockgen(ckg, genconf)


# mb548 EMI Configuration
def mb548_emi_configure():
    emi = sttp_root.std1000.EMI.st40_emi_regs
    emi.EMI_BANK0_EMICONFIGDATA0.poke(0x04001691)
    emi.EMI_BANK0_EMICONFIGDATA1.poke(0x0a000000)
    emi.EMI_BANK0_EMICONFIGDATA2.poke(0x0e004400)
    emi.EMI_BANK0_EMICONFIGDATA3.poke(0x00000000)


# mb548 LMI & LMI mixer Configurations
def mb548_mixer_bypass_configure():
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    ## Define both memory and register base addresses
    mixer.LMIBADDR.poke(0x00000010 | 0x00190000)
    mixer.GENC.poke(0x00000001)


def mb548_mixer_standard_configure():
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    ## Reset low-pri source register file
    mixer.GENC.poke(0x00000001)
    ## - Clear low-pri source register file reset bit
    ## - Enable flow regulations (input+arbiter+bank)
    ## - Set burst boundary = 32B
    ## - Enable wp, store & forward, page interrupt
    ## - Enable interrupt
    mixer.GENC.poke(0x100b1710)

    ## - Unmask interrupts
    mixer.INTMASK.poke(0x00030003)

    ## - Bank bit 8, 9
    mixer.BANKC.poke(0x00000908)

    ## - Row mask : 0xffffff00
    mixer.ROWADDRMSK.poke(0xffffff00)

    ## Define both memory and register base addresses
    mixer.LMIBADDR.poke(0x00000010 | 0x00190000)

    # Added according to LLP (24/05/07)
    mixer.BDCRIS.poke(0x00301010)
    mixer.BDHPP.poke(0x00001010)

    # Changed according to LLP  (24/05/07)
    #mixer.BDLAT.poke(0x60604010)
    mixer.BDLAT.poke(0x60604030)
    mixer.BDQTHD.poke(0x00000022)
    mixer.BDLDINEF.poke(0x0210834f)
    mixer.BDSTINEF.poke(0x0210834f)

    ## - 0/1 fifo  : 64
    ## - 2   fifos : 32
    ## - 3   fifos : 16
    ## - 4   fifos : 10
    mixer.INFLWREG.poke(0x0a102040)

    ## - Set asolute mode
    ## - Enable fifo full mode
    ## - Low thresh hold  = 20
    ## - High thresh hold = 30
    mixer.ARBFLWREG.poke(0x1e140003)

    ## - Default setting
    mixer.BKFLWREG.poke(0x0e0c4032)

    ## High priority port flow regulation
    ##   frame size=0xc0, slot=0x02, time out=0x3f
    ## CPU bandwith limitation (but to be removed when cache write-back functional)
    # Changed according to LLP (24/05/07) Limitation equal to 128 MB/s with LD/ST32
   #mixer.HPPFLWREG.poke(0x003f02c0)
    mixer.HPPFLWREG.poke(0x003f013f)

    ## - pkt_mem     = 64
    ## - st_cell_mem = 128
    mixer.REQMEM.poke(0x00800040)

    ## - r_pkt_mem = 128
    mixer.RESMEM.poke(0x01280080)

    mixer.SPSRCREG_47.poke(0x0f0f070f)
    mixer.SPSRCREG_46.poke(0x0f0f0f0f)


def mb548_lmi_configure():
    lmi = sttp_root.std1000.LMI.st40_lmigp_regs
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    genconf = sttp_root.std1000.GEN_CONF.std1000_gen_conf_regs
    mb548_mixer_standard_configure()

    # Added according to LLP (24/05/07)
    # setup DDRP1 mixer's register which is adapted with the specific DDR
    mixer.DDRP1.poke(0x042043c4)

    ## LMI.MIM_L
    ##   1=dce (DRAM controller)=enable
    ##   1=dt (DRAM type)=DDR2
    ##   0=ret (Retiming stage)
    ##   1=comb16=disabled
    ##   1=comb8=disabled
    ##   1=dqs_recov (DQS recovery mechanism)=enabled
    ##   01=bw (Bus width)=32bits
    ##   0=reserved
    ##   0=dre (DRAM refresh)=disable
    ##   0=reserved
    ##   0=by32ap (MA8 pin)=NOT used for PRE/PALL
    ##   0000=reserved
    ##   xxxxxxxxxxxxx=dri (DRAM refresh interval)
    ##   000=reserved
    ##
    ## LMI.MIM_H
    ##   000=reserved
    ##   00000000=frame=0=inactive
    ##   00000=limit=?
    ##   00000000=load_timeout=inactive
    ##   00000000=store_timeout=inactive

    ## _ST_display (_procname) "SDRAM Mode Register"
    lmi.LMI_MIM_0.poke(0x07a1007b)
    lmi.LMI_MIM_1.poke(0x00000000)

    ## LMI.STR_L
    ##   xxx=trp (RAS precharge to ACT command)
    ##   xxx=trcdr (RAS to read CAS delay)
    ##   xxxx=trc (RAS cycle time)
    ##   xxxx=tras (RAS active time)
    ##   xx=trrd (RAS to RAS active delay)
    ##   011=twr (Write recovery time)=4 clks
    ##   0100=cl (CAS latency)=4 clks
    ##   10000=trfc (Auto refresh RAS cycle time)=28 clks
    ##   0=disable_twtr (Write to read interruption)=enable
    ##   01=twtr (Write to read interruption)=2 clks
    ##   0=tx1rwl (Read to write extra cycle)=disable
    ##
    ## LMI.STR_H
    ##   0=reserved
    ##   011=trcdw (RAS to write CAS delay)
    ##   xxxx=txsr (Exit self-refresh to next command)
    ##   xx=trtp (Read to precharge command delay)
    ##   xxxx=tfaw (Four bank active time)
    ##   000=al (Additive latency)=0 clks
    ##   000000000000000=reserved

    ## _ST_display (_procname) "SDRAM Timing Register"
    lmi.LMI_STR_0.poke(0x282b5edb)
    lmi.LMI_STR_1.poke(0x000101d6)

    ## LMI.SDRAx_L
    ##   0=enable_ba0 (Bank remapping for BA0)=disable
    ##   0=enable_ba1 (Bank remapping for BA1)=disable
    ##   0=enable_ba2 (Bank remapping for BA2)=disable
    ##   0=swp_add_13_10 (Swap between bit 13 and bit 10 of STBUS address)=disable
    ##   0=swp_add_13_11 (Swap between bit 13 and bit 11 of STBUS address)=disable
    ##   000=reserved
    ##   1010=split=13x10
    ##   0=bank (Bank number)=4
    ##   00000000=reserved
    ##   00011000000=uba (Array upper bound address)=0x18000000 (128 MBytes of DDR2 at base address 0x10000000)

    ## _ST_display (_procname) "SDRAM Row Attribute 0"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA0_0.poke(0x18000a00)
    lmi.LMI_SDRA0_0.poke(0x18000a03)

    ## _ST_display (_procname) "SDRAM Row Attribute 1"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA1_0.poke(0x18000a00)
    lmi.LMI_SDRA1_0.poke(0x18000a03)

    ## LMI.SCR_L
    ##   011=sms (Mode select)=clock enable
    ##   0=reserved
    ##   xxx=brfsh (Burst refresh)
    ##   000000000=reserved
    ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
    ##   0000=reserved
    ##
    ## LMI.SCR_H
    ##   00000000000000000000000000000000=reserved

    ## _ST_display (_procname) "SDRAM Control Register"

    ##------
    ## ->CKE
    ## ->NOP
    ##------

    lmi.LMI_SCR_0.poke(0x00010013)
    ##----------------------------------------------------
    ## 8 NOP : Minimum 400ns between CKE and precharge all
    ##----------------------------------------------------
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------------
    ## ->Precharge all
    ## ->NOP
    ##----------------

    lmi.LMI_SCR_0.poke(0x00010012)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------
    ## EMR2->0's
    ## EMR3->0's
    ##----------

    lmi.LMI_SDMR0_0.poke(0x00000800)
    lmi.LMI_SDMR0_0.poke(0x00000c00)

    ##----------------------------------------------
    ## EMRS: DLL enable
    ##   EMRS val=01 000 0 0 0 000 0 000 0 0 0
    ##   STBUS   =000 0 0 0 01 000 0 000 0 0 0=0x400
    ##----------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00000426)

    ##--------------------------------------------
    ## MRS: DLL reset
    ##   MRS dec=BA       WR      CL    BL
    ##   MRS val=00 000 0 011 1 0 100 0 011
    ##   STBUS  =000 0 01 001 1 0 100 0 011=0x1343
    ##--------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001353)

    ##------------------------------------------------
    ## 8 NOP : 200 clock cycles needed to lock the DLL
    ##------------------------------------------------
    std1000.std1000_lmi_scr_nop(lmi, 32)

    ##------------------------
    ## ->Precharge all
    ## ->2x CBR (auto refresh)
    ##------------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010014)
    lmi.LMI_SCR_0.poke(0x00010014)

    ##------------------------------------------
    ## MRS device initialisation
    ##   MRS dec=BA     WR      CL    BL
    ##   MRS val=00 0 0 011 0 0 100 0 011
    ##   STBUS  =0 0 01 001 0 0 100 0 011=0x1243
    ##------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001253)

    ## Should wait at least 200 clks after "step 8"

    ##----------------------------------------------
    ## OCD calibration to default
    ##   EMRS val=01 000 0 0 0 111 0 000 0 0 0
    ##   STBUS   =000 0 0 0 01 111 0 000 0 0 0=0x780
    ##----------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x000007a6)

    ##----------------------------------------
    ## OCD calibration mode exit
    ##   Desc    =BA      OCD   AL
    ##   EMRS val=001 000 000 1 000 0 00
    ##   STBUS   =000 001 000 1 000 0 00=0x440
    ##----------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00000426)

    ##-------------------
    ## ->2x Precharge all
    ##-------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010012)

    ## Enable refresh
    lmi.LMI_MIM_0.or_const(0x00000200)

    ## _ST_display (_procname) "LMI Delay Registers"

    ## Set output impedance of DDR pads
    genconf.IO_DDR2_CONF.poke(0x00000142)

    ## Bypass PDL5 in dqs_en generation
    genconf.LMI_PL_CTL.poke(0x1001d002)

    ## Delay setting
    ## genconf.LMI_PDL_0_3_MSB_CTL.poke(0x0a0a0a0a)
    genconf.LMI_PDL_0_3_MSB_CTL.poke(0x06010105)


def mb548eval_lmi_configure():
    lmi = sttp_root.std1000.LMI.st40_lmigp_regs
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    genconf = sttp_root.std1000.GEN_CONF.std1000_gen_conf_regs
    #modif JB according to mb548.cmd
    #mb548_mixer_standard_configure()
    mb548_mixer_bypass_configure()


    # Added according to LLP (24/05/07)
    # setup DDRP1 mixer's register which is adapted with the specific DDR
    mixer.DDRP1.poke(0x042043c4)

    ## LMI.MIM_L
    ##   1=dce (DRAM controller)=enable
    ##   1=dt (DRAM type)=DDR2
    ##   0=ret (Retiming stage)
    ##   1=comb16=disabled
    ##   1=comb8=disabled
    ##   1=dqs_recov (DQS recovery mechanism)=enabled
    ##   01=bw (Bus width)=32bits
    ##   0=reserved
    ##   0=dre (DRAM refresh)=disable
    ##   0=reserved
    ##   0=by32ap (MA8 pin)=NOT used for PRE/PALL
    ##   0000=reserved
    ##   xxxxxxxxxxxxx=dri (DRAM refresh interval)
    ##   000=reserved
    ##
    ## LMI.MIM_H
    ##   000=reserved
    ##   00000000=frame=0=inactive
    ##   00000=limit=?
    ##   00000000=load_timeout=inactive
    ##   00000000=store_timeout=inactive

    ## _ST_display (_procname) "SDRAM Mode Register"
    lmi.LMI_MIM_0.poke(0x07a1007b)
    lmi.LMI_MIM_1.poke(0x00000000)

    ## LMI.STR_L
    ##   xxx=trp (RAS precharge to ACT command)
    ##   xxx=trcdr (RAS to read CAS delay)
    ##   xxxx=trc (RAS cycle time)
    ##   xxxx=tras (RAS active time)
    ##   xx=trrd (RAS to RAS active delay)
    ##   xxx=twr (Write recovery time)=4 clks
    ##   xxxx=cl (CAS latency)=4 clks
    ##   xxxxx=trfc (Auto refresh RAS cycle time)=28 clks
    ##   x=disable_twtr (Write to read interruption)=enable
    ##   xx=twtr (Write to read interruption)=2 clks
    ##   x=tx1rwl (Read to write extra cycle)=disable
    ##
    ## LMI.STR_H
    ##   0=reserved
    ##   xxx=trcdw (RAS to write CAS delay)
    ##   xxxx=txsr (Exit self-refresh to next command)
    ##   xx=trtp (Read to precharge command delay)
    ##   xxxx=tfaw (Four bank active time)
    ##   xxx=al (Additive latency)=0 clks
    ##   000000000000000=reserved

    ## Timing parameters for HY5PS121821BFP-C4 (1 clock cycle = 4ns @ 250MHz)

    ## LMI.STR_L
    ##  trp=20ns (min 15ns)
    ##  trcdr=20ns (min 15ns)
    ##  trc=68ns (min 60ns)
    ##  tras=48ns (min 45ns)
    ##  trrd=12ns (min 10ns)
    ##  twr=16ns (min 15ns)
    ##  cl=4 clks
    ##  trfc=112ns (min 105ns)=25 clks
    ##  disable_twtr=0
    ##  twtr=8ns (min 7.5ns)
    ##  tx1rwl=0
    ## LMI.STR_H
    ##  trcd=20ns (min 15ns)
    ##  txsr=68ns (min 200ns)
    ##  trtp=8ns (min 7.5ns)
    ##  tfawc=52ns (min 50ns)
    ##  al=4 clks

    ## _ST_display (_procname) "SDRAM Timing Register"
    lmi.LMI_STR_0.poke((((20 / 4) - 2) << 0) |
                      (((20 / 4) - 2) << 3) |
                      (((68 / 4) - 6) << 6) |
                      (((48 / 4) - 5) << 10) |
                      (((12 / 4) - 2) << 14) |
                      (((16 / 4) - 1) << 16) |
                      (4 << 19) |
                      (16 << 23) |
                      (0 << 28) |
                      (((8 / 4) - 1) << 29) |
                      (0 << 31))
    lmi.LMI_STR_1.poke((((20 / 4) - 2) << 1) |
                      ((68 / 4) << 4) |
                      (((8 / 4) - 1) << 8) |
                      (((52 / 4) - 7) << 10) |
                      (4 << 14))

    ## LMI.SDRAx_L
    ##   0=enable_ba0 (Bank remapping for BA0)=disable
    ##   0=enable_ba1 (Bank remapping for BA1)=disable
    ##   0=enable_ba2 (Bank remapping for BA2)=disable
    ##   0=swp_add_13_10 (Swap between bit 13 and bit 10 of STBUS address)=disable
    ##   0=swp_add_13_11 (Swap between bit 13 and bit 11 of STBUS address)=disable
    ##   000=reserved
    ##   1010=split=13x10
    ##   0=bank (Bank number)=4
    ##   00000000=reserved
    ##   00011000000=uba (Array upper bound address)=0x18000000 (128 MBytes of DDR2 at base address 0x10000000)

    ## _ST_display (_procname) "SDRAM Row Attribute 0"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA0_0.poke(0x18000a00)
    lmi.LMI_SDRA0_0.poke(0x18000a03)

    ## _ST_display (_procname) "SDRAM Row Attribute 1"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA1_0.poke(0x18000a00)
    lmi.LMI_SDRA1_0.poke(0x18000a03)

    ## LMI.SCR_L
    ##   011=sms (Mode select)=clock enable
    ##   0=reserved
    ##   xxx=brfsh (Burst refresh)
    ##   000000000=reserved
    ##   xxxxxxxxxxxx=cst (Clock stabilisation time)
    ##   0000=reserved
    ##
    ## LMI.SCR_H
    ##   00000000000000000000000000000000=reserved

    ## _ST_display (_procname) "SDRAM Control Register"

    ##------
    ## ->CKE
    ## ->NOP
    ##------

    lmi.LMI_SCR_0.poke(0x00010013)
    ##----------------------------------------------------
    ## 8 NOP : Minimum 400ns between CKE and precharge all
    ##----------------------------------------------------
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------------
    ## ->Precharge all
    ## ->NOP
    ##----------------

    lmi.LMI_SCR_0.poke(0x00010012)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------
    ## EMR2->0's
    ## EMR3->0's
    ##----------

    lmi.LMI_SDMR0_0.poke(0x00000800)
    lmi.LMI_SDMR0_0.poke(0x00000c00)

    ##------------------------------------------------------
    ## EMRS: DLL enable + ODT enabled 75ohms + DQSN disabled
    ##   EMRS val=01 000 0 0 0 000 0 000 0 0 0
    ##   STBUS   =000 0 0 0 01 000 0 000 0 0 0=0x400
    ##------------------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001426)

    ##--------------------------------------------
    ## MRS: DLL reset
    ##   MRS dec=BA       WR      CL    BL
    ##   MRS val=00 000 0 011 1 0 100 0 011
    ##   STBUS  =000 0 01 001 1 0 100 0 011=0x1343
    ##--------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001343)

    ##------------------------------------------------
    ## 8 NOP : 200 clock cycles needed to lock the DLL
    ##------------------------------------------------
    std1000.std1000_lmi_scr_nop(lmi, 32)

    ##------------------------
    ## ->Precharge all
    ## ->2x CBR (auto refresh)
    ##------------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010014)
    lmi.LMI_SCR_0.poke(0x00010014)

    ##------------------------------------------
    ## MRS device initialisation
    ##   MRS dec=BA     WR      CL    BL
    ##   MRS val=00 0 0 011 0 0 100 0 011
    ##   STBUS  =0 0 01 001 0 0 100 0 011=0x1243
    ##------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001243)

    ## Should wait at least 200 clks after "step 8"

    ##----------------------------------------------
    ## OCD calibration to default
    ##   EMRS val=01 000 0 0 0 111 0 000 0 0 0
    ##   STBUS   =000 0 0 0 01 111 0 000 0 0 0=0x780
    ##----------------------------------------------

    lmi.LMI_SDMR0_0.poke(0x000007a6)

    ##----------------------------------------
    ## OCD calibration mode exit
    ##   Desc    =BA      OCD   AL
    ##   EMRS val=001 000 000 1 000 0 00
    ##   STBUS   =000 001 000 1 000 0 00=0x440
    ##----------------------------------------

    lmi.LMI_SDMR0_0.poke(0x00001426)

    ##-------------------
    ## ->2x Precharge all
    ##-------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010012)

    ## Enable refresh
    lmi.LMI_MIM_0.or_const(0x00000200)

    ## _ST_display (_procname) "LMI Delay Registers"

    ## Bypass PDL5 in dqs_en generation
    genconf.LMI_PL_CTL.poke(0x1001d002)

    ## Delay setting
    genconf.LMI_PDL_0_3_MSB_CTL.poke(0x120f0f10)
    genconf.LMI_PDL_0_3_LSB_CTL.poke(0x00000000)

    ## Use external VREF reference voltage
    genconf.IO_DDR2_CONF.or_const(0x00000010)

    ## Set output impedance of DDR pads
    genconf.IO_DDR2_CONF.and_const(~0x00000020)


def mb548ssbe_lmi_configure():
    lmi = sttp_root.std1000.LMI.st40_lmigp_regs
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    genconf = sttp_root.std1000.GEN_CONF.std1000_gen_conf_regs
    mb548_mixer_standard_configure()

    # Added according to LLP (24/05/07)
    # setup DDRP1 mixer's register which is adapted with the specific DDR
    mixer.DDRP1.poke(0x042453c4)

    ## _ST_display (_procname) "SDRAM Mode Register"
    lmi.LMI_MIM_0.poke(0x07a1007b)
    lmi.LMI_MIM_1.poke(0x00000000)

    ## _ST_display (_procname) "SDRAM Timing Register"
    lmi.LMI_STR_0.poke(0x46235e9b)
    lmi.LMI_STR_1.poke(0x000102d6)

    ## _ST_display (_procname) "SDRAM Row Attribute 0"
    # Changed acccording to LLP (24/05/07)
   #lmi.LMI_SDRA0_0.poke(0x14000900)
    lmi.LMI_SDRA0_0.poke(0x14000903)

    ## _ST_display (_procname) "SDRAM Row Attribute 1"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA1_0.poke(0x14000900)
    lmi.LMI_SDRA1_0.poke(0x14000903)

    ## _ST_display (_procname) "SDRAM Control Register"

    ##------
    ## ->CKE
    ## ->NOP
    ##------

    lmi.LMI_SCR_0.poke(0x00010013)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------------
    ## ->Precharge all
    ## ->NOP
    ##----------------

    lmi.LMI_SCR_0.poke(0x00010012)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------
    ## EMR2->0's
    ## EMR3->0's
    ##----------

    lmi.LMI_SDMR0_0.poke(0x00000800)
    lmi.LMI_SDMR0_0.poke(0x00000c00)

    lmi.LMI_SDMR0_0.poke(0x00000426)
    lmi.LMI_SDMR0_0.poke(0x00001343)

    ##------------------------------------------------
    ## 8 NOP : 200 clock cycles needed to lock the DLL
    ##------------------------------------------------

    std1000.std1000_lmi_scr_nop(lmi, 32)

    ##------------------------
    ## ->Precharge all
    ## ->2x CBR (auto refresh)
    ##------------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010014)
    lmi.LMI_SCR_0.poke(0x00010014)

    lmi.LMI_SDMR0_0.poke(0x00001243)
    lmi.LMI_SDMR0_0.poke(0x000007a6)
    lmi.LMI_SDMR0_0.poke(0x00000426)

    ##-------------------
    ## ->2x Precharge all
    ##-------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010012)

    ## Enable refresh
    lmi.LMI_MIM_0.or_const(0x00000200)

    ## _ST_display (_procname) "LMI Delay Registers"

    ## Set output impedance of DDR pads
    genconf.IO_DDR2_CONF.poke(0x00000142)

    ## Bypass PDL5 in dqs_en generation
    genconf.LMI_PL_CTL.poke(0x1001d002)

    ## Delay setting
    genconf.LMI_PDL_0_3_MSB_CTL.poke(0x0a0a0a0a)


def mb548ssbu_lmi_configure():
    lmi = sttp_root.std1000.LMI.st40_lmigp_regs
    mixer = sttp_root.std1000.MIXER.std1000_mixer_regs
    genconf = sttp_root.std1000.GEN_CONF.std1000_gen_conf_regs
    mb548_mixer_standard_configure()

    # Added according to LLP (24/05/07)
    # setup DDRP1 mixer's register which is adapted with the specific DDR
    # mixer.DDRP1.poke(0x04204384)
    #JLL
    mixer.DDRP1.poke(0x042043c4)

    ## _ST_display (_procname) "SDRAM Mode Register"
    lmi.LMI_MIM_0.poke(0x07a1007b)
    lmi.LMI_MIM_1.poke(0x00000000)

    ## _ST_display (_procname) "SDRAM Timing Register"
    ## lmi.LMI_STR_0.poke(0x26235612)
    ## lmi.LMI_STR_1.poke(0x0000c1d4)
    ##JLL
    lmi.LMI_STR_0.poke(0x282b5edb)
    lmi.LMI_STR_1.poke(0x000101d6)

    ## _ST_display (_procname) "SDRAM Row Attribute 0"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA0_0.poke(0x14000900)
    lmi.LMI_SDRA0_0.poke(0x14000903)

    ## _ST_display (_procname) "SDRAM Row Attribute 1"
    # Changed according to LLP (24/05/07)
   #lmi.LMI_SDRA1_0.poke(0x14000900)
    lmi.LMI_SDRA1_0.poke(0x14000903)

    ## _ST_display (_procname) "SDRAM Control Register"

    ##------
    ## ->CKE
    ## ->NOP
    ##------

    lmi.LMI_SCR_0.poke(0x00010013)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------------
    ## ->Precharge all
    ## ->NOP
    ##----------------

    lmi.LMI_SCR_0.poke(0x00010012)
    std1000.std1000_lmi_scr_nop(lmi, 16)

    ##----------
    ## EMR2->0's
    ## EMR3->0's
    ##----------

    lmi.LMI_SDMR0_0.poke(0x00000800)
    lmi.LMI_SDMR0_0.poke(0x00000c00)

    ##lmi.LMI_SDMR0_0.poke(0x0000041c)
    ##lmi.LMI_SDMR0_0.poke(0x00001343)
    ##JLL
    lmi.LMI_SDMR0_0.poke(0x00000426)
    lmi.LMI_SDMR0_0.poke(0x00001353)

    ##------------------------------------------------
    ## 8 NOP : 200 clock cycles needed to lock the DLL
    ##------------------------------------------------

    std1000.std1000_lmi_scr_nop(lmi, 32)

    ##------------------------
    ## ->Precharge all
    ## ->2x CBR (auto refresh)
    ##------------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010014)
    lmi.LMI_SCR_0.poke(0x00010014)

    ##lmi.LMI_SDMR0_0.poke(0x00001243)
    ##lmi.LMI_SDMR0_0.poke(0x00000798)
    ##lmi.LMI_SDMR0_0.poke(0x0000041c)
    ##JLL
    lmi.LMI_SDMR0_0.poke(0x00001253)
    lmi.LMI_SDMR0_0.poke(0x000007a6)
    lmi.LMI_SDMR0_0.poke(0x00000426)

    ##-------------------
    ## ->2x Precharge all
    ##-------------------

    lmi.LMI_SCR_0.poke(0x00010012)
    lmi.LMI_SCR_0.poke(0x00010012)

    ## Enable refresh
    lmi.LMI_MIM_0.or_const(0x00000200)

    ## _ST_display (_procname) "LMI Delay Registers"

    ## Set output impedance of DDR pads
    genconf.IO_DDR2_CONF.poke(0x00000142)

    ## Bypass PDL5 in dqs_en generation
    genconf.LMI_PL_CTL.poke(0x1001d002)

    ## Delay setting
    ##genconf.LMI_PDL_0_3_MSB_CTL.poke(0x15151515)
    genconf.LMI_PDL_0_3_MSB_CTL.poke(0x06010105)
