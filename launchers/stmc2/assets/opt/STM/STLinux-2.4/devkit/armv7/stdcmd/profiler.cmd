set $profiler_enabled = 0
keep-variable $profiler_enabled

define enable_profiler
  if (!$profiler_enabled)
    installplugin profiler libarmprofiler.so
    aliasplugin profiler profiler
    set $profiler_enabled = 1
  else
    printf "Profiler plugin already enabled\n"
  end
end

document enable_profiler
Enable the profiler plugin
Usage: enable_profiler
end
