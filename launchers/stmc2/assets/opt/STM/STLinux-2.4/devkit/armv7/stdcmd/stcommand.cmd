set $stcommand_enabled = 0
keep-variable $stcommand_enabled

define enable_stcommand
  if (!$stcommand_enabled)
    installplugin stcommand libarmstcommand.so
    aliasplugin stcommand st
    set $stcommand_enabled = 1
  else
    printf "ST command plugin already enabled\n"
  end
end

document enable_stcommand
Enable the stcommand plugin
Usage: enable_stcommand
end
