################################################################################

define _ST_display
  init-if-undefined $_ST_display = 0
  keep-variable $_ST_display

  if ($argc < 2)
    help _ST_display
  else
    if ($arg0 < $_ST_display)
      if ($argc == 2)
        printf $arg1
      end
      if ($argc == 3)
        printf $arg1, $arg2
      end
      if ($argc == 4)
        printf $arg1, $arg2, $arg3
      end
      if ($argc == 5)
        printf $arg1, $arg2, $arg3, $arg4
      end
      if ($argc == 6)
        printf $arg1, $arg2, $arg3, $arg4, $arg5
      end
      if ($argc == 7)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6
      end
      if ($argc == 8)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7
      end
      if ($argc == 9)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8
      end
      if ($argc == 10)
        printf $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9
      end
      printf "\n"
    end
  end
end

document _ST_display
Display a message depending on the trace level (default: 0)
Usage: _ST_display <level> <format> [<argument>]*
end

################################################################################

define posixconsole
  if ($arg0)
    console on
  else
    console off
  end
end

document posixconsole
Enable or disable the creation of a separate POSIX console (default: 1)
Usage: posixconsole <mode>
where <mode> may be 0 (off) or 1 (on)
end

################################################################################

define stmcconfigure
  monitor STM-Configure:$arg0
end

document stmcconfigure
Specify an ST Micro Connect configuration command
Usage: stmcconfigure <option>[=<value>]
end

################################################################################

define stmcmsglevel
  stmcconfigure msglevel=$arg0
end

document stmcmsglevel
Set the ST Micro Connect message level (default: none)
Usage: stmcmsglevel <level>
where <level> may be all, alllvl, warning, info, debug or none
end

################################################################################

define ondisconnect
  stmcconfigure ondisconnect=$arg0
end

document ondisconnect
Set the action on target disconnect (default: none)
Usage: ondisconnect <action>
where <action> may be none, reset or restart
end

################################################################################
