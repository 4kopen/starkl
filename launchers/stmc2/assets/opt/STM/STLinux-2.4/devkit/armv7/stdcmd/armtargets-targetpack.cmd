################################################################################

define armtpbe
  source armconnect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectarmtpbe $arg0 $arg1
  else
    connectarmtpbe $arg0 ""
  end
end

document armtpbe
Connect to an ARM target (big endian)
Usage: armtpbe <target>
where <target> is an ST Micro Connect target string
end

define armtple
  source armconnect.cmd
  source plugins.cmd

  if ($argc > 1)
    connectarmtple $arg0 $arg1
  else
    connectarmtple $arg0 ""
  end
end

document armtple
Connect to an ARM target (little endian)
Usage: armtple <target>
where <target> is an ST Micro Connect target string
end

define armtp
  if ($argc > 1)
    armtple $arg0 $arg1
  else
    armtple $arg0
  end
end

document armtp
Connect to an ARM target
Usage: armtp <target>
where <target> is an ST Micro Connect target string
end

################################################################################
