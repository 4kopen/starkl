set $targetpack_enabled = 0
keep-variable $targetpack_enabled

define enable_targetpack
  if (!$targetpack_enabled)
    installplugin targetpack libarmtargetpack.so libstmc1.so
    aliasplugin targetpack targetpack
    set $targetpack_enabled = 1
  else
    printf "TargetPack plugin already enabled\n"
  end
end

document enable_targetpack
Enable the TargetPack plugin
Usage: enable_targetpack
end
