################################################################################

source profiler.cmd
source stcommand.cmd
source targetpack.cmd

################################################################################

define enable_all_plugins
  if (!$profiler_enabled)
    enable_profiler
  end

  if (!$stcommand_enabled)
    enable_stcommand
  end
end

document enable_all_plugins
Enable all the silicon target plugins
Usage: enable_all_plugins
end

################################################################################

define _init_all_plugins
  init-if-undefined $_enable_all_plugins = 1
  keep-variable $_enable_all_plugins

  if ($_enable_all_plugins)
    enable_all_plugins
  end
end

################################################################################
