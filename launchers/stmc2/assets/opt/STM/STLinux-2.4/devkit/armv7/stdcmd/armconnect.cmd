################################################################################
# ARM silicon connect commands
################################################################################

# arg0 = endian type
# arg1 = target string
# arg2 = configuration options

define _connectarmstmc2tp
  set endian $arg0
  set download-write-size 0
  set backtrace abi-sniffer off

  target shtdi libarmshdebug.so -sdi libarmsdi-stmc2.so -rtos libarmrtos-bare.so -io libarmio-posix.so -target $arg1 -targetpack $arg2 endian=$arg0

  _init_all_plugins

  enable_targetpack
  targetpack import $arg1
end

################################################################################

# arg0 = SDI server type
# arg1 = endian type
# arg2 = target string
# arg3 = configuration options

define _connectarmservertp
  set endian $arg1
  set download-write-size 0
  set backtrace abi-sniffer off

  target shtdi libarmshdebug.so -sdi libarmsdi-server.so -rtos libarmrtos-bare.so -io libarmio-posix.so -stmc libstmc1.so -server armsdi$arg0 -target $arg2 -targetpack $arg3 endian=$arg1

  _init_all_plugins

  enable_targetpack
  targetpack import $arg2
end

define _connectarmservertp-stmclite
  _connectarmservertp stmclite $arg0 $arg1 $arg2
end

################################################################################

# arg0 = endian type
# arg1 = target string
# arg2 = configuration options

define _connectarmautotp
  set endian $arg0
  set download-write-size 0
  set backtrace abi-sniffer off

  target shtdi libarmshdebug.so -sdi-auto stmc2=libarmsdi-stmc2.so -sdi-auto server@stmclite=libarmsdi-server.so@armsdistmclite -rtos libarmrtos-bare.so -io libarmio-posix.so -stmc libstmc1.so -target $arg1 -targetpack $arg2 endian=$arg0

  _init_all_plugins

  enable_targetpack
  targetpack import $arg1
end

################################################################################
# ARM little endian silicon connection wrapper commands
################################################################################

define connectarmstmc2tple
  _connectarmstmc2tp little $arg0 $arg1
end

document connectarmstmc2tple
Connect to and configure a little endian ARM silicon target
Usage: connectarmstmc2tple <target> <control-commands>
where <target> is an ST Micro Connect 2 target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmstmclitetple
  _connectarmservertp-stmclite little $arg0 $arg1
end

document connectarmstmclitetple
Connect to and configure a little endian ARM silicon target
Usage: connectarmstmclitetple <target> <control-commands>
where <target> is an ST Micro Connect Lite target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmautotple
  _connectarmautotp little $arg0 $arg1
end

document connectarmautotple
Connect to and configure a little endian ARM silicon target (auto detection of ST Micro Connect type)
Usage: connectarmautotple <target> <control-commands>
where <target> is an ST Micro Connect target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmtple
  connectarmautotple $arg0 $arg1
end

document connectarmtple
See "help connectarmautotple"
end

################################################################################
# ARM big endian silicon connection wrapper commands
################################################################################

define connectarmstmc2tpbe
  _connectarmstmc2tp big $arg0 $arg1
end

document connectarmstmc2tpbe
Connect to and configure a big endian ARM silicon target
Usage: connectarmstmc2tpbe <target> <control-commands>
where <target> is an ST Micro Connect 2 target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmstmclitetpbe
  _connectarmservertp-stmclite big $arg0 $arg1
end

document connectarmstmclitetpbe
Connect to and configure a big endian ARM silicon target
Usage: connectarmstmclitetpbe <target> <control-commands>
where <target> is an ST Micro Connect Lite target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmautotpbe
  _connectarmautotp big $arg0 $arg1
end

document connectarmautotpbe
Connect to and configure a big endian ARM silicon target (auto detection of ST Micro Connect type)
Usage: connectarmautotpbe <target> <control-commands>
where <target> is an ST Micro Connect target string
+     <control-commands> are commands to configure the ST Micro Connect
end

################################################################################

define connectarmtpbe
  connectarmautotpbe $arg0 $arg1
end

document connectarmtpbe
See "help connectarmautotpbe"
end

################################################################################
