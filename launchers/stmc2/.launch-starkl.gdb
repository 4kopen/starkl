# Don't pause at end of each screenful of GDB's output.
set pagination off

# Connect to the target.
armtp 10.20.30.50:boardentry:a9_0,boardrev=2,active_cores=a9_0:dbu,clk_shutdown=1,ddr_interleaving=4096,profile=h418,debugram_base=0x87FF8000,debugram_size=32768,boardrev=2,smp_mode=1

# Load the u-boot elf
load ./u-boot
symbol-file ./u-boot

# Stop at the env_relocate function to set the gd->env_valid variable to 1
hbreak *env_relocate
continue
del 1
set gd->env_valid=1

# Stop at the env_import function to load the env variables
hbreak *env_import
continue
del 2
restore __uboot_env__ binary $r0

# Load the PBL & Payload to be flashed.
restore __uboot_payload__ binary 0x98000000

if ! $flash_image
	#start other cpu
	set $load_offset=0x02000000
	symbol-file ./vmlinux
	hbreak *(&stext)+$load_offset
	continue

	#breakpoint hit, we start other cpus
	info threads nostackframe
	thread 2
	set $pc=(&sti_secondary_startup)+$load_offset
	set $cpsr &= ~(1<<5)
	thread 3
	set $pc=(&sti_secondary_startup)+$load_offset
	set $cpsr &= ~(1<<5)
	thread 4
	set $pc=(&sti_secondary_startup)+$load_offset
	set $cpsr &= ~(1<<5)
end

quit
