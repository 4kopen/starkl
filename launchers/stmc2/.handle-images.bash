if [ ! -e sdk2-dt.dtb ]; then
	echo "Required files not found, please check the specified path is correct."
	exit 1
fi

uboot_hwaddr_txt=$(mktemp)
echo -e "ethaddr=$uboot_macaddr" > "$uboot_hwaddr_txt"

modified_dtb=$(mktemp)
cp sdk2-dt.dtb "$modified_dtb"

# Discriminate in DT if images are for 2199 or 2264  board
model=$(fdtget $modified_dtb / model)

# if $board contains "2264" substring:
if [ -z "${model##*2264*}" ] ; then
	targetpack_boardentry=b2264stxh418
else
	targetpack_boardentry=b2199stxh418
fi

if [ $# -gt 2 ]; then
	fdtput --type s "$modified_dtb" /chosen bootargs "$(fdtget "$modified_dtb" /chosen bootargs) ${*:3}"

	echo "Linux fdt bootargs: $(fdtget "$modified_dtb" /chosen bootargs)"
fi

proot_options+=(-b u-boot.bin)

image_gz=$(mktemp)
gzip --to-stdout Image > "$image_gz"

# Prepare FIT template (used by mkimage & DTC to produce FIT)
# At the moment nothing has to be dynamically configured in
# this script.
image_tree_source=$(mktemp)
cat << HEREDOC > "$image_tree_source"
/*
 * Template to produce a simple U-Boot FIT uImage.
 * The image includes:
 *    - a single kernel image (gzip compressed)
 *    - the relevant DTB
 *    - a "config" node which tells to u-boot which images to
 *      to load and run (a FIT may contain copies of same object)
 */

/dts-v1/;

/ {
	description = "Flash FIT image";
	#address-cells = <1>;

	images {
		kernel@1 {
			description = "Starkl Linux kernel";
			data = /incbin/("$image_gz");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "gzip";
			load = <0x82008000>;
			entry = <0x82008000>;
			hash@1 {
				algo = "crc32";
			};
		};
		fdt@1 {
			description = "FDT blob";
			data = /incbin/("$modified_dtb");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			hash@1 {
				algo = "crc32";
			};
		};
	};

	/*
	 * Configuration is to tell to u-boot what to load and run
	 * by default (i.e. the behaviour of bootm command)
	 */
	configurations {
		default = "conf@1";
		conf@1 {
			description = "starkl boot";
			kernel = "kernel@1";
			fdt = "fdt@1";
		};
	};
};
HEREDOC

image_fit=$(mktemp)
mkimage -f "$image_tree_source" "$image_fit"
rm "$image_tree_source"
rm "$image_gz"

uboot_env_txt=$(mktemp)
if [ "$copy_to_external" -eq 1 ]; then
	uboot_payload=$image_fit
	cat <<- HEREDOC > "$uboot_env_txt"
		bootcmd=fatload \${bootdev} 0:1 0x98000000 uboot_payload.fit && bootm 0x98000000
	HEREDOC

elif [ "$flash_images" -eq 0 ]; then
	uboot_payload=$image_fit

	cat <<- HEREDOC > "$uboot_env_txt"
		bootdelay=0
		fdt_high=0xffffffff
		bootcmd=bootm 0x98000000
	HEREDOC
else
	if [ ! -e pbl_uboot.img ]; then
		cat <<- HEREDOC
			Required image not found.  It seems support for flash memory is not
			enabled for this configuration.  Please select:

			    boot & kernel options ---> [*] enable flash support

			in the Starkl menuconfig.
		HEREDOC
		exit 1
	fi

	# Assemble the primary boot image with the FIT image to form
	# a raw "Flash image" (this is an implementation choice to
	# facilitate u-boot life and Linux MTD partitioning)
	#     +----------+  0x0000:0000
	#     |          |
	#     |   pbl    |		OBSP + U-boot
	#     |          |
	#     +----------+  0x000C:0000
	#     |          |		Res: u-boot saves the env.
	#     +----------+  0x000D:0000
	#     |          |		Mac address of the ethernet IP
	#     +----------+  0x0010:0000
	#     |          |
	#     |   FIT    |		Kernel + DTB (see FIT format
	#       ......			in uboot DOC)
	#     |          |
	#     +----------+  end of Flash
	#
	# Note: the choice to limit the PBL to the first 768Kb (3/4 Mb)
	#       is absolutely arbitrary but largelly sufficient for any
	#	profile of PBL (obsp + u-boot)
	#
	uboot_payload=$(mktemp)

	dd if=pbl_uboot.img of="$uboot_payload" bs=256k        conv=sync
	dd if="$uboot_hwaddr_txt" of="$uboot_payload" bs=4096 seek=208 conv=sync
	dd if="$image_fit"  of="$uboot_payload" bs=256k seek=4 conv=sync

	function compute_aligned_size {
		size=$(stat -c %s "$2")
		blocks=$(( (size + 512) / 512))
		aligned_size=$((blocks * 512))

		eval "$1_aligned_size=$aligned_size"
	}

	compute_aligned_size payload "$uboot_payload"
	compute_aligned_size fit     "$image_fit"
	rm "$image_fit"

	cat <<- HEREDOC > "$uboot_env_txt"
		bootdelay=0
		load_env=env import -t 0x98000000 \${filesize}
		bootcmd=echo Updating SPI flash memory...;sf probe;sf update 0x98000000 0x0 $(printf %x "$payload_aligned_size");setenv bootcmd "run load_mac;echo \"Looking for uenv.txt on sdcard\";if mmc rescan && fatload mmc 0:1 0x98000000 uenv.txt; then setenv bootdev mmc; run load_env; boot; echo \"*** Boot from MMC failed... ***\"; fi; echo \"Looking for uenv.txt on usb 0:1\"; if usb start && usb storage && fatload usb 0:1 0x98000000 uenv.txt; then setenv bootdev usb; run load_env; boot; echo \"*** Boot from USB failed... ***\"; fi; echo \"Starting a default system\"; display_splash error; run error; sf probe; sf read 0x98000000 0x100000 $(printf %x "$fit_aligned_size"); bootm 0x98000000; reset;";setenv fdt_high 0xffffffff;setenv ledt "led green on;sleep 1;led green off;sleep 1;";setenv error "run ledt; run ledt; run ledt; run ledt; run ledt;";setenv load_mac "sf probe; sf read 0x97000000 0xD0000 1B;env import -t 0x97000000 1B;";saveenv;reset
	HEREDOC

fi

uboot_env=$(mktemp)

if [ "$copy_to_external" -eq 0 ]; then
	mkenvimage -s 16384 -o "$uboot_env" "$uboot_env_txt"
	proot_options+=(-b "$uboot_env")

	gdb_script=$(mktemp)

	sed_options+=(-e "s/^armtp 10.20.30.50:boardentry/armtp $stmc2_ip:$targetpack_boardentry/")
	sed_options+=(-e 's|restore __uboot_payload__|restore '$uboot_payload'|')
	sed_options+=(-e 's|restore __uboot_env__|restore '$uboot_env'|')

	sed "${sed_options[@]}" "${rootfs}/../.launch-starkl.gdb" > "$gdb_script"

	proot_options+=(-b "$gdb_script")
	proot_options+=(-b "$uboot_payload")
fi
