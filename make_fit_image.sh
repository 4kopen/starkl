
#!/bin/bash -eu
VAR1=${1:-}
VAR2=${2:-}
if [[ $VAR1 == "starkl"* ]]; then
  LOCATION=$VAR1
elif [[ $VAR2 == "starkl"* ]]; then
	LOCATION=$VAR2
else
	LOCATION="starkl-b2264-default"
fi
echo "making fit image inside outputs/${LOCATION}/images/"

cd ./outputs/"$LOCATION"/images/


KERNEL_ZIMAGE="zImage"
KERNEL_IMAGE="Image.gz"
KERNEL_OFFSET=$(binwalk -y gzip "${KERNEL_ZIMAGE}" | grep gzip | cut -d ' ' -f 1)

DEVICE_TREE="sdk2-dt.dtb"

FIT_CONFIG="kernel.conf"
FIT_IMAGE="kernel.fit"


dd if="${KERNEL_ZIMAGE}" skip=${KERNEL_OFFSET} iflag=skip_bytes \
	| gzip -d \
	| gzip -9 \
	> "${KERNEL_IMAGE}"


cat << EOF > "${FIT_CONFIG}"
/*
 * Template to produce a simple U-Boot FIT uImage.
 * The image includes:
 *    - a single kernel image (gzip compressed)
 *    - the relevant DTB
 *    - a "config" node which tells to u-boot which images to
 *      to load and run (a FIT may contain copies of same object)
 */

/dts-v1/;

/ {
	description = "Flash FIT image";
	#address-cells = <1>;

	images {
		kernel@1 {
			description = "Starkl Linux kernel";
			data = /incbin/("${KERNEL_IMAGE}");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "gzip";
			load = <0x82008000>;
			entry = <0x82008000>;
			hash@1 {
				algo = "crc32";
			};
		};
		fdt@1 {
			description = "FDT blob";
			data = /incbin/("${DEVICE_TREE}");
			type = "flat_dt";
			arch = "arm";
			compression = "none";
			hash@1 {
				algo = "crc32";
			};
		};
	};

	/*
	 * Configuration is to tell to u-boot what to load and run
	 * by default (i.e. the behaviour of bootm command)
	 */
	configurations {
		default = "conf@1";
		conf@1 {
			description = "starkl boot";
			kernel = "kernel@1";
			fdt = "fdt@1";
		};
	};
};
EOF

mkimage -f "${FIT_CONFIG}" "${FIT_IMAGE}"
if [[ ! $LOCATION == "starkl-b2264-raspbian" ]]; then
	if [[ $VAR1 == "tar" ]] || [[ $VAR2 == "tar" ]] || [[ $VAR1 == "uenv" ]] || [[ $VAR2 == "uenv" ]]; then
	  echo "Making uenv in location"
		echo 'bootcmd=fatload ${bootdev} 0:1 0x98000000 kernel.fit; bootm 0x98000000;' > uenv.txt
	fi
	if  [[ $VAR1 == "tar" ]] || [[ $VAR2 == "tar" ]]; then
		echo "Creating tar file with kernel.fit and uenv.txt"
		tar -cf kernel.tar kernel.fit uenv.txt
	fi
elif [[ $VAR1 == "tar" ]] || [[ $VAR2 == "tar" ]] || [[ $VAR1 == "uenv" ]] || [[ $VAR2 == "uenv" ]]; then
	echo "tar and uenv functionality disabled for raspbian due to different actions required"
fi
echo "Finished run"
